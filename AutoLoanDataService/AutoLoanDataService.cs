﻿using System;
using System.Collections.Generic;
using AutoLoanService.Interface;
using AutoLoanService.Entity;
using Azolution.Common.DataService;
using AutoLoanDataService.DataReader;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using Bat.Common;


namespace AutoLoanDataService
{

    public class AutoLoanDataService : Auto_InsuranceBancaRepository, AutoVehicleStatusIRRepository, AutoSCBStatusIRrepository, AutoVendorMOURepository, IRSettingsForPayrollRepository, ITanorAndLTV, IVehicleAndPriceSettings, IVendorNegativeListingRepository, IAutoVendorRepository, AutoSegmentSettingsRepository, DaviationSettingsRepository, ILoanApplicationRepository, IBankStatementRepository, Auto_BankStatementAvgTotalRepository
    {
        #region Auto_InsuranceBanca
        public List<Auto_InsuranceBanca> GetInsuranceAndBancaSettingsSummary(int pageNo, int pageSize, string search)
        {
            var condition = "";
            if (search != "")
            {
                condition = "where CompanyName like '%" + search + "%'";
            }
            var objAuto_InsuranceBancaList = new List<Auto_InsuranceBanca>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  CompanyName asc";
                string sql = string.Format(@"
select InsCompanyId,CompanyName,InsType,InsAgeLimit,InsVehicleCC,InsurancePercent,Tenor,ArtaRate,(select count(InsCompanyId) from auto_insurancebanca {2}) as TotalCount 
from auto_insurancebanca
{2}
{3}
LIMIT {0}, {1} 
", pageNumber, pageSize, condition, orderBy);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var auto_InsuranceBancaDataReader = new Auto_InsuranceBancaDataReader(reader);
                while (reader.Read())
                    objAuto_InsuranceBancaList.Add(auto_InsuranceBancaDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAuto_InsuranceBancaList;
        }

        public string SaveAuto_InsuranceBanca(Auto_InsuranceBanca insurance)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                string sql = @"INSERT INTO auto_insurancebanca
	(CompanyName,InsType,InsAgeLimit,InsVehicleCC,InsurancePercent,Tenor,ArtaRate)
	 VALUES ('" + insurance.CompanyName + "'," + insurance.InsType + "," + insurance.InsAgeLimit + "," + insurance.InsVehicleCc + "," + insurance.InsurancePercent + "," + insurance.Tenor+", " + insurance.ArtaRate + ")";

                if (insurance.InsCompanyId != 0)
                {
                    sql = @"UPDATE auto_insurancebanca SET
		CompanyName = '" + insurance.CompanyName + "',InsType = " + insurance.InsType + ",InsAgeLimit=" + insurance.InsAgeLimit + ",InsVehicleCC=" + insurance.InsVehicleCc + ",InsurancePercent=" + insurance.InsurancePercent + ",Tenor=" + insurance.Tenor + ", ArtaRate=" + insurance.ArtaRate + " WHERE InsCompanyId =" + insurance.InsCompanyId;
                }
                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }

        public string InsuranceDeleteById(int insurenceId)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                string quary = string.Format("Delete from auto_insurancebanca where InsCompanyId={0}", insurenceId);
                dbHelper.ExecuteNonQuery(quary);
            }
            catch
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }
        #endregion

        #region AutoVehicleStatusIR
        public List<AutoVehicleStatusIR> GetAutoVehicleStatusIRSummary(int pageNo, int pageSize)
        {
            //var condition = "";
            //if(search != "")
            //{
            //    condition = "where CompanyName like '%" + search + "%'";
            //}
            pageNo = pageNo * pageSize;
            var objAutoVehicleStatusIRList = new List<AutoVehicleStatusIR>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  VehicleStatus desc";
                string sql = string.Format(@"
Select VehicleStatusIRId,VehicleStatus,IrWithArta,IrWithoutArta,LastUpdateDate,auto_status.StatusName,
(select count(VehicleStatusIRId) from auto_vehiclestatusir) as TotalCount 
from auto_vehiclestatusir inner join auto_status on  auto_vehiclestatusir.VehicleStatus = auto_status.StatusId
{2}
LIMIT {0}, {1} 
", pageNumber, pageSize, orderBy);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleStatusIRDataReader = new AutoVehicleStatusIRDataReader(reader);
                while (reader.Read())
                    objAutoVehicleStatusIRList.Add(autoVehicleStatusIRDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoVehicleStatusIRList;
        }



        public string SaveAuto_IRVehicleStatus(AutoVehicleStatusIR insurance)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                //DateTime theDate = DateTime.Now;
                //string lastUpdateDate = theDate.to
                string sql = @"INSERT INTO auto_vehiclestatusir
	(VehicleStatus,IrWithArta,IrWithoutArta,LastUpdateDate)
	 VALUES (" + insurance.VehicleStatus + "," + insurance.IrWithArta + "," + insurance.IrWithoutArta + ",'" + DateTime.Now.ToString("yyyyMMdd") + "')";

                if (insurance.VehicleStatusIRId != 0)
                {
                    sql = @"UPDATE auto_vehiclestatusir SET
		VehicleStatus = " + insurance.VehicleStatus + ",IrWithArta = " + insurance.IrWithArta + ",IrWithoutArta=" + insurance.IrWithoutArta + ",LastUpdateDate = '" + DateTime.Now.ToString("yyyyMMdd") + "' WHERE VehicleStatusIRId =" + insurance.VehicleStatusIRId;
                }
                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }

        public string IRVehicleStatusDeleteById(int vehicleStatusIRId)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                string quary = string.Format("Delete from auto_vehiclestatusir where VehicleStatusIRId={0}", vehicleStatusIRId);
                dbHelper.ExecuteNonQuery(quary);
            }
            catch
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }
        #endregion

        #region AutoSCBStatusIR
        public List<AutoSCBStatusIR> GetAutoSCBStatusIRSummary(int pageNo, int pageSize)
        {
            //var condition = "";
            //if(search != "")
            //{
            //    condition = "where CompanyName like '%" + search + "%'";
            //}
            var objAutoSCBStatusIRList = new List<AutoSCBStatusIR>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  SCBStatus desc";
//                string sql = string.Format(@"
//select SCBStatusIRId,SCBStatus,IrWithArta,IrWithoutArta,LastUpdateDate,auto_status.StatusName,
//(select count(SCBStatusIRId) from auto_scbstatusir) as TotalCount 
//from auto_scbstatusir inner join auto_status on  auto_scbstatusir.SCBStatus = auto_status.StatusId
//{2}
//LIMIT {0}, {1} 
//", pageNumber, pageSize, orderBy);
                string sql = string.Format(@"
select SCBStatusIRId,SCBStatus,IrWithArta,IrWithoutArta,LastUpdateDate,
case SCBStatus when 1 then 'Priority Account Holder' when 2 then 'SCB PAYROLL CAT A' when 3 then 'SCB PAYROLL CAT B' when 4 then 'SCB PAYROLL CAT C' when 5 then 'NO CAT' end as StatusName,
(select count(SCBStatusIRId) from auto_scbstatusir) as TotalCount 
from auto_scbstatusir
{2}
LIMIT {0}, {1} 
", pageNumber, pageSize, orderBy);
                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoSCBStatusIRDataReader = new AutoSCBStatusIRDataReader(reader);
                while (reader.Read())
                    objAutoSCBStatusIRList.Add(autoSCBStatusIRDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoSCBStatusIRList;
        }



        public string SaveAuto_IRSCBStatus(AutoSCBStatusIR insurance)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                //DateTime theDate = DateTime.Now;
                //string lastUpdateDate = theDate.to
                string sql = @"INSERT INTO auto_scbstatusir
	(SCBStatus,IrWithArta,IrWithoutArta,LastUpdateDate) 
	 VALUES (" + insurance.SCBStatus + "," + insurance.IrWithArta + "," + insurance.IrWithoutArta + ",'" + DateTime.Now.ToString("yyyyMMdd") + "')";

                if (insurance.SCBStatusIRId > 0)
                {
                    sql = @"UPDATE auto_scbstatusir SET
		SCBStatus = " + insurance.SCBStatus + ",IrWithArta = " + insurance.IrWithArta + ",IrWithoutArta=" + insurance.IrWithoutArta + ",LastUpdateDate = '" + DateTime.Now.ToString("yyyyMMdd") + "' WHERE SCBStatusIRId =" + insurance.SCBStatusIRId;
                }
                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }

        public string IRSCBStatusDeleteById(int IRSCBStatusID)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                string quary = string.Format("Delete from auto_scbstatusir where SCBStatusIRId={0}", IRSCBStatusID);
                dbHelper.ExecuteNonQuery(quary);
            }
            catch
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }
        #endregion

        #region AutoVendorMOU
        public List<AutoVendorMOU> GetAutoVendorMOUSummary(int pageNo, int pageSize, string vendorName)
        {
            var condition = "where IsActive = 1";
            if (vendorName != "")
            {
                condition = "where IsActive=1 and VendorName like '%" + vendorName + "%'";
            }
            var objAutoVendorMOUList = new List<AutoVendorMOU>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  SCBStatus desc";
                //                string sql = string.Format(@"select VendorMouId,AutoVendorId,IrWithArta,IrWithoutArta,ProcessingFee,
                //ValidFrom,LastUpdateDate,VendorName,DealerType,Address,Phone,Website,DealerStatus,IsMou,
                //(Select count(AutoVendorId) from AutoVendorMOU_View {2}) as TotalCount from AutoVendorMOU_View {3}
                // 
                //
                //LIMIT {0}, {1} 
                //", pageNumber, pageSize, condition, condition);

                string sql = string.Format(@"select AutoVendorId,VendorName,VendorCode,Address,Phone,Website,isMOU,DealerType,IrWithArta,
IrWithoutArta,ProcessingFee,ValidFrom,StatusName as DealerStatus,IsActive,TotalCount from
(
SELECT s1.AutoVendorId, s1.VendorName,s1.VendorCode, s1.Address, s1.Phone,s1.Website,s1.isMOU,s1.DealerType, s2.IrWithArta,s2.IrWithoutArta,
s2.ProcessingFee,s2.ValidFrom,auto_status.StatusName,s1.IsActive,
(select count(*) from auto_vendor) AS TotalCount
FROM auto_vendor s1
Left outer JOIN auto_vendormou s2 ON s1.AutoVendorId = s2.AutoVendorId
and  s2.LastUpdateDate= 
(select Max(LastUpdateDate) as lsUpdateDate from auto_vendormou as s3 where s3.AutoVendorId=s1.AutoVendorId)
left join auto_status on auto_status.StatusId = s1.DealerType
order by s1.AutoVendorId asc) as tbl
{2}
order by VendorName
LIMIT {0}, {1} 
", pageNumber, pageSize, condition);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVendorMOUDataReader = new AutoVendorMOUDataReader(reader);
                while (reader.Read())
                    objAutoVendorMOUList.Add(autoVendorMOUDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoVendorMOUList;
        }

        public string SaveAuto_VendorMOU(AutoVendorMOU autoVendorMOU)
        {
            int vendorID = -1;
            DateTime theDate = DateTime.Now;
            string lastUpdateDate = theDate.ToString("yyyy-MM-dd");
            lastUpdateDate += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();

            DateTime thedate2 = autoVendorMOU.ValidFrom;
            string validFrom = thedate2.ToString("yyyy-MM-dd");
            validFrom += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();

            string returnValue = "";

            var dbHelper = new CommonDbHelper();
            dbHelper.BeginTransaction();
            try
            {

                //string insertQuery = "call Upload_PayRoll_VendorMOU_ExcelFile(" + autoVendorMOU.AutoVendorId + ",'" + autoVendorMOU.VendorName + "'," + autoVendorMOU.DealerType +
                //    ",'" + autoVendorMOU.Address + "','" + autoVendorMOU.phone + "','" + autoVendorMOU.Website + "'," +
                //    autoVendorMOU.IrWithArta + "," +
                //    autoVendorMOU.IrWithoutArta + "," +
                //    autoVendorMOU.ProcessingFee + ",'" +
                //    validFrom + "','" + lastUpdateDate + "','" + autoVendorMOU.IsMou + "')";

                var insertQuery = "";
                var parantTableQuery = "";
                if (autoVendorMOU.AutoVendorId > 0)
                {
                    insertQuery = string.Format(@"insert into auto_vendormou(AutoVendorId,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,LastUpdateDate)
values({0},'{1}','{2}','{3}','{4}','{5}')", autoVendorMOU.AutoVendorId, autoVendorMOU.IrWithArta, autoVendorMOU.IrWithoutArta, autoVendorMOU.ProcessingFee, validFrom, lastUpdateDate);
                    dbHelper.ExecuteNonQuery(insertQuery);

                    var updateQuary = string.Format(@"Update auto_vendor set VendorName = '{0}', VendorCode = '{1}', DealerType ={2}, Address='{3}', Phone='{4}', Website='{5}',IsMou = {6} where AutoVendorId = {7}",
                        autoVendorMOU.VendorName, autoVendorMOU.VendorCode, autoVendorMOU.DealerType, autoVendorMOU.Address, autoVendorMOU.phone, autoVendorMOU.Website, autoVendorMOU.IsMou, autoVendorMOU.AutoVendorId);
                    dbHelper.ExecuteNonQuery(updateQuary);

                }
                else
                {
                    insertQuery = string.Format(@"insert into auto_vendor(VendorName,VendorCode,DealerType,Address,Phone,Website, IsMou) values
                                    ('{0}','{1}',{2},'{3}','{4}','{5}', '{6}')"
                                    , autoVendorMOU.VendorName, autoVendorMOU.VendorCode, autoVendorMOU.DealerType, autoVendorMOU.Address, autoVendorMOU.phone, autoVendorMOU.Website, autoVendorMOU.IsMou);
                    dbHelper.ExecuteNonQuery(insertQuery);
                    parantTableQuery = string.Format(@"select AutoVendorId from auto_vendor 
where VendorName = '{0}' and DealerType = {1}", autoVendorMOU.VendorName, autoVendorMOU.DealerType);

                    IDataReader tempReder = dbHelper.GetDataReader(parantTableQuery);
                    //autoVendorID = 0;
                    while (tempReder.Read())
                    {
                        autoVendorMOU.AutoVendorId = tempReder.GetInt32(0);
                    }
                    tempReder.Close();
                    if (autoVendorMOU.AutoVendorId > 0)
                    {
                        insertQuery = string.Format(@"insert into auto_vendormou(AutoVendorId,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,LastUpdateDate)
values({0},'{1}','{2}','{3}','{4}','{5}')", autoVendorMOU.AutoVendorId, autoVendorMOU.IrWithArta, autoVendorMOU.IrWithoutArta, autoVendorMOU.ProcessingFee, validFrom, lastUpdateDate);
                        dbHelper.ExecuteNonQuery(insertQuery);
                    }
                }




                dbHelper.CommitTransaction();
                returnValue = "Success";
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                returnValue = "Failed";
            }
            finally
            {
                dbHelper.Close();
            }
            return returnValue;
        }

        public string SaveAuto_NegativeVendorNew(AutoVendorMOU autoVendorMOU)
        {
            DateTime theDate = DateTime.Now;
            string lastUpdateDate = theDate.ToString("yyyy-MM-dd");
            lastUpdateDate += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();

            DateTime thedate2 = autoVendorMOU.ValidFrom;
            string validFrom = thedate2.ToString("yyyy-MM-dd");
            validFrom += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();

            string returnValue = "";
            var insertQuery = "";
            var parantTableQuery = "";

            var dbHelper = new CommonDbHelper();
            dbHelper.BeginTransaction();

            try {
                insertQuery = string.Format(@"insert into auto_vendor(VendorName,DealerType,Address,Phone,Website, IsMou) values
                                    ('{0}',{1},'{2}','{3}','{4}', '{5}')"
                                    , autoVendorMOU.VendorName, autoVendorMOU.DealerType, autoVendorMOU.Address, autoVendorMOU.phone, autoVendorMOU.Website, autoVendorMOU.IsMou);
                dbHelper.ExecuteNonQuery(insertQuery);
                parantTableQuery = string.Format(@"select AutoVendorId from auto_vendor 
where VendorName = '{0}' and DealerType = {1}", autoVendorMOU.VendorName, autoVendorMOU.DealerType);

                IDataReader tempReder = dbHelper.GetDataReader(parantTableQuery);
                //autoVendorID = 0;
                while (tempReder.Read())
                {
                    autoVendorMOU.AutoVendorId = tempReder.GetInt32(0);
                }
                tempReder.Close();
                if (autoVendorMOU.AutoVendorId > 0)
                {
                    insertQuery = string.Format(@"insert into auto_vendormou(AutoVendorId,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,LastUpdateDate)
values({0},'{1}','{2}','{3}','{4}','{5}')", autoVendorMOU.AutoVendorId, autoVendorMOU.IrWithArta, autoVendorMOU.IrWithoutArta, autoVendorMOU.ProcessingFee, validFrom, lastUpdateDate);
                    dbHelper.ExecuteNonQuery(insertQuery);
                }

                var lastupdate =
                    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

                string sql = @"INSERT INTO auto_negativevendor
	(AutoVendorId,ListingDate)

	 VALUES (" + autoVendorMOU.AutoVendorId + ",'" + lastupdate + "')";


                dbHelper.ExecuteNonQuery(sql);
                dbHelper.CommitTransaction();
                returnValue = "Success";

            }
            catch (Exception ex) {
                returnValue = "Failed";
                dbHelper.RollBack();
            }
            finally {
                dbHelper.Close();
            }
            return returnValue;
        }

        public string DumpExcelFile_VendorMOU(string excelFilePath)
        {
            string returnValue = "";
            //string returnErrorValue = "Insertion Successfull with error. Few Data does not insert into database due to invalid Dealer Type.\n\n Those Serial number is : ";
            string returnErrorValue = "";

            string dealerTypeName = "";
            string vendorName = "";
            string vendorCode = "";
            string validFrom = "";
            string withARTA = "";
            string withoutArta = "";
            string processingFee = "";
            string address = "";
            string phoneNumber = "";
            string website = "";
            int dealerType = -1;
            int autoVendorID = -1;
            string ismou = "";


            string lastUpdateDate = "";
            string vDate = "";

            string parantTableQuery = "";
            string insertQuery = "";
            var objAutoStatusList = new List<AutoStatus>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFilePath + ";Extended Properties=Excel 8.0;";
                //string strConn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFilePath + ";Extended Properties=Excel 12.0;";

                //string sList = "All Cat";

                OleDbConnection oConn = new OleDbConnection();
                oConn.ConnectionString = strConn;
                oConn.Open();

                OleDbCommand oSelect = new OleDbCommand("SELECT * FROM [Sheet1$]", oConn);
                oSelect.CommandType = CommandType.Text;
                var dataTable = new DataTable();
                dataTable.Load(oSelect.ExecuteReader());


                //OleDbDataReader oReader = oSelect.ExecuteReader();

                //int a = oReader.RecordsAffected;
                objDbHelper.BeginTransaction();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    autoVendorID = -1;
                    dealerType = -1;

                    dealerTypeName = "";
                    vendorName = "";
                    vendorCode = "";
                    validFrom = "";
                    withARTA = "";
                    withoutArta = "";
                    processingFee = "";
                    address = "";
                    phoneNumber = "";
                    website = "";
                    ismou = "";

                    dealerTypeName = Convert.ToString(dataTable.Rows[i][0]).Trim();
                    vendorName = Convert.ToString(dataTable.Rows[i][2]).Trim();
                    vendorCode = Convert.ToString(dataTable.Rows[i][3]).Trim();
                    validFrom = Convert.ToString(dataTable.Rows[i][4]).Trim();
                    withARTA = Convert.ToString(dataTable.Rows[i][5]).Trim();
                    withoutArta = Convert.ToString(dataTable.Rows[i][6]).Trim();
                    processingFee = Convert.ToString(dataTable.Rows[i][7]).Trim();
                    address = Convert.ToString(dataTable.Rows[i][8]).Trim();
                    phoneNumber = Convert.ToString(dataTable.Rows[i][9]).Trim();
                    website = Convert.ToString(dataTable.Rows[i][10]).Trim();
                    ismou = Convert.ToString(dataTable.Rows[i][11]).Trim();

                    if (vendorName == "" || dealerTypeName == "" || withARTA == "" || withoutArta == "" || validFrom == "")
                    {
                        returnErrorValue += " ," + Convert.ToString(dataTable.Rows[i][1]).Trim();
                    }
                    else
                    {
                        DateTime theDate = DateTime.Now;
                        lastUpdateDate = theDate.ToString("yyyy-MM-dd");
                        lastUpdateDate += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();

                        dealerTypeName = dealerTypeName.Replace("'", "''");
                        vendorName = vendorName.Replace("'", "''");
                        vendorCode = vendorCode.Replace("'", "''");
                        validFrom = validFrom.Replace("'", "''");
                        withARTA = withARTA.Replace("'", "''");
                        withoutArta = withoutArta.Replace("'", "''");
                        processingFee = processingFee.Replace("'", "''");
                        address = address.Replace("'", "''");
                        phoneNumber = phoneNumber.Replace("'", "''");
                        website = website.Replace("'", "''");
                        ismou = ismou.Replace("'", "''");

                        //parantTableQuery = "Select AutoCompanyId from auto_company where Replace(companyName,'\\'','^') = '" + replacedCompanyname + "'";
                        parantTableQuery = "Select StatusID from auto_status where StatusName = '" + dealerTypeName + "'";

                        IDataReader tempDataReder = objDbHelper.GetDataReader(parantTableQuery);
                        while (tempDataReder.Read())
                        {
                            dealerType = tempDataReder.GetInt32(0);
                        }
                        tempDataReder.Close();
                        if (dealerType < 0)
                        {
                            returnErrorValue += " ," + Convert.ToString(dataTable.Rows[i][1]).Trim();
                        }

                        else
                        {
                            parantTableQuery = "Select AutoVendorId from auto_vendor where IsActive=1 and VendorName = '" + vendorName +
                                "' and DealerType = (Select StatusID from auto_status where StatusName = '" + dealerTypeName + "')";

                            IDataReader tempReder = objDbHelper.GetDataReader(parantTableQuery);
                            while (tempReder.Read())
                            {
                                autoVendorID = tempReder.GetInt32(0);
                            }
                            tempReder.Close();


                            if (validFrom == "")
                            {
                                vDate = validateParametar(validFrom, "string");
                            }
                            else
                            {
                                DateTime theDatevDate = Convert.ToDateTime(validFrom);
                                vDate = theDatevDate.ToString("yyyy-MM-dd");
                                vDate += " " + theDatevDate.Hour.ToString() + ":" + theDatevDate.Minute.ToString() + ":" + theDatevDate.Second.ToString();
                                vDate = validateParametar(vDate, "string");
                            }

                            dealerTypeName = validateParametar(dealerTypeName, "string");
                            vendorName = validateParametar(vendorName, "string");
                            vendorCode = validateParametar(vendorCode, "string");
                            withARTA = validateParametar(withARTA, "int");
                            withoutArta = validateParametar(withoutArta, "int");
                            processingFee = validateParametar(processingFee, "int");
                            address = validateParametar(address, "string");
                            phoneNumber = validateParametar(phoneNumber, "string");
                            website = validateParametar(website, "string");
                            ismou = validateParametar(ismou, "string");
                            int mou = 1;
                            if (ismou == "No")
                            {
                                mou = 0;
                            }

                            //vDate = validateParametar(vDate, "string");
                            //lastUpdateDate = validateParametar(lastUpdateDate, "string");
                            //dealerType = -1;
                            //autoVendorID = -1;

                            //insertQuery = "call Upload_PayRoll_VendorMOU_ExcelFile(" + autoVendorID + "," + vendorName + "," + dealerType +
                            //    "," + address + "," + phoneNumber + "," + website + "," + withARTA + "," + withoutArta + "," + processingFee +
                            //    "," + vDate + ",'" + lastUpdateDate + "'," + mou + ")";

                            if (autoVendorID > 0)
                            {
                                insertQuery = string.Format(@"insert into auto_vendormou(AutoVendorId,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,LastUpdateDate)
values({0},{1},{2},{3},{4},'{5}')", autoVendorID, withARTA, withoutArta, processingFee, vDate, lastUpdateDate);
                                objDbHelper.ExecuteNonQuery(insertQuery);

                                var updateQuary = string.Format(@"Update auto_vendor set VendorName = {0}, VendorCode={1}, DealerType ={2}, Address={3}, Phone={4}, Website={5},IsMou = {6} where AutoVendorId = {7}",
                                vendorName, vendorCode, dealerType, address, phoneNumber, website, mou, autoVendorID);
                                objDbHelper.ExecuteNonQuery(updateQuary);

                            }
                            else
                            {
                                insertQuery = string.Format(@"insert into auto_vendor(VendorName, VendorCode,DealerType,Address,Phone,Website, IsMou) values
                                    ({0},{1},{2},{3},{4}, {5}, {6})"
                                    , vendorName, vendorCode, dealerType, address, phoneNumber, website, mou);
                                objDbHelper.ExecuteNonQuery(insertQuery);
                                parantTableQuery = string.Format(@"select AutoVendorId from auto_vendor 
where VendorName = {0} and DealerType = {1}", vendorName, dealerType);

                                tempReder = objDbHelper.GetDataReader(parantTableQuery);
                                autoVendorID = 0;
                                while (tempReder.Read())
                                {
                                    autoVendorID = tempReder.GetInt32(0);
                                }
                                tempReder.Close();
                                if (autoVendorID > 0)
                                {
                                    insertQuery = string.Format(@"insert into auto_vendormou(AutoVendorId,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,LastUpdateDate)
values({0},{1},{2},{3},{4},'{5}')", autoVendorID, withARTA, withoutArta, processingFee, vDate, lastUpdateDate);
                                    objDbHelper.ExecuteNonQuery(insertQuery);
                                }


                            }

                            //objDbHelper.ExecuteNonQuery(insertQuery);
                            returnValue = "Success";
                        }
                    }
                }
                //oReader.Close();
                oConn.Close();
                objDbHelper.CommitTransaction();
                //oReader.Close();

                if (returnErrorValue != "")
                {
                    returnValue = "Success with few missing row.\n\nFew dealer type does not match with the existing dealer type.\n\n Missing Serial Numbers are : " + returnErrorValue;
                }
                else
                {
                    returnValue = "Success";
                }

            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
                objDbHelper.RollBack();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return returnValue;
        }


        private string validateParametar(string value, string type)
        {
            string returnValue = "";

            if (value == "")
            {
                if (type == "int")
                {
                    returnValue = "0";
                }
                else if (type == "string")
                {
                    returnValue = "null";
                }
            }
            else
            {
                if (type == "int")
                {
                    returnValue = value;
                }
                else if (type == "string")
                {
                    returnValue = "'" + value + "'";
                }
            }

            return returnValue;
        }

        public string AutoVendorMOUInactiveById(int autoVendorId) {
            var message = "";
            var dbHelper = new CommonDbHelper(); 
            try {
                var quary = "Update auto_vendor set IsActive=0 where AutoVendorId = " + autoVendorId;
                dbHelper.ExecuteNonQuery(quary);
                message = "Success";
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                message = "Failed";
            }
            finally
            {
                dbHelper.Close();
            }
            return message;
        }


        #endregion

        #region IRSettingsForPayroll

        public string DumpExcelFile(string excelFilePath)
        {
            string returnValue = "";
            string companyName = "";
            string companyCode = "";
            string remark = "";
            //string replacedCompanyname = "";
            string PayrollRate = "";
            string ProcessingFee = "";
            string CategoryName = "";
            int companyID = -1;
            DateTime theDate = DateTime.Now;
            string lastUpdateDate = theDate.ToString("yyyy-MM-dd");
            lastUpdateDate += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();

            string parantTableQuery = "";
            //string childTableQuery = "";
            var objAutoStatusList = new List<AutoStatus>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFilePath + ";Extended Properties=Excel 8.0;";
                //string strConn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFilePath + ";Extended Properties=Excel 12.0;";

                //string sList = "All Cat";

                OleDbConnection oConn = new OleDbConnection();
                oConn.ConnectionString = strConn;
                oConn.Open();

                OleDbCommand oSelect = new OleDbCommand("SELECT * FROM [All Cat$]", oConn);
                oSelect.CommandType = CommandType.Text;
                OleDbDataReader oReader = oSelect.ExecuteReader();

                int a = oReader.RecordsAffected;
                objDbHelper.BeginTransaction();
                while (oReader.Read())
                {
                    companyID = -1;
                    companyName = "";
                    PayrollRate = "";
                    ProcessingFee = "";
                    CategoryName = "";
                    companyCode = "";
                    remark = "";
                    //replacedCompanyname = "";

                    companyName = Convert.ToString(oReader.GetValue(2)).Trim();
                    PayrollRate = Convert.ToString(oReader.GetValue(3)).Trim();
                    ProcessingFee = Convert.ToString(oReader.GetValue(4)).Trim();
                    CategoryName = Convert.ToString(oReader.GetValue(0)).Trim();
                    try
                    {
                        companyCode = Convert.ToString(oReader.GetValue(5)).Trim();
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        remark = Convert.ToString(oReader.GetValue(6)).Trim();
                    }
                    catch (Exception ex)
                    {

                    }




                    companyName = companyName.Replace("'", "''");
                    PayrollRate = PayrollRate.Replace("'", "''");
                    ProcessingFee = ProcessingFee.Replace("'", "''");
                    CategoryName = CategoryName.Replace("'", "''");
                    companyCode = companyCode.Replace("'", "''");
                    remark = remark.Replace("'", "''");
                    //parantTableQuery = "Select AutoCompanyId from auto_company where Replace(companyName,'\\'','^') = '" + replacedCompanyname + "'";
                    parantTableQuery = "Select AutoCompanyId from auto_company where companyName = '" + companyName + "'";

                    IDataReader tempDataReder = objDbHelper.GetDataReader(parantTableQuery);
                    while (tempDataReder.Read())
                    {
                        companyID = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                    //string insertQuery = "call Upload_PayRoll_Rate_ExcelFile(" + companyID + ",'" + CategoryName + "','" + companyName +
                    //    "','" + PayrollRate + "','" + ProcessingFee + "','" + lastUpdateDate + "','" + companyCode + "','" + remark + "')";

                    string insertQuery = "";
                    if (companyID > 0)
                    {
                        insertQuery = string.Format(@"insert into auto_payrollrate(AutoCompanyId,CategoryName,PayrollRate,ProcessingFee,LastUpdateDate,Remark)
values({0},'{1}','{2}','{3}','{4}', '{5}')", companyID, CategoryName, PayrollRate, ProcessingFee, lastUpdateDate, remark);
                        objDbHelper.ExecuteNonQuery(insertQuery);
                        insertQuery = string.Format(@"update auto_company set CompanyName='{0}', CompanyCode='{1}',Remarks='{2}' where AutoCompanyId = {3}", companyName, companyCode, remark, companyID);
                        objDbHelper.ExecuteNonQuery(insertQuery);
                    }
                    else
                    {
                        insertQuery = string.Format(@"insert into auto_company(CompanyName,CompanyCode,Remarks) values('{0}','{1}', '{2}')", companyName, companyCode, remark);
                        objDbHelper.ExecuteNonQuery(insertQuery);
                        parantTableQuery = "Select AutoCompanyId from auto_company where companyName = '" + companyName + "'";

                        tempDataReder = objDbHelper.GetDataReader(parantTableQuery);
                        while (tempDataReder.Read())
                        {
                            companyID = tempDataReder.GetInt32(0);
                        }
                        tempDataReder.Close();
                        insertQuery = string.Format(@"insert into auto_payrollrate(AutoCompanyId,CategoryName,PayrollRate,ProcessingFee,LastUpdateDate,Remark)
values({0},'{1}','{2}','{3}','{4}', '{5}')", companyID, CategoryName, PayrollRate, ProcessingFee, lastUpdateDate, remark);
                        objDbHelper.ExecuteNonQuery(insertQuery);
                    }

                    returnValue = "Success";

                }
                oReader.Close();
                oConn.Close();
                objDbHelper.CommitTransaction();
                //oReader.Close();
            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
                objDbHelper.RollBack();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return returnValue;
        }

        public List<AutoPayrollRate> GetPayRollSettingsSummary(int pageNo, int pageSize, string search)
        {
            var condition = "";
            var condition2 = "";
            if (search != "")
            {
                condition = "where auto_company.CompanyName like '%" + search + "%'";
                condition2 = " and auto_company.CompanyName like '%" + search + "%'";
            }
            var objAutoPayrollRate = new List<AutoPayrollRate>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  CompanyName asc";
                string sql = string.Format(@"
select auto_company.AutoCompanyId, auto_company.CompanyName, auto_company.CompanyCode,auto_payrollrate.Remark,auto_payrollrate.AutoPayRateId,
auto_payrollrate.AutoCompanyId,auto_payrollrate.CategoryName,auto_payrollrate.PayrollRate,auto_payrollrate.ProcessingFee,
Max(auto_payrollrate.LastUpdateDate) as LastUpdateDate,
(select count(auto_company.AutoCompanyId) from auto_company {2}) as TotalCount
from auto_company
inner join auto_payrollrate on auto_company.AutoCompanyId = auto_payrollrate.AutoCompanyId

where auto_payrollrate.LastUpdateDate = (select max(a.LastUpdateDate) from auto_payrollrate a 
where a.AutoCompanyId = auto_payrollrate.AutoCompanyId) {4}

group by auto_company.AutoCompanyId
{3}
LIMIT {0}, {1} 
", pageNumber, pageSize, condition, orderBy, condition2);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPayrollRateDataReader = new AutoPayrollRateDataReader(reader);
                while (reader.Read())
                    objAutoPayrollRate.Add(autoPayrollRateDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoPayrollRate;

        }

        public string savePayroll(AutoPayrollRate autoPayrollRate)
        {
            string query = "";
            int companyID = 0;
            DateTime theDate = DateTime.Now;
            string lastUpdateDate = theDate.ToString("yyyy-MM-dd");
            lastUpdateDate += " " + theDate.Hour.ToString() + ":" + theDate.Minute.ToString() + ":" + theDate.Second.ToString();


            var dbHelper = new CommonDbHelper();
            try
            {
                query = "Select AutoCompanyId from auto_company where CompanyName = '" + autoPayrollRate.CompanyName + "'";

                IDataReader tempDataReder = dbHelper.GetDataReader(query);
                while (tempDataReder.Read())
                {
                    companyID = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
                //string insertQuery = "call Upload_PayRoll_Rate_ExcelFile(" + companyID + ",'" + autoPayrollRate.CategoryName + "','" + autoPayrollRate.CompanyName +
                //    "','" + autoPayrollRate.PayrollRate + "','" + autoPayrollRate.ProcessingFee + "','" + lastUpdateDate + "','" + autoPayrollRate.CompanyCode + "','" + autoPayrollRate.Remarks + "')";


                //dbHelper.ExecuteNonQuery(insertQuery);

                string insertQuery = "";
                if (companyID > 0)
                {
                    insertQuery = string.Format(@"insert into auto_payrollrate(AutoCompanyId,CategoryName,PayrollRate,ProcessingFee,LastUpdateDate,Remark)
values({0},'{1}','{2}','{3}','{4}', '{5}')", companyID, autoPayrollRate.CategoryName, autoPayrollRate.PayrollRate, autoPayrollRate.ProcessingFee, lastUpdateDate, autoPayrollRate.Remarks);
                    dbHelper.ExecuteNonQuery(insertQuery);
                    insertQuery = string.Format(@"update auto_company set CompanyName='{0}', CompanyCode='{1}',Remarks='{2}' where AutoCompanyId = {3}", autoPayrollRate.CompanyName, autoPayrollRate.CompanyCode, autoPayrollRate.Remarks, companyID);
                    dbHelper.ExecuteNonQuery(insertQuery);
                }
                else
                {
                    insertQuery = string.Format(@"insert into auto_company(CompanyName,CompanyCode,Remarks) values('{0}','{1}', '{2}')", autoPayrollRate.CompanyName, autoPayrollRate.CompanyCode, autoPayrollRate.Remarks);
                    dbHelper.ExecuteNonQuery(insertQuery);
                    query = "Select AutoCompanyId from auto_company where companyName = '" + autoPayrollRate.CompanyName + "'";

                    tempDataReder = dbHelper.GetDataReader(query);
                    while (tempDataReder.Read())
                    {
                        companyID = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                    insertQuery = string.Format(@"insert into auto_payrollrate(AutoCompanyId,CategoryName,PayrollRate,ProcessingFee,LastUpdateDate,Remark)
values({0},'{1}','{2}','{3}','{4}', '{5}')", companyID, autoPayrollRate.CategoryName, autoPayrollRate.PayrollRate, autoPayrollRate.ProcessingFee, lastUpdateDate, autoPayrollRate.Remarks);
                    dbHelper.ExecuteNonQuery(insertQuery);
                }


            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                throw ex;
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }

        #endregion

        #region GetStatus(int statusID)
        public List<AutoStatus> GetStatus(int statusID)
        {
            var objAutoStatusList = new List<AutoStatus>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  StatusName asc";
                string sql = string.Format(@"
                                Select * from auto_status where StatusType={0}", statusID, orderBy);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoStatusDataReader = new AutoStatusDataReader(reader);
                while (reader.Read())
                    objAutoStatusList.Add(autoStatusDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoStatusList;
        }


        #endregion

        #region GetProfession(int ProfID)
        public List<AutoProfession> GetProfession()
        {
            var objAutoProfessionList = new List<AutoProfession>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                //string orderBy = "ORDER BY  PROF_NAME asc";
                string sql = "Select PROF_ID,PROF_NAME from t_pluss_profession";

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoProfessionDataReader = new AutoProfessionDataReader(reader);
                while (reader.Read())
                    objAutoProfessionList.Add(autoProfessionDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoProfessionList;
        }


        #endregion


        #region Vehicle and Price Settings

        public void SaveVehicleAndPriceForImport(AutoMenufacturer objAutoMenufacturer, AutoVehicle objAutoVehicle, List<AutoPrice> objAutoPriceList, CommonDbHelper objDbHelper)
        {
            int manufacturerId = 0;
            int vehicleId = 0;
            try
            {
                string quary = "select ManufacturerId from auto_menufacturer where ManufacturerName='" + objAutoMenufacturer.ManufacturerName + "'";

                IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    manufacturerId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
                objAutoMenufacturer.ManufacturerId = manufacturerId;
                //if (manufacturerId == 0)
                //{
                    manufacturerId = SaveManufecture(objAutoMenufacturer, objDbHelper);
                //}

                quary =
                    "select VehicleId,ManufacturerId,Model,TrimLevel,Type,CC,ChassisCode from auto_vehicle where ManufacturerId=" +
                    manufacturerId + " and Model='" + objAutoVehicle.Model + "' and TrimLevel='" +
                    objAutoVehicle.TrimLevel + "' and CC=" + objAutoVehicle.CC + " and ChassisCode='" +
                    objAutoVehicle.ChassisCode + "'";
                tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    vehicleId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
                objAutoVehicle.VehicleId = vehicleId;
                //if (vehicleId == 0)
                //{
                    objAutoVehicle.ManufacturerId = manufacturerId;
                    vehicleId = SaveVehicle(objAutoVehicle, objDbHelper);

                //}
                objAutoVehicle.VehicleId = vehicleId;

                foreach (var autoPrice in objAutoPriceList)
                {
                    autoPrice.VehicleId = vehicleId;
                    SaveAutoPrice(autoPrice, objDbHelper);
                }

            }
            catch (Exception)
            {
                objDbHelper.RollBack();
            }
        }

        private void SaveAutoPrice(AutoPrice autoPrice, CommonDbHelper objDbHelper)
        {
            try
            {
                var lastupdate =
                    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();


                string quary =
                        "INSERT INTO auto_price (VehicleId, StatusId, ManufacturingYear, MinimumPrice,MaximumPrice,IsActive,LastUpdateDate) values (" +
                        autoPrice.VehicleId + "," + autoPrice.StatusId + "," + autoPrice.ManufacturingYear + ",'" + autoPrice.MinimumPrice + "','" +
                        autoPrice.MaximumPrice + "'," + autoPrice.IsActive + ",'" + lastupdate + "')";
                objDbHelper.ExecuteNonQuery(quary);
            }
            catch (Exception)
            {
                objDbHelper.RollBack();
            }
        }

        private int SaveVehicle(AutoVehicle objAutoVehicle, CommonDbHelper objDbHelper)
        {
            int vehicleId = 0;
            try
            {
                if (objAutoVehicle.VehicleId == 0)
                {
                    string quary =
                        "INSERT INTO auto_vehicle (ManufacturerId, Model, ModelCode, TrimLevel, Type,CC,ChassisCode) values (" +
                        objAutoVehicle.ManufacturerId + ", '" + objAutoVehicle.Model + "','" + objAutoVehicle.ModelCode + "','" +objAutoVehicle.TrimLevel + "','" + objAutoVehicle.Type + "'," +
                        objAutoVehicle.CC + ",'" + objAutoVehicle.ChassisCode + "')";
                    objDbHelper.ExecuteNonQuery(quary);
                    quary = "select VehicleId,ManufacturerId,Model,TrimLevel,Type,CC,ChassisCode from auto_vehicle where ManufacturerId=" +
                            objAutoVehicle.ManufacturerId + " and Model='" + objAutoVehicle.Model + "' and TrimLevel='" + objAutoVehicle.TrimLevel + "' and CC=" + objAutoVehicle.CC + " and ChassisCode='" + objAutoVehicle.ChassisCode + "'";
                    IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                    while (tempDataReder.Read())
                    {
                        vehicleId = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                }
                else
                {
                    string quary = "Update auto_vehicle set Model='" +
                                   objAutoVehicle.Model + "', ModelCode='" + objAutoVehicle.ModelCode + "', TrimLevel='" +
                                   objAutoVehicle.TrimLevel + "',Type='" + objAutoVehicle.Type + "',CC=" + objAutoVehicle.CC + ",ChassisCode='" + objAutoVehicle.ChassisCode +
                                   "' where VehicleId=" + objAutoVehicle.VehicleId;
                    objDbHelper.ExecuteNonQuery(quary);
                    vehicleId = objAutoVehicle.VehicleId;
                }


            }
            catch (Exception)
            {

                objDbHelper.RollBack();
            }
            return vehicleId;
        }



        public int SaveManufecture(AutoMenufacturer objAutoMenufacturer, CommonDbHelper objDbHelper)
        {
            int manufacturerId = 0;
            try
            {
                if (objAutoMenufacturer.ManufacturerId == 0)
                {
                    string quary =
                        "INSERT INTO auto_menufacturer (ManufacturerName,ManufacturerCode, ManufacturCountry, Remarks) values ('" +
                        objAutoMenufacturer.ManufacturerName + "', '" + objAutoMenufacturer.ManufacturerCode + "', '" + objAutoMenufacturer.ManufacturCountry + "','" +
                        objAutoMenufacturer.Remarks + "')";
                    objDbHelper.ExecuteNonQuery(quary);
                    quary = "select ManufacturerId from auto_menufacturer where ManufacturerName='" + objAutoMenufacturer.ManufacturerName + "'";
                    IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                    while (tempDataReder.Read())
                    {
                        manufacturerId = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                }
                else
                {
                    string quary = "Update auto_menufacturer set ManufacturerName='" +
                                   objAutoMenufacturer.ManufacturerName + "', ManufacturerCode='" + objAutoMenufacturer.ManufacturerCode + "', ManufacturCountry='" +
                                   objAutoMenufacturer.ManufacturCountry + "',Remarks='" + objAutoMenufacturer.Remarks +
                                   "' where ManufacturerId=" + objAutoMenufacturer.ManufacturerId;
                    objDbHelper.ExecuteNonQuery(quary);
                    manufacturerId = objAutoMenufacturer.ManufacturerId;
                }


            }
            catch (Exception)
            {

                objDbHelper.RollBack();
            }
            return manufacturerId;
        }

        public List<AutoVehicle> GetVehicleSummary(int pageNo, int pageSize, string search)
        {
            var condition = "";
            if (search != "")
            {
                condition = "where auto_menufacturer.ManufacturerName like '%" + search +
                            "%' or auto_vehicle.Model like '%" + search + "%' or auto_vehicle.TrimLevel like '%" +
                            search + "%' or auto_vehicle.CC like '%" + search + "%' or auto_vehicle.ChassisCode like '%" +
                            search + "%' or auto_vehicle.Type like '%" + search + "%'";
            }
            var objAutoVehicle = new List<AutoVehicle>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  auto_menufacturer.ManufacturerName asc";
                string sql = string.Format(@"
select auto_vehicle.VehicleId,auto_vehicle.ManufacturerId,auto_vehicle.Model,auto_vehicle.ModelCode,auto_vehicle.TrimLevel,auto_vehicle.Type,auto_vehicle.CC,
auto_vehicle.ChassisCode,auto_menufacturer.ManufacturerName,auto_menufacturer.ManufacturCountry,auto_menufacturer.ManufacturerCode, (select count(auto_vehicle.VehicleId) from auto_vehicle) as TotalCount
from auto_vehicle inner join auto_menufacturer on auto_vehicle.ManufacturerId = auto_menufacturer.ManufacturerId
{2}
{3}
LIMIT {0}, {1} 
", pageNumber, pageSize, condition, orderBy);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleDataReader = new AutoVehicleDataReader(reader);
                while (reader.Read())
                    objAutoVehicle.Add(autoVehicleDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoVehicle;
        }

        public List<AutoPrice> GetAutoPriceByVehicleId(int vehicleId)
        {

            var objAutoPrice = new List<AutoPrice>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  auto_menufacturer.ManufacturerName asc";
                string sql = string.Format(@"
select auto_price.VehicleId,auto_price.StatusId,auto_price.ManufacturingYear,auto_price.MinimumPrice,
auto_price.MaximumPrice,Max(auto_price.LastUpdateDate) as LastUpdateDate,auto_status.StatusName
from auto_price inner join auto_status on auto_price.StatusId = auto_status.StatusId
where VehicleId={0} and auto_price.IsActive=1
group by auto_price.StatusId,auto_price.ManufacturingYear
order by auto_price.ManufacturingYear desc
", vehicleId);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPriceDataReader = new AutoPriceDataReader(reader);
                while (reader.Read())
                    objAutoPrice.Add(autoPriceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoPrice;
        }

        public string SaveAutoPrice(AutoPrice autoPrice)
        {
            var msg = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                SaveAutoPrice(autoPrice, objDbHelper);
                msg = "Success";
            }
            catch (Exception exception)
            {
                msg = exception.Message;
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return msg;
        }

        public string SaveAutoVehicle(AutoVehicle autoVehicle)
        {
            var msg = "";
            int vehicleId = 0;
            var objDbHelper = new CommonDbHelper();
            try
            {
                if (autoVehicle.VehicleId == 0)
                {
                    string quary =
                        "INSERT INTO auto_vehicle (ManufacturerId, Model, TrimLevel, Type,CC,ChassisCode,ModelCode) values (" +
                        autoVehicle.ManufacturerId + ", '" + autoVehicle.Model + "','" + autoVehicle.TrimLevel + "','" + autoVehicle.Type + "'," +
                        autoVehicle.CC + ",'" + autoVehicle.ChassisCode + "','" + autoVehicle.ModelCode + "')";
                    objDbHelper.ExecuteNonQuery(quary);
                    quary = "select VehicleId,ManufacturerId,Model,TrimLevel,Type,CC,ChassisCode from auto_vehicle where ManufacturerId=" +
                            autoVehicle.ManufacturerId + " and Model='" + autoVehicle.Model + "' and TrimLevel='" + autoVehicle.TrimLevel +
                            "' and CC=" + autoVehicle.CC + " and ChassisCode='" + autoVehicle.ChassisCode + "'";
                    IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                    while (tempDataReder.Read())
                    {
                        vehicleId = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                }
                else
                {
                    string quary = "Update auto_vehicle set Model='" +
                                   autoVehicle.Model + "', TrimLevel='" +
                                   autoVehicle.TrimLevel + "',Type='" + autoVehicle.Type + "',CC=" + autoVehicle.CC +
                                   ",ChassisCode='" + autoVehicle.ChassisCode + "',ModelCode='" + autoVehicle.ModelCode +
                                   "' where VehicleId=" + autoVehicle.VehicleId;
                    objDbHelper.ExecuteNonQuery(quary);
                    vehicleId = autoVehicle.VehicleId;
                }
                string quaryNew = "Update auto_menufacturer set ManufacturerCode='" +
                                   autoVehicle.ManufacturerCode + "', ManufacturCountry='" +
                                   autoVehicle.ManufacturCountry +
                                   "' where ManufacturerId=" + autoVehicle.ManufacturerId;
                objDbHelper.ExecuteNonQuery(quaryNew);
                msg = "Success";
            }
            catch (Exception exception)
            {
                msg = exception.Message;
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return msg;
        }

        public int SaveAutoVehicle(AutoVehicle autoVehicle, CommonDbHelper objDbHelper)
        {
            var msg = "";
            int vehicleId = 0;
            try
            {
                if (autoVehicle.VehicleId == 0)
                {
                    string quary =
                        "INSERT INTO auto_vehicle (ManufacturerId, Model, TrimLevel, Type,CC,ChassisCode,ModelCode) values (" +
                        autoVehicle.ManufacturerId + ", '" + autoVehicle.Model + "','" + autoVehicle.TrimLevel + "','" + autoVehicle.Type + "'," +
                        autoVehicle.CC + ",'" + autoVehicle.ChassisCode + "','" + autoVehicle.ModelCode + "')";
                    objDbHelper.ExecuteNonQuery(quary);
                    quary = "select VehicleId,ManufacturerId,Model,TrimLevel,Type,CC,ChassisCode from auto_vehicle where ManufacturerId=" +
                            autoVehicle.ManufacturerId + " and Model='" + autoVehicle.Model + "' and TrimLevel='" + autoVehicle.TrimLevel +
                            "' and CC=" + autoVehicle.CC + " and ChassisCode='" + autoVehicle.ChassisCode + "'";
                    IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                    while (tempDataReder.Read())
                    {
                        vehicleId = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                }
                else
                {
                    string quary = "Update auto_vehicle set Model='" +
                                   autoVehicle.Model + "', TrimLevel='" +
                                   autoVehicle.TrimLevel + "',Type='" + autoVehicle.Type + "',CC=" + autoVehicle.CC +
                                   ",ChassisCode='" + autoVehicle.ChassisCode + "',ModelCode='" + autoVehicle.ModelCode +
                                   "' where VehicleId=" + autoVehicle.VehicleId;
                    objDbHelper.ExecuteNonQuery(quary);
                    vehicleId = autoVehicle.VehicleId;
                }
                string quaryNew = "Update auto_menufacturer set ManufacturerCode='" +
                                   autoVehicle.ManufacturerCode + "', ManufacturCountry='" +
                                   autoVehicle.ManufacturCountry +
                                   "' where ManufacturerId=" + autoVehicle.ManufacturerId;
                objDbHelper.ExecuteNonQuery(quaryNew);
                msg = "Success";
            }
            catch (Exception exception)
            {
                msg = exception.Message;
                objDbHelper.RollBack();
            }
            
            return vehicleId;
        }

        #endregion

        #region TanorAndLTV
        public List<AutoBrandTenorLTV> GetTenorList(int pageNo, int pageSize, string search)
        {
            var condition = "";
            if (search != "")
            {
                condition = "where auto_menufacturer.ManufacturerName like '%" + search + "%' or auto_vehicle.Model like '%" + search + "%' or auto_menufacturer.ManufacturCountry like '%" + search + "%'";
            }
            var objtanor = new List<AutoBrandTenorLTV>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  auto_menufacturer.ManufacturerName asc";
                string sql = string.Format(@"

select auto_brandtenorltv.TanorLtvID,auto_brandtenorltv.ManufacturerId,auto_brandtenorltv.VehicleId,
auto_brandtenorltv.StatusId,auto_brandtenorltv.Tenor,auto_brandtenorltv.LTV,
auto_brandtenorltv.UserId,auto_brandtenorltv.LastUpdateDate,auto_brandtenorltv.ValuePackAllowed,auto_menufacturer.ManufacturerName,
auto_menufacturer.ManufacturCountry,auto_vehicle.Model,auto_status.StatusName,auto_vehicle.TrimLevel,auto_vehicle.Type as VehicleType,
(select count(auto_brandtenorltv.TanorLtvID) from auto_brandtenorltv {2} )as Totalcount
from auto_brandtenorltv
inner join auto_menufacturer on auto_brandtenorltv.ManufacturerId = auto_menufacturer.ManufacturerId
inner join auto_vehicle on auto_brandtenorltv.VehicleId = auto_vehicle.VehicleId
inner join auto_status on auto_brandtenorltv.StatusId =auto_status.StatusId


 
{2}
{3}
LIMIT {0}, {1} 
", pageNumber, pageSize, condition, orderBy);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoBrandTenorLTVDataReader = new AutoBrandTenorLTVDataReader(reader);
                while (reader.Read())
                    objtanor.Add(autoBrandTenorLTVDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objtanor;
        }


        public List<AutoMenufacturer> GetAutoManufacturer()
        {

            var objmanufac = new List<AutoMenufacturer>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  ManufacturerName asc";
                string sql = string.Format(@"Select * from Auto_Menufacturer {0}", orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var AutoMenufacturerDataReader = new AutoMenufacturerDataReader(reader);
                while (reader.Read())
                    objmanufac.Add(AutoMenufacturerDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmanufac;
        }

        public List<AutoVehicle> GetAutoModel(int ManufacturerID)
        {

            var objmodel = new List<AutoVehicle>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string orderBy = "ORDER BY Model asc";
                string sql = string.Format(@"SELECT
auto_vehicle.VehicleId,
auto_vehicle.ManufacturerId as ManufacturerId,
auto_vehicle.Model,
auto_vehicle.ModelCode,
auto_vehicle.TrimLevel,
auto_vehicle.Type,
auto_vehicle.CC,
auto_vehicle.ChassisCode,
auto_vehicle.VehicleTypeCode,
IFNULL(auto_brandtenorltv.Tenor, 0) as Tenor,
IFNULL(auto_brandtenorltv.LTV, 0) as LTV
FROM
auto_vehicle
LEFT OUTER JOIN auto_brandtenorltv ON auto_brandtenorltv.VehicleId = auto_vehicle.VehicleId where auto_vehicle.ManufacturerId =" + ManufacturerID + " " + orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleDataReader = new AutoVehicleDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoVehicleDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public string SaveAuto_TenorAndLTV(AutoBrandTenorLTV autoBrandTenorLTV)
        {
            var lastupdate =
                    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();


            var dbHelper = new CommonDbHelper();
            try
            {
                string sql = @"INSERT INTO Auto_BrandTenorLTV
	(ManufacturerId,VehicleId,StatusId,Tenor,LTV,UserId,LastUpdateDate,ValuePackAllowed)

	 VALUES ('" + autoBrandTenorLTV.ManufacturerId + "'," + autoBrandTenorLTV.VehicleId + "," + autoBrandTenorLTV.StatusId + "," + autoBrandTenorLTV.Tenor + "," + autoBrandTenorLTV.LTV + "," + autoBrandTenorLTV.UserId + ",'" + lastupdate + "'," + autoBrandTenorLTV.ValuePackAllowed + ")";

                if (autoBrandTenorLTV.TanorLtvID != 0)
                {
                    sql = @"UPDATE Auto_BrandTenorLTV SET
		ManufacturerId = '" + autoBrandTenorLTV.ManufacturerId + "',VehicleId = " + autoBrandTenorLTV.VehicleId + ",StatusId=" + autoBrandTenorLTV.StatusId + ",Tenor=" + autoBrandTenorLTV.Tenor + ",LTV=" + autoBrandTenorLTV.LTV + ",UserId=" + autoBrandTenorLTV.UserId + ",LastUpdateDate='" + lastupdate + "', ValuePackAllowed=" + autoBrandTenorLTV.ValuePackAllowed + " WHERE TanorLtvID =" + autoBrandTenorLTV.TanorLtvID;
                }
                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }

        public string SaveAuto_TenorAndLTV(AutoBrandTenorLTV autoBrandTenorLTV, CommonDbHelper dbHelper)
        {
            var message = "";
            var lastupdate =
                    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                string sql = @"INSERT INTO Auto_BrandTenorLTV
	(ManufacturerId,VehicleId,StatusId,Tenor,LTV,UserId,LastUpdateDate,ValuePackAllowed)

	 VALUES ('" + autoBrandTenorLTV.ManufacturerId + "'," + autoBrandTenorLTV.VehicleId + "," + autoBrandTenorLTV.StatusId + "," + autoBrandTenorLTV.Tenor + "," + autoBrandTenorLTV.LTV + "," + autoBrandTenorLTV.UserId + ",'" + lastupdate + "'," + autoBrandTenorLTV.ValuePackAllowed + ")";

                if (autoBrandTenorLTV.TanorLtvID != 0)
                {
                    sql = @"UPDATE Auto_BrandTenorLTV SET
		ManufacturerId = '" + autoBrandTenorLTV.ManufacturerId + "',VehicleId = " + autoBrandTenorLTV.VehicleId + ",StatusId=" + autoBrandTenorLTV.StatusId + ",Tenor=" + autoBrandTenorLTV.Tenor + ",LTV=" + autoBrandTenorLTV.LTV + ",UserId=" + autoBrandTenorLTV.UserId + ",LastUpdateDate='" + lastupdate + "', ValuePackAllowed=" + autoBrandTenorLTV.ValuePackAllowed + " WHERE TanorLtvID =" + autoBrandTenorLTV.TanorLtvID;
                }
                dbHelper.ExecuteNonQuery(sql);

                message = "Success";
            }
            catch (Exception ex)
            {
                message = "Failed";
            }
            return message;
        }

        public AutoBrandTenorLTV getvaluepackallowed(int manufacturerID, int vehicleId, int vehicleStatus)
        {
            var dbHelper = new CommonDbHelper();
            var objAutoBrandTenorLTV = new AutoBrandTenorLTV();
            objAutoBrandTenorLTV = null;
            try
            {
                string sql = string.Format(@"Select * from auto_brandtenorltv where ManufacturerId = {0} and VehicleId = {1} and StatusId={2}", manufacturerID, vehicleId, vehicleStatus);
                IDataReader reader = dbHelper.GetDataReader(sql);

                var autoBrandTenorLTVDataReader = new AutoBrandTenorLTVDataReader(reader);
                if (reader.Read())
                    objAutoBrandTenorLTV = autoBrandTenorLTVDataReader.Read();
                reader.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return objAutoBrandTenorLTV;
        }

        public string UploadTenorLtv(AutoBrandTenorLTV objAutoBrandTenorLTV, CommonDbHelper objDbHelper)
        {
            var message = "Success";
            int manufacturerId = 0;
            int vehicleId = 0;
            int tenorLtvId = 0;
            try
            {
                #region Get Manufacturar Id
                string quary = "select ManufacturerId from auto_menufacturer where ManufacturerName='" + objAutoBrandTenorLTV.ManufacturerName + "'";

                IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    manufacturerId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
                if (manufacturerId == 0)
                {
                    var objManufactur = new AutoMenufacturer();
                    objManufactur.ManufacturerId = manufacturerId;
                    objManufactur.ManufacturerName = objAutoBrandTenorLTV.ManufacturerName;
                    objManufactur.Remarks = "";
                    objManufactur.ManufacturerCode = "";
                    objManufactur.ManufacturCountry = "";
                    manufacturerId = SaveManufecture(objManufactur, objDbHelper);
                }
                
                #endregion
                objAutoBrandTenorLTV.ManufacturerId = manufacturerId;

                #region Get Vehicle Id
                quary =
                    "select VehicleId,ManufacturerId,Model,TrimLevel,Type,CC,ChassisCode from auto_vehicle where ManufacturerId=" +
                    manufacturerId + " and Model='" + objAutoBrandTenorLTV.Model + "' and TrimLevel='" +
                    objAutoBrandTenorLTV.TrimLevel + "' and CC=" + objAutoBrandTenorLTV.VehicleCC + " and ChassisCode='" +
                    objAutoBrandTenorLTV.ChassisCode + "'";
                tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    vehicleId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();

                if (vehicleId == 0)
                {
                    var objAutoVehicle = new AutoVehicle();
                    objAutoVehicle.VehicleId = vehicleId;
                    objAutoVehicle.ManufacturerId = manufacturerId;
                    objAutoVehicle.Model = objAutoBrandTenorLTV.Model;
                    objAutoVehicle.ModelCode = "";
                    objAutoVehicle.TrimLevel = objAutoBrandTenorLTV.TrimLevel;
                    objAutoVehicle.Type = objAutoBrandTenorLTV.VehicleType;
                    objAutoVehicle.CC = objAutoBrandTenorLTV.VehicleCC;
                    objAutoVehicle.ChassisCode = objAutoBrandTenorLTV.ChassisCode;
                    vehicleId = SaveVehicle(objAutoVehicle, objDbHelper);

                }
                #endregion
                objAutoBrandTenorLTV.VehicleId = vehicleId;

                #region Tenor LTV Settings

                quary =
                    "Select * from auto_brandtenorltv where ManufacturerId=" +
                    manufacturerId + " and VehicleId=" + vehicleId + " and StatusId=" +
                    objAutoBrandTenorLTV.StatusId;
                tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    tenorLtvId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();

                #endregion
                objAutoBrandTenorLTV.TanorLtvID = tenorLtvId;

                var res = SaveAuto_TenorAndLTV(objAutoBrandTenorLTV, objDbHelper);
                if (res == "Failed") {
                    message = "Failed";
                }
            }
            catch (Exception ex) {
                message = "Failed";
            }
            return message;
        }

        public string TanorLtvDeleteById(int tanorLtvID)
        {
            var message = "";
            var dbHelper = new CommonDbHelper();
            try {
                string quary = "Delete from auto_brandtenorltv where TanorLtvID = " + tanorLtvID;
                dbHelper.ExecuteNonQuery(quary);
                message = "Success";
            }
            catch(Exception ex) {
                message = "Failed";
                dbHelper.RollBack();
            }
            finally {
                dbHelper.Close();
            }
            return message;
        }


        #endregion

        #region VendorNegativeListing

        public List<AutoVendorNegativeListing> GetAutoVendorNegativeList(int pageNo, int pageSize, string search)
        {
            var condition = "";
            //if (search != "")
            //{
            //    condition = "where auto_menufacturer.ManufacturerName like '%" + search + "%' or auto_vehicle.Model like '%" + search + "%' or auto_menufacturer.ManufacturCountry like '%" + search + "%'";
            //}
            var objNegativVendor = new List<AutoVendorNegativeListing>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "order by auto_vendor.VendorName asc";
                string sql = string.Format(@"


select auto_negativevendor.NegativeVendorId,auto_negativevendor.AutoVendorId,auto_negativevendor.ListingDate,
auto_vendor.AutoVendorId,auto_vendor.VendorName,
(select count(auto_negativevendor.NegativeVendorId) from auto_negativevendor {2})as Totalcount

from auto_negativevendor
inner join auto_vendor on auto_negativevendor.AutoVendorId= auto_vendor.AutoVendorId 

{2}
{3}
LIMIT {0}, {1} 
", pageNumber, pageSize, condition, orderBy);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var AutoVendorNegativeListingDataReader = new VendorNegativeListingDataReader(reader);
                while (reader.Read())
                    objNegativVendor.Add(AutoVendorNegativeListingDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objNegativVendor;
        }





        public string VendorNegativeById(int vendornegativeID)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                string quary = string.Format("delete from auto_negativevendor where auto_negativevendor.NegativeVendorId ={0}", vendornegativeID);
                dbHelper.ExecuteNonQuery(quary);
            }
            catch
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }



        public List<AutoVendor> GetAutoVendor()
        {

            var objvendor = new List<AutoVendor>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  VendorName asc";
                string sql = string.Format(@"select AutoVendorId,VendorName from auto_vendor
where IsActive=1 and AutoVendorId not in (Select AutoVendorId from auto_negativevendor) {0}", orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVendorDataReader = new AutoVendorDataReader(reader);
                while (reader.Read())
                    objvendor.Add(autoVendorDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objvendor;
        }

        public string SaveAuto_NegativeVendor(int autoVendorId)
        {

            var mesg = "";
            var lastupdate =
                    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();


            var dbHelper = new CommonDbHelper();
            try
            {
                string sql = @"INSERT INTO auto_negativevendor
	(AutoVendorId,ListingDate)

	 VALUES (" + autoVendorId + ",'" + lastupdate + "')";


                dbHelper.ExecuteNonQuery(sql);
                mesg = "Success";

            }
            catch (Exception exc)
            {
                mesg = exc.Message;
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return mesg;
        }

        #endregion

        
        #region SegmentSettings

        public List<AutoSegmentSettings> GetSegmentSettingsSummary(int pageNo, int pageSize)
        {
            var objAutoSegmentSettingsList = new List<AutoSegmentSettings>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                string orderBy = "ORDER BY  SegmentID ";
                string sql = string.Format(@"
select 	SegmentID, SegmentCode, SegmentName, Description, Profession, MinLoanAmt, MaxLoanAmt, MinTanor, 
	MaxTanor, IrWithARTA,IrWithoutARTA, MinAgeL1, MinAgeL2, MaxAgeL1, MaxAgeL2, LTVL1, LTVL2, ExperienceL1, ExperienceL2, 
	AcRelationshipL1, AcRelationshipL2, CriteriaCode, AssetCode, MinDBRL1, MinDBRL2, MaxDBRL2, 
	DBRL3, FieldVisit, Verification, LandTelephone, GuranteeReference, PrimaryMinIncomeL1, IsActive,
	PrimaryMinIncomeL2, JointMinIncomeL1, JointMinIncomeL2, Comments, UserId, LastUpdateDate,t_pluss_profession.PROF_NAME,t_pluss_profession.PROF_ID,

(select count(SegmentID) from auto_segment) as TotalCount 
from auto_segment inner join t_pluss_profession on  auto_segment.Profession = t_pluss_profession.PROF_ID
 where IsActive = 1 
{2}
LIMIT {0}, {1} 
", pageNumber, pageSize, orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoSegmentSettingsDataReader = new AutoSegmentSettingsDataReader(reader);
                while (reader.Read())
                    objAutoSegmentSettingsList.Add(autoSegmentSettingsDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoSegmentSettingsList;
        }


        public string saveSegment(AutoSegmentSettings autoSegmentSettings, int segmentID)
        {
            var returnValue = "";
            var sgId = 0;
            var lastupdate =
    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            var dbHelper = new CommonDbHelper();
            try
            {
                //DateTime theDate = DateTime.Now;
                //string lastUpdateDate = theDate.to


                string sql = String.Format(@"insert into auto_segment(SegmentCode, SegmentName, Description, Profession,
MinLoanAmt, MaxLoanAmt, MinTanor, MaxTanor, IrWithARTA,IrWithoutARTA,
MinAgeL1, MinAgeL2, MaxAgeL1, MaxAgeL2, LTVL1, LTVL2, ExperienceL1,
ExperienceL2, AcRelationshipL1, AcRelationshipL2, CriteriaCode,
AssetCode, MinDBRL1, MinDBRL2, MaxDBRL2, DBRL3, FieldVisit, Verification,
LandTelephone, GuranteeReference, PrimaryMinIncomeL1, PrimaryMinIncomeL2,
JointMinIncomeL1, JointMinIncomeL2, Comments,UserId,LastUpdateDate)
values('{0}', '{1}', '{2}', {3}, 
{4}, {5},{6},{7},{8},
{9},{10},{11},{12},{13},{14},{15},
{16},{17},{18},'{19}',
'{20}',{21},{22},{23},{24},{25},{26},
{27},{28},{29},{30},
{31},{32},'{33}','{34}',{35},'{36}')",
autoSegmentSettings.SegmentCode, autoSegmentSettings.SegmentName, autoSegmentSettings.Description, autoSegmentSettings.Profession,
autoSegmentSettings.MinLoanAmt, autoSegmentSettings.MaxLoanAmt, autoSegmentSettings.MinTanor, autoSegmentSettings.MaxTanor, autoSegmentSettings.IrWithARTA, autoSegmentSettings.IrWithoutARTA,
autoSegmentSettings.MinAgeL1, autoSegmentSettings.MinAgeL2, autoSegmentSettings.MaxAgeL1, autoSegmentSettings.MaxAgeL2, autoSegmentSettings.LTVL1, autoSegmentSettings.LTVL2, autoSegmentSettings.ExperienceL1,
autoSegmentSettings.ExperienceL2, autoSegmentSettings.AcRelationshipL1, autoSegmentSettings.AcRelationshipL2, autoSegmentSettings.CriteriaCode,
autoSegmentSettings.AssetCode, autoSegmentSettings.MinDBRL1, autoSegmentSettings.MinDBRL2, autoSegmentSettings.MaxDBRL2, autoSegmentSettings.DBRL3, autoSegmentSettings.FieldVisit, autoSegmentSettings.Verification,
autoSegmentSettings.LandTelephone, autoSegmentSettings.GuranteeReference, autoSegmentSettings.PrimaryMinIncomeL1, autoSegmentSettings.PrimaryMinIncomeL2,
autoSegmentSettings.JointMinIncomeL1, autoSegmentSettings.JointMinIncomeL2, autoSegmentSettings.Comments, autoSegmentSettings.UserId, lastupdate);

                if (segmentID != 0)
                {
                    sql = String.Format(@"UPDATE auto_segment SET
                                            SegmentCode = '{0}',
                                            SegmentName = '{1}',
                                            Description = '{2}',
                                            Profession = {3},
                                            MinLoanAmt = {4},
                                            MaxLoanAmt = {5},
                                            MinTanor = {6},
                                            MaxTanor = {7},
                                            IrWithARTA = {8},
                                            MinAgeL1 = {9},
                                            MinAgeL2 = {10},
                                            MaxAgeL1 = {11},
                                            MaxAgeL2 = {12},
                                            LTVL1 = {13},
                                            LTVL2 = {14},
                                            ExperienceL1 = {15},
                                            ExperienceL2 = {16},
                                            AcRelationshipL1 = {17},
                                            AcRelationshipL2 = {18},
                                            CriteriaCode = '{19}',
                                            AssetCode = '{20}',
                                            MinDBRL1 = {21},
                                            MinDBRL2 = {22},
                                            MaxDBRL2 = {23},
                                            DBRL3 = {24},
                                            FieldVisit = {25},
                                            Verification = {26},
                                            LandTelephone = {27},
                                            GuranteeReference = {28},
                                            PrimaryMinIncomeL1 = {29},
                                            PrimaryMinIncomeL2 = {30},
                                            JointMinIncomeL1 = {31},
                                            JointMinIncomeL2 = {32},
                                            Comments = '{33}',
                                            UserId = {34},
                                            LastUpdateDate = '{35}',IrWithoutARTA = '{37}'  WHERE SegmentID = {36}",
                                            autoSegmentSettings.SegmentCode,
                                            autoSegmentSettings.SegmentName, autoSegmentSettings.Description, autoSegmentSettings.Profession,
                                            autoSegmentSettings.MinLoanAmt, autoSegmentSettings.MaxLoanAmt,
                                            autoSegmentSettings.MinTanor, autoSegmentSettings.MaxTanor, autoSegmentSettings.IrWithARTA,
                                            autoSegmentSettings.MinAgeL1, autoSegmentSettings.MinAgeL2, autoSegmentSettings.MaxAgeL1,
                                            autoSegmentSettings.MaxAgeL2,
                                            autoSegmentSettings.LTVL1, autoSegmentSettings.LTVL2, autoSegmentSettings.ExperienceL1,
                                            autoSegmentSettings.ExperienceL2, autoSegmentSettings.AcRelationshipL1,
                                            autoSegmentSettings.AcRelationshipL2, autoSegmentSettings.CriteriaCode,
                                            autoSegmentSettings.AssetCode, autoSegmentSettings.MinDBRL1, autoSegmentSettings.MinDBRL2,
                                            autoSegmentSettings.MaxDBRL2,
                                            autoSegmentSettings.DBRL3,

                                            autoSegmentSettings.FieldVisit, autoSegmentSettings.Verification,
                                            autoSegmentSettings.LandTelephone, autoSegmentSettings.GuranteeReference,

                                            autoSegmentSettings.PrimaryMinIncomeL1,
                                            autoSegmentSettings.PrimaryMinIncomeL2, autoSegmentSettings.JointMinIncomeL1,
                                            autoSegmentSettings.JointMinIncomeL2,
                                            autoSegmentSettings.Comments, autoSegmentSettings.UserId, lastupdate, segmentID, autoSegmentSettings.IrWithoutARTA);
                    var query = string.Format(@"select SegmentID from auto_segment 
where SegmentID != {0} and SegmentCode = '{1}' and IsActive = 1", segmentID, autoSegmentSettings.SegmentCode);

                    var tempReder = dbHelper.GetDataReader(query);
                    while (tempReder.Read())
                    {
                        sgId = tempReder.GetInt32(0);
                    }
                    tempReder.Close();
                }
                else {
                    var query = string.Format(@"select SegmentID from auto_segment 
where SegmentCode = '{1}' and IsActive = 1", segmentID, autoSegmentSettings.SegmentCode);

                    var tempReder = dbHelper.GetDataReader(query);
                    while (tempReder.Read())
                    {
                        sgId = tempReder.GetInt32(0);
                    }
                    tempReder.Close();
                }
                if (sgId == 0)
                {
                    dbHelper.ExecuteNonQuery(sql);
                    returnValue = "Success";
                }
                else {
                    returnValue = "Already exist";
                }
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                returnValue = "Failed";
                throw ex;
            }
            finally
            {
                dbHelper.Close();
            }
            return returnValue;
        }

        public string InactiveSegmentById(int segmentId, int userId)
        {
            var returnValue = "";
            var lastupdate =
    DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                          DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            var dbHelper = new CommonDbHelper();
            try {
                var quary = string.Format("Update auto_segment set IsActive = 0, UserId={0}, LastUpdateDate = '{1}' where SegmentID = {2}", userId, lastupdate, segmentId);
                dbHelper.ExecuteNonQuery(quary);
                returnValue = "Success";
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                returnValue = "Failed";
                throw ex;
            }
            finally
            {
                dbHelper.Close();
            }
            return returnValue;
        }


        #endregion


        #region Deviation Settings

        public List<DaviationSettingsEntity> GetDeviationSettingsSummary(int pageNo, int pageSize, string searchKey)
        {
            var condition = "where IsActive=1";
            var condition1 = "where IsActive=1";
            if (searchKey != "")
            {
                condition = "where IsActive=1 and (DeviationCode like '%" + searchKey + "%' or Description like '%" + searchKey + "%' or DeviationLevel like '%" + searchKey + "%')";
                condition1 = "where IsActive=1 and (DeviationCode like '%" + searchKey + "%' or Description like '%" + searchKey + "%')";
            }
            var objDaviationSettingsEntityList = new List<DaviationSettingsEntity>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                var pageNumber = pageNo;
                //string orderBy = "ORDER BY  VehicleStatus desc";
                string sql = string.Format(@"
select DeviationId, DeviationFor, DeviationCode, Description, UserId, LastUpdateDate, DeviationLevel,IsActive, TotalCount from (
Select DeviationId, DeviationFor, DeviationCode, Description, UserId, LastUpdateDate,IsActive,
case DeviationFor when 1 then 'Income Assessment' when 2 then 'LEVEL-2' when 3 then 'LEVEL-3' end as DeviationLevel,
(select count(DeviationId) from auto_deviation {3}) as TotalCount 
from auto_deviation) as tbl
{2}
LIMIT {0}, {1} 
", pageNumber, pageSize, condition, condition1);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var daviationSettingsDataReader = new DaviationSettingsDataReader(reader);
                while (reader.Read())
                    objDaviationSettingsEntityList.Add(daviationSettingsDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objDaviationSettingsEntityList;
        }

        public string SaveDeviationSettings(DaviationSettingsEntity AutoDeviationSettings)
        {
            var returnValue = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            var description = AutoDeviationSettings.Description.Replace("'", "''");
            var dbHelper = new CommonDbHelper();
            var dvid = 0;
            try
            {

                string sql = String.Format(@"INSERT INTO auto_deviation
	(DeviationFor,DeviationCode,Description,UserId,LastUpdateDate)
	 VALUES ({0},'{1}','{2}',{3},'{4}')", AutoDeviationSettings.DeviationFor,
                                        AutoDeviationSettings.DeviationCode, description, AutoDeviationSettings.UserId, lastupdate);
                if (AutoDeviationSettings.DeviationId > 0)
                {
                    sql = String.Format(@"UPDATE auto_deviation SET
		                        DeviationFor = {0},
                                DeviationCode = '{1}',
                                Description = '{2}',
                                UserId = {3},
                                LastUpdateDate = '{4}' where DeviationId = {5}",
                                         AutoDeviationSettings.DeviationFor,
                                        AutoDeviationSettings.DeviationCode, description,
                                        AutoDeviationSettings.UserId, lastupdate, AutoDeviationSettings.DeviationId);


                    var query = string.Format(@"select DeviationId from auto_deviation 
where DeviationId != {0} and DeviationCode = '{1}' and IsActive = 1", AutoDeviationSettings.DeviationId, AutoDeviationSettings.DeviationCode);

                    var tempReder = dbHelper.GetDataReader(query);
                    while (tempReder.Read())
                    {
                        dvid = tempReder.GetInt32(0);
                    }
                    tempReder.Close();

                }
                else {
                    var objDev = GetDeviationbyCode(AutoDeviationSettings.DeviationCode);
                    if (objDev != null) {
                        dvid = objDev.DeviationId;
                    }
                }
                if (dvid == 0)
                {
                    dbHelper.ExecuteNonQuery(sql);
                    returnValue = "Success";
                }
                else {
                    returnValue = "Already exist";
                }
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                returnValue = "Failed";
                throw ex;
            }
            finally
            {
                dbHelper.Close();
            }
            return returnValue;
        }

        public DaviationSettingsEntity GetDeviationbyCode(string deviationCode)
        {
            var dbHelper = new CommonDbHelper();
            var objDaviationSettingsEntity = new DaviationSettingsEntity();
            objDaviationSettingsEntity = null;
            try
            {
                string sql = string.Format(@"Select * from auto_deviation where DeviationCode = '{0}' and IsActive=1 ", deviationCode);
                IDataReader reader = dbHelper.GetDataReader(sql);

                var daviationSettingsDataReader = new DaviationSettingsDataReader(reader);
                if (reader.Read())
                    objDaviationSettingsEntity = daviationSettingsDataReader.Read();
                reader.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return objDaviationSettingsEntity;
        }

        public string InactiveSettingsById(int deviationId, int userId)
        {
            var message = "";
            var dbHelper = new CommonDbHelper();

            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try {
                var quary = string.Format("update auto_deviation set IsActive = 0, LastUpdateDate = '{0}', UserId = {1} where DeviationId={2}", lastupdate, userId, deviationId);
                dbHelper.ExecuteNonQuery(quary);
                message = "Success";
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                message = "Failed";
                throw ex;
            }
            finally
            {
                dbHelper.Close();
            }
            return message;
        }

        //        public string IRVehicleStatusDeleteById(int vehicleStatusIRId)
        //        {
        //            var dbHelper = new CommonDbHelper();
        //            try
        //            {
        //                string quary = string.Format("Delete from auto_vehiclestatusir where VehicleStatusIRId={0}", vehicleStatusIRId);
        //                dbHelper.ExecuteNonQuery(quary);
        //            }
        //            catch
        //            {
        //                dbHelper.RollBack();
        //            }
        //            finally
        //            {
        //                dbHelper.Close();
        //            }
        //            return "Success";
        //        }



        #endregion

        #region Insurence Calculation Settings

        public string SaveAutoInsurenceCalculationsettings(List<AutoOwnDamage> objOwnDamageList, AutoOthercharge objOtherCharge)
        {
            var dbHelper = new CommonDbHelper();
            try
            {
                dbHelper.BeginTransaction();

                var sql = "Delete from auto_owndamage";
                dbHelper.ExecuteNonQuery(sql);

                foreach (var autoOwnDamage in objOwnDamageList)
                {
                    sql = @"INSERT INTO auto_owndamage
	(CCFrom,CCTo,OwnDamageCharge,ActLiability)
	 VALUES ('" + autoOwnDamage.CCFrom + "','" + autoOwnDamage.CCTo + "','" + autoOwnDamage.OwnDamageCharge + "','" + autoOwnDamage.ActLiability + "')";
                    dbHelper.ExecuteNonQuery(sql);
                }
                if (objOtherCharge.OtherChargeId == 0)
                {
                    sql =
                        "INSERT INTO auto_othercharge (AccidentOrTheft, Commission, Vat, PassengerPerSeatingCap, AmountofDriverSeat, StampCharge, DriverSeatCharge) values ('" +
                        objOtherCharge.AccidentOrTheft + "','" + objOtherCharge.Commission + "','" + objOtherCharge.Vat +
                        "','" + objOtherCharge.PassengerPerSeatingCap + "','" + objOtherCharge.AmountofDriverSeat +
                        "','" +
                        objOtherCharge.StampCharge + "','" + objOtherCharge.DriverSeatCharge + "' )";
                    dbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql =
                        "Update auto_othercharge set AccidentOrTheft='" + objOtherCharge.AccidentOrTheft +
                        "', Commission='" + objOtherCharge.Commission + "', Vat='" + objOtherCharge.Vat +
                        "', PassengerPerSeatingCap='" + objOtherCharge.PassengerPerSeatingCap +
                        "', AmountofDriverSeat='" + objOtherCharge.AmountofDriverSeat + "', StampCharge='" +
                        objOtherCharge.StampCharge + "', DriverSeatCharge='" + objOtherCharge.DriverSeatCharge + "' where OtherChargeId=" + objOtherCharge.OtherChargeId;
                    dbHelper.ExecuteNonQuery(sql);
                }

                dbHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return "Success";
        }

        public List<AutoOwnDamage> GetOwnDamage()
        {
            var objAutoOwnDamageList = new List<AutoOwnDamage>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = string.Format(@"Select * from auto_owndamage");

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoOwnDamageDataReader = new AutoOwnDamageDataReader(reader);
                while (reader.Read())
                    objAutoOwnDamageList.Add(autoOwnDamageDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoOwnDamageList;
        }

        public AutoOthercharge GetOtherDamage()
        {
            var dbHelper = new CommonDbHelper();
            var objAutoOthercharge = new AutoOthercharge();
            try
            {
                string sql = string.Format(@"Select * from auto_othercharge");
                IDataReader reader = dbHelper.GetDataReader(sql);

                var autoOtherchargeDataReader = new AutoOtherchargeDataReader(reader);
                if (reader.Read())
                    objAutoOthercharge = autoOtherchargeDataReader.Read();
                reader.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return objAutoOthercharge;
        }

        #endregion


        #region Loan Application

        #region save Data

        public string SaveAutoLoanApplication(AutoLoanMaster objAutoLoanMaster, AutoPRApplicant objAutoPRApplicant,
            AutoJTApplicant objAutoJTApplicant, AutoPRProfession objAutoPRProfession, AutoJTProfession objAutoJTProfession,
            AutoLoanVehicle objAutoLoanVehicle, AutoPRAccount objAutoPRAccount, AutoJTAccount objAutoJTAccount,
            List<AutoPRAccountDetail> objAutoPRAccountDetailList, List<AutoJTAccountDetail> objAutoJtAccountDetailList,
            AutoPRFinance objAutoPRFinance, AutoJTFinance objAutoJTFinance, AutoLoanReference objAutoLoanReference,
            List<AutoLoanFacility> objAutoLoanFacilityList, List<AutoLoanSecurity> objAutoLoanSecurityList,
            AutoApplicationInsurance objAutoApplicationInsurance, AutoVendor objAutoVendor)//, AutoPRBankStatement objPRBankStatement, 
        //AutoJTBankStatement objJTBankStatement)
        {
            var mesg = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                objDbHelper.BeginTransaction();
                int autoLoanMasterId = SaveAutoLoanMaster(objAutoLoanMaster, objDbHelper);


                #region INSERT into history table
                // 

                AutoStatusHistory AutoStatusHistory = new AutoStatusHistory();

                AutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;
                AutoStatusHistory.UserID = objAutoLoanMaster.UserID;
                AutoStatusHistory.StatusID = objAutoLoanMaster.StatusID;
                //var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                //                DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
                SaveAutoStatusHistory(AutoStatusHistory);


                //end history table
                #endregion INSERT into history table


                #region insert into ExecutionByUserTable

                Auto_App_Step_ExecutionByUser ExecutionByUser = new Auto_App_Step_ExecutionByUser();

                ExecutionByUser.AppRaiseBy = objAutoLoanMaster.UserID;
                ExecutionByUser.Auto_LoanMasterId = autoLoanMasterId;

                SaveExecutionByUser(ExecutionByUser, objDbHelper);


                #endregion insert into ExecutionByUserTable





                if (autoLoanMasterId != 0)
                {
                    objAutoPRApplicant.Auto_LoanMasterId = autoLoanMasterId;
                    SaveAutoPRApplicant(objAutoPRApplicant, objDbHelper);
                    if (objAutoLoanMaster.JointApplication == true)
                    {
                        objAutoJTApplicant.Auto_LoanMasterId = autoLoanMasterId;
                        SaveAutoJTApplicant(objAutoJTApplicant, objDbHelper);
                    }

                    objAutoPRProfession.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoPRProfession(objAutoPRProfession, objDbHelper);
                    if (objAutoLoanMaster.JointApplication == true)
                    {
                        objAutoJTProfession.AutoLoanMasterId = autoLoanMasterId;
                        SaveAutoJTProfession(objAutoJTProfession, objDbHelper);
                    }

                    int vehicleId = 0;
                    if (objAutoLoanVehicle.VendorId == -1)
                    {
                        if (objAutoVendor.VendorName != "")
                        {
                            int newVendorID = SaveNewVendor(objAutoVendor, objDbHelper);

                            objAutoLoanVehicle.AutoLoanMasterId = autoLoanMasterId;
                            objAutoLoanVehicle.VendorId = newVendorID;

                            //if (objAutoLoanVehicle.VehicleId == -7) {
                            //    var objAutoVehicle = new AutoVehicle();
                            //    objAutoVehicle.VehicleId = 0;
                            //    objAutoVehicle.ManufacturerId = objAutoLoanVehicle.Auto_ManufactureId;
                            //    objAutoVehicle.Model = objAutoLoanVehicle.Model;
                            //    objAutoVehicle.ModelCode = "";
                            //    objAutoVehicle.TrimLevel = objAutoLoanVehicle.TrimLevel;
                            //    objAutoVehicle.Type = objAutoLoanVehicle.VehicleType;
                            //    objAutoVehicle.CC = Convert.ToInt32(objAutoLoanVehicle.EngineCC);
                            //    objAutoVehicle.ChassisCode = "";


                            //    vehicleId = SaveAutoVehicle(objAutoVehicle, objDbHelper);
                            //    objAutoLoanVehicle.VehicleId = vehicleId;
                            //}



                            SaveAutoLoanVehicle(objAutoLoanVehicle, objDbHelper);
                        }
                    }
                    else
                    {
                        objAutoLoanVehicle.AutoLoanMasterId = autoLoanMasterId;
                        //if (objAutoLoanVehicle.VehicleId == -7)
                        //{
                        //    var objAutoVehicle = new AutoVehicle();
                        //    objAutoVehicle.VehicleId = 0;
                        //    objAutoVehicle.ManufacturerId = objAutoLoanVehicle.Auto_ManufactureId;
                        //    objAutoVehicle.Model = objAutoLoanVehicle.Model;
                        //    objAutoVehicle.ModelCode = "";
                        //    objAutoVehicle.TrimLevel = objAutoLoanVehicle.TrimLevel;
                        //    objAutoVehicle.Type = objAutoLoanVehicle.VehicleType;
                        //    objAutoVehicle.CC = Convert.ToInt32(objAutoLoanVehicle.EngineCC);
                        //    objAutoVehicle.ChassisCode = "";


                        //    vehicleId = SaveAutoVehicle(objAutoVehicle, objDbHelper);
                        //    objAutoLoanVehicle.VehicleId = vehicleId;
                        //}

                        SaveAutoLoanVehicle(objAutoLoanVehicle, objDbHelper);
                    }

                    objAutoPRAccount.AutoLoanMasterId = autoLoanMasterId;
                    int prAccountId = SaveAutoPRAccount(objAutoPRAccount, objDbHelper);
                    if (objAutoLoanMaster.JointApplication == true)
                    {
                        objAutoJTAccount.AutoLoanMasterId = autoLoanMasterId;
                        int jt_AccountId = SaveAutoJTAccount(objAutoJTAccount, objDbHelper);
                    }


                    #region Inserting data for analyst table
                    //Inserting data for analyst table

                    //AutoApplicantSCBAccount autoApplicantSCBAccount = new AutoApplicantSCBAccount();
                    //autoApplicantSCBAccount.AccountNumberSCB = objAutoPRAccount.AccountNumberForSCB;
                    //autoApplicantSCBAccount.LLID = objAutoLoanMaster.LLID;
                    //autoApplicantSCBAccount.AutoLoanMasterId = objAutoLoanMaster.Auto_LoanMasterId;
                    //autoApplicantSCBAccount.AccountStatus = 0;
                    //autoApplicantSCBAccount.PriorityHolder = 0;

                    //SaveAutoApplicantSCBAccount(autoApplicantSCBAccount, objDbHelper);



                    #endregion


                    string PRAccountDetailDeleteQuery = "delete from auto_praccount_details where AutoLoanMasterId = " + autoLoanMasterId;
                    objDbHelper.ExecuteNonQuery(PRAccountDetailDeleteQuery);

                    string PRBankStatementDeleteQuery = "delete from auto_prbankstatement where AutoLoanMasterId = " + autoLoanMasterId;
                    objDbHelper.ExecuteNonQuery(PRBankStatementDeleteQuery);

                    foreach (var objAutoPRAccountDetailListMember in objAutoPRAccountDetailList)
                    {
                        objAutoPRAccountDetailListMember.AutoLoanMasterId = autoLoanMasterId;
                        SaveAutoPRAccountDetail(objAutoPRAccountDetailListMember, objDbHelper);

                        var objPrBankStatement = new AutoPRBankStatement();
                        objPrBankStatement.PrBankStatementId = 0;
                        objPrBankStatement.AccountCategory = objAutoPRAccountDetailListMember.AccountCategory;
                        objPrBankStatement.AccountName = objAutoPRAccountDetailListMember.AccountNumber;
                        objPrBankStatement.AccountNumber = objAutoPRAccountDetailListMember.AccountNumber;
                        objPrBankStatement.AccountType = objAutoPRAccountDetailListMember.AccountType;
                        objPrBankStatement.AutoLoanMasterId = autoLoanMasterId;
                        objPrBankStatement.LLID = objAutoLoanMaster.LLID;
                        objPrBankStatement.BankId = objAutoPRAccountDetailListMember.BankId;
                        objPrBankStatement.BranchId = objAutoPRAccountDetailListMember.BranchId;
                        objPrBankStatement.Statement = "";
                        SaveAutoPRBankStatement(objPrBankStatement, objDbHelper);


                    }
                    if (objAutoLoanMaster.JointApplication == true)
                    {
                        string JTAccountDetailDeleteQuery = "delete from auto_jtaccount_details where AutoLoanMasterId = " + autoLoanMasterId;
                        objDbHelper.ExecuteNonQuery(JTAccountDetailDeleteQuery);

                        string JtBankStatementDeleteQuery = "delete from auto_jtbankstatement where AutoLoanMasterId = " + autoLoanMasterId;
                        objDbHelper.ExecuteNonQuery(JtBankStatementDeleteQuery);

                        foreach (var objAutoJtAccountDetailListMember in objAutoJtAccountDetailList)
                        {
                            objAutoJtAccountDetailListMember.AutoLoanMasterId = autoLoanMasterId;
                            SaveAutoJtAccountDetail(objAutoJtAccountDetailListMember, objDbHelper);

                            var objJtBankStatement = new AutoJTBankStatement();
                            objJtBankStatement.JtBankStatementId = 0;
                            objJtBankStatement.AccountCategory = objAutoJtAccountDetailListMember.AccountCategory;
                            objJtBankStatement.AccountName = objAutoJtAccountDetailListMember.AccountNumber;
                            objJtBankStatement.AccountNumber = objAutoJtAccountDetailListMember.AccountNumber;
                            objJtBankStatement.AccountType = objAutoJtAccountDetailListMember.AccountType;
                            objJtBankStatement.AutoLoanMasterId = autoLoanMasterId;
                            objJtBankStatement.LLID = objAutoLoanMaster.LLID;
                            objJtBankStatement.BankId = objAutoJtAccountDetailListMember.BankId;
                            objJtBankStatement.BranchId = objAutoJtAccountDetailListMember.BranchId;
                            objJtBankStatement.Statement = "";
                            SaveAutoJTBankStatement(objJtBankStatement, objDbHelper);
                        }
                    }

                    objAutoPRFinance.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoPRFinance(objAutoPRFinance, objDbHelper);
                    if (objAutoLoanMaster.JointApplication == true)
                    {
                        objAutoJTFinance.AutoLoanMasterId = autoLoanMasterId;
                        SaveAutoJTFinance(objAutoJTFinance, objDbHelper);
                    }

                    objAutoLoanReference.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLoanReference(objAutoLoanReference, objDbHelper);

                    string FacilityDeleteQuery = "Delete from auto_loanfacility where AutoLoanMasterId = " + autoLoanMasterId;
                    objDbHelper.ExecuteNonQuery(FacilityDeleteQuery);

                    string securityDeleteQuery = String.Format(@"delete from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterId);
                    objDbHelper.ExecuteNonQuery(securityDeleteQuery);

                    //Code comment by Washik 17/11/2012

                    //foreach (var objAutoLoanFacilityListMember in objAutoLoanFacilityList)
                    //{
                    //    objAutoLoanFacilityListMember.AutoLoanMasterId = autoLoanMasterId;
                    //    int facilityId = SaveAutoLoanFacilityWithReturn(objAutoLoanFacilityListMember, objDbHelper);
                        //SaveAutoLoanFacility(objAutoLoanFacilityListMember, objDbHelper);

                    //}

                    //string securityDeleteQuery = String.Format(@"delete from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterId);
                    //objDbHelper.ExecuteNonQuery(securityDeleteQuery);

                    //foreach (var objAutoLoanSecurityMember in objAutoLoanSecurityList)
                    //{
                    //    objAutoLoanSecurityMember.AutoLoanMasterId = autoLoanMasterId;
                    //    SaveAutoLoanSecurity(objAutoLoanSecurityMember, objDbHelper);
                    //}

                    //New Code Start by Washik 17/11/2012

                    for (var i = 0; i < objAutoLoanFacilityList.Count; i++) {
                        objAutoLoanFacilityList[i].AutoLoanMasterId = autoLoanMasterId;
                        int facilityId = SaveAutoLoanFacilityWithReturn(objAutoLoanFacilityList[i], objDbHelper);
                        
                        //Auto Loan Sequrity
                        objAutoLoanSecurityList[i].AutoLoanMasterId = autoLoanMasterId;
                        objAutoLoanSecurityList[i].FacilityId = facilityId;
                        SaveAutoLoanSecurity(objAutoLoanSecurityList[i], objDbHelper);
                    }


                    string insurenceDeleteQuery = String.Format(@"delete from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterId);
                    objDbHelper.ExecuteNonQuery(insurenceDeleteQuery);

                    objAutoApplicationInsurance.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoApplicationInsurance(objAutoApplicationInsurance, objDbHelper);



                    #region save information for DeDup

                    string PREVI_LOAN_SCB_TYPE = "";
                    string PREVI_LOAN_SCB_AMT = "null";
                    string CR_CARD_NO_SCB = "";
                    string CR_CARD_LIMIT = "null";

                    foreach (AutoLoanFacility objAutoLoanFacility in objAutoLoanFacilityList)
                    {
                        if (objAutoLoanFacility.FacilityType > 0)
                        {
                            if (objAutoLoanFacility.FacilityType == 22 ||
                                objAutoLoanFacility.FacilityType == 18)
                            {
                                //CR_CARD_NO_SCB = objAutoLoanFacility.FacilityType;
                                CR_CARD_NO_SCB = objAutoPRAccount.CreditCardNumberForSCB;
                                CR_CARD_LIMIT = objAutoLoanFacility.PresentLimit.ToString();
                            }
                            else
                            {
                                PREVI_LOAN_SCB_TYPE = objAutoLoanFacility.FacilityTypeName;
                                PREVI_LOAN_SCB_AMT = objAutoLoanFacility.PresentLimit.ToString();
                            }
                        }
                    }


                    string OTHER_ACC_BANK_ID_1 = "null";
                    string OTHER_ACC_TYPE_1 = "null";
                    string OTHER_ACC_BR_ID_1 = "null";
                    string OTHER_ACC_NO_1 = "null";

                    string OTHER_ACC_BANK_ID_2 = "null";
                    string OTHER_ACC_TYPE_2 = "null";
                    string OTHER_ACC_BR_ID_2 = "null";
                    string OTHER_ACC_NO_2 = "null";

                    string OTHER_ACC_BANK_ID_3 = "null";
                    string OTHER_ACC_TYPE_3 = "null";
                    string OTHER_ACC_BR_ID_3 = "null";
                    string OTHER_ACC_NO_3 = "null";

                    string OTHER_ACC_BANK_ID_4 = "null";
                    string OTHER_ACC_TYPE_4 = "null";
                    string OTHER_ACC_BR_ID_4 = "null";
                    string OTHER_ACC_NO_4 = "null";

                    string OTHER_ACC_BANK_ID_5 = "null";
                    string OTHER_ACC_TYPE_5 = "null";
                    string OTHER_ACC_BR_ID_5 = "null";
                    string OTHER_ACC_NO_5 = "null";

                    for (int i = 0; i < objAutoPRAccountDetailList.Count; i++)
                    {
                        //AutoPRAccountDetail objAutoPRAccountDetail = objAutoPRAccountDetailList[i];

                        switch (i)
                        {
                            case 0:
                                {
                                    OTHER_ACC_BANK_ID_1 = Convert.ToString(objAutoPRAccountDetailList[i].BankId);
                                    OTHER_ACC_TYPE_1 = Convert.ToString(objAutoPRAccountDetailList[i].AccountType);
                                    OTHER_ACC_BR_ID_1 = Convert.ToString(objAutoPRAccountDetailList[i].BranchId);
                                    OTHER_ACC_NO_1 = Convert.ToString(objAutoPRAccountDetailList[i].AccountNumber);
                                    break;
                                }
                            case 1:
                                {
                                    OTHER_ACC_BANK_ID_2 = Convert.ToString(objAutoPRAccountDetailList[i].BankId);
                                    OTHER_ACC_TYPE_2 = Convert.ToString(objAutoPRAccountDetailList[i].AccountType);
                                    OTHER_ACC_BR_ID_2 = Convert.ToString(objAutoPRAccountDetailList[i].BranchId);
                                    OTHER_ACC_NO_2 = Convert.ToString(objAutoPRAccountDetailList[i].AccountNumber);
                                    break;
                                }
                            case 2:
                                {
                                    OTHER_ACC_BANK_ID_3 = Convert.ToString(objAutoPRAccountDetailList[i].BankId);
                                    OTHER_ACC_TYPE_3 = Convert.ToString(objAutoPRAccountDetailList[i].AccountType);
                                    OTHER_ACC_BR_ID_3 = Convert.ToString(objAutoPRAccountDetailList[i].BranchId);
                                    OTHER_ACC_NO_3 = Convert.ToString(objAutoPRAccountDetailList[i].AccountNumber);
                                    break;
                                }
                            case 3:
                                {
                                    OTHER_ACC_BANK_ID_4 = Convert.ToString(objAutoPRAccountDetailList[i].BankId);
                                    OTHER_ACC_TYPE_4 = Convert.ToString(objAutoPRAccountDetailList[i].AccountType);
                                    OTHER_ACC_BR_ID_4 = Convert.ToString(objAutoPRAccountDetailList[i].BranchId);
                                    OTHER_ACC_NO_4 = Convert.ToString(objAutoPRAccountDetailList[i].AccountNumber);
                                    break;
                                }
                            case 4:
                                {
                                    OTHER_ACC_BANK_ID_5 = Convert.ToString(objAutoPRAccountDetailList[i].BankId);
                                    OTHER_ACC_TYPE_5 = Convert.ToString(objAutoPRAccountDetailList[i].AccountType);
                                    OTHER_ACC_BR_ID_5 = Convert.ToString(objAutoPRAccountDetailList[i].BranchId);
                                    OTHER_ACC_NO_5 = Convert.ToString(objAutoPRAccountDetailList[i].AccountNumber);
                                    break;
                                }
                        }
                    }

                    var datetimeNow = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                                      DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

                    string appliedDate = objAutoLoanMaster.AppliedDate.ToString("yyyy-MM-dd") + " " +
                                         objAutoLoanMaster.AppliedDate.Hour.ToString() + ":" +
                                         objAutoLoanMaster.AppliedDate.Minute.ToString() + ":" +
                                         objAutoLoanMaster.AppliedDate.Second.ToString();

                    string receiveDate = objAutoLoanMaster.ReceiveDate.ToString("yyyy-MM-dd") + " " +
                                         objAutoLoanMaster.ReceiveDate.Hour.ToString() + ":" +
                                         objAutoLoanMaster.ReceiveDate.Minute.ToString() + ":" +
                                         objAutoLoanMaster.ReceiveDate.Second.ToString();

                    string PrDateofBirth = objAutoPRApplicant.DateofBirth.ToString("yyyy-MM-dd") + " " +
                                           objAutoPRApplicant.DateofBirth.Hour.ToString() + ":" +
                                           objAutoPRApplicant.DateofBirth.Minute.ToString() + ":" +
                                           objAutoPRApplicant.DateofBirth.Second.ToString();

                    string JtDateofBirth = objAutoJTApplicant.DateofBirth.ToString("yyyy-MM-dd") + " " +
                                           objAutoJTApplicant.DateofBirth.Hour.ToString() + ":" +
                                           objAutoJTApplicant.DateofBirth.Minute.ToString() + ":" +
                                           objAutoJTApplicant.DateofBirth.Second.ToString();


                    string AppliedAmount = objAutoLoanMaster.AppliedAmount.ToString() == ""
                                               ? "null"
                                               : objAutoLoanMaster.AppliedAmount.ToString();

                    string DPIAmount = objAutoPRFinance.DPIAmount.ToString() == ""
                                           ? "null"
                                           : objAutoPRFinance.DPIAmount.ToString();

                    string AskingTenor = objAutoLoanMaster.AskingTenor.ToString() == ""
                                             ? "null"
                                             : objAutoLoanMaster.AskingTenor.ToString();

                    string NumberofDependents = objAutoPRApplicant.NumberofDependents.ToString() == ""
                                                    ? "null"
                                                    : objAutoPRApplicant.NumberofDependents.ToString();
                    string MonthinCurrentProfession = objAutoPRProfession.MonthinCurrentProfession.ToString() == ""
                                                          ? "null"
                                                          : objAutoPRProfession.MonthinCurrentProfession.ToString();
                    string TotalProfessionalExperience = objAutoPRProfession.TotalProfessionalExperience.ToString() == ""
                                                             ? "null"
                                                             : objAutoPRProfession.TotalProfessionalExperience.ToString();

                    string NumberOfEmployees = objAutoPRProfession.NumberOfEmployees.ToString() == ""
                                                   ? "null"
                                                   : objAutoPRProfession.NumberOfEmployees.ToString();

                    string EquityShare = objAutoPRProfession.EquityShare.ToString() == ""
                                             ? "null"
                                             : objAutoPRProfession.EquityShare.ToString();

                    string DOIAmount = objAutoPRFinance.DOIAmount.ToString() == ""
                                           ? "null"
                                           : objAutoPRFinance.DOIAmount.ToString();

                    string EDRentAndUtilities = objAutoPRFinance.EDRentAndUtilities.ToString() == ""
                                                    ? "null"
                                                    : objAutoPRFinance.EDRentAndUtilities.ToString();

                    string EDFoodAndClothing = objAutoPRFinance.EDFoodAndClothing.ToString() == ""
                                                   ? "null"
                                                   : objAutoPRFinance.EDFoodAndClothing.ToString();

                    string EDEducation = objAutoPRFinance.EDEducation.ToString() == ""
                                             ? "null"
                                             : objAutoPRFinance.EDEducation.ToString();
                    string EDLoanRePayment = objAutoPRFinance.EDLoanRePayment.ToString() == ""
                                                 ? "null"
                                                 : objAutoPRFinance.EDLoanRePayment.ToString();
                    string EDOther = objAutoPRFinance.EDOther.ToString() == ""
                                         ? "null"
                                         : objAutoPRFinance.EDOther.ToString();

                    string Auto_ManufactureId = objAutoLoanVehicle.Auto_ManufactureId.ToString() == ""
                                                    ? "null"
                                                    : objAutoLoanVehicle.Auto_ManufactureId.ToString();
                    string VendorId = objAutoLoanVehicle.VendorId.ToString() == ""
                                          ? "null"
                                          : objAutoLoanVehicle.VendorId.ToString();


                    string userID = "2";

                    string CheckSql = "select count(*) from loan_app_info where LOAN_APPLI_ID = " + objAutoLoanMaster.LLID;

                    IDataReader reader = objDbHelper.GetDataReader(CheckSql);

                    int check = 0;

                    while (reader.Read())
                    {
                        check = reader.GetInt32(0);
                    }
                    reader.Close();



                    if (check > 0)
                    {
                        string sql = String.Format(@"update loan_app_info set
LOAN_APPLI_ID={0},
LOAN_STATU_DT='{1}',
LOAN_STATU_ID={2},
BR_ID={3},
LOAP_SOURCE_PERSON_NAME='{4}',
APPLI_DT='{5}',
RCV_DT='{6}',
PROD_ID={7},
CUST_NM='{8}',
LOAN_APP_AMT={9},
LOAP_DECLARED_INCOME={10},
TENOR={11},
TIN_NO='{12}',
PREVI_LOAN_SCB_TYPE='{13}',
PREVI_LOAN_SCB_AMT={14},
CR_CARD_NO_SCB='{15}',
CR_CARD_LIMIT={16},
FATHE_NM='{17}',
MOTHE_NM='{18}',
DOB='{19}',
MARIT_STATU='{20}',
NO_DEPEN={21},
ID_TYPE='{22}',
ID_NO='{23}',
HIGH_EDU_LEVEL='{24}',
RES_ADD='{25}',
OFF_PH='{26}',
RES_PH='{27}',
MOBIL='{28}',
E_MAIL='{29}',
YR_CURRE_ADD ='{30}',
MAILI_ADD='{31}',
SPOUS_NM='{32}',
SPOUS_PROFE='{33}',
SPOUS_WORK_ADD='{34}',
SPOUS_MOB='{35}',
BUSIN_NATUR='{36}',
COMPA_NM='{37}',
OFF_ADD='{38}',
DESIG='{39}',
MON_CURRE_JOB={40},
EMPLO_STATU='{41}',
TOT_YR={42},
NO_STAFF={43},
TYPE_OWNER='{44}',
EQUIT_SHARE={45},
OTHER_ANUAL_INCOM={46},
OTHER_INCOM_SOURC='{47}',
RENT_UTILI={48},
FOOD_CLOTH={49},
EDUCA={50},
LOAN_REPAY={51},
OTHER_EXPEN={52},
OTHER_ACC_BANK_ID_1={53},
OTHER_ACC_TYPE_1='{54}',
OTHER_ACC_BR_ID_1={55},
OTHER_ACC_NO_1='{56}',
OTHER_ACC_BANK_ID_2={57},
OTHER_ACC_TYPE_2='{58}',
OTHER_ACC_BR_ID_2={59},
OTHER_ACC_NO_2='{60}',
OTHER_ACC_BANK_ID_3={61},
OTHER_ACC_TYPE_3='{62}',
OTHER_ACC_BR_ID_3={63},
OTHER_ACC_NO_3='{64}',
CAR_BRAND_ID={65},
VENDO_ID={66},
MFY='{67}',
CAR_CC='{68}',
J_APP_NM='{69}',
J_APP_FATHE_NM='{70}',
J_APP_MOTHE_NM='{71}',
J_APP_DOB='{72}',
J_APP_PROFE='{73}',
J_APP_EDUCA_LEVEL='{74}',
J_TIN='{75}',
J_ID_TYPE1='{76}',
J_ID_NO1='{77}',
REF_NM_1='{78}',
REF_REL_1='{79}',
REF_RES_ADD_1='{80}',
REF_WRK_ADD_1='{81}',
REF_RES_PH_1='{82}',
REF_MOB_1='{83}',
REF_NM_2='{84}',
REF_REL_2='{85}',
REF_RES_ADD_2='{86}',
REF_WRK_ADD_2='{87}',
REF_RES_PH_2='{88}',
REF_MOB_2='{89}',
LOAP_OTHERACWITHBANK_ID4={90},
LOAP_OTHERACWITHBANK_ID5={91},
LOAP_ORHREACTYPE_ID4='{92}',
LOAP_ORHREACTYPE_ID5='{93}',
LOAP_OTHERACWITHBRANCH_ID4={94},
LOAP_OTHERACWITHBRANCH_ID5={95},
LOAP_OTHERACWITHACNO4='{96}',
LOAP_OTHERACWITHACNO5='{97}',
LOAP_USERID = {98} where LOAN_APPLI_ID = {99}", objAutoLoanMaster.LLID, datetimeNow, 4, objAutoLoanMaster.Source,
                                objAutoLoanMaster.SourceName, appliedDate, receiveDate,
                                14, objAutoPRApplicant.PrName, AppliedAmount, DPIAmount, AskingTenor,
                                objAutoPRApplicant.TINNumber,
                                PREVI_LOAN_SCB_TYPE, PREVI_LOAN_SCB_AMT, CR_CARD_NO_SCB, CR_CARD_LIMIT,
                                objAutoPRApplicant.FathersName, objAutoPRApplicant.MothersName, PrDateofBirth,
                                objAutoPRApplicant.MaritalStatus, NumberofDependents,
                                objAutoPRApplicant.IdentificationNumberType, objAutoPRApplicant.IdentificationNumber,
                                objAutoPRApplicant.HighestEducation, objAutoPRApplicant.ResidenceAddress,
                                objAutoPRProfession.OfficePhone, objAutoPRApplicant.PhoneResidence,
                                objAutoPRApplicant.Mobile,
                                objAutoPRApplicant.Email, objAutoPRApplicant.ResidenceStatusYear,
                                objAutoPRApplicant.MailingAddress, objAutoPRApplicant.SpouseName,
                                objAutoPRApplicant.SpouseProfession,
                                objAutoPRApplicant.SpouseWorkAddress, objAutoPRApplicant.SpouseContactNumber,
                                objAutoPRProfession.NatureOfBussiness, objAutoPRProfession.NameofCompany,
                                objAutoPRProfession.Address, objAutoPRProfession.Designation,
                                MonthinCurrentProfession, objAutoPRProfession.EmploymentStatus,
                                TotalProfessionalExperience, NumberOfEmployees,
                                objAutoPRProfession.OwnerShipType, EquityShare,
                                DOIAmount, objAutoPRFinance.DOISource, EDRentAndUtilities, EDFoodAndClothing,
                                EDEducation, EDLoanRePayment,
                                EDOther, OTHER_ACC_BANK_ID_1, OTHER_ACC_TYPE_1, OTHER_ACC_BR_ID_1, OTHER_ACC_NO_1,
                                OTHER_ACC_BANK_ID_2, OTHER_ACC_TYPE_2, OTHER_ACC_BR_ID_2, OTHER_ACC_NO_2,
                                OTHER_ACC_BANK_ID_3, OTHER_ACC_TYPE_3, OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3,
                                Auto_ManufactureId, VendorId,
                                objAutoLoanVehicle.ManufacturingYear, objAutoLoanVehicle.EngineCC,
                                objAutoJTApplicant.JtName, objAutoJTApplicant.FathersName,
                                objAutoJTApplicant.MothersName, JtDateofBirth, objAutoJTProfession.PrimaryProfession,
                                objAutoJTApplicant.HighestEducation,
                                objAutoJTApplicant.TINNumber, objAutoJTApplicant.IdentificationNumberType,
                                objAutoJTApplicant.IdentificationNumber, objAutoLoanReference.ReferenceName1,
                                objAutoLoanReference.Relationship1, objAutoLoanReference.ResidenceAddress1,
                                objAutoLoanReference.WorkAddress1, objAutoLoanReference.PhoneNumber1,
                                objAutoLoanReference.MobileNumber1, objAutoLoanReference.ReferenceName2,
                                objAutoLoanReference.Relationship2,
                                objAutoLoanReference.ResidenceAddress2,
                                objAutoLoanReference.WorkAddress2, objAutoLoanReference.PhoneNumber2,
                                objAutoLoanReference.MobileNumber2, OTHER_ACC_BANK_ID_4, OTHER_ACC_TYPE_4,
                                OTHER_ACC_BR_ID_4, OTHER_ACC_NO_4,
                                OTHER_ACC_BANK_ID_5, OTHER_ACC_TYPE_5, OTHER_ACC_BR_ID_5, OTHER_ACC_NO_5, userID, objAutoLoanMaster.LLID);

                        objDbHelper.ExecuteNonQuery(sql);

                    }
                    else
                    {
                        string sql =
                            String.Format(
                                @"insert into loan_app_info(
                            LOAN_APPLI_ID,LOAN_STATU_DT,LOAN_STATU_ID,BR_ID,LOAP_SOURCE_PERSON_NAME,
                            APPLI_DT,RCV_DT,PROD_ID,CUST_NM,LOAN_APP_AMT,
                            LOAP_DECLARED_INCOME,TENOR,TIN_NO,PREVI_LOAN_SCB_TYPE,PREVI_LOAN_SCB_AMT,
                            CR_CARD_NO_SCB,CR_CARD_LIMIT,FATHE_NM,MOTHE_NM,
                            DOB,MARIT_STATU,NO_DEPEN,ID_TYPE,ID_NO,
                            HIGH_EDU_LEVEL,RES_ADD,OFF_PH,RES_PH,MOBIL,
                            E_MAIL,YR_CURRE_ADD, MAILI_ADD,SPOUS_NM,SPOUS_PROFE,
                            SPOUS_WORK_ADD,SPOUS_MOB,BUSIN_NATUR,COMPA_NM,OFF_ADD,
                            DESIG, MON_CURRE_JOB,EMPLO_STATU,TOT_YR,NO_STAFF,
                            TYPE_OWNER,EQUIT_SHARE,OTHER_ANUAL_INCOM,OTHER_INCOM_SOURC,RENT_UTILI,
                            FOOD_CLOTH,EDUCA,LOAN_REPAY,OTHER_EXPEN,OTHER_ACC_BANK_ID_1,
                            OTHER_ACC_TYPE_1,OTHER_ACC_BR_ID_1,OTHER_ACC_NO_1,OTHER_ACC_BANK_ID_2,OTHER_ACC_TYPE_2,
                            OTHER_ACC_BR_ID_2,OTHER_ACC_NO_2,OTHER_ACC_BANK_ID_3,OTHER_ACC_TYPE_3,OTHER_ACC_BR_ID_3,
                            OTHER_ACC_NO_3,CAR_BRAND_ID,VENDO_ID,MFY,CAR_CC,
                            J_APP_NM,J_APP_FATHE_NM,J_APP_MOTHE_NM,J_APP_DOB,J_APP_PROFE,
                            J_APP_EDUCA_LEVEL,J_TIN,J_ID_TYPE1,J_ID_NO1,REF_NM_1,
                            REF_REL_1,REF_RES_ADD_1,REF_WRK_ADD_1,REF_RES_PH_1,REF_MOB_1,
                            REF_NM_2,REF_REL_2,REF_RES_ADD_2,REF_WRK_ADD_2,REF_RES_PH_2,
                            REF_MOB_2,LOAP_OTHERACWITHBANK_ID4,LOAP_OTHERACWITHBANK_ID5,LOAP_ORHREACTYPE_ID4,LOAP_ORHREACTYPE_ID5,
                            LOAP_OTHERACWITHBRANCH_ID4,LOAP_OTHERACWITHBRANCH_ID5,LOAP_OTHERACWITHACNO4,LOAP_OTHERACWITHACNO5,LOAP_USERID) 
                            values({0},'{1}',{2},{3},'{4}','{5}','{6}',{7},'{8}',{9},{10},{11},'{12}','{13}',
                            {14},'{15}',{16},'{17}','{18}','{19}','{20}',{21},'{22}','{23}','{24}','{25}','{26}','{27}','{28}',
                            '{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}',{40},
                            '{41}',{42},{43},'{44}',{45},{46},'{47}',{48},{49},{50},{51},{52},{53},'{54}',{55},'{56}',{57},'{58}',{59},'{60}',
                            {61},'{62}',{63},'{64}',{65},{66},'{67}','{68}','{69}','{70}','{71}','{72}','{73}','{74}','{75}','{76}','{77}',
                            '{78}','{79}','{80}','{81}','{82}','{83}','{84}','{85}','{86}','{87}','{88}','{89}',{90},{91},'{92}','{93}',{94},
                            {95},'{96}', '{97}',{98})",
                                objAutoLoanMaster.LLID, datetimeNow, 4, objAutoLoanMaster.Source,
                                objAutoLoanMaster.SourceName, appliedDate, receiveDate,
                                14, objAutoPRApplicant.PrName, AppliedAmount, DPIAmount, AskingTenor,
                                objAutoPRApplicant.TINNumber,
                                PREVI_LOAN_SCB_TYPE, PREVI_LOAN_SCB_AMT, CR_CARD_NO_SCB, CR_CARD_LIMIT,
                                objAutoPRApplicant.FathersName, objAutoPRApplicant.MothersName, PrDateofBirth,
                                objAutoPRApplicant.MaritalStatus, NumberofDependents,
                                objAutoPRApplicant.IdentificationNumberType, objAutoPRApplicant.IdentificationNumber,
                                objAutoPRApplicant.HighestEducation, objAutoPRApplicant.ResidenceAddress,
                                objAutoPRProfession.OfficePhone, objAutoPRApplicant.PhoneResidence,
                                objAutoPRApplicant.Mobile,
                                objAutoPRApplicant.Email, objAutoPRApplicant.ResidenceStatusYear,
                                objAutoPRApplicant.MailingAddress, objAutoPRApplicant.SpouseName,
                                objAutoPRApplicant.SpouseProfession,
                                objAutoPRApplicant.SpouseWorkAddress, objAutoPRApplicant.SpouseContactNumber,
                                objAutoPRProfession.NatureOfBussiness, objAutoPRProfession.NameofCompany,
                                objAutoPRProfession.Address, objAutoPRProfession.Designation,
                                MonthinCurrentProfession, objAutoPRProfession.EmploymentStatus,
                                TotalProfessionalExperience, NumberOfEmployees,
                                objAutoPRProfession.OwnerShipType, EquityShare,
                                DOIAmount, objAutoPRFinance.DOISource, EDRentAndUtilities, EDFoodAndClothing,
                                EDEducation, EDLoanRePayment,
                                EDOther, OTHER_ACC_BANK_ID_1, OTHER_ACC_TYPE_1, OTHER_ACC_BR_ID_1, OTHER_ACC_NO_1,
                                OTHER_ACC_BANK_ID_2, OTHER_ACC_TYPE_2, OTHER_ACC_BR_ID_2, OTHER_ACC_NO_2,
                                OTHER_ACC_BANK_ID_3, OTHER_ACC_TYPE_3, OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3,
                                Auto_ManufactureId, VendorId,
                                objAutoLoanVehicle.ManufacturingYear, objAutoLoanVehicle.EngineCC,
                                objAutoJTApplicant.JtName, objAutoJTApplicant.FathersName,
                                objAutoJTApplicant.MothersName, JtDateofBirth, objAutoJTProfession.PrimaryProfession,
                                objAutoJTApplicant.HighestEducation,
                                objAutoJTApplicant.TINNumber, objAutoJTApplicant.IdentificationNumberType,
                                objAutoJTApplicant.IdentificationNumber, objAutoLoanReference.ReferenceName1,
                                objAutoLoanReference.Relationship1, objAutoLoanReference.ResidenceAddress1,
                                objAutoLoanReference.WorkAddress1, objAutoLoanReference.PhoneNumber1,
                                objAutoLoanReference.MobileNumber1, objAutoLoanReference.ReferenceName2,
                                objAutoLoanReference.Relationship2,
                                objAutoLoanReference.ResidenceAddress2,
                                objAutoLoanReference.WorkAddress2, objAutoLoanReference.PhoneNumber2,
                                objAutoLoanReference.MobileNumber2, OTHER_ACC_BANK_ID_4, OTHER_ACC_TYPE_4,
                                OTHER_ACC_BR_ID_4, OTHER_ACC_NO_4,
                                OTHER_ACC_BANK_ID_5, OTHER_ACC_TYPE_5, OTHER_ACC_BR_ID_5, OTHER_ACC_NO_5, userID);

                        objDbHelper.ExecuteNonQuery(sql);

                    }

                    #endregion save DeDup

                }
                else
                {
                }

                objDbHelper.CommitTransaction();
                mesg = "Success";
            }
            catch (Exception exception)
            {
                //mesg = exception.Message;
                objDbHelper.RollBack();
            }
            finally {
                objDbHelper.Close();
            }
            return mesg;
        }


        private void SaveExecutionByUser(Auto_App_Step_ExecutionByUser ExecutionByUser, CommonDbHelper objDbHelper)
        {
            int isExist = 0;
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            try
            {
                sql = string.Format(@"Select count(*) from Auto_App_Step_ExecutionByUser where Auto_LoanMasterId = {0}", ExecutionByUser.Auto_LoanMasterId);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                while (reader.Read())
                {
                    isExist = reader.GetInt32(0);
                }
                reader.Close();
                if (isExist > 0)
                {
                    sql = string.Format(@"update Auto_App_Step_ExecutionByUser set 
                                        AnalystMasterId = {0},
                                        AppRaiseBy = {1},
                                        AppSupportBy = {2},
                                        AppAnalysisBy = {3},
                                        AppApprovedBy = {4} where Auto_LoanMasterId = {5}",
                                        ExecutionByUser.AnalystMasterId, ExecutionByUser.AppRaiseBy, ExecutionByUser.AppSupportBy,
                                        ExecutionByUser.AppAnalysisBy, ExecutionByUser.AppApprovedBy, ExecutionByUser.Auto_LoanMasterId);
                }
                else
                {
                    sql = string.Format(@"insert into auto_app_step_executionbyuser 
                                        (AnalystMasterId, Auto_LoanMasterId, AppRaiseBy, AppSupportBy, 
                                        AppAnalysisBy, AppApprovedBy)
                                        values
                                        ({0}, {1}, {2}, {3}, 
                                        {4}, {5})",
                                        ExecutionByUser.AnalystMasterId, ExecutionByUser.Auto_LoanMasterId, ExecutionByUser.AppRaiseBy, ExecutionByUser.AppSupportBy,
                                        ExecutionByUser.AppAnalysisBy, ExecutionByUser.AppApprovedBy);
                }

                objDbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }


        public void SaveAutoApplicantSCBAccount(AutoApplicantSCBAccount autoApplicantSCBAccount, CommonDbHelper objDbHelper)
        {
            string sql = "";
            //var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
            //                 DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            sql =
                String.Format(
                    @"INSERT INTO Auto_ApplicantSCBAccount
                    (AutoLoanMasterId, LLID, AccountNumberSCB, AccountStatus, PriorityHolder) 
                    values
                    ({0}, {1}, '{2}',{3},{4})",
                    autoApplicantSCBAccount.AutoLoanMasterId, autoApplicantSCBAccount.LLID,
                    autoApplicantSCBAccount.AccountNumberSCB.Replace("'", "''"), autoApplicantSCBAccount.AccountStatus, autoApplicantSCBAccount.PriorityHolder);

            objDbHelper.ExecuteNonQuery(sql);

        }

        public void SaveAutoStatusHistory(AutoStatusHistory objAutoStatusHistory) 
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                sql =
                    String.Format(
                        @"INSERT INTO AutoStatusHistory
                    (AutoLoanMasterId, UserID, StatusID,ActionDate) 
                    values
                    ({0}, {1}, {2},'{3}')",
                        objAutoStatusHistory.AutoLoanMasterId,
                        objAutoStatusHistory.UserID, objAutoStatusHistory.StatusID, lastupdate);
                DbQueryManager.ExecuteNonQuery(sql);
                
            }
            catch (Exception ex) { 
               throw ex;
            }

        }

        public string SaveAutoPRBankStatement(AutoPRBankStatement objPRBankStatement, List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList, int oldBankID, string oldAccountNO)//, CommonDbHelper objDbHelper)
        {
            string returnValue = "";
            var objDbHelper = new CommonDbHelper();
            objDbHelper.BeginTransaction();
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            string statement = "";
            if (objPRBankStatement.Statement == "" || objPRBankStatement.Statement == null)
            {
                statement = "";
            }
            else
            {
                statement = objPRBankStatement.Statement.Replace("'", "''");
            }

            string fromDate = objPRBankStatement.StatementDateFrom.ToString("yyyy-MM-dd");
            string toDate = objPRBankStatement.StatementDateTo.ToString("yyyy-MM-dd");


            try
            {
                if (objPRBankStatement.PrBankStatementId != 0)
                {
                    sql = String.Format(@"UPDATE auto_prbankstatement SET 
                    LLID = {0}, 
                    AutoLoanMasterId = {1}, 
                    BankId = {2}, 
                    BranchId = {3}, 
                    AccountName = '{4}', 
                    AccountNumber = '{5}', 
                    AccountCategory = '{6}', 
                    AccountType = '{7}', 
                    Statement = '{8}', 
                    LastUpdateDate = '{9}',
                    StatementDateFrom = '{11}',
                    StatementDateTo = '{12}'  
                    where PrBankStatementId = {10}",
                    objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId,
                    objPRBankStatement.BankId, objPRBankStatement.BranchId, objPRBankStatement.AccountName.Replace("'", "''"),
                    objPRBankStatement.AccountNumber, objPRBankStatement.AccountCategory, objPRBankStatement.AccountType,
                    statement, lastupdate, objPRBankStatement.PrBankStatementId, fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);


                    #region Insering data for AVG Total table

                    string deleteSQL =
                        string.Format(
                            @"Delete from auto_bankstatementavgtotal where AutoLoanMasterId = {0} 
                              and LLId = {1} and BankId = {2} and AccountNumber = '{3}' and Type = 1",
                              objPRBankStatement.AutoLoanMasterId, objPRBankStatement.LLID, oldBankID,
                              oldAccountNO);

                    objDbHelper.ExecuteNonQuery(deleteSQL);

                    foreach (Auto_BankStatementAvgTotal avg in auto_BankStatementAvgTotalList)
                    {
                        avg.AccountNumber = objPRBankStatement.AccountNumber;
                        avg.AutoLoanMasterId = objPRBankStatement.AutoLoanMasterId;
                        avg.BankId = objPRBankStatement.BankId;
                        avg.BankStatementId = objPRBankStatement.PrBankStatementId;
                        avg.LLId = objPRBankStatement.LLID;
                        avg.Adjustment = 0;
                        avg.Type = 1;//1 = primary and 2 = joint
                        SaveBankStatementAVGTotal(avg, objDbHelper);
                    }

                    #endregion Insering data for AVG Total table



                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_prbankstatement
                    (LLID, AutoLoanMasterId, BankId, BranchId, 
                    AccountName, AccountNumber, AccountCategory, AccountType, Statement, 
                    LastUpdateDate,StatementDateFrom,StatementDateTo)
	                values
                    ({0}, {1}, {2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                    objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId,
                    objPRBankStatement.BankId, objPRBankStatement.BranchId, objPRBankStatement.AccountName,
                    objPRBankStatement.AccountNumber, objPRBankStatement.AccountCategory, objPRBankStatement.AccountType,
                    statement, lastupdate, fromDate, toDate);
                    objDbHelper.ExecuteNonQuery(sql);



                    #region Insering data for AVG Total table

                    int PrBankStatementId = 0;

                    string query = String.Format(@"Select Max(PrBankStatementId) from auto_prbankstatement where 
                                        LLID={0} and 
                                        AutoLoanMasterId={1} and 
                                        BankId={2} and  
                                        BranchId={3} and 
                                        AccountName='{4}' and 
                                        AccountNumber='{5}'",
                                      objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId, objPRBankStatement.BankId,
                                      objPRBankStatement.BranchId, objPRBankStatement.AccountName, objPRBankStatement.AccountNumber);
                    //true, objAutoLoanMaster.ReferenceLLId);
                    IDataReader reader = objDbHelper.GetDataReader(query);
                    while (reader.Read())
                    {
                        PrBankStatementId = Convert.ToInt32(reader.GetValue(0));
                    }
                    reader.Close();


                    string deleteSQL =
                        string.Format(
                            @"Delete from auto_bankstatementavgtotal where AutoLoanMasterId = {0} 
                              and LLId = {1} and BankId = {2} and AccountNumber = '{3}' and Type = 1",
                              objPRBankStatement.AutoLoanMasterId, objPRBankStatement.LLID, objPRBankStatement.BankId,
                              objPRBankStatement.AccountNumber);

                    objDbHelper.ExecuteNonQuery(deleteSQL);

                    foreach (Auto_BankStatementAvgTotal avg in auto_BankStatementAvgTotalList)
                    {
                        avg.AccountNumber = objPRBankStatement.AccountNumber;
                        avg.AutoLoanMasterId = objPRBankStatement.AutoLoanMasterId;
                        avg.BankId = objPRBankStatement.BankId;
                        avg.BankStatementId = PrBankStatementId;
                        avg.LLId = objPRBankStatement.LLID;
                        avg.Adjustment = 0;
                        avg.Type = 1;//1= primary, 2 = joint
                        SaveBankStatementAVGTotal(avg, objDbHelper);
                    }

                    #endregion Insering data for AVG Total table


                }
                returnValue = "Success";
                objDbHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                returnValue = "failed";
                objDbHelper.RollBack();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return returnValue;
        }

        public string SaveAutoPRBankStatement(AutoPRBankStatement objPRBankStatement)
        {
            string returnValue = "";
            var objDbHelper = new CommonDbHelper();
            objDbHelper.BeginTransaction();
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            string statement = "";
            if (objPRBankStatement.Statement == "" || objPRBankStatement.Statement == null)
            {
                statement = "";
            }
            else
            {
                statement = objPRBankStatement.Statement.Replace("'", "''");
            }

            string fromDate = objPRBankStatement.StatementDateFrom.ToString("yyyy-MM-dd");
            string toDate = objPRBankStatement.StatementDateTo.ToString("yyyy-MM-dd");


            try
            {
                if (objPRBankStatement.PrBankStatementId != 0)
                {
                    sql = String.Format(@"UPDATE auto_prbankstatement SET 
                    LLID = {0}, 
                    AutoLoanMasterId = {1}, 
                    BankId = {2}, 
                    BranchId = {3}, 
                    AccountName = '{4}', 
                    AccountNumber = '{5}', 
                    AccountCategory = '{6}', 
                    AccountType = '{7}', 
                    Statement = '{8}', 
                    LastUpdateDate = '{9}',
                    StatementDateFrom = '{11}',
                    StatementDateTo = '{12}'  
                    where PrBankStatementId = {10}",
                    objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId,
                    objPRBankStatement.BankId, objPRBankStatement.BranchId, objPRBankStatement.AccountName.Replace("'", "''"),
                    objPRBankStatement.AccountNumber, objPRBankStatement.AccountCategory, objPRBankStatement.AccountType,
                    statement, lastupdate, objPRBankStatement.PrBankStatementId, fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_prbankstatement
                    (LLID, AutoLoanMasterId, BankId, BranchId, 
                    AccountName, AccountNumber, AccountCategory, AccountType, Statement, 
                    LastUpdateDate,StatementDateFrom,StatementDateTo)
	                values
                    ({0}, {1}, {2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                    objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId,
                    objPRBankStatement.BankId, objPRBankStatement.BranchId, objPRBankStatement.AccountName,
                    objPRBankStatement.AccountNumber, objPRBankStatement.AccountCategory, objPRBankStatement.AccountType,
                    statement, lastupdate, fromDate, toDate);
                    objDbHelper.ExecuteNonQuery(sql);


                    if (objPRBankStatement.BankId != 42)
                    {
                        var objAutoPRAccountDetail = new AutoPRAccountDetail();
                        objAutoPRAccountDetail.PrAccountDetailsId = 0;
                        objAutoPRAccountDetail.AutoLoanMasterId = objPRBankStatement.AutoLoanMasterId;
                        objAutoPRAccountDetail.BankId = objPRBankStatement.BankId;
                        objAutoPRAccountDetail.BranchId = objPRBankStatement.BranchId;
                        objAutoPRAccountDetail.AccountNumber = objPRBankStatement.AccountNumber;
                        objAutoPRAccountDetail.AccountCategory = objPRBankStatement.AccountCategory;
                        objAutoPRAccountDetail.AccountType = objPRBankStatement.AccountType;
                        SaveAutoPRAccountDetail(objAutoPRAccountDetail, objDbHelper);
                    }
                    else {
                        var objAutoPRAccount = new AutoPRAccount();
                        objAutoPRAccount.PrAccountId = 0;
                        objAutoPRAccount.AutoLoanMasterId = objPRBankStatement.AutoLoanMasterId;
                        objAutoPRAccount.AccountNumberForSCB = objPRBankStatement.AccountNumber;
                        objAutoPRAccount.CreditCardNumberForSCB = "";
                        SaveAutoPRAccount(objAutoPRAccount, objDbHelper);

                        
                    }
                }
                returnValue = "Success";
                objDbHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                returnValue = "failed";
                objDbHelper.RollBack();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return returnValue;
        }

        public string SaveAutoPRBankStatement(AutoPRBankStatement objPRBankStatement, CommonDbHelper objDbHelper)
        {
            string returnValue = "";
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            string statement = "";
            if (objPRBankStatement.Statement == "" || objPRBankStatement.Statement == null)
            {
                statement = "";
            }
            else
            {
                statement = objPRBankStatement.Statement.Replace("'", "''");
            }

            string fromDate = objPRBankStatement.StatementDateFrom.ToString("yyyy-MM-dd");
            string toDate = objPRBankStatement.StatementDateTo.ToString("yyyy-MM-dd");


            try
            {
                if (objPRBankStatement.PrBankStatementId != 0)
                {
                    sql = String.Format(@"UPDATE auto_prbankstatement SET 
                    LLID = {0}, 
                    AutoLoanMasterId = {1}, 
                    BankId = {2}, 
                    BranchId = {3}, 
                    AccountName = '{4}', 
                    AccountNumber = '{5}', 
                    AccountCategory = '{6}', 
                    AccountType = '{7}', 
                    Statement = '{8}', 
                    LastUpdateDate = '{9}',
                    StatementDateFrom = '{11}',
                    StatementDateTo = '{12}'  
                    where PrBankStatementId = {10}",
                    objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId,
                    objPRBankStatement.BankId, objPRBankStatement.BranchId, objPRBankStatement.AccountName.Replace("'", "''"),
                    objPRBankStatement.AccountNumber, objPRBankStatement.AccountCategory, objPRBankStatement.AccountType,
                    statement, lastupdate, objPRBankStatement.PrBankStatementId, fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_prbankstatement
                    (LLID, AutoLoanMasterId, BankId, BranchId, 
                    AccountName, AccountNumber, AccountCategory, AccountType, Statement, 
                    LastUpdateDate,StatementDateFrom,StatementDateTo)
	                values
                    ({0}, {1}, {2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                    objPRBankStatement.LLID, objPRBankStatement.AutoLoanMasterId,
                    objPRBankStatement.BankId, objPRBankStatement.BranchId, objPRBankStatement.AccountName,
                    objPRBankStatement.AccountNumber, objPRBankStatement.AccountCategory, objPRBankStatement.AccountType,
                    statement, lastupdate, fromDate, toDate);
                    objDbHelper.ExecuteNonQuery(sql);

                    

                }
                returnValue = "Success";
            }
            catch (Exception ex)
            {
                returnValue = "failed";
                objDbHelper.RollBack();
                throw ex;
            }
            
            return returnValue;
        }

        public string SaveAutoJTBankStatement(AutoJTBankStatement objJTBankStatement, List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList, int oldBankID, string oldAccountNO)//, CommonDbHelper objDbHelper)
        {
            string returnValue = "";
            var objDbHelper = new CommonDbHelper();
            objDbHelper.BeginTransaction();

            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            string statement = "";
            if (objJTBankStatement.Statement == "" || objJTBankStatement.Statement == null)
            {
                statement = "";
            }
            else
            {
                statement = objJTBankStatement.Statement.Replace("'", "''");
            }

            string fromDate = objJTBankStatement.StatementDateFrom.ToString("yyyy-MM-dd");
            string toDate = objJTBankStatement.StatementDateTo.ToString("yyyy-MM-dd");


            try
            {
                if (objJTBankStatement.JtBankStatementId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtbankstatement SET 
                    LLID = {0}, 
                    AutoLoanMasterId = {1}, 
                    BankId = {2}, 
                    BranchId = {3}, 
                    AccountName = '{4}', 
                    AccountNumber = '{5}', 
                    AccountCategory = '{6}', 
                    AccountType = '{7}', 
                    Statement = '{8}', 
                    LastUpdateDate = '{9}',
                    StatementDateFrom = '{11}',
                    StatementDateTo = '{12}'  
                    where JtBankStatementId = {10}",
                    objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId,
                    objJTBankStatement.BankId, objJTBankStatement.BranchId, objJTBankStatement.AccountName,
                    objJTBankStatement.AccountNumber, objJTBankStatement.AccountCategory, objJTBankStatement.AccountType,
                    statement, lastupdate, objJTBankStatement.JtBankStatementId,
                    fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                    #region Insering data for AVG Total table

                    string deleteSQL =
                        string.Format(
                            @"Delete from auto_bankstatementavgtotal where AutoLoanMasterId = {0} 
                              and LLId = {1} and BankId = {2} and AccountNumber = '{3}' and Type = 2",
                              objJTBankStatement.AutoLoanMasterId, objJTBankStatement.LLID, oldBankID,
                              oldAccountNO);

                    objDbHelper.ExecuteNonQuery(deleteSQL);

                    foreach (Auto_BankStatementAvgTotal avg in auto_BankStatementAvgTotalList)
                    {
                        avg.AccountNumber = objJTBankStatement.AccountNumber;
                        avg.AutoLoanMasterId = objJTBankStatement.AutoLoanMasterId;
                        avg.BankId = objJTBankStatement.BankId;
                        avg.BankStatementId = objJTBankStatement.JtBankStatementId;
                        avg.LLId = objJTBankStatement.LLID;
                        avg.Adjustment = 0;
                        avg.Type = 2;//1 = primary and 2 = joint
                        SaveBankStatementAVGTotal(avg, objDbHelper);
                    }

                    #endregion Insering data for AVG Total table




                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_jtbankstatement
                    (LLID, AutoLoanMasterId, BankId, BranchId, 
                    AccountName, AccountNumber, AccountCategory, AccountType, Statement, 
                    LastUpdateDate,StatementDateFrom,StatementDateTo)
	                values
                    ({0}, {1}, {2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                    objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId,
                    objJTBankStatement.BankId, objJTBankStatement.BranchId, objJTBankStatement.AccountName,
                    objJTBankStatement.AccountNumber, objJTBankStatement.AccountCategory, objJTBankStatement.AccountType,
                    statement, lastupdate, fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                    #region Insering data for AVG Total table

                    int JtBankStatementId = 0;

                    string query = String.Format(@"Select Max(JtBankStatementId) from auto_jtbankstatement where 
                                        LLID={0} and 
                                        AutoLoanMasterId={1} and 
                                        BankId={2} and  
                                        BranchId={3} and 
                                        AccountName='{4}' and 
                                        AccountNumber='{5}'",
                                      objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId, objJTBankStatement.BankId,
                                      objJTBankStatement.BranchId, objJTBankStatement.AccountName, objJTBankStatement.AccountNumber);
                    //true, objAutoLoanMaster.ReferenceLLId);
                    IDataReader reader = objDbHelper.GetDataReader(query);
                    while (reader.Read())
                    {
                        JtBankStatementId = Convert.ToInt32(reader.GetValue(0));
                    }
                    reader.Close();


                    string deleteSQL =
                        string.Format(
                            @"Delete from auto_bankstatementavgtotal where AutoLoanMasterId = {0} 
                              and LLId = {1} and BankId = {2} and AccountNumber = '{3}' and Type = 2",
                              objJTBankStatement.AutoLoanMasterId, objJTBankStatement.LLID, objJTBankStatement.BankId,
                              objJTBankStatement.AccountNumber);

                    objDbHelper.ExecuteNonQuery(deleteSQL);

                    foreach (Auto_BankStatementAvgTotal avg in auto_BankStatementAvgTotalList)
                    {
                        avg.AccountNumber = objJTBankStatement.AccountNumber;
                        avg.AutoLoanMasterId = objJTBankStatement.AutoLoanMasterId;
                        avg.BankId = objJTBankStatement.BankId;
                        avg.BankStatementId = JtBankStatementId;
                        avg.LLId = objJTBankStatement.LLID;
                        avg.Adjustment = 0;
                        avg.Type = 2;//1= primary, 2 = joint
                        SaveBankStatementAVGTotal(avg, objDbHelper);
                    }

                    #endregion Insering data for AVG Total table


                }
                returnValue = "Success";
                objDbHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                returnValue = "failed";
                objDbHelper.RollBack();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return returnValue;
        }

        public string SaveAutoJTBankStatement(AutoJTBankStatement objJTBankStatement)//, CommonDbHelper objDbHelper)
        {
            string returnValue = "";
            var objDbHelper = new CommonDbHelper();
            objDbHelper.BeginTransaction();

            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            string statement = "";
            if (objJTBankStatement.Statement == "" || objJTBankStatement.Statement == null)
            {
                statement = "";
            }
            else
            {
                statement = objJTBankStatement.Statement.Replace("'", "''");
            }

            string fromDate = objJTBankStatement.StatementDateFrom.ToString("yyyy-MM-dd");
            string toDate = objJTBankStatement.StatementDateTo.ToString("yyyy-MM-dd");


            try
            {
                if (objJTBankStatement.JtBankStatementId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtbankstatement SET 
                    LLID = {0}, 
                    AutoLoanMasterId = {1}, 
                    BankId = {2}, 
                    BranchId = {3}, 
                    AccountName = '{4}', 
                    AccountNumber = '{5}', 
                    AccountCategory = '{6}', 
                    AccountType = '{7}', 
                    Statement = '{8}', 
                    LastUpdateDate = '{9}',
                    StatementDateFrom = '{11}',
                    StatementDateTo = '{12}'  
                    where JtBankStatementId = {10}",
                    objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId,
                    objJTBankStatement.BankId, objJTBankStatement.BranchId, objJTBankStatement.AccountName,
                    objJTBankStatement.AccountNumber, objJTBankStatement.AccountCategory, objJTBankStatement.AccountType,
                    statement, lastupdate, objJTBankStatement.JtBankStatementId,
                    fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_jtbankstatement
                    (LLID, AutoLoanMasterId, BankId, BranchId, 
                    AccountName, AccountNumber, AccountCategory, AccountType, Statement, 
                    LastUpdateDate,StatementDateFrom,StatementDateTo)
	                values
                    ({0}, {1}, {2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                    objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId,
                    objJTBankStatement.BankId, objJTBankStatement.BranchId, objJTBankStatement.AccountName,
                    objJTBankStatement.AccountNumber, objJTBankStatement.AccountCategory, objJTBankStatement.AccountType,
                    statement, lastupdate, fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);
                   // if (objAutoJTAccountDetail.BankId != 42)
                    //{
                        var objAutoJTAccountDetail = new AutoJTAccountDetail();
                        objAutoJTAccountDetail.JtAccountDetailsId = 0;
                        objAutoJTAccountDetail.AutoLoanMasterId = objJTBankStatement.AutoLoanMasterId;
                        objAutoJTAccountDetail.BankId = objJTBankStatement.BankId;
                        objAutoJTAccountDetail.BranchId = objJTBankStatement.BranchId;
                        objAutoJTAccountDetail.AccountNumber = objJTBankStatement.AccountNumber;
                        objAutoJTAccountDetail.AccountCategory = objJTBankStatement.AccountCategory;
                        objAutoJTAccountDetail.AccountType = objJTBankStatement.AccountType;
                        SaveAutoJtAccountDetail(objAutoJTAccountDetail, objDbHelper);
                    //}
                    //else
                    //{
                    //    var objAutoJtAccount = new AutoJTAccount();
                    //    objAutoJtAccount.jtAccountId = 0;
                    //    objAutoJtAccount.AccountNumberForSCB = 
                    //}

                }
                returnValue = "Success";
                objDbHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                returnValue = "failed";
                objDbHelper.RollBack();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return returnValue;
        }

        public string SaveAutoJTBankStatement(AutoJTBankStatement objJTBankStatement, CommonDbHelper objDbHelper)
        {
            string returnValue = "";
            

            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            string statement = "";
            if (objJTBankStatement.Statement == "" || objJTBankStatement.Statement == null)
            {
                statement = "";
            }
            else
            {
                statement = objJTBankStatement.Statement.Replace("'", "''");
            }

            string fromDate = objJTBankStatement.StatementDateFrom.ToString("yyyy-MM-dd");
            string toDate = objJTBankStatement.StatementDateTo.ToString("yyyy-MM-dd");


            try
            {
                if (objJTBankStatement.JtBankStatementId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtbankstatement SET 
                    LLID = {0}, 
                    AutoLoanMasterId = {1}, 
                    BankId = {2}, 
                    BranchId = {3}, 
                    AccountName = '{4}', 
                    AccountNumber = '{5}', 
                    AccountCategory = '{6}', 
                    AccountType = '{7}', 
                    Statement = '{8}', 
                    LastUpdateDate = '{9}',
                    StatementDateFrom = '{11}',
                    StatementDateTo = '{12}'  
                    where JtBankStatementId = {10}",
                    objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId,
                    objJTBankStatement.BankId, objJTBankStatement.BranchId, objJTBankStatement.AccountName,
                    objJTBankStatement.AccountNumber, objJTBankStatement.AccountCategory, objJTBankStatement.AccountType,
                    statement, lastupdate, objJTBankStatement.JtBankStatementId,
                    fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_jtbankstatement
                    (LLID, AutoLoanMasterId, BankId, BranchId, 
                    AccountName, AccountNumber, AccountCategory, AccountType, Statement, 
                    LastUpdateDate,StatementDateFrom,StatementDateTo)
	                values
                    ({0}, {1}, {2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                    objJTBankStatement.LLID, objJTBankStatement.AutoLoanMasterId,
                    objJTBankStatement.BankId, objJTBankStatement.BranchId, objJTBankStatement.AccountName,
                    objJTBankStatement.AccountNumber, objJTBankStatement.AccountCategory, objJTBankStatement.AccountType,
                    statement, lastupdate, fromDate, toDate);

                    objDbHelper.ExecuteNonQuery(sql);

                }
                returnValue = "Success";
                
            }
            catch (Exception ex)
            {
                returnValue = "failed";
                objDbHelper.RollBack();
                throw ex;
            }
            
            return returnValue;
        }

        private void SaveBankStatementAVGTotal(Auto_BankStatementAvgTotal avg, CommonDbHelper objDbHelper)
        {
            string month = avg.Month.Date.ToString("yyyy-MM-dd");
            string sql = string.Format(@"insert into auto_bankstatementavgtotal 
	                                    (LLId, AutoLoanMasterId, Type, BankStatementId,BankId, Month, AVGTotal, TrunOverTotal, 
                                            Adjustment, AccountNumber)
	                                    values
	                                    ({0},{1},{2},{3},{4},'{5}',{6},{7},{8},'{9}')",
                                      avg.LLId, avg.AutoLoanMasterId, avg.Type, avg.BankStatementId, avg.BankId,
                                      month, avg.AVGTotal, avg.TrunOverTotal, avg.Adjustment, avg.AccountNumber);
            objDbHelper.ExecuteNonQuery(sql);
        }

        private void SaveAutoLoanSecurity(AutoLoanSecurity objAutoLoanSecurityMember, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var issueDate = objAutoLoanSecurityMember.IssueDate.ToString("yyyy-MM-dd");
            try
            {
                //                if (objAutoLoanSecurityMember.SecurityId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_loansecurity SET
                //	                AutoLoanMasterId = {0},NatureOfSecurity = {1},IssuingOffice = '{2}',
                //	                FaceValue = {3},XTVRate = {4},IssueDate = '{5}',InterestRate = '{6}',FacilityType = {8} where SecurityId = {7}", 
                //                    objAutoLoanSecurityMember.AutoLoanMasterId,
                //                    objAutoLoanSecurityMember.NatureOfSecurity, objAutoLoanSecurityMember.IssuingOffice,
                //                    objAutoLoanSecurityMember.FaceValue, objAutoLoanSecurityMember.XTVRate, issueDate,
                //                    objAutoLoanSecurityMember.InterestRate, objAutoLoanSecurityMember.SecurityId, objAutoLoanSecurityMember.FacilityType);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql =
                    String.Format(
                        @"INSERT INTO auto_loansecurity
	                (AutoLoanMasterId, NatureOfSecurity, IssuingOffice, 
	                FaceValue, XTVRate, IssueDate, InterestRate,FacilityType, Status, FacilityId)
                    values
                    ({0}, {1}, '{2}', 
                    {3}, {4}, '{5}', '{6}',{7},{8},{9})",
                        objAutoLoanSecurityMember.AutoLoanMasterId,
                        objAutoLoanSecurityMember.NatureOfSecurity, objAutoLoanSecurityMember.IssuingOffice,
                        objAutoLoanSecurityMember.FaceValue, objAutoLoanSecurityMember.XTVRate, issueDate,
                        objAutoLoanSecurityMember.InterestRate, objAutoLoanSecurityMember.FacilityType, objAutoLoanSecurityMember.Status,objAutoLoanSecurityMember.FacilityId);

                objDbHelper.ExecuteNonQuery(sql);
                //}
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoApplicationInsurance(AutoApplicationInsurance objAutoApplicationInsurance, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoApplicationInsurance.ApplicationInsuranceId != 0)
                {
                    sql = String.Format(@"UPDATE auto_applicationinsurance SET
                    AutoLoanMasterId = {0},
                    GeneralInsurance = {1},
                    LifeInsurance = {2},
                    InsuranceFinancewithLoan = {3}, InsuranceFinancewithLoanLife = {5} where ApplicationInsuranceId = {4}",
                    objAutoApplicationInsurance.AutoLoanMasterId,
                    objAutoApplicationInsurance.GeneralInsurance, objAutoApplicationInsurance.LifeInsurance, objAutoApplicationInsurance.InsuranceFinancewithLoan,
                    objAutoApplicationInsurance.ApplicationInsuranceId, objAutoApplicationInsurance.InsuranceFinancewithLoanLife);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_applicationinsurance
                    (AutoLoanMasterId, GeneralInsurance, LifeInsurance, InsuranceFinancewithLoan,InsuranceFinancewithLoanLife) 
                    values
                    ({0}, {1}, {2},{3}, {4})", objAutoApplicationInsurance.AutoLoanMasterId,
                    objAutoApplicationInsurance.GeneralInsurance, objAutoApplicationInsurance.LifeInsurance,
                    objAutoApplicationInsurance.InsuranceFinancewithLoan, objAutoApplicationInsurance.InsuranceFinancewithLoanLife);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoLoanFacility(AutoLoanFacility objAutoLoanFacilityListMember, CommonDbHelper objDbHelper)
        {
            string sql = "";
            try
            {
                //                if (objAutoLoanFacilityListMember.FacilityId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_loanfacility SET
                //	                AutoLoanMasterId = {0},FacilityType = {1},InterestRate = {2},PresentBalance = {3},
                //	                PresentEMI = {4},PresentLimit = {5}, ProposedLimit = {6},RepaymentAgrement = {7} where FacilityId = {8}", 
                //                    objAutoLoanFacilityListMember.AutoLoanMasterId,
                //                    objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                //                    objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                //                    objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.FacilityId);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql = String.Format(@"INSERT INTO auto_loanfacility
	                (AutoLoanMasterId, FacilityType, InterestRate, PresentBalance, 
	                PresentEMI, PresentLimit, ProposedLimit, RepaymentAgrement)
                    values
                    ({0}, {1}, {2}, 
                    {3}, {4}, {5}, {6}, {7})", objAutoLoanFacilityListMember.AutoLoanMasterId,
                objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement);

                objDbHelper.ExecuteNonQuery(sql);
                //}
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private int SaveAutoLoanFacilityWithReturn(AutoLoanFacility objAutoLoanFacilityListMember, CommonDbHelper objDbHelper)
        {
            int facilityId = 0;
            string sql = "";
            try
            {
                //                if (objAutoLoanFacilityListMember.FacilityId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_loanfacility SET
                //	                AutoLoanMasterId = {0},FacilityType = {1},InterestRate = {2},PresentBalance = {3},
                //	                PresentEMI = {4},PresentLimit = {5}, ProposedLimit = {6},RepaymentAgrement = {7} where FacilityId = {8}", 
                //                    objAutoLoanFacilityListMember.AutoLoanMasterId,
                //                    objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                //                    objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                //                    objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.FacilityId);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql = String.Format(@"INSERT INTO auto_loanfacility
	                (AutoLoanMasterId, FacilityType, InterestRate, PresentBalance, 
	                PresentEMI, PresentLimit, ProposedLimit, RepaymentAgrement)
                    values
                    ({0}, {1}, {2}, 
                    {3}, {4}, {5}, {6}, {7})", objAutoLoanFacilityListMember.AutoLoanMasterId,
                objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement);
                objDbHelper.ExecuteNonQuery(sql);

                var selectQuary = string.Format(@"select FacilityId from auto_loanfacility where AutoLoanMasterId={0} and FacilityType={1} and 
                    InterestRate='{2}' and PresentBalance='{3}' and PresentEMI='{4}' and PresentLimit='{5}' and ProposedLimit='{6}' 
                    and RepaymentAgrement={7}", objAutoLoanFacilityListMember.AutoLoanMasterId, objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement);

                IDataReader reader = objDbHelper.GetDataReader(selectQuary);
                while (reader.Read())
                {
                    facilityId = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return facilityId;
        }

        private void SaveAutoLoanReference(AutoLoanReference objAutoLoanReference, CommonDbHelper objDbHelper)
        {
            string sql = "";
            try
            {
                if (objAutoLoanReference.ReferenceId != 0)
                {
                    sql = String.Format(@"UPDATE auto_loanreference SET
	AutoLoanMasterId = {0},ReferenceName1 = '{1}',ReferenceName2 = '{2}',
	Relationship1 = '{3}',Relationship2 = '{4}',Occupation1 = '{5}',Occupation2 = '{6}',OrganizationName1 = '{7}',
	OrganizationName2 = '{8}',Designation1 = '{9}',Designation2 = '{10}',WorkAddress1 = '{11}',
	WorkAddress2 = '{12}',ResidenceAddress1 = '{13}',ResidenceAddress2 = '{14}',PhoneNumber1 = '{15}',
	PhoneNumber2 = '{16}',MobileNumber1 = '{17}',MobileNumber2 = '{18}' where ReferenceId = {19}", objAutoLoanReference.AutoLoanMasterId,
                    objAutoLoanReference.ReferenceName1.Replace("'", "''"), objAutoLoanReference.ReferenceName2.Replace("'", "''"),
                    objAutoLoanReference.Relationship1.Replace("'", "''"), objAutoLoanReference.Relationship2.Replace("'", "''"), objAutoLoanReference.Occupation1.Replace("'", "''"), objAutoLoanReference.Occupation2.Replace("'", "''"), objAutoLoanReference.OrganizationName1.Replace("'", "''"),
                    objAutoLoanReference.OrganizationName2.Replace("'", "''"), objAutoLoanReference.Designation1.Replace("'", "''"), objAutoLoanReference.Designation2.Replace("'", "''"), objAutoLoanReference.WorkAddress1.Replace("'", "''"),
                    objAutoLoanReference.WorkAddress2.Replace("'", "''"), objAutoLoanReference.ResidenceAddress1.Replace("'", "''"), objAutoLoanReference.ResidenceAddress2.Replace("'", "''"), objAutoLoanReference.PhoneNumber1.Replace("'", "''"),
                    objAutoLoanReference.PhoneNumber2.Replace("'", "''"), objAutoLoanReference.MobileNumber1.Replace("'", "''"), objAutoLoanReference.MobileNumber2.Replace("'", "''"), objAutoLoanReference.ReferenceId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_loanreference
	(AutoLoanMasterId, ReferenceName1, ReferenceName2, 
	Relationship1, Relationship2, Occupation1, Occupation2, OrganizationName1, 
	OrganizationName2, Designation1, Designation2, WorkAddress1, 
	WorkAddress2, ResidenceAddress1, ResidenceAddress2, PhoneNumber1, 
	PhoneNumber2, MobileNumber1, MobileNumber2)
                    values
                    ({0}, '{1}', '{2}', 
                    '{3}', '{4}', '{5}', '{6}', '{7}', 
                    '{8}', '{9}', '{10}', '{11}',
                    '{12}','{13}','{14}','{15}',
                    '{16}','{17}','{18}')", objAutoLoanReference.AutoLoanMasterId,
                    objAutoLoanReference.ReferenceName1.Replace("'", "''"), objAutoLoanReference.ReferenceName2.Replace("'", "''"),
                    objAutoLoanReference.Relationship1.Replace("'", "''"), objAutoLoanReference.Relationship2.Replace("'", "''"), objAutoLoanReference.Occupation1.Replace("'", "''"), objAutoLoanReference.Occupation2.Replace("'", "''"), objAutoLoanReference.OrganizationName1.Replace("'", "''"),
                    objAutoLoanReference.OrganizationName2.Replace("'", "''"), objAutoLoanReference.Designation1.Replace("'", "''"), objAutoLoanReference.Designation2.Replace("'", "''"), objAutoLoanReference.WorkAddress1.Replace("'", "''"),
                    objAutoLoanReference.WorkAddress2.Replace("'", "''"), objAutoLoanReference.ResidenceAddress1.Replace("'", "''"), objAutoLoanReference.ResidenceAddress2.Replace("'", "''"), objAutoLoanReference.PhoneNumber1.Replace("'", "''"),
                    objAutoLoanReference.PhoneNumber2.Replace("'", "''"), objAutoLoanReference.MobileNumber1.Replace("'", "''"), objAutoLoanReference.MobileNumber2.Replace("'", "''"));

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoJTFinance(AutoJTFinance objAutoJTFinance, CommonDbHelper objDbHelper)
        {
            string sql = "";
            try
            {
                if (objAutoJTFinance.JtFinanceId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtfinance SET
	                AutoLoanMasterId = {0}, 
                    DPISource = '{1}',
                    DPIAmount = {2},
                    DOISource = '{3}',
                    DOIAmount = {4},
                    EDRentAndUtilities = {5},
                    EDFoodAndClothing = {6},
                    EDEducation = {7},
                    EDLoanRePayment = {8},
                    EDOther = {9},
                    RepaymentTo = '{10}',
                    PaymentFor = '{11}' where JtFinanceId = {12}",
                    objAutoJTFinance.AutoLoanMasterId,
                    objAutoJTFinance.DPISource.Replace("'", "''"), objAutoJTFinance.DPIAmount, objAutoJTFinance.DOISource,
                    objAutoJTFinance.DOIAmount, objAutoJTFinance.EDRentAndUtilities, objAutoJTFinance.EDFoodAndClothing, objAutoJTFinance.EDEducation,
                    objAutoJTFinance.EDLoanRePayment, objAutoJTFinance.EDOther, objAutoJTFinance.RepaymentTo.Replace("'", "''"), objAutoJTFinance.PaymentFor,
                    objAutoJTFinance.JtFinanceId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_jtfinance
	                (AutoLoanMasterId, DPISource, DPIAmount, DOISource, 
	                DOIAmount, EDRentAndUtilities, EDFoodAndClothing, EDEducation, 
	                EDLoanRePayment, EDOther, RepaymentTo, PaymentFor)
                    values
                    ({0}, '{1}', {2}, 
                    '{3}', {4}, {5}, {6}, {7}, 
                    {8}, {9}, '{10}', '{11}')", objAutoJTFinance.AutoLoanMasterId,
                    objAutoJTFinance.DPISource.Replace("'", "''"), objAutoJTFinance.DPIAmount, objAutoJTFinance.DOISource,
                    objAutoJTFinance.DOIAmount, objAutoJTFinance.EDRentAndUtilities, objAutoJTFinance.EDFoodAndClothing, objAutoJTFinance.EDEducation,
                    objAutoJTFinance.EDLoanRePayment, objAutoJTFinance.EDOther, objAutoJTFinance.RepaymentTo.Replace("'", "''"), objAutoJTFinance.PaymentFor);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoPRFinance(AutoPRFinance objAutoPRFinance, CommonDbHelper objDbHelper)
        {
            string sql = "";
            try
            {
                if (objAutoPRFinance.PrFinanceId != 0)
                {
                    sql = String.Format(@"UPDATE auto_prfinance SET
	                AutoLoanMasterId = {0}, 
                    DPISource = '{1}',
                    DPIAmount = {2},
                    DOISource = '{3}',
                    DOIAmount = {4},
                    EDRentAndUtilities = {5},
                    EDFoodAndClothing = {6},
                    EDEducation = {7},
                    EDLoanRePayment = {8},
                    EDOther = {9},
                    RepaymentTo = '{10}',
                    PaymentFor = '{11}' where PrFinanceId = {12}",
                    objAutoPRFinance.AutoLoanMasterId,
                    objAutoPRFinance.DPISource.Replace("'", "''"), objAutoPRFinance.DPIAmount, objAutoPRFinance.DOISource,
                    objAutoPRFinance.DOIAmount, objAutoPRFinance.EDRentAndUtilities, objAutoPRFinance.EDFoodAndClothing, objAutoPRFinance.EDEducation,
                    objAutoPRFinance.EDLoanRePayment, objAutoPRFinance.EDOther, objAutoPRFinance.RepaymentTo.Replace("'", "''"), objAutoPRFinance.PaymentFor, objAutoPRFinance.PrFinanceId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_prfinance
	                (AutoLoanMasterId, DPISource, DPIAmount, DOISource, 
	                DOIAmount, EDRentAndUtilities, EDFoodAndClothing, EDEducation, 
	                EDLoanRePayment, EDOther, RepaymentTo, PaymentFor)
                    values
                    ({0}, '{1}', {2}, 
                    '{3}', {4}, {5}, {6}, {7}, 
                    {8}, {9}, '{10}', '{11}')", objAutoPRFinance.AutoLoanMasterId,
                    objAutoPRFinance.DPISource.Replace("'", "''"), objAutoPRFinance.DPIAmount, objAutoPRFinance.DOISource,
                    objAutoPRFinance.DOIAmount, objAutoPRFinance.EDRentAndUtilities, objAutoPRFinance.EDFoodAndClothing, objAutoPRFinance.EDEducation,
                    objAutoPRFinance.EDLoanRePayment, objAutoPRFinance.EDOther, objAutoPRFinance.RepaymentTo.Replace("'", "''"), objAutoPRFinance.PaymentFor);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoJtAccountDetail(AutoJTAccountDetail objAutoJtAccountDetailListMember, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                //                if (objAutoJtAccountDetailListMember.JtAccountDetailsId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_jtaccount_details SET
                //                    	AutoLoanMasterId = {0},
                //                        BankId = {1},
                //                        BranchId = {2}, 
                //                        AccountNumber = '{3}',
                //                        AccountCategory = '{4}',
                //                        AccountType = '{5}' where JtAccountDetailsId = {6}", objAutoJtAccountDetailListMember.AutoLoanMasterId,
                //                    objAutoJtAccountDetailListMember.BankId, objAutoJtAccountDetailListMember.BranchId,
                //                    objAutoJtAccountDetailListMember.AccountNumber, objAutoJtAccountDetailListMember.AccountCategory,
                //                    objAutoJtAccountDetailListMember.AccountType, objAutoJtAccountDetailListMember.JtAccountDetailsId);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql = String.Format(@"INSERT INTO auto_jtaccount_details
                    	(AutoLoanMasterId, BankId, BranchId, AccountNumber, 
	                    AccountCategory, AccountType)
                    values
                    ({0}, {1}, {2},'{3}','{4}','{5}')", objAutoJtAccountDetailListMember.AutoLoanMasterId,
                objAutoJtAccountDetailListMember.BankId, objAutoJtAccountDetailListMember.BranchId,
                objAutoJtAccountDetailListMember.AccountNumber.Replace("'", "''"), objAutoJtAccountDetailListMember.AccountCategory.Replace("'", "''"),
                objAutoJtAccountDetailListMember.AccountType.Replace("'", "''"));

                objDbHelper.ExecuteNonQuery(sql);
                //}
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoPRAccountDetail(AutoPRAccountDetail objAutoPRAccountDetailListMember, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                //                if (objAutoPRAccountDetailListMember.PrAccountDetailsId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_praccount_details SET
                //                    	AutoLoanMasterId = {0},
                //                        BankId = {1},
                //                        BranchId = {2}, 
                //                        AccountNumber = '{3}',
                //                        AccountCategory = '{4}',
                //                        AccountType = '{5}' where PrAccountDetailsId = {6}", objAutoPRAccountDetailListMember.AutoLoanMasterId,
                //                    objAutoPRAccountDetailListMember.BankId, objAutoPRAccountDetailListMember.BranchId,
                //                    objAutoPRAccountDetailListMember.AccountNumber, objAutoPRAccountDetailListMember.AccountCategory,
                //                    objAutoPRAccountDetailListMember.AccountType, objAutoPRAccountDetailListMember.PrAccountDetailsId);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql = String.Format(@"INSERT INTO auto_praccount_details
                    	(AutoLoanMasterId, BankId, BranchId, AccountNumber, 
	                    AccountCategory, AccountType)
                    values
                    ({0}, {1}, {2},'{3}','{4}','{5}')", objAutoPRAccountDetailListMember.AutoLoanMasterId,
                objAutoPRAccountDetailListMember.BankId, objAutoPRAccountDetailListMember.BranchId,
                objAutoPRAccountDetailListMember.AccountNumber.Replace("'", "''"), objAutoPRAccountDetailListMember.AccountCategory.Replace("'", "''"), objAutoPRAccountDetailListMember.AccountType.Replace("'", "''"));

                objDbHelper.ExecuteNonQuery(sql);
                //}
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private int SaveAutoJTAccount(AutoJTAccount objAutoJTAccount, CommonDbHelper objDbHelper)
        {
            var returnValue = 0;
            string sql = "";

            try
            {
                if (objAutoJTAccount.jtAccountId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtaccount SET
                    AutoLoanMasterId = {0},
                    AccountNumberForSCB = '{1}',
                    CreditCardNumberForSCB = '{2}' where jtAccountId = {3}",
                    objAutoJTAccount.AutoLoanMasterId,
                    objAutoJTAccount.AccountNumberForSCB.Replace("'", "''"), objAutoJTAccount.CreditCardNumberForSCB.Replace("'", "''"), objAutoJTAccount.jtAccountId);

                    objDbHelper.ExecuteNonQuery(sql);
                    returnValue = objAutoJTAccount.jtAccountId;
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_jtaccount
                    (AutoLoanMasterId, AccountNumberForSCB, CreditCardNumberForSCB) 
                    values
                    ({0}, '{1}', '{2}')", objAutoJTAccount.AutoLoanMasterId,
                    objAutoJTAccount.AccountNumberForSCB, objAutoJTAccount.CreditCardNumberForSCB);

                    objDbHelper.ExecuteNonQuery(sql);

                    //get the jtAccountId
                    sql = String.Format(@"Select jtAccountId from auto_jtaccount where 
                                        AutoLoanMasterId={0} and 
                                        AccountNumberForSCB='{1}' and 
                                        CreditCardNumberForSCB='{2}'", objAutoJTAccount.AutoLoanMasterId,
                    objAutoJTAccount.AccountNumberForSCB.Replace("'", "''"), objAutoJTAccount.CreditCardNumberForSCB.Replace("'", "''"));

                    IDataReader reader = objDbHelper.GetDataReader(sql);
                    while (reader.Read())
                    {
                        returnValue = Convert.ToInt32(reader.GetValue(0));
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return returnValue;
        }

        private int SaveAutoPRAccount(AutoPRAccount objAutoPRAccount, CommonDbHelper objDbHelper)
        {
            var returnValue = 0;
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoPRAccount.PrAccountId != 0)
                {
                    sql = String.Format(@"UPDATE auto_praccount SET
                    AutoLoanMasterId = {0},
                    AccountNumberForSCB = '{1}',
                    CreditCardNumberForSCB = '{2}' where PrAccountId = {3}",
                    objAutoPRAccount.AutoLoanMasterId,
                    objAutoPRAccount.AccountNumberForSCB.Replace("'", "''"), objAutoPRAccount.CreditCardNumberForSCB.Replace("'", "''"), objAutoPRAccount.PrAccountId);

                    objDbHelper.ExecuteNonQuery(sql);
                    returnValue = objAutoPRAccount.PrAccountId;
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_praccount
                    (AutoLoanMasterId, AccountNumberForSCB, CreditCardNumberForSCB) 
                    values
                    ({0}, '{1}', '{2}')", objAutoPRAccount.AutoLoanMasterId,
                    objAutoPRAccount.AccountNumberForSCB.Replace("'", "''"), objAutoPRAccount.CreditCardNumberForSCB.Replace("'", "''"));

                    //get recent inserted ID
                    objDbHelper.ExecuteNonQuery(sql);
                    sql = String.Format(@"Select PrAccountId from auto_praccount where 
                                        AutoLoanMasterId={0} and 
                                        AccountNumberForSCB='{1}' and 
                                        CreditCardNumberForSCB='{2}'", objAutoPRAccount.AutoLoanMasterId,
                    objAutoPRAccount.AccountNumberForSCB.Replace("'", "''"), objAutoPRAccount.CreditCardNumberForSCB.Replace("'", "''"));
                    IDataReader reader = objDbHelper.GetDataReader(sql);
                    while (reader.Read())
                    {
                        returnValue = Convert.ToInt32(reader.GetValue(0));
                    }
                    reader.Close();

                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return returnValue;
        }

        private int SaveNewVendor(AutoVendor objAutoVendor, CommonDbHelper objDbHelper)
        {
            var returnValue = 0;
            string sql = "";

            try
            {
                sql = String.Format(@"INSERT INTO auto_vendor
                    (VendorName, DealerType) 
                    values
                    ('{0}', {1})", objAutoVendor.VendorName,
                objAutoVendor.DealerType);

                objDbHelper.ExecuteNonQuery(sql);

                //get the jtAccountId
                sql = String.Format(@"Select AutoVendorId from auto_vendor where 
                                        VendorName='{0}' and 
                                        DealerType={1}", objAutoVendor.VendorName.Replace("'", "''"), objAutoVendor.DealerType);

                IDataReader reader = objDbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    returnValue = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return returnValue;
        }

        private void SaveAutoLoanVehicle(AutoLoanVehicle objAutoLoanVehicle, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoLoanVehicle.AutoLoanVehicleId != 0)
                {
                    sql = String.Format(@"UPDATE auto_loanvehicle SET
                    AutoLoanMasterId = {0}, 
                    VendorId = {1},
                    Auto_ManufactureId = {2},
                    Model = '{3}',
                    TrimLevel = '{4}',
                    EngineCC = '{5}',
                    VehicleType = '{6}',
                    VehicleStatus = {7},
                    ManufacturingYear = {8},
                    QuotedPrice = {9},
                    VerifiedPrice = {10},
                    PriceConsideredBasedOn = '{11}',
                    SeatingCapacity = '{12}',
                    CarVerification  = '{13}',VehicleId  = '{15}' where AutoLoanVehicleId = {14}",
                    objAutoLoanVehicle.AutoLoanMasterId,
                    objAutoLoanVehicle.VendorId, objAutoLoanVehicle.Auto_ManufactureId,
                    objAutoLoanVehicle.Model.Replace("'", "''"), objAutoLoanVehicle.TrimLevel.Replace("'", "''"), objAutoLoanVehicle.EngineCC, objAutoLoanVehicle.VehicleType.Replace("'", "''"),
                    objAutoLoanVehicle.VehicleStatus, objAutoLoanVehicle.ManufacturingYear,
                    objAutoLoanVehicle.QuotedPrice, objAutoLoanVehicle.VerifiedPrice, objAutoLoanVehicle.PriceConsideredBasedOn, objAutoLoanVehicle.SeatingCapacity,
                    objAutoLoanVehicle.CarVerification, objAutoLoanVehicle.AutoLoanVehicleId, objAutoLoanVehicle.VehicleId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_loanvehicle
                    (AutoLoanMasterId, VendorId, Auto_ManufactureId, 
	                Model, TrimLevel, EngineCC, VehicleType, VehicleStatus, ManufacturingYear, 
	                QuotedPrice, VerifiedPrice, PriceConsideredBasedOn, SeatingCapacity, 
	                CarVerification,VehicleId)
                    values
                    ({0}, {1}, {2}, 
                    '{3}', '{4}', '{5}', '{6}', {7}, 
                    {8}, {9}, {10}, '{11}', 
                    '{12}', '{13}',{14})", objAutoLoanVehicle.AutoLoanMasterId,
                    objAutoLoanVehicle.VendorId, objAutoLoanVehicle.Auto_ManufactureId,
                    objAutoLoanVehicle.Model.Replace("'", "''"), objAutoLoanVehicle.TrimLevel.Replace("'", "''"), objAutoLoanVehicle.EngineCC, objAutoLoanVehicle.VehicleType,
                    objAutoLoanVehicle.VehicleStatus, objAutoLoanVehicle.ManufacturingYear,
                    objAutoLoanVehicle.QuotedPrice, objAutoLoanVehicle.VerifiedPrice, objAutoLoanVehicle.PriceConsideredBasedOn, objAutoLoanVehicle.SeatingCapacity,
                    objAutoLoanVehicle.CarVerification, objAutoLoanVehicle.VehicleId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoJTProfession(AutoJTProfession objAutoJTProfession, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoJTProfession.JtProfessionId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtprofession SET
                    AutoLoanMasterId = {0}, 
                    PrimaryProfession =  {1}, 
                    OtherProfession =  {23}, 
                    MonthinCurrentProfession = '{2}',
                    NameofCompany = {3},
                    NameofOrganization = '{4}',
                    Designation = '{5}',
                    NatureOfBussiness = '{6}',
                    YearsInBussiness = '{7}',
                    OfficeStatus = '{8}',
                    Address = '{9}',
                    OfficePhone = '{10}',
                    OwnerShipType = '{11}',
                    EmploymentStatus = '{12}',
                    TotalProfessionalExperience = '{13}',
                    NumberOfEmployees = {14},
                    EquityShare = {15}, 
                    JtIncomeSource = '{16}',
                    OtherIncomeSource = '{17}',
                    NoOfFloorsRented = {18},
                    NatureOfrentedFloors = '{19}',
                    RentedAresInSFT = '{20}',
                    ConstructionCompletingYear = '{21}' where JtProfessionId = {22}", objAutoJTProfession.AutoLoanMasterId,
                    objAutoJTProfession.PrimaryProfession, objAutoJTProfession.MonthinCurrentProfession.Replace("'", "''"),
                    objAutoJTProfession.NameofCompany, objAutoJTProfession.NameofOrganization, objAutoJTProfession.Designation, objAutoJTProfession.NatureOfBussiness.Replace("'", "''"),
                    objAutoJTProfession.YearsInBussiness, objAutoJTProfession.OfficeStatus.Replace("'", "''"), objAutoJTProfession.Address.Replace("'", "''"), objAutoJTProfession.OfficePhone.Replace("'", "''"), objAutoJTProfession.OwnerShipType.Replace("'", "''"),
                    objAutoJTProfession.EmploymentStatus.Replace("'", "''"), objAutoJTProfession.TotalProfessionalExperience, objAutoJTProfession.NumberOfEmployees,
                    objAutoJTProfession.EquityShare, objAutoJTProfession.JtIncomeSource, objAutoJTProfession.OtherIncomeSource, objAutoJTProfession.NoOfFloorsRented,
                    objAutoJTProfession.NatureOfrentedFloors.Replace("'", "''"), objAutoJTProfession.RentedAresInSFT.Replace("'", "''"), objAutoJTProfession.ConstructionCompletingYear,
                    objAutoJTProfession.JtProfessionId, objAutoJTProfession.OtherProfession);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else if (objAutoJTProfession.PrimaryProfession != -1)
                {
                    sql = String.Format(@"INSERT INTO auto_jtprofession
                    	(AutoLoanMasterId, PrimaryProfession, OtherProfession, 
	                    MonthinCurrentProfession, NameofCompany, NameofOrganization, 
	                    Designation, NatureOfBussiness, YearsInBussiness, OfficeStatus, 
	                    Address, OfficePhone, OwnerShipType, EmploymentStatus, TotalProfessionalExperience, 
	                    NumberOfEmployees, EquityShare, JtIncomeSource, OtherIncomeSource, 
	                    NoOfFloorsRented, NatureOfrentedFloors, RentedAresInSFT, ConstructionCompletingYear
	                    )

                    values
                    ({0}, {1},{22}, '{2}', 
                    {3}, '{4}', '{5}', '{6}', '{7}', 
                    '{8}', '{9}', '{10}', '{11}', 
                    '{12}', '{13}', {14}, {15}, 
                    '{16}', '{17}', {18}, '{19}', 
                    '{20}', '{21}')", objAutoJTProfession.AutoLoanMasterId,
                    objAutoJTProfession.PrimaryProfession, objAutoJTProfession.MonthinCurrentProfession,
                    objAutoJTProfession.NameofCompany, objAutoJTProfession.NameofOrganization.Replace("'", "''"), objAutoJTProfession.Designation.Replace("'", "''"), objAutoJTProfession.NatureOfBussiness.Replace("'", "''"),
                    objAutoJTProfession.YearsInBussiness, objAutoJTProfession.OfficeStatus, objAutoJTProfession.Address.Replace("'", "''"), objAutoJTProfession.OfficePhone.Replace("'", "''"), objAutoJTProfession.OwnerShipType.Replace("'", "''"),
                    objAutoJTProfession.EmploymentStatus, objAutoJTProfession.TotalProfessionalExperience, objAutoJTProfession.NumberOfEmployees,
                    objAutoJTProfession.EquityShare, objAutoJTProfession.JtIncomeSource, objAutoJTProfession.OtherIncomeSource, objAutoJTProfession.NoOfFloorsRented,
                    objAutoJTProfession.NatureOfrentedFloors, objAutoJTProfession.RentedAresInSFT.Replace("'", "''"), objAutoJTProfession.ConstructionCompletingYear, objAutoJTProfession.OtherProfession);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoPRProfession(AutoPRProfession objAutoPRProfession, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoPRProfession.PrProfessionId != 0)
                {
                    sql = String.Format(@"UPDATE auto_prprofession SET
                    AutoLoanMasterId = {0}, 
                    PrimaryProfession =  {1}, 
                    OtherProfession =  {23}, 
                    MonthinCurrentProfession = '{2}',
                    NameofCompany = {3},
                    NameofOrganization = '{4}',
                    Designation = '{5}',
                    NatureOfBussiness = '{6}',
                    YearsInBussiness = '{7}',
                    OfficeStatus = '{8}',
                    Address = '{9}',
                    OfficePhone = '{10}',
                    OwnerShipType = '{11}',
                    EmploymentStatus = '{12}',
                    TotalProfessionalExperience = '{13}',
                    NumberOfEmployees = {14},
                    EquityShare = {15}, 
                    PrIncomeSource = '{16}',
                    OtherIncomeSource = '{17}',
                    NoOfFloorsRented = {18},
                    NatureOfrentedFloors = '{19}',
                    RentedAresInSFT = '{20}',
                    ConstructionCompletingYear = '{21}' where PrProfessionId = {22}", objAutoPRProfession.AutoLoanMasterId,
                    objAutoPRProfession.PrimaryProfession, objAutoPRProfession.MonthinCurrentProfession,
                    objAutoPRProfession.NameofCompany, objAutoPRProfession.NameofOrganization.Replace("'", "''"), objAutoPRProfession.Designation.Replace("'", "''"), objAutoPRProfession.NatureOfBussiness.Replace("'", "''"),
                    objAutoPRProfession.YearsInBussiness, objAutoPRProfession.OfficeStatus, objAutoPRProfession.Address.Replace("'", "''"), objAutoPRProfession.OfficePhone.Replace("'", "''"), objAutoPRProfession.OwnerShipType.Replace("'", "''"),
                    objAutoPRProfession.EmploymentStatus, objAutoPRProfession.TotalProfessionalExperience, objAutoPRProfession.NumberOfEmployees,
                    objAutoPRProfession.EquityShare, objAutoPRProfession.PrIncomeSource, objAutoPRProfession.OtherIncomeSource, objAutoPRProfession.NoOfFloorsRented,
                    objAutoPRProfession.NatureOfrentedFloors, objAutoPRProfession.RentedAresInSFT.Replace("'", "''"), objAutoPRProfession.ConstructionCompletingYear,
                    objAutoPRProfession.PrProfessionId, objAutoPRProfession.OtherProfession);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_prprofession
                    (AutoLoanMasterId, PrimaryProfession,OtherProfession, MonthinCurrentProfession, 
                    NameofCompany, NameofOrganization, Designation, NatureOfBussiness, 
                    YearsInBussiness, OfficeStatus, Address, OfficePhone, OwnerShipType, 
                    EmploymentStatus, TotalProfessionalExperience, NumberOfEmployees, 
                    EquityShare, PrIncomeSource, OtherIncomeSource, NoOfFloorsRented, 
                    NatureOfrentedFloors, RentedAresInSFT, ConstructionCompletingYear
                    )
                    values
                    ({0}, {1},{22}, '{2}', 
                    {3}, '{4}', '{5}', '{6}', '{7}', 
                    '{8}', '{9}', '{10}', '{11}', 
                    '{12}', '{13}', {14}, {15}, 
                    '{16}', '{17}', {18}, '{19}', 
                    '{20}', '{21}')", objAutoPRProfession.AutoLoanMasterId,
                    objAutoPRProfession.PrimaryProfession, objAutoPRProfession.MonthinCurrentProfession,
                    objAutoPRProfession.NameofCompany, objAutoPRProfession.NameofOrganization.Replace("'", "''"), objAutoPRProfession.Designation.Replace("'", "''"), objAutoPRProfession.NatureOfBussiness.Replace("'", "''"),
                    objAutoPRProfession.YearsInBussiness, objAutoPRProfession.OfficeStatus, objAutoPRProfession.Address, objAutoPRProfession.OfficePhone.Replace("'", "''"), objAutoPRProfession.OwnerShipType.Replace("'", "''"),
                    objAutoPRProfession.EmploymentStatus, objAutoPRProfession.TotalProfessionalExperience, objAutoPRProfession.NumberOfEmployees,
                    objAutoPRProfession.EquityShare, objAutoPRProfession.PrIncomeSource, objAutoPRProfession.OtherIncomeSource, objAutoPRProfession.NoOfFloorsRented,
                    objAutoPRProfession.NatureOfrentedFloors, objAutoPRProfession.RentedAresInSFT.Replace("'", "''"), objAutoPRProfession.ConstructionCompletingYear,
                    objAutoPRProfession.OtherProfession);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoJTApplicant(AutoJTApplicant objAutoJTApplicant, CommonDbHelper objDbHelper)
        {
            string sql = "";
            //var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
            //                 DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            var birthDate = objAutoJTApplicant.DateofBirth.ToString("yyyy-MM-dd");
            try
            {
                if (objAutoJTApplicant.JtApplicantId != 0)
                {
                    sql = String.Format(@"UPDATE auto_jtapplicant SET
Auto_LoanMasterId = {0}, 
JtName = '{1}',
RelationShipNumber, '{2}',
TINNumber = '{3}',
FathersName = '{4}',
MothersName = '{5}',
DateofBirth =  '{6}',
MaritalStatus = '{7}',
SpouseName = '{8}',
SpouseProfession = '{9}',
SpouseWorkAddress = '{10}',
SpouseContactNumber = '{11}',
RelationShipwithPrimary = '{12}',
Nationality = '{13}',
IdentificationNumberType = '{27}',
IdentificationNumber = '{14}',
IdentificationDocument = '{28}',
NumberofDependents = '{15}',
HighestEducation = '{16}',
ResidenceAddress = '{17}',
MailingAddress = '{18}',
PermanentAddress = '{19}',
ResidenceStatus = '{20}',
OwnershipDocument = '{29}',
ResidenceStatusYear = '{21}',
PhoneResidence = '{22}',
Mobile = '{23}',
Email = '{24}',
DirectorshipwithanyBank = {25},
Gender = {30} where JtApplicantId = {26}", objAutoJTApplicant.Auto_LoanMasterId,
objAutoJTApplicant.JtName.Replace("'", "''"), objAutoJTApplicant.RelationShipNumber,
objAutoJTApplicant.TINNumber.Replace("'", "''"), objAutoJTApplicant.FathersName.Replace("'", "''"), objAutoJTApplicant.MothersName.Replace("'", "''"), birthDate, objAutoJTApplicant.MaritalStatus.Replace("'", "''"),
objAutoJTApplicant.SpouseName.Replace("'", "''"), objAutoJTApplicant.SpouseProfession, objAutoJTApplicant.SpouseWorkAddress.Replace("'", "''"), objAutoJTApplicant.SpouseContactNumber.Replace("'", "''"),
objAutoJTApplicant.RelationShipwithPrimary.Replace("'", "''"), objAutoJTApplicant.Nationality.Replace("'", "''"), objAutoJTApplicant.IdentificationNumber, objAutoJTApplicant.NumberofDependents.Replace("'", "''"),
objAutoJTApplicant.HighestEducation.Replace("'", "''"), objAutoJTApplicant.ResidenceAddress.Replace("'", "''"), objAutoJTApplicant.MailingAddress.Replace("'", "''"), objAutoJTApplicant.PermanentAddress.Replace("'", "''"),
objAutoJTApplicant.ResidenceStatus, objAutoJTApplicant.ResidenceStatusYear.Replace("'", "''"), objAutoJTApplicant.PhoneResidence.Replace("'", "''"), objAutoJTApplicant.Mobile.Replace("'", "''"),
objAutoJTApplicant.Email.Replace("'", "''"), objAutoJTApplicant.DirectorshipwithanyBank, objAutoJTApplicant.JtApplicantId, objAutoJTApplicant.IdentificationNumberType.Replace("'", "''"),
objAutoJTApplicant.IdentificationDocument.Replace("'", "''"), objAutoJTApplicant.OwnershipDocument.Replace("'", "''"), objAutoJTApplicant.Gender);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else if (objAutoJTApplicant.JtName != "")
                {
                    sql = String.Format(@"INSERT INTO auto_jtapplicant
	(Auto_LoanMasterId, JtName, RelationShipNumber, 
	TINNumber, FathersName, MothersName, DateofBirth, MaritalStatus, 
	SpouseName, SpouseProfession, SpouseWorkAddress, SpouseContactNumber, 
	RelationShipwithPrimary, Nationality, IdentificationNumberType, IdentificationNumber, IdentificationDocument, NumberofDependents, 
	HighestEducation, ResidenceAddress, MailingAddress, PermanentAddress, 
	ResidenceStatus,OwnershipDocument, ResidenceStatusYear, PhoneResidence, Mobile, 
	Email, DirectorshipwithanyBank, Gender)
	values
	({0}, '{1}', '{2}', 
	'{3}', '{4}', '{5}', '{6}', '{7}', 
	'{8}', '{9}', '{10}', '{11}', 
	'{12}', '{13}','{26}', '{14}','{27}', '{15}', 
	'{16}', '{17}', '{18}', '{19}', 
	'{20}','{28}', '{21}', '{22}', '{23}', 
	'{24}', {25},{29})", objAutoJTApplicant.Auto_LoanMasterId,
objAutoJTApplicant.JtName.Replace("'", "''"), objAutoJTApplicant.RelationShipNumber.Replace("'", "''"),
objAutoJTApplicant.TINNumber.Replace("'", "''"), objAutoJTApplicant.FathersName.Replace("'", "''"), objAutoJTApplicant.MothersName.Replace("'", "''"), birthDate.Replace("'", "''"), objAutoJTApplicant.MaritalStatus.Replace("'", "''"),
objAutoJTApplicant.SpouseName.Replace("'", "''"), objAutoJTApplicant.SpouseProfession.Replace("'", "''"), objAutoJTApplicant.SpouseWorkAddress.Replace("'", "''"), objAutoJTApplicant.SpouseContactNumber.Replace("'", "''"),
objAutoJTApplicant.RelationShipwithPrimary.Replace("'", "''"), objAutoJTApplicant.Nationality.Replace("'", "''"), objAutoJTApplicant.IdentificationNumber.Replace("'", "''"), objAutoJTApplicant.NumberofDependents.Replace("'", "''"),
objAutoJTApplicant.HighestEducation.Replace("'", "''"), objAutoJTApplicant.ResidenceAddress.Replace("'", "''"), objAutoJTApplicant.MailingAddress.Replace("'", "''"), objAutoJTApplicant.PermanentAddress.Replace("'", "''"),
objAutoJTApplicant.ResidenceStatus.Replace("'", "''"), objAutoJTApplicant.ResidenceStatusYear.Replace("'", "''"), objAutoJTApplicant.PhoneResidence.Replace("'", "''"), objAutoJTApplicant.Mobile.Replace("'", "''"),
objAutoJTApplicant.Email.Replace("'", "''"), objAutoJTApplicant.DirectorshipwithanyBank, objAutoJTApplicant.IdentificationNumberType.Replace("'", "''"),
objAutoJTApplicant.IdentificationDocument.Replace("'", "''"), objAutoJTApplicant.OwnershipDocument.Replace("'", "''"), objAutoJTApplicant.Gender);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        public int SaveAutoLoanMaster(AutoLoanMaster objAutoLoanMaster, CommonDbHelper dbHelper)
        {
            string sql = "";
            var returnValue = 0;
            //var appliedDate = objAutoLoanMaster.AppliedDate.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
            //                 DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            var appliedDate = objAutoLoanMaster.AppliedDate.ToString("yyyy-MM-dd");
            var receiveDate = objAutoLoanMaster.ReceiveDate.ToString("yyyy-MM-dd");
            try
            {
                if (objAutoLoanMaster.Auto_LoanMasterId != 0)
                {
                    sql = String.Format(@"UPDATE auto_loanmaster SET
                                        LLID={0}, 
                                        ProductId={1}, 
                                        Source='{2}', 
                                        SourceCode='{3}', 
                                        SourceName='{4}', 
                                        AppliedDate='{5}', 
                                        AppliedAmount={6}, 
                                        ReceiveDate='{7}', 
                                        AskingTenor='{8}', 
                                        JointApplication={9}, 
                                        ReferenceLLId={10},
                                        StatusID = {12},
                                        UserID = {13},
                                        AskingRate = {14}
                                        where Auto_LoanMasterId = {11}",
                                        objAutoLoanMaster.LLID,
    objAutoLoanMaster.ProductId, objAutoLoanMaster.Source, objAutoLoanMaster.SourceCode.Replace("'", "''"), objAutoLoanMaster.SourceName.Replace("'", "''"),
    appliedDate, objAutoLoanMaster.AppliedAmount, receiveDate, objAutoLoanMaster.AskingTenor,
    objAutoLoanMaster.JointApplication, objAutoLoanMaster.ReferenceLLId, objAutoLoanMaster.Auto_LoanMasterId,
    objAutoLoanMaster.StatusID, objAutoLoanMaster.UserID, objAutoLoanMaster.AskingRate, objAutoLoanMaster.ParentLLID);
                    //true, objAutoLoanMaster.ReferenceLLId, objAutoLoanMaster.Auto_LoanMasterId);

                    dbHelper.ExecuteNonQuery(sql);
                    returnValue = objAutoLoanMaster.Auto_LoanMasterId;
                }
                else
                {
                    string quary = string.Format(@"delete from auto_loanmaster where LLID={0}", objAutoLoanMaster.LLID);
                    dbHelper.ExecuteNonQuery(quary);
                    
                    sql = String.Format(@"INSERT INTO auto_loanmaster
	(LLID, ProductId, Source, SourceCode, SourceName, 
	AppliedDate, AppliedAmount, ReceiveDate, AskingTenor, JointApplication, 
	ReferenceLLId,StatusID,UserID,AskingRate, ParentLLID)
	values
	({0}, {1}, '{2}','{3}','{4}','{5}',{6},'{7}','{8}',{9},{10},{11},{12},{13},{14})", objAutoLoanMaster.LLID,
objAutoLoanMaster.ProductId, objAutoLoanMaster.Source.Replace("'", "''"), objAutoLoanMaster.SourceCode.Replace("'", "''"), objAutoLoanMaster.SourceName.Replace("'", "''"),
appliedDate, objAutoLoanMaster.AppliedAmount, receiveDate, objAutoLoanMaster.AskingTenor,
objAutoLoanMaster.JointApplication, objAutoLoanMaster.ReferenceLLId, objAutoLoanMaster.StatusID, objAutoLoanMaster.UserID, objAutoLoanMaster.AskingRate, objAutoLoanMaster.ParentLLID);
                    //true, objAutoLoanMaster.ReferenceLLId);

                    dbHelper.ExecuteNonQuery(sql);
                    sql = String.Format(@"Select Auto_LoanMasterId from auto_loanmaster where 
                                        LLID={0} and 
                                        ProductId={1} and 
                                        Source='{2}' and  
                                        SourceCode='{3}' and 
                                        SourceName='{4}' and 
                                        AppliedDate='{5}' and 
                                        AppliedAmount={6} and 
                                        ReceiveDate='{7}' and 
                                        AskingTenor='{8}' and  
                                        JointApplication={9} and  
                                        ReferenceLLId={10}", objAutoLoanMaster.LLID,
objAutoLoanMaster.ProductId, objAutoLoanMaster.Source, objAutoLoanMaster.SourceCode.Replace("'", "''"), objAutoLoanMaster.SourceName.Replace("'", "''"),
appliedDate, objAutoLoanMaster.AppliedAmount, receiveDate, objAutoLoanMaster.AskingTenor,
objAutoLoanMaster.JointApplication, objAutoLoanMaster.ReferenceLLId);
                    //true, objAutoLoanMaster.ReferenceLLId);
                    IDataReader reader = dbHelper.GetDataReader(sql);
                    while (reader.Read())
                    {
                        returnValue = Convert.ToInt32(reader.GetValue(0));
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                returnValue = 0;
                throw ex;
            }
            return returnValue;
        }

        public void SaveAutoPRApplicant(AutoPRApplicant objAutoPRApplicant, CommonDbHelper dbHelper)
        {
            string sql = "";
            //var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
            //                 DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            var birthDate = objAutoPRApplicant.DateofBirth.ToString("yyyy-MM-dd");
            try
            {
                if (objAutoPRApplicant.PrApplicantId != 0)
                {
                    sql = String.Format(@"UPDATE auto_prapplicant SET
Auto_LoanMasterId = {0}, 
PrName = '{1}',
RelationShipNumber = '{2}',
TINNumber = '{3}',
FathersName = '{4}',
MothersName = '{5}',
DateofBirth =  '{6}',
MaritalStatus = '{7}',
SpouseName = '{8}',
SpouseProfession = '{9}',
SpouseWorkAddress = '{10}',
SpouseContactNumber = '{11}',
RelationShipwithPrimary = '{12}',
Nationality = '{13}',
IdentificationNumberType = '{27}',
IdentificationNumber = '{14}',
IdentificationDocument = '{28}',
NumberofDependents = '{15}',
HighestEducation = '{16}',
ResidenceAddress = '{17}',
MailingAddress = '{18}',
PermanentAddress = '{19}',
ResidenceStatus = '{20}',
OwnershipDocument = '{29}',
ResidenceStatusYear = '{21}',
PhoneResidence = '{22}',
Mobile = '{23}',
Email = '{24}',
DirectorshipwithanyBank = {25},
Gender = {30} where PrApplicantId = {26}", objAutoPRApplicant.Auto_LoanMasterId,
objAutoPRApplicant.PrName.Replace("'", "''"), objAutoPRApplicant.RelationShipNumber,
objAutoPRApplicant.TINNumber.Replace("'", "''"), objAutoPRApplicant.FathersName.Replace("'", "''"), objAutoPRApplicant.MothersName.Replace("'", "''"), birthDate, objAutoPRApplicant.MaritalStatus.Replace("'", "''"),
objAutoPRApplicant.SpouseName.Replace("'", "''"), objAutoPRApplicant.SpouseProfession.Replace("'", "''"), objAutoPRApplicant.SpouseWorkAddress.Replace("'", "''"), objAutoPRApplicant.SpouseContactNumber.Replace("'", "''"),
objAutoPRApplicant.RelationShipwithPrimary, objAutoPRApplicant.Nationality.Replace("'", "''"), objAutoPRApplicant.IdentificationNumber.Replace("'", "''"), objAutoPRApplicant.NumberofDependents.Replace("'", "''"),
objAutoPRApplicant.HighestEducation.Replace("'", "''"), objAutoPRApplicant.ResidenceAddress.Replace("'", "''"), objAutoPRApplicant.MailingAddress.Replace("'", "''"), objAutoPRApplicant.PermanentAddress.Replace("'", "''"),
objAutoPRApplicant.ResidenceStatus.Replace("'", "''"), objAutoPRApplicant.ResidenceStatusYear.Replace("'", "''"), objAutoPRApplicant.PhoneResidence.Replace("'", "''"), objAutoPRApplicant.Mobile.Replace("'", "''"),
objAutoPRApplicant.Email.Replace("'", "''"), objAutoPRApplicant.DirectorshipwithanyBank, objAutoPRApplicant.PrApplicantId, objAutoPRApplicant.IdentificationNumberType.Replace("'", "''"),
objAutoPRApplicant.IdentificationDocument.Replace("'", "''"), objAutoPRApplicant.OwnershipDocument.Replace("'", "''"), objAutoPRApplicant.Gender);

                    dbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_prapplicant
	(Auto_LoanMasterId, PrName, RelationShipNumber, 
	TINNumber, FathersName, MothersName, DateofBirth, MaritalStatus, 
	SpouseName, SpouseProfession, SpouseWorkAddress, SpouseContactNumber, 
	RelationShipwithPrimary, Nationality, IdentificationNumberType, IdentificationNumber, IdentificationDocument, NumberofDependents, 
	HighestEducation, ResidenceAddress, MailingAddress, PermanentAddress, 
	ResidenceStatus, OwnershipDocument, ResidenceStatusYear, PhoneResidence, Mobile, 
	Email, DirectorshipwithanyBank,Gender)
	values
	({0}, '{1}', '{2}', 
	'{3}', '{4}', '{5}', '{6}', '{7}', 
	'{8}', '{9}', '{10}', '{11}', 
	'{12}', '{13}','{26}', '{14}','{27}', '{15}', 
	'{16}', '{17}', '{18}', '{19}', 
	'{20}','{28}', '{21}', '{22}', '{23}', 
	'{24}', {25}, {29})", objAutoPRApplicant.Auto_LoanMasterId,
objAutoPRApplicant.PrName.Replace("'", "''"), objAutoPRApplicant.RelationShipNumber.Replace("'", "''"),
objAutoPRApplicant.TINNumber.Replace("'", "''"), objAutoPRApplicant.FathersName.Replace("'", "''"), objAutoPRApplicant.MothersName.Replace("'", "''"), birthDate, objAutoPRApplicant.MaritalStatus.Replace("'", "''"),
objAutoPRApplicant.SpouseName.Replace("'", "''"), objAutoPRApplicant.SpouseProfession.Replace("'", "''"), objAutoPRApplicant.SpouseWorkAddress.Replace("'", "''"), objAutoPRApplicant.SpouseContactNumber.Replace("'", "''"),
objAutoPRApplicant.RelationShipwithPrimary.Replace("'", "''"), objAutoPRApplicant.Nationality.Replace("'", "''"), objAutoPRApplicant.IdentificationNumber.Replace("'", "''"), objAutoPRApplicant.NumberofDependents,
objAutoPRApplicant.HighestEducation.Replace("'", "''"), objAutoPRApplicant.ResidenceAddress.Replace("'", "''"), objAutoPRApplicant.MailingAddress.Replace("'", "''"), objAutoPRApplicant.PermanentAddress.Replace("'", "''"),
objAutoPRApplicant.ResidenceStatus.Replace("'", "''"), objAutoPRApplicant.ResidenceStatusYear, objAutoPRApplicant.PhoneResidence.Replace("'", "''"), objAutoPRApplicant.Mobile.Replace("'", "''"),
objAutoPRApplicant.Email.Replace("'", "''"), objAutoPRApplicant.DirectorshipwithanyBank, objAutoPRApplicant.IdentificationNumberType.Replace("'", "''"),
objAutoPRApplicant.IdentificationDocument.Replace("'", "''"), objAutoPRApplicant.OwnershipDocument.Replace("'", "''"), objAutoPRApplicant.Gender);

                    dbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                throw ex;
            }
        }

        #endregion Save data



        #region Get inforamtion

        public Object getLoanLocetorInfoByLLID(int llid, int userType)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();
            string sql = "select count(*) from auto_loanmaster where llid = " + llid;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSS(llid, userType);
            }
            else
            {
                dbHelper = new CommonDbHelper("conStringLoanLocator");
                sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                }
                if (isExistLLID > 0)
                {
                    returnValue = getDataFromLoanLocator(llid, userType);
                }
                else
                {
                    returnValue = "No Data";
                }
            }

            return returnValue;
        }

        public Object getLoanLocetorInfoByLLIDForOps(int llid, int userType)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();
            string sql = "select count(*) from auto_loanmaster where llid = " + llid;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSSForOps(llid, userType);
            }
            else
            {
                dbHelper = new CommonDbHelper("conStringLoanLocator");
                sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                }
                if (isExistLLID > 0)
                {
                    returnValue = getDataFromLoanLocator(llid, userType);
                }
                else
                {
                    returnValue = "No Data";
                }
            }

            return returnValue;
        }

        public Object getLoanLocetorInfoByLLID(int llid)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();
            string sql = "select count(*) from auto_loanmaster where llid = " + llid;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSS(llid);
            }
            else
            {
                dbHelper = new CommonDbHelper("conStringLoanLocator");
                sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                }
                if (isExistLLID > 0)
                {
                    returnValue = getDataFromLoanLocator(llid);
                }
                else
                {
                    returnValue = "No Data";
                }
            }

            return returnValue;
        }

        public Object getLoanLocetorInfoByLLIDForSales(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();
            string sql = "select count(*) from auto_loanmaster where llid = " + llid;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {

                isExistLLID = 0;
                dbHelper = new CommonDbHelper();

                sql = "select count(*) from auto_loanmaster where llid = " + llid + " and StatusId=42";
                reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
                dbHelper.Close();
                if (isExistLLID != 0)
                {
                    returnValue = getDataFromPLUSSForAutoSales(llid);
                }
                else
                {
                    returnValue = "No Data";
                }
            }
            else
            {
                dbHelper = new CommonDbHelper("conStringLoanLocator");
                sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                }
                if (isExistLLID > 0)
                {
                    returnValue = getDataFromLoanLocator(llid);
                }
                else
                {
                    returnValue = "No Data";
                }
            }

            return returnValue;
        }

        public Object getLoanLocetorInfoByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();

            string condition = "";

            if (statePermission.Count > 0)
            {
                condition += " and StatusID in (";
                foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                {
                    condition += autwf.WorkFlowID.ToString();

                    if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                    {
                        condition += ",";
                    }
                    else
                    {
                        condition += ")";
                    }
                }
            }
            string sql = "select count(*) from auto_loanmaster where llid = " + llid + condition;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSS(llid);
            }
            else
            {
                //dbHelper = new CommonDbHelper("conStringLoanLocator");
                //sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                //reader = dbHelper.GetDataReader(sql);
                //while (reader.Read())
                //{
                //    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                //}
                //if (isExistLLID > 0)
                //{
                //    returnValue = getDataFromLoanLocator(llid);
                //}
                //else
                //{
                //    returnValue = "No Data";
                //}
                returnValue = "No Data found.";
            }

            return returnValue;
        }

        public Object getLoanLocetorInfoByLLIDForCISupport(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();

            string condition = "";

            if (statePermission.Count > 0)
            {
                condition += " and StatusID in (";
                foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                {
                    condition += autwf.WorkFlowID.ToString();

                    if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                    {
                        condition += ",";
                    }
                    else
                    {
                        condition += ")";
                    }
                }
            }
            string sql = "select count(*) from auto_loanmaster where llid = " + llid + condition;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSSForCISupport(llid);
            }
            else
            {
                //dbHelper = new CommonDbHelper("conStringLoanLocator");
                //sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                //reader = dbHelper.GetDataReader(sql);
                //while (reader.Read())
                //{
                //    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                //}
                //if (isExistLLID > 0)
                //{
                //    returnValue = getDataFromLoanLocator(llid);
                //}
                //else
                //{
                //    returnValue = "No Data";
                //}
                returnValue = "No Data found.";
            }

            return returnValue;
        }


        public Object getLoanLocetorInfoByLLIDForSalesAndOps(int llid)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();
            string sql = "select count(*) from auto_loanmaster where llid = " + llid;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSSForAutoSales(llid);
            }
            else
            {
                dbHelper = new CommonDbHelper("conStringLoanLocator");
                sql = "select count(*) from loan_app_info where LOAN_APPL_ID = " + llid;
                reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    isExistLLID = Convert.ToInt32(reader.GetValue(0));
                }
                if (isExistLLID > 0)
                {
                    returnValue = getDataFromLoanLocator(llid);
                }
                else
                {
                    returnValue = "No Data";
                }
            }

            return returnValue;
        }


        private Object getDataFromPLUSSForAutoSales(int llid)
        {
            
            var autoLoanApplicationAll = new AutoLoanApplicationAll();
            var autoLoanMasterID = 0;
            var PRaccountID = 0;
            var JTaccountID = 0;
            var AutoVendorID = 0;

            var objDbHelper = new CommonDbHelper();
            string queryString = "";
            try
            {
                #region Loan Application

                #region AutoLoanMaster
                var autoLoanMaster = new AutoLoanMaster();

                queryString = String.Format(@"Select * from auto_loanmaster where LLID = {0}", llid);

                IDataReader reader = objDbHelper.GetDataReader(queryString);

                var autoLoanMasterDataReader = new AutoLoanMasterDataReader(reader);
                while (reader.Read())
                    autoLoanMaster = autoLoanMasterDataReader.Read();
                reader.Close();
                autoLoanMasterID = autoLoanMaster.Auto_LoanMasterId;
                //AutoLoanMaster Close
                //autoLoanMaster.UserType = userType;

                autoLoanApplicationAll.objAutoLoanMaster = autoLoanMaster;

                #endregion

                #region AutoPRApplicant

                var autoPRApplicant = new AutoPRApplicant();
                queryString = String.Format(@"Select * from auto_prapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoPRApplicantDataReader = new AutoPRApplicantDataReader(reader);
                while (reader.Read())
                    autoPRApplicant = autoPRApplicantDataReader.Read();
                reader.Close();
                // AutoPRApplican close

                autoLoanApplicationAll.objAutoPRApplicant = autoPRApplicant;

                #endregion AutoPRApplicant

                #region AutoJTApplicant

                var autoJTApplicant = new AutoJTApplicant();


                if (autoLoanMaster.JointApplication == true)
                {
                    queryString = String.Format(@"Select * from auto_jtapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
                    reader = objDbHelper.GetDataReader(queryString);
                    var autoJTApplicantDataReader = new AutoJTApplicantDataReader(reader);
                    while (reader.Read())
                        autoJTApplicant = autoJTApplicantDataReader.Read();
                    reader.Close();
                }
                autoLoanApplicationAll.objAutoJTApplicant = autoJTApplicant;

                #endregion AutoJTApplicant

                #region AutoPRProfession

                var autoPRProfession = new AutoPRProfession();
                queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'PrPrimaryProfessionName',pluss2.PROF_NAME 'PrOtherProfessionName' 
                                        from auto_prprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
					                    left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID
                                        where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoPRProfessionDataReader = new AutoPRProfessionDataReader(reader);
                while (reader.Read())
                    autoPRProfession = autoPRProfessionDataReader.Read();
                reader.Close();

                autoLoanApplicationAll.objAutoPRProfession = autoPRProfession;

                #endregion AutoPRProfession

                #region AutoJTProfession
                var autoJTProfession = new AutoJTProfession();
                if (autoLoanMaster.JointApplication == true)
                {
                    queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'JtPrimaryProfessionName',pluss2.PROF_NAME 'JtOtherProfessionName'
                                        from auto_jtprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
                                        left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID 
                                        where auto.AutoLoanMasterId = {0}", autoLoanMasterID);
                    reader = objDbHelper.GetDataReader(queryString);
                    var autoJTProfessionDataReader = new AutoJTProfessionDataReader(reader);
                    while (reader.Read())
                        autoJTProfession = autoJTProfessionDataReader.Read();
                    reader.Close();
                    // AutoJTProfession close
                }
                autoLoanApplicationAll.objAutoJTProfession = autoJTProfession;

                #endregion AutoJTProfession

                #region AutoLoanVehicle
                //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1),FALSE) as ValuePackAllowed
                var autoLoanVehicle = new AutoLoanVehicle();
                queryString = String.Format(@"SELECT auto_loanvehicle.*,
auto_status.StatusCode,
auto_menufacturer.ManufacturerCode,
auto_vehicle.ModelCode,
auto_vehicle.VehicleTypeCode,
auto_vendor.VendorCode,

IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
auto_brandtenorltv.ValuePackAllowed,
auto_brandtenorltv.Tenor,
auto_brandtenorltv.LTV
FROM
auto_loanvehicle
left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.VehicleId
INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
LEFT OUTER JOIN auto_brandtenorltv on auto_brandtenorltv.VehicleId = auto_vehicle.VehicleId and auto_brandtenorltv.StatusId = auto_loanvehicle.VehicleStatus
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
                while (reader.Read())
                    autoLoanVehicle = autoLoanVehicleDataReader.Read();
                reader.Close();
                AutoVendorID = autoLoanVehicle.VendorId;

                autoLoanApplicationAll.objAutoLoanVehicle = autoLoanVehicle;

                #endregion AutoLoanVehicle

                #region AutoPRAccount
                var autoPRAccount = new AutoPRAccount();
                queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
                while (reader.Read())
                    autoPRAccount = autoPRAccountDataReader.Read();
                reader.Close();
                PRaccountID = autoPRAccount.PrAccountId;
                // AutoPRAccount close

                autoLoanApplicationAll.objAutoPRAccount = autoPRAccount;

                #endregion AutoPRAccount

                #region AutoJTAccount
                var autoJTAccount = new AutoJTAccount();
                if (autoLoanMaster.JointApplication == true)
                {
                    queryString = String.Format(@"Select * from auto_jtaccount where AutoLoanMasterId = {0}", autoLoanMasterID);
                    reader = objDbHelper.GetDataReader(queryString);
                    var autoJTAccountDataReader = new AutoJTAccountDataReader(reader);
                    while (reader.Read())
                        autoJTAccount = autoJTAccountDataReader.Read();
                    reader.Close();
                    JTaccountID = autoJTAccount.jtAccountId;
                    // AutoJTAccount close
                }
                autoLoanApplicationAll.objAutoJTAccount = autoJTAccount;

                #endregion AutoJTAccount

                #region AutoPRAccountDetail
                List<AutoPRAccountDetail> AutoPRAccountDetailList = new List<AutoPRAccountDetail>();
                var autoPRAccountDetail = new AutoPRAccountDetail();
                queryString = String.Format(@"Select * from auto_praccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoPRAccountDetailDataReader = new AutoPRAccountDetailDataReader(reader);
                while (reader.Read())
                {
                    autoPRAccountDetail = autoPRAccountDetailDataReader.Read();
                    AutoPRAccountDetailList.Add(autoPRAccountDetail);
                }
                reader.Close();
                // AutoPRAccountDetail close

                autoLoanApplicationAll.objAutoPRAccountDetailList = AutoPRAccountDetailList;
                #endregion AutoPRAccountDetail

                #region AutoJTAccountDetail

                //
                List<AutoJTAccountDetail> autoJTAccountDetailList = new List<AutoJTAccountDetail>();
                if (autoLoanMaster.JointApplication == true)
                {
                    var autoJTAccountDetail = new AutoJTAccountDetail();
                    queryString = String.Format(@"Select * from auto_jtaccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
                    reader = objDbHelper.GetDataReader(queryString);
                    var autoJTAccountDetailDataReader = new AutoJTAccountDetailDataReader(reader);
                    while (reader.Read())
                    {
                        autoJTAccountDetail = autoJTAccountDetailDataReader.Read();
                        autoJTAccountDetailList.Add(autoJTAccountDetail);
                    }
                    reader.Close();
                }
                // AutoJTAccountDetail close

                autoLoanApplicationAll.objAutoJtAccountDetailList = autoJTAccountDetailList;

                #endregion AutoJTAccountDetail

                #region objAutoPRFinance
                //
                var autoPRFinance = new AutoPRFinance();
                queryString = String.Format(@"Select * from auto_prfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoPRFinanceDataReader = new AutoPRFinanceDataReader(reader);
                while (reader.Read())
                    autoPRFinance = autoPRFinanceDataReader.Read();
                reader.Close();
                // objAutoPRFinance close

                autoLoanApplicationAll.objAutoPRFinance = autoPRFinance;

                #endregion objAutoPRFinance

                #region objAutoJTFinance
                //
                var autoJTFinance = new AutoJTFinance();
                if (autoLoanMaster.JointApplication == true)
                {
                    queryString = String.Format(@"Select * from auto_jtfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
                    reader = objDbHelper.GetDataReader(queryString);
                    var autoJTFinanceDataReader = new AutoJTFinanceDataReader(reader);
                    while (reader.Read())
                        autoJTFinance = autoJTFinanceDataReader.Read();
                    reader.Close();
                    // objAutoJTFinance close
                }
                autoLoanApplicationAll.objAutoJTFinance = autoJTFinance;

                #endregion objAutoJTFinance

                #region objAutoLoanReference
                //
                var autoLoanReference = new AutoLoanReference();
                queryString = String.Format(@"Select * from auto_loanreference where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoLoanReferenceDataReader = new AutoLoanReferenceDataReader(reader);
                while (reader.Read())
                    autoLoanReference = autoLoanReferenceDataReader.Read();
                reader.Close();
                // objAutoLoanReference close

                autoLoanApplicationAll.objAutoLoanReference = autoLoanReference;

                #endregion objAutoLoanReference

                #region objAutoLoanFacilityList
                //
                List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
                var autoLoanFacility = new AutoLoanFacility();
                queryString = String.Format(@"SELECT
auto_loanfacility.FacilityId,
auto_loanfacility.AutoLoanMasterId,
auto_loanfacility.FacilityType,
auto_loanfacility.InterestRate,
auto_loanfacility.PresentBalance,
auto_loanfacility.PresentEMI,
auto_loanfacility.PresentLimit,
auto_loanfacility.ProposedLimit,
auto_loanfacility.RepaymentAgrement,
auto_loanfacility.Consider,
auto_loanfacility.EMIPercent,
auto_loanfacility.EMIShare,
t_pluss_facility.FACI_NAME,
auto_loansecurity.NatureOfSecurity,
auto_loansecurity.FaceValue,
auto_loansecurity.XTVRate,
auto_loansecurity.FacilityType,
auto_loansecurity.`Status`
FROM
auto_loanfacility
INNER JOIN t_pluss_facility ON auto_loanfacility.FacilityType = t_pluss_facility.FACI_ID
LEFT OUTER JOIN auto_loansecurity ON auto_loanfacility.AutoLoanMasterId = auto_loansecurity.AutoLoanMasterId AND t_pluss_facility.FACI_ID = auto_loansecurity.FacilityType AND auto_loansecurity.FacilityId = auto_loanfacility.FacilityId
where auto_loanfacility.AutoLoanMasterId ={0}", autoLoanMasterID);



                reader = objDbHelper.GetDataReader(queryString);
                var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
                while (reader.Read())
                {
                    autoLoanFacility = autoLoanFacilityDataReader.Read();
                    autoLoanFacilityList.Add(autoLoanFacility);
                }
                reader.Close();
                // objAutoLoanFacilityList close

                autoLoanApplicationAll.objAutoLoanFacilityList = autoLoanFacilityList;

                #endregion objAutoLoanFacilityList

                #region objAutoLoanSecurityList
                //
                List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();
                var autoLoanSecurity = new AutoLoanSecurity();
                queryString = String.Format(@"Select * from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoLoanSecurityDataReader = new AutoLoanSecurityDataReader(reader);
                while (reader.Read())
                {
                    autoLoanSecurity = autoLoanSecurityDataReader.Read();
                    autoLoanSecurityList.Add(autoLoanSecurity);
                }
                reader.Close();
                // objAutoLoanSecurityList close

                autoLoanApplicationAll.objAutoLoanSecurityList = autoLoanSecurityList;

                #endregion objAutoLoanSecurityList

                #region AutoApplicationInsurance
                //AutoApplicationInsurance
                var autoApplicationInsurance = new AutoApplicationInsurance();
                queryString = String.Format(@"Select * from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoApplicationInsuranceDataReader = new AutoApplicationInsuranceDataReader(reader);
                while (reader.Read())
                    autoApplicationInsurance = autoApplicationInsuranceDataReader.Read();
                reader.Close();
                // AutoApplicationInsurance close

                autoLoanApplicationAll.objAutoApplicationInsurance = autoApplicationInsurance;

                #endregion AutoApplicationInsurance

                #endregion Loan Application

                #region Vehicle Information
                //Auto vendore   objAutoVendor
                var autoVendor = new AutoVendor();
                queryString = String.Format(@"Select * from auto_vendor where AutoVendorId = {0}", AutoVendorID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoVendorDataReader = new AutoVendorDataReader(reader);
                while (reader.Read())
                    autoVendor = autoVendorDataReader.Read();
                reader.Close();
                autoLoanApplicationAll.objAutoVendor = autoVendor;
                //End

                //objAutoVendorMou
                var autoVendorMou = new AutoVendorMOU();
                queryString = String.Format(@"select AutoVendorId,VendorName,VendorCode,Address,Phone,Website,isMOU,DealerType,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,StatusName as DealerStatus,TotalCount from
(
SELECT s1.AutoVendorId, s1.VendorName,s1.VendorCode, s1.Address, s1.Phone,s1.Website,s1.isMOU,s1.DealerType, s2.IrWithArta,s2.IrWithoutArta,s2.ProcessingFee,s2.ValidFrom,auto_status.StatusName,
(select count(*) from auto_vendor) AS TotalCount
FROM auto_vendor s1
Left outer JOIN auto_vendormou s2 ON s1.AutoVendorId = s2.AutoVendorId
and  s2.LastUpdateDate= 
(select Max(LastUpdateDate) as lsUpdateDate from auto_vendormou as s3 where s3.AutoVendorId=s1.AutoVendorId)
left join auto_status on auto_status.StatusId = s1.DealerType
order by s1.AutoVendorId asc) as tbl where AutoVendorId = {0}", AutoVendorID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoVendorMOUDataReader = new AutoVendorMOUDataReader(reader);
                while (reader.Read())
                    autoVendorMou = autoVendorMOUDataReader.Read();
                reader.Close();
                // objAutoVendor close

                autoLoanApplicationAll.objAutoVendorMou = autoVendorMou;

                #endregion Vehicle Information
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }

            return autoLoanApplicationAll;
        }

        private Object getDataFromPLUSS(int llid)
        {
            var autoLoanApplicationAll = new AutoLoanApplicationAll();
            var autoLoanMasterID = 0;
            var PRaccountID = 0;
            var JTaccountID = 0;
            var AutoVendorID = 0;
            var analystMasterId = 0;
            var sigmentID = 0;

            var objDbHelper = new CommonDbHelper();
            string queryString = "";

            #region Loan Application

            #region AutoLoanMaster
            var autoLoanMaster = new AutoLoanMaster();

            queryString = String.Format(@"Select * from auto_loanmaster where LLID = {0}", llid);

            IDataReader reader = objDbHelper.GetDataReader(queryString);

            var autoLoanMasterDataReader = new AutoLoanMasterDataReader(reader);
            while (reader.Read())
                autoLoanMaster = autoLoanMasterDataReader.Read();
            reader.Close();
            autoLoanMasterID = autoLoanMaster.Auto_LoanMasterId;
            //AutoLoanMaster Close
            //autoLoanMaster.UserType = userType;

            autoLoanApplicationAll.objAutoLoanMaster = autoLoanMaster;

            #endregion

            #region AutoPRApplicant

            var autoPRApplicant = new AutoPRApplicant();
            queryString = String.Format(@"Select * from auto_prapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRApplicantDataReader = new AutoPRApplicantDataReader(reader);
            while (reader.Read())
                autoPRApplicant = autoPRApplicantDataReader.Read();
            reader.Close();
            // AutoPRApplican close

            autoLoanApplicationAll.objAutoPRApplicant = autoPRApplicant;

            #endregion AutoPRApplicant

            #region AutoJTApplicant

            var autoJTApplicant = new AutoJTApplicant();
            queryString = String.Format(@"Select * from auto_jtapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTApplicantDataReader = new AutoJTApplicantDataReader(reader);
            while (reader.Read())
                autoJTApplicant = autoJTApplicantDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.objAutoJTApplicant = autoJTApplicant;

            #endregion AutoJTApplicant

            #region AutoPRProfession

            var autoPRProfession = new AutoPRProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'PrPrimaryProfessionName',pluss2.PROF_NAME 'PrOtherProfessionName' 
                                        from auto_prprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
					                    left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID
                                        where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRProfessionDataReader = new AutoPRProfessionDataReader(reader);
            while (reader.Read())
                autoPRProfession = autoPRProfessionDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.objAutoPRProfession = autoPRProfession;

            #endregion AutoPRProfession

            #region AutoJTProfession
            var autoJTProfession = new AutoJTProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'JtPrimaryProfessionName',pluss2.PROF_NAME 'JtOtherProfessionName'
                                        from auto_jtprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
                                        left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID 
                                        where auto.AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTProfessionDataReader = new AutoJTProfessionDataReader(reader);
            while (reader.Read())
                autoJTProfession = autoJTProfessionDataReader.Read();
            reader.Close();
            // AutoJTProfession close

            autoLoanApplicationAll.objAutoJTProfession = autoJTProfession;

            #endregion AutoJTProfession

            #region AutoLoanVehicle
            //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1),FALSE) as ValuePackAllowed
            var autoLoanVehicle = new AutoLoanVehicle();
            queryString = String.Format(@"SELECT auto_loanvehicle.*,
auto_status.StatusCode,
auto_menufacturer.ManufacturerCode,
auto_vehicle.ModelCode,
auto_vehicle.VehicleTypeCode,
auto_vendor.VendorCode,

IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
auto_brandtenorltv.ValuePackAllowed,
auto_brandtenorltv.Tenor,
auto_brandtenorltv.LTV
FROM
auto_loanvehicle
left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.VehicleId
INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
LEFT OUTER JOIN auto_brandtenorltv on auto_brandtenorltv.VehicleId = auto_vehicle.VehicleId and auto_brandtenorltv.StatusId = auto_loanvehicle.VehicleStatus
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
            while (reader.Read())
                autoLoanVehicle = autoLoanVehicleDataReader.Read();
            reader.Close();
            AutoVendorID = autoLoanVehicle.VendorId;

            autoLoanApplicationAll.objAutoLoanVehicle = autoLoanVehicle;

            #endregion AutoLoanVehicle

            #region AutoPRAccount
            var autoPRAccount = new AutoPRAccount();
            queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
            while (reader.Read())
                autoPRAccount = autoPRAccountDataReader.Read();
            reader.Close();
            PRaccountID = autoPRAccount.PrAccountId;
            // AutoPRAccount close

            autoLoanApplicationAll.objAutoPRAccount = autoPRAccount;

            #endregion AutoPRAccount

            #region AutoJTAccount
            var autoJTAccount = new AutoJTAccount();
            queryString = String.Format(@"Select * from auto_jtaccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTAccountDataReader = new AutoJTAccountDataReader(reader);
            while (reader.Read())
                autoJTAccount = autoJTAccountDataReader.Read();
            reader.Close();
            JTaccountID = autoJTAccount.jtAccountId;
            // AutoJTAccount close

            autoLoanApplicationAll.objAutoJTAccount = autoJTAccount;

            #endregion AutoJTAccount

            #region AutoPRAccountDetail
            List<AutoPRAccountDetail> AutoPRAccountDetailList = new List<AutoPRAccountDetail>();
            var autoPRAccountDetail = new AutoPRAccountDetail();
            queryString = String.Format(@"Select * from auto_praccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDetailDataReader = new AutoPRAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoPRAccountDetail = autoPRAccountDetailDataReader.Read();
                AutoPRAccountDetailList.Add(autoPRAccountDetail);
            }
            reader.Close();
            // AutoPRAccountDetail close

            autoLoanApplicationAll.objAutoPRAccountDetailList = AutoPRAccountDetailList;
            #endregion AutoPRAccountDetail

            #region AutoJTAccountDetail

            //
            List<AutoJTAccountDetail> autoJTAccountDetailList = new List<AutoJTAccountDetail>();
            var autoJTAccountDetail = new AutoJTAccountDetail();
            queryString = String.Format(@"Select * from auto_jtaccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTAccountDetailDataReader = new AutoJTAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoJTAccountDetail = autoJTAccountDetailDataReader.Read();
                autoJTAccountDetailList.Add(autoJTAccountDetail);
            }
            reader.Close();
            // AutoJTAccountDetail close

            autoLoanApplicationAll.objAutoJtAccountDetailList = autoJTAccountDetailList;

            #endregion AutoJTAccountDetail

            #region objAutoPRFinance
            //
            var autoPRFinance = new AutoPRFinance();
            queryString = String.Format(@"Select * from auto_prfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRFinanceDataReader = new AutoPRFinanceDataReader(reader);
            while (reader.Read())
                autoPRFinance = autoPRFinanceDataReader.Read();
            reader.Close();
            // objAutoPRFinance close

            autoLoanApplicationAll.objAutoPRFinance = autoPRFinance;

            #endregion objAutoPRFinance

            #region objAutoJTFinance
            //
            var autoJTFinance = new AutoJTFinance();
            queryString = String.Format(@"Select * from auto_jtfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTFinanceDataReader = new AutoJTFinanceDataReader(reader);
            while (reader.Read())
                autoJTFinance = autoJTFinanceDataReader.Read();
            reader.Close();
            // objAutoJTFinance close

            autoLoanApplicationAll.objAutoJTFinance = autoJTFinance;

            #endregion objAutoJTFinance

            #region objAutoLoanReference
            //
            var autoLoanReference = new AutoLoanReference();
            queryString = String.Format(@"Select * from auto_loanreference where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanReferenceDataReader = new AutoLoanReferenceDataReader(reader);
            while (reader.Read())
                autoLoanReference = autoLoanReferenceDataReader.Read();
            reader.Close();
            // objAutoLoanReference close

            autoLoanApplicationAll.objAutoLoanReference = autoLoanReference;

            #endregion objAutoLoanReference

            #region objAutoLoanFacilityList
            //
            List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
            var autoLoanFacility = new AutoLoanFacility();
            queryString = String.Format(@"SELECT
auto_loanfacility.FacilityId,
auto_loanfacility.AutoLoanMasterId,
auto_loanfacility.FacilityType,
auto_loanfacility.InterestRate,
auto_loanfacility.PresentBalance,
auto_loanfacility.PresentEMI,
auto_loanfacility.PresentLimit,
auto_loanfacility.ProposedLimit,
auto_loanfacility.RepaymentAgrement,
auto_loanfacility.Consider,
auto_loanfacility.EMIPercent,
auto_loanfacility.EMIShare,
auto_loanfacility.`Status`,
t_pluss_facility.FACI_NAME,
auto_loansecurity.NatureOfSecurity,
auto_loansecurity.FaceValue,
auto_loansecurity.XTVRate,
auto_loansecurity.FacilityType
FROM
auto_loanfacility
INNER JOIN t_pluss_facility ON auto_loanfacility.FacilityType = t_pluss_facility.FACI_ID
LEFT OUTER JOIN auto_loansecurity ON auto_loanfacility.AutoLoanMasterId = auto_loansecurity.AutoLoanMasterId AND t_pluss_facility.FACI_ID = auto_loansecurity.FacilityType
where auto_loanfacility.AutoLoanMasterId = {0}", autoLoanMasterID);

            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
            while (reader.Read())
            {
                autoLoanFacility = autoLoanFacilityDataReader.Read();
                autoLoanFacilityList.Add(autoLoanFacility);
            }
            reader.Close();
            // objAutoLoanFacilityList close

            autoLoanApplicationAll.objAutoLoanFacilityList = autoLoanFacilityList;

            #endregion objAutoLoanFacilityList

            #region objAutoLoanSecurityList
            //
            List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();
            var autoLoanSecurity = new AutoLoanSecurity();
            queryString = String.Format(@"Select * from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanSecurityDataReader = new AutoLoanSecurityDataReader(reader);
            while (reader.Read())
            {
                autoLoanSecurity = autoLoanSecurityDataReader.Read();
                autoLoanSecurityList.Add(autoLoanSecurity);
            }
            reader.Close();
            // objAutoLoanSecurityList close

            autoLoanApplicationAll.objAutoLoanSecurityList = autoLoanSecurityList;

            #endregion objAutoLoanSecurityList

            #region AutoApplicationInsurance
            //AutoApplicationInsurance
            var autoApplicationInsurance = new AutoApplicationInsurance();
            queryString = String.Format(@"Select * from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoApplicationInsuranceDataReader = new AutoApplicationInsuranceDataReader(reader);
            while (reader.Read())
                autoApplicationInsurance = autoApplicationInsuranceDataReader.Read();
            reader.Close();
            // AutoApplicationInsurance close

            autoLoanApplicationAll.objAutoApplicationInsurance = autoApplicationInsurance;

            #endregion AutoApplicationInsurance

            #endregion Loan Application

            #region Vehicle Information
            //Auto vendore   objAutoVendor
            var autoVendor = new AutoVendor();
            queryString = String.Format(@"Select * from auto_vendor where AutoVendorId = {0}", AutoVendorID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoVendorDataReader = new AutoVendorDataReader(reader);
            while (reader.Read())
                autoVendor = autoVendorDataReader.Read();
            reader.Close();
            autoLoanApplicationAll.objAutoVendor = autoVendor;
            //End

            //objAutoVendorMou
            var autoVendorMou = new AutoVendorMOU();
            queryString = String.Format(@"select AutoVendorId,VendorName,VendorCode,Address,Phone,Website,isMOU,DealerType,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,StatusName as DealerStatus,TotalCount from
(
SELECT s1.AutoVendorId, s1.VendorName,s1.VendorCode, s1.Address, s1.Phone,s1.Website,s1.isMOU,s1.DealerType, s2.IrWithArta,s2.IrWithoutArta,s2.ProcessingFee,s2.ValidFrom,auto_status.StatusName,
(select count(*) from auto_vendor) AS TotalCount
FROM auto_vendor s1
Left outer JOIN auto_vendormou s2 ON s1.AutoVendorId = s2.AutoVendorId
and  s2.LastUpdateDate= 
(select Max(LastUpdateDate) as lsUpdateDate from auto_vendormou as s3 where s3.AutoVendorId=s1.AutoVendorId)
left join auto_status on auto_status.StatusId = s1.DealerType
order by s1.AutoVendorId asc) as tbl where AutoVendorId = {0}", AutoVendorID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoVendorMOUDataReader = new AutoVendorMOUDataReader(reader);
            while (reader.Read())
                autoVendorMou = autoVendorMOUDataReader.Read();
            reader.Close();
            // objAutoVendor close

            autoLoanApplicationAll.objAutoVendorMou = autoVendorMou;

            #endregion Vehicle Information

            #region Bank Statement

            //AutoPRBankStatementList
            List<AutoPRBankStatement> objAutoPRBankStatementList = new List<AutoPRBankStatement>();
            var autoPRBankStatement = new AutoPRBankStatement();
            queryString = String.Format(@"Select * from auto_prbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRBankStatementDataReader = new AutoPRBankStatementDataReader(reader);
            while (reader.Read())
            {
                autoPRBankStatement = autoPRBankStatementDataReader.Read();
                objAutoPRBankStatementList.Add(autoPRBankStatement);
            }
            reader.Close();
            // AutoPRBankStatementList close

            autoLoanApplicationAll.AutoPRBankStatementList = objAutoPRBankStatementList;


            //AutoJTBankStatementList
            List<AutoJTBankStatement> objAutoJTBankStatementList = new List<AutoJTBankStatement>();
            var autoJTBankStatement = new AutoJTBankStatement();
            queryString = String.Format(@"Select * from auto_jtbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTBankStatementDataReader = new AutoJTBankStatementDataReader(reader);
            while (reader.Read())
            {
                autoJTBankStatement = autoJTBankStatementDataReader.Read();
                objAutoJTBankStatementList.Add(autoJTBankStatement);
            }
            reader.Close();
            // AutoJTBankStatementList close

            autoLoanApplicationAll.AutoJTBankStatementList = objAutoJTBankStatementList;


            //AutoBankStatementAvgTotalList
            List<Auto_BankStatementAvgTotal> AutoBankStatementAvgTotalList = new List<Auto_BankStatementAvgTotal>();
            var autoBankStatementAvgTotal = new Auto_BankStatementAvgTotal();
            queryString = String.Format(@"Select * from auto_bankstatementavgtotal where AutoLoanMasterId = {0} order by Type,month asc", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoBankStatementAvgTotalDataReader = new AutoBankStatementAvgTotalDataReader(reader);
            while (reader.Read())
            {
                autoBankStatementAvgTotal = autoBankStatementAvgTotalDataReader.Read();
                AutoBankStatementAvgTotalList.Add(autoBankStatementAvgTotal);
            }
            reader.Close();
            // AutoBankStatementAvgTotalList close

            autoLoanApplicationAll.AutoBankStatementAvgTotalList = AutoBankStatementAvgTotalList;

            #endregion Bank Statement

            #region Loan Appliaction Analyst Part

            #region General Tab

            #region Aanalyst Master start

            var autoLoanAnalystMaster = new Auto_LoanAnalystMaster();

            queryString = String.Format(@"Select * from auto_loananalystmaster where LLID = {0} and AutoLoanMasterId = {1}", llid, autoLoanMasterID);

            reader = objDbHelper.GetDataReader(queryString);

            var autoLoanAnalystMasterDataReader = new Auto_LoanAnalystMasterDataReader(reader);
            while (reader.Read())
                autoLoanAnalystMaster = autoLoanAnalystMasterDataReader.Read();
            reader.Close();
            analystMasterId = autoLoanAnalystMaster.AnalystMasterId;

            autoLoanApplicationAll.AutoLoanAnalystMaster = autoLoanAnalystMaster;

            #endregion Aanalyst Master start

            #region Auto_Loan Analyst Application

            var autoLoanAnalystApplication = new Auto_LoanAnalystApplication();

            queryString = String.Format(@"Select a.*,b.SourceName AppSourceName,c.CompanyName ARTAName from auto_loananalystapplication a 
                                            LEFT OUTER JOIN auto_source b ON a.BranchId = b.AutoSourceId 
                                            LEFT OUTER JOIN auto_insurancebanca c ON a.ARTAStatus = c.InsCompanyId 
                                            where a.AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);

            var autoLoanAnalystApplicationDataReader = new Auto_LoanAnalystApplicationDataReader(reader);

            while (reader.Read())
                autoLoanAnalystApplication = autoLoanAnalystApplicationDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.AutoLoanAnalystApplication = autoLoanAnalystApplication;

            #endregion Auto_Loan Analyst Application

            #region Auto_Loan Analyst Vehicle Detail

            var autoLoanAnalystVehicleDetail = new Auto_LoanAnalystVehicleDetail();

            queryString = String.Format(@"Select * from auto_loananalystvehicledetail where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);

            var autoLoanAnalystVehicleDetailDataReader = new Auto_LoanAnalystVehicleDetailDataReader(reader);

            while (reader.Read())
                autoLoanAnalystVehicleDetail = autoLoanAnalystVehicleDetailDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.AutoLoanAnalystVehicleDetail = autoLoanAnalystVehicleDetail;

            #endregion Auto_Loan Analyst Vehicle Detail

            #region Auto Applicant SCB Account

            List<AutoApplicantSCBAccount> autoApplicantSCBAccountList = new List<AutoApplicantSCBAccount>();
            var autoApplicantSCBAccount = new AutoApplicantSCBAccount();

            queryString = String.Format(@"Select * from auto_applicantscbaccount where AutoLoanMasterId = {0}", autoLoanMasterID);

            reader = objDbHelper.GetDataReader(queryString);
            var autoApplicantSCBAccountDataReader = new AutoApplicantSCBAccountDataReader(reader);
            while (reader.Read())
            {
                autoApplicantSCBAccount = autoApplicantSCBAccountDataReader.Read();
                autoApplicantSCBAccountList.Add(autoApplicantSCBAccount);
            }
            reader.Close();

            autoLoanApplicationAll.AutoApplicantSCBAccountList = autoApplicantSCBAccountList;

            #endregion Auto Applicant SCB Account

            #region Auto Applicant Account Other List

            // already create the list in AutoLoan region

            #endregion Auto Applicant Account Other List

            #region Auto Facility List

            // already create the list in AutoLoan region

            #endregion Auto Facility List

            #region Auto Security List

            // already create the list in AutoLoan region

            #endregion Auto Security List

            #region OFF SCB Facility List

            List<AutoLoanFacilityOffSCB> autoLoanFacilityOffSCBList = new List<AutoLoanFacilityOffSCB>();
            var autoLoanFacilityOffSCB = new AutoLoanFacilityOffSCB();

            queryString = String.Format(@"Select * from auto_loanfacilityoffscb where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanFacilityOffSCBDataReader = new AutoLoanFacilityOffSCBDataReader(reader);
            while (reader.Read())
            {
                autoLoanFacilityOffSCB = autoLoanFacilityOffSCBDataReader.Read();
                autoLoanFacilityOffSCBList.Add(autoLoanFacilityOffSCB);
            }
            reader.Close();

            autoLoanApplicationAll.AutoLoanFacilityOffSCBList = autoLoanFacilityOffSCBList;

            #endregion OFF SCB Facility List

            #endregion General Tab

            #region Loan Calculation Tab

            #region Applicant Income List

            List<Auto_An_IncomeSegmentApplicantIncome> auto_An_IncomeSegmentApplicantIncomeList = new List<Auto_An_IncomeSegmentApplicantIncome>();
            var auto_An_IncomeSegmentApplicantIncome = new Auto_An_IncomeSegmentApplicantIncome();

            queryString = String.Format(@"Select * from Auto_An_In_Seg_Applicant_Income where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_IncomeSegmentApplicantIncomeDataReader = new Auto_An_IncomeSegmentApplicantIncomeDataReader(reader);
            while (reader.Read())
            {
                auto_An_IncomeSegmentApplicantIncome = auto_An_IncomeSegmentApplicantIncomeDataReader.Read();
                auto_An_IncomeSegmentApplicantIncomeList.Add(auto_An_IncomeSegmentApplicantIncome);
            }
            reader.Close();

            autoLoanApplicationAll.Auto_An_IncomeSegmentApplicantIncomeList = auto_An_IncomeSegmentApplicantIncomeList;

            #endregion Applicant Income List

            #region Segment Income

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_IncomeSegmentIncome = new Auto_An_IncomeSegmentIncome();

            queryString = String.Format(@"Select * from Auto_An_In_Seg_Income where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_IncomeSegmentIncomeDataReader = new Auto_An_IncomeSegmentIncomeDataReader(reader);
            while (reader.Read())
            {
                auto_An_IncomeSegmentIncome = auto_An_IncomeSegmentIncomeDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
                sigmentID = auto_An_IncomeSegmentIncome.EmployeeSegment;
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_IncomeSegmentIncome = auto_An_IncomeSegmentIncome;

            #endregion Segment Income

            #region REPAYMENT CAPABILITY

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_RepaymentCapability = new Auto_An_RepaymentCapability();

            queryString = String.Format(@"Select * from Auto_An_RepaymentCapability where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_RepaymentCapabilityDataReader = new Auto_An_RepaymentCapabilityDataReader(reader);
            while (reader.Read())
            {
                auto_An_RepaymentCapability = auto_An_RepaymentCapabilityDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_RepaymentCapability = auto_An_RepaymentCapability;

            #endregion REPAYMENT CAPABILITY

            #region Loan Calculation Tenor

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationTenor = new Auto_An_LoanCalculationTenor();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationTenor where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_LoanCalculationTenorDataReader = new Auto_An_LoanCalculationTenorDataReader(reader);
            while (reader.Read())
            {
                auto_An_LoanCalculationTenor = auto_An_LoanCalculationTenorDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_LoanCalculationTenor = auto_An_LoanCalculationTenor;

            #endregion Loan Calculation Tenor

            #region Loan Calculation INTEREST

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationInterest = new Auto_An_LoanCalculationInterest();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationInterest where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_LoanCalculationInterestDataReader = new Auto_An_LoanCalculationInterestDataReader(reader);
            while (reader.Read())
            {
                auto_An_LoanCalculationInterest = auto_An_LoanCalculationInterestDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_LoanCalculationInterest = auto_An_LoanCalculationInterest;

            #endregion Loan Calculation INTEREST

            #region Loan Calculation Amount

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationAmount = new Auto_An_LoanCalculationAmount();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationAmount where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_LoanCalculationAmountDataReader = new Auto_An_LoanCalculationAmountDataReader(reader);
            while (reader.Read())
            {
                auto_An_LoanCalculationAmount = auto_An_LoanCalculationAmountDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_LoanCalculationAmount = auto_An_LoanCalculationAmount;

            #endregion Loan Calculation Amount

            #region Loan Calculation INSURANCE

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationInsurance = new Auto_An_LoanCalculationInsurance();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationInsurance where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_LoanCalculationInsuranceDataReader = new Auto_An_LoanCalculationInsuranceDataReader(reader);
            while (reader.Read())
            {
                auto_An_LoanCalculationInsurance = auto_An_LoanCalculationInsuranceDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_LoanCalculationInsurance = auto_An_LoanCalculationInsurance;

            #endregion Loan Calculation INSURANCE

            #region Loan Calculation Total Loan Amount

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationTotal = new Auto_An_LoanCalculationTotal();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationTotal where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_LoanCalculationTotalDataReader = new Auto_An_LoanCalculationTotalDataReader(reader);
            while (reader.Read())
            {
                auto_An_LoanCalculationTotal = auto_An_LoanCalculationTotalDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_LoanCalculationTotal = auto_An_LoanCalculationTotal;

            #endregion Loan Calculation Total Loan Amount

            #endregion Loan Calculation Tab

            #region Finalization Tab

            #region Auto_An_Finalization Deviation List

            List<Auto_An_FinalizationDeviation> auto_An_FinalizationDeviationList = new List<Auto_An_FinalizationDeviation>();
            var auto_An_FinalizationDeviation = new Auto_An_FinalizationDeviation();

            queryString = String.Format(@"SELECT auto_an_finalizationdeviation.*,auto_deviation.Description as DescriptionText
                                        FROM auto_an_finalizationdeviation INNER JOIN auto_deviation ON 
                                        auto_an_finalizationdeviation.Description = auto_deviation.DeviationId
                                        where auto_an_finalizationdeviation.AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationDeviationDataReader = new Auto_An_FinalizationDeviationDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationDeviation = auto_An_FinalizationDeviationDataReader.Read();
                auto_An_FinalizationDeviationList.Add(auto_An_FinalizationDeviation);
            }
            reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationDeviationList = auto_An_FinalizationDeviationList;

            #endregion Deviation List

            #region Auto_An_Finalization Dev Found

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationDevFound = new Auto_An_FinalizationDevFound();

            queryString = String.Format(@"Select * from auto_an_finalizationdevfound where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationDevFoundDataReader = new Auto_An_FinalizationDevFoundDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationDevFound = auto_An_FinalizationDevFoundDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_FinalizationDevFound = auto_An_FinalizationDevFound;

            #endregion Auto_An_Finalization Dev Found

            #region Auto_An_Finalization Sterength List

            List<Auto_An_FinalizationSterength> auto_An_FinalizationSterengthList = new List<Auto_An_FinalizationSterength>();
            var auto_An_FinalizationSterength = new Auto_An_FinalizationSterength();

            queryString = String.Format(@"Select * from auto_an_finalizationsterength where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationSterengthDataReader = new Auto_An_FinalizationSterengthDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationSterength = auto_An_FinalizationSterengthDataReader.Read();
                auto_An_FinalizationSterengthList.Add(auto_An_FinalizationSterength);
            }
            reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationSterengthList = auto_An_FinalizationSterengthList;

            #endregion Auto_An_Finalization Sterength List

            #region Auto_An_Finalization Weakness List

            List<Auto_An_FinalizationWeakness> auto_An_FinalizationWeaknessList = new List<Auto_An_FinalizationWeakness>();
            var auto_An_FinalizationWeakness = new Auto_An_FinalizationWeakness();

            queryString = String.Format(@"Select * from auto_an_finalizationweakness where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationWeaknessDataReader = new Auto_An_FinalizationWeaknessDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationWeakness = auto_An_FinalizationWeaknessDataReader.Read();
                auto_An_FinalizationWeaknessList.Add(auto_An_FinalizationWeakness);
            }
            reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationWeaknessList = auto_An_FinalizationWeaknessList;

            #endregion Auto_An_Finalization Weakness List

            #region Auto_An_Finalization Condition List

            List<Auto_An_FinalizationCondition> auto_An_FinalizationConditionList = new List<Auto_An_FinalizationCondition>();
            var auto_An_FinalizationCondition = new Auto_An_FinalizationCondition();

            queryString = String.Format(@"Select * from auto_an_finalizationcondition where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationConditionDataReader = new Auto_An_FinalizationConditionDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationCondition = auto_An_FinalizationConditionDataReader.Read();
                auto_An_FinalizationConditionList.Add(auto_An_FinalizationCondition);
            }
            reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationConditionList = auto_An_FinalizationConditionList;

            #endregion Auto_An_Finalization Condition List

            #region Auto_An_Finalization Remark List

            List<Auto_An_FinalizationRemark> auto_An_FinalizationRemarkList = new List<Auto_An_FinalizationRemark>();
            var auto_An_FinalizationRemark = new Auto_An_FinalizationRemark();

            queryString = String.Format(@"Select * from auto_an_finalizationremark where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationRemarkDataReader = new Auto_An_FinalizationRemarkDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationRemark = auto_An_FinalizationRemarkDataReader.Read();
                auto_An_FinalizationRemarkList.Add(auto_An_FinalizationRemark);
            }
            reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationRemarkList = auto_An_FinalizationRemarkList;

            #endregion Auto_An_Finalization Remark List

            #region Auto_An_Finalization Repayment Method

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationRepaymentMethod = new Auto_An_FinalizationRepaymentMethod();

            queryString = String.Format(@"Select a.*,b.BANK_NM BankName from auto_an_finalizationrepaymentmethod a 
                                            LEFT OUTER JOIN t_pluss_bank b ON a.BankID = b.BANK_ID 
                                            where a.AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationRepaymentMethodDataReader = new Auto_An_FinalizationRepaymentMethodDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationRepaymentMethod = auto_An_FinalizationRepaymentMethodDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_FinalizationRepaymentMethod = auto_An_FinalizationRepaymentMethod;

            #endregion Auto_An_Finalization Repayment Method

            #region Auto_An_Finalization Req Doc

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationReqDoc = new Auto_An_FinalizationReqDoc();

            queryString = String.Format(@"Select * from auto_an_finalizationreqdoc where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationReqDocDataReader = new Auto_An_FinalizationReqDocDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationReqDoc = auto_An_FinalizationReqDocDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_FinalizationReqDoc = auto_An_FinalizationReqDoc;

            #endregion Auto_An_Finalization Req Doc

            #region Auto_An_ Finalization Varification Report

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationVarificationReport = new Auto_An_FinalizationVarificationReport();

            queryString = String.Format(@"Select * from auto_an_finalizationvarificationreport where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationVarificationReportDataReader = new Auto_An_FinalizationVarificationReportDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationVarificationReport = auto_An_FinalizationVarificationReportDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_FinalizationVarificationReport = auto_An_FinalizationVarificationReport;

            #endregion Auto_An_ Finalization Varification Report

            #region Auto_An_ Finalization Appraiser And Approver

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationAppraiserAndApprover = new Auto_An_FinalizationAppraiserAndApprover();

            queryString = String.Format(@"Select * from auto_an_finalizationappraiserandapprover where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationAppraiserAndApproverDataReader = new Auto_An_FinalizationAppraiserAndApproverDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationAppraiserAndApprover = auto_An_FinalizationAppraiserAndApproverDataReader.Read();
                //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_FinalizationAppraiserAndApprover = auto_An_FinalizationAppraiserAndApprover;

            #endregion Auto_An_ Finalization Appraiser And Approver

            #region Auto_An_Alert Verification Report

            var auto_An_AlertVerificationReport = new Auto_An_AlertVerificationReport();

            queryString = String.Format(@"Select * from auto_an_alertverificationreport where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_AlertVerificationReportDataReader = new Auto_An_AlertVerificationReportDataReader(reader);
            while (reader.Read())
            {
                auto_An_AlertVerificationReport = auto_An_AlertVerificationReportDataReader.Read();
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_AlertVerificationReport = auto_An_AlertVerificationReport;

            #endregion Auto_An_Alert Verification Report

            #region Auto_An_ Finalization Rework

            var auto_An_FinalizationRework = new Auto_An_FinalizationRework();

            queryString = String.Format(@"Select * from auto_an_rework where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_FinalizationReworkDataReader = new Auto_An_FinalizationReworkDataReader(reader);
            while (reader.Read())
            {
                auto_An_FinalizationRework = auto_An_FinalizationReworkDataReader.Read();
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_FinalizationRework = auto_An_FinalizationRework;

            #endregion Auto_An_ Finalization Rework

            #region Auto_An_Finalization auto_An_CibRequestLoan

            List<auto_An_CibRequestLoan> auto_An_CibRequestLoanList = new List<auto_An_CibRequestLoan>();
            var auto_An_CibRequestLoan = new auto_An_CibRequestLoan();

            queryString = String.Format(@"Select * from auto_An_CibRequestLoan where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var auto_An_CibRequestLoanDataReader = new auto_An_CibRequestLoanDataReader(reader);
            while (reader.Read())
            {
                auto_An_CibRequestLoan = auto_An_CibRequestLoanDataReader.Read();
                auto_An_CibRequestLoanList.Add(auto_An_CibRequestLoan);
            }
            reader.Close();

            autoLoanApplicationAll.objAuto_An_CibRequestLoan = auto_An_CibRequestLoanList;

            #endregion Auto_An_Finalization auto_An_CibRequestLoan



            #endregion Finalization Tab




            #endregion Loan Appliaction Analyst Part

            #region Extra Property

            #region Segment settings

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var autoSegmentSettings = new AutoSegmentSettings();

            queryString = String.Format(@"select 	SegmentID, SegmentCode, SegmentName, Description, Profession, MinLoanAmt, MaxLoanAmt, MinTanor, 
	MaxTanor, IrWithARTA,IrWithoutARTA, MinAgeL1, MinAgeL2, MaxAgeL1, MaxAgeL2, LTVL1, LTVL2, ExperienceL1, ExperienceL2, 
	AcRelationshipL1, AcRelationshipL2, CriteriaCode, AssetCode, MinDBRL1, MinDBRL2, MaxDBRL2, 
	DBRL3, FieldVisit, Verification, LandTelephone, GuranteeReference, PrimaryMinIncomeL1, 
	PrimaryMinIncomeL2, JointMinIncomeL1, JointMinIncomeL2, Comments, UserId, LastUpdateDate,t_pluss_profession.PROF_NAME,t_pluss_profession.PROF_ID,

(select count(SegmentID) from auto_segment) as TotalCount 
from auto_segment inner join t_pluss_profession on  auto_segment.Profession = t_pluss_profession.PROF_ID 
where SegmentID = {0}", sigmentID);

            reader = objDbHelper.GetDataReader(queryString);
            var autoSegmentSettingsDataReader = new AutoSegmentSettingsDataReader(reader);
            while (reader.Read())
            {
                autoSegmentSettings = autoSegmentSettingsDataReader.Read();
            }
            reader.Close();

            autoLoanApplicationAll.objAutoSegmentSettings = autoSegmentSettings;

            #endregion Segment settings

            #endregion Extra Property



            return autoLoanApplicationAll;
        }

        private Object getDataFromPLUSSForCISupport(int llid)
        {
            var autoLoanApplicationAll = new AutoLoanApplicationAll();
            var autoLoanMasterID = 0;
            var PRaccountID = 0;
            var JTaccountID = 0;
            var AutoVendorID = 0;
            var analystMasterId = 0;
            var sigmentID = 0;

            var objDbHelper = new CommonDbHelper();
            string queryString = "";

            #region Loan Application

            #region AutoLoanMaster
            var autoLoanMaster = new AutoLoanMaster();

            queryString = String.Format(@"Select * from auto_loanmaster where LLID = {0}", llid);

            IDataReader reader = objDbHelper.GetDataReader(queryString);

            var autoLoanMasterDataReader = new AutoLoanMasterDataReader(reader);
            while (reader.Read())
                autoLoanMaster = autoLoanMasterDataReader.Read();
            reader.Close();
            autoLoanMasterID = autoLoanMaster.Auto_LoanMasterId;
            //AutoLoanMaster Close
            //autoLoanMaster.UserType = userType;

            autoLoanApplicationAll.objAutoLoanMaster = autoLoanMaster;

            #endregion

            #region AutoPRApplicant

            var autoPRApplicant = new AutoPRApplicant();
            queryString = String.Format(@"Select * from auto_prapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRApplicantDataReader = new AutoPRApplicantDataReader(reader);
            while (reader.Read())
                autoPRApplicant = autoPRApplicantDataReader.Read();
            reader.Close();
            // AutoPRApplican close

            autoLoanApplicationAll.objAutoPRApplicant = autoPRApplicant;

            #endregion AutoPRApplicant

            #region AutoJTApplicant

            var autoJTApplicant = new AutoJTApplicant();
            queryString = String.Format(@"Select * from auto_jtapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTApplicantDataReader = new AutoJTApplicantDataReader(reader);
            while (reader.Read())
                autoJTApplicant = autoJTApplicantDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.objAutoJTApplicant = autoJTApplicant;

            #endregion AutoJTApplicant

            #region AutoPRProfession

            var autoPRProfession = new AutoPRProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'PrPrimaryProfessionName',pluss2.PROF_NAME 'PrOtherProfessionName' 
                                        from auto_prprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
					                    left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID
                                        where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRProfessionDataReader = new AutoPRProfessionDataReader(reader);
            while (reader.Read())
                autoPRProfession = autoPRProfessionDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.objAutoPRProfession = autoPRProfession;

            #endregion AutoPRProfession

            #region AutoJTProfession
            var autoJTProfession = new AutoJTProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'JtPrimaryProfessionName',pluss2.PROF_NAME 'JtOtherProfessionName'
                                        from auto_jtprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
                                        left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID 
                                        where auto.AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTProfessionDataReader = new AutoJTProfessionDataReader(reader);
            while (reader.Read())
                autoJTProfession = autoJTProfessionDataReader.Read();
            reader.Close();
            // AutoJTProfession close

            autoLoanApplicationAll.objAutoJTProfession = autoJTProfession;

            #endregion AutoJTProfession

            #region AutoLoanVehicle
            //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1),FALSE) as ValuePackAllowed
            var autoLoanVehicle = new AutoLoanVehicle();
            queryString = String.Format(@"SELECT auto_loanvehicle.*,
auto_status.StatusCode,
auto_menufacturer.ManufacturerCode,
auto_vehicle.ModelCode,
auto_vehicle.VehicleTypeCode,
auto_vendor.VendorCode,

IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
auto_brandtenorltv.ValuePackAllowed,
auto_brandtenorltv.Tenor,
auto_brandtenorltv.LTV
FROM
auto_loanvehicle
left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.VehicleId
INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
LEFT OUTER JOIN auto_brandtenorltv on auto_brandtenorltv.VehicleId = auto_vehicle.VehicleId and auto_brandtenorltv.StatusId = auto_loanvehicle.VehicleStatus
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
            while (reader.Read())
                autoLoanVehicle = autoLoanVehicleDataReader.Read();
            reader.Close();
            AutoVendorID = autoLoanVehicle.VendorId;

            autoLoanApplicationAll.objAutoLoanVehicle = autoLoanVehicle;

            #endregion AutoLoanVehicle

            #region AutoPRAccount
            //var autoPRAccount = new AutoPRAccount();
            //queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
            //while (reader.Read())
            //    autoPRAccount = autoPRAccountDataReader.Read();
            //reader.Close();
            //PRaccountID = autoPRAccount.PrAccountId;

            List<AutoPRAccount> autoPRAccountList = new List<AutoPRAccount>();
            var autoPRAccount = new AutoPRAccount();
            queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
            while (reader.Read())
            {
                autoPRAccount = autoPRAccountDataReader.Read();
                autoPRAccountList.Add(autoPRAccount);
            }
            reader.Close();
            // AutoPRAccountDetail close

            autoLoanApplicationAll.objAutoPRAccountList = autoPRAccountList;
            // AutoPRAccount close

            autoLoanApplicationAll.objAutoPRAccount = autoPRAccount;

            #endregion AutoPRAccount

            #region AutoJTAccount
            var autoJTAccount = new AutoJTAccount();
            queryString = String.Format(@"Select * from auto_jtaccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTAccountDataReader = new AutoJTAccountDataReader(reader);
            while (reader.Read())
                autoJTAccount = autoJTAccountDataReader.Read();
            reader.Close();
            JTaccountID = autoJTAccount.jtAccountId;
            // AutoJTAccount close

            autoLoanApplicationAll.objAutoJTAccount = autoJTAccount;

            #endregion AutoJTAccount

            #region AutoPRAccountDetail
            List<AutoPRAccountDetail> AutoPRAccountDetailList = new List<AutoPRAccountDetail>();
            var autoPRAccountDetail = new AutoPRAccountDetail();
            queryString = String.Format(@"Select * from auto_praccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDetailDataReader = new AutoPRAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoPRAccountDetail = autoPRAccountDetailDataReader.Read();
                AutoPRAccountDetailList.Add(autoPRAccountDetail);
            }
            reader.Close();
            // AutoPRAccountDetail close

            autoLoanApplicationAll.objAutoPRAccountDetailList = AutoPRAccountDetailList;
            #endregion AutoPRAccountDetail

            #region AutoJTAccountDetail

            //
            List<AutoJTAccountDetail> autoJTAccountDetailList = new List<AutoJTAccountDetail>();
            var autoJTAccountDetail = new AutoJTAccountDetail();
            queryString = String.Format(@"Select * from auto_jtaccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTAccountDetailDataReader = new AutoJTAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoJTAccountDetail = autoJTAccountDetailDataReader.Read();
                autoJTAccountDetailList.Add(autoJTAccountDetail);
            }
            reader.Close();
            // AutoJTAccountDetail close

            autoLoanApplicationAll.objAutoJtAccountDetailList = autoJTAccountDetailList;

            #endregion AutoJTAccountDetail

            #region objAutoPRFinance
            //
            var autoPRFinance = new AutoPRFinance();
            queryString = String.Format(@"Select * from auto_prfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRFinanceDataReader = new AutoPRFinanceDataReader(reader);
            while (reader.Read())
                autoPRFinance = autoPRFinanceDataReader.Read();
            reader.Close();
            // objAutoPRFinance close

            autoLoanApplicationAll.objAutoPRFinance = autoPRFinance;

            #endregion objAutoPRFinance

            #region objAutoJTFinance
            //
            var autoJTFinance = new AutoJTFinance();
            queryString = String.Format(@"Select * from auto_jtfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTFinanceDataReader = new AutoJTFinanceDataReader(reader);
            while (reader.Read())
                autoJTFinance = autoJTFinanceDataReader.Read();
            reader.Close();
            // objAutoJTFinance close

            autoLoanApplicationAll.objAutoJTFinance = autoJTFinance;

            #endregion objAutoJTFinance

            #region objAutoLoanReference
            //
            var autoLoanReference = new AutoLoanReference();
            queryString = String.Format(@"Select * from auto_loanreference where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanReferenceDataReader = new AutoLoanReferenceDataReader(reader);
            while (reader.Read())
                autoLoanReference = autoLoanReferenceDataReader.Read();
            reader.Close();
            // objAutoLoanReference close

            autoLoanApplicationAll.objAutoLoanReference = autoLoanReference;

            #endregion objAutoLoanReference

            #region objAutoLoanFacilityList
            //
            List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
            var autoLoanFacility = new AutoLoanFacility();
            queryString = String.Format(@"SELECT
auto_loanfacility.FacilityId,
auto_loanfacility.AutoLoanMasterId,
auto_loanfacility.FacilityType,
auto_loanfacility.InterestRate,
auto_loanfacility.PresentBalance,
auto_loanfacility.PresentEMI,
auto_loanfacility.PresentLimit,
auto_loanfacility.ProposedLimit,
auto_loanfacility.RepaymentAgrement,
auto_loanfacility.Consider,
auto_loanfacility.EMIPercent,
auto_loanfacility.EMIShare,
t_pluss_facility.FACI_NAME,
auto_loansecurity.NatureOfSecurity,
auto_loansecurity.FaceValue,
auto_loansecurity.XTVRate,
auto_loansecurity.FacilityType,
auto_loansecurity.`Status`
FROM
auto_loanfacility
INNER JOIN t_pluss_facility ON auto_loanfacility.FacilityType = t_pluss_facility.FACI_ID
LEFT OUTER JOIN auto_loansecurity ON auto_loanfacility.AutoLoanMasterId = auto_loansecurity.AutoLoanMasterId AND t_pluss_facility.FACI_ID = auto_loansecurity.FacilityType AND auto_loansecurity.FacilityId = auto_loanfacility.FacilityId
where auto_loanfacility.AutoLoanMasterId = {0}", autoLoanMasterID);

            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
            while (reader.Read())
            {
                autoLoanFacility = autoLoanFacilityDataReader.Read();
                autoLoanFacilityList.Add(autoLoanFacility);
            }
            reader.Close();
            // objAutoLoanFacilityList close

            autoLoanApplicationAll.objAutoLoanFacilityList = autoLoanFacilityList;

            #endregion objAutoLoanFacilityList

            #region objAutoLoanSecurityList
            //
            List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();
            var autoLoanSecurity = new AutoLoanSecurity();
            queryString = String.Format(@"Select * from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanSecurityDataReader = new AutoLoanSecurityDataReader(reader);
            while (reader.Read())
            {
                autoLoanSecurity = autoLoanSecurityDataReader.Read();
                autoLoanSecurityList.Add(autoLoanSecurity);
            }
            reader.Close();
            // objAutoLoanSecurityList close

            autoLoanApplicationAll.objAutoLoanSecurityList = autoLoanSecurityList;

            #endregion objAutoLoanSecurityList

            #region AutoApplicationInsurance
            //AutoApplicationInsurance
            var autoApplicationInsurance = new AutoApplicationInsurance();
            queryString = String.Format(@"Select * from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoApplicationInsuranceDataReader = new AutoApplicationInsuranceDataReader(reader);
            while (reader.Read())
                autoApplicationInsurance = autoApplicationInsuranceDataReader.Read();
            reader.Close();
            // AutoApplicationInsurance close

            autoLoanApplicationAll.objAutoApplicationInsurance = autoApplicationInsurance;

            #endregion AutoApplicationInsurance

            #endregion Loan Application

            #region Vehicle Information
            //Auto vendore   objAutoVendor
            var autoVendor = new AutoVendor();
            queryString = String.Format(@"Select * from auto_vendor where AutoVendorId = {0}", AutoVendorID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoVendorDataReader = new AutoVendorDataReader(reader);
            while (reader.Read())
                autoVendor = autoVendorDataReader.Read();
            reader.Close();
            autoLoanApplicationAll.objAutoVendor = autoVendor;
            //End

            //objAutoVendorMou
            var autoVendorMou = new AutoVendorMOU();
            queryString = String.Format(@"select AutoVendorId,VendorName,VendorCode,Address,Phone,Website,isMOU,DealerType,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,StatusName as DealerStatus,TotalCount from
(
SELECT s1.AutoVendorId, s1.VendorName,s1.VendorCode, s1.Address, s1.Phone,s1.Website,s1.isMOU,s1.DealerType, s2.IrWithArta,s2.IrWithoutArta,s2.ProcessingFee,s2.ValidFrom,auto_status.StatusName,
(select count(*) from auto_vendor) AS TotalCount
FROM auto_vendor s1
Left outer JOIN auto_vendormou s2 ON s1.AutoVendorId = s2.AutoVendorId
and  s2.LastUpdateDate= 
(select Max(LastUpdateDate) as lsUpdateDate from auto_vendormou as s3 where s3.AutoVendorId=s1.AutoVendorId)
left join auto_status on auto_status.StatusId = s1.DealerType
order by s1.AutoVendorId asc) as tbl where AutoVendorId = {0}", AutoVendorID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoVendorMOUDataReader = new AutoVendorMOUDataReader(reader);
            while (reader.Read())
                autoVendorMou = autoVendorMOUDataReader.Read();
            reader.Close();
            // objAutoVendor close

            autoLoanApplicationAll.objAutoVendorMou = autoVendorMou;

            #endregion Vehicle Information



            #region Loan Appliaction Analyst Part

            #region General Tab

            #region Aanalyst Master start

            var autoLoanAnalystMaster = new Auto_LoanAnalystMaster();

            queryString = String.Format(@"Select * from auto_loananalystmaster where LLID = {0} and AutoLoanMasterId = {1}", llid, autoLoanMasterID);

            reader = objDbHelper.GetDataReader(queryString);

            var autoLoanAnalystMasterDataReader = new Auto_LoanAnalystMasterDataReader(reader);
            while (reader.Read())
                autoLoanAnalystMaster = autoLoanAnalystMasterDataReader.Read();
            reader.Close();
            analystMasterId = autoLoanAnalystMaster.AnalystMasterId;

            autoLoanApplicationAll.AutoLoanAnalystMaster = autoLoanAnalystMaster;

            #endregion Aanalyst Master start

            #region Auto_Loan Analyst Application

            var autoLoanAnalystApplication = new Auto_LoanAnalystApplication();

            queryString = String.Format(@"Select * from auto_loananalystapplication where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);

            var autoLoanAnalystApplicationDataReader = new Auto_LoanAnalystApplicationDataReader(reader);

            while (reader.Read())
                autoLoanAnalystApplication = autoLoanAnalystApplicationDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.AutoLoanAnalystApplication = autoLoanAnalystApplication;

            #endregion Auto_Loan Analyst Application

            #region Auto_Loan Analyst Vehicle Detail

            var autoLoanAnalystVehicleDetail = new Auto_LoanAnalystVehicleDetail();

            queryString = String.Format(@"Select * from auto_loananalystvehicledetail where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);

            var autoLoanAnalystVehicleDetailDataReader = new Auto_LoanAnalystVehicleDetailDataReader(reader);

            while (reader.Read())
                autoLoanAnalystVehicleDetail = autoLoanAnalystVehicleDetailDataReader.Read();
            reader.Close();

            autoLoanApplicationAll.AutoLoanAnalystVehicleDetail = autoLoanAnalystVehicleDetail;

            #endregion Auto_Loan Analyst Vehicle Detail

            #region Auto Applicant SCB Account

            List<AutoApplicantSCBAccount> autoApplicantSCBAccountList = new List<AutoApplicantSCBAccount>();
            var autoApplicantSCBAccount = new AutoApplicantSCBAccount();

            queryString = String.Format(@"Select * from auto_applicantscbaccount where AutoLoanMasterId = {0}", autoLoanMasterID);

            reader = objDbHelper.GetDataReader(queryString);
            var autoApplicantSCBAccountDataReader = new AutoApplicantSCBAccountDataReader(reader);
            while (reader.Read())
            {
                autoApplicantSCBAccount = autoApplicantSCBAccountDataReader.Read();
                autoApplicantSCBAccountList.Add(autoApplicantSCBAccount);
            }
            reader.Close();

            autoLoanApplicationAll.AutoApplicantSCBAccountList = autoApplicantSCBAccountList;

            #endregion Auto Applicant SCB Account

            #region Auto Applicant Account Other List

            // already create the list in AutoLoan region

            #endregion Auto Applicant Account Other List

            #region Auto Facility List

            // already create the list in AutoLoan region

            #endregion Auto Facility List

            #region Auto Security List

            // already create the list in AutoLoan region

            #endregion Auto Security List

            #region OFF SCB Facility List

            List<AutoLoanFacilityOffSCB> autoLoanFacilityOffSCBList = new List<AutoLoanFacilityOffSCB>();
            var autoLoanFacilityOffSCB = new AutoLoanFacilityOffSCB();

            queryString = String.Format(@"Select * from auto_loanfacilityoffscb where AnalystMasterId = {0}", analystMasterId);

            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanFacilityOffSCBDataReader = new AutoLoanFacilityOffSCBDataReader(reader);
            while (reader.Read())
            {
                autoLoanFacilityOffSCB = autoLoanFacilityOffSCBDataReader.Read();
                autoLoanFacilityOffSCBList.Add(autoLoanFacilityOffSCB);
            }
            reader.Close();

            autoLoanApplicationAll.AutoLoanFacilityOffSCBList = autoLoanFacilityOffSCBList;

            #endregion OFF SCB Facility List

            #endregion General Tab

            #endregion Loan Appliaction Analyst Part



            return autoLoanApplicationAll;
        }


        private Object getDataFromPLUSS(int llid, int userType)
        {
            var autoLoanApplicationAll = new AutoLoanApplicationAll();
            var autoLoanMasterID = 0;
            var PRaccountID = 0;
            var JTaccountID = 0;
            var AutoVendorID = 0;
            var objDbHelper = new CommonDbHelper();
            string queryString = "";

            //AutoLoanMaster
            var autoLoanMaster = new AutoLoanMaster();

            queryString = String.Format(@"Select * from auto_loanmaster where LLID = {0}", llid);

            IDataReader reader = objDbHelper.GetDataReader(queryString);

            var autoLoanMasterDataReader = new AutoLoanMasterDataReader(reader);
            while (reader.Read())
                autoLoanMaster = autoLoanMasterDataReader.Read();
            reader.Close();
            autoLoanMasterID = autoLoanMaster.Auto_LoanMasterId;
            //AutoLoanMaster Close
            autoLoanMaster.UserType = userType;

            autoLoanApplicationAll.objAutoLoanMaster = autoLoanMaster;

            //AutoPRApplicant
            var autoPRApplicant = new AutoPRApplicant();
            queryString = String.Format(@"Select * from auto_prapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRApplicantDataReader = new AutoPRApplicantDataReader(reader);
            while (reader.Read())
                autoPRApplicant = autoPRApplicantDataReader.Read();
            reader.Close();
            // AutoPRApplican close

            autoLoanApplicationAll.objAutoPRApplicant = autoPRApplicant;

            //AutoJTApplicant
            var autoJTApplicant = new AutoJTApplicant();
            queryString = String.Format(@"Select * from auto_jtapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTApplicantDataReader = new AutoJTApplicantDataReader(reader);
            while (reader.Read())
                autoJTApplicant = autoJTApplicantDataReader.Read();
            reader.Close();
            // AutoJTApplicant close

            autoLoanApplicationAll.objAutoJTApplicant = autoJTApplicant;

            //AutoPRProfession
            var autoPRProfession = new AutoPRProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'PrPrimaryProfessionName',pluss2.PROF_NAME 'PrOtherProfessionName' 
                                        from auto_prprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
					                    left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID
                                        where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRProfessionDataReader = new AutoPRProfessionDataReader(reader);
            while (reader.Read())
                autoPRProfession = autoPRProfessionDataReader.Read();
            reader.Close();
            // AutoPRProfession close

            autoLoanApplicationAll.objAutoPRProfession = autoPRProfession;

            //AutoJTProfession
            var autoJTProfession = new AutoJTProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'JtPrimaryProfessionName',pluss2.PROF_NAME 'JtOtherProfessionName'
                                        from auto_jtprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
                                        left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID 
                                        where auto.AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTProfessionDataReader = new AutoJTProfessionDataReader(reader);
            while (reader.Read())
                autoJTProfession = autoJTProfessionDataReader.Read();
            reader.Close();
            // AutoJTProfession close

            autoLoanApplicationAll.objAutoJTProfession = autoJTProfession;

            //AutoLoanVehicle
            //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1),FALSE) as ValuePackAllowed
            var autoLoanVehicle = new AutoLoanVehicle();
            queryString = String.Format(@"SELECT *,
auto_status.StatusCode,
auto_menufacturer.ManufacturerCode,
auto_vehicle.ModelCode,
auto_vehicle.VehicleTypeCode,
auto_vendor.VendorCode,
IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1) as ValuePackAllowed
FROM
auto_loanvehicle
left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.vehicleId
INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
            while (reader.Read())
                autoLoanVehicle = autoLoanVehicleDataReader.Read();
            reader.Close();
            AutoVendorID = autoLoanVehicle.VendorId;
            // AutoLoanVehicle close

            autoLoanApplicationAll.objAutoLoanVehicle = autoLoanVehicle;

            //AutoPRAccount
            var autoPRAccount = new AutoPRAccount();
            queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
            while (reader.Read())
                autoPRAccount = autoPRAccountDataReader.Read();
            reader.Close();
            PRaccountID = autoPRAccount.PrAccountId;
            // AutoPRAccount close

            autoLoanApplicationAll.objAutoPRAccount = autoPRAccount;

            //AutoJTAccount
            var autoJTAccount = new AutoJTAccount();
            queryString = String.Format(@"Select * from auto_jtaccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTAccountDataReader = new AutoJTAccountDataReader(reader);
            while (reader.Read())
                autoJTAccount = autoJTAccountDataReader.Read();
            reader.Close();
            JTaccountID = autoJTAccount.jtAccountId;
            // AutoJTAccount close

            autoLoanApplicationAll.objAutoJTAccount = autoJTAccount;

            //AutoPRAccountDetail
            List<AutoPRAccountDetail> AutoPRAccountDetailList = new List<AutoPRAccountDetail>();
            var autoPRAccountDetail = new AutoPRAccountDetail();
            queryString = String.Format(@"Select * from auto_praccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDetailDataReader = new AutoPRAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoPRAccountDetail = autoPRAccountDetailDataReader.Read();
                AutoPRAccountDetailList.Add(autoPRAccountDetail);
            }
            reader.Close();
            // AutoPRAccountDetail close

            autoLoanApplicationAll.objAutoPRAccountDetailList = AutoPRAccountDetailList;

            //AutoJTAccountDetail
            List<AutoJTAccountDetail> autoJTAccountDetailList = new List<AutoJTAccountDetail>();
            var autoJTAccountDetail = new AutoJTAccountDetail();
            queryString = String.Format(@"Select * from auto_jtaccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTAccountDetailDataReader = new AutoJTAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoJTAccountDetail = autoJTAccountDetailDataReader.Read();
                autoJTAccountDetailList.Add(autoJTAccountDetail);
            }
            reader.Close();
            // AutoJTAccountDetail close

            autoLoanApplicationAll.objAutoJtAccountDetailList = autoJTAccountDetailList;

            //objAutoPRFinance
            var autoPRFinance = new AutoPRFinance();
            queryString = String.Format(@"Select * from auto_prfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRFinanceDataReader = new AutoPRFinanceDataReader(reader);
            while (reader.Read())
                autoPRFinance = autoPRFinanceDataReader.Read();
            reader.Close();
            // objAutoPRFinance close

            autoLoanApplicationAll.objAutoPRFinance = autoPRFinance;

            //objAutoJTFinance
            var autoJTFinance = new AutoJTFinance();
            queryString = String.Format(@"Select * from auto_jtfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTFinanceDataReader = new AutoJTFinanceDataReader(reader);
            while (reader.Read())
                autoJTFinance = autoJTFinanceDataReader.Read();
            reader.Close();
            // objAutoJTFinance close

            autoLoanApplicationAll.objAutoJTFinance = autoJTFinance;

            //objAutoLoanReference
            var autoLoanReference = new AutoLoanReference();
            queryString = String.Format(@"Select * from auto_loanreference where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanReferenceDataReader = new AutoLoanReferenceDataReader(reader);
            while (reader.Read())
                autoLoanReference = autoLoanReferenceDataReader.Read();
            reader.Close();
            // objAutoLoanReference close

            autoLoanApplicationAll.objAutoLoanReference = autoLoanReference;

            //objAutoLoanFacilityList
            List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
            var autoLoanFacility = new AutoLoanFacility();
            queryString = String.Format(@"Select * from auto_loanfacility where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
            while (reader.Read())
            {
                autoLoanFacility = autoLoanFacilityDataReader.Read();
                autoLoanFacilityList.Add(autoLoanFacility);
            }
            reader.Close();
            // objAutoLoanFacilityList close

            autoLoanApplicationAll.objAutoLoanFacilityList = autoLoanFacilityList;


            //objAutoLoanSecurityList
            List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();
            var autoLoanSecurity = new AutoLoanSecurity();
            queryString = String.Format(@"Select * from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanSecurityDataReader = new AutoLoanSecurityDataReader(reader);
            while (reader.Read())
            {
                autoLoanSecurity = autoLoanSecurityDataReader.Read();
                autoLoanSecurityList.Add(autoLoanSecurity);
            }
            reader.Close();
            // objAutoLoanSecurityList close

            autoLoanApplicationAll.objAutoLoanSecurityList = autoLoanSecurityList;

            //AutoApplicationInsurance
            var autoApplicationInsurance = new AutoApplicationInsurance();
            queryString = String.Format(@"Select * from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoApplicationInsuranceDataReader = new AutoApplicationInsuranceDataReader(reader);
            while (reader.Read())
                autoApplicationInsurance = autoApplicationInsuranceDataReader.Read();
            reader.Close();
            // AutoApplicationInsurance close

            autoLoanApplicationAll.objAutoApplicationInsurance = autoApplicationInsurance;


            //objAutoVendor
            var autoVendor = new AutoVendor();
            queryString = String.Format(@"Select * from auto_vendor where AutoVendorId = {0}", AutoVendorID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoVendorDataReader = new AutoVendorDataReader(reader);
            while (reader.Read())
                autoVendor = autoVendorDataReader.Read();
            reader.Close();
            // objAutoVendor close

            autoLoanApplicationAll.objAutoVendor = autoVendor;


            //AutoPRBankStatementList
            List<AutoPRBankStatement> objAutoPRBankStatementList = new List<AutoPRBankStatement>();
            var autoPRBankStatement = new AutoPRBankStatement();
            queryString = String.Format(@"Select * from auto_prbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRBankStatementDataReader = new AutoPRBankStatementDataReader(reader);
            while (reader.Read())
            {
                autoPRBankStatement = autoPRBankStatementDataReader.Read();
                objAutoPRBankStatementList.Add(autoPRBankStatement);
            }
            reader.Close();
            // AutoPRBankStatementList close

            autoLoanApplicationAll.AutoPRBankStatementList = objAutoPRBankStatementList;


            //AutoJTBankStatementList
            List<AutoJTBankStatement> objAutoJTBankStatementList = new List<AutoJTBankStatement>();
            var autoJTBankStatement = new AutoJTBankStatement();
            queryString = String.Format(@"Select * from auto_jtbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoJTBankStatementDataReader = new AutoJTBankStatementDataReader(reader);
            while (reader.Read())
            {
                autoJTBankStatement = autoJTBankStatementDataReader.Read();
                objAutoJTBankStatementList.Add(autoJTBankStatement);
            }
            reader.Close();
            // AutoJTBankStatementList close

            autoLoanApplicationAll.AutoJTBankStatementList = objAutoJTBankStatementList;


            //AutoBankStatementAvgTotalList
            List<Auto_BankStatementAvgTotal> AutoBankStatementAvgTotalList = new List<Auto_BankStatementAvgTotal>();
            var autoBankStatementAvgTotal = new Auto_BankStatementAvgTotal();
            queryString = String.Format(@"Select * from auto_bankstatementavgtotal where AutoLoanMasterId = {0} order by Type,month asc", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoBankStatementAvgTotalDataReader = new AutoBankStatementAvgTotalDataReader(reader);
            while (reader.Read())
            {
                autoBankStatementAvgTotal = autoBankStatementAvgTotalDataReader.Read();
                AutoBankStatementAvgTotalList.Add(autoBankStatementAvgTotal);
            }
            reader.Close();
            // AutoBankStatementAvgTotalList close

            autoLoanApplicationAll.AutoBankStatementAvgTotalList = AutoBankStatementAvgTotalList;


            return autoLoanApplicationAll;
        }

        private Object getDataFromPLUSSForOps(int llid, int userType)
        {
            var autoLoanApplicationAll = new AutoLoanApplicationAll();
            var autoLoanMasterID = 0;
            var PRaccountID = 0;
            var JTaccountID = 0;
            var AutoVendorID = 0;
            var objDbHelper = new CommonDbHelper();
            string queryString = "";

            //AutoLoanMaster
            var autoLoanMaster = new AutoLoanMaster();

            queryString = String.Format(@"Select * from auto_loanmaster where LLID = {0}", llid);

            IDataReader reader = objDbHelper.GetDataReader(queryString);

            var autoLoanMasterDataReader = new AutoLoanMasterDataReader(reader);
            while (reader.Read())
                autoLoanMaster = autoLoanMasterDataReader.Read();
            reader.Close();
            autoLoanMasterID = autoLoanMaster.Auto_LoanMasterId;
            //AutoLoanMaster Close
            autoLoanMaster.UserType = userType;

            autoLoanApplicationAll.objAutoLoanMaster = autoLoanMaster;

            //AutoPRApplicant
            var autoPRApplicant = new AutoPRApplicant();
            queryString = String.Format(@"Select * from auto_prapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRApplicantDataReader = new AutoPRApplicantDataReader(reader);
            while (reader.Read())
                autoPRApplicant = autoPRApplicantDataReader.Read();
            reader.Close();
            // AutoPRApplican close

            autoLoanApplicationAll.objAutoPRApplicant = autoPRApplicant;

            //AutoJTApplicant
            var autoJTApplicant = new AutoJTApplicant();
            if (autoLoanMaster.JointApplication == true)
            {
                queryString = String.Format(@"Select * from auto_jtapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoJTApplicantDataReader = new AutoJTApplicantDataReader(reader);
                while (reader.Read())
                    autoJTApplicant = autoJTApplicantDataReader.Read();
                reader.Close();
            }
            // AutoJTApplicant close

            autoLoanApplicationAll.objAutoJTApplicant = autoJTApplicant;

            //AutoPRProfession
            var autoPRProfession = new AutoPRProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'PrPrimaryProfessionName',pluss2.PROF_NAME 'PrOtherProfessionName' 
                                        from auto_prprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
					                    left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID
                                        where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRProfessionDataReader = new AutoPRProfessionDataReader(reader);
            while (reader.Read())
                autoPRProfession = autoPRProfessionDataReader.Read();
            reader.Close();
            // AutoPRProfession close

            autoLoanApplicationAll.objAutoPRProfession = autoPRProfession;

            //AutoJTProfession
            var autoJTProfession = new AutoJTProfession();
            if (autoLoanMaster.JointApplication == true)
            {
                queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'JtPrimaryProfessionName',pluss2.PROF_NAME 'JtOtherProfessionName'
                                        from auto_jtprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
                                        left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID 
                                        where auto.AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoJTProfessionDataReader = new AutoJTProfessionDataReader(reader);
                while (reader.Read())
                    autoJTProfession = autoJTProfessionDataReader.Read();
                reader.Close();
            }
            // AutoJTProfession close

            autoLoanApplicationAll.objAutoJTProfession = autoJTProfession;

            //AutoLoanVehicle
            //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1),FALSE) as ValuePackAllowed
            var autoLoanVehicle = new AutoLoanVehicle();
//            queryString = String.Format(@"SELECT *,
//auto_status.StatusCode,
//auto_menufacturer.ManufacturerCode,
//auto_vehicle.ModelCode,
//auto_vehicle.VehicleTypeCode,
//auto_vendor.VendorCode,
//IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
//(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1) as ValuePackAllowed
//FROM
//auto_loanvehicle
//left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
//INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
//left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.vehicleId
//INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
//LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);

            queryString = String.Format(@"SELECT auto_loanvehicle.*,
auto_status.StatusCode,
auto_menufacturer.ManufacturerCode,
auto_vehicle.ModelCode,
auto_vehicle.VehicleTypeCode,
auto_vendor.VendorCode,

IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
auto_brandtenorltv.ValuePackAllowed,
auto_brandtenorltv.Tenor,
auto_brandtenorltv.LTV
FROM
auto_loanvehicle
left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.VehicleId
INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
LEFT OUTER JOIN auto_brandtenorltv on auto_brandtenorltv.VehicleId = auto_vehicle.VehicleId and auto_brandtenorltv.StatusId = auto_loanvehicle.VehicleStatus
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
            while (reader.Read())
                autoLoanVehicle = autoLoanVehicleDataReader.Read();
            reader.Close();
            AutoVendorID = autoLoanVehicle.VendorId;
            // AutoLoanVehicle close

            autoLoanApplicationAll.objAutoLoanVehicle = autoLoanVehicle;

            //AutoPRAccount
            var autoPRAccount = new AutoPRAccount();
            queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
            while (reader.Read())
                autoPRAccount = autoPRAccountDataReader.Read();
            reader.Close();
            PRaccountID = autoPRAccount.PrAccountId;
            // AutoPRAccount close

            autoLoanApplicationAll.objAutoPRAccount = autoPRAccount;

            //AutoJTAccount
            var autoJTAccount = new AutoJTAccount();
            if (autoLoanMaster.JointApplication == true)
            {
                queryString = String.Format(@"Select * from auto_jtaccount where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoJTAccountDataReader = new AutoJTAccountDataReader(reader);
                while (reader.Read())
                    autoJTAccount = autoJTAccountDataReader.Read();
                reader.Close();
                JTaccountID = autoJTAccount.jtAccountId;
                // AutoJTAccount close
            }
            autoLoanApplicationAll.objAutoJTAccount = autoJTAccount;

            //AutoPRAccountDetail
            List<AutoPRAccountDetail> AutoPRAccountDetailList = new List<AutoPRAccountDetail>();
            var autoPRAccountDetail = new AutoPRAccountDetail();
            queryString = String.Format(@"Select * from auto_praccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRAccountDetailDataReader = new AutoPRAccountDetailDataReader(reader);
            while (reader.Read())
            {
                autoPRAccountDetail = autoPRAccountDetailDataReader.Read();
                AutoPRAccountDetailList.Add(autoPRAccountDetail);
            }
            reader.Close();
            // AutoPRAccountDetail close

            autoLoanApplicationAll.objAutoPRAccountDetailList = AutoPRAccountDetailList;

            //AutoJTAccountDetail
            List<AutoJTAccountDetail> autoJTAccountDetailList = new List<AutoJTAccountDetail>();
            if (autoLoanMaster.JointApplication == true)
            {
                var autoJTAccountDetail = new AutoJTAccountDetail();
                queryString = String.Format(@"Select * from auto_jtaccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoJTAccountDetailDataReader = new AutoJTAccountDetailDataReader(reader);
                while (reader.Read())
                {
                    autoJTAccountDetail = autoJTAccountDetailDataReader.Read();
                    autoJTAccountDetailList.Add(autoJTAccountDetail);
                }
                reader.Close();
            }
            // AutoJTAccountDetail close

            autoLoanApplicationAll.objAutoJtAccountDetailList = autoJTAccountDetailList;

            //objAutoPRFinance
            var autoPRFinance = new AutoPRFinance();
            queryString = String.Format(@"Select * from auto_prfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRFinanceDataReader = new AutoPRFinanceDataReader(reader);
            while (reader.Read())
                autoPRFinance = autoPRFinanceDataReader.Read();
            reader.Close();
            // objAutoPRFinance close

            autoLoanApplicationAll.objAutoPRFinance = autoPRFinance;

            //objAutoJTFinance
            var autoJTFinance = new AutoJTFinance();
            if (autoLoanMaster.JointApplication == true)
            {
                queryString = String.Format(@"Select * from auto_jtfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoJTFinanceDataReader = new AutoJTFinanceDataReader(reader);
                while (reader.Read())
                    autoJTFinance = autoJTFinanceDataReader.Read();
                reader.Close();
                // objAutoJTFinance close
            }
            autoLoanApplicationAll.objAutoJTFinance = autoJTFinance;

            //objAutoLoanReference
            var autoLoanReference = new AutoLoanReference();
            queryString = String.Format(@"Select * from auto_loanreference where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanReferenceDataReader = new AutoLoanReferenceDataReader(reader);
            while (reader.Read())
                autoLoanReference = autoLoanReferenceDataReader.Read();
            reader.Close();
            // objAutoLoanReference close

            autoLoanApplicationAll.objAutoLoanReference = autoLoanReference;

            //objAutoLoanFacilityList
            List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
            var autoLoanFacility = new AutoLoanFacility();
            queryString = String.Format(@"Select * from auto_loanfacility where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
            while (reader.Read())
            {
                autoLoanFacility = autoLoanFacilityDataReader.Read();
                autoLoanFacilityList.Add(autoLoanFacility);
            }
            reader.Close();
            // objAutoLoanFacilityList close

            autoLoanApplicationAll.objAutoLoanFacilityList = autoLoanFacilityList;


            //objAutoLoanSecurityList
            List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();
            var autoLoanSecurity = new AutoLoanSecurity();
            queryString = String.Format(@"Select * from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoLoanSecurityDataReader = new AutoLoanSecurityDataReader(reader);
            while (reader.Read())
            {
                autoLoanSecurity = autoLoanSecurityDataReader.Read();
                autoLoanSecurityList.Add(autoLoanSecurity);
            }
            reader.Close();
            // objAutoLoanSecurityList close

            autoLoanApplicationAll.objAutoLoanSecurityList = autoLoanSecurityList;

            //AutoApplicationInsurance
            var autoApplicationInsurance = new AutoApplicationInsurance();
            queryString = String.Format(@"Select * from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoApplicationInsuranceDataReader = new AutoApplicationInsuranceDataReader(reader);
            while (reader.Read())
                autoApplicationInsurance = autoApplicationInsuranceDataReader.Read();
            reader.Close();
            // AutoApplicationInsurance close

            autoLoanApplicationAll.objAutoApplicationInsurance = autoApplicationInsurance;


            //objAutoVendor
            var autoVendor = new AutoVendor();
            queryString = String.Format(@"Select * from auto_vendor where AutoVendorId = {0}", AutoVendorID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoVendorDataReader = new AutoVendorDataReader(reader);
            while (reader.Read())
                autoVendor = autoVendorDataReader.Read();
            reader.Close();
            // objAutoVendor close

            autoLoanApplicationAll.objAutoVendor = autoVendor;


            //AutoPRBankStatementList
            List<AutoPRBankStatement> objAutoPRBankStatementList = new List<AutoPRBankStatement>();
            var autoPRBankStatement = new AutoPRBankStatement();
            queryString = String.Format(@"Select * from auto_prbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoPRBankStatementDataReader = new AutoPRBankStatementDataReader(reader);
            while (reader.Read())
            {
                autoPRBankStatement = autoPRBankStatementDataReader.Read();
                objAutoPRBankStatementList.Add(autoPRBankStatement);
            }
            reader.Close();
            // AutoPRBankStatementList close

            autoLoanApplicationAll.AutoPRBankStatementList = objAutoPRBankStatementList;


            //AutoJTBankStatementList
            List<AutoJTBankStatement> objAutoJTBankStatementList = new List<AutoJTBankStatement>();
            var autoJTBankStatement = new AutoJTBankStatement();
            if (autoLoanMaster.JointApplication == true)
            {
                queryString = String.Format(@"Select * from auto_jtbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
                reader = objDbHelper.GetDataReader(queryString);
                var autoJTBankStatementDataReader = new AutoJTBankStatementDataReader(reader);
                while (reader.Read())
                {
                    autoJTBankStatement = autoJTBankStatementDataReader.Read();
                    objAutoJTBankStatementList.Add(autoJTBankStatement);
                }
                reader.Close();
                // AutoJTBankStatementList close
            }
            

            autoLoanApplicationAll.AutoJTBankStatementList = objAutoJTBankStatementList;


            //AutoBankStatementAvgTotalList
            List<Auto_BankStatementAvgTotal> AutoBankStatementAvgTotalList = new List<Auto_BankStatementAvgTotal>();
            var autoBankStatementAvgTotal = new Auto_BankStatementAvgTotal();
            queryString = String.Format(@"Select * from auto_bankstatementavgtotal where AutoLoanMasterId = {0} order by Type,month asc", autoLoanMasterID);
            reader = objDbHelper.GetDataReader(queryString);
            var autoBankStatementAvgTotalDataReader = new AutoBankStatementAvgTotalDataReader(reader);
            while (reader.Read())
            {
                autoBankStatementAvgTotal = autoBankStatementAvgTotalDataReader.Read();
                AutoBankStatementAvgTotalList.Add(autoBankStatementAvgTotal);
            }
            reader.Close();
            // AutoBankStatementAvgTotalList close

            autoLoanApplicationAll.AutoBankStatementAvgTotalList = AutoBankStatementAvgTotalList;


            return autoLoanApplicationAll;
        }

        private LoanLocatorApplicantInfoByLLID getDataFromLoanLocator(int llid, int userType)
        {
            var loanLocatorApplicantInfoByLLID = new LoanLocatorApplicantInfoByLLID();
            var objDbHelper = new CommonDbHelper("conStringLoanLocator");

            string queryString = String.Format("SELECT  LOAN_APPL_ID, IFNULL((Case RECE_DATE WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(RECE_DATE AS CHAR) END),'0001-01-01') AS RECE_DATE, PROD_ID, CUST_NAME, " +
                                   "LOAN_APP_AMOU, BR_ID, IFNULL(EMPL_TYPE, 'Null') AS EMPL_TYPE, ORGAN, ACC_NUMB, " +
                                   "NAME_LEARNER FROM  loan_app_info left join learner_details_table on PERS_ID=ID_LEARNER " +
                                   "WHERE LOAN_APPL_ID = {0}", llid);

            IDataReader reader = objDbHelper.GetDataReader(queryString);

            var loanLocatorApplicantInfoByLLIDDataReader = new LoanLocatorApplicantInfoByLLIDDataReader(reader);
            while (reader.Read())
                loanLocatorApplicantInfoByLLID = loanLocatorApplicantInfoByLLIDDataReader.Read();
            reader.Close();

            loanLocatorApplicantInfoByLLID.userType = userType;

            //if (loanLocatorLoanAppDataReaderObj != null)
            //{
            //    while (loanLocatorLoanAppDataReaderObj.Read())
            //    {
            //        loanLocatorLoanApplicantObj = new LoanLocatorLoanApplicant();
            //        loanLocatorLoanApplicantObj.LLID = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["LOAN_APPL_ID"].ToString());
            //        loanLocatorLoanApplicantObj.ApplicantName = Convert.ToString(loanLocatorLoanAppDataReaderObj["CUST_NAME"]);
            //        loanLocatorLoanApplicantObj.ProductId = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["PROD_ID"].ToString());
            //        loanLocatorLoanApplicantObj.BranchId = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["BR_ID"].ToString());
            //        loanLocatorLoanApplicantObj.EmployeType = Convert.ToString(loanLocatorLoanAppDataReaderObj["EMPL_TYPE"]);
            //        loanLocatorLoanApplicantObj.Organization = Convert.ToString(loanLocatorLoanAppDataReaderObj["ORGAN"]);
            //        loanLocatorLoanApplicantObj.AccountNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["ACC_NUMB"]);
            //        loanLocatorLoanApplicantObj.LoanApplyAmount = Convert.ToDouble(loanLocatorLoanAppDataReaderObj["LOAN_APP_AMOU"].ToString());
            //        loanLocatorLoanApplicantObj.RecieveDate = Convert.ToDateTime(loanLocatorLoanAppDataReaderObj["RECE_DATE"]);
            //        loanLocatorLoanApplicantObj.SourcePersonName = Convert.ToString(loanLocatorLoanAppDataReaderObj["NAME_LEARNER"]);
            //    }
            //    loanLocatorLoanAppDataReaderObj.Close();
            //    //Bat.Common.Connection.GetConnection("conStringLoanLocator").Close();
            //}

            return loanLocatorApplicantInfoByLLID;

        }

        private LoanLocatorApplicantInfoByLLID getDataFromLoanLocator(int llid)
        {
            var loanLocatorApplicantInfoByLLID = new LoanLocatorApplicantInfoByLLID();
            var objDbHelper = new CommonDbHelper("conStringLoanLocator");

            string queryString = String.Format("SELECT  LOAN_APPL_ID, IFNULL((Case RECE_DATE WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(RECE_DATE AS CHAR) END),'0001-01-01') AS RECE_DATE, PROD_ID, CUST_NAME, " +
                                   "LOAN_APP_AMOU, BR_ID, IFNULL(EMPL_TYPE, 'Null') AS EMPL_TYPE, ORGAN, ACC_NUMB, " +
                                   "NAME_LEARNER FROM  loan_app_info left join learner_details_table on PERS_ID=ID_LEARNER " +
                                   "WHERE LOAN_APPL_ID = {0}", llid);

            IDataReader reader = objDbHelper.GetDataReader(queryString);

            var loanLocatorApplicantInfoByLLIDDataReader = new LoanLocatorApplicantInfoByLLIDDataReader(reader);
            while (reader.Read())
                loanLocatorApplicantInfoByLLID = loanLocatorApplicantInfoByLLIDDataReader.Read();
            reader.Close();

            //loanLocatorApplicantInfoByLLID.userType = userType;

            //if (loanLocatorLoanAppDataReaderObj != null)
            //{
            //    while (loanLocatorLoanAppDataReaderObj.Read())
            //    {
            //        loanLocatorLoanApplicantObj = new LoanLocatorLoanApplicant();
            //        loanLocatorLoanApplicantObj.LLID = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["LOAN_APPL_ID"].ToString());
            //        loanLocatorLoanApplicantObj.ApplicantName = Convert.ToString(loanLocatorLoanAppDataReaderObj["CUST_NAME"]);
            //        loanLocatorLoanApplicantObj.ProductId = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["PROD_ID"].ToString());
            //        loanLocatorLoanApplicantObj.BranchId = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["BR_ID"].ToString());
            //        loanLocatorLoanApplicantObj.EmployeType = Convert.ToString(loanLocatorLoanAppDataReaderObj["EMPL_TYPE"]);
            //        loanLocatorLoanApplicantObj.Organization = Convert.ToString(loanLocatorLoanAppDataReaderObj["ORGAN"]);
            //        loanLocatorLoanApplicantObj.AccountNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["ACC_NUMB"]);
            //        loanLocatorLoanApplicantObj.LoanApplyAmount = Convert.ToDouble(loanLocatorLoanAppDataReaderObj["LOAN_APP_AMOU"].ToString());
            //        loanLocatorLoanApplicantObj.RecieveDate = Convert.ToDateTime(loanLocatorLoanAppDataReaderObj["RECE_DATE"]);
            //        loanLocatorLoanApplicantObj.SourcePersonName = Convert.ToString(loanLocatorLoanAppDataReaderObj["NAME_LEARNER"]);
            //    }
            //    loanLocatorLoanAppDataReaderObj.Close();
            //    //Bat.Common.Connection.GetConnection("conStringLoanLocator").Close();
            //}

            return loanLocatorApplicantInfoByLLID;

        }

        public List<AutoLoanApplicationSource> GetAllSource()
        {
            List<AutoLoanApplicationSource> AutoLoanApplicationSourceListObj = new List<AutoLoanApplicationSource>();
            var objDbHelper = new CommonDbHelper();
            string queryString = "SELECT SCSO_ID,SCSO_NAME FROM t_pluss_scbsource ORDER BY SCSO_NAME ASC";


            IDataReader reader = objDbHelper.GetDataReader(queryString);

            while (reader.Read())
            {
                var autoLoanApplicationSource = new AutoLoanApplicationSource();

                autoLoanApplicationSource.SourceId = Convert.ToInt32(reader.GetValue(0));
                autoLoanApplicationSource.SourceName = Convert.ToString(reader.GetValue(1));

                AutoLoanApplicationSourceListObj.Add(autoLoanApplicationSource);
            }

            return AutoLoanApplicationSourceListObj;
        }

        public List<AutoBank> GetBank(int status)
        {
            var objAutoBankList = new List<AutoBank>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  BANK_NM ";
                string sql = string.Format(@"
select distinct BANK_ID, BANK_CODE, BANK_NM, BANK_STATUS 
from t_pluss_bank where BANK_STATUS = {0} {1}", status, orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                while (reader.Read())
                {
                    var objAutoBank = new AutoBank();

                    objAutoBank.BankID = Convert.ToInt32(reader.GetValue(0));
                    objAutoBank.BankName = Convert.ToString(reader.GetValue(2));
                    objAutoBank.BankCode = Convert.ToString(reader.GetValue(1));

                    objAutoBankList.Add(objAutoBank);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoBankList;
        }

        public List<AutoBranch> GetBranch(int bankID)
        {
            var objAutoBranchList = new List<AutoBranch>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  BRAN_NAME ";
                string sql = string.Format(@"
select distinct BRAN_ID, BRAN_NAME, REGI_NM, BRAN_BANK_ID, BRAN_LOCATION_ID, BRAN_ADDRESS, BRAN_BRABCH_STSTUS, BRAN_STATUS  
from t_pluss_branch where BRAN_BANK_ID = {0} and BRAN_STATUS = 1 {1}", bankID, orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                while (reader.Read())
                {
                    var objAutoBranch = new AutoBranch();

                    objAutoBranch.BRAN_ID = Convert.ToInt32(reader.GetValue(0));
                    objAutoBranch.BRAN_NAME = Convert.ToString(reader.GetValue(1));
                    objAutoBranch.REGI_NM = Convert.ToString(reader.GetValue(2));
                    objAutoBranch.BRAN_BANK_ID = Convert.ToInt32(reader.GetValue(3));
                    objAutoBranch.BRAN_LOCATION_ID = Convert.ToInt32(reader.GetValue(4));
                    objAutoBranch.BRAN_ADDRESS = Convert.ToString(reader.GetValue(5));
                    objAutoBranch.BRAN_BRABCH_STSTUS = Convert.ToInt32(reader.GetValue(6));
                    objAutoBranch.BRAN_STATUS = Convert.ToInt32(reader.GetValue(7));

                    objAutoBranchList.Add(objAutoBranch);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoBranchList;
        }

        public List<AutoVendor> GetVendor()
        {

            var objvendor = new List<AutoVendor>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  VendorName asc";
                //                string sql = string.Format(@"select AutoVendorId,VendorName from auto_vendor
                //where AutoVendorId in (Select AutoVendorId from auto_vendormou)", orderBy);
                //string sql = string.Format(@"select * from auto_vendor where VendorName != ''", orderBy);

                string sql = string.Format(@"select AutoVendorId,VendorName,VendorCode,Address,Phone,Website,isMOU,DealerType,IrWithArta,
IrWithoutArta,ProcessingFee,ValidFrom,StatusName as DealerStatus,IsActive,TotalCount from
(
SELECT s1.AutoVendorId, s1.VendorName,s1.VendorCode, s1.Address, s1.Phone,s1.Website,s1.isMOU,s1.DealerType, s2.IrWithArta,s2.IrWithoutArta,
s2.ProcessingFee,s2.ValidFrom,auto_status.StatusName,s1.IsActive,
(select count(*) from auto_vendor) AS TotalCount
FROM auto_vendor s1
Left outer JOIN auto_vendormou s2 ON s1.AutoVendorId = s2.AutoVendorId
and  s2.LastUpdateDate= 
(select Max(LastUpdateDate) as lsUpdateDate from auto_vendormou as s3 where s3.AutoVendorId=s1.AutoVendorId)
left join auto_status on auto_status.StatusId = s1.DealerType
order by s1.AutoVendorId asc) as tbl where tbl.VendorName != '' {0}", orderBy);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVendorDataReader = new AutoVendorDataReader(reader);
                while (reader.Read())
                    objvendor.Add(autoVendorDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objvendor;
        }

        public List<AutoVehicle> GetAutoModelByManufacturerID(int ManufacturerID)
        {

            var objmodel = new List<AutoVehicle>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string orderBy = "ORDER BY Model asc";
                string sql = string.Format(@"select distinct Model from auto_vehicle where ManufacturerId =" + ManufacturerID + " " + orderBy);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleDataReader = new AutoVehicleDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoVehicleDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoVehicle> GetAutoTrimLevelByModel(int manufacturerID, string model)
        {

            var objmodel = new List<AutoVehicle>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string orderBy = "ORDER BY TrimLevel asc";
                string sql = string.Format(@"select distinct TrimLevel from auto_vehicle where ManufacturerId =" + manufacturerID + " and Model = '" + model + "' " + orderBy);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleDataReader = new AutoVehicleDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoVehicleDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoVehicle> GetAutoEngineCCByModel_TrimLevel(int manufacturerID, string model, string trimLevel)
        {

            var objmodel = new List<AutoVehicle>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select distinct CC,Type from auto_vehicle where ManufacturerId =" + manufacturerID + " and Model = '" + model + "' and TrimLevel = '" + trimLevel + "'");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleDataReader = new AutoVehicleDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoVehicleDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoVehicle> GetAutoVehicleType(int manufacturerID, string model, string engineCC, string TrimLevel)
        {

            var objmodel = new List<AutoVehicle>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select distinct Type from auto_vehicle where 
                                            ManufacturerId ={0} and 
                                            Model = '{1}' and 
                                            CC = {2} and 
                                            TrimLevel = '{3}'", manufacturerID, model, engineCC, TrimLevel);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoVehicleDataReader = new AutoVehicleDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoVehicleDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoCompany> getNameOfCompany()
        {

            var objmodel = new List<AutoCompany>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select AutoCompanyId, CompanyName, Remarks from auto_company");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoCompanyDataReader = new AutoCompanyDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoCompanyDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoPrice> getManufacturingYear(int manufacturerID, string model, string engineCC, string TrimLevel)
        {

            var objmodel = new List<AutoPrice>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select distinct ManufacturingYear from auto_price 
                                            where VehicleId = (Select VehicleId from auto_vehicle where ManufacturerId = {0} and 
                                            Model = '{1}' and TrimLevel = '{2}' and CC = {3} )", manufacturerID, model, TrimLevel, engineCC);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPriceDataReader = new AutoPriceDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoPriceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoPrice> GetPrice(int manufacturerID, string model, string engineCC, string TrimLevel, string manufacturingYear)
        {

            var objmodel = new List<AutoPrice>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select MinimumPrice,Max(auto_price.LastUpdateDate) as LastUpdateDate from auto_price 
                                            where VehicleId = (Select VehicleId from auto_vehicle where ManufacturerId = {0} and 
                                            Model = '{1}' and TrimLevel = '{2}' and CC = {3} and ManufacturingYear = {4} )",
                                            manufacturerID, model, TrimLevel, engineCC, manufacturingYear);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPriceDataReader = new AutoPriceDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoPriceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoPrice> GetPriceByVehicleId(int vehicleId, int manufacturingYear, int vehicleStatus)
        {
            var objmodel = new List<AutoPrice>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select MinimumPrice,Max(auto_price.LastUpdateDate) as LastUpdateDate from auto_price 
                                            where VehicleId = {0} and Manufacturingyear={1} and StatusId={2} group by VehicleId,StatusId,ManufacturingYear", vehicleId, manufacturingYear, vehicleStatus);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPriceDataReader = new AutoPriceDataReader(reader);
                while (reader.Read())
                    objmodel.Add(autoPriceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {

            var objmodel = new List<Auto_InsuranceBanca>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select InsCompanyId,InsType,CompanyName,InsAgeLimit,InsVehicleCC,InsurancePercent,Tenor,ArtaRate, 0 as TotalCount
                                            from auto_insurancebanca");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var auto_InsuranceBancaDataReader = new Auto_InsuranceBancaDataReader(reader);
                while (reader.Read())
                    objmodel.Add(auto_InsuranceBancaDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objmodel;
        }

        public List<AutoPRBankStatement> GetPRBankStatementData(string llid)
        {
            var AutoPRBankStatementList = new List<AutoPRBankStatement>();
            //string sReturnData = String.Empty;
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = @"select a.PrBankStatementId, a.LLID, a.AutoLoanMasterId, a.BankId,b.BANK_NM, a.BranchId,c.BRAN_NAME, a.AccountName, a.AccountNumber, 
	a.AccountCategory,a.Statement, a.AccountType, a.LastUpdateDate, a.StatementDateFrom, 
	a.StatementDateTo from auto_prbankstatement as a,t_pluss_bank as b,t_pluss_branch as c  
where a.BankID = b.Bank_ID and 
a.BranchId = c.bran_ID and 
a.BankID = c.bran_bank_ID and 
a.LLID = " + llid;

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPRBankStatementDataReader = new AutoPRBankStatementDataReader(reader);
                while (reader.Read())
                    AutoPRBankStatementList.Add(autoPRBankStatementDataReader.Read());
                reader.Close();

            }
            catch (Exception ex)
            {
                objDbHelper.Close();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }


            return AutoPRBankStatementList;
        }

        public List<AutoJTBankStatement> GetJTBankStatementData(string llid)
        {
            var AutoJTBankStatementList = new List<AutoJTBankStatement>();
            //string sReturnData = String.Empty;
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = @"select a.JtBankStatementId, a.LLID, a.AutoLoanMasterId, a.BankId,b.BANK_NM, a.BranchId,c.BRAN_NAME, a.AccountName, a.AccountNumber, 
	a.AccountCategory,a.Statement, a.AccountType, a.LastUpdateDate, a.StatementDateFrom, 
	a.StatementDateTo from auto_jtbankstatement as a,t_pluss_bank as b,t_pluss_branch as c  
where a.BankID = b.Bank_ID and 
a.BranchId = c.bran_ID and 
a.BankID = c.bran_bank_ID and 
a.LLID = " + llid;

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoJTBankStatementDataReader = new AutoJTBankStatementDataReader(reader);
                while (reader.Read())
                    AutoJTBankStatementList.Add(autoJTBankStatementDataReader.Read());
                reader.Close();

            }
            catch (Exception ex)
            {
                objDbHelper.Close();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }


            return AutoJTBankStatementList;
        }

        public List<AutoGetBankStatement> getBankStatement(string llid)
        {
            var AutoGetBankStatementList = new List<AutoGetBankStatement>();
            //var AutoGetBankStatementObj = new AutoGetBankStatement();

            ////returnValue.Insert(0, GetPRBankStatementData(llid));
            ////returnValue.Insert(1, GetJTBankStatementData(llid));

            //AutoGetBankStatementObj.PRStatement = GetPRBankStatementData(llid);
            //AutoGetBankStatementObj.JTStatement = GetJTBankStatementData(llid);

            //AutoGetBankStatementList.Add(AutoGetBankStatementObj);
            return AutoGetBankStatementList;
        }

        public List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid)
        {
            var SearchParantBankStatementInformationList = new List<SearchParantBankStatementInformation>();
            //string sReturnData = String.Empty;
            var objDbHelper = new CommonDbHelper();
            try
            {

                string sql =
                    @"SELECT
                            auto_loanmaster.Auto_LoanMasterId,
                            auto_loanmaster.LLID,
                            auto_prapplicant.PrName,
                            auto_prapplicant.DateofBirth AS PrDateOfBirth,
                            auto_prapplicant.ResidenceAddress,
                            t_pluss_profession.PROF_NAME AS PrProfession,
                            auto_jtapplicant.JtName,
                            auto_jtapplicant.DateofBirth AS JtDateOfBirth,
                            a.PROF_NAME AS Jtprofession,
                            auto_loanmaster.JointApplication,
                            auto_prfinance.DPIAmount AS PrIncomeAmount,
                            auto_jtfinance.DPIAmount AS JtIncomeAmount

                            FROM
                            auto_loanmaster
                            INNER JOIN auto_prapplicant ON auto_loanmaster.Auto_LoanMasterId = auto_prapplicant.Auto_LoanMasterId
                            INNER JOIN auto_prprofession ON auto_loanmaster.Auto_LoanMasterId = auto_prprofession.AutoLoanMasterId
                            INNER JOIN t_pluss_profession ON auto_prprofession.PrimaryProfession = t_pluss_profession.PROF_ID
                            INNER JOIN auto_jtapplicant ON auto_loanmaster.Auto_LoanMasterId = auto_jtapplicant.Auto_LoanMasterId 
                            INNER JOIN auto_jtprofession ON auto_loanmaster.Auto_LoanMasterId = auto_jtprofession.AutoLoanMasterId 
                            INNER JOIN t_pluss_profession as a ON auto_jtprofession.PrimaryProfession = a.PROF_ID
                            INNER JOIN auto_prfinance ON auto_loanmaster.Auto_LoanMasterId = auto_prfinance.AutoLoanMasterId
                            INNER JOIN auto_jtfinance ON auto_loanmaster.Auto_LoanMasterId = auto_jtfinance.AutoLoanMasterId 
                            where auto_loanmaster.llid = " + llid;


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var searchParantBankStatementInformationDataReader = new SearchParantBankStatementInformationDataReader(reader);
                while (reader.Read())
                    SearchParantBankStatementInformationList.Add(searchParantBankStatementInformationDataReader.Read());
                reader.Close();

            }
            catch (Exception ex)
            {
                objDbHelper.Close();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }



            return SearchParantBankStatementInformationList;
        }

        public List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            var SearchParantBankStatementInformationList = new List<SearchParantBankStatementInformation>();
            //string sReturnData = String.Empty;
            var objDbHelper = new CommonDbHelper();
            try
            {
                string condition = "StatusID in (";
                foreach (AutoUserTypeVsWorkflow a in statePermission)
                {
                    if (statePermission.IndexOf(a) == statePermission.Count - 1)
                    {
                        condition += a.WorkFlowID.ToString() + ")";
                    }
                    else
                    {
                        condition += a.WorkFlowID.ToString() + ",";
                    }
                }

                //                string sql =
                //                    @"SELECT
                //                            auto_loanmaster.Auto_LoanMasterId,
                //                            auto_loanmaster.LLID,
                //                            auto_prapplicant.PrName,
                //                            auto_prapplicant.DateofBirth AS PrDateOfBirth,
                //                            auto_prapplicant.ResidenceAddress,
                //                            t_pluss_profession.PROF_NAME AS PrProfession,
                //                            auto_jtapplicant.JtName,
                //                            auto_jtapplicant.DateofBirth AS JtDateOfBirth,
                //                            a.PROF_NAME AS Jtprofession,
                //                            auto_loanmaster.JointApplication,
                //                            auto_prfinance.DPIAmount AS PrIncomeAmount,
                //                            auto_jtfinance.DPIAmount AS JtIncomeAmount
                //
                //                            FROM
                //                            auto_loanmaster
                //                            INNER JOIN auto_prapplicant ON auto_loanmaster.Auto_LoanMasterId = auto_prapplicant.Auto_LoanMasterId
                //                            INNER JOIN auto_prprofession ON auto_loanmaster.Auto_LoanMasterId = auto_prprofession.AutoLoanMasterId
                //                            INNER JOIN t_pluss_profession ON auto_prprofession.PrimaryProfession = t_pluss_profession.PROF_ID
                //                            INNER JOIN auto_jtapplicant ON auto_loanmaster.Auto_LoanMasterId = auto_jtapplicant.Auto_LoanMasterId 
                //                            INNER JOIN auto_jtprofession ON auto_loanmaster.Auto_LoanMasterId = auto_jtprofession.AutoLoanMasterId 
                //                            INNER JOIN t_pluss_profession as a ON auto_jtprofession.PrimaryProfession = a.PROF_ID
                //                            INNER JOIN auto_prfinance ON auto_loanmaster.Auto_LoanMasterId = auto_prfinance.AutoLoanMasterId
                //                            INNER JOIN auto_jtfinance ON auto_loanmaster.Auto_LoanMasterId = auto_jtfinance.AutoLoanMasterId 
                //                            where auto_loanmaster.llid = " + llid + " and auto_loanmaster." + condition;

                string sql =
                    @"SELECT
                            auto_loanmaster.Auto_LoanMasterId,
                            auto_loanmaster.LLID,
                            auto_prapplicant.PrName,
                            auto_prapplicant.DateofBirth AS PrDateOfBirth,
                            auto_prapplicant.ResidenceAddress,
                            t_pluss_profession.PROF_NAME AS PrProfession,
                            auto_jtapplicant.JtName,
                            auto_jtapplicant.DateofBirth AS JtDateOfBirth,
                            a.PROF_NAME AS Jtprofession,
                            auto_loanmaster.JointApplication,
                            auto_prfinance.DPIAmount AS PrIncomeAmount,
                            auto_jtfinance.DPIAmount AS JtIncomeAmount

                            FROM
                            auto_loanmaster
                            INNER JOIN auto_prapplicant ON auto_loanmaster.Auto_LoanMasterId = auto_prapplicant.Auto_LoanMasterId
                            INNER JOIN auto_prprofession ON auto_loanmaster.Auto_LoanMasterId = auto_prprofession.AutoLoanMasterId
                            INNER JOIN t_pluss_profession ON auto_prprofession.PrimaryProfession = t_pluss_profession.PROF_ID
                            left JOIN auto_jtapplicant ON auto_loanmaster.Auto_LoanMasterId = auto_jtapplicant.Auto_LoanMasterId 
                            left JOIN auto_jtprofession ON auto_loanmaster.Auto_LoanMasterId = auto_jtprofession.AutoLoanMasterId 
                            Left outer JOIN t_pluss_profession as a ON auto_jtprofession.PrimaryProfession = a.PROF_ID
                            INNER JOIN auto_prfinance ON auto_loanmaster.Auto_LoanMasterId = auto_prfinance.AutoLoanMasterId
                            left JOIN auto_jtfinance ON auto_loanmaster.Auto_LoanMasterId = auto_jtfinance.AutoLoanMasterId 
                            where auto_loanmaster.llid = " + llid + " and auto_loanmaster." + condition; //+ "LIMIT 1,1";


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var searchParantBankStatementInformationDataReader = new SearchParantBankStatementInformationDataReader(reader);
                while (reader.Read())
                    SearchParantBankStatementInformationList.Add(searchParantBankStatementInformationDataReader.Read());
                reader.Close();

            }
            catch (Exception ex)
            {
                objDbHelper.Close();
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }



            return SearchParantBankStatementInformationList;
        }


        #endregion get inforamtion


        #endregion

        #region Workflow Status

        public List<AutoUserTypeVsWorkflow> LoadState(int userType) {
            var objAutoStatusList = new List<AutoUserTypeVsWorkflow>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "ORDER BY  autoworkflowstatus.StatusName asc";
                string sql = string.Format(@"
                                SELECT
auto_usertype_vs_workflow.ID,
auto_usertype_vs_workflow.UserTypeID,
auto_usertype_vs_workflow.WorkFlowID,
autoworkflowstatus.StatusName
FROM
auto_usertype_vs_workflow
INNER JOIN autoworkflowstatus ON autoworkflowstatus.StatusID = auto_usertype_vs_workflow.WorkFlowID
where auto_usertype_vs_workflow.UserTypeID = {0}", userType, orderBy);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoStatusDataReader = new AutoUserTypeVsWorkflowDataReader(reader);
                while (reader.Read())
                    objAutoStatusList.Add(autoStatusDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoStatusList;
        }

        #endregion

    }
}