﻿using System;
using System.Collections.Generic;
using AutoLoanService.Interface;
using AutoLoanService.Entity;
using Azolution.Common.DataService;
using AutoLoanDataService.DataReader;
using System.Data;
using System.Configuration;
using Bat.Common;
using System.Data.OleDb;
using System.Linq;

namespace AutoLoanDataService
{
    public class AutoLoanAssessmentDataService : AutoLoanAssessmentRrepository
    {

        public List<PlussSector> GetAllPlussSector()
        {
            List<PlussSector> PlussSectorList = new List<PlussSector>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select * from t_pluss_sector");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var plussSectorDataReader = new PlussSectorDataReader(reader);
                while (reader.Read())
                    PlussSectorList.Add(plussSectorDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }
            return PlussSectorList;

        }

        public List<PlussSubSector> GetPlussSubSectorBySectorID(int SectorID)
        {
            List<PlussSubSector> PlussSubSectorList = new List<PlussSubSector>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select * from t_pluss_subsector where SUSE_SECTOR_ID = {0}",SectorID);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var plussSubSectorDataReader = new PlussSubSectorDataReader(reader);
                while (reader.Read())
                    PlussSubSectorList.Add(plussSubSectorDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }




            return PlussSubSectorList;
        }

        public List<PlussIncomeAcesmentMethod> GetPlussIncomeAcesmentMethod()
        {
            List<PlussIncomeAcesmentMethod> PlussIncomeAcesmentMethodList = new List<PlussIncomeAcesmentMethod>();
            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"select * from t_pluss_incomeacesmentmethod");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var plussIncomeAcesmentMethodDataReader = new PlussIncomeAcesmentMethodDataReader(reader);
                while (reader.Read())
                    PlussIncomeAcesmentMethodList.Add(plussIncomeAcesmentMethodDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }


            return PlussIncomeAcesmentMethodList;
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize)
        {
            List<AutoLoanSummary> AutoLoanSummaryList = new List<AutoLoanSummary>();

            var objDbHelper = new CommonDbHelper();

            try
            {
                string sql = string.Format(@"SELECT
                                                autoloansummary.Auto_LoanMasterId,
                                                autoloansummary.LLID,
                                                autoloansummary.PrName,
                                                autoloansummary.ProductName,
                                                autoloansummary.ManufacturerName,
                                                autoloansummary.Model,
                                                autoloansummary.AskingTenor,
                                                autoloansummary.AppliedAmount,
                                                autoloansummary.StatusID,
                                                autoloansummary.StatusName,
                                                (Select COUNT(Auto_LoanMasterId) from autoloansummary) as TotalCount
                                                FROM
(select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` 
from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`)))) as autoloansummary

LIMIT {0}, {1}", pageNo, pageSize);

//                string sql = string.Format(@"select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
//`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
//`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
//`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
//`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
//(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` 
//from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
//join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
//left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`))) LIMIT {0}, {1}", pageNo, pageSize);


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoLoanSummaryDataReader = new AutoLoanSummaryDataReader(reader);
                while (reader.Read())
                    AutoLoanSummaryList.Add(autoLoanSummaryDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }

            return AutoLoanSummaryList;
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize, int searchKey, List<AutoUserTypeVsWorkflow> statePermission)
        {
            List<AutoLoanSummary> AutoLoanSummaryList = new List<AutoLoanSummary>();

            var objDbHelper = new CommonDbHelper();

            try
            {
                var condition = "";
                if (searchKey !=0)
                {
                    condition = "Where auto_loanmaster.LLID =" + searchKey + "";
                    //condition = "Where LLID =" + searchKey + "";

                    if(statePermission.Count > 0)
                    {
                        condition += " and auto_loanmaster.StatusID in (";
                        foreach(AutoUserTypeVsWorkflow autwf in statePermission)
                        {
                            condition += autwf.WorkFlowID.ToString();

                            if(statePermission.IndexOf(autwf) != statePermission.Count - 1)
                            {
                                condition += ",";
                            }
                            else
                            {
                                condition += ")";
                            }
                        }
                    }
                }
                else
                {
                    if (statePermission.Count > 0)
                    {
                        condition += " Where auto_loanmaster.StatusID in (";
                        foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                        {
                            condition += autwf.WorkFlowID.ToString();

                            if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                            {
                                condition += ",";
                            }
                            else
                            {
                                condition += ")";
                            }
                        }
                    }
                }


                string sql = string.Format(@"select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` ,
(Select COUNT(Auto_LoanMasterId) from auto_loanmaster {2}) as TotalCount
from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`)))
{2} LIMIT {0}, {1}", pageNo, pageSize, condition);


//                string sql = string.Format(@"select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
//`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
//`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
//`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
//`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
//(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` 
//from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
//join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
//left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`))) {2} LIMIT {0}, {1}", pageNo, pageSize, condition, condition);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoLoanSummaryDataReader = new AutoLoanSummaryDataReader(reader);
                while (reader.Read())
                    AutoLoanSummaryList.Add(autoLoanSummaryDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }

            return AutoLoanSummaryList;
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAllForAnalyst(int pageNo, int pageSize, string searchKey, List<AutoUserTypeVsWorkflow> statePermission)
        {
            List<AutoLoanSummary> AutoLoanSummaryList = new List<AutoLoanSummary>();

            var objDbHelper = new CommonDbHelper();

            try
            {
                var condition = "";
                var src = searchKey.Split('^');
                if (src[0] != "0")
                {
                    condition = "Where auto_loanmaster.LLID =" + searchKey + "";
                    //condition = "Where LLID =" + searchKey + "";
                    if (src[1] == "-1")
                    {
                        if (statePermission.Count > 0)
                        {
                            condition += " and auto_loanmaster.StatusID in (";
                            foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                            {
                                condition += autwf.WorkFlowID.ToString();

                                if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                                {
                                    condition += ",";
                                }
                                else
                                {
                                    condition += ")";
                                }
                            }
                        }
                    }
                    else {
                        condition += " and auto_loanmaster.StatusID =" + src[1] + "";
                    }
                }
                else
                {
                    if (src[1] == "-1")
                    {
                        if (statePermission.Count > 0)
                        {
                            condition += " Where auto_loanmaster.StatusID in (";
                            foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                            {
                                condition += autwf.WorkFlowID.ToString();

                                if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                                {
                                    condition += ",";
                                }
                                else
                                {
                                    condition += ")";
                                }
                            }
                        }
                    }
                    else {
                        condition += " Where auto_loanmaster.StatusID =" + src[1];
                    }
                }


                string sql = string.Format(@"select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` ,
(Select COUNT(Auto_LoanMasterId) from auto_loanmaster {2}) as TotalCount
from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`)))
{2} LIMIT {0}, {1}", pageNo, pageSize, condition);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoLoanSummaryDataReader = new AutoLoanSummaryDataReader(reader);
                while (reader.Read())
                    AutoLoanSummaryList.Add(autoLoanSummaryDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }

            return AutoLoanSummaryList;
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAllForApprover(int pageNo, int pageSize, int searchKey, List<AutoUserTypeVsWorkflow> statePermission, int userID)
        {
            List<AutoLoanSummary> AutoLoanSummaryList = new List<AutoLoanSummary>();

            var objDbHelper = new CommonDbHelper();

            try
            {
                var condition = "";
                if (searchKey != 0)
                {
                    condition = "Where auto_loanmaster.LLID =" + searchKey + "";

                    if (statePermission.Count > 0)
                    {
                        condition += " and auto_loanmaster.StatusID in (";
                        foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                        {
                            condition += autwf.WorkFlowID.ToString();

                            if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                            {
                                condition += ",";
                            }
                            else
                            {
                                condition += ")";
                            }
                        }
                    }
                }
                else
                {
                    if (statePermission.Count > 0)
                    {
                        condition += " Where auto_loanmaster.StatusID in (";
                        foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                        {
                            condition += autwf.WorkFlowID.ToString();

                            if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                            {
                                condition += ",";
                            }
                            else
                            {
                                condition += ")";
                            }
                        }
                    }
                }

                if(condition == "")
                {
                    condition = " Where ApprovedByUserID = " + userID;
                }
                else
                {
                    condition += " and ApprovedByUserID=" + userID;
                }


                string sql = string.Format(@"select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName`, 
(Select COUNT(Auto_LoanMasterId) from auto_loanmaster {2}) as TotalCount
from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`)))

{2} LIMIT {0}, {1}", pageNo, pageSize, condition);


               

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoLoanSummaryDataReader = new AutoLoanSummaryDataReader(reader);
                while (reader.Read())
                    AutoLoanSummaryList.Add(autoLoanSummaryDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }

            return AutoLoanSummaryList;
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize, int searchKey)
        {
            List<AutoLoanSummary> AutoLoanSummaryList = new List<AutoLoanSummary>();

            var objDbHelper = new CommonDbHelper();

            try
            {

                var condition = "";
                if (searchKey !=0)
                {
                    condition = "Where auto_loanmaster.LLID =" + searchKey + "";
                }

                //We need to optimize Total Count. Now Total Count found by same quary twice. It is urgent delivery that's why not optimize just fullfill Customer Requiremnt.
//                string sql = string.Format(@"
//
//select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
//`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
//`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
//`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
//`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
//(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` ,
//(select COUNT(*) from (
//select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
//`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
//`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
//`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
//`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
//(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName`
//from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
//join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
//left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`))) {2} ) as totalCountTable) as TotalCount
//
//from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
//join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
//left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
//left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`)))
//{2} LIMIT {0}, {1}", pageNo, pageSize, condition);

                string sql = string.Format(@"

select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName` ,
(select COUNT(*) from (
select `auto_loanmaster`.`Auto_LoanMasterId` AS `Auto_LoanMasterId`,`auto_loanmaster`.`LLID` AS `LLID`,
`auto_prapplicant`.`PrName` AS `PrName`,`auto_menufacturer`.`ManufacturerName` AS `ManufacturerName`,`auto_loanvehicle`.`Model` AS `Model`,
`auto_loanmaster`.`AskingTenor` AS `AskingTenor`,`auto_loanmaster`.`AppliedAmount` AS `AppliedAmount`,`auto_loanmaster`.`StatusID` AS `StatusID`,
`autoworkflowstatus`.`StatusName` AS `StatusName`,`auto_loananalystmaster`.`AnalystMasterId` AS `AnalystMasterId`,
`auto_an_finalizationappraiserandapprover`.`ApprovedByUserID` AS `ApprovedByUserID`,
(case `auto_loanmaster`.`ProductId` when 1 then 'Staff Auto' when 2 then 'Conventional Auto' when 3 then 'Saadiq Auto' end) AS `ProductName`
from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`))) {2} ) as totalCountTable) as TotalCount

from ((((((`auto_loanmaster` join `auto_prapplicant` on((`auto_prapplicant`.`Auto_LoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_loanvehicle` on((`auto_loanvehicle`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_menufacturer` on((`auto_menufacturer`.`ManufacturerId` = `auto_loanvehicle`.`Auto_ManufactureId`))) 
join `autoworkflowstatus` on((`autoworkflowstatus`.`StatusID` = `auto_loanmaster`.`StatusID`))) 
left join `auto_loananalystmaster` on((`auto_loananalystmaster`.`AutoLoanMasterId` = `auto_loanmaster`.`Auto_LoanMasterId`))) 
left join `auto_an_finalizationappraiserandapprover` on((`auto_an_finalizationappraiserandapprover`.`AnalystMasterId` = `auto_loananalystmaster`.`AnalystMasterId`)))
{2} LIMIT {0}, {1}", pageNo, pageSize, condition);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoLoanSummaryDataReader = new AutoLoanSummaryDataReader(reader);
                while (reader.Read())
                    AutoLoanSummaryList.Add(autoLoanSummaryDataReader.Read());
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                objDbHelper.Close();
            }

            return AutoLoanSummaryList;
        }

        public string AppealedByAutoLoanMasterId(AutoStatusHistory objAutoStatusHistory)
        {

         //  var con = CommonConnection();
            
            var dbHelper = new CommonDbHelper();
            var Result = "";
            int llid = 0;
            try
            {
                string orderBy = "";
                string sql = string.Format(@"update auto_loanmaster set StatusID =" + objAutoStatusHistory.StatusID + " where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId+ "");
                DbQueryManager.ExecuteNonQuery(sql);
              //  dbHelper.ExecuteNonQuery(sql);

                sql = "select LLID from auto_loanmaster where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId;
                llid = Convert.ToInt32(DbQueryManager.ExecuteScalar(sql));
               

                if (llid > 0)
                {
                    sql = String.Format("UPDATE loc_loan_app_info SET LOAN_STATUS_ID = {1} " +
                                   "WHERE LOAN_APPL_ID = {0}", llid, objAutoStatusHistory.StatusID);
                    DbQueryManager.ExecuteNonQuery(sql);
                    //var dbHelper1 = new CommonDbHelper("conStringLoanLocator");

                    //dbHelper1.ExecuteNonQuery(sql);//.ExecuteNonQuery(sql, "conStringLoanLocator");

                }

                AutoLoanDataService autoLoanDataService = new AutoLoanDataService();
                autoLoanDataService.SaveAutoStatusHistory(objAutoStatusHistory);
                Result = "Success";
            }
            catch (Exception ex)
            {
               // dbHelper.RollBack();
                Result = "Failed to Save";
            }
          
            return Result;
        }


        public string AppealedByAutoLoanMasterId(AutoStatusHistory objAutoStatusHistory, string remark)
        {
            var dbHelper = new CommonDbHelper();
            var Result = "";
            int llid = 0;
            try
            {
                string orderBy = "";
                string sql = string.Format(@"update auto_loanmaster set 
                                            StatusID =" + objAutoStatusHistory.StatusID + " where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId + "");
                dbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"update auto_loananalystmaster set ApproverRemark = '" + remark + "' where AutoLoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId + "");
                dbHelper.ExecuteNonQuery(sql);


                sql = "select LLID from auto_loanmaster where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId;

                IDataReader reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    llid = reader.GetInt32(0);
                }
                reader.Close();

                if (llid > 0)
                {
                    sql = String.Format("UPDATE loan_app_info SET LOAN_STATUS_ID = {1} " +
                                   "WHERE LOAN_APPL_ID = {0}", llid, objAutoStatusHistory.StatusID);

                    var dbHelper1 = new CommonDbHelper("conStringLoanLocator");

                    dbHelper1.ExecuteNonQuery(sql);//.ExecuteNonQuery(sql, "conStringLoanLocator");

                }

                AutoLoanDataService autoLoanDataService = new AutoLoanDataService();
               // autoLoanDataService.SaveAutoStatusHistory(objAutoStatusHistory, dbHelper);
                autoLoanDataService.SaveAutoStatusHistory(objAutoStatusHistory);
                Result = "Success";
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                Result = "Failed to Save";
            }
            finally
            {
                dbHelper.Close();
            }
            return Result;
        }

        public string ChangeStateForAnalyst(AutoStatusHistory objAutoStatusHistory, string remark) 
        {
            var dbHelper = new CommonDbHelper();
            var Result = "";
            int llid = 0;
            try
            {
                dbHelper.BeginTransaction();
                string orderBy = "";
                string sql = string.Format(@"update auto_loanmaster set 
                                            StatusID =" + objAutoStatusHistory.StatusID + " where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId + "");
                dbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"update auto_loananalystmaster set AnalystRemark = '" + remark + "' where AutoLoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId + "");
                dbHelper.ExecuteNonQuery(sql);


                sql = "select LLID from auto_loanmaster where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId;

                IDataReader reader = dbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    llid = reader.GetInt32(0);
                }
                reader.Close();

                if (llid > 0)
                {
                    sql = String.Format("UPDATE loan_app_info SET LOAN_STATUS_ID = {1} " +
                                   "WHERE LOAN_APPL_ID = {0}", llid, objAutoStatusHistory.StatusID);

                    var dbHelper1 = new CommonDbHelper("conStringLoanLocator");

                    dbHelper1.ExecuteNonQuery(sql);//.ExecuteNonQuery(sql, "conStringLoanLocator");

                }

                AutoLoanDataService autoLoanDataService = new AutoLoanDataService();
                //autoLoanDataService.SaveAutoStatusHistory(objAutoStatusHistory, dbHelper);
                autoLoanDataService.SaveAutoStatusHistory(objAutoStatusHistory);


                dbHelper.CommitTransaction();
                Result = "Success";
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                Result = "Failed to Save";
            }
            finally
            {
                dbHelper.Close();
            }
            return Result;
        }

        public AutoLoanVehicle CheckMouByVendoreId(int vendoreId)
        {
            var dbHelper = new CommonDbHelper();
            var objAutoLoanVehicle = new AutoLoanVehicle();
            objAutoLoanVehicle = null;
            try
            {
                string sql = string.Format(@"SELECT
auto_vendor.AutoVendorId,
auto_vendor.VendorName,
auto_vendor.VendorCode,
auto_vendor.DealerType,
auto_vendor.Address,
auto_vendor.Phone,
auto_vendor.Website,
auto_vendor.isMOU,
auto_negativevendor.NegativeVendorId,
IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus
FROM
auto_vendor
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId
where auto_vendor.AutoVendorId = {0}", vendoreId);
                IDataReader reader = dbHelper.GetDataReader(sql);

                var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
                if (reader.Read())
                    objAutoLoanVehicle = autoLoanVehicleDataReader.Read();
                reader.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return objAutoLoanVehicle;
        }



        public List<AutoPayrollRate> GetEmployerForSalaried()
        {
           
            var objAutoPayrollRate = new List<AutoPayrollRate>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = string.Format(@"
select auto_company.AutoCompanyId, auto_company.CompanyName,auto_company.CompanyCode,auto_payrollrate.AutoPayRateId,
auto_payrollrate.AutoCompanyId,auto_payrollrate.CategoryName,auto_payrollrate.PayrollRate,auto_payrollrate.ProcessingFee,
Max(auto_payrollrate.LastUpdateDate) as LastUpdateDate, 0 as TotalCount
from auto_company
inner join auto_payrollrate on auto_company.AutoCompanyId = auto_payrollrate.AutoCompanyId
group by auto_company.AutoCompanyId
");

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoPayrollRateDataReader = new AutoPayrollRateDataReader(reader);
                while (reader.Read())
                    objAutoPayrollRate.Add(autoPayrollRateDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoPayrollRate;

        }

        public List<AutoSegmentSettings> GetEmployeSegment()
        {
            var objAutoSegmentSettings = new List<AutoSegmentSettings>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = string.Format(@"Select * from auto_segment");

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoSegmentSettingsDataReader = new AutoSegmentSettingsDataReader(reader);
                while (reader.Read())
                    objAutoSegmentSettings.Add(autoSegmentSettingsDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoSegmentSettings;
        }

        public List<auto_CibInfo> getCibInfo() {
            var objauto_CibInfo = new List<auto_CibInfo>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = string.Format(@"Select * from auto_CibInfo");

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoCibInfoDataReader = new auto_CibInfoDataReader(reader);
                while (reader.Read())
                    objauto_CibInfo.Add(autoCibInfoDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objauto_CibInfo;
        }

        //quary = INSERT INTO qa_discountcoupons (status_code) VALUES (5); SELECT LAST_INSERT_ID()
        //res = dbh.ex(Quary);



        private int SaveAutoLoanAnalystMaster_1(Auto_LoanAnalystMaster objAutoLoanAnalystMaster)
        {
            var dbHelper = new CommonDbHelper();

            var analystMasterId = 0;
            try
            {

                var param = new[]
                               {
                                   new SQLParam("@LastInsertedId", 0, ParameterDirection.Output)
                               };


                string orderBy = "";
                string sql = string.Format(@"INSERT INTO auto_loananalystmaster(LlId,AutoLoanMasterId,IsJoint,ReasonOfJoint)
                VALUES (" + objAutoLoanAnalystMaster.LlId + "," + objAutoLoanAnalystMaster.AutoLoanMasterId + "," + 
                          objAutoLoanAnalystMaster.IsJoint + "," + objAutoLoanAnalystMaster.ReasonOfJoint + ") ");



                dbHelper.ExecuteNonQuery(sql, param);

                if (analystMasterId == 0)
                {
                    analystMasterId = (int)param[0].Value;
                }






            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
                analystMasterId = 0;
            }
            finally
            {
                dbHelper.Close();
            }
            return analystMasterId;
        }
        private void SaveAutoLoanAnalystVehicleDetails_1(Auto_LoanAnalystVehicleDetail objAutoAnVehicleDetail, CommonDbHelper dbHelper)
        {
            // var dbHelper = new CommonDbHelper();

            try
            {


                string orderBy = "";
                string sql =
                    string.Format(
                        @"INSERT INTO auto_loananalystvehicledetail(AnalystMasterId,valuePackAllowed,SeatingCapacity,ConsideredPrice,PriceConsideredOn,CarVarification,DeliveryStatus)
                        VALUES (" +
                        objAutoAnVehicleDetail.AnalystMasterId + "," + objAutoAnVehicleDetail.valuePackAllowed + "," +
                        objAutoAnVehicleDetail.SeatingCapacity + "," + objAutoAnVehicleDetail.ConsideredPrice + "," +
                        objAutoAnVehicleDetail.PriceConsideredOn + "," + objAutoAnVehicleDetail.CarVarification + "," +
                        objAutoAnVehicleDetail.DeliveryStatus +
                        ")");

                dbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }


        }
        private void SaveAutoLoanAnalystApplication_1(Auto_LoanAnalystApplication objAutoLoanAnalystApplication, CommonDbHelper dbHelper)
        {
            // var dbHelper = new CommonDbHelper();

            try
            {
                string orderBy = "";
                string sql =
                    string.Format(
                        @"INSERT INTO auto_loananalystapplication(AnalystMasterId,Source,BranchId,BranchCode,ValuePack,BorrowingRelationShip,BorrowingRelationShipCode,CustomerStatus,AskingRepaymentMethod,ARTA,ARTAStatus,GeneralInsurance,CashSecured100Per,CCBundle)
                          VALUES (" + objAutoLoanAnalystApplication.AnalystMasterId + "," + objAutoLoanAnalystApplication.Source + "," + objAutoLoanAnalystApplication.BranchId + "," + objAutoLoanAnalystApplication.BranchCode + "," + objAutoLoanAnalystApplication.ValuePack + "," + objAutoLoanAnalystApplication.BorrowingRelationShip + "," + objAutoLoanAnalystApplication.BorrowingRelationShipCode + "," + objAutoLoanAnalystApplication.CustomerStatus + "," + objAutoLoanAnalystApplication.AskingRepaymentMethod + "," + objAutoLoanAnalystApplication.ARTA + "," + objAutoLoanAnalystApplication.ARTAStatus + "," + objAutoLoanAnalystApplication.GeneralInsurance + "," + objAutoLoanAnalystApplication.CashSecured100Per + "," + objAutoLoanAnalystApplication.CCBundle + ")");
                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
        }
        

        #region getting all loan information


        public Object getLoanLocetorInfoByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();

            string condition = "";

            if (statePermission.Count > 0)
            {
                condition += " and StatusID in (";
                foreach (AutoUserTypeVsWorkflow autwf in statePermission)
                {
                    condition += autwf.WorkFlowID.ToString();

                    if (statePermission.IndexOf(autwf) != statePermission.Count - 1)
                    {
                        condition += ",";
                    }
                    else
                    {
                        condition += ")";
                    }
                }
            }
            string sql = "select count(*) from auto_loanmaster where llid = " + llid + condition;
            isExistLLID = Convert.ToInt32(DbQueryManager.ExecuteScalar(sql));

            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSS(llid);
            }
            else
            {
                returnValue = "No Data found.";
            }

            return returnValue;
        }

        public Object getLoanLocetorInfoByLLID(int llid)
        {
            var returnValue = new Object();
            var isExistLLID = 0;
            var dbHelper = new CommonDbHelper();

            string sql = "select count(*) from auto_loanmaster where llid = " + llid;
            IDataReader reader = dbHelper.GetDataReader(sql);
            while (reader.Read())
            {
                isExistLLID = Convert.ToInt32(reader.GetValue(0));
            }
            reader.Close();
            dbHelper.Close();


            if (isExistLLID > 0)
            {
                returnValue = getDataFromPLUSS(llid);
            }
            else
            {
                returnValue = "No Data found.";
            }

            return returnValue;
        }



        private Object getDataFromPLUSS(int llid)
        {
            var autoLoanApplicationAll = new AutoLoanApplicationAll();
            var autoLoanMasterID = 0;
            var PRaccountID = 0;
            var JTaccountID = 0;
            var AutoVendorID = 0;
            var analystMasterId = 0;
            var sigmentID = 0;
            var dataTable = new DataTable();
            var reader = new DataTableReader(dataTable);

            var objDbHelper = new CommonDbHelper();
            string queryString = "";

            #region Loan Application

            //AutoLoanMaster
            var autoLoanMaster = new AutoLoanMaster();

            queryString = String.Format(@"Select Auto_LoanMasterId from auto_loanmaster where LLID = {0}", llid);
            // IDataReader reader = objDbHelper.GetDataReader(queryString);
            autoLoanMasterID = (int)DbQueryManager.ExecuteScalar(queryString); ;
            //AutoLoanMaster Close
            //autoLoanMaster.UserType = userType;

            autoLoanApplicationAll.objAutoLoanMaster = autoLoanMaster;

            //AutoPRApplicant
            //var autoPRApplicant = new AutoPRApplicant();
            var autoPRApplicant = new AutoPRApplicant();
            queryString = String.Format(@"Select * from auto_prapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            var data = DbQueryManager.GetTable(queryString);

            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {

                    autoPRApplicant.PrApplicantId = (int)(xRs["PrApplicantId"]);
                    autoPRApplicant.Auto_LoanMasterId = (int)(xRs["Auto_LoanMasterId"]);
                    autoPRApplicant.PrName = Convert.ToString(xRs["PrName"]);
                    autoPRApplicant.Gender = (int)(xRs["Gender"]);
                    autoPRApplicant.RelationShipNumber = Convert.ToString(xRs["RelationShipNumber"]);
                    autoPRApplicant.TINNumber = Convert.ToString(xRs["TINNumber"]);
                    autoPRApplicant.FathersName = Convert.ToString(xRs["FathersName"]);
                    autoPRApplicant.MothersName = Convert.ToString(xRs["MothersName"]);
                    autoPRApplicant.DateofBirth = Convert.ToDateTime(xRs["DateofBirth"]);
                    autoPRApplicant.MaritalStatus = Convert.ToString(xRs["MaritalStatus"]);
                    autoPRApplicant.SpouseName = Convert.ToString(xRs["SpouseName"]);
                    autoPRApplicant.SpouseProfession = Convert.ToString(xRs["SpouseProfession"]);
                    autoPRApplicant.SpouseWorkAddress = Convert.ToString(xRs["SpouseWorkAddress"]);
                    autoPRApplicant.SpouseContactNumber = Convert.ToString(xRs["SpouseContactNumber"]);
                    autoPRApplicant.RelationShipwithPrimary = Convert.ToString(xRs["RelationShipwithPrimary"]);
                    autoPRApplicant.Nationality = Convert.ToString(xRs["Nationality"]);
                    autoPRApplicant.IdentificationNumberType = Convert.ToString(xRs["IdentificationNumberType"]);
                    autoPRApplicant.IdentificationNumber = Convert.ToString(xRs["IdentificationNumber"]);
                    autoPRApplicant.IdentificationDocument = Convert.ToString(xRs["IdentificationDocument"]);
                    autoPRApplicant.NumberofDependents = Convert.ToString(xRs["NumberofDependents"]);
                    autoPRApplicant.HighestEducation = Convert.ToString(xRs["HighestEducation"]);
                    autoPRApplicant.ResidenceAddress = Convert.ToString(xRs["ResidenceAddress"]);
                    autoPRApplicant.MailingAddress = Convert.ToString(xRs["MailingAddress"]);
                    autoPRApplicant.PermanentAddress = Convert.ToString(xRs["PermanentAddress"]);
                    autoPRApplicant.ResidenceStatus = Convert.ToString(xRs["ResidenceStatus"]);
                    autoPRApplicant.OwnershipDocument = Convert.ToString(xRs["OwnershipDocument"]);
                    autoPRApplicant.ResidenceStatusYear = Convert.ToString(xRs["ResidenceStatusYear"]);
                    autoPRApplicant.PhoneResidence = Convert.ToString(xRs["PhoneResidence"]);
                    autoPRApplicant.Mobile = Convert.ToString(xRs["Mobile"]);
                    autoPRApplicant.Email = Convert.ToString(xRs["Email"]);
                    if (!String.IsNullOrEmpty(xRs["DirectorshipwithanyBank"].ToString()))
                    {
                        autoPRApplicant.DirectorshipwithanyBank = (bool) (xRs["DirectorshipwithanyBank"]);
                    }
                   
                   
                    //autoPRApplicantList.Add(autoPRApplicant);
                }
                xRs.Close();
            }
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRApplicantDataReader = new AutoPRApplicantDataReader(reader);
            //while (reader.Read())
            //    autoPRApplicant = autoPRApplicantDataReader.Read();
            //reader.Close();
            // AutoPRApplican close

            autoLoanApplicationAll.objAutoPRApplicant = autoPRApplicant;

            //AutoJTApplicant

            var autoJTApplicant = new AutoJTApplicant();
            queryString = String.Format(@"Select * from auto_jtapplicant where Auto_LoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoJTApplicantDataReader = new AutoJTApplicantDataReader(reader);
            //while (reader.Read())
            //    autoJTApplicant = autoJTApplicantDataReader.Read();
            //reader.Close();
            // AutoJTApplicant close

            var list = DataTableToList.GetList<AutoJTApplicant>(DbQueryManager.GetTable(queryString));
            if (list != null && list.Any())
            {
                autoLoanApplicationAll.objAutoJTApplicant =DataTableToList.GetList<AutoJTApplicant>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           
            

            //AutoPRProfession
            var autoPRProfession = new AutoPRProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'PrPrimaryProfessionName',pluss2.PROF_NAME 'PrOtherProfessionName' 
                                        from auto_prprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
					                    left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID
                                        where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRProfessionDataReader = new AutoPRProfessionDataReader(reader);
            //while (reader.Read())
            //    autoPRProfession = autoPRProfessionDataReader.Read();
            //reader.Close();
            // AutoPRProfession close
            var prList = DataTableToList.GetList<AutoPRProfession>(DbQueryManager.GetTable(queryString));
            if (prList != null && list.Any())
            {
                autoLoanApplicationAll.objAutoPRProfession = DataTableToList.GetList<AutoPRProfession>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            //AutoJTProfession

            var autoJTProfession = new AutoJTProfession();
            queryString = String.Format(@"Select auto.*,pluss1.PROF_NAME 'JtPrimaryProfessionName',pluss2.PROF_NAME 'JtOtherProfessionName'
                                        from auto_jtprofession auto join t_pluss_profession pluss1 on auto.PrimaryProfession = pluss1.PROF_ID
                                        left outer join t_pluss_profession pluss2 on auto.OtherProfession = pluss2.PROF_ID 
                                        where auto.AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoJTProfessionDataReader = new AutoJTProfessionDataReader(reader);
            //while (reader.Read())
            //    autoJTProfession = autoJTProfessionDataReader.Read();
            //reader.Close();
            // AutoJTProfession close
            var jtList = DataTableToList.GetList<AutoJTProfession>(DbQueryManager.GetTable(queryString));
            if (jtList != null && list.Any())
            {
                autoLoanApplicationAll.objAutoJTProfession = DataTableToList.GetList<AutoJTProfession>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            //AutoLoanVehicle
            //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1),FALSE) as ValuePackAllowed
            var autoLoanVehicle = new AutoLoanVehicle();
            //            queryString = String.Format(@"SELECT *,
            //auto_status.StatusCode,
            //auto_menufacturer.ManufacturerCode,
            //auto_vehicle.ModelCode,
            //auto_vehicle.VehicleTypeCode,
            //auto_vendor.VendorCode,
            //IFNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
            //(Select auto_brandtenorltv.ValuePackAllowed from auto_brandtenorltv where auto_brandtenorltv.ManufacturerId = auto_loanvehicle.Auto_ManufactureId and auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId ORDER BY auto_brandtenorltv.LastUpdateDate desc LIMIT 1) as ValuePackAllowed
            //FROM
            //auto_loanvehicle
            //left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
            //INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
            //left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.VehicleId
            //INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
            //LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);

            queryString = String.Format(@"SELECT auto_loanvehicle.*,
auto_status.StatusCode,
auto_menufacturer.ManufacturerCode,
auto_vehicle.ModelCode,
auto_vehicle.VehicleTypeCode,
auto_vendor.VendorCode,

IsNULL(auto_negativevendor.NegativeVendorId, auto_vendor.isMOU) as VendoreRelationshipStatus,
auto_brandtenorltv.ValuePackAllowed,
auto_brandtenorltv.Tenor,
auto_brandtenorltv.LTV
FROM
auto_loanvehicle
left outer JOIN auto_status ON auto_status.StatusId = auto_loanvehicle.VehicleStatus
INNER JOIN auto_menufacturer ON auto_menufacturer.ManufacturerId = auto_loanvehicle.Auto_ManufactureId
left outer JOIN auto_vehicle ON auto_vehicle.VehicleId = auto_loanvehicle.VehicleId
INNER JOIN auto_vendor ON auto_vendor.AutoVendorId = auto_loanvehicle.VendorId
LEFT OUTER JOIN auto_brandtenorltv on auto_brandtenorltv.VehicleId = auto_loanvehicle.AutoLoanVehicleId and auto_brandtenorltv.StatusId = auto_loanvehicle.VehicleStatus
LEFT OUTER JOIN auto_negativevendor ON auto_negativevendor.AutoVendorId = auto_vendor.AutoVendorId where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoLoanVehicleDataReader = new AutoLoanVehicleDataReader(reader);
            //while (reader.Read())
            //    autoLoanVehicle = autoLoanVehicleDataReader.Read();
            //reader.Close();
            AutoVendorID = autoLoanVehicle.VendorId;
            // AutoLoanVehicle close
            var vehicleList = DataTableToList.GetList<AutoLoanVehicle>(DbQueryManager.GetTable(queryString));
            if (vehicleList != null && vehicleList.Any())
            {
                autoLoanApplicationAll.objAutoLoanVehicle = DataTableToList.GetList<AutoLoanVehicle>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            //AutoPRAccount
            var autoPRAccount = new AutoPRAccount();
            queryString = String.Format(@"Select * from auto_praccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRAccountDataReader = new AutoPRAccountDataReader(reader);
            //while (reader.Read())
            //    autoPRAccount = autoPRAccountDataReader.Read();
            //reader.Close();
            PRaccountID = autoPRAccount.PrAccountId;
            // AutoPRAccount close
            var PRAccountList = DataTableToList.GetList<AutoPRAccount>(DbQueryManager.GetTable(queryString));
            if (PRAccountList != null && PRAccountList.Any())
            {
                autoLoanApplicationAll.objAutoPRAccount = DataTableToList.GetList<AutoPRAccount>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            //AutoJTAccount
            var autoJTAccount = new AutoJTAccount();
            queryString = String.Format(@"Select * from auto_jtaccount where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoJTAccountDataReader = new AutoJTAccountDataReader(reader);
            //while (reader.Read())
            //    autoJTAccount = autoJTAccountDataReader.Read();
            //reader.Close();
            JTaccountID = autoJTAccount.jtAccountId;
            // AutoJTAccount close
            var JTAccountList = DataTableToList.GetList<AutoJTAccount>(DbQueryManager.GetTable(queryString));
            if (JTAccountList != null && JTAccountList.Any())
            {
                autoLoanApplicationAll.objAutoJTAccount = DataTableToList.GetList<AutoJTAccount>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            //AutoPRAccountDetail
            List<AutoPRAccountDetail> AutoPRAccountDetailList = new List<AutoPRAccountDetail>();
            var autoPRAccountDetail = new AutoPRAccountDetail();
            queryString = String.Format(@"Select * from auto_praccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRAccountDetailDataReader = new AutoPRAccountDetailDataReader(reader);
            //while (reader.Read())
            //{
            //    autoPRAccountDetail = autoPRAccountDetailDataReader.Read();
            //    AutoPRAccountDetailList.Add(autoPRAccountDetail);
            //}
            //reader.Close();
            // AutoPRAccountDetail close

            autoLoanApplicationAll.objAutoPRAccountDetailList = DataTableToList.GetList<AutoPRAccountDetail>(DbQueryManager.GetTable(queryString));

            //AutoJTAccountDetail
            List<AutoJTAccountDetail> autoJTAccountDetailList = new List<AutoJTAccountDetail>();
            var autoJTAccountDetail = new AutoJTAccountDetail();
            queryString = String.Format(@"Select * from auto_jtaccount_details where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoJTAccountDetailDataReader = new AutoJTAccountDetailDataReader(reader);
            //while (reader.Read())
            //{
            //    autoJTAccountDetail = autoJTAccountDetailDataReader.Read();
            //    autoJTAccountDetailList.Add(autoJTAccountDetail);
            //}
            //reader.Close();
            // AutoJTAccountDetail close

            autoLoanApplicationAll.objAutoJtAccountDetailList = DataTableToList.GetList<AutoJTAccountDetail>(DbQueryManager.GetTable(queryString));

            //objAutoPRFinance
            var autoPRFinance = new AutoPRFinance();
            queryString = String.Format(@"Select * from auto_prfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRFinanceDataReader = new AutoPRFinanceDataReader(reader);
            //while (reader.Read())
            //    autoPRFinance = autoPRFinanceDataReader.Read();
            //reader.Close();
            // objAutoPRFinance close
            var PRFinanceList = DataTableToList.GetList<AutoPRFinance>(DbQueryManager.GetTable(queryString));
            if (PRFinanceList != null && PRFinanceList.Any())
            {
                autoLoanApplicationAll.objAutoPRFinance = DataTableToList.GetList<AutoPRFinance>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            //objAutoJTFinance
            var autoJTFinance = new AutoJTFinance();
            queryString = String.Format(@"Select * from auto_jtfinance where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoJTFinanceDataReader = new AutoJTFinanceDataReader(reader);
            //while (reader.Read())
            //    autoJTFinance = autoJTFinanceDataReader.Read();
            //reader.Close();
            // objAutoJTFinance close
            var JTFinanceList = DataTableToList.GetList<AutoJTFinance>(DbQueryManager.GetTable(queryString));
            if (JTFinanceList != null && JTFinanceList.Any())
            {
                autoLoanApplicationAll.objAutoJTFinance = DataTableToList.GetList<AutoJTFinance>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
          

            //objAutoLoanReference
            var autoLoanReference = new AutoLoanReference();
            queryString = String.Format(@"Select * from auto_loanreference where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoLoanReferenceDataReader = new AutoLoanReferenceDataReader(reader);
            //while (reader.Read())
            //    autoLoanReference = autoLoanReferenceDataReader.Read();
            //reader.Close();
            // objAutoLoanReference close
            var LoanReferenceList = DataTableToList.GetList<AutoLoanReference>(DbQueryManager.GetTable(queryString));
            if (LoanReferenceList != null && LoanReferenceList.Any())
            {
                autoLoanApplicationAll.objAutoLoanReference = DataTableToList.GetList<AutoLoanReference>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            


            #region objAutoLoanFacilityList
            //
            List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
            var autoLoanFacility = new AutoLoanFacility();
            queryString = String.Format(@"SELECT
auto_loanfacility.FacilityId,
auto_loanfacility.AutoLoanMasterId,
auto_loanfacility.FacilityType,
auto_loanfacility.InterestRate,
auto_loanfacility.PresentBalance,
auto_loanfacility.PresentEMI,
auto_loanfacility.PresentLimit,
auto_loanfacility.ProposedLimit,
auto_loanfacility.RepaymentAgrement,
auto_loanfacility.Consider,
auto_loanfacility.EMIPercent,
auto_loanfacility.EMIShare,
t_pluss_facility.FACI_NAME,
auto_loansecurity.NatureOfSecurity,
auto_loansecurity.FaceValue,
auto_loansecurity.XTVRate,
auto_loansecurity.FacilityType,
auto_loansecurity.`Status`,
t_pluss_facility.FACI_CODE
FROM
auto_loanfacility
INNER JOIN t_pluss_facility ON auto_loanfacility.FacilityType = t_pluss_facility.FACI_ID
LEFT OUTER JOIN auto_loansecurity ON auto_loanfacility.AutoLoanMasterId = auto_loansecurity.AutoLoanMasterId AND t_pluss_facility.FACI_ID = auto_loansecurity.FacilityType AND auto_loansecurity.FacilityId = auto_loanfacility.FacilityId
where auto_loanfacility.AutoLoanMasterId ={0}", autoLoanMasterID);

            //reader = objDbHelper.GetDataReader(queryString);
            //var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
            //while (reader.Read())
            //{
            //    autoLoanFacility = autoLoanFacilityDataReader.Read();
            //    autoLoanFacilityList.Add(autoLoanFacility);
            //}
            //reader.Close();
            // objAutoLoanFacilityList close

            autoLoanApplicationAll.objAutoLoanFacilityList = DataTableToList.GetList<AutoLoanFacility>(DbQueryManager.GetTable(queryString));

            #endregion objAutoLoanFacilityList



            ////objAutoLoanFacilityList
            //List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();
            //var autoLoanFacility = new AutoLoanFacility();
            //queryString = String.Format(@"Select auto_loanfacility.*,t_pluss_facility.FACI_NAME from auto_loanfacility,t_pluss_facility where auto_loanfacility.FacilityType = t_pluss_facility.FACI_ID and AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoLoanFacilityDataReader = new AutoLoanFacilityDataReader(reader);
            //while (reader.Read())
            //{
            //    autoLoanFacility = autoLoanFacilityDataReader.Read();
            //    autoLoanFacilityList.Add(autoLoanFacility);
            //}
            //reader.Close();
            //// objAutoLoanFacilityList close

            //autoLoanApplicationAll.objAutoLoanFacilityList = autoLoanFacilityList;


            //objAutoLoanSecurityList
            List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();
            var autoLoanSecurity = new AutoLoanSecurity();
            queryString = String.Format(@"Select * from auto_loansecurity where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoLoanSecurityDataReader = new AutoLoanSecurityDataReader(reader);
            //while (reader.Read())
            //{
            //    autoLoanSecurity = autoLoanSecurityDataReader.Read();
            //    autoLoanSecurityList.Add(autoLoanSecurity);
            //}
            //reader.Close();
            // objAutoLoanSecurityList close

            autoLoanApplicationAll.objAutoLoanSecurityList = DataTableToList.GetList<AutoLoanSecurity>(DbQueryManager.GetTable(queryString));

            //AutoApplicationInsurance
            var autoApplicationInsurance = new AutoApplicationInsurance();
            queryString = String.Format(@"Select * from auto_applicationinsurance where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoApplicationInsuranceDataReader = new AutoApplicationInsuranceDataReader(reader);
            //while (reader.Read())
            //    autoApplicationInsurance = autoApplicationInsuranceDataReader.Read();
            //reader.Close();
            // AutoApplicationInsurance close
            var insuranceList = DataTableToList.GetList<AutoApplicationInsurance>(DbQueryManager.GetTable(queryString));
            if (insuranceList != null && insuranceList.Any())
            {
                autoLoanApplicationAll.objAutoApplicationInsurance = DataTableToList.GetList<AutoApplicationInsurance>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            #endregion Loan Application

            #region Vehicle Information
            //Auto vendore   objAutoVendor
            var autoVendor = new AutoVendor();
            queryString = String.Format(@"Select * from auto_vendor where AutoVendorId = {0}", AutoVendorID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoVendorDataReader = new AutoVendorDataReader(reader);
            //while (reader.Read())
            //    autoVendor = autoVendorDataReader.Read();
            //reader.Close();
            var vList = DataTableToList.GetList<AutoVendor>(DbQueryManager.GetTable(queryString));
            if (vList != null && vList.Any())
            {
                autoLoanApplicationAll.objAutoVendor = DataTableToList.GetList<AutoVendor>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
          
            //End

            //objAutoVendorMou
            var autoVendorMou = new AutoVendorMOU();
            queryString = String.Format(@"select AutoVendorId,VendorName,VendorCode,Address,Phone,Website,isMOU,DealerType,IrWithArta,IrWithoutArta,ProcessingFee,ValidFrom,StatusName as DealerStatus,TotalCount from
(
SELECT s1.AutoVendorId, s1.VendorName,s1.VendorCode, s1.Address, s1.Phone,s1.Website,s1.isMOU,s1.DealerType, s2.IrWithArta,s2.IrWithoutArta,s2.ProcessingFee,s2.ValidFrom,auto_status.StatusName,
(select count(*) from auto_vendor) AS TotalCount
FROM auto_vendor s1
Left outer JOIN auto_vendormou s2 ON s1.AutoVendorId = s2.AutoVendorId
and  s2.LastUpdateDate= 
(select Max(LastUpdateDate) as lsUpdateDate from auto_vendormou as s3 where s3.AutoVendorId=s1.AutoVendorId)
left join auto_status on auto_status.StatusId = s1.DealerType
order by s1.AutoVendorId asc) as tbl where AutoVendorId = {0}", AutoVendorID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoVendorMOUDataReader = new AutoVendorMOUDataReader(reader);
            //while (reader.Read())
            //    autoVendorMou = autoVendorMOUDataReader.Read();
            //reader.Close();
            // objAutoVendor close
            var mouList =DataTableToList.GetList<AutoVendorMOU>(DbQueryManager.GetTable(queryString));
            if (mouList != null && mouList.Any())
            {
                autoLoanApplicationAll.objAutoVendorMou = DataTableToList.GetList<AutoVendorMOU>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            #endregion Vehicle Information

            #region Bank Statement

            //AutoPRBankStatementList
            List<AutoPRBankStatement> objAutoPRBankStatementList = new List<AutoPRBankStatement>();
            var autoPRBankStatement = new AutoPRBankStatement();
            queryString = String.Format(@"Select * from auto_prbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoPRBankStatementDataReader = new AutoPRBankStatementDataReader(reader);
            //while (reader.Read())
            //{
            //    autoPRBankStatement = autoPRBankStatementDataReader.Read();
            //    objAutoPRBankStatementList.Add(autoPRBankStatement);
            //}
            //reader.Close();
            // AutoPRBankStatementList close

            autoLoanApplicationAll.AutoPRBankStatementList = DataTableToList.GetList<AutoPRBankStatement>(DbQueryManager.GetTable(queryString));


            //AutoJTBankStatementList
            List<AutoJTBankStatement> objAutoJTBankStatementList = new List<AutoJTBankStatement>();
            var autoJTBankStatement = new AutoJTBankStatement();
            queryString = String.Format(@"Select * from auto_jtbankstatement where AutoLoanMasterId = {0}", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoJTBankStatementDataReader = new AutoJTBankStatementDataReader(reader);
            //while (reader.Read())
            //{
            //    autoJTBankStatement = autoJTBankStatementDataReader.Read();
            //    objAutoJTBankStatementList.Add(autoJTBankStatement);
            //}
            //reader.Close();
            // AutoJTBankStatementList close

            autoLoanApplicationAll.AutoJTBankStatementList = DataTableToList.GetList<AutoJTBankStatement>(DbQueryManager.GetTable(queryString));


            //AutoBankStatementAvgTotalList
            List<Auto_BankStatementAvgTotal> AutoBankStatementAvgTotalList = new List<Auto_BankStatementAvgTotal>();
            var autoBankStatementAvgTotal = new Auto_BankStatementAvgTotal();
            queryString = String.Format(@"Select * from auto_bankstatementavgtotal where AutoLoanMasterId = {0} order by Type,month asc", autoLoanMasterID);
            //reader = objDbHelper.GetDataReader(queryString);
            //var autoBankStatementAvgTotalDataReader = new AutoBankStatementAvgTotalDataReader(reader);
            //while (reader.Read())
            //{
            //    autoBankStatementAvgTotal = autoBankStatementAvgTotalDataReader.Read();
            //    AutoBankStatementAvgTotalList.Add(autoBankStatementAvgTotal);
            //}
            //reader.Close();
            // AutoBankStatementAvgTotalList close

            autoLoanApplicationAll.AutoBankStatementAvgTotalList = DataTableToList.GetList<Auto_BankStatementAvgTotal>(DbQueryManager.GetTable(queryString));

            #endregion Bank Statement

            #region Loan Appliaction Analyst Part

            #region General Tab

            #region Aanalyst Master start

            var autoLoanAnalystMaster = new Auto_LoanAnalystMaster();

            queryString = String.Format(@"Select a.*,b.FACI_NAME ColleterizationName, c.FACI_NAME ColleterizationWithName  
                                            from auto_loananalystmaster a
                                            LEFT OUTER JOIN t_pluss_facility b on a.Colleterization = b.FACI_ID
                                            LEFT OUTER JOIN t_pluss_facility c on a.ColleterizationWith = c.FACI_ID 
                                            where LLID = {0} and a.AutoLoanMasterId = {1}", llid, autoLoanMasterID);

            //reader = objDbHelper.GetDataReader(queryString);

            //var autoLoanAnalystMasterDataReader = new Auto_LoanAnalystMasterDataReader(reader);
            //while (reader.Read())
            //    autoLoanAnalystMaster = autoLoanAnalystMasterDataReader.Read();
            //reader.Close();
            analystMasterId = autoLoanAnalystMaster.AnalystMasterId;
            var masList = DataTableToList.GetList<Auto_LoanAnalystMaster>(DbQueryManager.GetTable(queryString));
            if (masList != null && masList.Any())
            {
                autoLoanApplicationAll.AutoLoanAnalystMaster = DataTableToList.GetList<Auto_LoanAnalystMaster>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }

            #endregion Aanalyst Master start

            #region Auto_Loan Analyst Application

            var autoLoanAnalystApplication = new Auto_LoanAnalystApplication();
            autoLoanAnalystApplication = null;

            queryString = String.Format(@"Select a.*,b.SourceName AppSourceName,c.CompanyName ARTAName from auto_loananalystapplication a 
                                            LEFT OUTER JOIN auto_source b ON a.BranchId = b.AutoSourceId 
                                            LEFT OUTER JOIN auto_insurancebanca c ON a.ARTAStatus = c.InsCompanyId 
                                            where a.AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);

            //var autoLoanAnalystApplicationDataReader = new Auto_LoanAnalystApplicationDataReader(reader);

            //while (reader.Read())
            //    autoLoanAnalystApplication = autoLoanAnalystApplicationDataReader.Read();
            //reader.Close();
            var anaList = DataTableToList.GetList<Auto_LoanAnalystApplication>(DbQueryManager.GetTable(queryString));
            if (anaList != null && anaList.Any())
            {
                autoLoanApplicationAll.AutoLoanAnalystApplication = DataTableToList.GetList<Auto_LoanAnalystApplication>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            #endregion Auto_Loan Analyst Application

            #region Auto_Loan Analyst Vehicle Detail

            var autoLoanAnalystVehicleDetail = new Auto_LoanAnalystVehicleDetail();
            autoLoanAnalystVehicleDetail = null;

            queryString = String.Format(@"Select * from auto_loananalystvehicledetail where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);

            //var autoLoanAnalystVehicleDetailDataReader = new Auto_LoanAnalystVehicleDetailDataReader(reader);

            //while (reader.Read())
            //    autoLoanAnalystVehicleDetail = autoLoanAnalystVehicleDetailDataReader.Read();
            //reader.Close();
            var avdList = DataTableToList.GetList<Auto_LoanAnalystVehicleDetail>(DbQueryManager.GetTable(queryString));
            if (avdList != null && avdList.Any())
            {
                autoLoanApplicationAll.AutoLoanAnalystVehicleDetail = DataTableToList.GetList<Auto_LoanAnalystVehicleDetail>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
            

            #endregion Auto_Loan Analyst Vehicle Detail

            #region Auto Applicant SCB Account

            List<AutoApplicantSCBAccount> autoApplicantSCBAccountList = new List<AutoApplicantSCBAccount>();
            var autoApplicantSCBAccount = new AutoApplicantSCBAccount();

            queryString = String.Format(@"Select * from auto_applicantscbaccount where AutoLoanMasterId = {0}", autoLoanMasterID);

            //reader = objDbHelper.GetDataReader(queryString);
            //var autoApplicantSCBAccountDataReader = new AutoApplicantSCBAccountDataReader(reader);
            //while (reader.Read())
            //{
            //    autoApplicantSCBAccount = autoApplicantSCBAccountDataReader.Read();
            //    autoApplicantSCBAccountList.Add(autoApplicantSCBAccount);
            //}
            //reader.Close();

            autoLoanApplicationAll.AutoApplicantSCBAccountList = DataTableToList.GetList<AutoApplicantSCBAccount>(DbQueryManager.GetTable(queryString));

            #endregion Auto Applicant SCB Account

            #region Auto Applicant Account Other List

            // already create the list in AutoLoan region

            #endregion Auto Applicant Account Other List

            #region Auto Facility List

            // already create the list in AutoLoan region

            #endregion Auto Facility List

            #region Auto Security List

            // already create the list in AutoLoan region

            #endregion Auto Security List

            #region OFF SCB Facility List

            List<AutoLoanFacilityOffSCB> autoLoanFacilityOffSCBList = new List<AutoLoanFacilityOffSCB>();
            var autoLoanFacilityOffSCB = new AutoLoanFacilityOffSCB();

            queryString = String.Format(@"Select a.*,b.BANK_NM BankName from auto_loanfacilityoffscb a 
                                            LEFT OUTER JOIN t_pluss_bank b ON a.BankID = b.BANK_ID
                                            where a.AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var autoLoanFacilityOffSCBDataReader = new AutoLoanFacilityOffSCBDataReader(reader);
            //while (reader.Read())
            //{
            //    autoLoanFacilityOffSCB = autoLoanFacilityOffSCBDataReader.Read();
            //    autoLoanFacilityOffSCBList.Add(autoLoanFacilityOffSCB);
            //}
            //reader.Close();

            autoLoanApplicationAll.AutoLoanFacilityOffSCBList = DataTableToList.GetList<AutoLoanFacilityOffSCB>(DbQueryManager.GetTable(queryString));

            #endregion OFF SCB Facility List

            #endregion General Tab

            #region LoanApplication VerifiedUserList

            var objAuto_App_Step_ExecutionByUser = new Auto_App_Step_ExecutionByUser();
            queryString = String.Format(@"Select a.*,b.USER_NAME AppRaiseByName,c.USER_NAME AppSupportByName,d.USER_NAME AppAnalysisByName, 
                                            e.USER_NAME AppApprovedByName from auto_app_step_executionbyuser a 
                                            LEFT OUTER JOIN t_pluss_user b on a.AppRaiseBy = b.USER_ID
                                            LEFT OUTER JOIN t_pluss_user c on a.AppSupportBy = c.USER_ID
                                            LEFT OUTER JOIN t_pluss_user d on a.AppAnalysisBy = d.USER_ID
                                            LEFT OUTER JOIN t_pluss_user e on a.AppApprovedBy = e.USER_ID
                                             where Auto_LoanMasterId = {0} ", autoLoanMasterID);

            //reader = objDbHelper.GetDataReader(queryString);

            //var auto_App_Step_ExecutionByUserDataReader = new Auto_App_Step_ExecutionByUserDataReader(reader);
            //while (reader.Read())
            //    objAuto_App_Step_ExecutionByUser = auto_App_Step_ExecutionByUserDataReader.Read();
            //reader.Close();

            var apseuList = DataTableToList.GetList<Auto_App_Step_ExecutionByUser>(DbQueryManager.GetTable(queryString));
            if (apseuList != null && apseuList.Any())
            {
                autoLoanApplicationAll.objAuto_App_Step_ExecutionByUser = DataTableToList.GetList<Auto_App_Step_ExecutionByUser>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
           


            #endregion

            #region Loan Calculation Tab

            #region Applicant Income List

            List<Auto_An_IncomeSegmentApplicantIncome> auto_An_IncomeSegmentApplicantIncomeList = new List<Auto_An_IncomeSegmentApplicantIncome>();
            var auto_An_IncomeSegmentApplicantIncome = new Auto_An_IncomeSegmentApplicantIncome();

            queryString = String.Format(@"Select * from Auto_An_In_Seg_Applicant_Income where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_IncomeSegmentApplicantIncomeDataReader = new Auto_An_IncomeSegmentApplicantIncomeDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_IncomeSegmentApplicantIncome = auto_An_IncomeSegmentApplicantIncomeDataReader.Read();
            //    auto_An_IncomeSegmentApplicantIncomeList.Add(auto_An_IncomeSegmentApplicantIncome);
            //}
            //reader.Close();

            autoLoanApplicationAll.Auto_An_IncomeSegmentApplicantIncomeList = DataTableToList.GetList<Auto_An_IncomeSegmentApplicantIncome>(DbQueryManager.GetTable(queryString));

            #endregion Applicant Income List

            #region Segment Income

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_IncomeSegmentIncome = new Auto_An_IncomeSegmentIncome();

            queryString = String.Format(@"Select * from Auto_An_In_Seg_Income where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_IncomeSegmentIncomeDataReader = new Auto_An_IncomeSegmentIncomeDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_IncomeSegmentIncome = auto_An_IncomeSegmentIncomeDataReader.Read();
            //    sigmentID = auto_An_IncomeSegmentIncome.EmployeeSegment;
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var incomeSegmentList = 
                DataTableToList.GetList<Auto_An_IncomeSegmentIncome>(DbQueryManager.GetTable(queryString));
            if (incomeSegmentList != null && incomeSegmentList.Any())
            {
                autoLoanApplicationAll.objAuto_An_IncomeSegmentIncome = DataTableToList.GetList<Auto_An_IncomeSegmentIncome>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            #endregion Segment Income

            #region REPAYMENT CAPABILITY

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_RepaymentCapability = new Auto_An_RepaymentCapability();

            queryString = String.Format(@"Select * from Auto_An_RepaymentCapability where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_RepaymentCapabilityDataReader = new Auto_An_RepaymentCapabilityDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_RepaymentCapability = auto_An_RepaymentCapabilityDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var rcList = DataTableToList.GetList<Auto_An_RepaymentCapability>(DbQueryManager.GetTable(queryString));
            if (rcList != null && rcList.Any())
            {
                autoLoanApplicationAll.objAuto_An_RepaymentCapability = DataTableToList.GetList<Auto_An_RepaymentCapability>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            #endregion REPAYMENT CAPABILITY

            #region Loan Calculation Tenor

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationTenor = new Auto_An_LoanCalculationTenor();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationTenor where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_LoanCalculationTenorDataReader = new Auto_An_LoanCalculationTenorDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_LoanCalculationTenor = auto_An_LoanCalculationTenorDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var alTendorList =
                DataTableToList.GetList<Auto_An_LoanCalculationTenor>(DbQueryManager.GetTable(queryString));
            if (alTendorList != null && alTendorList.Any())
            {
                autoLoanApplicationAll.objAuto_An_LoanCalculationTenor = DataTableToList.GetList<Auto_An_LoanCalculationTenor>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
          

            #endregion Loan Calculation Tenor

            #region Loan Calculation INTEREST

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationInterest = new Auto_An_LoanCalculationInterest();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationInterest where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_LoanCalculationInterestDataReader = new Auto_An_LoanCalculationInterestDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_LoanCalculationInterest = auto_An_LoanCalculationInterestDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var calInList =
                DataTableToList.GetList<Auto_An_LoanCalculationInterest>(DbQueryManager.GetTable(queryString));
            if (calInList != null && calInList.Any())
            {
                autoLoanApplicationAll.objAuto_An_LoanCalculationInterest = DataTableToList.GetList<Auto_An_LoanCalculationInterest>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            #endregion Loan Calculation INTEREST

            #region Loan Calculation Amount

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationAmount = new Auto_An_LoanCalculationAmount();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationAmount where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_LoanCalculationAmountDataReader = new Auto_An_LoanCalculationAmountDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_LoanCalculationAmount = auto_An_LoanCalculationAmountDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var alcAmList = DataTableToList.GetList<Auto_An_LoanCalculationAmount>(DbQueryManager.GetTable(queryString));
            if (alcAmList != null && alcAmList.Any())
            {
                autoLoanApplicationAll.objAuto_An_LoanCalculationAmount = DataTableToList.GetList<Auto_An_LoanCalculationAmount>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            


            #endregion Loan Calculation Amount

            #region Loan Calculation INSURANCE

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationInsurance = new Auto_An_LoanCalculationInsurance();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationInsurance where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_LoanCalculationInsuranceDataReader = new Auto_An_LoanCalculationInsuranceDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_LoanCalculationInsurance = auto_An_LoanCalculationInsuranceDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var alCalInList =
                DataTableToList.GetList<Auto_An_LoanCalculationInsurance>(DbQueryManager.GetTable(queryString));
            if (alCalInList != null && alCalInList.Any())
            {
                autoLoanApplicationAll.objAuto_An_LoanCalculationInsurance = DataTableToList.GetList<Auto_An_LoanCalculationInsurance>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            #endregion Loan Calculation INSURANCE

            #region Loan Calculation Total Loan Amount

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_LoanCalculationTotal = new Auto_An_LoanCalculationTotal();

            queryString = String.Format(@"Select * from Auto_An_LoanCalculationTotal where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_LoanCalculationTotalDataReader = new Auto_An_LoanCalculationTotalDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_LoanCalculationTotal = auto_An_LoanCalculationTotalDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var alcalTotalList =
                DataTableToList.GetList<Auto_An_LoanCalculationTotal>(DbQueryManager.GetTable(queryString));
            if (alcalTotalList!=null && alcalTotalList.Any())
            {
                  autoLoanApplicationAll.objAuto_An_LoanCalculationTotal = DataTableToList.GetList<Auto_An_LoanCalculationTotal>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
          

            #endregion Loan Calculation Total Loan Amount

            #endregion Loan Calculation Tab

            #region Finalization Tab

            #region Auto_An_Finalization Deviation List

            List<Auto_An_FinalizationDeviation> auto_An_FinalizationDeviationList = new List<Auto_An_FinalizationDeviation>();
            var auto_An_FinalizationDeviation = new Auto_An_FinalizationDeviation();

            queryString = String.Format(@"SELECT auto_an_finalizationdeviation.*,auto_deviation.Description as DescriptionText
                                        FROM auto_an_finalizationdeviation INNER JOIN auto_deviation ON 
                                        auto_an_finalizationdeviation.Description = auto_deviation.DeviationId
                                        where auto_an_finalizationdeviation.AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationDeviationDataReader = new Auto_An_FinalizationDeviationDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationDeviation = auto_An_FinalizationDeviationDataReader.Read();
            //    auto_An_FinalizationDeviationList.Add(auto_An_FinalizationDeviation);
            //}
            //reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationDeviationList = DataTableToList.GetList<Auto_An_FinalizationDeviation>(DbQueryManager.GetTable(queryString));

            #endregion Deviation List

            #region Auto_An_Finalization Dev Found

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationDevFound = new Auto_An_FinalizationDevFound();
            auto_An_FinalizationDevFound = null;

            queryString = String.Format(@"Select * from auto_an_finalizationdevfound where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationDevFoundDataReader = new Auto_An_FinalizationDevFoundDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationDevFound = auto_An_FinalizationDevFoundDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var fDevList = DataTableToList.GetList<Auto_An_FinalizationDevFound>(DbQueryManager.GetTable(queryString));
            if (fDevList != null && fDevList.Any())
            {
                autoLoanApplicationAll.objAuto_An_FinalizationDevFound = DataTableToList.GetList<Auto_An_FinalizationDevFound>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
           


            #endregion Auto_An_Finalization Dev Found

            #region Auto_An_Finalization Sterength List

            List<Auto_An_FinalizationSterength> auto_An_FinalizationSterengthList = new List<Auto_An_FinalizationSterength>();
            var auto_An_FinalizationSterength = new Auto_An_FinalizationSterength();

            queryString = String.Format(@"Select * from auto_an_finalizationsterength where AnalystMasterId = {0} order by Id asc", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationSterengthDataReader = new Auto_An_FinalizationSterengthDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationSterength = auto_An_FinalizationSterengthDataReader.Read();
            //    auto_An_FinalizationSterengthList.Add(auto_An_FinalizationSterength);
            //}
            //reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationSterengthList = DataTableToList.GetList<Auto_An_FinalizationSterength>(DbQueryManager.GetTable(queryString));

            #endregion Auto_An_Finalization Sterength List

            #region Auto_An_Finalization Weakness List

            List<Auto_An_FinalizationWeakness> auto_An_FinalizationWeaknessList = new List<Auto_An_FinalizationWeakness>();
            var auto_An_FinalizationWeakness = new Auto_An_FinalizationWeakness();

            queryString = String.Format(@"Select * from auto_an_finalizationweakness where AnalystMasterId = {0} order by Id asc", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationWeaknessDataReader = new Auto_An_FinalizationWeaknessDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationWeakness = auto_An_FinalizationWeaknessDataReader.Read();
            //    auto_An_FinalizationWeaknessList.Add(auto_An_FinalizationWeakness);
            //}
            //reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationWeaknessList = DataTableToList.GetList<Auto_An_FinalizationWeakness>(DbQueryManager.GetTable(queryString));


            #endregion Auto_An_Finalization Weakness List

            #region Auto_An_Finalization Condition List

            List<Auto_An_FinalizationCondition> auto_An_FinalizationConditionList = new List<Auto_An_FinalizationCondition>();
            var auto_An_FinalizationCondition = new Auto_An_FinalizationCondition();

            queryString = String.Format(@"Select * from auto_an_finalizationcondition where AnalystMasterId = {0} order by Id asc", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationConditionDataReader = new Auto_An_FinalizationConditionDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationCondition = auto_An_FinalizationConditionDataReader.Read();
            //    auto_An_FinalizationConditionList.Add(auto_An_FinalizationCondition);
            //}
            //reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationConditionList = DataTableToList.GetList<Auto_An_FinalizationCondition>(DbQueryManager.GetTable(queryString));

            #endregion Auto_An_Finalization Condition List

            #region Auto_An_Finalization Remark List

            List<Auto_An_FinalizationRemark> auto_An_FinalizationRemarkList = new List<Auto_An_FinalizationRemark>();
            var auto_An_FinalizationRemark = new Auto_An_FinalizationRemark();

            queryString = String.Format(@"Select * from auto_an_finalizationremark where AnalystMasterId = {0} order by Id asc", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationRemarkDataReader = new Auto_An_FinalizationRemarkDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationRemark = auto_An_FinalizationRemarkDataReader.Read();
            //    auto_An_FinalizationRemarkList.Add(auto_An_FinalizationRemark);
            //}
            //reader.Close();

            autoLoanApplicationAll.Auto_An_FinalizationRemarkList = DataTableToList.GetList<Auto_An_FinalizationRemark>(DbQueryManager.GetTable(queryString));

            #endregion Auto_An_Finalization Remark List

            #region Auto_An_Finalization Repayment Method

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationRepaymentMethod = new Auto_An_FinalizationRepaymentMethod();
            auto_An_FinalizationRepaymentMethod = null;

            queryString = String.Format(@"Select a.*,b.BANK_NM BankName from auto_an_finalizationrepaymentmethod a 
                                            LEFT OUTER JOIN t_pluss_bank b ON a.BankID = b.BANK_ID 
                                            where a.AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationRepaymentMethodDataReader = new Auto_An_FinalizationRepaymentMethodDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationRepaymentMethod = auto_An_FinalizationRepaymentMethodDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var frmList =
                DataTableToList.GetList<Auto_An_FinalizationRepaymentMethod>(DbQueryManager.GetTable(queryString));
            if (frmList != null && frmList.Any())
            {
                autoLoanApplicationAll.objAuto_An_FinalizationRepaymentMethod = DataTableToList.GetList<Auto_An_FinalizationRepaymentMethod>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            #endregion Auto_An_Finalization Repayment Method

            #region Auto_An_Finalization Req Doc

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationReqDoc = new Auto_An_FinalizationReqDoc();
            auto_An_FinalizationReqDoc = null;

            queryString = String.Format(@"Select * from auto_an_finalizationreqdoc where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationReqDocDataReader = new Auto_An_FinalizationReqDocDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationReqDoc = auto_An_FinalizationReqDocDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var reqDocList = DataTableToList.GetList<Auto_An_FinalizationReqDoc>(DbQueryManager.GetTable(queryString));
            if (reqDocList != null && reqDocList.Any())
            {
                autoLoanApplicationAll.objAuto_An_FinalizationReqDoc = DataTableToList.GetList<Auto_An_FinalizationReqDoc>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
           

            #endregion Auto_An_Finalization Req Doc

            #region Auto_An_ Finalization Varification Report

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationVarificationReport = new Auto_An_FinalizationVarificationReport();
            auto_An_FinalizationVarificationReport = null;

            queryString = String.Format(@"Select * from auto_an_finalizationvarificationreport where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationVarificationReportDataReader = new Auto_An_FinalizationVarificationReportDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationVarificationReport = auto_An_FinalizationVarificationReportDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var finVarList =
                DataTableToList.GetList<Auto_An_FinalizationVarificationReport>(DbQueryManager.GetTable(queryString));
            if (finVarList != null && finVarList.Any())
            {
                autoLoanApplicationAll.objAuto_An_FinalizationVarificationReport = DataTableToList.GetList<Auto_An_FinalizationVarificationReport>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
           

            #endregion Auto_An_ Finalization Varification Report

            #region Auto_An_ Finalization Appraiser And Approver

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var auto_An_FinalizationAppraiserAndApprover = new Auto_An_FinalizationAppraiserAndApprover();
            auto_An_FinalizationAppraiserAndApprover = null;

            queryString = String.Format(@"Select * from auto_an_finalizationappraiserandapprover where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationAppraiserAndApproverDataReader = new Auto_An_FinalizationAppraiserAndApproverDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationAppraiserAndApprover = auto_An_FinalizationAppraiserAndApproverDataReader.Read();
            //    //auto_An_IncomeSegmentIncomeList.Add(auto_An_IncomeSegmentIncome);
            //}
            //reader.Close();
            var finAppList =
                DataTableToList.GetList<Auto_An_FinalizationAppraiserAndApprover>(DbQueryManager.GetTable(queryString));
            if (finAppList != null && finAppList.Any())
            {
                autoLoanApplicationAll.objAuto_An_FinalizationAppraiserAndApprover = DataTableToList.GetList<Auto_An_FinalizationAppraiserAndApprover>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            #endregion Auto_An_ Finalization Appraiser And Approver

            #region Auto_An_Alert Verification Report

            var auto_An_AlertVerificationReport = new Auto_An_AlertVerificationReport();
            auto_An_AlertVerificationReport = null;

            queryString = String.Format(@"Select * from auto_an_alertverificationreport where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_AlertVerificationReportDataReader = new Auto_An_AlertVerificationReportDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_AlertVerificationReport = auto_An_AlertVerificationReportDataReader.Read();
            //}
            //reader.Close();
            var alVerList =
                DataTableToList.GetList<Auto_An_AlertVerificationReport>(DbQueryManager.GetTable(queryString));
            if (alVerList != null && alVerList.Any())
            {
                autoLoanApplicationAll.objAuto_An_AlertVerificationReport = DataTableToList.GetList<Auto_An_AlertVerificationReport>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
           
            #endregion Auto_An_Alert Verification Report

            #region Auto_An_ Finalization Rework

            var auto_An_FinalizationRework = new Auto_An_FinalizationRework();
            auto_An_FinalizationRework = null;

            queryString = String.Format(@"Select * from auto_an_rework where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_FinalizationReworkDataReader = new Auto_An_FinalizationReworkDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_FinalizationRework = auto_An_FinalizationReworkDataReader.Read();
            //}
            //reader.Close();
            var finRework = DataTableToList.GetList<Auto_An_FinalizationRework>(DbQueryManager.GetTable(queryString));
            if (finRework != null && finRework.Any())
            {
                autoLoanApplicationAll.objAuto_An_FinalizationRework = DataTableToList.GetList<Auto_An_FinalizationRework>(DbQueryManager.GetTable(queryString)).FirstOrDefault();
            }
            

            #endregion Auto_An_ Finalization Rework

            #region Auto_An_Finalization auto_An_CibRequestLoan

            List<auto_An_CibRequestLoan> auto_An_CibRequestLoanList = new List<auto_An_CibRequestLoan>();
            var auto_An_CibRequestLoan = new auto_An_CibRequestLoan();

            queryString = String.Format(@"Select * from auto_An_CibRequestLoan where AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);
            //var auto_An_CibRequestLoanDataReader = new auto_An_CibRequestLoanDataReader(reader);
            //while (reader.Read())
            //{
            //    auto_An_CibRequestLoan = auto_An_CibRequestLoanDataReader.Read();
            //    auto_An_CibRequestLoanList.Add(auto_An_CibRequestLoan);
            //}
            //reader.Close();

            autoLoanApplicationAll.objAuto_An_CibRequestLoan = DataTableToList.GetList<auto_An_CibRequestLoan>(DbQueryManager.GetTable(queryString));

            #endregion Auto_An_Finalization auto_An_CibRequestLoan


            #endregion Finalization Tab

            #region Income Generator

            #region Auto Salaried
            List<Auto_IncomeSalaried> AutoIncomeSalariedList = new List<Auto_IncomeSalaried>();
            List<Auto_VariableSalaryCalculation> Auto_VariableSalaryCalculationLoop = new List<Auto_VariableSalaryCalculation>();

            var AutoIncomeSalaried = new Auto_IncomeSalaried();
            var Auto_VariableSalaryCalculationForLoop = new Auto_VariableSalaryCalculation();
            //var auto_An_SalariedVariableSalaryMonthIncome = new Auto_An_SalariedVariableSalaryMonthIncome();


            queryString = String.Format(@"SELECT
auto_income_salaried.Id,
auto_income_salaried.AnalystMasterId,
auto_income_salaried.SalaryMode,
auto_income_salaried.Employer,
auto_income_salaried.AccountType,
auto_income_salaried.IncomeAssId,
auto_income_salaried.GrosSalary,
auto_income_salaried.NetSalary,
auto_income_salaried.VariableSalary,
auto_income_salaried.VariableSalaryAmount,
auto_income_salaried.VariableSalaryPercent,
auto_income_salaried.CarAllownce,
auto_income_salaried.CarAllownceAmount,
auto_income_salaried.CarAllowncePer,
auto_income_salaried.OwnHouseBenifit,
auto_income_salaried.TotalIncome,
auto_income_salaried.Remarks,
auto_an_salariedvariablesalarymonth.`Month`,
auto_an_salariedvariablesalarymonth.Amount,
auto_an_salariedvariablesalarymonth.SalaiedID
FROM
auto_income_salaried
LEFT OUTER JOIN auto_an_salariedvariablesalarymonth ON auto_an_salariedvariablesalarymonth.SalaiedID = auto_income_salaried.Id
where auto_income_salaried.AnalystMasterId = {0}", analystMasterId);

            //reader = objDbHelper.GetDataReader(queryString);

             dataTable = DbQueryManager.GetTable(queryString);


             reader = dataTable.CreateDataReader();

            var auto_IncomeSalariedDataReader = new Auto_IncomeSalariedDataReader(reader);
            var auto_VariableSalaryCalculationDataReaderForLoop = new Auto_VariableSalaryCalculationDataReader(reader);

            var alreadyAdded = false;
            while (reader.Read())
            {
                alreadyAdded = false;
                int salryId = reader.GetInt32(0);
                for (var i = 0; i < AutoIncomeSalariedList.Count; i++)
                {
                    if (AutoIncomeSalariedList[i].Id == salryId)
                    {
                        alreadyAdded = true;
                        break;
                    }
                }
                if (alreadyAdded == false)
                {
                    AutoIncomeSalaried = auto_IncomeSalariedDataReader.Read();
                    AutoIncomeSalariedList.Add(AutoIncomeSalaried);
                }

                Auto_VariableSalaryCalculationForLoop = auto_VariableSalaryCalculationDataReaderForLoop.Read();

                Auto_VariableSalaryCalculationLoop.Add(Auto_VariableSalaryCalculationForLoop);



            }
            reader.Close();


            //DataSet ds = DbQueryManager.GetDataSet(queryString);
            //DataTable dt = ds.Tables[0];



            //for (var i = 0; i < AutoIncomeSalariedList.Count; i++)
            //{
            //    List<Auto_VariableSalaryCalculation> Auto_VariableSalaryCalculationList = new List<Auto_VariableSalaryCalculation>();
            //    for (var j = 0; j < dt.Rows.Count; j++ )
            //    {
            //        if (!string.IsNullOrEmpty(dt.Rows[j][19].ToString()) && Convert.ToInt32(dt.Rows[j][19]) == AutoIncomeSalariedList[i].Id)
            //        {
            //            var objAuto_VariableSalaryCalculation = new Auto_VariableSalaryCalculation();
            //            objAuto_VariableSalaryCalculation.SalaiedID = Convert.ToInt32(dt.Rows[j][19]);
            //            objAuto_VariableSalaryCalculation.Amount = Convert.ToDouble(dt.Rows[j][18]);
            //            objAuto_VariableSalaryCalculation.Month = Convert.ToInt32(dt.Rows[j][17]);


            //            Auto_VariableSalaryCalculationList.Add(objAuto_VariableSalaryCalculation);
            //        }
            //    }

            //    AutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist = Auto_VariableSalaryCalculationList;

            //}

            for (var i = 0; i < AutoIncomeSalariedList.Count; i++)
            {
                List<Auto_VariableSalaryCalculation> Auto_VariableSalaryCalculationList = new List<Auto_VariableSalaryCalculation>();
                for (var j = 0; j < Auto_VariableSalaryCalculationLoop.Count; j++)
                {
                    if (AutoIncomeSalariedList[i].Id == Auto_VariableSalaryCalculationLoop[j].SalaiedID)
                    {
                        Auto_VariableSalaryCalculationList.Add(Auto_VariableSalaryCalculationLoop[j]);
                    }
                }
                AutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist = Auto_VariableSalaryCalculationList;
            }

            autoLoanApplicationAll.objAutoIncomeSalariedList = AutoIncomeSalariedList;


            #endregion

            #region Auto Landlord
            List<AutoLandLord> autoLandLordlist = new List<AutoLandLord>();

            List<Auto_LandLord_VerifiedRent> Auto_LandLord_VerifiedRentListForloop = new List<Auto_LandLord_VerifiedRent>();

            var autoLandLord = new AutoLandLord();
            var auto_LandLord_VerifiedRent = new Auto_LandLord_VerifiedRent();
            #region Land Lord Quary
            queryString = String.Format(@"SELECT
auto_landlord.Id,
auto_landlord.AutoLoanMasterId,
auto_landlord.EmployerCatagoryId,
auto_landlord.SectorId,
auto_landlord.IncomeAssId,
auto_landlord.TotalRentalIncome,
auto_landlord.ReflectionRequired,
auto_landlord.Percentage,
auto_landlord.AccountType,
auto_landlord.Remarks,
auto_landlord_verifiedrent.Property,
auto_landlord_verifiedrent.`Share`,
auto_landlord_verifiedrent.AutoLandLordId
FROM
auto_landlord
INNER JOIN auto_landlord_verifiedrent ON auto_landlord_verifiedrent.AutoLandLordId = auto_landlord.Id
where auto_landlord.AutoLoanMasterId = {0}", autoLoanMasterID);
            #endregion

            dataTable = DbQueryManager.GetTable(queryString);
            reader = dataTable.CreateDataReader();



            var autoLandLordDataReader = new AutoLandLordDataReader(reader);
            var auto_LandLord_VerifiedRentDataReaderForLoop = new Auto_LandLord_VerifiedRentDataReader(reader);

            alreadyAdded = false;
            while (reader.Read())
            {
                alreadyAdded = false;
                int landlordId = reader.GetInt32(0);
                for (var i = 0; i < autoLandLordlist.Count; i++)
                {
                    if (autoLandLordlist[i].Id == landlordId)
                    {
                        alreadyAdded = true;
                        break;
                    }
                }
                if (alreadyAdded == false)
                {
                    autoLandLord = autoLandLordDataReader.Read();
                    autoLandLordlist.Add(autoLandLord);
                }
                auto_LandLord_VerifiedRent = auto_LandLord_VerifiedRentDataReaderForLoop.Read();
                Auto_LandLord_VerifiedRentListForloop.Add(auto_LandLord_VerifiedRent);

            }
            reader.Close();
            //ds = DbQueryManager.GetDataSet(queryString);
            //dt = ds.Tables[0];



            //for (var i = 0; i < autoLandLordlist.Count; i++)
            //{
            //    List<Auto_LandLord_VerifiedRent> auto_LandLord_VerifiedRentList = new List<Auto_LandLord_VerifiedRent>();
            //    for (var j = 0; j < dt.Rows.Count; j++)
            //    {
            //        if (!string.IsNullOrEmpty(dt.Rows[j][12].ToString()) && Convert.ToInt32(dt.Rows[j][12]) == autoLandLordlist[i].Id)
            //        {
            //            var objAuto_LandLord_VerifiedRent = new Auto_LandLord_VerifiedRent();
            //            objAuto_LandLord_VerifiedRent.AutoLandLordId = Convert.ToInt32(dt.Rows[j][12]);
            //            objAuto_LandLord_VerifiedRent.Property = Convert.ToDouble(dt.Rows[j][10]);
            //            objAuto_LandLord_VerifiedRent.Share = Convert.ToInt32(dt.Rows[j][11]);


            //            auto_LandLord_VerifiedRentList.Add(objAuto_LandLord_VerifiedRent);
            //        }
            //    }

            //    autoLandLordlist[i].Property = auto_LandLord_VerifiedRentList;

            //}

            for (var i = 0; i < autoLandLordlist.Count; i++)
            {
                List<Auto_LandLord_VerifiedRent> auto_LandLord_VerifiedRentList = new List<Auto_LandLord_VerifiedRent>();
                for (var j = 0; j < Auto_LandLord_VerifiedRentListForloop.Count; j++)
                {
                    if (autoLandLordlist[i].Id == Auto_LandLord_VerifiedRentListForloop[j].AutoLandLordId)
                    {
                        auto_LandLord_VerifiedRentList.Add(Auto_LandLord_VerifiedRentListForloop[j]);

                    }
                }
                autoLandLordlist[i].Property = auto_LandLord_VerifiedRentList;
            }

            autoLoanApplicationAll.objAutoLandLord = autoLandLordlist;


            #endregion

            #region Bank Statement For Bussiness Man And Self Employed


            List<Auto_AnalystIncomBankStatementSum> autoAnalystIncomBankStatementSumList = new List<Auto_AnalystIncomBankStatementSum>();
            var autoAnalystIncomBankStatementSum = new Auto_AnalystIncomBankStatementSum();



            queryString = String.Format(@"Select * from auto_anincombstatemetsum where AnalystMasterId={0}", analystMasterId);

             dataTable = DbQueryManager.GetTable(queryString);
            if (dataTable !=null)
            {
                reader = dataTable.CreateDataReader();
                var autoAnalystIncomBankStatementSumDataReader = new Auto_AnalystIncomBankStatementSumDataReader(reader);
                while (reader.Read())
                {
                    autoAnalystIncomBankStatementSum = autoAnalystIncomBankStatementSumDataReader.Read();
                    autoAnalystIncomBankStatementSumList.Add(autoAnalystIncomBankStatementSum);
                }

                reader.Close();
            }
            

            #region Auto loan Facility

            queryString = String.Format(@"Select * from auto_an_overdraftfacility where AutoLoanMasterId ={0}", autoLoanMasterID);
            //ds = DbQueryManager.GetDataSet(queryString);
            //dt = ds.Tables[0];
            //for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++) { 
            //    List<Auto_An_OverDraftFacility> ObjAutoAnOverDraftFacilityList = new List<Auto_An_OverDraftFacility>();
            //    for (var j = 0; j < dt.Rows.Count; j++)
            //    {
            //        if (Convert.ToInt32(dt.Rows[j][2]) == autoAnalystIncomBankStatementSumList[i].Id)
            //        {
            //            var objAuto_An_OverDraftFacility = new Auto_An_OverDraftFacility();
            //            objAuto_An_OverDraftFacility.BankId = Convert.ToInt32(dt.Rows[j][4]);
            //            objAuto_An_OverDraftFacility.LimitAmount = Convert.ToDouble(dt.Rows[j][5]);
            //            objAuto_An_OverDraftFacility.Interest = Convert.ToDouble(dt.Rows[j][6]);
            //            objAuto_An_OverDraftFacility.Consider = Convert.ToInt32(dt.Rows[j][7]);
            //            objAuto_An_OverDraftFacility.Type = Convert.ToInt32(dt.Rows[j][8]);

            //            ObjAutoAnOverDraftFacilityList.Add(objAuto_An_OverDraftFacility);
            //        }
            //    }

            //    autoAnalystIncomBankStatementSumList[i].AutoAnOverDraftFacility = ObjAutoAnOverDraftFacilityList;

            //}

            List<Auto_An_OverDraftFacility> ObjAutoAnOverDraftFacilityListForLoop = new List<Auto_An_OverDraftFacility>();
            var objAuto_An_OverDraftFacilityForLoop = new Auto_An_OverDraftFacility();
            dataTable = DbQueryManager.GetTable(queryString);
            if (dataTable != null)
            {
                reader = dataTable.CreateDataReader();
                var auto_An_OverDraftFacilityDataReader = new Auto_An_OverDraftFacilityDataReader(reader);
                while (reader.Read())
                {
                    objAuto_An_OverDraftFacilityForLoop = auto_An_OverDraftFacilityDataReader.Read();
                    ObjAutoAnOverDraftFacilityListForLoop.Add(objAuto_An_OverDraftFacilityForLoop);
                }
                reader.Close();
            }
           

            for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
            {
                List<Auto_An_OverDraftFacility> ObjAutoAnOverDraftFacilityList = new List<Auto_An_OverDraftFacility>();
                for (var j = 0; j < ObjAutoAnOverDraftFacilityListForLoop.Count; j++)
                {
                    var objAuto_An_OverDraftFacility = new Auto_An_OverDraftFacility();
                    if (autoAnalystIncomBankStatementSumList[i].Id == ObjAutoAnOverDraftFacilityListForLoop[j].IncomeSumId)
                    {
                        objAuto_An_OverDraftFacility = ObjAutoAnOverDraftFacilityListForLoop[j];
                        ObjAutoAnOverDraftFacilityList.Add(objAuto_An_OverDraftFacility);
                    }
                }
                autoAnalystIncomBankStatementSumList[i].AutoAnOverDraftFacility = ObjAutoAnOverDraftFacilityList;
            }


            #endregion

            #region Previous Repayment History

            queryString = String.Format(@"Select * from auto_previous_repayment_history where AutoLoanMasterId ={0}", autoLoanMasterID);
            dataTable = DbQueryManager.GetTable(queryString);
            if (dataTable != null)
            {
                reader = dataTable.CreateDataReader();
                var auto_PreviousRePaymentHistoryDataReaderForLoop = new Auto_PreviousRePaymentHistoryDataReader(reader);
                List<Auto_PreviousRePaymentHistory> ObjAuto_PreviousRePaymentHistoryListForLoop =
                    new List<Auto_PreviousRePaymentHistory>();
                var objAuto_PreviousRePaymentHistoryForLoop = new Auto_PreviousRePaymentHistory();
                while (reader.Read())
                {
                    objAuto_PreviousRePaymentHistoryForLoop = auto_PreviousRePaymentHistoryDataReaderForLoop.Read();
                    ObjAuto_PreviousRePaymentHistoryListForLoop.Add(objAuto_PreviousRePaymentHistoryForLoop);
                }
                reader.Close();



                //ds = DbQueryManager.GetDataSet(queryString);
                //dt = ds.Tables[0];

                //for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                //{
                //    if (dt != null)
                //    {
                //        for (var j = 0; j < dt.Rows.Count; j++)
                //        {
                //            if (Convert.ToInt32(dt.Rows[j][1]) == autoAnalystIncomBankStatementSumList[i].Id)
                //            {
                //                var objAutoPreviousRePaymentHistory = new Auto_PreviousRePaymentHistory();
                //                objAutoPreviousRePaymentHistory.Id = Convert.ToInt32(dt.Rows[j][0]);
                //                objAutoPreviousRePaymentHistory.IncomeSumId = Convert.ToInt32(dt.Rows[j][1]);
                //                objAutoPreviousRePaymentHistory.BankStatementId = Convert.ToInt32(dt.Rows[j][3]);
                //                objAutoPreviousRePaymentHistory.BankId = Convert.ToInt32(dt.Rows[j][4]);
                //                objAutoPreviousRePaymentHistory.OffScb_Total = Convert.ToDouble(dt.Rows[j][5]);
                //                objAutoPreviousRePaymentHistory.OnScb_Total = Convert.ToDouble(dt.Rows[j][6]);
                //                objAutoPreviousRePaymentHistory.TotalEMIClosingConsidered = Convert.ToInt32(dt.Rows[j][7]);
                //                objAutoPreviousRePaymentHistory.Remarks = Convert.ToString(dt.Rows[j][8]);
                //                autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory = objAutoPreviousRePaymentHistory;
                //                break;
                //            }
                //        }
                //    }
                //}

                for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                {
                    for (var j = 0; j < ObjAuto_PreviousRePaymentHistoryListForLoop.Count; j++)
                    {
                        if (autoAnalystIncomBankStatementSumList[i].Id ==
                            ObjAuto_PreviousRePaymentHistoryListForLoop[j].IncomeSumId)
                        {
                            autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory =
                                ObjAuto_PreviousRePaymentHistoryListForLoop[j];
                            break;
                        }
                    }
                }

            }
            queryString = String.Format(@"Select * from auto_sub_repaymenthistoryoffscb_onscb where AutoLoanMasterId = {0}", autoLoanMasterID);

            #region Old Code For Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB

            //ds = DbQueryManager.GetDataSet(queryString);
            //dt = ds.Tables[0];

            //for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
            //{
            //    if (autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory != null)
            //    {
            //        var prevRepaymentHisId = autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory.Id;
            //        List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB> objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBlist = new List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB>();

            //        if (dt != null)
            //        {
            //            for (var j = 0; j < dt.Rows.Count; j++)
            //            {

            //                if (prevRepaymentHisId == Convert.ToInt32(dt.Rows[j][2]))
            //                {
            //                    var objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB = new Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB();
            //                    objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.BankId = Convert.ToInt32(dt.Rows[j][3]);
            //                    objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.Type = Convert.ToInt32(dt.Rows[j][4]);
            //                    objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.EMI = Convert.ToDouble(dt.Rows[j][5]);
            //                    objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.Consider = Convert.ToBoolean(dt.Rows[j][6]);
            //                    objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBlist.Add(objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB);
            //                }
            //            }
            //        }

            //        autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb = objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBlist;
            //    }
            //}

            #endregion

            dataTable = DbQueryManager.GetTable(queryString);
            if (dataTable != null)
            {

                reader = dataTable.CreateDataReader();
                var Auto_SubPreviousRepaymentHistoryOffSCB_OnSCBDataReaderForLoop =
                    new Auto_SubPreviousRepaymentHistoryOffSCB_OnSCBDataReader(reader);
                List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB>
                    ObjAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBListForLoop =
                        new List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB>();
                var objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBForLoop =
                    new Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB();
                while (reader.Read())
                {
                    objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBForLoop =
                        Auto_SubPreviousRepaymentHistoryOffSCB_OnSCBDataReaderForLoop.Read();
                    ObjAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBListForLoop.Add(
                        objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBForLoop);
                }
                reader.Close();
                for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                {
                    if (autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory != null)
                    {
                        var prevRepaymentHisId = autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory.Id;
                        List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB>
                            objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBlist =
                                new List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB>();
                        for (var j = 0; j < ObjAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBListForLoop.Count; j++)
                        {
                            if (prevRepaymentHisId ==
                                ObjAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBListForLoop[j].PreviousRepaymentHistoryId)
                            {
                                objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBlist.Add(
                                    ObjAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBListForLoop[j]);
                            }
                        }
                        autoAnalystIncomBankStatementSumList[i].AutoPreviousRePaymentHistory
                            .AutoSubPreviousRepaymentHistoryOffScbOnScb =
                            objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCBlist;
                    }

                }

            }

            #endregion

            #region Auto_PracticingDoctor

            queryString = String.Format(@"Select * from auto_practicing_doctor where AutoLoanMasterId ={0}", autoLoanMasterID);
            if (dataTable != null)
            {
                dataTable = DbQueryManager.GetTable(queryString);
                reader = dataTable.CreateDataReader();
                var Auto_PracticingDoctorDataReaderForLoop = new Auto_PracticingDoctorDataReader(reader);
                List<Auto_PracticingDoctor> ObjAuto_PracticingDoctorListForLoop = new List<Auto_PracticingDoctor>();
                var objAuto_PracticingDoctorForLoop = new Auto_PracticingDoctor();
                while (reader.Read())
                {
                    objAuto_PracticingDoctorForLoop = Auto_PracticingDoctorDataReaderForLoop.Read();
                    ObjAuto_PracticingDoctorListForLoop.Add(objAuto_PracticingDoctorForLoop);
                }
                reader.Close();




                #region old Code

                //ds = DbQueryManager.GetDataSet(queryString);
                //dt = ds.Tables[0];

                //for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                //{
                //    if (dt != null)
                //    {
                //        for (var j = 0; j < dt.Rows.Count; j++)
                //        {
                //            if (Convert.ToInt32(dt.Rows[j][1]) == autoAnalystIncomBankStatementSumList[i].Id)
                //            {
                //                var objAuto_PracticingDoctor = new Auto_PracticingDoctor();
                //                objAuto_PracticingDoctor.BankStatementId = Convert.ToInt32(dt.Rows[j][3]);
                //                objAuto_PracticingDoctor.Id = Convert.ToInt32(dt.Rows[j][0]);
                //                objAuto_PracticingDoctor.AutoLoanMasterId = Convert.ToInt32(dt.Rows[j][2]);
                //                objAuto_PracticingDoctor.IncomeSumId = Convert.ToInt32(dt.Rows[j][1]);
                //                objAuto_PracticingDoctor.Type = Convert.ToInt32(dt.Rows[j][4]);
                //                objAuto_PracticingDoctor.TotalChamberIncome = Convert.ToDouble(dt.Rows[j][5]);
                //                objAuto_PracticingDoctor.OtherOTIncome = Convert.ToDouble(dt.Rows[j][6]);
                //                objAuto_PracticingDoctor.TotalIncome = Convert.ToDouble(dt.Rows[j][7]);

                //                autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor = objAuto_PracticingDoctor;
                //                break;
                //            }
                //        }

                //    }
                //}

                #endregion

                for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                {
                    for (var j = 0; j < ObjAuto_PracticingDoctorListForLoop.Count; j++)
                    {
                        if (autoAnalystIncomBankStatementSumList[i].Id ==
                            ObjAuto_PracticingDoctorListForLoop[j].IncomeSumId)
                        {
                            autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor =
                                ObjAuto_PracticingDoctorListForLoop[j];
                            break;
                        }
                    }
                }
            }


            queryString = String.Format(@"Select * from auto_chamber_income where AutoLoanMasterId = {0}", autoLoanMasterID);
            dataTable = DbQueryManager.GetTable(queryString);
            if (dataTable != null)
            {
                reader = dataTable.CreateDataReader();
                var Auto_ChamberIncomeDataReaderForLoop = new Auto_ChamberIncomeDataReader(reader);
                List<Auto_ChamberIncome> ObjAuto_ChamberIncomeListForLoop = new List<Auto_ChamberIncome>();
                var objAuto_ChamberIncomeForLoop = new Auto_ChamberIncome();
                while (reader.Read())
                {
                    objAuto_ChamberIncomeForLoop = Auto_ChamberIncomeDataReaderForLoop.Read();
                    ObjAuto_ChamberIncomeListForLoop.Add(objAuto_ChamberIncomeForLoop);
                }
                reader.Close();

                #region Old Code

                //ds = DbQueryManager.GetDataSet(queryString);
                //dt = ds.Tables[0];

                //for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                //{
                //    if (autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor != null)
                //    {
                //        var doctorId = autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor.Id;
                //        List<Auto_ChamberIncome> objAuto_ChamberIncomeList = new List<Auto_ChamberIncome>();
                //        if (dt != null)
                //        {
                //            for (var j = 0; j < dt.Rows.Count; j++)
                //            {

                //                if (doctorId == Convert.ToInt32(dt.Rows[j][2]))
                //                {
                //                    var objAuto_ChamberIncome = new Auto_ChamberIncome();
                //                    objAuto_ChamberIncome.Id = Convert.ToInt32(dt.Rows[j][0]);
                //                    objAuto_ChamberIncome.AutoPracticingDoctorId = Convert.ToInt32(dt.Rows[j][2]);
                //                    objAuto_ChamberIncome.ChamberId = Convert.ToInt32(dt.Rows[j][3]);
                //                    objAuto_ChamberIncome.OldPatents = Convert.ToInt32(dt.Rows[j][4]);
                //                    objAuto_ChamberIncome.OldFee = Convert.ToDouble(dt.Rows[j][5]);
                //                    objAuto_ChamberIncome.OldTotal = Convert.ToDouble(dt.Rows[j][6]);
                //                    objAuto_ChamberIncome.NewPatents = Convert.ToInt32(dt.Rows[j][7]);
                //                    objAuto_ChamberIncome.NewFee = Convert.ToDouble(dt.Rows[j][8]);
                //                    objAuto_ChamberIncome.NewTotal = Convert.ToDouble(dt.Rows[j][9]);
                //                    objAuto_ChamberIncome.Total = Convert.ToDouble(dt.Rows[j][10]);
                //                    objAuto_ChamberIncome.DayMonth = Convert.ToInt32(dt.Rows[j][11]);
                //                    objAuto_ChamberIncome.Income = Convert.ToDouble(dt.Rows[j][12]);

                //                    objAuto_ChamberIncomeList.Add(objAuto_ChamberIncome);
                //                }
                //            }
                //        }
                //        autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor.AutoChamberIncome = objAuto_ChamberIncomeList;
                //    }
                //}

                #endregion

                for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                {
                    if (autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor != null)
                    {
                        var doctorId = autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor.Id;
                        List<Auto_ChamberIncome> objAuto_ChamberIncomeList = new List<Auto_ChamberIncome>();
                        for (var j = 0; j < ObjAuto_ChamberIncomeListForLoop.Count; j++)
                        {
                            if (doctorId == ObjAuto_ChamberIncomeListForLoop[j].AutoPracticingDoctorId)
                            {
                                objAuto_ChamberIncomeList.Add(ObjAuto_ChamberIncomeListForLoop[j]);
                            }
                        }
                        autoAnalystIncomBankStatementSumList[i].AutoPracticingDoctor.AutoChamberIncome =
                            objAuto_ChamberIncomeList;
                    }

                }


            }

            #endregion


            #region Auto_TutioningTeacher
            queryString = String.Format(@"Select * from auto_tutioning_teacher where AutoLoanMasterId ={0}", autoLoanMasterID);
            dataTable = DbQueryManager.GetTable(queryString);
            if (dataTable != null)
            {
                reader = dataTable.CreateDataReader();
                var Auto_TutioningTeacherDataReaderForLoop = new Auto_TutioningTeacherDataReader(reader);
                List<Auto_TutioningTeacher> ObjAuto_TutioningTeacherListForLoop = new List<Auto_TutioningTeacher>();
                var objAuto_TutioningTeacherForLoop = new Auto_TutioningTeacher();
                while (reader.Read())
                {
                    objAuto_TutioningTeacherForLoop = Auto_TutioningTeacherDataReaderForLoop.Read();
                    ObjAuto_TutioningTeacherListForLoop.Add(objAuto_TutioningTeacherForLoop);
                }
                reader.Close();

                #region Old Code

                //ds = DbQueryManager.GetDataSet(queryString);
                //dt = ds.Tables[0];

                //for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                //{
                //    if (dt != null) {
                //        for (var j = 0; j < dt.Rows.Count; j++) {
                //            if (autoAnalystIncomBankStatementSumList[i].Id == Convert.ToInt32(dt.Rows[j][1]))
                //            {
                //                var objTutoningIncome = new Auto_TutioningTeacher();
                //                objTutoningIncome.Id = Convert.ToInt32(dt.Rows[j][0]);
                //                objTutoningIncome.IncomeSumId = Convert.ToInt32(dt.Rows[j][1]);
                //                objTutoningIncome.AutoLoanMasterId = Convert.ToInt32(dt.Rows[j][2]);
                //                objTutoningIncome.BankStatementId = Convert.ToInt32(dt.Rows[j][3]);
                //                objTutoningIncome.Type = Convert.ToInt32(dt.Rows[j][4]);
                //                objTutoningIncome.NoOfStudent = Convert.ToInt32(dt.Rows[j][5]);
                //                objTutoningIncome.NoOfDaysForTution = Convert.ToInt32(dt.Rows[j][6]);
                //                objTutoningIncome.Fees = Convert.ToInt32(dt.Rows[j][7]);
                //                objTutoningIncome.TotalIncome = Convert.ToInt32(dt.Rows[j][8]);
                //                autoAnalystIncomBankStatementSumList[i].AutoTeacherIncome = objTutoningIncome;
                //                break;
                //            }
                //        }
                //    }
                //}

                #endregion

                for (var i = 0; i < autoAnalystIncomBankStatementSumList.Count; i++)
                {
                    for (var j = 0; j < ObjAuto_TutioningTeacherListForLoop.Count; j++)
                    {
                        if (autoAnalystIncomBankStatementSumList[i].Id ==
                            ObjAuto_TutioningTeacherListForLoop[j].IncomeSumId)
                        {
                            autoAnalystIncomBankStatementSumList[i].AutoTeacherIncome =
                                ObjAuto_TutioningTeacherListForLoop[j];
                            break;
                        }
                    }
                }

            }

            #endregion



            autoLoanApplicationAll.objAutoAnalystIncomBankStatementSum = autoAnalystIncomBankStatementSumList;




            #endregion

            #endregion

            #endregion Loan Appliaction Analyst Part

            #region Extra Property

            #region Segment settings

            //List<Auto_An_IncomeSegmentIncome> auto_An_IncomeSegmentIncomeList = new List<Auto_An_IncomeSegmentIncome>();
            var autoSegmentSettings = new AutoSegmentSettings();
            autoSegmentSettings = null;

            queryString = String.Format(@"select 	SegmentID, SegmentCode, SegmentName, Description, Profession, MinLoanAmt, MaxLoanAmt, MinTanor, 
	MaxTanor, IrWithARTA,IrWithoutARTA, MinAgeL1, MinAgeL2, MaxAgeL1, MaxAgeL2, LTVL1, LTVL2, ExperienceL1, ExperienceL2, 
	AcRelationshipL1, AcRelationshipL2, CriteriaCode, AssetCode, MinDBRL1, MinDBRL2, MaxDBRL2, 
	DBRL3, FieldVisit, Verification, LandTelephone, GuranteeReference, PrimaryMinIncomeL1, 
	PrimaryMinIncomeL2, JointMinIncomeL1, JointMinIncomeL2, Comments, UserId, LastUpdateDate,t_pluss_profession.PROF_NAME,t_pluss_profession.PROF_ID,

(select count(SegmentID) from auto_segment) as TotalCount 
from auto_segment inner join t_pluss_profession on  auto_segment.Profession = t_pluss_profession.PROF_ID 
where SegmentID = {0}", sigmentID);

            //reader = objDbHelper.GetDataReader(queryString);
            //var autoSegmentSettingsDataReader = new AutoSegmentSettingsDataReader(reader);
            //while (reader.Read())
            //{
            //    autoSegmentSettings = autoSegmentSettingsDataReader.Read();
            //}
            //reader.Close();
            var segSetting = DataTableToList.GetList<AutoSegmentSettings>(DbQueryManager.GetTable(queryString));
            if (segSetting != null && segSetting.Any())
            {
                autoLoanApplicationAll.objAutoSegmentSettings = DataTableToList.GetList<AutoSegmentSettings>(DbQueryManager.GetTable(queryString)).FirstOrDefault(); 
            }
            

            #endregion Segment settings

            #endregion Extra Property

            List<Auto_An_OverDraftFacility> auto_An_OverDraftFacilityList = new List<Auto_An_OverDraftFacility>();
            //Auto_An_OverDraftFacility auto_An_OverDraftFacility = new Auto_An_OverDraftFacility();

            foreach (Auto_AnalystIncomBankStatementSum a in autoLoanApplicationAll.objAutoAnalystIncomBankStatementSum)
            {
                foreach (Auto_An_OverDraftFacility b in a.AutoAnOverDraftFacility)
                {
                    auto_An_OverDraftFacilityList.Add(b);
                }
            }

            autoLoanApplicationAll.Auto_An_OverDraftFacilityList = auto_An_OverDraftFacilityList;

            return autoLoanApplicationAll;
        }



        #endregion


        #region Finalization Tab when Page Load

        public List<DaviationSettingsEntity> getDeviationByLevel(int deviationFor)
        {
            List<DaviationSettingsEntity> allDeviation = new List<DaviationSettingsEntity>();
            string sql = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                if (deviationFor > 0)
                {
                    sql = string.Format(@"Select distinct * from auto_deviation where DeviationFor = {0}", deviationFor);
                }
                else
                {
                    sql = string.Format(@"Select distinct * from auto_deviation");
                }

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var allDeviationDataReader = new DaviationSettingsDataReader(reader);
                while (reader.Read())
                    allDeviation.Add(allDeviationDataReader.Read());
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return allDeviation;
        }

        public List<DaviationSettingsEntity> getDeviationLevel()
        {
            List<DaviationSettingsEntity> allDeviationLevel = new List<DaviationSettingsEntity>();
            string sql = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                sql = string.Format(@"Select distinct DeviationFor from auto_deviation");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var allDeviationDataReader = new DaviationSettingsDataReader(reader);
                while (reader.Read())
                    allDeviationLevel.Add(allDeviationDataReader.Read());
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return allDeviationLevel;
        }

        public string getDeviationCodeByID(int deviationID)
        {
            string returnValue = "";
            string sql = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                sql = string.Format(@"Select DeviationCode from auto_deviation where DeviationId = {0}", deviationID);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                while (reader.Read())
                    returnValue = Convert.ToString(reader.GetValue(0));
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return returnValue;
        }

        public List<t_Pluss_Remark> getRemarkStrangthConditionWeaknessAll()
        {
            List<t_Pluss_Remark> allt_Pluss_Remark = new List<t_Pluss_Remark>();
            string sql = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                sql = string.Format(@"Select distinct * from t_pluss_remarks");

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var allt_Pluss_RemarkDataReader = new t_Pluss_RemarkDataReader(reader);
                while (reader.Read())
                    allt_Pluss_Remark.Add(allt_Pluss_RemarkDataReader.Read());
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return allt_Pluss_Remark;
        }

        public List<t_pluss_user> gett_pluss_userAll(int userId)
        {
            List<t_pluss_user> allt_pluss_user = new List<t_pluss_user>();
            string sql = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                sql = string.Format(@"Select distinct * from t_pluss_user where USER_ID <> {0}", userId);
               

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var allt_pluss_userDataReader = new t_pluss_userDataReader(reader);
                while (reader.Read())
                    allt_pluss_user.Add(allt_pluss_userDataReader.Read());
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return allt_pluss_user;
        }

        public List<t_pluss_user> get_pluss_userAll()
        {
            List<t_pluss_user> allt_pluss_user = new List<t_pluss_user>();
            string sql = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                sql = string.Format(@"Select distinct * from t_pluss_user");


                IDataReader reader = objDbHelper.GetDataReader(sql);

                var allt_pluss_userDataReader = new t_pluss_userDataReader(reader);
                while (reader.Read())
                    allt_pluss_user.Add(allt_pluss_userDataReader.Read());
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }

            return allt_pluss_user;
        }

        public t_pluss_userapproverlimit GetApproverLimitByUserId(int userId)
        {
            var dbHelper = new CommonDbHelper();
            var objt_pluss_userapproverlimit = new t_pluss_userapproverlimit();
            objt_pluss_userapproverlimit = null;
            try
            {
                string sql = string.Format(@"SELECT
t_pluss_userapproverlimit.USER_APP_ID,
t_pluss_userapproverlimit.USER_ID,
t_pluss_userapproverlimit.PROD_ID,
t_pluss_userapproverlimit.PROD_TAG,
t_pluss_userapproverlimit.LEVEL1_AMT,
t_pluss_userapproverlimit.LEVEL2_AMT,
t_pluss_userapproverlimit.LEVEL3_AMT,
t_pluss_userapproverlimit.CHANG_DT,
t_pluss_userapproverlimit.CHANG_BY,
t_pluss_userapproverlimit.`STATUS`,
Sum(t_pluss_userapproverlimit.LEVEL1_AMT) AS TotalDLAAmountL1,
Sum(t_pluss_userapproverlimit.LEVEL2_AMT) AS TotalDLAAmountL2,
Sum(t_pluss_userapproverlimit.LEVEL3_AMT) AS TotalDLAAmountL3
FROM
t_pluss_userapproverlimit
where t_pluss_userapproverlimit.USER_ID = {0} and t_pluss_userapproverlimit.`STATUS` = 1
Group by 
t_pluss_userapproverlimit.USER_APP_ID,
t_pluss_userapproverlimit.USER_ID,
t_pluss_userapproverlimit.PROD_ID,
t_pluss_userapproverlimit.PROD_TAG,
t_pluss_userapproverlimit.LEVEL1_AMT,
t_pluss_userapproverlimit.LEVEL2_AMT,
t_pluss_userapproverlimit.LEVEL3_AMT,
t_pluss_userapproverlimit.CHANG_DT,
t_pluss_userapproverlimit.CHANG_BY,
t_pluss_userapproverlimit.`STATUS`
ORDER BY t_pluss_userapproverlimit.PROD_TAG asc
", userId);
                IDataReader reader = dbHelper.GetDataReader(sql);

                var tplussuserapproverlimitDataReader = new t_pluss_userapproverlimitDataReader(reader);
                if (reader.Read())
                    objt_pluss_userapproverlimit = tplussuserapproverlimitDataReader.Read();
                reader.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return objt_pluss_userapproverlimit;
        }


        #endregion Finalization Tab when Page Load


        


        #region Get BA Letter
        public BALetter_Entity GetBALetter(int AutoLoanMasterId)
        {
            var condition = "where AutoLoanMasterId =" + AutoLoanMasterId;

            var objBALetter = new BALetter_Entity();
            var objDbHelper = new CommonDbHelper();
            try
            {

                string sql =
                    string.Format(
                        @"SELECT
                                view_baletter.LlId,
                                view_baletter.AutoLoanMasterId,
                                view_baletter.AnalystMasterId,
                                view_baletter.AutoBranchId,
                                view_baletter.AutoBranchName,
                                view_baletter.AppliedDate,
                                view_baletter.TotalLoanAmount,
                                view_baletter.ProductId,
                                view_baletter.PROD_NAME,
                                view_baletter.MaxEMI,
                                view_baletter.ConsideredTenor,
                                view_baletter.ManufacturerName,
                                view_baletter.ConsideredRate,
                                view_baletter.ConsideredInMonth,
                                view_baletter.Model,
                                view_baletter.HypothecationCheck,
                                view_baletter.SecurityPDC,
                                view_baletter.AccountNumber,
                                view_baletter.SCBAccount
                                from view_baletter {0}",
                        condition);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var bALetterDataReader = new BALetterDataReader(reader);
                while (reader.Read())
                    objBALetter = bALetterDataReader.Read();
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objBALetter;
        }
        #endregion

        #region Save Analyst
        public string SaveAnalystLoanApplication(Auto_AnalystAll analystAll, int stateid, int userId)
        {
            string returnValue = "";
            string deleteSql = "";
            string selectSql = "";

            int autoLoanMasterId = 0;
            int analystMasterId = 0;
            var msg = "";

            var objDbHelper = new CommonDbHelper();
            try
            {
                objDbHelper.BeginTransaction();


                #region General Tab

                //Save Auto Analyst Master
                autoLoanMasterId = analystAll.AutoAnalistMaster.AutoLoanMasterId;
                analystMasterId = SaveAutoLoanAnalystMaster(analystAll.AutoAnalistMaster, objDbHelper);

                #region update ExecutionUserBY table

                Auto_App_Step_ExecutionByUser ebu = new Auto_App_Step_ExecutionByUser();

                ebu.Auto_LoanMasterId = autoLoanMasterId;
                ebu.AppAnalysisBy = userId;
                ebu.AppApprovedBy = analystAll.AutoAnFinalizationAppraiserAndApprover.ApprovedByUserID;
                    
                UpdateExecutionByUser(ebu,"anlyst", objDbHelper);

                #endregion update ExecutionUserBY table


                //Save auto Loan Analyst Application
                analystAll.AutoLoanAnalystApplication.AnalystMasterId = analystMasterId;
                SaveAutoLoanAnalystApplication(analystAll.AutoLoanAnalystApplication, objDbHelper);

                if (analystAll.AutoLoanVehicle.VendorId == -1)
                {
                    if (analystAll.AutoLoanVendore.VendorName != "")
                    {
                        int newVendorID = SaveNewVendor(analystAll.AutoLoanVendore, objDbHelper);

                        analystAll.AutoLoanVehicle.AutoLoanMasterId = autoLoanMasterId;
                        analystAll.AutoLoanVehicle.VendorId = newVendorID;
                        SaveAutoLoanVehicle(analystAll.AutoLoanVehicle, objDbHelper);
                    }
                }
                else
                {
                    analystAll.AutoLoanVehicle.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLoanVehicle(analystAll.AutoLoanVehicle, objDbHelper);
                }

                //Save auto Loan Analyst Vehicle Detail
                analystAll.AutoLoanAnalystVehicleDetail.AnalystMasterId = analystMasterId;
                SaveAutoLoanAnalystVehicleDetails(analystAll.AutoLoanAnalystVehicleDetail, objDbHelper);

                // Save With SCB Accounts
                deleteSql = "Delete from auto_applicantscbaccount where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                foreach (AutoApplicantSCBAccount scbAccount in analystAll.AutoApplicantAccountScbList)
                {
                    scbAccount.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoApplicantSCBAccount(scbAccount, objDbHelper);
                }

                //Save PR Account Detail
                deleteSql = "Delete from auto_praccount_details where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                foreach (AutoPRAccountDetail pra in analystAll.AutoApplicantAccountOtherList)
                {
                    pra.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoPRAccountDetail(pra, objDbHelper);
                }
                //Change by Washik 18/11/2012
                //Save Auto Loan Facility
                deleteSql = "Delete from auto_loanfacility where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_loansecurity where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                //foreach (AutoLoanFacility af in analystAll.AutoFacilityList)
                //{
                //    af.AutoLoanMasterId = autoLoanMasterId;
                //    SaveAutoLoanFacility(af, objDbHelper);
                //}

                ////Save Auto Loan Security
                //foreach (AutoLoanSecurity al in analystAll.AutoSecurityList)
                //{
                //    al.AutoLoanMasterId = autoLoanMasterId;
                //    SaveAutoLoanSecurity(al, objDbHelper);
                //}

                for (var i = 0; i < analystAll.AutoFacilityList.Count; i++)
                {
                    analystAll.AutoFacilityList[i].AutoLoanMasterId = autoLoanMasterId;
                    int facilityId = SaveAutoLoanFacilityWithReturn(analystAll.AutoFacilityList[i], objDbHelper);

                    //Auto Loan Sequrity
                    analystAll.AutoSecurityList[i].AutoLoanMasterId = autoLoanMasterId;
                    analystAll.AutoSecurityList[i].FacilityId = facilityId;
                    SaveAutoLoanSecurity(analystAll.AutoSecurityList[i], objDbHelper);
                }

                // Save Auto Loan Facility OFF SCB
                deleteSql = "Delete from auto_loanfacilityoffscb where AnalystMasterId = " + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                foreach (AutoLoanFacilityOffSCB afscb in analystAll.OFfscbFacilityList)
                {
                    afscb.AnalystMasterId = analystMasterId;
                    afscb.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLoanFacilityOffSCB(afscb, objDbHelper);
                }

                #endregion General Tab

                #region IncomeGenerator Tab

                #region Salaried

                deleteSql = "Delete from auto_income_salaried where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_an_salariedvariablesalarymonth where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                for (var i = 0; i < analystAll.Salaried.Count; i++ )
                {
                    analystAll.Salaried[i].AnalystMasterId = analystMasterId;
                    int salaryId = SaveAutoIncomeSalaried(analystAll.Salaried[i], objDbHelper);
                    
                    for(var j=0; j<analystAll.Salaried[i].ObjSalariedVariableSalaryMonthIncomelist.Count; j++)
                    {
                        analystAll.Salaried[i].ObjSalariedVariableSalaryMonthIncomelist[j].SalaiedID =
                            salaryId;
                        analystAll.Salaried[i].ObjSalariedVariableSalaryMonthIncomelist[j].AnalystMasterId =
                            analystMasterId;
                        SaveAuto_VariableSalaryCalculation(
                            analystAll.Salaried[i].ObjSalariedVariableSalaryMonthIncomelist[j], objDbHelper);
                    }


                }

                #endregion

                #region Land lord tab

                deleteSql = "Delete from auto_landlord where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.LandLord.Count; i++)
                {

                    analystAll.LandLord[i].AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLandLord(analystAll.LandLord[i], objDbHelper);
                    selectSql = "Select Id from auto_landlord where AutoLoanMasterId = " + autoLoanMasterId + " and AccountType = " + analystAll.LandLord[i].AccountType;
                    int landLordId = 0;
                    IDataReader tempDataReder = objDbHelper.GetDataReader(selectSql);
                    while (tempDataReder.Read())
                    {
                        landLordId = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                    deleteSql = "Delete from auto_landlord_verifiedrent where AutoLandLordId=" + landLordId;
                    objDbHelper.ExecuteNonQuery(deleteSql);
                    for (var j = 0; j < analystAll.LandLord[i].Property.Count; j++)
                    {
                        analystAll.LandLord[i].Property[j].AutoLandLordId = landLordId;
                        SaveAutoLandLordVerifiedRent(analystAll.LandLord[i].Property[j], objDbHelper);
                    }


                }

                #endregion

                #region Bank Statement For BussinessMan and Self Employed
                deleteSql = "Delete from auto_anincombstatemetsum where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_an_overdraftfacility where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                
                deleteSql = "Delete from auto_previous_repayment_history where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_sub_repaymenthistoryoffscb_onscb where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_practicing_doctor where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_chamber_income where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_tutioning_teacher where AutoLoanMasterId=" + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);


                for (var i = 0; i < analystAll.AutoAnalystIncomBankStatementSum.Count; i++ )
                {
                    analystAll.AutoAnalystIncomBankStatementSum[i].AnalystMasterId = analystMasterId;
                    SaveAutoAnalystIncomBankStatementSum(analystAll.AutoAnalystIncomBankStatementSum[i], objDbHelper);
                    int incomBankStatementSum = 0;
                    selectSql = "Select Id from auto_anincombstatemetsum where AnalystMasterId = " + analystMasterId + " and TabType = " + analystAll.AutoAnalystIncomBankStatementSum[i].TabType + " and BankStatementId=" + analystAll.AutoAnalystIncomBankStatementSum[i].BankStatementId + " and Type=" + analystAll.AutoAnalystIncomBankStatementSum[i].Type;
                    IDataReader tempDataReder = objDbHelper.GetDataReader(selectSql);
                    while (tempDataReder.Read())
                    {
                        incomBankStatementSum = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();

                    #region auto_an_overdraftfacility
                    
                    for(var j=0; j<analystAll.AutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.Count; j++)
                    {
                        analystAll.AutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[j].IncomeSumId =incomBankStatementSum;
                        analystAll.AutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[j].AutoLoanMasterId = autoLoanMasterId;
                        SaveAutoAnOverDraftFacility(analystAll.AutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[j], objDbHelper);

                    }
                    #endregion

                    #region AutoBankStatementAvgTotalList Need to be update

                    for (var j = 0; j < analystAll.AutoAnalystIncomBankStatementSum[i].AutoBankStatementAvgTotalList.Count; j++)
                    {
                        updateBankStatementAveragetotalbyId(analystAll.AutoAnalystIncomBankStatementSum[i].AutoBankStatementAvgTotalList[j], objDbHelper);
                    }
                    #endregion

                    #region Previous Repayment history Save and Delete
                   
                    analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.IncomeSumId = incomBankStatementSum;
                    if (analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoLoanMasterId != 0)
                    {
                        analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoLoanMasterId = autoLoanMasterId;
                        SaveAutoPreviousRePaymentHistory(analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory, objDbHelper);
                    }
                    selectSql = "Select Id from auto_previous_repayment_history where AutoLoanMasterId = " + autoLoanMasterId;
                    IDataReader tempDataRederPrHis = objDbHelper.GetDataReader(selectSql);
                    int prevrepHisId = 0;
                    while (tempDataRederPrHis.Read())
                    {
                        prevrepHisId = tempDataRederPrHis.GetInt32(0);
                    }
                    tempDataRederPrHis.Close();

                    
                    if (analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null)
                    {
                        for (var j = 0; j < analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.Count; j++)
                        {
                            analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.
                                AutoSubPreviousRepaymentHistoryOffScbOnScb[j].PreviousRepaymentHistoryId = prevrepHisId;
                            analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.
                                AutoSubPreviousRepaymentHistoryOffScbOnScb[j].AutoLoanMasterId = autoLoanMasterId;

                            SaveAutoSubPreviousRepaymentHistoryOffSCBOnSCB(
                                analystAll.AutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.
                                    AutoSubPreviousRepaymentHistoryOffScbOnScb[j], objDbHelper);
                        }
                    }
                    #endregion
                    
                    #region Auto Practicing Doctor
                    if (analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null)
                    {
                        
                        analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.IncomeSumId = incomBankStatementSum;
                        analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoLoanMasterId = autoLoanMasterId;
                        SaveAutoPractingDoctor(analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor, objDbHelper);
                        selectSql = "Select Id from auto_practicing_doctor where IncomeSumId = " + incomBankStatementSum;
                        IDataReader tempDataRederPrDoctor = objDbHelper.GetDataReader(selectSql);
                        int prDoctorId = 0;
                        while (tempDataRederPrDoctor.Read())
                        {
                            prDoctorId = tempDataRederPrDoctor.GetInt32(0);
                        }
                        tempDataRederPrDoctor.Close();
                        
                        for (var j = 0; j < analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.Count; j++)
                        {
                            analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[j].AutoPracticingDoctorId = prDoctorId;
                            analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[j].AutoLoanMasterId = autoLoanMasterId;
                            SaveAutoChamberIncome(analystAll.AutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[j], objDbHelper);
                        }
                    }

                    #endregion

                    #region Auto Teacher Income
                    if (analystAll.AutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null)
                    {
                        analystAll.AutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.IncomeSumId = incomBankStatementSum;
                        SaveAutoTutioningTeacher(analystAll.AutoAnalystIncomBankStatementSum[i].AutoTeacherIncome, objDbHelper);
                    }

                    #endregion



                }

                #endregion


                #endregion IncomeGenerator Tab

                #region Loan Calculation Tab
                //Total Loan Amount 
                deleteSql = "Delete from auto_an_loancalculationtotal where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_LoanCalculationTotal(analystAll.AutoAnLoanCalculationTotal, objDbHelper);

                //loan Calculation Insurence
                deleteSql = "Delete from auto_an_loancalculationinsurance where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_LoanCalculationInsurance(analystAll.AutoAnLoanCalculationInsurance, objDbHelper);

                //loan Auto An Loan Calculation Amount

                deleteSql = "Delete from auto_an_loancalculationamount where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_LoanCalculationAmount(analystAll.AutoAnLoanCalculationAmount, objDbHelper);

                //Auto An Loan Calculation Interest
                deleteSql = "Delete from auto_an_loancalculationinterest where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_LoanCalculationInterest(analystAll.AutoAnLoanCalculationInterest, objDbHelper);
                
                //Auto An Loan Calculation Tenor
                deleteSql = "Delete from auto_an_loancalculationtenor where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_LoanCalculationTenor(analystAll.AutoAnLoanCalculationTenor, objDbHelper);

                //Auto An Loan Auto An Repayment Capability
                deleteSql = "Delete from auto_an_repaymentcapability where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_RepaymentCapability(analystAll.AutoAnRepaymentCapability, objDbHelper);

                //Auto An Income Segment Income
                deleteSql = "Delete from auto_an_in_seg_income where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_IncomeSegmentIncome(analystAll.AutoAnIncomeSegmentIncome, objDbHelper);

                //Auto An Income Segment Income
                deleteSql = "Delete from auto_an_in_seg_applicant_income where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var k = 0; k < analystAll.AutoAnIncomeSegmentApplicantIncomeList.Count; k++ )
                {
                    SaveAuto_An_IncomeSegmentApplicantIncome(analystAll.AutoAnIncomeSegmentApplicantIncomeList[k], objDbHelper);
                }
                #endregion Loan Calculation Tab

                #region Finalization Tab
                //AutoAnFinalizationDeviation
                deleteSql = "Delete from auto_an_finalizationdeviation where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.AutoAnFinalizationDeviation.Count; i++ )
                {
                    SaveAuto_An_FinalizationDeviation(analystAll.AutoAnFinalizationDeviation[i], objDbHelper);
                }

                //Auto_An_FinalizationDevFound
                deleteSql = "Delete from auto_an_finalizationdevfound where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_FinalizationDevFound(analystAll.AutoAnFinalizationDevFound, objDbHelper);
                //AutoAnFinalizationSterength
                deleteSql = "Delete from auto_an_finalizationsterength where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.AutoAnFinalizationSterength.Count; i++)
                {
                    SaveAuto_An_FinalizationSterength(analystAll.AutoAnFinalizationSterength[i], objDbHelper);
                }
                //AutoAnFinalizationWeakness
                deleteSql = "Delete from auto_an_finalizationweakness where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.AutoAnFinalizationWeakness.Count; i++)
                {
                    SaveAuto_An_FinalizationWeakness(analystAll.AutoAnFinalizationWeakness[i], objDbHelper);
                }
                //AutoAnFinalizationRepaymentMethod
                deleteSql = "Delete from auto_an_finalizationrepaymentmethod where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_FinalizationRepaymentMethod(analystAll.AutoAnFinalizationRepaymentMethod, objDbHelper);

                //AutoAnFinalizationWeakness
                deleteSql = "Delete from auto_an_finalizationcondition where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.AutoAnFinalizationCondition.Count; i++)
                {
                    SaveAuto_An_FinalizationCondition(analystAll.AutoAnFinalizationCondition[i], objDbHelper);
                }
                //AutoAnFinalizationRemark
                deleteSql = "Delete from auto_an_finalizationremark where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.AutoAnFinalizationRemark.Count; i++)
                {
                    SaveAuto_An_FinalizationRemark(analystAll.AutoAnFinalizationRemark[i], objDbHelper);
                }
                //AutoAnFinalizationReqDoc
                deleteSql = "Delete from auto_an_finalizationreqdoc where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_FinalizationReqDoc(analystAll.AutoAnFinalizationReqDoc, objDbHelper);

                //AutoAnFinalizationReqDoc
                //deleteSql = "Delete from auto_an_finalizationreqdoc where AnalystMasterId=" + analystMasterId;
                //objDbHelper.ExecuteNonQuery(deleteSql);
                //SaveAuto_An_FinalizationReqDoc(analystAll.AutoAnFinalizationReqDoc, objDbHelper);

                //AutoAnFinalizationReqDoc
                deleteSql = "Delete from auto_an_finalizationvarificationreport where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_FinalizationVarificationReport(analystAll.AutoAnFinalizationVarificationReport, objDbHelper);

                //AutoAnFinalizationRework
                deleteSql = "Delete from auto_an_rework where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                SaveAuto_An_FinalizationRework(analystAll.AutoAnFinalizationRework, objDbHelper);

                //Auto Finalization Appriaser and Approver
                SaveAuto_An_FinalizationAppraiserAndApprover(analystAll.AutoAnFinalizationAppraiserAndApprover, objDbHelper);

                //AutoAnVerificationReport
                
                SaveAuto_An_AlertVerificationReport(analystAll.AutoAnAlertVerificationReport, objDbHelper);

                deleteSql = "Delete from auto_An_CibRequestLoan where AnalystMasterId=" + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);
                for (var i = 0; i < analystAll.AutoCibrequestLoan.Count; i++)
                {
                    SaveAuto_auto_An_CibRequestLoan(analystAll.AutoCibrequestLoan[i], objDbHelper);
                }


                #endregion Finalization Tab

                #region Change Stateus For Forwading Analyst To Approver
                if (stateid != 0) {
                    AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                    objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;
                    objAutoStatusHistory.StatusID = stateid;
                    objAutoStatusHistory.UserID = userId;

                    string sql = string.Format(@"update auto_loanmaster set 
                                            StatusID =" + objAutoStatusHistory.StatusID + " where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId + "");
                    objDbHelper.ExecuteNonQuery(sql);

                    sql = string.Format(@"update auto_loananalystmaster set AnalystRemark = '" + analystAll.AutoAnalistMaster.AnalystRemark + "' where AutoLoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId + "");
                    objDbHelper.ExecuteNonQuery(sql);


                    sql = "select LLID from auto_loanmaster where Auto_LoanMasterId =" + objAutoStatusHistory.AutoLoanMasterId;

                    IDataReader reader = objDbHelper.GetDataReader(sql);
                    int llid = 0;
                    while (reader.Read())
                    {
                        llid = reader.GetInt32(0);
                    }
                    reader.Close();

                    if (llid > 0)
                    {
                        sql = String.Format("UPDATE loan_app_info SET LOAN_STATUS_ID = {1} " +
                                       "WHERE LOAN_APPL_ID = {0}", llid, objAutoStatusHistory.StatusID);

                        var dbHelper1 = new CommonDbHelper("conStringLoanLocator");

                        dbHelper1.ExecuteNonQuery(sql);//.ExecuteNonQuery(sql, "conStringLoanLocator");

                    }

                    AutoLoanDataService autoLoanDataService = new AutoLoanDataService();
                    autoLoanDataService.SaveAutoStatusHistory(objAutoStatusHistory);

                }
                #endregion

                objDbHelper.CommitTransaction();
                msg = "Success";
            }
            catch (Exception ex)
            {

                msg = ex.Message;
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return msg;
        }



        private void UpdateExecutionByUser(Auto_App_Step_ExecutionByUser ebu, string updateUserType, CommonDbHelper objDbHelper)
        {
            string quary = "";
            try { 
                if(updateUserType == "anlyst"){
                    quary = string.Format("Update auto_app_step_executionbyuser set AppAnalysisBy = {0},AppApprovedBy = {2} where Auto_LoanMasterId = {1}", ebu.AppAnalysisBy, ebu.Auto_LoanMasterId,ebu.AppApprovedBy);
                    objDbHelper.ExecuteNonQuery(quary);
                }
                else{
                    quary = string.Format("Update auto_app_step_executionbyuser set AppApprovedBy = {0} where Auto_LoanMasterId = {1}", ebu.AppApprovedBy, ebu.Auto_LoanMasterId);
                    objDbHelper.ExecuteNonQuery(quary);
                }
            }
            catch(Exception ex){
                objDbHelper.RollBack();
            }
        }



        private void SaveAuto_auto_An_CibRequestLoan(auto_An_CibRequestLoan autoCibrequestLoan, CommonDbHelper objDbHelper)
        {
            try
            {

                string sql = string.Format(@"insert into auto_An_CibRequestLoan 
	                                        (AnalystMasterId, CibFacilityId, CibAmount, CibTenor)
	                                        values
	                                        ({0}, {1},'{2}','{3}')", autoCibrequestLoan.AnalystMasterId, autoCibrequestLoan.CibFacilityId, autoCibrequestLoan.CibAmount, autoCibrequestLoan.CibTenor);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }
        private void SaveAutoLoanVehicle(AutoLoanVehicle objAutoLoanVehicle, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoLoanVehicle.AutoLoanVehicleId != 0)
                {
                    sql = String.Format(@"UPDATE auto_loanvehicle SET
                    AutoLoanMasterId = {0}, 
                    VendorId = {1},
                    Auto_ManufactureId = {2},
                    Model = '{3}',
                    TrimLevel = '{4}',
                    EngineCC = '{5}',
                    VehicleType = '{6}',
                    VehicleStatus = {7},
                    ManufacturingYear = {8},
                    QuotedPrice = {9},
                    VerifiedPrice = {10},
                    PriceConsideredBasedOn = '{11}',
                    SeatingCapacity = '{12}',
                    CarVerification  = '{13}',VehicleId  = '{15}' where AutoLoanVehicleId = {14}",
                    objAutoLoanVehicle.AutoLoanMasterId,
                    objAutoLoanVehicle.VendorId, objAutoLoanVehicle.Auto_ManufactureId,
                    objAutoLoanVehicle.Model, objAutoLoanVehicle.TrimLevel, objAutoLoanVehicle.EngineCC, objAutoLoanVehicle.VehicleType,
                    objAutoLoanVehicle.VehicleStatus, objAutoLoanVehicle.ManufacturingYear,
                    objAutoLoanVehicle.QuotedPrice, objAutoLoanVehicle.VerifiedPrice, objAutoLoanVehicle.PriceConsideredBasedOn, objAutoLoanVehicle.SeatingCapacity,
                    objAutoLoanVehicle.CarVerification, objAutoLoanVehicle.AutoLoanVehicleId, objAutoLoanVehicle.VehicleId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_loanvehicle
                    (AutoLoanMasterId, VendorId, Auto_ManufactureId, 
	                Model, TrimLevel, EngineCC, VehicleType, VehicleStatus, ManufacturingYear, 
	                QuotedPrice, VerifiedPrice, PriceConsideredBasedOn, SeatingCapacity, 
	                CarVerification,VehicleId)
                    values
                    ({0}, {1}, {2}, 
                    '{3}', '{4}', '{5}', '{6}', {7}, 
                    {8}, {9}, {10}, '{11}', 
                    '{12}', '{13}',{14})", objAutoLoanVehicle.AutoLoanMasterId,
                    objAutoLoanVehicle.VendorId, objAutoLoanVehicle.Auto_ManufactureId,
                    objAutoLoanVehicle.Model, objAutoLoanVehicle.TrimLevel, objAutoLoanVehicle.EngineCC, objAutoLoanVehicle.VehicleType,
                    objAutoLoanVehicle.VehicleStatus, objAutoLoanVehicle.ManufacturingYear,
                    objAutoLoanVehicle.QuotedPrice, objAutoLoanVehicle.VerifiedPrice, objAutoLoanVehicle.PriceConsideredBasedOn, objAutoLoanVehicle.SeatingCapacity,
                    objAutoLoanVehicle.CarVerification, objAutoLoanVehicle.VehicleId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private int SaveNewVendor(AutoVendor objAutoVendor, CommonDbHelper objDbHelper)
        {
            var returnValue = 0;
            string sql = "";

            try
            {
                sql = String.Format(@"INSERT INTO auto_vendor
                    (VendorName, DealerType) 
                    values
                    ('{0}', {1})", objAutoVendor.VendorName,
                objAutoVendor.DealerType);

                objDbHelper.ExecuteNonQuery(sql);

                //get the jtAccountId
                sql = String.Format(@"Select AutoVendorId from auto_vendor where 
                                        VendorName='{0}' and 
                                        DealerType={1}", objAutoVendor.VendorName, objAutoVendor.DealerType);

                IDataReader reader = objDbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    returnValue = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return returnValue;
        }

        

        #region Salaried

        private int SaveAutoIncomeSalaried(Auto_IncomeSalaried objAutoIncomeSalaried, CommonDbHelper dbHelper)
        {
            int salaryId = 0;
            try
            {

                string orderBy = "";
                string sql = "";
                //if (objAutoIncomeSalaried.Remarks == "") {
                //    objAutoIncomeSalaried.Remarks = null;
                //}

                if (objAutoIncomeSalaried.Id == 0)
                {
                    sql = string.Format(@"Insert into auto_income_salaried(AnalystMasterId,SalaryMode,Employer,IncomeAssId,GrosSalary,NetSalary,VariableSalary,VariableSalaryAmount,VariableSalaryPercent,CarAllownce,CarAllownceAmount,CarAllowncePer,OwnHouseBenifit,TotalIncome,Remarks,AccountType)
                                             values('" + objAutoIncomeSalaried.AnalystMasterId + "','" + objAutoIncomeSalaried.SalaryMode + "','" + objAutoIncomeSalaried.Employer + "','" + objAutoIncomeSalaried.IncomeAssId + "','" + objAutoIncomeSalaried.GrosSalary + "','" + objAutoIncomeSalaried.NetSalary + "'," + objAutoIncomeSalaried.VariableSalary + ",'" +
                                                       objAutoIncomeSalaried.VariableSalaryAmount + "','" + objAutoIncomeSalaried.VariableSalaryPercent + "'," +
                                                       objAutoIncomeSalaried.CarAllownce + ",'" + objAutoIncomeSalaried.CarAllownceAmount
                                                       + "','" + objAutoIncomeSalaried.CarAllowncePer + "'," + objAutoIncomeSalaried.OwnHouseBenifit + ",'" +
                                                       objAutoIncomeSalaried.TotalIncome + "','" + objAutoIncomeSalaried.Remarks + "','" +
                                                       objAutoIncomeSalaried.AccountType + "')");

                }
                else
                {
                    sql =
                       string.Format(@"Update auto_income_salaried  Set   AnalystMasterId='{0}',
                                        SalaryMode='{1}',
                                        Employer='{2}',
                                        IncomeAssId='{3}',
                                        GrosSalary='{4}',
                                        NetSalary='{5}',
                                        VariableSalary={6},
                                        VariableSalaryAmount='{7}',
                                        CarAllownce={8},
                                        CarAllownceAmount='{9}',
                                        CarAllowncePer='{10}',
                                        OwnHouseBenifit={11},
                                        TotalIncome='{12}',
                                        Remarks='{13}',
                                        AccountType='{14}',
                                        VariableSalaryPercent = {16},
                                        where  Id = '{15}'",
                                        objAutoIncomeSalaried.AnalystMasterId,
                                        objAutoIncomeSalaried.SalaryMode,
                                        objAutoIncomeSalaried.Employer,
                                        objAutoIncomeSalaried.IncomeAssId,
                                        objAutoIncomeSalaried.GrosSalary,
                                        objAutoIncomeSalaried.NetSalary,
                                        objAutoIncomeSalaried.VariableSalary,
                                        objAutoIncomeSalaried.VariableSalaryAmount,
                                        objAutoIncomeSalaried.CarAllownce,
                                        objAutoIncomeSalaried.CarAllownceAmount,
                                        objAutoIncomeSalaried.CarAllowncePer,
                                        objAutoIncomeSalaried.OwnHouseBenifit,
                                        objAutoIncomeSalaried.TotalIncome,
                                        objAutoIncomeSalaried.Remarks,
                                        objAutoIncomeSalaried.AccountType,
                                        objAutoIncomeSalaried.Id,
                                        objAutoIncomeSalaried.VariableSalaryPercent
                                        );
                }


                dbHelper.ExecuteNonQuery(sql);
                var selectSql = "Select ID from auto_income_salaried where AnalystMasterId = " + objAutoIncomeSalaried.AnalystMasterId + " and AccountType = " + objAutoIncomeSalaried.AccountType;
                
                IDataReader tempDataReder = dbHelper.GetDataReader(selectSql);
                while (tempDataReder.Read())
                {
                    salaryId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            return salaryId;
        }

        private void SaveAuto_VariableSalaryCalculation(Auto_VariableSalaryCalculation objAutoVariableSalaryCalculation, CommonDbHelper dbHelper)
        {
            
            try
            {
                string sql = "";

                if (objAutoVariableSalaryCalculation.Id == 0)
                {
                    sql = string.Format(@"Insert into auto_an_salariedvariablesalarymonth(AnalystMasterId,SalaiedID,Month,Amount)
                                           values('" +objAutoVariableSalaryCalculation.AnalystMasterId+"','"+ objAutoVariableSalaryCalculation.SalaiedID + "','" + objAutoVariableSalaryCalculation.Month + "','" + objAutoVariableSalaryCalculation.Amount + "')");

                }
                else
                {
                    sql =
                        string.Format(
                            @"Update auto_an_salariedvariablesalarymonth  Set AnalystMasterId={0},   
                                        AutoIncomeSalariedId='{1}',
                                        Month='{2}',
                                        Amount='{3}',
                                        where  Id = '{4}'",
                            objAutoVariableSalaryCalculation.AnalystMasterId,
                            objAutoVariableSalaryCalculation.SalaiedID,
                            objAutoVariableSalaryCalculation.Month,
                            objAutoVariableSalaryCalculation.Amount,
                            objAutoVariableSalaryCalculation.Id);
                }

                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            
        }

        #endregion

        #region Land Lord

        private void SaveAutoLandLord(AutoLandLord objAutoLandLord, CommonDbHelper dbHelper)
        {
            try
            {
                string sql = "";

                if (objAutoLandLord.Id == 0)
                {
                    sql =
                        string.Format(
                            @"Insert Into auto_landlord(AutoLoanMasterId,EmployerCatagoryId,SectorId,IncomeAssId,TotalRentalIncome,ReflectionRequired,
Percentage,AccountType,Remarks)values('" +
                            objAutoLandLord.AutoLoanMasterId + "','" + objAutoLandLord.EmployerCatagoryId + "','" + 
                            objAutoLandLord.SectorId + "','" +objAutoLandLord.IncomeAssId + "','" + objAutoLandLord.TotalRentalIncome + "','" +
                            objAutoLandLord.ReflectionRequired + "','" + objAutoLandLord.Percentage + "','" + objAutoLandLord.AccountType + "','" +
                            objAutoLandLord.Remarks + "')");
                }
                else
                {
                    sql =
                       string.Format(@"Update auto_landlord  Set AutoLoanMasterId='{0}',
                                        SectorId='{1}',
                                        IncomeAssId='{2}',
                                        TotalRentalIncome='{3}',
                                        ReflectionRequired='{4}',
                                        Percentage='{5}',
                                        Remarks='{6}',
                                        EmployerCatagoryId='{7}',
                                        AccountType='{8}',
                                        where  AutoLoanMasterId = '{9}'",
                                       objAutoLandLord.AutoLoanMasterId,
                                       objAutoLandLord.SectorId,
                                       objAutoLandLord.IncomeAssId,
                                       objAutoLandLord.TotalRentalIncome,
                                       objAutoLandLord.ReflectionRequired,
                                       objAutoLandLord.Percentage,
                                       objAutoLandLord.Remarks,
                                       objAutoLandLord.EmployerCatagoryId,
                                       objAutoLandLord.AccountType,
                                       objAutoLandLord.Id);
                }

                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            
        }
        private void SaveAutoLandLordVerifiedRent(Auto_LandLord_VerifiedRent objAutoLandLordVerifiedRent, CommonDbHelper dbHelper)
        {
            try
            {
                string sql = "";

                if (objAutoLandLordVerifiedRent.Id == 0)
                {
                    sql =
                         string.Format(@"Insert Into auto_landlord_verifiedrent(AutoLandLordId,Property,Share)
                                  values('" +
                                  objAutoLandLordVerifiedRent.AutoLandLordId + "','" +
                                  objAutoLandLordVerifiedRent.Property + "','" + objAutoLandLordVerifiedRent.Share + "')");
                }
                else
                {
                    sql =
                       string.Format(@"Update auto_landlord_verifiedrentSet AutoLandLordId='{0}',
                                        Property='{1}',
                                        Share='{2}',
                                        where  Id = '{3}'",
                                        objAutoLandLordVerifiedRent.AutoLandLordId,
                                        objAutoLandLordVerifiedRent.Property,
                                        objAutoLandLordVerifiedRent.Share,
                                        objAutoLandLordVerifiedRent.Id);
                }


                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
        }
        

        #endregion

        #region Bank Statement For BussinessMan and Self Employed

        private void SaveAutoAnalystIncomBankStatementSum(Auto_AnalystIncomBankStatementSum objAuto_AIBS, CommonDbHelper dbHelper)
        {
            try
            {

                string orderBy = "";
                string sql = "";

                if (objAuto_AIBS.Id == 0)
                {
                    sql =
                     string.Format(
                         @"INSERT INTO auto_anincombstatemetsum(AnalystMasterId,BankStatementId,TotalCTO12,AvgCTO12,AvgBalance12,TotalCTO6,AvgCTO6,AvgBalance6,ConsideredCTO,ConsideredBalance,ConsideredMargin,GrossIncome,NetAfterPayment,Share,NetIncome,IncomeAssessmentMethod,Industry,SubSector,PrivateIncome,AvgBalance1st9m,AvgBalance1st6m,AvgBalanceLast6m,AvgBalanceLast3m,Type, TabType,Remarks)
                                 VALUES (" + objAuto_AIBS.AnalystMasterId + "," + objAuto_AIBS.BankStatementId + "," + objAuto_AIBS.TotalCTO12 + "," + objAuto_AIBS.AvgCTO12 + "," + objAuto_AIBS.AvgBalance12 + "," + objAuto_AIBS.TotalCTO6 + "," + objAuto_AIBS.AvgCTO6 + "," + objAuto_AIBS.AvgBalance6 + "," + objAuto_AIBS.ConsideredCTO + "," + objAuto_AIBS.ConsideredBalance + "," + objAuto_AIBS.ConsideredMargin + "," 
                                           + objAuto_AIBS.GrossIncome + "," + objAuto_AIBS.NetAfterPayment + "," + objAuto_AIBS.Share + "," + 
                                           objAuto_AIBS.NetIncome + "," + objAuto_AIBS.IncomeAssessmentMethod + "," + objAuto_AIBS.Industry + "," + 
                                           objAuto_AIBS.SubSector + "," + objAuto_AIBS.PrivateIncome + "," + objAuto_AIBS.AvgBalance1st9m + "," + 
                                           objAuto_AIBS.AvgBalance1st6m + "," + objAuto_AIBS.AvgBalanceLast6m + "," + objAuto_AIBS.AvgBalanceLast3m
                                           + "," + objAuto_AIBS.Type + "," + objAuto_AIBS.TabType + ",'" + objAuto_AIBS.Remarks + "')");
                }
                else
                {
                    sql =
                       string.Format(@"Update auto_sub_repaymenthistoryoffscb_onscb  Set   AnalystMasterId='{0}',
                                        BankStatementId='{1}',
                                        TotalCTO12='{2}',
                                        AvgCTO12='{3}',
                                        AvgBalance12='{4}',
                                        TotalCTO6='{5}',
                                        AvgCTO6='{6}',
                                        AvgBalance6='{7}',
                                        ConsideredCTO='{8}',
                                        ConsideredBalance='{9}',
                                        ConsideredMargin='{10}',
                                        GrossIncome='{11}',
                                        NetAfterPayment='{12}',
                                        Share='{13}',
                                        NetIncome='{14}',
                                        IncomeAssessmentMethod='{15}',
                                        Industry='{16}',
                                        SubSector='{17}',
                                        AvgBalance1st9m='{18}',
                                        AvgBalance1st6m='{19}',
                                        AvgBalanceLast6m='{20}',
                                        AvgBalanceLast3m='{21}',
                                        Type='{22}',
                                        PrivateIncome={24},
                                        TabType={25},
                                        TabType={26}
                                        where  Id = '{23}'",
                                        objAuto_AIBS.AnalystMasterId,
                                        objAuto_AIBS.BankStatementId,
                                        objAuto_AIBS.TotalCTO12,
                                        objAuto_AIBS.AvgCTO12,
                                        objAuto_AIBS.AvgBalance12,
                                        objAuto_AIBS.TotalCTO6,
                                        objAuto_AIBS.AvgCTO6,
                                        objAuto_AIBS.AvgBalance6,
                                        objAuto_AIBS.ConsideredCTO,
                                        objAuto_AIBS.ConsideredBalance,
                                        objAuto_AIBS.ConsideredMargin,
                                        objAuto_AIBS.GrossIncome,
                                        objAuto_AIBS.NetAfterPayment,
                                        objAuto_AIBS.Share,
                                        objAuto_AIBS.NetIncome,
                                        objAuto_AIBS.IncomeAssessmentMethod,
                                        objAuto_AIBS.Industry,
                                        objAuto_AIBS.SubSector,
                                        objAuto_AIBS.AvgBalance1st9m,
                                        objAuto_AIBS.AvgBalance1st6m,
                                        objAuto_AIBS.AvgBalanceLast6m,
                                        objAuto_AIBS.AvgBalanceLast3m,
                                        objAuto_AIBS.Type,
                                        objAuto_AIBS.Id,
                                        objAuto_AIBS.PrivateIncome, objAuto_AIBS.TabType, objAuto_AIBS.Remarks);
                }








                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
        }

        private void SaveAutoAnOverDraftFacility(Auto_An_OverDraftFacility autoAnOverDraftFacility, CommonDbHelper objDbHelper)
        {
            var sql = "";
            try
            {

                sql =
                     string.Format(
                         @"INSERT INTO auto_an_overdraftfacility(IncomeSumId,AutoLoanMasterId,BankStatementId,BankId,LimitAmount,Interest,Consider, Type)
                                 VALUES (" + autoAnOverDraftFacility.IncomeSumId + "," + autoAnOverDraftFacility.AutoLoanMasterId + "," + autoAnOverDraftFacility.BankStatementId + "," + 
                                           autoAnOverDraftFacility.BankId + "," + autoAnOverDraftFacility.LimitAmount + "," +
                                           autoAnOverDraftFacility.Interest + "," + autoAnOverDraftFacility.Consider + "," + autoAnOverDraftFacility.Type + ")");
                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        private void updateBankStatementAveragetotalbyId(Auto_BankStatementAvgTotal auto_BankStatementAvgTotal, CommonDbHelper objDbHelper)
        {
            var sql = "";
            try
            {
                sql =
                    string.Format(
                        "update auto_bankstatementavgtotal set Adjustment = {1} where BankStatementAvgTotalId = {0}",
                        auto_BankStatementAvgTotal.BankStatementAvgTotalId, auto_BankStatementAvgTotal.Adjustment);
                
                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        private void SaveAutoPreviousRePaymentHistory(Auto_PreviousRePaymentHistory objAutoPreviousRePaymentHistory, CommonDbHelper dbHelper)
        {
            try
            {
                string sql = "";

                if (objAutoPreviousRePaymentHistory.Id == 0)
                {
                    sql =
                    string.Format(@"Insert Into auto_previous_repayment_history(IncomeSumId,AutoLoanMasterId,BankStatementId,BankId,OffScb_Total,OnScb_Total,TotalEMIClosingConsidered,Remarks)
                                    values('" +objAutoPreviousRePaymentHistory.IncomeSumId + "','"+ objAutoPreviousRePaymentHistory.AutoLoanMasterId + "','" + objAutoPreviousRePaymentHistory.BankStatementId + "','" + objAutoPreviousRePaymentHistory.BankId + "','" + objAutoPreviousRePaymentHistory.OffScb_Total + "','" + objAutoPreviousRePaymentHistory.OnScb_Total + "','" + objAutoPreviousRePaymentHistory.TotalEMIClosingConsidered + "','" + objAutoPreviousRePaymentHistory.Remarks + "')");
                }
                else
                {
                    sql =
                       string.Format(@"Update auto_previous_repayment_history    Set   AutoLoanMasterId='{0}',
                                        BankStatementId='{1}',
                                        BankId='{2}',
                                        OffScb_Total='{3}',
                                        OnScb_Total='{4}',
                                        TotalEMIClosingConsidered='{5}',
                                        Remarks='{6}',
                                        IncomeSumId = '{8}'
                                        where  Id = '{7}'",
                                        objAutoPreviousRePaymentHistory.AutoLoanMasterId,
                                        objAutoPreviousRePaymentHistory.BankStatementId,
                                        objAutoPreviousRePaymentHistory.BankId,
                                        objAutoPreviousRePaymentHistory.OffScb_Total,
                                        objAutoPreviousRePaymentHistory.OnScb_Total,
                                        objAutoPreviousRePaymentHistory.TotalEMIClosingConsidered,
                                        objAutoPreviousRePaymentHistory.Remarks,
                                        objAutoPreviousRePaymentHistory.Id, objAutoPreviousRePaymentHistory.IncomeSumId);
                }


                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            
        }

        private void SaveAutoSubPreviousRepaymentHistoryOffSCBOnSCB(Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB objAutoSubOffScbOnScb, CommonDbHelper dbHelper)
        {
            try
            {
                string sql = "";

                if (objAutoSubOffScbOnScb.Id == 0)
                {
                    sql =
                    string.Format(@"Insert Into auto_sub_repaymenthistoryoffscb_onscb(PreviousRepaymentHistoryId,AutoLoanMasterId,BankId,Type,EMI,Consider)
values('" + objAutoSubOffScbOnScb.PreviousRepaymentHistoryId + "','" + objAutoSubOffScbOnScb.AutoLoanMasterId + "','" + objAutoSubOffScbOnScb.BankId + "','" + objAutoSubOffScbOnScb.Type + "','" + objAutoSubOffScbOnScb.EMI + "'," + objAutoSubOffScbOnScb.Consider + ")");

                }
                else
                {
                    sql =
                       string.Format(@"Update auto_sub_repaymenthistoryoffscb_onscb  Set   PreviousRepaymentHistoryId='{0}',AutoLoanMasterId={6},
                                        BankId='{1}',
                                        Type='{2}',
                                        EMI='{3}',
                                        Consider={4},
                                        where  Id = '{5}'",
                                        objAutoSubOffScbOnScb.PreviousRepaymentHistoryId,
                                        objAutoSubOffScbOnScb.BankId,
                                        objAutoSubOffScbOnScb.Type,
                                        objAutoSubOffScbOnScb.EMI,
                                        objAutoSubOffScbOnScb.Consider,
                                        objAutoSubOffScbOnScb.Id,
                                        objAutoSubOffScbOnScb.AutoLoanMasterId
                                        );
                }

                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            
        }

        private void SaveAutoPractingDoctor(Auto_PracticingDoctor objAutoPracticingDoctor, CommonDbHelper dbHelper)
        {
            try
            {
                string sql = "";

                if (objAutoPracticingDoctor.Id == 0)
                {
                    sql = string.Format(@"Insert Into auto_practicing_doctor(IncomeSumId,AutoLoanMasterId,BankStatementId,Type,TotalChamberIncome,OtherOTIncome,TotalIncome)
                            values('" + objAutoPracticingDoctor.IncomeSumId + "','" + objAutoPracticingDoctor.AutoLoanMasterId + "','" + objAutoPracticingDoctor.BankStatementId + "','" + objAutoPracticingDoctor.Type + "','" + objAutoPracticingDoctor.TotalChamberIncome + "','" + objAutoPracticingDoctor.OtherOTIncome + "','" + objAutoPracticingDoctor.TotalIncome + "')");

                }
                else
                {
                    sql =
                       string.Format(@"Update auto_practicing_doctor Set AutoLoanMasterId='{0}',
                                        BankStatementId='{1}',
                                        Type='{2}',
                                        TotalChamberIncome='{3}',
                                        OtherOTIncome='{4}',
                                        TotalIncome='{5}',
                                        IncomeSumId = '{7}'
                                        where  Id = '{6}'",
                                        objAutoPracticingDoctor.AutoLoanMasterId,
                                        objAutoPracticingDoctor.BankStatementId,
                                        objAutoPracticingDoctor.Type,
                                        objAutoPracticingDoctor.TotalChamberIncome,
                                        objAutoPracticingDoctor.OtherOTIncome,
                                        objAutoPracticingDoctor.TotalIncome,
                                        objAutoPracticingDoctor.Id, objAutoPracticingDoctor.IncomeSumId);
                }
                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
        }

        private void SaveAutoChamberIncome(Auto_ChamberIncome objAutoChamberIncome, CommonDbHelper dbHelper)
        {
            try
            {
                string sql = "";

                if (objAutoChamberIncome.Id == 0)
                {
                    sql = string.Format(@"Insert Into auto_chamber_income(AutoPracticingDoctorId,AutoLoanMasterId,ChamberId,OldPatents,OldFee,OldTotal,NewPatents,NewFee,NewTotal,Total,DayMonth,Income)
                                    values('" + objAutoChamberIncome.AutoPracticingDoctorId + "','" + objAutoChamberIncome.AutoLoanMasterId + "','" + objAutoChamberIncome.ChamberId + "','" + objAutoChamberIncome.OldPatents + "','" + objAutoChamberIncome.OldFee + "','" + objAutoChamberIncome.OldTotal + "','" + objAutoChamberIncome.NewPatents + "','" + objAutoChamberIncome.NewFee + "','" + objAutoChamberIncome.NewTotal + "','" + objAutoChamberIncome.Total + "','" + objAutoChamberIncome.DayMonth + "','" + objAutoChamberIncome.Income + "')");

                }
                else
                {
                    sql =
                       string.Format(@"Update auto_chamber_incomeSet AutoPracticingDoctorId='{0}',AutoLoanMasterId={12},
                                        ChamberId='{1}',
                                        OldPatents='{2}',
                                        OldFee='{3}',
                                        OldTotal='{4}',
                                        NewPatents='{5}',
                                        NewFee='{6}',
                                        NewTotal='{7}',
                                        Total='{8}',
                                        DayMonth='{9}',
                                        Income='{10}',
                                        where  Id = '{11}'",
                                        objAutoChamberIncome.AutoPracticingDoctorId,
                                        objAutoChamberIncome.ChamberId,
                                        objAutoChamberIncome.OldPatents,
                                        objAutoChamberIncome.OldFee,
                                        objAutoChamberIncome.OldTotal,
                                        objAutoChamberIncome.NewPatents,
                                        objAutoChamberIncome.NewFee,
                                        objAutoChamberIncome.NewTotal,
                                        objAutoChamberIncome.Total,
                                        objAutoChamberIncome.DayMonth,
                                        objAutoChamberIncome.Income,
                                        objAutoChamberIncome.Id,
                                        objAutoChamberIncome.AutoLoanMasterId
                                        );
                }



                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            
        }

        private void SaveAutoTutioningTeacher(Auto_TutioningTeacher objAutoTutioningTeacher, CommonDbHelper dbHelper)
        {
            try
            {

                string orderBy = "";
                string sql = "";

                if (objAutoTutioningTeacher.Id == 0)
                {
                    sql = string.Format(@"Insert Into auto_tutioning_teacher(IncomeSumId,AutoLoanMasterId,BankStatementId,Type,NoOfStudent,NoOfDaysForTution,Fees,TotalIncome)
                                    values('" + objAutoTutioningTeacher.IncomeSumId + "','" + objAutoTutioningTeacher.AutoLoanMasterId + "','" + objAutoTutioningTeacher.BankStatementId + "','" + objAutoTutioningTeacher.Type + "','" + objAutoTutioningTeacher.NoOfStudent + "','" + objAutoTutioningTeacher.NoOfDaysForTution + "','" + objAutoTutioningTeacher.Fees + "','" + objAutoTutioningTeacher.TotalIncome + "')");

                }
                else
                {
                    sql =
                       string.Format(@"Update auto_tutioning_teacher Set AutoLoanMasterId='{0}',
                                        BankStatementId='{1}',
                                        Type='{2}',
                                        NoOfStudent='{3}',
                                        NoOfDaysForTution='{4}',
                                        Fees='{5}',
                                        TotalIncome='{6}',
                                        TotalIncome='{8}',
                                        whereId='{7}'",

                                        objAutoTutioningTeacher.AutoLoanMasterId,
                                        objAutoTutioningTeacher.BankStatementId,
                                        objAutoTutioningTeacher.Type,
                                        objAutoTutioningTeacher.NoOfStudent,
                                        objAutoTutioningTeacher.NoOfDaysForTution,
                                        objAutoTutioningTeacher.Fees,
                                        objAutoTutioningTeacher.TotalIncome,
                                        objAutoTutioningTeacher.Id,objAutoTutioningTeacher.IncomeSumId
                                        );
                }






                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
        }

        #endregion
        
        #endregion

        #region Save General tab

        


        private int SaveAutoLoanAnalystMaster(Auto_LoanAnalystMaster autoAnalistMaster, CommonDbHelper objDbHelper)
        {
            //var dbHelper = new CommonDbHelper();
            int analystMasterId = 0;
            string sql = "";

            try
            {
                if (autoAnalistMaster.AnalystMasterId > 0)
                {
                    sql =
                         string.Format(
                             @"Update auto_loananalystmaster set
                                                LlId = {0},AutoLoanMasterId={1},IsJoint={2},ReasonOfJoint='{3}',Colleterization={5},
ColleterizationWith={6}, CibInfoId={7} where AnalystMasterId = {4}",
                             autoAnalistMaster.LlId, autoAnalistMaster.AutoLoanMasterId, autoAnalistMaster.IsJoint,
                             autoAnalistMaster.ReasonOfJoint, autoAnalistMaster.AnalystMasterId,
                             autoAnalistMaster.Colleterization, autoAnalistMaster.ColleterizationWith, autoAnalistMaster.CibInfoId);

                    objDbHelper.ExecuteNonQuery(sql);
                    analystMasterId = autoAnalistMaster.AnalystMasterId;
                }
                else
                {
                    sql = string.Format(
                        @"INSERT INTO auto_loananalystmaster(LlId,AutoLoanMasterId,IsJoint,ReasonOfJoint,Colleterization,ColleterizationWith, CibInfoId)
                              VALUES (" +
                        autoAnalistMaster.LlId + "," + autoAnalistMaster.AutoLoanMasterId + "," +
                        autoAnalistMaster.IsJoint + ",'" + autoAnalistMaster.ReasonOfJoint + "'," + autoAnalistMaster.Colleterization + "," + autoAnalistMaster.ColleterizationWith + "," + autoAnalistMaster.CibInfoId + ")");

                    objDbHelper.ExecuteNonQuery(sql);

                    sql =
                        string.Format(
                            @"Select Max(AnalystMasterId) from auto_loananalystmaster where 
                                    LlId = {0} and AutoLoanMasterId={1} and IsJoint={2} and ReasonOfJoint='{3}'",
                            autoAnalistMaster.LlId, autoAnalistMaster.AutoLoanMasterId, autoAnalistMaster.IsJoint,
                            autoAnalistMaster.ReasonOfJoint);

                    IDataReader reader = objDbHelper.GetDataReader(sql);
                    while (reader.Read())
                    {
                        analystMasterId = reader.GetInt32(0);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }

            return analystMasterId;
        }

        private void SaveAutoLoanAnalystApplication(Auto_LoanAnalystApplication objAutoLoanAnalystApplication, CommonDbHelper dbHelper)
        {
            //string returnValue = "";
            string sql = "";
            try
            {
                if (objAutoLoanAnalystApplication.Id > 0)
                {
                    sql =
                        string.Format(
                            @"update auto_loananalystapplication set 
                            AnalystMasterId = {0},
                            Source = {1},
                            BranchId = {2},
                            BranchCode = '{3}',
                            ValuePack = {4},
                            BorrowingRelationShip = '{5}',
                            BorrowingRelationShipCode = '{6}',
                            CustomerStatus = {7},
                            AskingRepaymentMethod = {8},
                            ARTA = {9},
                            ARTAStatus = {10},
                            GeneralInsurance = {11},
                            CashSecured100Per = {12},
                            CCBundle = {13},
                            SecurityValue = {15},
                            GeneralInsurancePreference = {16}
                            where Id = {14}",
                            objAutoLoanAnalystApplication.AnalystMasterId, objAutoLoanAnalystApplication.Source,
                            objAutoLoanAnalystApplication.BranchId, objAutoLoanAnalystApplication.BranchCode,
                            objAutoLoanAnalystApplication.ValuePack, objAutoLoanAnalystApplication.BorrowingRelationShip,
                            objAutoLoanAnalystApplication.BorrowingRelationShipCode,
                            objAutoLoanAnalystApplication.CustomerStatus,
                            objAutoLoanAnalystApplication.AskingRepaymentMethod, objAutoLoanAnalystApplication.ARTA,
                            objAutoLoanAnalystApplication.ARTAStatus, objAutoLoanAnalystApplication.GeneralInsurance,
                            objAutoLoanAnalystApplication.CashSecured100Per, objAutoLoanAnalystApplication.CCBundle,
                            objAutoLoanAnalystApplication.Id, objAutoLoanAnalystApplication.SecurityValue, objAutoLoanAnalystApplication.GeneralInsurancePreference);
                }
                else
                {
                    sql =
                        string.Format(
                            @"INSERT INTO auto_loananalystapplication(AnalystMasterId,Source,BranchId,BranchCode,ValuePack,BorrowingRelationShip,BorrowingRelationShipCode,CustomerStatus,AskingRepaymentMethod,ARTA,ARTAStatus,GeneralInsurance,CashSecured100Per,CCBundle,SecurityValue,GeneralInsurancePreference)
                          VALUES (" +
                            objAutoLoanAnalystApplication.AnalystMasterId + "," + objAutoLoanAnalystApplication.Source +
                            "," +
                            objAutoLoanAnalystApplication.BranchId + ",'" + objAutoLoanAnalystApplication.BranchCode +
                            "'," +
                            objAutoLoanAnalystApplication.ValuePack + ",'" +
                            objAutoLoanAnalystApplication.BorrowingRelationShip + "','" +
                            objAutoLoanAnalystApplication.BorrowingRelationShipCode + "'," +
                            objAutoLoanAnalystApplication.CustomerStatus + "," +
                            objAutoLoanAnalystApplication.AskingRepaymentMethod + "," +
                            objAutoLoanAnalystApplication.ARTA +
                            "," + objAutoLoanAnalystApplication.ARTAStatus + "," +
                            objAutoLoanAnalystApplication.GeneralInsurance + "," +
                            objAutoLoanAnalystApplication.CashSecured100Per + "," +
                            objAutoLoanAnalystApplication.CCBundle + "," +
                            objAutoLoanAnalystApplication.SecurityValue + "," +
                            objAutoLoanAnalystApplication.GeneralInsurancePreference +
                            ")");

                }
                dbHelper.ExecuteNonQuery(sql);
                //returnValue = "Success";
            }
            catch (Exception ex)
            {
                //returnValue = "Failed to save AutoLoan Analyst Application";
                dbHelper.RollBack();
            }

            //return returnValue;
        }

        private void SaveAutoLoanAnalystVehicleDetails(Auto_LoanAnalystVehicleDetail objAutoAnVehicleDetail, CommonDbHelper dbHelper)
        {
            string sql = "";
            try
            {
                if (objAutoAnVehicleDetail.Id > 0)
                {
                    sql = string.Format(@"update auto_loananalystvehicledetail set 
                                            AnalystMasterId = {0},
                                            valuePackAllowed = {1},
                                            SeatingCapacity = {2},
                                            ConsideredPrice = {3},
                                            PriceConsideredOn = {4},
                                            CarVarification = {5},
                                            DeliveryStatus = {6}
                                            where Id = {7}",
                        objAutoAnVehicleDetail.AnalystMasterId, objAutoAnVehicleDetail.valuePackAllowed, objAutoAnVehicleDetail.SeatingCapacity,
                    objAutoAnVehicleDetail.ConsideredPrice, objAutoAnVehicleDetail.PriceConsideredOn, objAutoAnVehicleDetail.CarVarification,
                    objAutoAnVehicleDetail.DeliveryStatus, objAutoAnVehicleDetail.Id);
                }
                else
                {
                    sql =
                        string.Format(
                            @"INSERT INTO auto_loananalystvehicledetail(AnalystMasterId,valuePackAllowed,SeatingCapacity,ConsideredPrice,PriceConsideredOn,CarVarification,DeliveryStatus)
                        VALUES (" +
                            objAutoAnVehicleDetail.AnalystMasterId + "," + objAutoAnVehicleDetail.valuePackAllowed + "," +
                            objAutoAnVehicleDetail.SeatingCapacity + "," + objAutoAnVehicleDetail.ConsideredPrice + "," +
                            objAutoAnVehicleDetail.PriceConsideredOn + "," + objAutoAnVehicleDetail.CarVarification +
                            "," +
                            objAutoAnVehicleDetail.DeliveryStatus +
                            ")");

                }

                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }


        }

        private void SaveAutoApplicantSCBAccount(AutoApplicantSCBAccount scbAccount, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql =
                    string.Format(
                        @"insert into auto_applicantscbaccount 
	                                    (AutoLoanMasterId, LLID, AccountNumberSCB, 
	                                    AccountStatus, PriorityHolder)
	                                    values
	                                    ({0}, {1}, '{2}', {3}, 
	                                    {4})",
                        scbAccount.AutoLoanMasterId, scbAccount.LLID, scbAccount.AccountNumberSCB, scbAccount.AccountStatus,
                        scbAccount.PriorityHolder);

                objDbHelper.ExecuteNonQuery(sql);
            }
            catch {
                objDbHelper.RollBack();
            }

        }

        private void SaveAutoPRAccountDetail(AutoPRAccountDetail pra, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql =
                    string.Format(
                        @"insert into auto_praccount_details 
	                            (AutoLoanMasterId, BankId, BranchId, 
	                            AccountNumber, AccountCategory, AccountType)
	                            values
	                            ({0}, {1}, {2}, 
	                            '{3}', '{4}', '{5}')",
                        pra.AutoLoanMasterId, pra.BankId, pra.BranchId, pra.AccountNumber, pra.AccountCategory,
                        pra.AccountType);

                objDbHelper.ExecuteNonQuery(sql);
            }
            catch {
                objDbHelper.RollBack();
            }
        }

        private void SaveAutoLoanFacility(AutoLoanFacility af, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql =
                    string.Format(
                        @"insert into auto_loanfacility 
	                                    (AutoLoanMasterId, FacilityType, InterestRate, 
	                                    PresentBalance, PresentEMI, PresentLimit, ProposedLimit, 
	                                    RepaymentAgrement, Consider, EMIPercent, EMIShare, Status
	                                    )
	                                    values
	                                    ({0}, {1}, {2}, 
	                                    {3}, {4}, {5}, {6}, 
	                                    {7}, {8}, {9}, {10}, {11})",
                        af.AutoLoanMasterId, af.FacilityType, af.InterestRate, af.PresentBalance, af.PresentEMI,
                        af.PresentLimit, af.ProposedLimit,
                        af.RepaymentAgrement, af.Consider, af.EMIPercent, af.EMIShare, af.Status);

                objDbHelper.ExecuteNonQuery(sql);
            }
            catch {
                objDbHelper.RollBack();
            }
        }

        private int SaveAutoLoanFacilityWithReturn(AutoLoanFacility objAutoLoanFacilityListMember, CommonDbHelper objDbHelper)
        {
            int facilityId = 0;
            string sql = "";
            try
            {
                //                if (objAutoLoanFacilityListMember.FacilityId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_loanfacility SET
                //	                AutoLoanMasterId = {0},FacilityType = {1},InterestRate = {2},PresentBalance = {3},
                //	                PresentEMI = {4},PresentLimit = {5}, ProposedLimit = {6},RepaymentAgrement = {7} where FacilityId = {8}", 
                //                    objAutoLoanFacilityListMember.AutoLoanMasterId,
                //                    objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                //                    objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                //                    objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.FacilityId);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql = String.Format(@"INSERT INTO auto_loanfacility
	                (AutoLoanMasterId, FacilityType, InterestRate, PresentBalance, 
	                PresentEMI, PresentLimit, ProposedLimit, RepaymentAgrement,EMIPercent,EMIShare,Status)
                    values
                    ({0}, {1}, {2}, 
                    {3}, {4}, {5}, {6}, {7},{8},{9},{10})", objAutoLoanFacilityListMember.AutoLoanMasterId,
                objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.EMIPercent, objAutoLoanFacilityListMember.EMIShare,
                objAutoLoanFacilityListMember.Status);
                objDbHelper.ExecuteNonQuery(sql);

                var selectQuary = string.Format(@"select FacilityId from auto_loanfacility where AutoLoanMasterId={0} and FacilityType={1} and 
                    InterestRate='{2}' and PresentBalance='{3}' and PresentEMI='{4}' and PresentLimit='{5}' and ProposedLimit='{6}' 
                    and RepaymentAgrement={7} and EMIPercent={8}", objAutoLoanFacilityListMember.AutoLoanMasterId, objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.EMIPercent);

                IDataReader reader = objDbHelper.GetDataReader(selectQuary);
                while (reader.Read())
                {
                    facilityId = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return facilityId;
        }

        private void SaveAutoLoanSecurity(AutoLoanSecurity objAutoLoanSecurityMember, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var issueDate = objAutoLoanSecurityMember.IssueDate.ToString("yyyy-MM-dd");
            try
            {

                sql =
                    String.Format(
                        @"INSERT INTO auto_loansecurity
	                (AutoLoanMasterId, NatureOfSecurity, IssuingOffice, 
	                FaceValue, XTVRate, IssueDate, InterestRate,FacilityType, Status, FacilityId)
                    values
                    ({0}, {1}, '{2}', 
                    {3}, {4}, '{5}', '{6}',{7},{8},{9})",
                        objAutoLoanSecurityMember.AutoLoanMasterId,
                        objAutoLoanSecurityMember.NatureOfSecurity, objAutoLoanSecurityMember.IssuingOffice,
                        objAutoLoanSecurityMember.FaceValue, objAutoLoanSecurityMember.XTVRate, issueDate,
                        objAutoLoanSecurityMember.InterestRate, objAutoLoanSecurityMember.FacilityType, objAutoLoanSecurityMember.Status, objAutoLoanSecurityMember.FacilityId);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoLoanFacilityOffSCB(AutoLoanFacilityOffSCB afscb, CommonDbHelper objDbHelper)
        {
            //string theDate = al.IssueDate.Date.ToString("yyyy-MM-dd");
            try
            {
                string sql =
                    string.Format(
                        @"insert into auto_loanfacilityoffscb 
                                (AnalystMasterId, AutoLoanMasterId, BankID, 
                                OriginalLimit, EMI, EMIPercent, EMIShare, Status, Consider, 
                                Type, InterestRate)
                                values
                                ({0}, {1}, {2}, 
                                {3}, {4}, {5}, {6}, {7}, {8}, 
                                {9}, {10})",
                        afscb.AnalystMasterId, afscb.AutoLoanMasterId, afscb.BankID, afscb.OriginalLimit, afscb.EMI,
                        afscb.EMIPercent, afscb.EMIShare,
                        afscb.Status, afscb.Consider, afscb.Type, afscb.InterestRate);

                objDbHelper.ExecuteNonQuery(sql);
            }
            catch {
                objDbHelper.RollBack();
            }
        }


        #endregion General Tab

        #region Save Loan Calculation Tab

        //auto_an_in_seg_applicant_income
        private void SaveAuto_An_IncomeSegmentApplicantIncome(Auto_An_IncomeSegmentApplicantIncome ai, CommonDbHelper objDbHelper)
        {
            try
            {
                //string sql = "delete from auto_an_in_seg_applicant_income where AnalystMasterId = " + ai.AnalystMasterId;
                //objDbHelper.ExecuteNonQuery(sql);

                string sql = string.Format(@"insert into auto_an_in_seg_applicant_income 
                                    (AnalystMasterId, CalculationSource, Income, IncomeAssesmentMethod, 
                                    IncomeAssesmentCode, EmployerCategory, EmployerCategoryCode, 
                                    Consider, ApplicantType)
                                    values
                                    ({0}, {1}, {2}, {3}, 
                                    '{4}', {5}, '{6}', 
                                    {7}, {8})", ai.AnalystMasterId, ai.CalculationSource, ai.Income, ai.IncomeAssesmentMethod,
                                              ai.IncomeAssesmentCode, ai.EmployerCategory, ai.EmployerCategoryCode,
                                              ai.Consider, ai.ApplicantType);
                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_in_seg_income
        private void SaveAuto_An_IncomeSegmentIncome(Auto_An_IncomeSegmentIncome ii, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_in_seg_income where AnalystMasterId = " + ii.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_in_seg_income 
	                                (AnalystMasterId, TotalIncome, DeclaredIncome, AppropriateIncome, 
	                                EmployeeSegment, EmployeeSegmentCode, AssesmentMethod, AssesmentMethodCode, 
	                                Category, CategoryCode, TotalJointIncome)
	                                values
	                                ({0}, {1}, {2}, {3}, 
	                                {4}, '{5}', {6}, '{7}', 
	                                {8}, '{9}', {10})"
                                    , ii.AnalystMasterId, ii.TotalIncome, ii.DeclaredIncome, ii.AppropriateIncome,
                                    ii.EmployeeSegment, ii.EmployeeSegmentCode, ii.AssesmentMethod, ii.AssesmentMethodCode,
                                    ii.Category, ii.CategoryCode, ii.TotalJointIncome);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_repaymentcapability
        private void SaveAuto_An_RepaymentCapability(Auto_An_RepaymentCapability rc, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_repaymentcapability where AnalystMasterId = " + rc.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_repaymentcapability 
	                                (AnalystMasterId, DBRLevel, AppropriateDBR, ConsideredDBR, 
	                                MaxEMICapability, ExistingEMI, CurrentEMICapability, BalanceSupported, 
	                                MaxEMI)
	                                values
	                                ({0}, {1}, {2}, {3}, 
	                                {4}, {5}, {6}, {7}, 
	                                {8})",
                                    rc.AnalystMasterId, rc.DBRLevel, rc.AppropriateDBR, rc.ConsideredDBR,
                                    rc.MaxEMICapability, rc.ExistingEMI, rc.CurrentEMICapability, rc.BalanceSupported,
                                    rc.MaxEMI);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();

            }
        }

        //auto_an_loancalculationtenor
        private void SaveAuto_An_LoanCalculationTenor(Auto_An_LoanCalculationTenor t, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_loancalculationtenor where AnalystMasterId = " + t.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_loancalculationtenor 
	                                    (AnalystMasterId, ARTAAllowed, VehicleAllowed, AgeAllowed, 
	                                    AskingTenor, AppropriateTenor, ConsideredTenor, ConsideredInMonth, 
	                                    Level)
	                                    values
	                                    ({0}, {1}, {2}, {3}, 
	                                    {4}, {5}, {6}, {7}, 
	                                    {8})",
                                        t.AnalystMasterId, t.ARTAAllowed, t.VehicleAllowed, t.AgeAllowed,
                                        t.AskingTenor, t.AppropriateTenor, t.ConsideredTenor, t.ConsideredInMonth,
                                        t.Level);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_loancalculationinterest
        private void SaveAuto_An_LoanCalculationInterest(Auto_An_LoanCalculationInterest i, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_loancalculationinterest where AnalystMasterId = " + i.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_loancalculationinterest 
	                                (AnalystMasterId, VendorAllowed, ARTAAllowed, SegmentAllowed, 
	                                AskingRate, AppropriateRate, ConsideredRate)
	                                values
	                                ({0}, {1}, {2}, {3}, 
	                                {4}, {5}, {6})",
                                    i.AnalystMasterId, i.VendorAllowed, i.ARTAAllowed, i.SegmentAllowed,
                                    i.AskingRate, i.AppropriateRate, i.ConsideredRate);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_loancalculationamount
        private void SaveAuto_An_LoanCalculationAmount(Auto_An_LoanCalculationAmount a, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_loancalculationamount where AnalystMasterId = " + a.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_loancalculationamount 
                                    (AnalystMasterId, LTVAllowed, IncomeAllowed, BB_CADAllowed, 
                                    AskingLoan, ExistingFacilityAllowed, AppropriateLoan, ApprovedLoan
                                    )
                                    values
                                    ({0}, {1}, {2}, {3}, 
                                    {4}, {5}, {6}, {7}
                                    )",
                                    a.AnalystMasterId, a.LTVAllowed, a.IncomeAllowed, a.BB_CADAllowed,
                                    a.AskingLoan, a.ExistingFacilityAllowed, a.AppropriateLoan, a.ApprovedLoan);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_loancalculationinsurance
        private void SaveAuto_An_LoanCalculationInsurance(Auto_An_LoanCalculationInsurance i, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_loancalculationinsurance where AnalystMasterId = " + i.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_loancalculationinsurance 
	                                (AnalystMasterId, GeneralInsurance, ARTA, TotalInsurance)
	                                values
	                                ({0}, {1}, {2}, {3}
	                                )",
                                    i.AnalystMasterId, i.GeneralInsurance, i.ARTA, i.TotalInsurance);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_loancalculationtotal
        private void SaveAuto_An_LoanCalculationTotal(Auto_An_LoanCalculationTotal t, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_loancalculationtotal where AnalystMasterId = " + t.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_loancalculationtotal 
	                                (AnalystMasterId, TotalLoanAmount, LTVExcludingInsurance, 
	                                FBRExcludingInsurance, LTVIncludingInsurance, FBRIncludingInsurance, 
	                                EMR, ExtraLTV, ExtraDBR)
	                                values
	                                ({0}, {1}, {2}, 
	                                {3}, {4}, {5}, 
	                                {6}, {7}, {8})",
                                     t.AnalystMasterId, t.TotalLoanAmount, t.LTVExcludingInsurance,
                                     t.FBRExcludingInsurance, t.LTVIncludingInsurance, t.FBRIncludingInsurance,
                                     t.EMR, t.ExtraLTV, t.ExtraDBR);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }




        #endregion Loan calculation

        #region Save Finalization Tab

        //auto_an_finalizationdeviation
        private void SaveAuto_An_FinalizationDeviation(Auto_An_FinalizationDeviation d, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "";

                sql = string.Format(@"insert into auto_an_finalizationdeviation 
	                                (AnalystMasterId, Level, Description, Code)
	                                values
	                                ({0}, {1}, {2}, '{3}')",
                                     d.AnalystMasterId, d.Level, d.Description, d.Code);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationdevfound
        private void SaveAuto_An_FinalizationDevFound(Auto_An_FinalizationDevFound df, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_finalizationdevfound where AnalystMasterId = " + df.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_finalizationdevfound 
	                                (AnalystMasterId, Age, DBR, SeatingCapacilty, SharePortion
	                                )
	                                values
	                                ({0}, '{1}', '{2}', '{3}', '{4}'
	                                )",
                                    df.AnalystMasterId, df.Age, df.DBR, df.SeatingCapacilty, df.SharePortion);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationsterength
        private void SaveAuto_An_FinalizationSterength(Auto_An_FinalizationSterength s, CommonDbHelper objDbHelper)
        {
            try
            {
                //string sql = "delete from auto_an_finalizationsterength where AnalystMasterId = " + df.AnalystMasterId;
                //objDbHelper.ExecuteNonQuery(sql);

                string sql = string.Format(@"insert into auto_an_finalizationsterength 
	                                        (AnalystMasterId, Strength)
	                                        values
	                                        ({0}, '{1}')", s.AnalystMasterId, s.Strength);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationweakness
        private void SaveAuto_An_FinalizationWeakness(Auto_An_FinalizationWeakness w, CommonDbHelper objDbHelper)
        {
            try
            {
                //string sql = "delete from auto_an_finalizationsterength where AnalystMasterId = " + df.AnalystMasterId;
                //objDbHelper.ExecuteNonQuery(sql);

                string sql = string.Format(@"insert into auto_an_finalizationweakness 
	                                        (AnalystMasterId, Weakness)
	                                        values
	                                        ({0}, '{1}')", w.AnalystMasterId, w.Weakness);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationrepaymentmethod
        private void SaveAuto_An_FinalizationRepaymentMethod(Auto_An_FinalizationRepaymentMethod rm, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_finalizationrepaymentmethod where AnalystMasterId = " + rm.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_finalizationrepaymentmethod 
	                                (AnalystMasterId, Asking, Instrument, SecurityPDC, PDCAmount, 
	                                BankID, SCBAccount, NoOfPDC, AccountNumber)
	                                values
	                                ({0}, '{1}', {2}, {3}, {4}, 
	                                {5}, '{6}', {7}, '{8}')",
                                    rm.AnalystMasterId, rm.Asking, rm.Instrument, rm.SecurityPDC, rm.PDCAmount,
                                    rm.BankID, rm.SCBAccount, rm.NoOfPDC, rm.AccountNumber);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationcondition
        private void SaveAuto_An_FinalizationCondition(Auto_An_FinalizationCondition c, CommonDbHelper objDbHelper)
        {
            try
            {
                //string sql = "delete from auto_an_finalizationrepaymentmethod where AnalystMasterId = " + rm.AnalystMasterId;
                //objDbHelper.ExecuteNonQuery(sql);

                string sql = string.Format(@"insert into auto_an_finalizationcondition 
	                                        (AnalystMasterId, Conditions)
	                                        values
	                                        ({0}, '{1}')", c.AnalystMasterId, c.Condition);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationremark
        private void SaveAuto_An_FinalizationRemark(Auto_An_FinalizationRemark r, CommonDbHelper objDbHelper)
        {
            try
            {
                //string sql = "delete from auto_an_finalizationrepaymentmethod where AnalystMasterId = " + rm.AnalystMasterId;
                //objDbHelper.ExecuteNonQuery(sql);

                var remark = r.Remarks.Replace("'", "\'");

                string sql = string.Format(@"insert into auto_an_finalizationremark 
	                                        (AnalystMasterId, Remarks)
	                                        values
	                                        ({0}, '{1}')", r.AnalystMasterId, remark);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationreqdoc
        private void SaveAuto_An_FinalizationReqDoc(Auto_An_FinalizationReqDoc rd, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_finalizationreqdoc where AnalystMasterId = " + rd.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_finalizationreqdoc 
	                                (AnalystMasterId, LetterOfIntroduction, Payslips, TradeLicence, 
	                                MOA_AOA, PartnershipDeed, FormX, BoardResolution, DiplomaCertificate, 
	                                ProfessionalCertificate, MunicipalTaxRecipt, TitleDeed, 
	                                RentalDeed, UtilityBill, LoanOfferLatter)
	                                values
	                                ({0}, {1}, {2}, {3}, 
	                                {4}, {5}, {6}, {7}, {8}, 
	                                {9}, {10}, {11}, 
	                                {12}, {13}, {14})",
                                    rd.AnalystMasterId, rd.LetterOfIntroduction, rd.Payslips, rd.TradeLicence,
                                    rd.MOA_AOA, rd.PartnershipDeed, rd.FormX, rd.BoardResolution, rd.DiplomaCertificate,
                                    rd.ProfessionalCertificate, rd.MunicipalTaxRecipt, rd.TitleDeed,
                                    rd.RentalDeed, rd.UtilityBill, rd.LoanOfferLatter);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationvarificationreport
        private void SaveAuto_An_FinalizationVarificationReport(Auto_An_FinalizationVarificationReport vr, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_finalizationvarificationreport where AnalystMasterId = " + vr.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_finalizationvarificationreport 
	                                (AnalystMasterId, CPVReport, CarPriceVarificationReport, 
	                                StatementAuthentication, CreditReport, CarVerificationReport, 
	                                UsedCarValuationReport, UsedCarDocs, CardRepaymentHistory
	                                )
	                                values
	                                ({0}, {1}, {2}, 
	                                {3}, {4}, {5}, 
	                                {6}, {7}, {8}
	                                )",
                                    vr.AnalystMasterId, vr.CPVReport, vr.CarPriceVarificationReport,
                                    vr.StatementAuthentication, vr.CreditReport, vr.CarVerificationReport,
                                    vr.UsedCarValuationReport, vr.UsedCarDocs, vr.CardRepaymentHistory);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_finalizationappraiserandapprover
        private void SaveAuto_An_FinalizationAppraiserAndApprover(Auto_An_FinalizationAppraiserAndApprover ar, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_finalizationappraiserandapprover where AnalystMasterId = " + ar.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_finalizationappraiserandapprover 
	                                (AnalystMasterId, AppraisedByUserID, AppraisedByUserCode, 
	                                SupportedByUserID, SupportedByUserCode, ApprovedByUserID, 
	                                ApprovedByUserCode, AutoDLALimit, TotalSecuredLimit)
	                                values
	                                ({0}, {1}, {2}, 
	                                {3}, {4}, {5}, 
	                                {6}, {7}, {8})",
                                    ar.AnalystMasterId, ar.AppraisedByUserID, ar.AppraisedByUserCode,
                                    ar.SupportedByUserID, ar.SupportedByUserCode, ar.ApprovedByUserID,
                                    ar.ApprovedByUserCode, ar.AutoDLALimit, ar.TotalSecuredLimit);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_alertverificationreport
        private void SaveAuto_An_AlertVerificationReport(Auto_An_AlertVerificationReport a, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_alertverificationreport where AnalystMasterId = " + a.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_alertverificationreport 
	                                (AnalystMasterId, MaxAgeAllowed, MaxAgeFound, MaxAgeFlag, 
	                                MinAgeAllowed, MinAgeFound, MinAgeFlag, MinIncomeAllowed, 
	                                MinIncomeFound, MinIncomeFlag, Location, LocationCosidered, 
	                                Nationality, NationalityConsidered, CarAgeAtTenorEndAllowed, 
	                                CarAgeAtTenorEndFound, CarAgeAtTenorEndFlag, SeatingCapacityAllowed, 
	                                SeatingCapacityFound, SeatingCapacityFlag, CarVerification, 
	                                CarVerificationFlag, HypothecationCheck, HypothecationCheckFlag, 
	                                VendorStatus, VendorFlag, LoanAmountAllowed, LoanAmountFound, 
	                                LoanAmountFlag, BBMaxLimitAllowed, BBMaxLimitFound, BBMaxLimitFlag, 
	                                InterestRateAllowed, InterestRateFound, InterestRateFlag, 
	                                TenorAllowed, TenorFound, TenorFlag, ARTAEnrollmentAllowed, 
	                                ARTAEnrollmentFound, ARTAEnrollmentFlag, LTVIncBancaAllowed, 
	                                LTVIncBancaFound, LTVIncBancaFlag, MaxLTVSpreadAllowed, 
	                                MaxLTVSpreadFound, MaxLTVSpreadFlag, DBRAllowed, DBRFound, 
	                                DBRFlag, TotalAutoLoanAllowed, TotalAutoLoanFound, TotalAutoLoanFlag, 
	                                ApproversDLAAllowed, ApproversDLAFound, ApproversDLAFlag, 
	                                CIBOptained, CIBOptainedFlag, JtMaxAgeAllowed, JtMaxAgeFound, 
	                                JtMaxAgeFlag, JtMinAgeAllowed, JtMinAgeFound, JtMinAgeFlag, 
	                                JtMinIncomeAllowed, JtMinIncomeFound, JtMinIncomeFlag)
	                                values
	                                ('{0}','{1}','{2}','{3}',
                                    '{4}','{5}','{6}','{7}',
                                    '{8}','{9}','{10}','{11}',
                                    '{12}','{13}','{14}','{15}',
                                    '{16}','{17}','{18}','{19}',
                                    '{20}','{21}','{22}','{23}',
                                    '{24}','{25}','{26}','{27}',
                                    '{28}','{29}','{30}','{31}',
                                    '{32}','{33}','{34}','{35}',
                                    '{36}','{37}','{38}','{39}',
                                    '{40}','{41}','{42}','{43}',
                                    '{44}','{45}','{46}','{47}',
                                    '{48}','{49}','{50}','{51}',
                                    '{52}','{53}','{54}','{55}',
                                    '{56}','{57}','{58}','{59}',
                                    '{60}','{61}','{62}','{63}',
                                    '{64}','{65}','{66}')",
                                    a.AnalystMasterId, a.MaxAgeAllowed, a.MaxAgeFound, a.MaxAgeFlag,
                                    a.MinAgeAllowed, a.MinAgeFound, a.MinAgeFlag, a.MinIncomeAllowed,
                                    a.MinIncomeFound, a.MinIncomeFlag, a.Location, a.LocationCosidered,
                                    a.Nationality, a.NationalityConsidered, a.CarAgeAtTenorEndAllowed,
                                    a.CarAgeAtTenorEndFound, a.CarAgeAtTenorEndFlag, a.SeatingCapacityAllowed,
                                    a.SeatingCapacityFound, a.SeatingCapacityFlag, a.CarVerification,
                                    a.CarVerificationFlag, a.HypothecationCheck, a.HypothecationCheckFlag,
                                    a.VendorStatus, a.VendorFlag, a.LoanAmountAllowed, a.LoanAmountFound,
                                    a.LoanAmountFlag, a.BBMaxLimitAllowed, a.BBMaxLimitFound, a.BBMaxLimitFlag,
                                    a.InterestRateAllowed, a.InterestRateFound, a.InterestRateFlag,
                                    a.TenorAllowed, a.TenorFound, a.TenorFlag, a.ARTAEnrollmentAllowed,
                                    a.ARTAEnrollmentFound, a.ARTAEnrollmentFlag, a.LTVIncBancaAllowed,
                                    a.LTVIncBancaFound, a.LTVIncBancaFlag, a.MaxLTVSpreadAllowed,
                                    a.MaxLTVSpreadFound, a.MaxLTVSpreadFlag, a.DBRAllowed, a.DBRFound,
                                    a.DBRFlag, a.TotalAutoLoanAllowed, a.TotalAutoLoanFound, a.TotalAutoLoanFlag,
                                    a.ApproversDLAAllowed, a.ApproversDLAFound, a.ApproversDLAFlag,
                                    a.CIBOptained, a.CIBOptainedFlag, a.JtMaxAgeAllowed, a.JtMaxAgeFound,
                                    a.JtMaxAgeFlag, a.JtMinAgeAllowed, a.JtMinAgeFound, a.JtMinAgeFlag,
                                    a.JtMinIncomeAllowed, a.JtMinIncomeFound, a.JtMinIncomeFlag);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }

        //auto_an_rework
        private void SaveAuto_An_FinalizationRework(Auto_An_FinalizationRework rw, CommonDbHelper objDbHelper)
        {
            try
            {
                string sql = "delete from auto_an_rework where AnalystMasterId = " + rw.AnalystMasterId;
                objDbHelper.ExecuteNonQuery(sql);

                sql = string.Format(@"insert into auto_an_rework 
	                                (AnalystMasterId, ReworkDone, ReworkReason, LastApprovalDate, 
	                                ReworkDate, ReworkCount, LastApprovalAmount)
	                                values
	                                ({0}, {1}, '{2}', '{3}', 
	                                '{4}', {5}, {6})",
                                     rw.AnalystMasterId, rw.ReworkDone, rw.ReworkReason, rw.LastApprovalDate,
                                     rw.ReworkDate, rw.ReworkCount, rw.LastApprovalAmount);

                objDbHelper.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
            }
        }



        #endregion Finalization

        public int CheckLLIDIsAuto(string llId)
        {
            Int32 masterId = 0;
            var objDbHelper = new CommonDbHelper();
            var objAutoLoanMaster = new AutoLoanMaster();
            objAutoLoanMaster = null;
            try
            {
                var quary = "Select Auto_LoanMasterId from auto_loanmaster where LLID=" + llId;
                return masterId = Convert.ToInt32(DbQueryManager.ExecuteScalar(quary));
            }
            catch (Exception exception)
            {
                throw exception;
            }
           
        }


        public AutoStatusHistory GetRejectInformation(int appiledOnAutoLoanMasterId)
        {
            var dbHelper = new CommonDbHelper();
            var objAutoStatusHistory = new AutoStatusHistory();
            objAutoStatusHistory = null;
            try
            {
                string sql = string.Format(@"select * from autostatushistory 
where  StatusID in (37,38) and AutoLoanMasterId = {0}
ORDER BY ActionDate desc
LIMIT 1", appiledOnAutoLoanMasterId);
                IDataReader reader = dbHelper.GetDataReader(sql);

                var autoStatusHistoryDataReader = new AutoStatusHistoryDataReader(reader);
                if (reader.Read())
                    objAutoStatusHistory = autoStatusHistoryDataReader.Read();
                reader.Close();
            }
            catch (Exception ex)
            {
                dbHelper.RollBack();
            }
            finally
            {
                dbHelper.Close();
            }
            return objAutoStatusHistory;
        }


    }


}
