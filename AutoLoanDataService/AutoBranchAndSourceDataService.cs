﻿using System;
using System.Collections.Generic;
using AutoLoanService.Interface;
using AutoLoanService.Entity;
using Azolution.Common.DataService;
using AutoLoanDataService.DataReader;
using System.Data;
using System.Configuration;
using Bat.Common;
using System.Data.OleDb;

namespace AutoLoanDataService
{
    public class AutoBranchAndSourceDataService : IAutobranchAndSource
    {
        public void SaveAutoBranchWithSource(AutoBranchWithSource objAutoBranchWithSource, CommonDbHelper objDbHelper)
        {
            int autobranchId = 0;
            int autoSourceId = 0;
            try
            {
                string quary = "select AutoBranchId from Auto_Branch where rtrim(ltrim(AutoBranchName))='" + objAutoBranchWithSource.AutoBranchName.Trim() + "'";
                IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    autobranchId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
                if (autobranchId == 0)
                {
                    autobranchId = SaveAutoBranch(objAutoBranchWithSource, objDbHelper);
                }
                objAutoBranchWithSource.AutoBranchId = autobranchId;

                quary =
                    "select AutoSourceId from Auto_Source where AutoBranchId=" +
                    objAutoBranchWithSource.AutoBranchId + " and rtrim(ltrim(SourceName))='" +
                    objAutoBranchWithSource.SourceName.Trim() + "' and rtrim(ltrim(SourceCode))='" +
                    objAutoBranchWithSource.SourceCode.Trim() + "'";
                tempDataReder = objDbHelper.GetDataReader(quary);
                while (tempDataReder.Read())
                {
                    autoSourceId = tempDataReder.GetInt32(0);
                }
                tempDataReder.Close();
                objAutoBranchWithSource.AutoSourceId = autoSourceId;
                if(objAutoBranchWithSource.AutoSourceId == 0)
                {
                    SaveAutoSource(objAutoBranchWithSource, objDbHelper);
                }
            }
            catch (Exception)
            {
                objDbHelper.RollBack();
            }
        }

        private void SaveAutoSource(AutoBranchWithSource objAutoBranchWithSource, CommonDbHelper objDbHelper)
        {
            try
            {
                if (objAutoBranchWithSource.AutoSourceId == 0)
                {
                    string quary = "INSERT INTO Auto_Source (AutoBranchId,SourceName,SourceCode) values (" +
                                   objAutoBranchWithSource.AutoBranchId + ",'" +
                                   objAutoBranchWithSource.SourceName.Trim() + "','" +
                                   objAutoBranchWithSource.SourceCode.Trim() + "')";
                    objDbHelper.ExecuteNonQuery(quary);
                }
                else
                {
                    string quary = "Update Auto_Source set SourceName='" + objAutoBranchWithSource.SourceName.Trim() +
                                   "',SourceCode='" + objAutoBranchWithSource.SourceCode.Trim() +
                                   "' where AutoSourceId=" + objAutoBranchWithSource.AutoSourceId;
                    objDbHelper.ExecuteNonQuery(quary);
                }
            }
            catch (Exception)
            {

                objDbHelper.RollBack();
            }
            
        }

        private int SaveAutoBranch(AutoBranchWithSource objAutoBranchWithSource, CommonDbHelper objDbHelper)
        {
            int autoBranchId = 0;
            try
            {
                if (objAutoBranchWithSource.AutoBranchId == 0)
                {
                    string quary =
                        "INSERT INTO Auto_Branch (AutoBranchName) values ('" +
                        objAutoBranchWithSource.AutoBranchName.Trim() + "')";
                    objDbHelper.ExecuteNonQuery(quary);
                    quary = "select AutoBranchId from Auto_Branch where rtrim(ltrim(AutoBranchName))='" + objAutoBranchWithSource.AutoBranchName.Trim() + "'";
                    IDataReader tempDataReder = objDbHelper.GetDataReader(quary);
                    while (tempDataReder.Read())
                    {
                        autoBranchId = tempDataReder.GetInt32(0);
                    }
                    tempDataReder.Close();
                }
                else
                {
                    string quary = "Update Auto_Branch set AutoBranchName='" +
                                   objAutoBranchWithSource.AutoBranchName.Trim() + "' where AutoBranchId=" + objAutoBranchWithSource.AutoBranchId;
                    objDbHelper.ExecuteNonQuery(quary);
                    autoBranchId = objAutoBranchWithSource.AutoSourceId;
                }


            }
            catch (Exception)
            {

                objDbHelper.RollBack();
            }
            return autoBranchId;
        }

        public List<AutoBranchWithSource> GetAllSourceNameWithBranch(int pageNo, int pageSize, string searchtext)
        {
            var condition = "where auto_source.IsActive = 1";
            if (searchtext != "")
            {
                condition = "where auto_source.IsActive = 1 and (auto_branch.AutoBranchName like '%" + searchtext + "%' or SourceName like '%" + searchtext + "%' or SourceCode like '%" + searchtext + "%')";
            }
            var objAutoBranchWithSource = new List<AutoBranchWithSource>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                //var pageNumber = pageNo * pageSize;
                string orderBy = "ORDER BY  AutoBranchName asc";
                condition = condition + " " + orderBy;

                string sql = string.Format(@"SELECT
auto_branch.AutoBranchId,
auto_branch.AutoBranchName,
auto_source.AutoSourceId,
auto_source.SourceName,
auto_source.SourceCode,
(Select Count(AutoSourceId) from auto_source) as TotalCount
FROM
auto_branch
INNER JOIN auto_source ON auto_source.AutoBranchId = auto_branch.AutoBranchId
 {2} 
LIMIT {0}, {1} 
", pageNo, pageSize, condition);

                //DataTable auto_InsuranceBancaListDataTableObj = new DataTable();
                //auto_InsuranceBancaListDataTableObj = DbQueryManager.GetTable(sql);
                //DataTableReader reader = auto_InsuranceBancaListDataTableObj.CreateDataReader();

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoBranchWithSourceDataReader = new AutoBranchWithSourceDataReader(reader);
                while (reader.Read())
                    objAutoBranchWithSource.Add(autoBranchWithSourceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoBranchWithSource;
        }

        public string SaveAutoBranchAndSource(AutoBranchWithSource sourceAndbranch)
        {
            var mes = "";
            var sourceId = 0;
            var objDbHelper = new CommonDbHelper();
            try
            {
                if (sourceAndbranch.AutoSourceId > 0)
                {
                    var query = string.Format(@"select AutoSourceId from auto_source 
where AutoSourceId != {0} and SourceCode = '{1}' and IsActive = 1", sourceAndbranch.AutoSourceId, sourceAndbranch.SourceCode);

                    var tempReder = objDbHelper.GetDataReader(query);
                    while (tempReder.Read())
                    {
                        sourceId = tempReder.GetInt32(0);
                    }
                    tempReder.Close();
                }
                else {
                    var query = string.Format(@"select AutoSourceId from auto_source 
where SourceCode = '{0}' and IsActive = 1",sourceAndbranch.SourceCode);

                    var tempReder = objDbHelper.GetDataReader(query);
                    while (tempReder.Read())
                    {
                        sourceId = tempReder.GetInt32(0);
                    }
                    tempReder.Close();
                }

                if (sourceId == 0)
                {
                    if (sourceAndbranch.AutoBranchId == 0)
                    {
                        var autobranchId = SaveAutoBranch(sourceAndbranch, objDbHelper);
                        sourceAndbranch.AutoBranchId = autobranchId;
                    }
                    SaveAutoSource(sourceAndbranch, objDbHelper);
                    mes = "Success";
                }
                else {
                    mes = "Already exist";
                }
            }
            catch (Exception)
            {
                mes = "Failed";
            }
            finally
            {
                objDbHelper.Close();
            }
            return mes;
        }

        public List<AutoBranchWithSource> GetAllbranchName()
        {
            
            var objAutoBranchWithSource = new List<AutoBranchWithSource>();
            var objDbHelper = new CommonDbHelper();
            try
            {

                string sql = string.Format(@"SELECT * from auto_branch");
                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoBranchWithSourceDataReader = new AutoBranchWithSourceDataReader(reader);
                while (reader.Read())
                    objAutoBranchWithSource.Add(autoBranchWithSourceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoBranchWithSource;
        }

        public List<AutoBranchWithSource> GetAllAutoSourceByBranchId(int branchId)
        {
            var objAutoBranchWithSource = new List<AutoBranchWithSource>();
            var objDbHelper = new CommonDbHelper();
            try
            {

                string sql = string.Format(@"SELECT * from Auto_Source where AutoBranchId = {0} and IsActive = 1", branchId);
                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoBranchWithSourceDataReader = new AutoBranchWithSourceDataReader(reader);
                while (reader.Read())
                    objAutoBranchWithSource.Add(autoBranchWithSourceDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoBranchWithSource;
        }

        public List<AutoFacility> GetFacility()
        {
            var objAutoFacility = new List<AutoFacility>();
            var objDbHelper = new CommonDbHelper();
            try
            {

                string sql = string.Format(@"SELECT * from t_pluss_facility");
                IDataReader reader = objDbHelper.GetDataReader(sql);

                var autoFacilityDataReader = new AutoFacilityDataReader(reader);
                while (reader.Read())
                    objAutoFacility.Add(autoFacilityDataReader.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoFacility;
        }

        public string InactiveSourceById(int sourceId) {
            var message = "";
            var objDbHelper = new CommonDbHelper();
            try
            {
                string sql = string.Format(@"Update Auto_Source set IsActive = 0 where AutoSourceId = {0}", sourceId);
                objDbHelper.ExecuteNonQuery(sql);
                message = "Success";
            }
            catch (Exception ex)
            {
                message = "Failed";
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return message;
        }
    }
}
