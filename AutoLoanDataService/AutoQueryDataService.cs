﻿using System;
using System.Collections.Generic;
using AutoLoanService.Interface;
using AutoLoanService.Entity;
using Azolution.Common.DataService;
using AutoLoanDataService.DataReader;
using System.Data;
using System.Configuration;
using Bat.Common;
using System.Data.OleDb;

namespace AutoLoanDataService
{
   public class AutoQueryDataService : IQueryRepository
    {
        #region Query/Feedback

        public List<Department_Entity> GetAll_AT_Department()
        {
            var condition = "where t_pluss_usertype.USLE_CODE ='AT'";

            var objATDepartment = new List<Department_Entity>();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "order by t_pluss_usertype.USLE_ID asc";
                string sql = string.Format(@"select USLE_ID,USLE_CODE,USLE_NAME from t_pluss_usertype {0} {1}", condition, orderBy);

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var ATDepartment = new DepartmentDataReader(reader);
                while (reader.Read())
                    objATDepartment.Add(ATDepartment.Read());
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objATDepartment;
        }

        public QueryTrackerDetails_Entity SearchQueryDetailsByLLID(int pageNumber, int pageSize, int searchKey)
        {
            var condition = "where auto_loanmaster.LLID ="+searchKey+"";

          //  List<Department_Entity> objQueryBtLLId =new List<Department_Entity>();
           QueryTrackerDetails_Entity objQueryBtLLId = new QueryTrackerDetails_Entity();
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "";
                string sql = string.Format(@"SELECT auto_loanmaster.LLID,auto_prapplicant.Auto_LoanMasterId,auto_prapplicant.PrName,auto_loanmaster.AppliedAmount,auto_loanmaster.AppliedDate,auto_prapplicant.DateofBirth,auto_loanmaster.AskingTenor FROM auto_prapplicant
                                             INNER JOIN auto_loanmaster ON auto_loanmaster.Auto_LoanMasterId = auto_prapplicant.Auto_LoanMasterId {2} LIMIT {0}, {1}", pageNumber, pageSize, condition); 

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var QueryBtLLId = new QueryTrackerDetailsDaraReader(reader);
                while (reader.Read())
                {
                objQueryBtLLId = QueryBtLLId.Read();
                }
                reader.Close();
                //if (objQueryBtLLId != null)
                //{
                //    objQueryBtLLId.QueryTrackerList = GetQueryTrackerListByLLID(pageNumber,pageSize,searchKey);
                   
                //}



            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objQueryBtLLId;
        }

        public List<QueryTracker_Entity> GetQueryTrackerListByLLID(int userId,int usLeId, int pageNumber, int pageSize, int searchKey)
        {
            
           // var condition = "where LLID =" + searchKey + " and PostingById =" + userId + " or QueryToId IN(SELECT pluss_groupmember.GroupId from pluss_groupmember where pluss_groupmember.UserId= " + userId + ")";
            var condition = "where LLID =" + searchKey + " and (PostingById =" + userId + " or QueryToId =" + usLeId + " or auto_querytracker.refTrackerId IN (SELECT auto_querytracker.TrackerId from auto_querytracker where LLID =" + searchKey + " and refTrackerId is null and PostingById =" + userId + "))";
            List<QueryTracker_Entity> objQueryTrackerList = new List<QueryTracker_Entity>();
            
            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "";
//                string sql = string.Format(@"select `auto_querytracker`.`TrackerId` AS `TrackerId`,`auto_querytracker`.`QueryDate` AS `QueryDate`,`t_pluss_usertype`.`USLE_NAME` AS `User_And_Department`,`auto_querytracker`.`Comments` AS `Comments`,`t_pluss_user`.`USER_NAME` AS `QueriedBy`,(select `prevtracker`.`Comments` AS `Comments` from `auto_querytracker` `prevtracker` where (`prevtracker`.`TrackerId` = `auto_querytracker`.`RefTrackerId`)) AS `QueriedOnComments`,
//(select count(auto_querytracker.TrackerId) from auto_querytracker {2})as Totalcount
//from ((`auto_querytracker` join `t_pluss_user` on((`auto_querytracker`.`PostingById` = `t_pluss_user`.`USER_ID`))) join `t_pluss_usertype` on((`auto_querytracker`.`QueryToId` = `t_pluss_usertype`.`USLE_ID`))) {2} LIMIT {0}, {1}", pageNumber, pageSize, condition);

//                string sql = string.Format(@"SELECT
//querymaster.TrackerId,
//querymaster.LLID,
//querymaster.QueryDate,
//querymaster.USLE_ID,
//querymaster.User_And_Department,
//querymaster.PostingById,
//querymaster.QueriedBy,
//querymaster.Comments,
//querymaster.QueryToId,
//querymaster.QueriedOnComments,
//(select count(querymaster.TrackerId) from querymaster {2})as Totalcount
//FROM
//querymaster
//{2}
// LIMIT {0}, {1}", pageNumber, pageSize, condition);
                string sql = string.Format(@"
select `auto_querytracker`.`TrackerId` AS `TrackerId`,`auto_querytracker`.`LLId` AS `LLId`,`auto_querytracker`.`QueryDate` AS `QueryDate`,`t_pluss_usertype`.`USLE_ID` AS `USLE_ID`,concat(`t_pluss_user`.`USER_NAME`,' ( ',`t_pluss_usertype`.`USLE_NAME`,' ) ') AS `User_And_Department`,`auto_querytracker`.`PostingById` AS `PostingById`,`t_pluss_user`.`USER_NAME` AS `QueriedBy`,`auto_querytracker`.`Comments` AS `Comments`,`auto_querytracker`.`QueryToId` AS `QueryToId`,`auto_querytracker`.`Comments` AS `QueriedOnComments`,`auto_querytracker`.`RefTrackerId` AS `RefTrackerId` 
,(select count(auto_querytracker.TrackerId) from auto_querytracker {2})as Totalcount
from ((`auto_querytracker` join `t_pluss_user` on((`t_pluss_user`.`USER_ID` = `auto_querytracker`.`PostingById`))) join `t_pluss_usertype` on((`t_pluss_usertype`.`USLE_ID` = `t_pluss_user`.`AUTO_USER_LEVEL`)))

{2}
 LIMIT {0}, {1}", pageNumber, pageSize, condition);
                IDataReader reader = objDbHelper.GetDataReader(sql);

                var QueryTrackerList = new QueryTrackerDataReader(reader);
                while (reader.Read())
                    objQueryTrackerList.Add(QueryTrackerList.Read());
               
                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objQueryTrackerList;
        }

        public List<QueryTracker_Entity> GetQueryTrackerListByReadUnreadStatus(int userId, int usLeId, int pageNumber, int pageSize, int readUnreadStatus)
       {

           
         List<QueryTracker_Entity> objQueryTrackerList = new List<QueryTracker_Entity>();

           var objDbHelper = new CommonDbHelper();
           try
           {
               string orderBy = "";
               string sql =
                   string.Format(@"select `auto_querytracker`.`TrackerId` AS `TrackerId`,`auto_querytracker`.`LLId` AS `LLId`,`auto_querytracker`.`QueryDate` AS `QueryDate`,
`t_pluss_usertype`.`USLE_ID` AS `USLE_ID`,concat(`t_pluss_user`.`USER_NAME`,' ( ',`t_pluss_usertype`.`USLE_NAME`,' ) ') AS `User_And_Department`,
`auto_querytracker`.`PostingById` AS `PostingById`,`t_pluss_user`.`USER_NAME` AS `QueriedBy`,`auto_querytracker`.`Comments` AS `Comments`,
`auto_querytracker`.`QueryToId` AS `QueryToId`,`auto_querytracker`.`Comments` AS `QueriedOnComments`,`auto_querytracker`.`RefTrackerId` AS `RefTrackerId` 
,(select count(auto_querytracker.TrackerId) from auto_querytracker where ReadUnReadStatus =0 And (RefTrackerId in (Select TrackerId from auto_querytracker where PostingById = " + userId + ") or QueryToId = " + usLeId + @" ))as Totalcount
from ((`auto_querytracker` join `t_pluss_user` on((`t_pluss_user`.`USER_ID` = `auto_querytracker`.`PostingById`))) join `t_pluss_usertype` on((`t_pluss_usertype`.`USLE_ID` = `t_pluss_user`.`AUTO_USER_LEVEL`)))
where ReadUnReadStatus =0 And ( RefTrackerId in (Select TrackerId from auto_querytracker where PostingById = " + userId + ") or QueryToId = " + usLeId + @"
 ) LIMIT {0}, {1}", pageNumber, pageSize);
//               string sql = string.Format(@"
//select `auto_querytracker`.`TrackerId` AS `TrackerId`,`auto_querytracker`.`LLId` AS `LLId`,`auto_querytracker`.`QueryDate` AS `QueryDate`,`t_pluss_usertype`.`USLE_ID` AS `USLE_ID`,concat(`t_pluss_user`.`USER_NAME`,' ( ',`t_pluss_usertype`.`USLE_NAME`,' ) ') AS `User_And_Department`,`auto_querytracker`.`PostingById` AS `PostingById`,`t_pluss_user`.`USER_NAME` AS `QueriedBy`,`auto_querytracker`.`Comments` AS `Comments`,`auto_querytracker`.`QueryToId` AS `QueryToId`,`auto_querytracker`.`Comments` AS `QueriedOnComments`,`auto_querytracker`.`RefTrackerId` AS `RefTrackerId` 
//,(select count(auto_querytracker.TrackerId) from auto_querytracker {2})as Totalcount
//from ((`auto_querytracker` join `t_pluss_user` on((`t_pluss_user`.`USER_ID` = `auto_querytracker`.`PostingById`))) join `t_pluss_usertype` on((`t_pluss_usertype`.`USLE_ID` = `t_pluss_user`.`AUTO_USER_LEVEL`)))
//
//{2}
// LIMIT {0}, {1}", pageNumber, pageSize, condition);
               IDataReader reader = objDbHelper.GetDataReader(sql);

               var QueryTrackerList = new QueryTrackerDataReader(reader);
               while (reader.Read())
                   objQueryTrackerList.Add(QueryTrackerList.Read());

               reader.Close();


           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               objDbHelper.Close();
           }
           return objQueryTrackerList;
       }

       public string SaveNewQuery(QueryTracker_Entity objNewQuery)
       {
           var dbHelper = new CommonDbHelper();
           var Result = "";
           try
           {
               string orderBy = "";
               string sql = string.Format(@"Insert into auto_querytracker(LLID,PostingById,QueryToId,Comments,QueryDate)
                                            values(" + objNewQuery.LLId + "," + objNewQuery.PostingById + "," + objNewQuery.QueryToId + ",'" + objNewQuery.Comments+ "',NOW())");

               dbHelper.ExecuteNonQuery(sql);
               Result = "Success";
           }
           catch (Exception ex)
           {
               dbHelper.RollBack();
               Result = "Failed to Save";
           }
           finally
           {
               dbHelper.Close();
           }
           return Result;
       }

       public List<AutoStatusHistory> SearchApplicationStatusByLLID(int searchKey)
        {
            var condition = "where (`auto_loanmaster`.`LLID` = " + searchKey + ")";

            List<AutoStatusHistory> objAutoStatusHistoryList = new List<AutoStatusHistory>();

            var objDbHelper = new CommonDbHelper();
            try
            {
                string orderBy = "";
                string sql = string.Format(@"SELECT auto_loanmaster.LLID,autostatushistory.AutoLoanMasterId,autoworkflowstatus.StatusID,autoworkflowstatus.StatusName,autostatushistory.ActionDate FROM auto_loanmaster INNER JOIN autostatushistory ON auto_loanmaster.Auto_LoanMasterId = autostatushistory.AutoLoanMasterId INNER JOIN autoworkflowstatus ON autostatushistory.StatusID = autoworkflowstatus.StatusID {0}", condition); 

                IDataReader reader = objDbHelper.GetDataReader(sql);

                var ApplicationStatusList = new AutoStatusHistoryDataReader(reader);
                while (reader.Read())
                    objAutoStatusHistoryList.Add(ApplicationStatusList.Read());

                reader.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDbHelper.Close();
            }
            return objAutoStatusHistoryList;
        }

       public string ReplyOnQuery(QueryTracker_Entity objReplyOnQuery)
       {
           var dbHelper = new CommonDbHelper();
           var Result = "";
           try
           {
               string orderBy = "";
               string sql = string.Format(@"Insert into auto_querytracker(LLID,RefTrackerId,PostingById,QueryToId,Comments,QueryDate)
                                            values(" + objReplyOnQuery.LLId + "," + objReplyOnQuery.RefTrackerId + "," + objReplyOnQuery.PostingById + "," + objReplyOnQuery.QueryToId + ",'" + objReplyOnQuery.Comments + "',NOW())");

               dbHelper.ExecuteNonQuery(sql);
               Result = "Success";
           }
           catch (Exception ex)
           {
               dbHelper.RollBack();
               Result = "Failed to Save";
           }
           finally
           {
               dbHelper.Close();
           }
           return Result;
       }

       public string UpdateStatusAsReadQueryByTrackerId(int trackerId)
       {
           var dbHelper = new CommonDbHelper();
           var Result = "";
           try
           {
               string orderBy = "";
               string sql =
                   string.Format("Update auto_querytracker set ReadUnReadStatus =1 where TrackerId =" + trackerId + "");

               dbHelper.ExecuteNonQuery(sql);
               Result = "Success";
           }
           catch (Exception ex)
           {
               dbHelper.RollBack();
               Result = "Failed to Update";
           }
           finally
           {
               dbHelper.Close();
           }
           return Result;
       }

       #endregion
    }
}
