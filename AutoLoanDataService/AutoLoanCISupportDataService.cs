﻿using System;
using System.Collections.Generic;
using AutoLoanService.Interface;
using AutoLoanService.Entity;
using Azolution.Common.DataService;
using AutoLoanDataService.DataReader;
using System.Data;
using System.Configuration;
using Bat.Common;
using System.Data.OleDb;

namespace AutoLoanDataService
{
    public class AutoLoanCISupportDataService : AutoLoanCISupportRepository
    {



        public string SaveCISupportLoanApp(Auto_LoanAnalystMaster autoAnalistMaster,
            Auto_LoanAnalystApplication autoLoanAnalystApplication, Auto_LoanAnalystVehicleDetail autoLoanAnalystVehicleDetail,
            List<AutoApplicantSCBAccount> autoApplicantAccountSCBList, List<AutoPRAccountDetail> autoApplicantAccountOtherList,
            List<AutoLoanFacility> autoFacilityList, List<AutoLoanSecurity> autoSecurityList, List<AutoLoanFacilityOffSCB> oFFSCBFacilityList,
            int userID, AutoLoanVehicle objAutoLoanVehicle, AutoVendor objAutoVendor)
        {
            string returnValue = "";
            string deleteSql = "";

            var objDbHelper = new CommonDbHelper();
            try
            {
                objDbHelper.BeginTransaction();

                //Save Auto Analyst Master
                int autoLoanMasterId = autoAnalistMaster.AutoLoanMasterId;
                int analystMasterId = SaveAutoLoanAnalystMaster(autoAnalistMaster, objDbHelper);

                #region update ExecutionUserBY table

                Auto_App_Step_ExecutionByUser ebu = new Auto_App_Step_ExecutionByUser();

                ebu.Auto_LoanMasterId = autoLoanMasterId;
                ebu.AppSupportBy = userID;

                UpdateExecutionByUser(ebu, objDbHelper);

                #endregion update ExecutionUserBY table


                //Save auto Loan Analyst Application
                autoLoanAnalystApplication.AnalystMasterId = analystMasterId;
                SaveAutoLoanAnalystApplication(autoLoanAnalystApplication, objDbHelper);

                //Save auto Loan Analyst Vehicle Detail
                autoLoanAnalystVehicleDetail.AnalystMasterId = analystMasterId;
                SaveAutoLoanAnalystVehicleDetails(autoLoanAnalystVehicleDetail, objDbHelper);

                // Save With SCB Accounts
                deleteSql = "Delete from auto_applicantscbaccount where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                foreach(AutoApplicantSCBAccount scbAccount in autoApplicantAccountSCBList)
                {
                    scbAccount.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoApplicantSCBAccount(scbAccount, objDbHelper);
                }

                //Save PR Account Detail
                deleteSql = "Delete from auto_praccount_details where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                foreach(AutoPRAccountDetail pra in autoApplicantAccountOtherList)
                {
                    pra.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoPRAccountDetail(pra, objDbHelper);
                }

                //Save Auto Loan Facility

                //Code change by Washik 17/11/2012
                deleteSql = "Delete from auto_loanfacility where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                deleteSql = "Delete from auto_loansecurity where AutoLoanMasterId = " + autoLoanMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                //foreach(AutoLoanFacility af in autoFacilityList)
                //{
                //    af.AutoLoanMasterId = autoLoanMasterId;
                //    SaveAutoLoanFacility(af, objDbHelper);
                //}

                ////Save Auto Loan Security

                //foreach(AutoLoanSecurity al in autoSecurityList)
                //{
                //    al.AutoLoanMasterId = autoLoanMasterId;
                //    SaveAutoLoanSecurity(al, objDbHelper);
                //}

                for (var i = 0; i < autoFacilityList.Count; i++)
                {
                    autoFacilityList[i].AutoLoanMasterId = autoLoanMasterId;
                    int facilityId = SaveAutoLoanFacilityWithReturn(autoFacilityList[i], objDbHelper);

                    autoSecurityList[i].FacilityId = facilityId;
                    autoSecurityList[i].AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLoanSecurity(autoSecurityList[i], objDbHelper);
                }


                    // Save Auto Loan Facility OFF SCB
                    deleteSql = "Delete from auto_loanfacilityoffscb where AnalystMasterId = " + analystMasterId;
                objDbHelper.ExecuteNonQuery(deleteSql);

                foreach(AutoLoanFacilityOffSCB afscb in oFFSCBFacilityList)
                {
                    afscb.AnalystMasterId = analystMasterId;
                    afscb.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLoanFacilityOffSCB(afscb, objDbHelper);
                }

                if (objAutoLoanVehicle.VendorId == -1)
                {
                    if (objAutoVendor.VendorName != "")
                    {
                        int newVendorID = SaveNewVendor(objAutoVendor, objDbHelper);

                        objAutoLoanVehicle.AutoLoanMasterId = autoLoanMasterId;
                        objAutoLoanVehicle.VendorId = newVendorID;
                        SaveAutoLoanVehicle(objAutoLoanVehicle, objDbHelper);
                    }
                }
                else
                {
                    objAutoLoanVehicle.AutoLoanMasterId = autoLoanMasterId;
                    SaveAutoLoanVehicle(objAutoLoanVehicle, objDbHelper);
                }




                objDbHelper.CommitTransaction();
                returnValue = "Success";
            }
            catch (Exception ex)
            {
                returnValue = "Failed";
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }

            return returnValue;
        }

        private void SaveAutoLoanVehicle(AutoLoanVehicle objAutoLoanVehicle, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var lastupdate = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Hour.ToString() + ":" +
                             DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
            try
            {
                if (objAutoLoanVehicle.AutoLoanVehicleId != 0)
                {
                    sql = String.Format(@"UPDATE auto_loanvehicle SET
                    AutoLoanMasterId = {0}, 
                    VendorId = {1},
                    Auto_ManufactureId = {2},
                    Model = '{3}',
                    TrimLevel = '{4}',
                    EngineCC = '{5}',
                    VehicleType = '{6}',
                    VehicleStatus = {7},
                    ManufacturingYear = {8},
                    QuotedPrice = {9},
                    VerifiedPrice = {10},
                    PriceConsideredBasedOn = '{11}',
                    SeatingCapacity = '{12}',
                    CarVerification  = '{13}',VehicleId  = '{15}' where AutoLoanVehicleId = {14}",
                    objAutoLoanVehicle.AutoLoanMasterId,
                    objAutoLoanVehicle.VendorId, objAutoLoanVehicle.Auto_ManufactureId,
                    objAutoLoanVehicle.Model, objAutoLoanVehicle.TrimLevel, objAutoLoanVehicle.EngineCC, objAutoLoanVehicle.VehicleType,
                    objAutoLoanVehicle.VehicleStatus, objAutoLoanVehicle.ManufacturingYear,
                    objAutoLoanVehicle.QuotedPrice, objAutoLoanVehicle.VerifiedPrice, objAutoLoanVehicle.PriceConsideredBasedOn, objAutoLoanVehicle.SeatingCapacity,
                    objAutoLoanVehicle.CarVerification, objAutoLoanVehicle.AutoLoanVehicleId, objAutoLoanVehicle.VehicleId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = String.Format(@"INSERT INTO auto_loanvehicle
                    (AutoLoanMasterId, VendorId, Auto_ManufactureId, 
	                Model, TrimLevel, EngineCC, VehicleType, VehicleStatus, ManufacturingYear, 
	                QuotedPrice, VerifiedPrice, PriceConsideredBasedOn, SeatingCapacity, 
	                CarVerification,VehicleId)
                    values
                    ({0}, {1}, {2}, 
                    '{3}', '{4}', '{5}', '{6}', {7}, 
                    {8}, {9}, {10}, '{11}', 
                    '{12}', '{13}',{14})", objAutoLoanVehicle.AutoLoanMasterId,
                    objAutoLoanVehicle.VendorId, objAutoLoanVehicle.Auto_ManufactureId,
                    objAutoLoanVehicle.Model, objAutoLoanVehicle.TrimLevel, objAutoLoanVehicle.EngineCC, objAutoLoanVehicle.VehicleType,
                    objAutoLoanVehicle.VehicleStatus, objAutoLoanVehicle.ManufacturingYear,
                    objAutoLoanVehicle.QuotedPrice, objAutoLoanVehicle.VerifiedPrice, objAutoLoanVehicle.PriceConsideredBasedOn, objAutoLoanVehicle.SeatingCapacity,
                    objAutoLoanVehicle.CarVerification, objAutoLoanVehicle.VehicleId);

                    objDbHelper.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private int SaveNewVendor(AutoVendor objAutoVendor, CommonDbHelper objDbHelper)
        {
            var returnValue = 0;
            string sql = "";

            try
            {
                sql = String.Format(@"INSERT INTO auto_vendor
                    (VendorName, DealerType) 
                    values
                    ('{0}', {1})", objAutoVendor.VendorName,
                objAutoVendor.DealerType);

                objDbHelper.ExecuteNonQuery(sql);

                //get the jtAccountId
                sql = String.Format(@"Select AutoVendorId from auto_vendor where 
                                        VendorName='{0}' and 
                                        DealerType={1}", objAutoVendor.VendorName, objAutoVendor.DealerType);

                IDataReader reader = objDbHelper.GetDataReader(sql);
                while (reader.Read())
                {
                    returnValue = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return returnValue;
        }


        private void UpdateExecutionByUser(Auto_App_Step_ExecutionByUser ExecutionByUser, CommonDbHelper objDbHelper)
        {
            string sql = "";

            try
            {
                sql = string.Format(@"update Auto_App_Step_ExecutionByUser set 
                                        AppSupportBy = {0} where Auto_LoanMasterId = {1}",
                                    ExecutionByUser.AppSupportBy,ExecutionByUser.Auto_LoanMasterId);

                objDbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int SaveAutoLoanAnalystMaster(Auto_LoanAnalystMaster autoAnalistMaster, CommonDbHelper objDbHelper)
        {
            //var dbHelper = new CommonDbHelper();
            int analystMasterId = 0;
            string sql = "";

            try
            {
                if (autoAnalistMaster.AnalystMasterId > 0)
                {
                    sql =
                        string.Format(
                            @"Update auto_loananalystmaster set
                                                LlId = {0},AutoLoanMasterId={1},IsJoint={2},ReasonOfJoint='{3}',Colleterization={5},
ColleterizationWith={6} where AnalystMasterId = {4}",
                            autoAnalistMaster.LlId, autoAnalistMaster.AutoLoanMasterId, autoAnalistMaster.IsJoint,
                            autoAnalistMaster.ReasonOfJoint, autoAnalistMaster.AnalystMasterId, autoAnalistMaster.Colleterization, autoAnalistMaster.ColleterizationWith);

                    objDbHelper.ExecuteNonQuery(sql);
                    analystMasterId = autoAnalistMaster.AnalystMasterId;
                }
                else
                {
                    sql = string.Format(
                        @"INSERT INTO auto_loananalystmaster(LlId,AutoLoanMasterId,IsJoint,ReasonOfJoint,Colleterization,ColleterizationWith)
                              VALUES (" +
                        autoAnalistMaster.LlId + "," + autoAnalistMaster.AutoLoanMasterId + "," +
                        autoAnalistMaster.IsJoint + ",'" + autoAnalistMaster.ReasonOfJoint+"',"+ autoAnalistMaster.Colleterization+","+ autoAnalistMaster.ColleterizationWith + ")");

                    objDbHelper.ExecuteNonQuery(sql);

                    sql =
                        string.Format(
                            @"Select Max(AnalystMasterId) from auto_loananalystmaster where 
                                    LlId = {0} and AutoLoanMasterId={1} and IsJoint={2} and ReasonOfJoint='{3}'",
                            autoAnalistMaster.LlId, autoAnalistMaster.AutoLoanMasterId, autoAnalistMaster.IsJoint,
                            autoAnalistMaster.ReasonOfJoint);

                    IDataReader reader = objDbHelper.GetDataReader(sql);
                    while(reader.Read())
                    {
                        analystMasterId = reader.GetInt32(0);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                analystMasterId = 0;
            }

            return analystMasterId;
        }

        private void SaveAutoLoanAnalystApplication(Auto_LoanAnalystApplication objAutoLoanAnalystApplication, CommonDbHelper dbHelper)
        {
            //string returnValue = "";
            string sql = "";
            try
            {
                if (objAutoLoanAnalystApplication.Id > 0)
                {
                    sql =
                        string.Format(
                            @"update auto_loananalystapplication set 
                            AnalystMasterId = {0},
                            Source = {1},
                            BranchId = {2},
                            BranchCode = '{3}',
                            ValuePack = {4},
                            BorrowingRelationShip = '{5}',
                            BorrowingRelationShipCode = '{6}',
                            CustomerStatus = {7},
                            AskingRepaymentMethod = {8},
                            ARTA = {9},
                            ARTAStatus = {10},
                            GeneralInsurance = {11},
                            CashSecured100Per = {12},
                            CCBundle = {13},
                            SecurityValue = {15},
                            GeneralInsurancePreference = {16}
                            where Id = {14}",
                            objAutoLoanAnalystApplication.AnalystMasterId, objAutoLoanAnalystApplication.Source,
                            objAutoLoanAnalystApplication.BranchId, objAutoLoanAnalystApplication.BranchCode,
                            objAutoLoanAnalystApplication.ValuePack, objAutoLoanAnalystApplication.BorrowingRelationShip,
                            objAutoLoanAnalystApplication.BorrowingRelationShipCode,
                            objAutoLoanAnalystApplication.CustomerStatus,
                            objAutoLoanAnalystApplication.AskingRepaymentMethod, objAutoLoanAnalystApplication.ARTA,
                            objAutoLoanAnalystApplication.ARTAStatus, objAutoLoanAnalystApplication.GeneralInsurance,
                            objAutoLoanAnalystApplication.CashSecured100Per, objAutoLoanAnalystApplication.CCBundle,
                            objAutoLoanAnalystApplication.Id, objAutoLoanAnalystApplication.SecurityValue, objAutoLoanAnalystApplication.GeneralInsurancePreference);
                }
                else
                {
                    sql =
                        string.Format(
                            @"INSERT INTO auto_loananalystapplication(AnalystMasterId,Source,BranchId,BranchCode,ValuePack,BorrowingRelationShip,
BorrowingRelationShipCode,CustomerStatus,AskingRepaymentMethod,ARTA,ARTAStatus,GeneralInsurance,CashSecured100Per,CCBundle,SecurityValue,GeneralInsurancePreference)
                          VALUES (" +
                            objAutoLoanAnalystApplication.AnalystMasterId + "," + objAutoLoanAnalystApplication.Source +
                            "," +
                            objAutoLoanAnalystApplication.BranchId + ",'" + objAutoLoanAnalystApplication.BranchCode +
                            "'," +
                            objAutoLoanAnalystApplication.ValuePack + ",'" +
                            objAutoLoanAnalystApplication.BorrowingRelationShip + "','" +
                            objAutoLoanAnalystApplication.BorrowingRelationShipCode + "'," +
                            objAutoLoanAnalystApplication.CustomerStatus + "," +
                            objAutoLoanAnalystApplication.AskingRepaymentMethod + "," +
                            objAutoLoanAnalystApplication.ARTA +
                            "," + objAutoLoanAnalystApplication.ARTAStatus + "," +
                            objAutoLoanAnalystApplication.GeneralInsurance + "," +
                            objAutoLoanAnalystApplication.CashSecured100Per + "," +
                            objAutoLoanAnalystApplication.CCBundle+"," +objAutoLoanAnalystApplication.SecurityValue +
                            "," + objAutoLoanAnalystApplication.GeneralInsurancePreference +
                            ")");

                }
                dbHelper.ExecuteNonQuery(sql);
                //returnValue = "Success";
            }
            catch (Exception ex)
            {
                //returnValue = "Failed to save AutoLoan Analyst Application";
                throw ex;
            }

            //return returnValue;
        }

        private void SaveAutoLoanAnalystVehicleDetails(Auto_LoanAnalystVehicleDetail objAutoAnVehicleDetail, CommonDbHelper dbHelper)
        {
            string sql = "";
            try
            {
                if (objAutoAnVehicleDetail.Id > 0)
                {
                    sql = string.Format(@"update auto_loananalystvehicledetail set 
                                            AnalystMasterId = {0},
                                            valuePackAllowed = {1},
                                            SeatingCapacity = {2},
                                            ConsideredPrice = {3},
                                            PriceConsideredOn = {4},
                                            CarVarification = {5},
                                            DeliveryStatus = {6},
                                            RegistrationYear = {8}
                                            where Id = {7}",
                        objAutoAnVehicleDetail.AnalystMasterId,objAutoAnVehicleDetail.valuePackAllowed,objAutoAnVehicleDetail.SeatingCapacity,
                    objAutoAnVehicleDetail.ConsideredPrice, objAutoAnVehicleDetail.PriceConsideredOn, objAutoAnVehicleDetail.CarVarification,
                    objAutoAnVehicleDetail.DeliveryStatus, objAutoAnVehicleDetail.Id, objAutoAnVehicleDetail.RegistrationYear);
                }
                else
                {
                    sql =
                        string.Format(
                            @"INSERT INTO auto_loananalystvehicledetail(AnalystMasterId,valuePackAllowed,SeatingCapacity,ConsideredPrice,PriceConsideredOn,CarVarification,DeliveryStatus, RegistrationYear)
                        VALUES (" +
                            objAutoAnVehicleDetail.AnalystMasterId + "," + objAutoAnVehicleDetail.valuePackAllowed + "," +
                            objAutoAnVehicleDetail.SeatingCapacity + "," + objAutoAnVehicleDetail.ConsideredPrice + "," +
                            objAutoAnVehicleDetail.PriceConsideredOn + "," + objAutoAnVehicleDetail.CarVarification +
                            "," +
                            objAutoAnVehicleDetail.DeliveryStatus + "," +
                            objAutoAnVehicleDetail.RegistrationYear +
                            ")");

                }

                dbHelper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        private void SaveAutoApplicantSCBAccount(AutoApplicantSCBAccount scbAccount, CommonDbHelper objDbHelper)
        {
            string sql =
                string.Format(
                    @"insert into auto_applicantscbaccount 
	                                    (AutoLoanMasterId, LLID, AccountNumberSCB, 
	                                    AccountStatus, PriorityHolder)
	                                    values
	                                    ({0}, {1}, '{2}', {3}, 
	                                    {4})",
                    scbAccount.AutoLoanMasterId, scbAccount.LLID, scbAccount.AccountNumberSCB, scbAccount.AccountStatus,
                    scbAccount.PriorityHolder);

            objDbHelper.ExecuteNonQuery(sql);

        }

        private void SaveAutoPRAccountDetail(AutoPRAccountDetail pra, CommonDbHelper objDbHelper)
        {
            string sql =
                string.Format(
                    @"insert into auto_praccount_details 
	                            (AutoLoanMasterId, BankId, BranchId, 
	                            AccountNumber, AccountCategory, AccountType)
	                            values
	                            ({0}, {1}, {2}, 
	                            '{3}', '{4}', '{5}')",
                    pra.AutoLoanMasterId, pra.BankId, pra.BranchId, pra.AccountNumber, pra.AccountCategory,
                    pra.AccountType);

            objDbHelper.ExecuteNonQuery(sql);
        }

        private void SaveAutoLoanFacility(AutoLoanFacility af, CommonDbHelper objDbHelper)
        {
            string sql =
                string.Format(
                    @"insert into auto_loanfacility 
	                                    (AutoLoanMasterId, FacilityType, InterestRate, 
	                                    PresentBalance, PresentEMI, PresentLimit, ProposedLimit, 
	                                    RepaymentAgrement, Consider, EMIPercent, EMIShare, Status
	                                    )
	                                    values
	                                    ({0}, {1}, {2}, 
	                                    {3}, {4}, {5}, {6}, 
	                                    {7}, {8}, {9}, {10}, {11})",
                    af.AutoLoanMasterId, af.FacilityType, af.InterestRate, af.PresentBalance, af.PresentEMI,
                    af.PresentLimit, af.ProposedLimit,
                    af.RepaymentAgrement, af.Consider, af.EMIPercent, af.EMIShare, af.Status);

            objDbHelper.ExecuteNonQuery(sql);
        }

        private int SaveAutoLoanFacilityWithReturn(AutoLoanFacility objAutoLoanFacilityListMember, CommonDbHelper objDbHelper)
        {
            int facilityId = 0;
            string sql = "";
            try
            {
                //                if (objAutoLoanFacilityListMember.FacilityId != 0)
                //                {
                //                    sql = String.Format(@"UPDATE auto_loanfacility SET
                //	                AutoLoanMasterId = {0},FacilityType = {1},InterestRate = {2},PresentBalance = {3},
                //	                PresentEMI = {4},PresentLimit = {5}, ProposedLimit = {6},RepaymentAgrement = {7} where FacilityId = {8}", 
                //                    objAutoLoanFacilityListMember.AutoLoanMasterId,
                //                    objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                //                    objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                //                    objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.FacilityId);

                //                    objDbHelper.ExecuteNonQuery(sql);
                //                }
                //                else
                //                {
                sql = String.Format(@"INSERT INTO auto_loanfacility
	                (AutoLoanMasterId, FacilityType, InterestRate, PresentBalance, 
	                PresentEMI, PresentLimit, ProposedLimit, RepaymentAgrement,EMIPercent,EMIShare,Status)
                    values
                    ({0}, {1}, {2}, 
                    {3}, {4}, {5}, {6}, {7},'{8}','{9}','{10}')", objAutoLoanFacilityListMember.AutoLoanMasterId,
                objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.EMIPercent, objAutoLoanFacilityListMember.EMIShare, objAutoLoanFacilityListMember.Status);
                objDbHelper.ExecuteNonQuery(sql);

                var selectQuary = string.Format(@"select FacilityId from auto_loanfacility where AutoLoanMasterId={0} and FacilityType={1} and 
                    InterestRate='{2}' and PresentBalance='{3}' and PresentEMI='{4}' and PresentLimit='{5}' and ProposedLimit='{6}' 
                    and RepaymentAgrement={7} and EMIPercent='{8}' and EMIShare='{9}' and Status='{10}'", objAutoLoanFacilityListMember.AutoLoanMasterId, objAutoLoanFacilityListMember.FacilityType, objAutoLoanFacilityListMember.InterestRate, objAutoLoanFacilityListMember.PresentBalance,
                objAutoLoanFacilityListMember.PresentEMI, objAutoLoanFacilityListMember.PresentLimit, objAutoLoanFacilityListMember.ProposedLimit,
                objAutoLoanFacilityListMember.RepaymentAgrement, objAutoLoanFacilityListMember.EMIPercent, objAutoLoanFacilityListMember.EMIShare, objAutoLoanFacilityListMember.Status);

                IDataReader reader = objDbHelper.GetDataReader(selectQuary);
                while (reader.Read())
                {
                    facilityId = Convert.ToInt32(reader.GetValue(0));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
            return facilityId;
        }

        private void SaveAutoLoanSecurity(AutoLoanSecurity objAutoLoanSecurityMember, CommonDbHelper objDbHelper)
        {
            string sql = "";
            var issueDate = objAutoLoanSecurityMember.IssueDate.ToString("yyyy-MM-dd");
            try
            {
                
                sql =
                    String.Format(
                        @"INSERT INTO auto_loansecurity
	                (AutoLoanMasterId, NatureOfSecurity, IssuingOffice, 
	                FaceValue, XTVRate, IssueDate, InterestRate,FacilityType, Status, FacilityId)
                    values
                    ({0}, {1}, '{2}', 
                    {3}, {4}, '{5}', '{6}',{7},{8},{9})",
                        objAutoLoanSecurityMember.AutoLoanMasterId,
                        objAutoLoanSecurityMember.NatureOfSecurity, objAutoLoanSecurityMember.IssuingOffice,
                        objAutoLoanSecurityMember.FaceValue, objAutoLoanSecurityMember.XTVRate, issueDate,
                        objAutoLoanSecurityMember.InterestRate, objAutoLoanSecurityMember.FacilityType, objAutoLoanSecurityMember.Status, objAutoLoanSecurityMember.FacilityId);

                objDbHelper.ExecuteNonQuery(sql);
                
            }
            catch (Exception ex)
            {
                objDbHelper.RollBack();
                throw ex;
            }
        }

        private void SaveAutoLoanFacilityOffSCB(AutoLoanFacilityOffSCB afscb, CommonDbHelper objDbHelper)
        {
            //string theDate = al.IssueDate.Date.ToString("yyyy-MM-dd");
            string sql =
                string.Format(
                    @"insert into auto_loanfacilityoffscb 
                                (AnalystMasterId, AutoLoanMasterId, BankID, 
                                OriginalLimit, EMI, EMIPercent, EMIShare, Status, Consider, 
                                Type, InterestRate)
                                values
                                ({0}, {1}, {2}, 
                                {3}, {4}, {5}, {6}, {7}, {8}, 
                                {9}, {10})",
                    afscb.AnalystMasterId, afscb.AutoLoanMasterId, afscb.BankID, afscb.OriginalLimit, afscb.EMI,
                    afscb.EMIPercent, afscb.EMIShare,
                    afscb.Status, afscb.Consider, afscb.Type, afscb.InterestRate);

            objDbHelper.ExecuteNonQuery(sql);
        }




    }
}
