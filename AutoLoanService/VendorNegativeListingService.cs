﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace AutoLoanService
{
   public  class VendorNegativeListingService
    {

       private IVendorNegativeListingRepository repository { get; set; }

        public VendorNegativeListingService()
        {

        }

        public VendorNegativeListingService(IVendorNegativeListingRepository repository)
        {
            this.repository = repository;
        }

        public List<AutoVendorNegativeListing> GetAutoVendorNegativeList(int pageNo, int pageSize, string search)
        {
            return repository.GetAutoVendorNegativeList(pageNo, pageSize, search);
        }

        public string VendorNegativeById(int vendornegativeID)
        {
            return repository.VendorNegativeById(vendornegativeID);
        }

        public string SaveAuto_NegativeVendor(int manufacturerId)
        {
            return repository.SaveAuto_NegativeVendor(manufacturerId);
        }

     
    }
}
