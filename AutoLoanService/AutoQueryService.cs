﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;
namespace AutoLoanService
{
   public class AutoQueryService
    {
       private IQueryRepository repository { get; set; }
       public  AutoQueryService(){}
       public  AutoQueryService(IQueryRepository repository)
       {
           this.repository = repository;
       }

       public List<Department_Entity> GetAll_AT_Department()
       {
           return repository.GetAll_AT_Department();
       }

       public QueryTrackerDetails_Entity SearchQueryDetailsByLLID(int pageNo, int pageSize, int searchKey)
       {
           return repository.SearchQueryDetailsByLLID(pageNo,pageSize,searchKey);
       }
       public List<AutoStatusHistory> SearchApplicationStatusByLLID(int searchKey)
       {
           return repository.SearchApplicationStatusByLLID(searchKey);
       }
       public List<QueryTracker_Entity> GetQueryTrackerListByLLID(int userId, int usLeId, int pageNo, int pageSize, int searchKey)
       {
           return repository.GetQueryTrackerListByLLID(userId,usLeId, pageNo, pageSize, searchKey);
       }
       public List<QueryTracker_Entity> GetQueryTrackerListByReadUnreadStatus(int userId, int usLeId, int pageNo, int pageSize, int readUnreadStatus)
       {
           return repository.GetQueryTrackerListByReadUnreadStatus(userId, usLeId, pageNo, pageSize, readUnreadStatus);
       }
       public string SaveNewQuery(QueryTracker_Entity objNewQuery)
       {
           return repository.SaveNewQuery(objNewQuery);
       }
       public string ReplyOnQuery(QueryTracker_Entity objReplyOnQuery)
       {
           return repository.ReplyOnQuery(objReplyOnQuery);
       }

       public string UpdateStatusAsReadQueryByTrackerId(int trackerId)
       {
           return repository.UpdateStatusAsReadQueryByTrackerId(trackerId);
       }
    }
}
