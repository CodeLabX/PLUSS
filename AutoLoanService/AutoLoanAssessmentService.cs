﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoLoanAssessmentService
    {
        private AutoLoanAssessmentRrepository repository { get; set; }

        public AutoLoanAssessmentService()
        {

        }

        public AutoLoanAssessmentService(AutoLoanAssessmentRrepository repository)
        {
            this.repository = repository;
        }


        public List<PlussSector> GetAllPlussSector()
        {
            return repository.GetAllPlussSector();
        }

        public List<PlussSubSector> GetPlussSubSectorBySectorID(int SectorID)
        {
            return repository.GetPlussSubSectorBySectorID(SectorID);
        }

        public List<PlussIncomeAcesmentMethod> GetPlussIncomeAcesmentMethod()
        {
            return repository.GetPlussIncomeAcesmentMethod();
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize, int searchKey, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.getAutoLoanSummaryAll(pageNo, pageSize, searchKey, statePermission);
        }
       
        public List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize, int searchKey)
        {
            return repository.getAutoLoanSummaryAll(pageNo, pageSize, searchKey);
        }

        public string AppealedByAutoLoanMasterId(AutoStatusHistory objAutoStatusHistory)
        {
            return repository.AppealedByAutoLoanMasterId(objAutoStatusHistory);
        }

        public string AppealedByAutoLoanMasterId(AutoStatusHistory objAutoStatusHistory, string remark)
        {
            return repository.AppealedByAutoLoanMasterId(objAutoStatusHistory, remark);
        }

        public List<AutoPayrollRate> GetEmployerForSalaried()
        {
            return repository.GetEmployerForSalaried();
        }

        public List<AutoSegmentSettings> GetEmployeSegment()
        {
            return repository.GetEmployeSegment();
        }

        public Object getLoanLocetorInfoByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.getLoanLocetorInfoByLLID(llid, statePermission);
        }

        public Object getLoanLocetorInfoByLLID(int llid)
        {
            return repository.getLoanLocetorInfoByLLID(llid);
        }


        public List<DaviationSettingsEntity> getDeviationByLevel(int deviationFor)
        {
            return repository.getDeviationByLevel(deviationFor);
        }

        public List<DaviationSettingsEntity> getDeviationLevel()
        {
            return repository.getDeviationLevel();
        }

        public string getDeviationCodeByID(int deviationID)
        {
            return repository.getDeviationCodeByID(deviationID);
        }

        public List<t_Pluss_Remark> getRemarkStrangthConditionWeaknessAll()
        {
            return repository.getRemarkStrangthConditionWeaknessAll();
        }

        public List<t_pluss_user> gett_pluss_userAll(int userId)
        {
            return repository.gett_pluss_userAll(userId);
        }

        //public string SaveAnalystPart(Auto_AnalystAll analystAll)
        //{
        //    return repository.SaveAnalystPart(analystAll);
        //}

        public BALetter_Entity getBaLetter(int appiledOnAutoLoanMasterId)
        {
            return repository.GetBALetter(appiledOnAutoLoanMasterId);
        }

        public string SaveAnalystLoanApplication(Auto_AnalystAll objAnalystAll, int stateId, int userId)
        {
            return repository.SaveAnalystLoanApplication(objAnalystAll, stateId, userId);
        }

        public string ChangeStateForAnalyst(AutoStatusHistory objAutoStatusHistory, string remark)
        {
            return repository.ChangeStateForAnalyst(objAutoStatusHistory, remark);
        }

        public AutoLoanVehicle CheckMouByVendoreId(int vendoreId)
        {
            return repository.CheckMouByVendoreId(vendoreId);
        }

        public t_pluss_userapproverlimit GetApproverLimitByUserId(int userId)
        {
            return repository.GetApproverLimitByUserId(userId);
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAllForApprover(int pageNo, int pageSize, int searchKey, List<AutoUserTypeVsWorkflow> statePermission, int userID)
        {
            return repository.getAutoLoanSummaryAllForApprover(pageNo, pageSize, searchKey, statePermission, userID);
        }

        public int CheckLLIDIsAuto(string llId)
        {
            return repository.CheckLLIDIsAuto(llId);
        }

        public List<auto_CibInfo> getCibInfo()
        {
            return repository.getCibInfo();
        }

        public List<AutoLoanSummary> getAutoLoanSummaryAllForAnalyst(int pageNo, int pageSize, string searchKey, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.getAutoLoanSummaryAllForAnalyst(pageNo, pageSize, searchKey, statePermission);
        }

        public AutoStatusHistory GetRejectInformation(int appiledOnAutoLoanMasterId)
        {
            return repository.GetRejectInformation(appiledOnAutoLoanMasterId);
        }

        public List<t_pluss_user> get_pluss_userAll()
        {
            return repository.get_pluss_userAll();
        }
    }
}
