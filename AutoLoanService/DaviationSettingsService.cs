﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class DaviationSettingsService
    {

        private DaviationSettingsRepository repository { get; set; }
        
        public DaviationSettingsService()
        {
        }
        public DaviationSettingsService(DaviationSettingsRepository repository)
        {
            this.repository = repository;
        }
        public List<DaviationSettingsEntity> GetDeviationSettingsSummary(int pageNo, int pageSize, string searchKey)
        {
            return repository.GetDeviationSettingsSummary(pageNo, pageSize, searchKey);
        }

        public string SaveDeviationSettings(DaviationSettingsEntity AutoDeviationSettings)
        {
            return repository.SaveDeviationSettings(AutoDeviationSettings);
        }
        public string InactiveSettingsById(int deviationId, int userId)
        {
            return repository.InactiveSettingsById(deviationId, userId);
        }



    }
}
