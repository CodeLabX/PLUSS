﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;


namespace AutoLoanService
{
    public class IRSettingsForPayrollService
    {
        private IRSettingsForPayrollRepository repository { get; set; }

        public IRSettingsForPayrollService()
        {

        }

        public IRSettingsForPayrollService(IRSettingsForPayrollRepository repository)
        {
            this.repository = repository;
        }

        public string DumpExcelFile(string excelFilePath)
        {
            return repository.DumpExcelFile(excelFilePath);
        }

        public List<AutoPayrollRate> GetPayRollSettingsSummary(int pageNo, int pageSize, string search)
        {
            return repository.GetPayRollSettingsSummary(pageNo, pageSize, search);
        }


        public string savePayroll(AutoPayrollRate autoPayrollRate)
        {
            return repository.savePayroll(autoPayrollRate);
        }



    }
}
