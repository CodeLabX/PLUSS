﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;


namespace AutoLoanService.Interface
{
    public class AutoVendorService
    {
        private IAutoVendorRepository repository { get; set; }

        public AutoVendorService()
        {

        }

        public AutoVendorService(IAutoVendorRepository repository)
        {
            this.repository = repository;
        }

        public List<AutoVendor> GetAutoVendor()
        {
            return repository.GetAutoVendor();
        }
        
    }

    
}
