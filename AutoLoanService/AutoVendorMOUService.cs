﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoVendorMOUService
    {
        private AutoVendorMOURepository repository { get; set; }

        public AutoVendorMOUService()
        {

        }

        public AutoVendorMOUService(AutoVendorMOURepository repository)
        {
            this.repository = repository;
        }

        public List<AutoVendorMOU> GetAutoVendorMOUSummary(int pageNo, int pageSize, string vendorName)
        {
            return repository.GetAutoVendorMOUSummary(pageNo, pageSize, vendorName);
        }

        public string SaveAuto_VendorMOU(AutoVendorMOU autoVendorMOU)
        {
            return repository.SaveAuto_VendorMOU(autoVendorMOU);
        }

        public string AutoVendorMOUInactiveById(int VendorMouId)
        {
            return repository.AutoVendorMOUInactiveById(VendorMouId);
        }

        public string DumpExcelFile_VendorMOU(string excelFilePath)
        {
            return repository.DumpExcelFile_VendorMOU(excelFilePath);
        }


        public List<AutoStatus> GetStatus(int statusID)
        {
            return repository.GetStatus(statusID);
        }


        public string SaveAuto_NegativeVendorNew(AutoVendorMOU autoVendorMOU)
        {
            return repository.SaveAuto_NegativeVendorNew(autoVendorMOU);
        }
    }
}
