﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoSegmentSettingsService
    {
        private AutoSegmentSettingsRepository repository { get; set; }


        public AutoSegmentSettingsService()
        {

        }
        public AutoSegmentSettingsService(AutoSegmentSettingsRepository repository)
        {
            this.repository = repository;
        }

        public List<AutoSegmentSettings> GetSegmentSettingsSummary(int pageNo, int pageSize)
        {
            return repository.GetSegmentSettingsSummary(pageNo, pageSize);
        }

        public object saveSegment(AutoSegmentSettings autoSegmentSettings, int segmentID)
        {
            return repository.saveSegment(autoSegmentSettings, segmentID);
        }

        public List<AutoProfession> GetProfession()
        {
            return repository.GetProfession();
        }

        public string InactiveSegmentById(int segmentId, int userId)
        {
            return repository.InactiveSegmentById(segmentId, userId);
        }
    }
}
