﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;
using Azolution.Common.DataService;

namespace AutoLoanService
{
    public class VehicleAndPriceSettingsService
    {
        private IVehicleAndPriceSettings repository { get; set; }

        public VehicleAndPriceSettingsService()
        {

        }

        public VehicleAndPriceSettingsService(IVehicleAndPriceSettings repository)
        {
            this.repository = repository;
        }

        public string DumpExcelFile(string excelFilePath, int uploadType)
        {
            var objDbHelper = new CommonDbHelper();
            var msg = "";
            //string strConn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFilePath + ";Extended Properties=Excel 12.0;HDR=YES;IMEX=1";

            string strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;
                                      Data Source=" + excelFilePath + @";Extended Properties=""Excel 8.0;HDR=NO;""";

            //const string CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='{0}';Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            OleDbConnection oConn = new OleDbConnection();
            oConn.ConnectionString = strConn;
            oConn.Open();
            OleDbCommand oSelect = new OleDbCommand("SELECT * FROM [Sheet1$]", oConn);
            var dataTable = new DataTable();
            dataTable.Load(oSelect.ExecuteReader());
            oConn.Close();
            objDbHelper.BeginTransaction();
            
            try
            {
                if (uploadType == 1)
                {
                    for (int i = 1; i < dataTable.Rows.Count; i++)
                    {
                        var objAutoMenufacturer = new AutoMenufacturer();
                        var objAutoVehicle = new AutoVehicle();
                        var objAutoPriceList = new List<AutoPrice>();
                        objAutoMenufacturer.ManufacturerName = Convert.ToString(dataTable.Rows[i][0]);
                        //New Code Done by Washik
                        objAutoMenufacturer.ManufacturerCode = Convert.ToString(dataTable.Rows[i][1]);
                        objAutoMenufacturer.ManufacturCountry = Convert.ToString(dataTable.Rows[i][2]);
                        objAutoMenufacturer.Remarks = Convert.ToString(dataTable.Rows[i][3]);
                        


                        //objAutoVehicle.Type = Convert.ToString(dataTable.Rows[i][1]);
                        //objAutoVehicle.Model = Convert.ToString(dataTable.Rows[i][2]);
                        objAutoVehicle.Type = Convert.ToString(dataTable.Rows[i][4]);
                        objAutoVehicle.Model = Convert.ToString(dataTable.Rows[i][5]);
                        objAutoVehicle.ModelCode = Convert.ToString(dataTable.Rows[i][6]);

                        
                        //objAutoVehicle.TrimLevel = Convert.ToString(dataTable.Rows[i][3]);
                        //if (Convert.ToString(dataTable.Rows[i][3]) != "")
                        //{
                        //    objAutoVehicle.CC = Convert.ToInt32(dataTable.Rows[i][3]);
                        //}
                        //else
                        //{
                        //    objAutoVehicle.CC = 0;
                        //}
                        if (Convert.ToString(dataTable.Rows[i][7]) != "")
                        {
                            objAutoVehicle.CC = Convert.ToInt32(dataTable.Rows[i][7]);
                        }
                        else
                        {
                            objAutoVehicle.CC = 0;
                        }
                        //objAutoVehicle.ChassisCode = Convert.ToString(dataTable.Rows[i][5]);

                        var objAutoPrice = new AutoPrice();
                        objAutoPrice.StatusId = uploadType;
                        //if (Convert.ToString(dataTable.Rows[i][6]) == "")
                        //{
                        //    objAutoPrice.ManufacturingYear = 0;
                        //}
                        //else
                        //{
                        //    objAutoPrice.ManufacturingYear = Convert.ToInt32(dataTable.Rows[i][6]);
                        //}
                        if (Convert.ToString(dataTable.Rows[i][10]) == "")
                        {
                            objAutoPrice.ManufacturingYear = 0;
                        }
                        else
                        {
                            objAutoPrice.ManufacturingYear = Convert.ToInt32(dataTable.Rows[i][10]);
                        }

                        //if (Convert.ToString(dataTable.Rows[i][4]) != "" && Convert.ToString(dataTable.Rows[i][4]) != "N/A")
                        //{
                        //    objAutoPrice.MinimumPrice = Convert.ToDouble(dataTable.Rows[i][4]);
                        //}
                        //else
                        //{
                        //    objAutoPrice.MinimumPrice = 0;
                        //}
                        if (Convert.ToString(dataTable.Rows[i][8]) != "" && Convert.ToString(dataTable.Rows[i][8]) != "N/A")
                        {
                            objAutoPrice.MinimumPrice = Convert.ToDouble(dataTable.Rows[i][8]);
                        }
                        else
                        {
                            objAutoPrice.MinimumPrice = 0;
                        }
                        //if (Convert.ToString(dataTable.Rows[i][5]) != "" && Convert.ToString(dataTable.Rows[i][5]) != "N/A")
                        //{
                        //    objAutoPrice.MaximumPrice = Convert.ToDouble(dataTable.Rows[i][5]);
                        //}
                        //else
                        //{
                        //    objAutoPrice.MaximumPrice = 0;
                        //}
                        if (Convert.ToString(dataTable.Rows[i][9]) != "" && Convert.ToString(dataTable.Rows[i][9]) != "N/A")
                        {
                            objAutoPrice.MaximumPrice = Convert.ToDouble(dataTable.Rows[i][9]);
                        }
                        else
                        {
                            objAutoPrice.MaximumPrice = 0;
                        }
                        objAutoPrice.IsActive = 1;
                        if ((objAutoPrice.MinimumPrice != 0 && objAutoPrice.MaximumPrice != 0) && objAutoPrice.ManufacturingYear != 0)
                        {
                            objAutoPriceList.Add(objAutoPrice);
                        }

                        repository.SaveVehicleAndPriceForImport(objAutoMenufacturer, objAutoVehicle, objAutoPriceList, objDbHelper);
                    }
                    msg = "'Success'";
                }
                else if (uploadType == 2)
                {
                    
                    for (int i = 2; i < dataTable.Rows.Count; i++)
                    {
                        var objAutoMenufacturer = new AutoMenufacturer();
                        var objAutoVehicle = new AutoVehicle();
                        var objAutoPriceList = new List<AutoPrice>();

                        
                        objAutoMenufacturer.ManufacturerName = Convert.ToString(dataTable.Rows[i][0]);
                        //New Code by Washik
                        objAutoMenufacturer.ManufacturerCode = Convert.ToString(dataTable.Rows[i][1]);
                        objAutoMenufacturer.ManufacturCountry = Convert.ToString(dataTable.Rows[i][2]);
                        objAutoMenufacturer.Remarks = Convert.ToString(dataTable.Rows[i][3]);
                        

                        

                        //objAutoVehicle.Type = Convert.ToString(dataTable.Rows[i][1]);
                        //objAutoVehicle.Model = Convert.ToString(dataTable.Rows[i][2]);
                        //objAutoVehicle.TrimLevel = Convert.ToString(dataTable.Rows[i][3]);

                        objAutoVehicle.Type = Convert.ToString(dataTable.Rows[i][4]);
                        objAutoVehicle.Model = Convert.ToString(dataTable.Rows[i][5]);
                        objAutoVehicle.ModelCode = Convert.ToString(dataTable.Rows[i][6]);
                        objAutoVehicle.TrimLevel = Convert.ToString(dataTable.Rows[i][7]);

                        //if (Convert.ToString(dataTable.Rows[i][4]) != "")
                        //{
                        //    objAutoVehicle.CC = Convert.ToInt32(dataTable.Rows[i][4]);
                        //}
                        //else
                        //{
                        //    objAutoVehicle.CC = 0;
                        //}
                        if (Convert.ToString(dataTable.Rows[i][8]) != "")
                        {
                            objAutoVehicle.CC = Convert.ToInt32(dataTable.Rows[i][8]);
                        }
                        else
                        {
                            objAutoVehicle.CC = 0;
                        }
                        //objAutoVehicle.ChassisCode = Convert.ToString(dataTable.Rows[i][5]);
                        objAutoVehicle.ChassisCode = Convert.ToString(dataTable.Rows[i][9]);
                        var columnCount = dataTable.Columns.Count;
                        var originalColumn = columnCount - 10;
                        for (var j = 0; j < originalColumn; j += 2)
                        {
                            var col = j + 10;
                            var objAutoPrice = new AutoPrice();
                            objAutoPrice.StatusId = uploadType;

                            if (Convert.ToString(dataTable.Rows[0][col]) == "")
                            {
                                objAutoPrice.ManufacturingYear = 0;
                            }
                            else
                            {
                                objAutoPrice.ManufacturingYear = Convert.ToInt32(dataTable.Rows[0][col]);
                            }
                            
                            if (Convert.ToString(dataTable.Rows[i][col]) != "")
                            {
                                objAutoPrice.MinimumPrice = Convert.ToDouble(dataTable.Rows[i][col]);
                            }
                            else
                            {
                                objAutoPrice.MinimumPrice = 0;
                            }
                            if (Convert.ToString(dataTable.Rows[i][col + 1]) != "")
                            {
                                objAutoPrice.MaximumPrice = Convert.ToDouble(dataTable.Rows[i][col + 1]);
                            }
                            else
                            {
                                objAutoPrice.MaximumPrice = 0;
                            }
                            objAutoPrice.IsActive = 1;
                            if ((objAutoPrice.MinimumPrice != 0 && objAutoPrice.MaximumPrice != 0) && objAutoPrice.ManufacturingYear != 0)
                            {
                                objAutoPriceList.Add(objAutoPrice);
                            }
                        }

                        repository.SaveVehicleAndPriceForImport(objAutoMenufacturer, objAutoVehicle, objAutoPriceList, objDbHelper);

                    }
                    
                    msg = "'Success'";

                }
                else
                {
                    msg = "'You select wrong file'";
                }
                objDbHelper.CommitTransaction();
            }
            catch (Exception exception)
            {
                msg = "'"+exception.Message+"'";
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return msg;
        }



        public List<AutoVehicle> GetVehicleSummary(int pageNo, int pageSize, string search)
        {
            return repository.GetVehicleSummary(pageNo, pageSize, search);
        }

        public List<AutoPrice> GetAutoPriceByVehicleId(int vehicleId)
        {
            return repository.GetAutoPriceByVehicleId(vehicleId);
        }

        public string SaveAutoPrice(AutoPrice autoPrice)
        {
            return repository.SaveAutoPrice(autoPrice);
        }

        public string SaveAutoVehicle(AutoVehicle autoVehicle)
        {
            return repository.SaveAutoVehicle(autoVehicle);
        }
    }
}
