﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoSCBStatusIRService
    {
        private AutoSCBStatusIRrepository repository { get; set; }

        public AutoSCBStatusIRService()
        {
        }


        public AutoSCBStatusIRService(AutoSCBStatusIRrepository repository)
        {
            this.repository = repository;
        }

        public List<AutoSCBStatusIR> GetAutoSCBStatusIRSummary(int pageNo, int pageSize)
        {
            return repository.GetAutoSCBStatusIRSummary(pageNo, pageSize);
        }

        public string SaveAuto_IRSCBStatus(AutoSCBStatusIR SCBStatusIR)
        {
            return repository.SaveAuto_IRSCBStatus(SCBStatusIR);
        }

        public string IRSCBStatusDeleteById(int IRSCBStatusID)
        {
            return repository.IRSCBStatusDeleteById(IRSCBStatusID);
        }

        public List<AutoStatus> GetStatus(int statusID)
        {
            return repository.GetStatus(statusID);
        }







    }
}
