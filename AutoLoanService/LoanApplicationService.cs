﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace AutoLoanService
{
    public class LoanApplicationService
    {
        private ILoanApplicationRepository repository { get; set; }

        public LoanApplicationService()
        {

        }

        public LoanApplicationService(ILoanApplicationRepository repository)
        {
            this.repository = repository;
        }

        public string SaveAutoLoanApplication(AutoLoanMaster objAutoLoanMaster, AutoPRApplicant objAutoPRApplicant, 
            AutoJTApplicant objAutoJTApplicant, AutoPRProfession objAutoPRProfession, 
            AutoJTProfession objAutoJTProfession, AutoLoanVehicle objAutoLoanVehicle, 
            AutoPRAccount objAutoPRAccount, AutoJTAccount objAutoJTAccount, 
            List<AutoPRAccountDetail> objAutoPRAccountDetailList, List<AutoJTAccountDetail> objAutoJtAccountDetailList, 
            AutoPRFinance objAutoPRFinance, AutoJTFinance objAutoJTFinance, AutoLoanReference objAutoLoanReference, 
            List<AutoLoanFacility> objAutoLoanFacilityList, List<AutoLoanSecurity> objAutoLoanSecurityList,
            AutoApplicationInsurance objAutoApplicationInsurance, AutoVendor objAutoVendor)//, AutoPRBankStatement objPRBankStatement, 
            //AutoJTBankStatement objJTBankStatement)
        {

            var autoLoanMaster = getAutoLoanMaster(objAutoLoanMaster);
            var autoPRApplicant = objAutoPRApplicant;
            var autoJTApplicant = objAutoJTApplicant;

            var autoPRProfession = getAutoPRProfession(objAutoPRProfession);
            var autoJTProfession = getAautoJTProfession(objAutoJTProfession);
            var autoLoanVehicle = getAutoLoanVehicle(objAutoLoanVehicle);
            var autoPRAccount = objAutoPRAccount;
            var autoJTAccount = objAutoJTAccount;
            List<AutoPRAccountDetail> autoPRAccountDetailList = objAutoPRAccountDetailList;
            List<AutoJTAccountDetail> autoJtAccountDetailList = objAutoJtAccountDetailList;
            var autoPRFinance = getAutoPRFinance(objAutoPRFinance);
            var autoJTFinance = getaAutoJTFinance(objAutoJTFinance);
            var autoLoanReference = objAutoLoanReference;
            List<AutoLoanFacility> autoLoanFacilityList = getAutoLoanFacilityList(objAutoLoanFacilityList);
            List<AutoLoanSecurity> autoLoanSecurityList = getAutoLoanSecurityList(objAutoLoanSecurityList);
            var autoApplicationInsurance = getAutoApplicationInsurance(objAutoApplicationInsurance);
            var autoVendor = objAutoVendor;

            return repository.SaveAutoLoanApplication(autoLoanMaster, autoPRApplicant, autoJTApplicant, autoPRProfession,
                autoJTProfession, autoLoanVehicle, autoPRAccount, autoJTAccount, autoPRAccountDetailList,
                autoJtAccountDetailList, autoPRFinance, autoJTFinance, autoLoanReference, autoLoanFacilityList,
                autoLoanSecurityList, autoApplicationInsurance, autoVendor);//, objPRBankStatement, objJTBankStatement);
        }

        private AutoLoanMaster getAutoLoanMaster(AutoLoanMaster objAutoLoanMaster)
        {
            var autoLoanMaster = new AutoLoanMaster();
            autoLoanMaster.Auto_LoanMasterId = objAutoLoanMaster.Auto_LoanMasterId;
            autoLoanMaster.LLID = objAutoLoanMaster.LLID;
            autoLoanMaster.ProductId = objAutoLoanMaster.ProductId;
            autoLoanMaster.Source = objAutoLoanMaster.Source;
            autoLoanMaster.SourceCode = objAutoLoanMaster.SourceCode;
            autoLoanMaster.SourceName = objAutoLoanMaster.SourceName;
            string thedate = objAutoLoanMaster.AppliedDate.ToShortDateString();
            //autoLoanMaster.AppliedDate = Convert.ToDateTime(thedate);
            autoLoanMaster.AppliedDate = objAutoLoanMaster.AppliedDate;
            autoLoanMaster.AppliedAmount = objAutoLoanMaster.AppliedAmount;
            autoLoanMaster.ReceiveDate = Convert.ToDateTime(objAutoLoanMaster.ReceiveDate.ToShortDateString());
            autoLoanMaster.AskingTenor = objAutoLoanMaster.AskingTenor;
            autoLoanMaster.JointApplication = objAutoLoanMaster.JointApplication;
            autoLoanMaster.StatusID = objAutoLoanMaster.StatusID;
            autoLoanMaster.UserID = objAutoLoanMaster.UserID;
            autoLoanMaster.AskingRate = objAutoLoanMaster.AskingRate;
            if (objAutoLoanMaster.ReferenceLLId == 0)
            {
            }
            else
            {
                autoLoanMaster.ReferenceLLId = objAutoLoanMaster.ReferenceLLId;
            }
            autoLoanMaster.ParentLLID = objAutoLoanMaster.ParentLLID;

            return autoLoanMaster;
        }

        private AutoPRProfession getAutoPRProfession(AutoPRProfession objAutoPRProfession)
        {
            var autoPRProfession = new AutoPRProfession();

            autoPRProfession.PrProfessionId = objAutoPRProfession.PrProfessionId;
            autoPRProfession.AutoLoanMasterId = objAutoPRProfession.AutoLoanMasterId;
            autoPRProfession.PrimaryProfession = objAutoPRProfession.PrimaryProfession;
            autoPRProfession.OtherProfession = objAutoPRProfession.OtherProfession;
            autoPRProfession.MonthinCurrentProfession = objAutoPRProfession.MonthinCurrentProfession;
            if (objAutoPRProfession.NameofCompany == -1)
            {
            }
            else
            {
                autoPRProfession.NameofCompany = objAutoPRProfession.NameofCompany;
            }

            autoPRProfession.NameofOrganization = objAutoPRProfession.NameofOrganization;
            autoPRProfession.Designation = objAutoPRProfession.Designation;
            autoPRProfession.NatureOfBussiness = objAutoPRProfession.NatureOfBussiness;
            autoPRProfession.YearsInBussiness = objAutoPRProfession.YearsInBussiness;
            autoPRProfession.OfficeStatus = objAutoPRProfession.OfficeStatus;
            autoPRProfession.Address = objAutoPRProfession.Address;
            autoPRProfession.OfficePhone = objAutoPRProfession.OfficePhone;
            autoPRProfession.OwnerShipType = objAutoPRProfession.OwnerShipType;
            autoPRProfession.EmploymentStatus = objAutoPRProfession.EmploymentStatus;
            autoPRProfession.TotalProfessionalExperience = objAutoPRProfession.TotalProfessionalExperience;
            if (objAutoPRProfession.NumberOfEmployees == 0)
            {

            }
            else
            {
                autoPRProfession.NumberOfEmployees = objAutoPRProfession.NumberOfEmployees;
            }
            if (objAutoPRProfession.EquityShare == 0)
            {
            }
            else
            {
                autoPRProfession.EquityShare = objAutoPRProfession.EquityShare;
            }

            autoPRProfession.PrIncomeSource = objAutoPRProfession.PrIncomeSource;
            autoPRProfession.OtherIncomeSource = objAutoPRProfession.OtherIncomeSource;
            if (objAutoPRProfession.NoOfFloorsRented == 0)
            {
            }
            else
            {
                autoPRProfession.NoOfFloorsRented = objAutoPRProfession.NoOfFloorsRented;
            }
            autoPRProfession.NatureOfrentedFloors = objAutoPRProfession.NatureOfrentedFloors;
            autoPRProfession.RentedAresInSFT = objAutoPRProfession.RentedAresInSFT;
            autoPRProfession.ConstructionCompletingYear = objAutoPRProfession.ConstructionCompletingYear;



            return autoPRProfession;
        }

        private AutoJTProfession getAautoJTProfession(AutoJTProfession objAutoJTProfession)
        {
            var autoJTProfession = new AutoJTProfession();

            autoJTProfession.JtProfessionId = objAutoJTProfession.JtProfessionId;
            autoJTProfession.AutoLoanMasterId = objAutoJTProfession.AutoLoanMasterId;
            autoJTProfession.PrimaryProfession = objAutoJTProfession.PrimaryProfession;
            autoJTProfession.OtherProfession = objAutoJTProfession.OtherProfession;
            autoJTProfession.MonthinCurrentProfession = objAutoJTProfession.MonthinCurrentProfession;
            if (objAutoJTProfession.NameofCompany == -1)
            {
            }
            else
            {
                autoJTProfession.NameofCompany = objAutoJTProfession.NameofCompany;
            }

            autoJTProfession.NameofOrganization = objAutoJTProfession.NameofOrganization;
            autoJTProfession.Designation = objAutoJTProfession.Designation;
            autoJTProfession.NatureOfBussiness = objAutoJTProfession.NatureOfBussiness;
            autoJTProfession.YearsInBussiness = objAutoJTProfession.YearsInBussiness;
            autoJTProfession.OfficeStatus = objAutoJTProfession.OfficeStatus;
            autoJTProfession.Address = objAutoJTProfession.Address;
            autoJTProfession.OfficePhone = objAutoJTProfession.OfficePhone;
            autoJTProfession.OwnerShipType = objAutoJTProfession.OwnerShipType;
            autoJTProfession.EmploymentStatus = objAutoJTProfession.EmploymentStatus;
            autoJTProfession.TotalProfessionalExperience = objAutoJTProfession.TotalProfessionalExperience;
            if (objAutoJTProfession.NumberOfEmployees == 0)
            {

            }
            else
            {
                autoJTProfession.NumberOfEmployees = objAutoJTProfession.NumberOfEmployees;
            }
            if (objAutoJTProfession.EquityShare == 0)
            {
            }
            else
            {
                autoJTProfession.EquityShare = objAutoJTProfession.EquityShare;
            }

            autoJTProfession.JtIncomeSource = objAutoJTProfession.JtIncomeSource;
            autoJTProfession.OtherIncomeSource = objAutoJTProfession.OtherIncomeSource;
            if (objAutoJTProfession.NoOfFloorsRented == 0)
            {
            }
            else
            {
                autoJTProfession.NoOfFloorsRented = objAutoJTProfession.NoOfFloorsRented;
            }
            autoJTProfession.NatureOfrentedFloors = objAutoJTProfession.NatureOfrentedFloors;
            autoJTProfession.RentedAresInSFT = objAutoJTProfession.RentedAresInSFT;
            autoJTProfession.ConstructionCompletingYear = objAutoJTProfession.ConstructionCompletingYear;

            return autoJTProfession;
        }

        private AutoLoanVehicle getAutoLoanVehicle(AutoLoanVehicle objAutoLoanVehicle)
        {
            var autoLoanVehicle = new AutoLoanVehicle();

            autoLoanVehicle.AutoLoanVehicleId = objAutoLoanVehicle.AutoLoanVehicleId;
            autoLoanVehicle.VendorId = objAutoLoanVehicle.VendorId;
            autoLoanVehicle.Auto_ManufactureId = objAutoLoanVehicle.Auto_ManufactureId;
            autoLoanVehicle.Model = objAutoLoanVehicle.Model;
            autoLoanVehicle.TrimLevel = objAutoLoanVehicle.TrimLevel;
            autoLoanVehicle.EngineCC = objAutoLoanVehicle.EngineCC;
            autoLoanVehicle.VehicleType = objAutoLoanVehicle.VehicleType;
            autoLoanVehicle.VehicleStatus = objAutoLoanVehicle.VehicleStatus;
            if (objAutoLoanVehicle.ManufacturingYear == 0)
            {
            }
            else
            {
            autoLoanVehicle.ManufacturingYear = objAutoLoanVehicle.ManufacturingYear;
            }
            if (objAutoLoanVehicle.QuotedPrice == 0)
            {
            }
            else
            {
            autoLoanVehicle.QuotedPrice = objAutoLoanVehicle.QuotedPrice;
            }
            if (objAutoLoanVehicle.VerifiedPrice == 0)
            {

            }
            else
            {
                autoLoanVehicle.VerifiedPrice = objAutoLoanVehicle.VerifiedPrice;
            }
            autoLoanVehicle.PriceConsideredBasedOn = objAutoLoanVehicle.PriceConsideredBasedOn;
            autoLoanVehicle.SeatingCapacity = objAutoLoanVehicle.SeatingCapacity;
            autoLoanVehicle.CarVerification = objAutoLoanVehicle.CarVerification;
            autoLoanVehicle.VehicleId = objAutoLoanVehicle.VehicleId;

            return autoLoanVehicle;
        }

        private AutoPRFinance getAutoPRFinance(AutoPRFinance objAutoPRFinance)
        {
            var autoPRFinance = new AutoPRFinance();

            autoPRFinance.PrFinanceId = objAutoPRFinance.PrFinanceId;
            autoPRFinance.AutoLoanMasterId = objAutoPRFinance.AutoLoanMasterId;
            autoPRFinance.DPISource = objAutoPRFinance.DPISource;
            autoPRFinance.DPIAmount = objAutoPRFinance.DPIAmount;
            autoPRFinance.DOISource = objAutoPRFinance.DOISource;
            if (objAutoPRFinance.DOIAmount == 0)
            {
            }
            else
            {
                autoPRFinance.DOIAmount = objAutoPRFinance.DOIAmount;
            }
            if (objAutoPRFinance.EDRentAndUtilities == 0)
            {
            }
            else
            {
                autoPRFinance.EDRentAndUtilities = objAutoPRFinance.EDRentAndUtilities;
            }
            if (objAutoPRFinance.EDFoodAndClothing == 0)
            {
            }
            else
            {
                autoPRFinance.EDFoodAndClothing = objAutoPRFinance.EDFoodAndClothing;
            }
            if (objAutoPRFinance.EDEducation == 0)
            {
            }
            else
            {
                autoPRFinance.EDEducation = objAutoPRFinance.EDEducation;
            }
            if (objAutoPRFinance.EDLoanRePayment == 0)
            {
            }
            else
            {
                autoPRFinance.EDLoanRePayment = objAutoPRFinance.EDLoanRePayment;
            }
            if (objAutoPRFinance.EDOther == 0)
            {
            }
            else
            {
                autoPRFinance.EDOther = objAutoPRFinance.EDOther;
            }


            autoPRFinance.RepaymentTo = objAutoPRFinance.RepaymentTo;
            autoPRFinance.PaymentFor = objAutoPRFinance.PaymentFor;

            return autoPRFinance;
        }

        private AutoJTFinance getaAutoJTFinance(AutoJTFinance objAutoJTFinance)
        {
            var autoJTFinance = new AutoJTFinance();

            autoJTFinance.JtFinanceId = objAutoJTFinance.JtFinanceId;
            autoJTFinance.AutoLoanMasterId = objAutoJTFinance.AutoLoanMasterId;
            autoJTFinance.DPISource = objAutoJTFinance.DPISource;
            autoJTFinance.DPIAmount = objAutoJTFinance.DPIAmount;
            autoJTFinance.DOISource = objAutoJTFinance.DOISource;
            if (objAutoJTFinance.DOIAmount == 0)
            {
            }
            else
            {
                autoJTFinance.DOIAmount = objAutoJTFinance.DOIAmount;
            }
            if (objAutoJTFinance.EDRentAndUtilities == 0)
            {
            }
            else
            {
                autoJTFinance.EDRentAndUtilities = objAutoJTFinance.EDRentAndUtilities;
            }
            if (objAutoJTFinance.EDFoodAndClothing == 0)
            {
            }
            else
            {
                autoJTFinance.EDFoodAndClothing = objAutoJTFinance.EDFoodAndClothing;
            }
            if (objAutoJTFinance.EDEducation == 0)
            {
            }
            else
            {
                autoJTFinance.EDEducation = objAutoJTFinance.EDEducation;
            }
            if (objAutoJTFinance.EDLoanRePayment == 0)
            {
            }
            else
            {
                autoJTFinance.EDLoanRePayment = objAutoJTFinance.EDLoanRePayment;
            }
            if (objAutoJTFinance.EDOther == 0)
            {
            }
            else
            {
                autoJTFinance.EDOther = objAutoJTFinance.EDOther;
            }


            autoJTFinance.RepaymentTo = objAutoJTFinance.RepaymentTo;
            autoJTFinance.PaymentFor = objAutoJTFinance.PaymentFor;

            return autoJTFinance;
        }

        private List<AutoLoanFacility> getAutoLoanFacilityList(List<AutoLoanFacility> objAutoLoanFacilityList)
        {
            List<AutoLoanFacility> autoLoanFacilityList = new List<AutoLoanFacility>();


            foreach (var member in objAutoLoanFacilityList)
            {
                var autoLoanFacilityObj = new AutoLoanFacility();
                autoLoanFacilityObj.FacilityId = member.FacilityId;
                autoLoanFacilityObj.AutoLoanMasterId = member.AutoLoanMasterId;
                autoLoanFacilityObj.FacilityType = member.FacilityType;
                autoLoanFacilityObj.InterestRate = member.InterestRate;
                if (member.PresentBalance == 0)
                {
                }
                else
                {
                    autoLoanFacilityObj.PresentBalance = member.PresentBalance;
                }
                if (member.PresentEMI == 0)
                {
                }
                else
                {
                    autoLoanFacilityObj.PresentEMI = member.PresentEMI;
                }
                if (member.PresentLimit == 0)
                {
                }
                else
                {
                    autoLoanFacilityObj.PresentLimit = member.PresentLimit;
                }
                if (member.ProposedLimit == 0)
                {
                }
                else
                {
                    autoLoanFacilityObj.ProposedLimit = member.ProposedLimit;
                }
                if (member.RepaymentAgrement == 0)
                {
                }
                else
                {
                    autoLoanFacilityObj.RepaymentAgrement = member.RepaymentAgrement;
                }

                autoLoanFacilityList.Add(autoLoanFacilityObj);
            }


            return autoLoanFacilityList;
        }

        private List<AutoLoanSecurity> getAutoLoanSecurityList(List<AutoLoanSecurity> objAutoLoanSecurityList)
        {
            List<AutoLoanSecurity> autoLoanSecurityList = new List<AutoLoanSecurity>();

            foreach (var member in objAutoLoanSecurityList)
            {

            var autoLoanSecurityObj = new AutoLoanSecurity();
                autoLoanSecurityObj.SecurityId = member.SecurityId;
                autoLoanSecurityObj.AutoLoanMasterId = member.AutoLoanMasterId;
                autoLoanSecurityObj.NatureOfSecurity = member.NatureOfSecurity;
                autoLoanSecurityObj.IssuingOffice = member.IssuingOffice;
                if (member.NatureOfSecurity == 0)
                {
                    
                }
                else
                {
                    autoLoanSecurityObj.NatureOfSecurity = member.NatureOfSecurity;
                }
                if(member.FacilityType == 0)
                {
                    
                }
                else
                {
                    autoLoanSecurityObj.FacilityType = member.FacilityType;
                }

                if (member.FaceValue == 0)
                {
                }
                else
                {
                autoLoanSecurityObj.FaceValue = member.FaceValue;
                }
                if(member.XTVRate == 0)
                {
                }
                else
                {
                autoLoanSecurityObj.XTVRate = member.XTVRate;
                }
                if (member.IssueDate.Year == 0001)
                {
                }
                else
                {
                autoLoanSecurityObj.IssueDate = member.IssueDate;
                }
                if (member.InterestRate == 0)
                {
                }
                else
                {
                autoLoanSecurityObj.InterestRate = member.InterestRate;
                }
                autoLoanSecurityObj.Status = member.Status;

                autoLoanSecurityList.Add(autoLoanSecurityObj);
            }


            return autoLoanSecurityList;
        }

        private AutoApplicationInsurance getAutoApplicationInsurance(AutoApplicationInsurance objAutoApplicationInsurance)
        {
            var autoApplicationInsurance = new AutoApplicationInsurance();

            autoApplicationInsurance.ApplicationInsuranceId = objAutoApplicationInsurance.ApplicationInsuranceId;
            autoApplicationInsurance.AutoLoanMasterId = objAutoApplicationInsurance.AutoLoanMasterId;
            if (objAutoApplicationInsurance.GeneralInsurance == -1)
            {
            }
            else
            {
                autoApplicationInsurance.GeneralInsurance = objAutoApplicationInsurance.GeneralInsurance;
            }
            if (objAutoApplicationInsurance.LifeInsurance == -1)
            {
            }
            else
            {
                autoApplicationInsurance.LifeInsurance = objAutoApplicationInsurance.LifeInsurance;
            }

            autoApplicationInsurance.InsuranceFinancewithLoan = objAutoApplicationInsurance.InsuranceFinancewithLoan;
            autoApplicationInsurance.InsuranceFinancewithLoanLife = objAutoApplicationInsurance.InsuranceFinancewithLoanLife;

            return autoApplicationInsurance;
        }



        public Object getLoanLocetorInfoByLLID(int llid)
        {
            return repository.getLoanLocetorInfoByLLID(llid);
        }

        public Object getLoanLocetorInfoByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.getLoanLocetorInfoByLLID(llid, statePermission);
        }

        public Object getLoanLocetorInfoByLLID(int llid, int userType)
        {
            return repository.getLoanLocetorInfoByLLID(llid, userType);
        }

        public List<AutoBank> GetBank(int status)
        {
            return repository.GetBank(status);
        }

        public List<AutoBranch> GetBranch(int bankID)
        {
            return repository.GetBranch(bankID);
        }

        public List<AutoVendor> GetVendor()
        {
            return repository.GetVendor();
        }
        public List<AutoVehicle> GetAutoModelByManufacturerID(int ManufacturerID)
        {
            return repository.GetAutoModelByManufacturerID(ManufacturerID);
        }


        public List<AutoVehicle> GetAutoTrimLevelByModel(int manufacturerID, string model)
        {
            return repository.GetAutoTrimLevelByModel(manufacturerID, model);
        }

        public List<AutoVehicle> GetAutoEngineCCByModel_TrimLevel(int manufacturerID, string model, string trimLevel)
        {
            return repository.GetAutoEngineCCByModel_TrimLevel(manufacturerID, model,trimLevel);
        }

        public List<AutoVehicle> GetAutoVehicleType(int manufacturerID, string model, string engineCC, string TrimLevel)
        {
            return repository.GetAutoVehicleType(manufacturerID, model, engineCC, TrimLevel);
        }

        public List<AutoCompany> getNameOfCompany()
        {
            return repository.getNameOfCompany();
        }

        public List<AutoPrice> getManufacturingYear(int manufacturerID, string model, string engineCC, string TrimLevel)
        {
            return repository.getManufacturingYear(manufacturerID, model, engineCC, TrimLevel);
        }

        public List<AutoPrice> GetPrice(int manufacturerID, string model, string engineCC, string TrimLevel, string manufacturingYear)
        {
            return repository.GetPrice(manufacturerID, model, engineCC, TrimLevel, manufacturingYear);
        }

        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            return repository.getGeneralInsurance();
        }

        public List<AutoLoanApplicationSource> GetAllSource()
        {
            return repository.GetAllSource();
        }

        public List<AutoPRBankStatement> GetPRBankStatementData(string llid)
        {
            return repository.GetPRBankStatementData(llid);
        }
        public List<AutoGetBankStatement> getBankStatement(string llid)
        {
            return repository.getBankStatement(llid);
        }

        public List<AutoJTBankStatement> GetJTBankStatementData(string llid)
        {
            return repository.GetJTBankStatementData(llid);
        }

        public Object getLoanLocetorInfoByLLIDForSales(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.getLoanLocetorInfoByLLIDForSales(llid, statePermission);
        }

        public List<AutoPrice> GetPriceByVehicleId(int vehicleId, int manufacturingYear, int vehicleStatus)
        {
            return repository.GetPriceByVehicleId(vehicleId, manufacturingYear, vehicleStatus);
        }

        public Object getLoanLocetorInfoByLLIDForSalesAndOps(int llid)
        {
            return repository.getLoanLocetorInfoByLLIDForSalesAndOps(llid);
        }

        public Object getLoanLocetorInfoByLLIDForOps(int llid, int userType)
        {
            return repository.getLoanLocetorInfoByLLIDForOps(llid, userType);
        }

        public Object getLoanLocetorInfoByLLIDForCISupport(int llId, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.getLoanLocetorInfoByLLIDForCISupport(llId, statePermission);
        }
    }
}
