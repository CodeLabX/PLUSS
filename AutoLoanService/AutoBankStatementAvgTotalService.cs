﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoBankStatementAvgTotalService
    {
        private Auto_BankStatementAvgTotalRepository repository { get; set; }

        public AutoBankStatementAvgTotalService()
        {

        }

        public AutoBankStatementAvgTotalService(Auto_BankStatementAvgTotalRepository repository)
        {
            this.repository = repository;
        }

    }
}
