﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using Azolution.Common.DataService;
using System.Data.OleDb;
using System.Data;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoBranchAndSourceService
    {
        private IAutobranchAndSource repository { get; set; }

        public AutoBranchAndSourceService()
        {
        }


        public AutoBranchAndSourceService(IAutobranchAndSource repository)
        {
            this.repository = repository;
        }


        public string DumpExcelFileForBranchAndService(string excelFilePath)
        {
            var objDbHelper = new CommonDbHelper();
            var msg = "";
            string strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;
                                      Data Source=" + excelFilePath + @";Extended Properties=""Excel 8.0;HDR=YES;""";
            OleDbConnection oConn = new OleDbConnection();
            oConn.ConnectionString = strConn;
            oConn.Open();
            OleDbCommand oSelect = new OleDbCommand("SELECT * FROM [Source$]", oConn);
            var dataTable = new DataTable();
            dataTable.Load(oSelect.ExecuteReader());
            oConn.Close();
            objDbHelper.BeginTransaction();
            try
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    var objAutoBranchWithSource = new AutoBranchWithSource();
                    objAutoBranchWithSource.AutoBranchName = Convert.ToString(dataTable.Rows[i][0]);
                    objAutoBranchWithSource.SourceName = Convert.ToString(dataTable.Rows[i][1]);
                    objAutoBranchWithSource.SourceCode = Convert.ToString(dataTable.Rows[i][2]);
                    repository.SaveAutoBranchWithSource(objAutoBranchWithSource, objDbHelper);
                }
                objDbHelper.CommitTransaction();
                msg = "Success";

            }
            catch (Exception exception)
            {
                msg = "'" + exception.Message + "'";
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return msg;
        }

        public List<AutoBranchWithSource> GetAllSourceNameWithBranch(int pageNo, int pageSize, string searchtext)
        {
            return repository.GetAllSourceNameWithBranch(pageNo, pageSize, searchtext);
        }

        public string SaveAutoBranchAndSource(AutoBranchWithSource sourceAndbranch)
        {
            return repository.SaveAutoBranchAndSource(sourceAndbranch);
        }

        public List<AutoBranchWithSource> GetAllbranchName()
        {
            return repository.GetAllbranchName();
        }

        public List<AutoBranchWithSource> GetAllAutoSourceByBranchId(int branchId)
        {
            return repository.GetAllAutoSourceByBranchId(branchId);
        }

        public List<AutoFacility> GetFacility()
        {
            return repository.GetFacility();
        }

        public string InactiveSourceById(int sourceId)
        {
            return repository.InactiveSourceById(sourceId);
        }
    }
}
