﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace AutoLoanService
{
    public class BankStatementService
    {
        private IBankStatementRepository repository { get; set; }

        public BankStatementService()
        {

        }

        public BankStatementService(IBankStatementRepository repository)
        {
            this.repository = repository;
        }

        public string SavePrimaryBankStatement(AutoPRBankStatement autoPRBankStatement, List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList, int oldBankID, string oldAccountNO)
        {
            return repository.SaveAutoPRBankStatement(autoPRBankStatement, auto_BankStatementAvgTotalList, oldBankID, oldAccountNO);
        }

        public string SaveJointBankStatement(AutoJTBankStatement AutojtBankStatement, List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList, int oldBankID, string oldAccountNO)
        {
            return repository.SaveAutoJTBankStatement(AutojtBankStatement, auto_BankStatementAvgTotalList, oldBankID, oldAccountNO);
        }

        public List<AutoPRBankStatement> GetPRBankStatementData(string llid)
        {
            return repository.GetPRBankStatementData(llid);
        }

        public List<AutoJTBankStatement> GetJTBankStatementData(string llid)
        {
            return repository.GetJTBankStatementData(llid);
        }

        public List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid)
        {
            return repository.SearchParantBankStatementInformationByLLID(llid);
        }
        public List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission)
        {
            return repository.SearchParantBankStatementInformationByLLID(llid, statePermission);
        }


        public string SavePrimaryBankStatement(AutoPRBankStatement autoPRBankStatement)
        {
            return repository.SaveAutoPRBankStatement(autoPRBankStatement);
        }

        public string SaveJointBankStatement(AutoJTBankStatement AutojtBankStatement)
        {
            return repository.SaveAutoJTBankStatement(AutojtBankStatement);
        }
    }
}
