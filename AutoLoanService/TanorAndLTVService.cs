﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;
using Azolution.Common.DataService;
using System.Data.OleDb;
using System.Data;

namespace AutoLoanService
{
    public class TanorAndLTVService
    {

        private ITanorAndLTV repository { get; set; }

        public TanorAndLTVService()
        {

        }

        public TanorAndLTVService(ITanorAndLTV repository)
        {
            this.repository = repository;
        }

        public List<AutoBrandTenorLTV> GetTenorList(int pageNo, int pageSize, string search)
        {
            return repository.GetTenorList(pageNo, pageSize, search);
        }


        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            return repository.GetAutoManufacturer();
        }
        public List<AutoVehicle> GetModelList(int manufacturerId)
        {
            return repository.GetAutoModel(manufacturerId);
        }

        public string SaveAuto_TenorAndLTV(AutoBrandTenorLTV tenor)
        {
            return repository.SaveAuto_TenorAndLTV(tenor);
        }


        public string TanorLtvDeleteById(int tanorLtvID)
        {
            return repository.TanorLtvDeleteById(tanorLtvID);
        }




        public AutoBrandTenorLTV getvaluepackallowed(int manufacturerID, int vehicleId, int vehicleStatus)
        {
            return repository.getvaluepackallowed(manufacturerID, vehicleId, vehicleStatus);
        }

        public string DumpExcelFile_ForTenorAndLTV(string excelFilePath)
        {
            
            var objDbHelper = new CommonDbHelper();
            var msg = "";
            string strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;
                                      Data Source=" + excelFilePath + @";Extended Properties=""Excel 8.0;HDR=YES;""";
            OleDbConnection oConn = new OleDbConnection();
            oConn.ConnectionString = strConn;
            oConn.Open();
            OleDbCommand oSelect = new OleDbCommand("SELECT * FROM [Sheet1$]", oConn);
            var dataTable = new DataTable();
            dataTable.Load(oSelect.ExecuteReader());
            oConn.Close();
            objDbHelper.BeginTransaction();
            try
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    var objAutoBrandTenorLTV = new AutoBrandTenorLTV();
                    objAutoBrandTenorLTV.ManufacturerName = Convert.ToString(dataTable.Rows[i][0]);
                    objAutoBrandTenorLTV.Model = Convert.ToString(dataTable.Rows[i][1]);
                    objAutoBrandTenorLTV.TrimLevel = Convert.ToString(dataTable.Rows[i][2]);
                    if (dataTable.Rows[i][3].ToString() == "")
                    {
                        objAutoBrandTenorLTV.VehicleCC = 0;
                    }
                    else
                    {
                        objAutoBrandTenorLTV.VehicleCC = Convert.ToInt32(dataTable.Rows[i][3]);
                    }
                    objAutoBrandTenorLTV.ChassisCode = Convert.ToString(dataTable.Rows[i][4]);
                    objAutoBrandTenorLTV.VehicleType = Convert.ToString(dataTable.Rows[i][5]);
                    objAutoBrandTenorLTV.StatusName = Convert.ToString(dataTable.Rows[i][6]);

                    switch(objAutoBrandTenorLTV.StatusName){
                        case "New Car":
                        objAutoBrandTenorLTV.StatusId = 1;
                        break;
                        case "Recondition Car":
                        objAutoBrandTenorLTV.StatusId = 2;
                        break;
                        case "Used Car":
                        objAutoBrandTenorLTV.StatusId = 3;
                        break;
                    }
                    objAutoBrandTenorLTV.Tenor = Convert.ToInt32(dataTable.Rows[i][7]);
                    objAutoBrandTenorLTV.LTV = Convert.ToInt32(dataTable.Rows[i][8]);
                    if (Convert.ToString(dataTable.Rows[i][9]) == "Yes")
                    {
                        objAutoBrandTenorLTV.ValuePackAllowed = 1;
                    }
                    else {
                        objAutoBrandTenorLTV.ValuePackAllowed = 0;
                    }

                    var res = repository.UploadTenorLtv(objAutoBrandTenorLTV, objDbHelper);
                    if (res == "Failed") {
                        msg = "Partial Success";
                    }
                }
                objDbHelper.CommitTransaction();
                if (msg == "")
                {
                    msg = "Success";
                }

            }
            catch (Exception exception)
            {
                msg = "'" + exception.Message + "'";
                objDbHelper.RollBack();
            }
            finally
            {
                objDbHelper.Close();
            }
            return msg;
        }
    }
}
