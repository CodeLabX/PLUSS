﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.DataService;

namespace AutoLoanService.Interface
{
    public interface IAutobranchAndSource
    {
        void SaveAutoBranchWithSource(AutoBranchWithSource objAutoBranchWithSource, CommonDbHelper objDbHelper);

        List<AutoBranchWithSource> GetAllSourceNameWithBranch(int pageNo, int pageSize, string searchtext);

        string SaveAutoBranchAndSource(AutoBranchWithSource sourceAndbranch);

        List<AutoBranchWithSource> GetAllbranchName();

        List<AutoBranchWithSource> GetAllAutoSourceByBranchId(int branchId);

        List<AutoFacility> GetFacility();

        string InactiveSourceById(int sourceId);
    }
}
