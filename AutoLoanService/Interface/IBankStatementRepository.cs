﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface IBankStatementRepository
    {

        string SaveAutoPRBankStatement(AutoPRBankStatement autoPRBankStatement, List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList, int oldBankID, string oldAccountNO);

        string SaveAutoJTBankStatement(AutoJTBankStatement AutojtBankStatement, List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList, int oldBankID, string oldAccountNO);

        List<AutoPRBankStatement> GetPRBankStatementData(string llid);

        List<AutoJTBankStatement> GetJTBankStatementData(string llid);

        List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid);

        List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission);

        string SaveAutoPRBankStatement(AutoPRBankStatement autoPRBankStatement);

        string SaveAutoJTBankStatement(AutoJTBankStatement AutojtBankStatement);
    }
}
