﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.DataService;



namespace AutoLoanService.Interface
{
    public interface ITanorAndLTV
    {
        List<AutoBrandTenorLTV> GetTenorList(int pageNo, int pageSize, string search);

        List<AutoMenufacturer> GetAutoManufacturer();
        List<AutoVehicle> GetAutoModel(int manufacturerID);


        string SaveAuto_TenorAndLTV(AutoBrandTenorLTV tenor);

        AutoBrandTenorLTV getvaluepackallowed(int manufacturerID, int vehicleId, int vehicleStatus);

        string UploadTenorLtv(AutoBrandTenorLTV objAutoBrandTenorLTV, CommonDbHelper objDbHelper);

        string TanorLtvDeleteById(int tanorLtvID);
    }
    
}
