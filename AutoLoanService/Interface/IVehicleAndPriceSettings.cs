﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.DataService;

namespace AutoLoanService.Interface
{
    public interface IVehicleAndPriceSettings
    {

        void SaveVehicleAndPriceForImport(AutoMenufacturer objAutoMenufacturer, AutoVehicle objAutoVehicle, List<AutoPrice> objAutoPriceList, CommonDbHelper objDbHelper);

        List<AutoVehicle> GetVehicleSummary(int pageNo, int pageSize, string search);

        List<AutoPrice> GetAutoPriceByVehicleId(int vehicleId);

        string SaveAutoPrice(AutoPrice autoPrice);

        string SaveAutoVehicle(AutoVehicle autoVehicle);
    }
}
