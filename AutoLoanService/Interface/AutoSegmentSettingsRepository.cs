﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface AutoSegmentSettingsRepository
    {
        List<AutoSegmentSettings> GetSegmentSettingsSummary(int pageNo, int pageSize);

        string saveSegment(AutoSegmentSettings autoSegmentSettings, int segmentID);

        List<AutoProfession> GetProfession();

        string InactiveSegmentById(int segmentId, int userId);
    }
}
