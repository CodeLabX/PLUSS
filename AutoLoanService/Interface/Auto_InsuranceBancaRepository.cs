﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface Auto_InsuranceBancaRepository
    {

        List<Auto_InsuranceBanca> GetInsuranceAndBancaSettingsSummary(int pageNo, int pageSize, string search);

        string SaveAuto_InsuranceBanca(Auto_InsuranceBanca insurance);

        string InsuranceDeleteById(int insurenceId);

        string SaveAutoInsurenceCalculationsettings(List<AutoOwnDamage> objOwnDamageList, AutoOthercharge objOtherCharge);

        List<AutoOwnDamage> GetOwnDamage();

        AutoOthercharge GetOtherDamage();
    }
}
