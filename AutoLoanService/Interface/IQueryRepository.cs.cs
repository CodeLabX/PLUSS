﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
   public interface IQueryRepository
   {
       List<Department_Entity> GetAll_AT_Department();
       QueryTrackerDetails_Entity SearchQueryDetailsByLLID(int pageNo, int pageSize, int searchKey);
       List<AutoStatusHistory> SearchApplicationStatusByLLID(int searchKey);
       List<QueryTracker_Entity> GetQueryTrackerListByLLID(int userId, int usLeId, int pageNo, int pageSize, int searchKey);
       List<QueryTracker_Entity> GetQueryTrackerListByReadUnreadStatus(int userId, int usLeId, int pageNo, int pageSize, int readUnreadStatus);
       string SaveNewQuery(QueryTracker_Entity objNewQuery);
       string ReplyOnQuery(QueryTracker_Entity objReplyOnQuery);
       string UpdateStatusAsReadQueryByTrackerId(int trackerId);
   }
   
}
