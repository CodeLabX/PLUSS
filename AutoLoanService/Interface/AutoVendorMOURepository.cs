﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface AutoVendorMOURepository
    {
        List<AutoVendorMOU> GetAutoVendorMOUSummary(int pageNo, int pageSize, string vendorName);

        string SaveAuto_VendorMOU(AutoVendorMOU autoVendorMOU);
        string DumpExcelFile_VendorMOU(string excelFilePath);
        string AutoVendorMOUInactiveById(int AutoVendorId);
        List<AutoStatus> GetStatus(int statusID);

        string SaveAuto_NegativeVendorNew(AutoVendorMOU autoVendorMOU);
    }
}
