﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface IAutoVendorRepository
    {
        List<AutoVendor> GetAutoVendor();
    }
}
