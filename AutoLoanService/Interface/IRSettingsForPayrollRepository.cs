﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface IRSettingsForPayrollRepository
    {

        string DumpExcelFile(string excelFilePath);
        List<AutoPayrollRate> GetPayRollSettingsSummary(int pageNo, int pageSize, string search);
        string savePayroll(AutoPayrollRate autoPayrollRate);
    }
}
