﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
namespace AutoLoanService.Interface
{
    public interface AutoLoanAssessmentRrepository
    {
        List<PlussSector> GetAllPlussSector();

        List<PlussSubSector> GetPlussSubSectorBySectorID(int SectorID);

        List<PlussIncomeAcesmentMethod> GetPlussIncomeAcesmentMethod();

        List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize);

        string AppealedByAutoLoanMasterId(AutoStatusHistory objAutoStatusHistory);

        string AppealedByAutoLoanMasterId(AutoStatusHistory objAutoStatusHistory, string remark);

        List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize, int searchKey,
                                                    List<AutoUserTypeVsWorkflow> statePermission);

        List<AutoLoanSummary> getAutoLoanSummaryAll(int pageNo, int pageSize, int searchKey);

        List<AutoPayrollRate> GetEmployerForSalaried();

        List<AutoSegmentSettings> GetEmployeSegment();

        Object getLoanLocetorInfoByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission);

        Object getLoanLocetorInfoByLLID(int llid);

        List<DaviationSettingsEntity> getDeviationByLevel(int deviationFor);

        List<DaviationSettingsEntity> getDeviationLevel();

        string getDeviationCodeByID(int deviationID);

        List<t_Pluss_Remark> getRemarkStrangthConditionWeaknessAll();

        List<t_pluss_user> gett_pluss_userAll(int userId);

        //string SaveAnalystPart(Auto_AnalystAll analystAll);

        BALetter_Entity GetBALetter(int appiledOnAutoLoanMasterId);

        string SaveAnalystLoanApplication(Auto_AnalystAll objAnalystAll, int stateId, int userId);

        string ChangeStateForAnalyst(AutoStatusHistory objAutoStatusHistory, string remark);

        AutoLoanVehicle CheckMouByVendoreId(int vendoreId);

        t_pluss_userapproverlimit GetApproverLimitByUserId(int userId);

        List<AutoLoanSummary> getAutoLoanSummaryAllForApprover(int pageNo, int pageSize, int searchKey, List<AutoUserTypeVsWorkflow> statePermission, int userID);

        int CheckLLIDIsAuto(string llId);

        List<auto_CibInfo> getCibInfo();

        List<AutoLoanSummary> getAutoLoanSummaryAllForAnalyst(int pageNo, int pageSize, string searchKey, List<AutoUserTypeVsWorkflow> statePermission);

        AutoStatusHistory GetRejectInformation(int appiledOnAutoLoanMasterId);

        List<t_pluss_user> get_pluss_userAll();
    }
}
