﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface DaviationSettingsRepository
    {
        List<DaviationSettingsEntity> GetDeviationSettingsSummary(int pageNo, int pageSize, string searchKey);

        //string SaveAuto_IRVehicleStatus(AutoVehicleStatusIR insurance);

        //string IRVehicleStatusDeleteById(int IRVehicleStatusID);
        //List<AutoStatus> GetStatus(int statusID);

        string SaveDeviationSettings(DaviationSettingsEntity AutoDeviationSettings);

        DaviationSettingsEntity GetDeviationbyCode(string deviationCode);

        string InactiveSettingsById(int deviationId, int userId);
    }
}
