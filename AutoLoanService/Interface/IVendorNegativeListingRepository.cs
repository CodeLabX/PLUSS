﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface IVendorNegativeListingRepository
    {
        List<AutoVendorNegativeListing> GetAutoVendorNegativeList(int pageNo, int pageSize, string search);
        string VendorNegativeById(int vendornegativeID);
        string SaveAuto_NegativeVendor(int autoVendorId);

    }

   
}
