﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface AutoSCBStatusIRrepository
    {

        List<AutoSCBStatusIR> GetAutoSCBStatusIRSummary(int pageNo, int pageSize);

        string SaveAuto_IRSCBStatus(AutoSCBStatusIR scbStatus);

        string IRSCBStatusDeleteById(int IRSCBStatusID);
        List<AutoStatus> GetStatus(int statusID);


    }
}
