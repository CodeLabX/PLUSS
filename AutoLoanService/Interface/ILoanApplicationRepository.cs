﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface ILoanApplicationRepository
    {
        string SaveAutoLoanApplication(AutoLoanMaster objAutoLoanMaster, AutoPRApplicant objAutoPRApplicant, AutoJTApplicant objAutoJTApplicant,
            AutoPRProfession objAutoPRProfession, AutoJTProfession objAutoJTProfession, AutoLoanVehicle objAutoLoanVehicle,
            AutoPRAccount objAutoPRAccount, AutoJTAccount objAutoJTAccount, List<AutoPRAccountDetail> objAutoPRAccountDetailList,
            List<AutoJTAccountDetail> objAutoJtAccountDetailList, AutoPRFinance objAutoPRFinance, AutoJTFinance objAutoJTFinance,
            AutoLoanReference objAutoLoanReference, List<AutoLoanFacility> objAutoLoanFacilityList, List<AutoLoanSecurity> objAutoLoanSecurityList,
            AutoApplicationInsurance objAutoApplicationInsurance, AutoVendor objAutoVendor);//, AutoPRBankStatement objPRBankStatement, AutoJTBankStatement objJTBankStatement);

        Object getLoanLocetorInfoByLLID(int llid, int userType);

        Object getLoanLocetorInfoByLLID(int llid);

        List<AutoBank> GetBank(int status);

        List<AutoBranch> GetBranch(int bankID);

        List<AutoVendor> GetVendor();
        List<AutoVehicle> GetAutoModelByManufacturerID(int ManufacturerID);

        List<AutoVehicle> GetAutoTrimLevelByModel(int manufacturerID, string model);

        List<AutoVehicle> GetAutoEngineCCByModel_TrimLevel(int manufacturerID, string model, string trimLevel);

        List<AutoVehicle> GetAutoVehicleType(int manufacturerID, string model, string engineCC, string TrimLevel);

        List<AutoCompany> getNameOfCompany();

        List<AutoPrice> getManufacturingYear(int manufacturerID, string model, string engineCC, string TrimLevel);

        List<AutoPrice> GetPrice(int manufacturerID, string model, string engineCC, string TrimLevel, string manufacturingYear);

        List<Auto_InsuranceBanca> getGeneralInsurance();

        List<AutoLoanApplicationSource> GetAllSource();

        List<AutoPRBankStatement> GetPRBankStatementData(string llid);

        List<AutoJTBankStatement> GetJTBankStatementData(string llid);

        List<AutoGetBankStatement> getBankStatement(string llid);

        Object getLoanLocetorInfoByLLID(int llid, List<AutoUserTypeVsWorkflow> statePermission);

        Object getLoanLocetorInfoByLLIDForSales(int llid, List<AutoUserTypeVsWorkflow> statePermission);

        List<AutoPrice> GetPriceByVehicleId(int vehicleId, int manufacturingYear, int vehicleStatus);

        Object getLoanLocetorInfoByLLIDForSalesAndOps(int llid);

        Object getLoanLocetorInfoByLLIDForOps(int llid, int userType);

        Object getLoanLocetorInfoByLLIDForCISupport(int llId, List<AutoUserTypeVsWorkflow> statePermission);
    }
}
