﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;


namespace AutoLoanService.Interface
{
    public interface AutoVehicleStatusIRRepository
    {
        List<AutoVehicleStatusIR> GetAutoVehicleStatusIRSummary(int pageNo, int pageSize);

        string SaveAuto_IRVehicleStatus(AutoVehicleStatusIR insurance);

        string IRVehicleStatusDeleteById(int IRVehicleStatusID);
        List<AutoStatus> GetStatus(int statusID);

        List<AutoUserTypeVsWorkflow> LoadState(int userType);
    }
}
