﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;

namespace AutoLoanService.Interface
{
    public interface AutoLoanCISupportRepository
    {

        string SaveCISupportLoanApp(Auto_LoanAnalystMaster autoAnalistMaster,
            Auto_LoanAnalystApplication autoLoanAnalystApplication, Auto_LoanAnalystVehicleDetail autoLoanAnalystVehicleDetail,
            List<AutoApplicantSCBAccount> autoApplicantAccountSCBList, List<AutoPRAccountDetail> autoApplicantAccountOtherList,
            List<AutoLoanFacility> autoFacilityList, List<AutoLoanSecurity> autoSecurityList, List<AutoLoanFacilityOffSCB> oFFSCBFacilityList,
            int userID, AutoLoanVehicle objAutoLoanVehicle, AutoVendor objAutoVendor);



    }
}
