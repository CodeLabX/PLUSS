﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class AutoVehicleStatusIRService
    {
        private AutoVehicleStatusIRRepository repository { get; set; }
        
        public AutoVehicleStatusIRService()
        {
        }


        public AutoVehicleStatusIRService(AutoVehicleStatusIRRepository repository)
        {
            this.repository = repository;
        }

        public List<AutoVehicleStatusIR> GetAutoVehicleStatusIRSummary(int pageNo, int pageSize)
        {
            return repository.GetAutoVehicleStatusIRSummary(pageNo, pageSize);
        }

        public string SaveAuto_IRVehicleStatus(AutoVehicleStatusIR insurance)
        {
            return repository.SaveAuto_IRVehicleStatus(insurance);
        }

        public string IRVehicleStatusDeleteById(int IRVehicleStatusID)
        {
            return repository.IRVehicleStatusDeleteById(IRVehicleStatusID);
        }

        public List<AutoStatus> GetStatus(int statusID)
        {
            return repository.GetStatus(statusID);
        }




        public List<AutoUserTypeVsWorkflow> LoadState(int userType)
        {
            return repository.LoadState(userType);
        }
    }
}
