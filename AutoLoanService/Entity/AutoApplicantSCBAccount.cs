﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoApplicantSCBAccount
    {

        public int ApplicantAccountID { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int LLID { get; set; }
        public string AccountNumberSCB { get; set; }
        public int AccountStatus { get; set; }
        public int PriorityHolder { get; set; }

    }
}
