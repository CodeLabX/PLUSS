﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoBranchWithSource
    {
        public AutoBranchWithSource(){}

        public int AutoBranchId { get; set; }
        public string AutoBranchName { get; set; }

        public int AutoSourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceCode { get; set; }

        public int TotalCount { get; set; }
    }
}
