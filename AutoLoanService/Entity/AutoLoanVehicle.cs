﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoLoanVehicle
    {
        public AutoLoanVehicle()
       {
       }

        public int AutoLoanVehicleId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int VendorId { get; set; }
        public int Auto_ManufactureId { get; set; }

        public string Model { get; set; }
        public string TrimLevel { get; set; }
        public string EngineCC { get; set; }
        public string VehicleType { get; set; }

        public int VehicleStatus { get; set; }
        public int ManufacturingYear { get; set; }
        public double QuotedPrice { get; set; }
        public double VerifiedPrice { get; set; }
        public string PriceConsideredBasedOn { get; set; }
        public string SeatingCapacity { get; set; }
        public string CarVerification { get; set; }


        public string StatusCode { get; set; }
        public string ManufacturerCode { get; set; }
        public string ModelCode { get; set; }
        public string VehicleTypeCode { get; set; }
        public string VendorCode { get; set; }
        public int VendoreRelationshipStatus { get; set; }
        public bool ValuePackAllowed { get; set; }
        public int Tenor { get; set; }
        public int LTV { get; set; }
        public int VehicleId { get; set; }


    }
}
