﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class PlussSubSector
    {
        public int SUSE_ID { get; set; }
        public string SUSE_NAME { get; set; }
        public int SUSE_SECTOR_ID { get; set; }
        public string SUSE_CODE { get; set; }
        public string SUSE_INTERESTRATE { get; set; }
        public string SUSE_IRCODE { get; set; }
        public string SUSE_COLOR { get; set; }
        public int SUSE_STATUS { get; set; }




    }
}
