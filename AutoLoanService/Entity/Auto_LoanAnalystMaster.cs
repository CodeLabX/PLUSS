﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_LoanAnalystMaster
    {
        public int AnalystMasterId { get; set; }
        public int LlId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public Boolean IsJoint { get; set; }
        public string ReasonOfJoint { get; set; }

        public string AnalystRemark { get; set; }
        public string ApproverRemark { get; set; }
        public int Colleterization { get; set; }
        public int ColleterizationWith { get; set; }
        public int CibInfoId { get; set; }

        //extra Property
        public string ColleterizationName { get; set; }
        public string ColleterizationWithName { get; set; }


    }
}
