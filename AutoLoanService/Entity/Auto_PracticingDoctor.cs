﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
  public class Auto_PracticingDoctor
    {
        public int Id { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int BankStatementId { get; set; }
        public int Type { get; set; }
        public double TotalChamberIncome { get; set; }
        public double OtherOTIncome { get; set; }
        public double TotalIncome { get; set; }
        public int IncomeSumId { get; set; }
        public List<Auto_ChamberIncome> AutoChamberIncome { get; set; }

    }
}
