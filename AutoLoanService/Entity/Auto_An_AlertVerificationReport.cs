﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_AlertVerificationReport
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public string MaxAgeAllowed { get; set; }
        public string MaxAgeFound { get; set; }
        public string MaxAgeFlag { get; set; }
        public string MinAgeAllowed { get; set; }
        public string MinAgeFound { get; set; }
        public string MinAgeFlag { get; set; }
        public string MinIncomeAllowed { get; set; }
        public string MinIncomeFound { get; set; }
        public string MinIncomeFlag { get; set; }
        public string Location { get; set; }
        public string LocationCosidered { get; set; }
        public string Nationality { get; set; }
        public string NationalityConsidered { get; set; }
        public string CarAgeAtTenorEndAllowed { get; set; }
        public string CarAgeAtTenorEndFound { get; set; }
        public string CarAgeAtTenorEndFlag { get; set; }
        public string SeatingCapacityAllowed { get; set; }
        public string SeatingCapacityFound { get; set; }
        public string SeatingCapacityFlag { get; set; }
        public string CarVerification { get; set; }
        public string CarVerificationFlag { get; set; }
        public string HypothecationCheck { get; set; }
        public string HypothecationCheckFlag { get; set; }
        public string VendorStatus { get; set; }
        public string VendorFlag { get; set; }
        public string LoanAmountAllowed { get; set; }
        public string LoanAmountFound { get; set; }
        public string LoanAmountFlag { get; set; }
        public string BBMaxLimitAllowed { get; set; }
        public string BBMaxLimitFound { get; set; }
        public string BBMaxLimitFlag { get; set; }
        public string InterestRateAllowed { get; set; }
        public string InterestRateFound { get; set; }
        public string InterestRateFlag { get; set; }
        public string TenorAllowed { get; set; }
        public string TenorFound { get; set; }
        public string TenorFlag { get; set; }
        public string ARTAEnrollmentAllowed { get; set; }
        public string ARTAEnrollmentFound { get; set; }
        public string ARTAEnrollmentFlag { get; set; }
        public string LTVIncBancaAllowed { get; set; }
        public string LTVIncBancaFound { get; set; }
        public string LTVIncBancaFlag { get; set; }
        public string MaxLTVSpreadAllowed { get; set; }
        public string MaxLTVSpreadFound { get; set; }
        public string MaxLTVSpreadFlag { get; set; }
        public string DBRAllowed { get; set; }
        public string DBRFound { get; set; }
        public string DBRFlag { get; set; }
        public string TotalAutoLoanAllowed { get; set; }
        public string TotalAutoLoanFound { get; set; }
        public string TotalAutoLoanFlag { get; set; }
        public string ApproversDLAAllowed { get; set; }
        public string ApproversDLAFound { get; set; }
        public string ApproversDLAFlag { get; set; }
        public string CIBOptained { get; set; }
        public string CIBOptainedFlag { get; set; }

        public string JtMaxAgeAllowed { get; set; }
        public string JtMaxAgeFound { get; set; }
        public string JtMaxAgeFlag { get; set; }
        public string JtMinAgeAllowed { get; set; }
        public string JtMinAgeFound { get; set; }
        public string JtMinAgeFlag { get; set; }
        public string JtMinIncomeAllowed { get; set; }
        public string JtMinIncomeFound { get; set; }
        public string JtMinIncomeFlag { get; set; }


    }
}
