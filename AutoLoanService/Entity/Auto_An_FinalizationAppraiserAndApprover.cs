﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationAppraiserAndApprover
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int AppraisedByUserID { get; set; }
        public int AppraisedByUserCode { get; set; }
        public int SupportedByUserID { get; set; }
        public int SupportedByUserCode { get; set; }
        public int ApprovedByUserID { get; set; }
        public int ApprovedByUserCode { get; set; }
        public double AutoDLALimit { get; set; }
        public double TotalSecuredLimit { get; set; }








    }
}
