﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
  public  class BALetter_Entity
    {
        public int LlId { get; set; }
        public int AutoLoanMasterId { get; set; }
      public int AnalystMasterId { get; set; }
        public int AutoBranchId { get; set; }
        public string AutoBranchName { get; set; }
        public DateTime AppliedDate { get; set; }
        public double TotalLoanAmount { get; set; }
        public int ProductId { get; set; }
        public string ProdName { get; set; }
        public double MaxEMI { get; set; }
        public int ConsideredTenor { get; set; }
        public string ManufacturerName { get; set; }
        public double ConsideredRate { get; set; }
        public int ConsideredInMonth { get; set; }
        public string Model { get; set; }
        public string HypothecationCheck { get; set; }
        public int SecurityPDC { get; set; }
        public string AccountNumber { get; set; }
        public string SCBAccount { get; set; }

    }
}
