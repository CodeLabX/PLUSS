﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_LoanCalculationTenor
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public double ARTAAllowed { get; set; }
        public double VehicleAllowed { get; set; }
        public double AgeAllowed { get; set; }
        public double AskingTenor { get; set; }
        public double AppropriateTenor { get; set; }
        public double ConsideredTenor { get; set; }
        public double ConsideredInMonth { get; set; }
        public int Level { get; set; }


    }
}
