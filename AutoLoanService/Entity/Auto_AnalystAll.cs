﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_AnalystAll
    {
        public Auto_LoanAnalystMaster AutoAnalistMaster { get; set; }
        public Auto_LoanAnalystApplication AutoLoanAnalystApplication { get; set; }
        public Auto_LoanAnalystVehicleDetail AutoLoanAnalystVehicleDetail { get; set; }
        public List<AutoApplicantSCBAccount> AutoApplicantAccountScbList { get; set; }
        public List<AutoPRAccountDetail> AutoApplicantAccountOtherList { get; set; }
        public List<AutoLoanFacility> AutoFacilityList { get; set; }
        public List<AutoLoanSecurity> AutoSecurityList { get; set; }
        public List<AutoLoanFacilityOffSCB> OFfscbFacilityList { get; set; }

        public List<Auto_IncomeSalaried> Salaried { get; set; }
        public List<AutoLandLord> LandLord { get; set; }
        public List<Auto_AnalystIncomBankStatementSum> AutoAnalystIncomBankStatementSum { get; set; }

        public AutoLoanVehicle AutoLoanVehicle { get; set; }
        public AutoVendor AutoLoanVendore { get; set; }

        //Loan Calculator Tab
        public Auto_An_LoanCalculationTotal AutoAnLoanCalculationTotal { get; set; }
        public Auto_An_LoanCalculationInsurance AutoAnLoanCalculationInsurance { get; set; }
        public Auto_An_LoanCalculationAmount AutoAnLoanCalculationAmount { get; set; }
        public Auto_An_LoanCalculationInterest AutoAnLoanCalculationInterest { get; set; }
        public Auto_An_LoanCalculationTenor AutoAnLoanCalculationTenor { get; set; }
        public Auto_An_RepaymentCapability AutoAnRepaymentCapability { get; set; }
        public Auto_An_IncomeSegmentIncome AutoAnIncomeSegmentIncome { get; set; }
        public List<Auto_An_IncomeSegmentApplicantIncome> AutoAnIncomeSegmentApplicantIncomeList { get; set; }

        //Finalize Tab
        public List<Auto_An_FinalizationDeviation> AutoAnFinalizationDeviation { get; set; }
        public Auto_An_FinalizationDevFound AutoAnFinalizationDevFound { get; set; }
        public List<Auto_An_FinalizationSterength> AutoAnFinalizationSterength { get; set; }
        public List<Auto_An_FinalizationWeakness> AutoAnFinalizationWeakness { get; set; }
        public Auto_An_FinalizationRepaymentMethod AutoAnFinalizationRepaymentMethod { get; set; }
        public List<Auto_An_FinalizationCondition> AutoAnFinalizationCondition { get; set; }
        public List<Auto_An_FinalizationRemark> AutoAnFinalizationRemark { get; set; }
        public Auto_An_FinalizationReqDoc AutoAnFinalizationReqDoc { get; set; }
        public Auto_An_FinalizationVarificationReport AutoAnFinalizationVarificationReport { get; set; }
        public Auto_An_FinalizationRework AutoAnFinalizationRework { get; set; }
        public Auto_An_FinalizationAppraiserAndApprover AutoAnFinalizationAppraiserAndApprover { get; set; }
        public Auto_An_AlertVerificationReport AutoAnAlertVerificationReport { get; set; }
        public List<auto_An_CibRequestLoan> AutoCibrequestLoan { get; set; }
    }
}
