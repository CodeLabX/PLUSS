﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class t_pluss_user
    {
        public int USER_ID { get; set; }
        public string USER_CODE { get; set; }
        public string USER_NAME { get; set; }
        public string USER_DESIGNATION { get; set; }
        public string USER_EMAILADDRESS { get; set; }
        public string USER_LEVELCODE { get; set; }
        public string USER_PASSWORD { get; set; }
        public DateTime USER_CHANGDATE { get; set; }
        public int USER_CHANGBY { get; set; }
        public int USER_LEVEL { get; set; }
        public int USER_STATUS { get; set; }
        public DateTime USER_ENTRYDATE { get; set; }
        public int USER_USER_ID { get; set; }
        public int USER_LOGINSTATUS { get; set; }
        public int AUTO_USER_LEVEL { get; set; }

        


    }
}
