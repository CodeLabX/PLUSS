﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoJTAccountDetail
    {

        public AutoJTAccountDetail()
        {
        }

        public int JtAccountDetailsId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int BankId { get; set; }
        public int BranchId { get; set; }
        public string AccountNumber { get; set; }
        public string AccountCategory { get; set; }
        public string AccountType { get; set; }


    }
}
