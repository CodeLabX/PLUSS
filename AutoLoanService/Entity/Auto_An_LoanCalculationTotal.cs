﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_LoanCalculationTotal
    {

        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public double TotalLoanAmount { get; set; }
        public double LTVExcludingInsurance { get; set; }
        public double FBRExcludingInsurance { get; set; }
        public double LTVIncludingInsurance { get; set; }
        public double FBRIncludingInsurance { get; set; }
        public double EMR { get; set; }
        public double ExtraLTV { get; set; }
        public double ExtraDBR { get; set; }

    }
}
