﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoPRProfession
    {

       public AutoPRProfession()
       {
       }

        public int PrProfessionId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int PrimaryProfession { get; set; }
        public int OtherProfession { get; set; }
        public string MonthinCurrentProfession { get; set; }

        public int NameofCompany { get; set; }
        public string NameofOrganization { get; set; }
        public string Designation { get; set; }
        public string NatureOfBussiness { get; set; }

        public string YearsInBussiness { get; set; }
        public string OfficeStatus { get; set; }
        public string Address { get; set; }
        public string OfficePhone { get; set; }

        public string OwnerShipType { get; set; }
        public string EmploymentStatus { get; set; }
        public string TotalProfessionalExperience { get; set; }
        public int NumberOfEmployees { get; set; }

        public double EquityShare { get; set; }
        public string PrIncomeSource { get; set; }
        public string OtherIncomeSource { get; set; }
        public int NoOfFloorsRented { get; set; }

        public string NatureOfrentedFloors { get; set; }
        public string RentedAresInSFT { get; set; }
        public string ConstructionCompletingYear { get; set; }


        // Extra Property
        public string PrimaryProfessionName { get; set; }
        public string OtherProfessionName { get; set; }

    }
}
