﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoJTAccount
    {

        public AutoJTAccount()
        {
        }

        public int jtAccountId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public string AccountNumberForSCB { get; set; }
        public string CreditCardNumberForSCB { get; set; }

    }
}
