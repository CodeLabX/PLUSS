﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoLoanSecurity
    {
        public AutoLoanSecurity()
        {
        }

        public int SecurityId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int NatureOfSecurity { get; set; }
        public string IssuingOffice { get; set; }
        public double FaceValue { get; set; }
        public double XTVRate { get; set; }
        public DateTime IssueDate { get; set; }
        public double InterestRate { get; set; }
        public int FacilityType { get; set; }
        public int Status { get; set; }

        public int FacilityId { get; set; }

    }
}
