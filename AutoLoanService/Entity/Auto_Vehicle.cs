﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
   public class AutoVehicle
    {
       public AutoVehicle()
       {
       }

       public int VehicleId { get; set; }
       public int ManufacturerId { get; set; }
       public string Model { get; set; }
       public string Tanor { get; set; }

       public string TrimLevel { get; set; }
       public string Type { get; set; }
       public int CC { get; set; }
       public string ChassisCode { get; set; }

       public string ManufacturerName { get; set; }
       public string ManufacturCountry { get; set; }
       public string ManufacturerCode { get; set; }
        public string Remarks { get; set; }
       public int TotalCount { get; set; }
       public string ModelCode { get; set; }
       public int Tenor { get; set; }
       public double Ltv { get; set; }
      
    }
}
