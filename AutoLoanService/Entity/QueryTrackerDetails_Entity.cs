﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class QueryTrackerDetails_Entity
    {
        public int LlId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public string PrName { get; set; }
        public double AppliedAmount { get; set; }
        public DateTime AppliedDate { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string AskingTenor { get; set; }
        public int Totalcount { get; set; }
        public List<QueryTracker_Entity> QueryTrackerList { get; set; }

        //-----------Extra Property------------------------
        public string LoginUserName { get; set; }
        public int AutoUserTypeId { get; set; }

    }

}
