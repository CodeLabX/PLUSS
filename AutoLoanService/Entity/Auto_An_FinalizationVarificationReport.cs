﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationVarificationReport
    {

        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int CPVReport { get; set; }
        public int CarPriceVarificationReport { get; set; }
        public int StatementAuthentication { get; set; }
        public int CreditReport { get; set; }
        public int CarVerificationReport { get; set; }
        public int UsedCarValuationReport { get; set; }
        public int UsedCarDocs { get; set; }
        public int CardRepaymentHistory { get; set; }

    }
}
