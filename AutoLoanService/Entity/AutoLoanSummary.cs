﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoLoanSummary
    {

        public int Auto_LoanMasterId { get; set; }
        public int LLID { get; set; }
        public string PrName { get; set; }
        public string ProductName { get; set; }
        public string ManufacturerName { get; set; }
        public string Model { get; set; }
        public int AskingTenor { get; set; }
        public double AppliedAmount { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }

        //extra property
        public int TotalCount { get; set; }

    }
}
