﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class DaviationSettingsEntity
    {
        public DaviationSettingsEntity()
        {

        }

        public int DeviationId { get; set; }
        public int DeviationFor { get; set; }
        public string DeviationCode { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public DateTime LastUpdateDate { get; set; }

        ////Selected Property
        //public string StatusName { get; set; }
        public int TotalCount { get; set; }


    }
}
