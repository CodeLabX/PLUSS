﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    
    public class AutoStatusHistory
    {
        public int AutoLoanHistoryID { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int UserID { get; set; }
        public int StatusID { get; set; }
        public DateTime ActionDate { get; set; }

        //extra property
        public string StatusName { get; set; }
        public int Totalcount { get; set; }

    }
}
