﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class Department_Entity
    {
        public Department_Entity()
        {
        }

        public int UsLeId { get; set; }
        public string UsLeCode { get; set; }
        public string UsLeName { get; set; }
    }
}
