﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_SalaiedApplicantIncome
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int SalaryMode { get; set; }
        public int Employer { get; set; }
        public string EmployerCategory { get; set; }
        public int IncomeAssessmentID { get; set; }
        public string IncomeAssessmentCode { get; set; }
        public double GrossSalary { get; set; }
        public double NetSalary { get; set; }
        public int VariableSalary { get; set; }
        public double VariableSalaryPercent { get; set; }
        public double VariableSalaryAmount { get; set; }
        public int CarAllowance { get; set; }
        public double CarAllowanceAmount { get; set; }
        public double CarAllowancePercent { get; set; }
        public double CarAllowanceCalculatedAmount { get; set; }
        public int OwnHouseBenefit { get; set; }
        public double TotalIncome { get; set; }
        public int ApplicantType { get; set; }
        public string Remark { get; set; }

        public List<Auto_An_SalariedVariableSalaryMonthIncome> ObjSalariedVariableSalaryMonthIncomelist { get; set; }
    }
}
