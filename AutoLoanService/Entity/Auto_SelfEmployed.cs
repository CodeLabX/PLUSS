﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
   public class Auto_SelfEmployed
    {
       public int BankId { get; set; }
       public int BranchId{ get; set; }
       public string AccountNumber { get; set; }
       public int ApplicantTypeId { get; set; } // if 1= Pr, 2= Jt

       public Auto_PracticingDoctor AutoPracticingDoctor { get; set; }
       public List<Auto_ChamberIncome> AutoChamberIncome { get; set; }
      public Auto_TutioningTeacher AutoTutioningTeacher { get; set; }
    }
}
