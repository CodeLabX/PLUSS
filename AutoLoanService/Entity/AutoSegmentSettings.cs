﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoSegmentSettings
    {
        public AutoSegmentSettings()
        {
        }

        public int SegmentID { get; set; }
        public string SegmentCode { get; set; }
        public string SegmentName { get; set; }
        public string Description { get; set; }
        public int Profession { get; set; }
        public double MinLoanAmt { get; set; }
        public double MaxLoanAmt { get; set; }
        public int MinTanor { get; set; }
        public int MaxTanor { get; set; }
        public double IrWithARTA { get; set; }
        public double IrWithoutARTA { get; set; }
        public int MinAgeL1 { get; set; }
        public int MinAgeL2 { get; set; }
        public int MaxAgeL1 { get; set; }
        public int MaxAgeL2 { get; set; }
        public double LTVL1 { get; set; }
        public double LTVL2 { get; set; }
        public double ExperienceL1 { get; set; }
        public double ExperienceL2 { get; set; }
        public int AcRelationshipL1 { get; set; }
        public int AcRelationshipL2 { get; set; }
        public string CriteriaCode { get; set; }
        public string AssetCode { get; set; }
        public double MinDBRL1 { get; set; }
        public double  MinDBRL2 { get; set; }
        public double MaxDBRL2 { get; set; }
        public double DBRL3 { get; set; }
        public bool FieldVisit { get; set; }
        public bool Verification { get; set; }
        public bool LandTelephone { get; set; }
        public bool GuranteeReference { get; set; }
        public double PrimaryMinIncomeL1 { get; set; }
        public double PrimaryMinIncomeL2 { get; set; }
        public double JointMinIncomeL1 { get; set; }
        public double JointMinIncomeL2 { get; set; }
        public string Comments { get; set; }
        public int UserId { get; set; }
        public DateTime LastUpdateDate { get; set; }

        //Selected Property
        public string ProfName { get; set; }
        public int ProfID { get; set; }
        public int TotalCount { get; set; }

    }
}
