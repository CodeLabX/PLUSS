﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_LoanCalculationInsurance
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public double GeneralInsurance { get; set; }
        public double ARTA { get; set; }
        public double TotalInsurance { get; set; }
    }
}
