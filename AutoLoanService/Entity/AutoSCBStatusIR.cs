﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoSCBStatusIR
    {
        public AutoSCBStatusIR()
        {
        }

        public int SCBStatusIRId { get; set; }
        public int SCBStatus { get; set; }
        public double IrWithArta { get; set; }
        public double IrWithoutArta { get; set; }
        public DateTime LastUpdateDate { get; set; }

        //Selected Property
        public string StatusName { get; set; }
        public int TotalCount { get; set; }



    }
}
