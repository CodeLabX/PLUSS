﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
   public class Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB
    {
        public int Id { get; set; }
        public int PreviousRepaymentHistoryId { get; set; }
        public int BankId { get; set; }
        public int Type { get; set; }
        public double EMI { get; set; }
        public Boolean Consider { get; set; }
        public int AutoLoanMasterId { get; set; }

    }
}
