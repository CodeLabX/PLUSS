﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoPRBankStatement
    {
        public AutoPRBankStatement()
        {

        }

        public int PrBankStatementId { get; set; }
        public int LLID { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int BankId { get; set; }
        public int BranchId { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountCategory { get; set; }
        public string AccountType { get; set; }
        public string Statement { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime StatementDateFrom { get; set; }
        public DateTime StatementDateTo { get; set; }


        // Extra property
        public string BANK_NM { get; set; }
        public string BRAN_NAME { get; set; }

    }
}
