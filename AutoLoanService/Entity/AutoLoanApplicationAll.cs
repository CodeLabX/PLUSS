﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoLoanApplicationAll
    {

        public AutoLoanApplicationAll()
        {
        }

        public AutoLoanMaster objAutoLoanMaster { get; set; }
        public AutoPRApplicant objAutoPRApplicant { get; set; }
        public AutoJTApplicant objAutoJTApplicant { get; set; }
        public AutoPRProfession objAutoPRProfession { get; set; }
        public AutoJTProfession objAutoJTProfession { get; set; }
        public AutoLoanVehicle objAutoLoanVehicle { get; set; }
        public AutoApplicationInsurance objAutoApplicationInsurance { get; set; }
        public AutoPRAccount objAutoPRAccount { get; set; }
        public List<AutoPRAccount> objAutoPRAccountList { get; set; }
        public AutoJTAccount objAutoJTAccount { get; set; }
        public List<AutoPRAccountDetail> objAutoPRAccountDetailList { get; set; }
        public List<AutoJTAccountDetail> objAutoJtAccountDetailList { get; set; }
        public AutoPRFinance objAutoPRFinance { get; set; }
        public AutoJTFinance objAutoJTFinance { get; set; }
        public AutoLoanReference objAutoLoanReference { get; set; }
        public List<AutoLoanFacility> objAutoLoanFacilityList { get; set; }
        public List<AutoLoanSecurity> objAutoLoanSecurityList { get; set; }
        public AutoVendor objAutoVendor { get; set; }
        public AutoVendorMOU objAutoVendorMou { get; set; }
        public List<Auto_BankStatementAvgTotal> AutoBankStatementAvgTotalList { get; set; }
        public List<AutoJTBankStatement> AutoJTBankStatementList { get; set; }
        public List<AutoPRBankStatement> AutoPRBankStatementList { get; set; }
        public Auto_App_Step_ExecutionByUser objAuto_App_Step_ExecutionByUser { get; set; }


        // Analyst Part

        public Auto_LoanAnalystMaster AutoLoanAnalystMaster { get; set; }
        public Auto_LoanAnalystApplication AutoLoanAnalystApplication { get; set; }
        public Auto_LoanAnalystVehicleDetail AutoLoanAnalystVehicleDetail { get; set; }
        public List<AutoApplicantSCBAccount> AutoApplicantSCBAccountList { get; set; }
        public List<AutoLoanFacilityOffSCB> AutoLoanFacilityOffSCBList { get; set; }

        //Loan Calculator
        public List<Auto_An_IncomeSegmentApplicantIncome> Auto_An_IncomeSegmentApplicantIncomeList { get; set; }
        public Auto_An_IncomeSegmentIncome objAuto_An_IncomeSegmentIncome { get; set; }
        public Auto_An_RepaymentCapability objAuto_An_RepaymentCapability { get; set; }
        public Auto_An_LoanCalculationTenor objAuto_An_LoanCalculationTenor { get; set; }
        public Auto_An_LoanCalculationInterest objAuto_An_LoanCalculationInterest { get; set; }
        public Auto_An_LoanCalculationAmount objAuto_An_LoanCalculationAmount { get; set; }
        public Auto_An_LoanCalculationInsurance objAuto_An_LoanCalculationInsurance { get; set; }
        public Auto_An_LoanCalculationTotal objAuto_An_LoanCalculationTotal { get; set; }

        //Finalization Tab
        public List<Auto_An_FinalizationDeviation> Auto_An_FinalizationDeviationList { get; set; }
        public Auto_An_FinalizationDevFound objAuto_An_FinalizationDevFound { get; set; }
        public List<Auto_An_FinalizationSterength> Auto_An_FinalizationSterengthList { get; set; }
        public List<Auto_An_FinalizationWeakness> Auto_An_FinalizationWeaknessList { get; set; }
        public List<Auto_An_FinalizationCondition> Auto_An_FinalizationConditionList { get; set; }
        public List<Auto_An_FinalizationRemark> Auto_An_FinalizationRemarkList { get; set; }
        public Auto_An_FinalizationRepaymentMethod objAuto_An_FinalizationRepaymentMethod { get; set; }
        public Auto_An_FinalizationReqDoc objAuto_An_FinalizationReqDoc { get; set; }

        public Auto_An_FinalizationVarificationReport objAuto_An_FinalizationVarificationReport { get; set; }
        public Auto_An_FinalizationAppraiserAndApprover objAuto_An_FinalizationAppraiserAndApprover { get; set; }
        public Auto_An_AlertVerificationReport objAuto_An_AlertVerificationReport { get; set; }
        public Auto_An_FinalizationRework objAuto_An_FinalizationRework { get; set; }
        public List<auto_An_CibRequestLoan> objAuto_An_CibRequestLoan { get; set; }



        //Extra Property

        public AutoSegmentSettings objAutoSegmentSettings { get; set; }

        //Income Generator
        public List<Auto_IncomeSalaried> objAutoIncomeSalariedList { get; set; }
        public List<AutoLandLord> objAutoLandLord { get; set; }
        public List<Auto_AnalystIncomBankStatementSum> objAutoAnalystIncomBankStatementSum { get; set; }
        public List<Auto_An_OverDraftFacility> Auto_An_OverDraftFacilityList { get; set; }


    }
}
