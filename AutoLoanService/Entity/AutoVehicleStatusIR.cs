﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoVehicleStatusIR
    {
        public AutoVehicleStatusIR()
        {

        }

        public int VehicleStatusIRId { get; set; }
        public int VehicleStatus { get; set; }
        public double IrWithArta { get; set; }
        public double IrWithoutArta { get; set; }
        public DateTime LastUpdateDate { get; set; }

        //Selected Property
        public string StatusName { get; set; }
        public int TotalCount { get; set; }

    }
}
