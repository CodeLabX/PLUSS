﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoLoanMaster
    {

        public AutoLoanMaster()
       {
       }

        public int Auto_LoanMasterId { get; set; }
        public int LLID { get; set; }
        public int ProductId { get; set; }
        public string Source { get; set; }

        public string SourceCode { get; set; }
        public string SourceName { get; set; }
        public DateTime AppliedDate { get; set; }
        public double AppliedAmount { get; set; }

        public DateTime ReceiveDate { get; set; }
        public string AskingTenor { get; set; }
        public bool JointApplication { get; set; }
        public int ReferenceLLId { get; set; }
        public int StatusID { get; set; }
        public int UserID { get; set; }
        public double AskingRate { get; set; }
        public int ParentLLID { get; set; }


        // Extra Property
        public int UserType { get; set; }

    }
}
