﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoFacility
    {
        public AutoFacility(){}

        public int FACI_ID { get; set; }
        public string FACI_NAME { get; set; }
        public string FACI_CODE { get; set; }
    }
}
