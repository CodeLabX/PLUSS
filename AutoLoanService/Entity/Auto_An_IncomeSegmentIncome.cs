﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_IncomeSegmentIncome
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public double TotalIncome { get; set; }
        public double DeclaredIncome { get; set; }
        public double AppropriateIncome { get; set; }
        public int EmployeeSegment { get; set; }
        public string EmployeeSegmentCode { get; set; }
        public int AssesmentMethod { get; set; }
        public string AssesmentMethodCode { get; set; }
        public int Category { get; set; }
        public string CategoryCode { get; set; }
        public double TotalJointIncome { get; set; }

    }
}
