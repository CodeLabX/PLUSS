﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoPrice
    {
        public AutoPrice()
        {
        }

        public int PriceId { get; set; }
        public int VehicleId { get; set; }
        public int StatusId { get; set; }
        public int ManufacturingYear { get; set; }
        public double MinimumPrice { get; set; }
        public double MaximumPrice { get; set; }
        public int IsActive { get; set; }
        public DateTime LastUpdateDate { get; set; }

        public string StatusName { get; set; }
    }
}
