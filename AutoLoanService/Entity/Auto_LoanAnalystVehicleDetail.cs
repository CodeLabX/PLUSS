﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_LoanAnalystVehicleDetail
    {
        public int Id { get; set; }
        public int AnalystMasterId { get; set; }
        public Boolean valuePackAllowed { get; set; }
        public int SeatingCapacity { get; set; }
        public Double ConsideredPrice { get; set; }
        public int PriceConsideredOn { get; set; }
        public int CarVarification { get; set; }
        public int DeliveryStatus { get; set; }
        public int RegistrationYear { get; set; }
    }
}
