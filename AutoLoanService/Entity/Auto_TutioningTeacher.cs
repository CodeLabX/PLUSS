﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_TutioningTeacher
    {
        public int Id { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int BankStatementId { get; set; }
        public int Type { get; set; }
        public int NoOfStudent { get; set; }
        public int NoOfDaysForTution { get; set; }
        public double Fees { get; set; }
        public double TotalIncome { get; set; }
        public int IncomeSumId { get; set; }

    }
}
