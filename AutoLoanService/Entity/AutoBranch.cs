﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoBranch
    {
       public AutoBranch()
       {
       }

       public int BRAN_ID { get; set; }
       public string BRAN_NAME { get; set; }
       public string REGI_NM { get; set; }
       public int BRAN_BANK_ID { get; set; }
       public int BRAN_LOCATION_ID { get; set; }
       public string BRAN_ADDRESS { get; set; }
       public int BRAN_BRABCH_STSTUS { get; set; }
       public int BRAN_STATUS { get; set; }
    }
}
