﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class auto_An_CibRequestLoan
    {
        public int CibRequestLoanId { get; set; }
        public int AnalystMasterId { get; set; }
        public int CibFacilityId { get; set; }
        public double CibAmount { get; set; }
        public double CibTenor { get; set; }
    }
}
