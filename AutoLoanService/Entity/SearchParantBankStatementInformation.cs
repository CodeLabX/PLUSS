﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class SearchParantBankStatementInformation
    {

        public int Auto_LoanMasterId { get; set; }
        public string LLID { get; set; }
        public string PrName { get; set; }
        public DateTime PrDateOfBirth { get; set; }
        public string ResidenceAddress { get; set; }
        public string PrProfession { get; set; }
        public string JtName { get; set; }
        public DateTime JtDateOfBirth { get; set; }
        public string Jtprofession { get; set; }
        public int JointApplication { get; set; }
        public double PrIncomeAmount { get; set; }
        public double JtIncomeAmount { get; set; }

    }
}
