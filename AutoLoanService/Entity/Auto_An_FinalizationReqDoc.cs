﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationReqDoc
    {

        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int LetterOfIntroduction { get; set; }
        public int Payslips { get; set; }
        public int TradeLicence { get; set; }
        public int MOA_AOA { get; set; }
        public int PartnershipDeed { get; set; }
        public int FormX { get; set; }
        public int BoardResolution { get; set; }
        public int DiplomaCertificate { get; set; }
        public int ProfessionalCertificate { get; set; }
        public int MunicipalTaxRecipt { get; set; }
        public int TitleDeed { get; set; }
        public int RentalDeed { get; set; }
        public int UtilityBill { get; set; }
        public int LoanOfferLatter { get; set; }


    }
}
