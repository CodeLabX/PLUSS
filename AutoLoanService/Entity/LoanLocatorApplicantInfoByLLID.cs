﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class LoanLocatorApplicantInfoByLLID
    {
        public LoanLocatorApplicantInfoByLLID()
        { 
        }

        public int LLID { get; set; }
        public string ApplicantName { get; set; }
        public string AccountNo { get; set; }
        public string TinNo { get; set; }
        public string LoanStatus { get; set; }
        public string Product { get; set; }
        public string Branch { get; set; }
        public string DateOfBirth { get; set; }

        //LLInfo
        public DateTime RecieveDate { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 BranchId { get; set; }
        public Double LoanApplyAmount { get; set; }
        public string EmployeType { get; set; }
        public string Organization { get; set; }
        public string SourcePersonName { get; set; }

        //Extra Property
        public int userType { get; set; }

    }
}
