﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoGetBankStatement
    {

        public AutoGetBankStatement()
        {
        }

        public string PRStatement { get; set; }
        public string JTStatement { get; set; }

    }
}
