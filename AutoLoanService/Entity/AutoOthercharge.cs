﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoOthercharge
    {
        public AutoOthercharge(){}

        public int OtherChargeId { get; set; }
        public double AccidentOrTheft { get; set; }
        public double Commission { get; set; }
        public double Vat { get; set; }
        public double PassengerPerSeatingCap { get; set; }
        public double AmountofDriverSeat { get; set; }
        public double StampCharge { get; set; }
        public double DriverSeatCharge { get; set; }
    }
}
