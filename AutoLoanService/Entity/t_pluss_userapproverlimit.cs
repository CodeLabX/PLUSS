﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class t_pluss_userapproverlimit
    {
        public int LEVEL1_AMT { get; set; }
        public int LEVEL2_AMT { get; set; }
        public int LEVEL3_AMT { get; set; }
        public int TotalDLAAmountL1 { get; set; }
        public int TotalDLAAmountL2 { get; set; }
        public int TotalDLAAmountL3 { get; set; }
        public string PROD_TAG { get; set; }
    }
}
