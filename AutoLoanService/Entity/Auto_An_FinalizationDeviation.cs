﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationDeviation
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int Level { get; set; }
        public int Description { get; set; }
        public string Code { get; set; }

        //Extra Property
        public string DescriptionText { get; set; }
    }
}
