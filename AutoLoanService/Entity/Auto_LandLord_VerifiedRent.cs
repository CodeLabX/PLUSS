﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
   public class Auto_LandLord_VerifiedRent
    {
        public int Id { get; set; }
        public int AutoLandLordId { get; set; }
        public double Property { get; set; }
        public double Share { get; set; }

    }
}
