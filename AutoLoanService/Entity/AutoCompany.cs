﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoCompany
    {
        public AutoCompany()
        {
            
        }

        public int AutoCompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Remarks { get; set; }

    }
}
