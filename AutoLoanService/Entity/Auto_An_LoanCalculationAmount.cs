﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_LoanCalculationAmount
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public double LTVAllowed { get; set; }
        public double IncomeAllowed { get; set; }
        public double BB_CADAllowed { get; set; }
        public double AskingLoan { get; set; }
        public double ExistingFacilityAllowed { get; set; }
        public double AppropriateLoan { get; set; }
        public double ApprovedLoan { get; set; }
    }
}
