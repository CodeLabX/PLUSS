﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity.CheckListPrint
{
    public class OfferLatterPrint
    {
        public string PDate { get; set; }
        public string lProductId { get; set; }
        public string Ref { get; set; }
        public string lName { get; set; }
        public string lAddress { get; set; }
        public string lTotalLaon { get; set; }
        public string lOfferedLoanAmount { get; set; }
        public string lCoreLoanAmount { get; set; }
        public string lLifeInsurance { get; set; }
        public string lVehicleInsurance { get; set; }
        public string lTotalLoanAmount { get; set; }
        public string lTenor { get; set; }
        public string lInterestRate { get; set; }
        public string lMonthlyInstalment { get; set; }
        public string lSIFrom { get; set; }
        public string lSIFromAccountNo { get; set; }
        public string lSIFromBankName { get; set; }
        public string lBankStatementName { get; set; }
        public string Label10 { get; set; }
        public string Label11 { get; set; }
        public string Label12 { get; set; }

        public string SatisfactoryScbBank { get; set; }
        public string CompanyTrade { get; set; }
        public string SettlementOf { get; set; }
        public string SettlementWith { get; set; }
        public string DynamicCondition { get; set; }
        public string NewGivenCondition { get; set; }

    }
}
