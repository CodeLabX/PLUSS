﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity.CheckListPrint
{
    public class BAlatterPrintEntity
    {
        public string Label3 { get; set; }
        public string lblBranch { get; set; }
        public string lblACNo { get; set; }
        public string lblDate { get; set; }
        public string lblApplicationDate { get; set; }
        public string Label12 { get; set; }
        public string lblLoanAmount { get; set; }
        public string lblInterestRate { get; set; }
        public string Label17 { get; set; }
        public string Label38 { get; set; }
        public string lblEMIAmount { get; set; }
        public string lblNoOfInstallemt { get; set; }
        public string lblTenor { get; set; }
        public string lblExpiryDate { get; set; }
        public string lblBrand { get; set; }
        public string lblModel { get; set; }
        public string Label39 { get; set; }
        public string lblProcessingFee { get; set; }
        public string lblDPNoteLetter { get; set; }
        public string lblDPNoteLetter0 { get; set; }
        public string lblDPNoteLetter1 { get; set; }
        public string lblDPNoteLetter2 { get; set; }
        public string lblDPNoteLetter3 { get; set; }
        public string lblLetterOfHypothecate { get; set; }
        public string lblDPNoteLetter5 { get; set; }
        public string lblDPNoteLetter6 { get; set; }
        public string lblDPNoteLetter7 { get; set; }
        public string lblSecurity { get; set; }
        public string lblDPNoteLetter9 { get; set; }
        public string lblLoanAccNo { get; set; }
        public string lblDebitAuthorityAcc { get; set; }
        public string Label40 { get; set; }
        public string Label41 { get; set; }
        public string Label42 { get; set; }
        public string Label43 { get; set; }
        public string lblDPNoteLetter12 { get; set; }
        public string lblDPNoteLetter13 { get; set; }
        public string Label46 { get; set; }
        public string lblBranch0 { get; set; }
        public string lblACNo0 { get; set; }
        public string lblDate0 { get; set; }
        public string lblApplicationDate0 { get; set; }
        public string Label9 { get; set; }
        public string lblLoanAmount0 { get; set; }
        public string lblInterestRate0 { get; set; }
        public string Label11 { get; set; }
        public string Label13 { get; set; }
        public string lblEMIAmount0 { get; set; }
        public string lblNoOfInstallemt0 { get; set; }
        public string lblTenor0 { get; set; }
        public string lblExpiryDate0 { get; set; }
        public string lblBrand0 { get; set; }
        public string lblModel0 { get; set; }
        public string Label16 { get; set; }
        public string lblProcessingFee0 { get; set; }
        public string lblDPNoteLetter_0 { get; set; }
        public string Label44 { get; set; }
        public string Label45 { get; set; }
        public string Label19 { get; set; }
        public string Label20 { get; set; }
        public string Label21 { get; set; }
        public string Label22 { get; set; }
        public string lblLetterOfHypothecate0 { get; set; }
        public string Label24 { get; set; }
        public string Label25 { get; set; }
        public string Label26 { get; set; }
        public string lblSecurity0 { get; set; }
        public string Label28 { get; set; }
        public string lblLoanAccNo0 { get; set; }
        public string lblDebitAuthorityAcc0 { get; set; }
        public string Label31 { get; set; }
        public string Label32 { get; set; }
        public string Label33 { get; set; }
        public string Label34 { get; set; }
        public string Label35 { get; set; }
        public string Label36 { get; set; }
        public string reportType { get; set; }
    }
}
