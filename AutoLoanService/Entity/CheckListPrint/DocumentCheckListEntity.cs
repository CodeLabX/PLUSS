﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity.CheckListPrint
{
    public class DocumentCheckListEntity
    {
        public string Label1 { get; set; }
        public string lblProductId { get; set; }
        public string lLLID { get; set; }
        public string lUnderWritingLevel { get; set; }
        public string lApplicantName { get; set; }
        public string lAccountNumber { get; set; }
        public string lSource { get; set; }
        public string lSegment { get; set; }
        public string lApplicationForm { get; set; }
        public string lPhoto { get; set; }
        public string lTin { get; set; }
        public string lPhotoID { get; set; }
        public string lSalaryCertificate { get; set; }
        public string lPaySlip { get; set; }
        public string lTradeLisence { get; set; }
        public string lMOA { get; set; }
        public string lPartnership { get; set; }
        public string lFormX { get; set; }
        public string lBoardRes { get; set; }
        public string lDipCert { get; set; }
        public string lCertFromPof { get; set; }
        public string lTaxRecipt { get; set; }
        public string lTitleDeed { get; set; }
        public string lRentalDeed { get; set; }
        public string lUtilityBill { get; set; }
        public string lLoanOfferLetter { get; set; }
        public string lGlobalConso { get; set; }
        public string lSignedIID { get; set; }
        public string lUsedCarDoc { get; set; }
        public string lCreditCheck { get; set; }
        public string lIncomeAssSheet { get; set; }
        public string lLimitLoadingIns { get; set; }
        public string lBASEL2Data { get; set; }
        public string lDeDup { get; set; }
        public string lNorkom { get; set; }
        public string lCIBReport { get; set; }
        public string lCPVReport { get; set; }
        public string lPriceVerf { get; set; }
        public string lCreditCardRep { get; set; }
        public string lBankStatVeri { get; set; }
        public string lCreditReport { get; set; }
        public string lCarVerifReport { get; set; }
        public string lUsedCarValuation { get; set; }
        public string lFacility1 { get; set; }
        public string lAmount1 { get; set; }
        public string lTenor1 { get; set; }
        public string lFacility2 { get; set; }
        public string lAmount2 { get; set; }
        public string lTenor2 { get; set; }
        public string lFacility3 { get; set; }
        public string lAmount3 { get; set; }
        public string lTenor3 { get; set; }
        public string lFacility4 { get; set; }
        public string lAmount4 { get; set; }
        public string lTenor4 { get; set; }
        public string lFacility5 { get; set; }
        public string lAmount5 { get; set; }
        public string lTenor5 { get; set; }
        public string lOtherBank1 { get; set; }
        public string lAccNO1 { get; set; }
        public string lType1 { get; set; }
        public string lOtherBank2 { get; set; }
        public string lAccNO2 { get; set; }
        public string lType2 { get; set; }
        public string lOtherBank3 { get; set; }
        public string lAccNO3 { get; set; }
        public string lType3 { get; set; }
        public string lOtherBank4 { get; set; }
        public string lAccNO4 { get; set; }
        public string lType4 { get; set; }
        public string lOtherBank5 { get; set; }
        public string lAccNO5 { get; set; }
        public string lType5 { get; set; }
        public string lAppRaiseBy { get; set; }
        public string lSupportedBy { get; set; }
    }
}
