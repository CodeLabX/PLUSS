﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity.CheckListPrint
{
    public class Basal2Entity
    {
        public string lCustomarName { get; set; }
        public string lCustomerMaster { get; set; }
        public string lHighestEducation { get; set; }
        public string lEmploymentStatusCode { get; set; }
        public string lBusinessEstablishDate { get; set; }
        public string lMonthOfTotalWorkExperience { get; set; }
        public string lMonthInCurrentJob { get; set; }
        public string lDateOfJoining { get; set; }
        public string sdfsadfsd { get; set; }
        public string lOtherMonthlyIncome { get; set; }
        public string lMonthlyLoanRepayment { get; set; }
        public string lOutstandingAmount { get; set; }
        public string lTypeOfAccount { get; set; }
        public string lLoanAccountNumber { get; set; }
        public string lDateLoanBooked { get; set; }
        public string lDesignation { get; set; }
        public string lDepartment { get; set; }
        public string lDateOfBirth { get; set; }
        public string lWithdrawalDate { get; set; }
        public string lWithdrawalReason { get; set; }
        public string lCreditPolicy { get; set; }
        public string lSCBUnsecuredExposure { get; set; }
        public string lSCBSecuredExposure { get; set; }
        public string lLTV { get; set; }
        public string lFTV { get; set; }
        public string lTotalLTV { get; set; }
        public string lTotalFTV { get; set; }
        public string lDeviationCodeAssetOps { get; set; }
        public string lDIRatio { get; set; }
        public string lInternalBacklistReason { get; set; }
        public string lYearsOfResidence { get; set; }
        public string lRentalPerMonth { get; set; }
        public string lNoOfDependents { get; set; }
        public string lHomeTelNo { get; set; }
        public string lOfficeTelNo { get; set; }
        public string lMobileNo { get; set; }
        public string lNumbreOfCars { get; set; }
        public string lPackagePromoCode { get; set; }
        public string lCoBorrower1Relationship { get; set; }
        public string lCoBorrower1SCBFlag { get; set; }
        public string lPurchaseDate { get; set; }
        public string lApplicationDate { get; set; }
        public string lTopUpAmount { get; set; }
        public string lNumberOfTopUp { get; set; }
        public string lTopUp1Amount { get; set; }
        public string lTopUp1Date { get; set; }
        public string lTopUp2Amount { get; set; }
        public string lTopUp2Date { get; set; }
        public string lTopUp3Amount { get; set; }
        public string lTopUp3Date { get; set; }
        public string lPriceOfCar { get; set; }
        public string lCurrentMarketValue { get; set; }
        public string lCarModel { get; set; }
        public string lCarMadeYear { get; set; }
        public string lEngineCC { get; set; }
        public string lCarUsage { get; set; }
        public string lDealerCode { get; set; }
        public string lNameOfFriend { get; set; }
        public string lFriendRelativeContact { get; set; }
    }
}
