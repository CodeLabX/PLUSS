﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity.CheckListPrint
{
    public class LLIPrintEntity
    {
        public string lDate { get; set; }
        public string lProductType { get; set; }
        public string lblProductId { get; set; }
        public string lApplicantName { get; set; }
        public string lIncome { get; set; }
        public string lLoanAmount { get; set; }
        public string lTenor { get; set; }
        public string lInterestRate { get; set; }
        public string lEMI { get; set; }
        public string lLoanType { get; set; }
        public string lLoneTypeRemark { get; set; }
        public string lEmployerCat { get; set; }
        public string lEmployerCatRemark { get; set; }
        public string lAnalystCode { get; set; }
        public string lAnalystCodeRemark { get; set; }
        public string lApproverCode { get; set; }
        public string lApproverCodeRemark { get; set; }
        public string lIncomeAssesmentCode { get; set; }
        public string lIncomeAssesmentCodeRemark { get; set; }
        public string lDeviationCode1 { get; set; }
        public string lDeviationCodeRemark1 { get; set; }
        public string lDeviationCode2 { get; set; }
        public string lDeviationCodeRemark2 { get; set; }
        public string lDeviationCode3 { get; set; }
        public string lDeviationCodeRemark3 { get; set; }
        public string lCollaterals { get; set; }
        public string lCollateralsRemark { get; set; }
        public string lCRD { get; set; }
        public string lCRDRemark { get; set; }
        public string lBorrowerCode { get; set; }
        public string lBorrowerCodeRemark { get; set; }
        public string lApplicationSourceCode { get; set; }
        public string lApplicationSourceCodeRemark { get; set; }
        public string lDiscretion { get; set; }
        public string lDiscretionRemark { get; set; }
        public string lInprft { get; set; }
        public string lInprftRemark { get; set; }
        public string lBTF { get; set; }
        public string lBTFRemark { get; set; }
        public string lVehicleStatus { get; set; }
        public string lVehicleStatusRemark { get; set; }
        public string lVehicleType { get; set; }
        public string lVehicleTypeRemark { get; set; }
        public string lVehicleSize { get; set; }
        public string lVehicleSizeRemark { get; set; }
        public string lVehicleManufacturingYear { get; set; }
        public string lVehicleManufacturingYearRemark { get; set; }
        public string lVendorCat { get; set; }
        public string lVendorCatRemark { get; set; }
        public string lLoanPurposeCode { get; set; }
        public string lLoanPurposeCodeRemark { get; set; }
        public string lBancaInfo { get; set; }
        public string lBancaInfoRemark { get; set; }
        public string lLoanClass { get; set; }
        public string lLoanClassRemark { get; set; }
        public string lDBR { get; set; }
        public string lDBRRemark { get; set; }
        public string lLTV { get; set; }
        public string lLTVRemark { get; set; }
        public string lSAL { get; set; }
        public string lSALRemark { get; set; }
        public string lDBT { get; set; }
        public string lDBTRemark { get; set; }
        public string lMIS4 { get; set; }
        public string lMIS4Remark { get; set; }
        public string lVehicleBrand { get; set; }
        public string lVehicleBrandRemark { get; set; }
        public string lVehicleModel { get; set; }
        public string lVehicleModelRemark { get; set; }
        public string lVehicleDeler { get; set; }
        public string lVehicleDelerRemark { get; set; }
        public string lVehicleManufacYear { get; set; }
        public string lVehicleManufacYearRemark { get; set; }
        public string lVehiclceSizee { get; set; }
        public string lVehiclceSizeeRemark { get; set; }
        public string lVehiclePrice { get; set; }
        public string lVehiclePriceRemark { get; set; }
        public string lInsurenceCode { get; set; }
        public string lInsurenceCodeRemark { get; set; }
        public string lGeneralInsurance { get; set; }
        public string lGeneralInsuranceRemark { get; set; }
        public string Label83 { get; set; }
        public string Label85 { get; set; }
        public string Label87 { get; set; }
        public string Label89 { get; set; }
        public string Label84 { get; set; }
        public string Label86 { get; set; }
        public string Label88 { get; set; }
        public string Label90 { get; set; }
    }
}
