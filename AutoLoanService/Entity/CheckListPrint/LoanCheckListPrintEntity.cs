﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity.CheckListPrint
{
    public class LoanCheckListPrintEntity
    {
        public string Label1 { get; set; }
        public string lLLID { get; set; }
        public string lblProductId { get; set; }
        public string lApplicant { get; set; }
        public string lProduct { get; set; }
        public string lAge { get; set; }
        public string lSource { get; set; }
        public string lIncome { get; set; }
        public string lSubmissionDate { get; set; }
        public string lBorrowingRelationship { get; set; }
        public string lAppraisalDate { get; set; }
        public string lApplicationType { get; set; }
        public string lPurpose { get; set; }
        public string lBrand { get; set; }
        public string lEngine { get; set; }
        public string lQuotedPrice { get; set; }
        public string lModel { get; set; }
        public string lManufacturingYear { get; set; }
        public string lConsideredPrice { get; set; }
        public string lVehicleType { get; set; }
        public string lSeatingCapacity { get; set; }
        public string lConsiderationON { get; set; }
        public string lVehicleStatus { get; set; }
        public string lAgeAtLoanExpory { get; set; }
        public string lCarVerification { get; set; }
        public string lVendor { get; set; }
        public string lVendorCategory { get; set; }
        public string lVerificationStatus { get; set; }
        public string lMou { get; set; }
        public string lPOAmount { get; set; }
        public string lLTV { get; set; }
        public string lRepayment { get; set; }
        public string lGenInsurance { get; set; }
        public string lLTVWithInsurance { get; set; }
        public string lBankName { get; set; }
        public string lARTA { get; set; }
        public string lExtraLTV { get; set; }
        public string lAccountNumber { get; set; }
        public string lTotalLoan { get; set; }
        public string lAllowedDBR { get; set; }
        public string lSecurityPDC { get; set; }
        public string lEMI { get; set; }
        public string lGivenDBR { get; set; }
        public string lSecPDCAmount { get; set; }
        public string lTenor { get; set; }
        public string lDBRWithInsurance { get; set; }
        public string lNoOfSecPDC { get; set; }
        public string lInterestRate { get; set; }
        public string lExtraDBR { get; set; }
        public string lSecPDCBank { get; set; }
        public string lARTAFrom { get; set; }
        public string lLoanExpiry { get; set; }
        public string lACNumber { get; set; }
        public string lTermLoanType1 { get; set; }
        public string lTermLoanFlag1 { get; set; }
        public string lTermLoanOriginalAmount1 { get; set; }
        public string lTermLoanOutstanding1 { get; set; }
        public string lTermLoanEMI1 { get; set; }
        public string lTermLoanSecurityType1 { get; set; }
        public string lTermLoanSecurityFV1 { get; set; }
        public string lTermLoanSecurityPV1 { get; set; }
        public string lTermLoanLTV1 { get; set; }
        public string lTermLoanType2 { get; set; }
        public string lTermLoanFlag2 { get; set; }
        public string lTermLoanOriginalAmount2 { get; set; }
        public string lTermLoanOutstanding2 { get; set; }
        public string lTermLoanEMI2 { get; set; }
        public string lTermLoanSecurityType2 { get; set; }
        public string lTermLoanSecurityFV2 { get; set; }
        public string lTermLoanSecurityPV2 { get; set; }
        public string lTermLoanLTV2 { get; set; }
        public string lTermLoanType3 { get; set; }
        public string lTermLoanFlag3 { get; set; }
        public string lTermLoanOriginalAmount3 { get; set; }
        public string lTermLoanOutstanding3 { get; set; }
        public string lTermLoanEMI3 { get; set; }
        public string lTermLoanSecurityType3 { get; set; }
        public string lTermLoanSecurityFV3 { get; set; }
        public string lTermLoanSecurityPV3 { get; set; }
        public string lTermLoanLTV3 { get; set; }
        public string lTermLoanType4 { get; set; }
        public string lTermLoanFlag4 { get; set; }
        public string lTermLoanOriginalAmount4 { get; set; }
        public string lTermLoanOutstanding4 { get; set; }
        public string lTermLoanEMI4 { get; set; }
        public string lTermLoanSecurityType4 { get; set; }
        public string lTermLoanSecurityFV4 { get; set; }
        public string lTermLoanSecurityPV4 { get; set; }
        public string lTermLoanLTV4 { get; set; }
        public string lCreditCardType1 { get; set; }
        public string lCreditCardFlag1 { get; set; }
        public string lCreditCardOriginalAmount1 { get; set; }
        public string lCreditCardOutstanding1 { get; set; }
        public string lCreditCardEMI1 { get; set; }
        public string lCreditCardSecurityType1 { get; set; }
        public string lCreditCardSecurityFV1 { get; set; }
        public string lCreditCardSecurityPV1 { get; set; }
        public string lCreditCardLTV1 { get; set; }
        public string lCreditCardType2 { get; set; }
        public string lCreditCardFlag2 { get; set; }
        public string lCreditCardOriginalAmount2 { get; set; }
        public string lCreditCardOutstanding2 { get; set; }
        public string lCreditCardEMI2 { get; set; }
        public string lCreditCardSecurityType2 { get; set; }
        public string lCreditCardSecurityFV2 { get; set; }
        public string lCreditCardSecurityPV2 { get; set; }
        public string lCreditCardLTV2 { get; set; }
        public string lThisLoanFlag { get; set; }
        public string lThisLoanOgiginalAmount { get; set; }
        public string lThisLoanOutstanding { get; set; }
        public string lThisLoanEMI { get; set; }
        public string lThisLoanSecType { get; set; }
        public string lThisLoanSecurityFV { get; set; }
        public string lThisLoanSecurityPV { get; set; }
        public string lThisLoanLTV { get; set; }
        public string lOverDraftType1 { get; set; }
        public string lOverDraftFlag1 { get; set; }
        public string lOverDraftOriginalAmount1 { get; set; }
        public string lOverDraftOutstanding1 { get; set; }
        public string lOverDraftEMI1 { get; set; }
        public string lOverDraftSecurityType1 { get; set; }
        public string lOverDraftSecurityFV1 { get; set; }
        public string lOverDraftSecurityPV1 { get; set; }
        public string lOverDraftLTV1 { get; set; }
        public string lOverDraftType2 { get; set; }
        public string lOverDraftFlag2 { get; set; }
        public string lOverDraftOriginalAmount2 { get; set; }
        public string lOverDraftOutstanding2 { get; set; }
        public string lOverDraftEMI2 { get; set; }
        public string lOverDraftSecurityType2 { get; set; }
        public string lOverDraftSecurityFV2 { get; set; }
        public string lOverDraftSecurityPV2 { get; set; }
        public string lOverDraftLTV2 { get; set; }
        public string lOverDraftType3 { get; set; }
        public string lOverDraftFlag3 { get; set; }
        public string lOverDraftOriginalAmount3 { get; set; }
        public string lOverDraftOutstanding3 { get; set; }
        public string lOverDraftEMI3 { get; set; }
        public string lOverDraftSecurityType3 { get; set; }
        public string lOverDraftSecurityFV3 { get; set; }
        public string lOverDraftSecurityPV3 { get; set; }
        public string lOverDraftLTV3 { get; set; }
        public string lOffSCBTermLoanBank1 { get; set; }
        public string lOffSCBTermLoanOriginalLimit1 { get; set; }
        public string lOffSCBTermLoanEMI1 { get; set; }
        public string lOffSCBTermLoanBank2 { get; set; }
        public string lOffSCBTermLoanOriginalLimit2 { get; set; }
        public string lOffSCBTermLoanEMI2 { get; set; }
        public string lOffSCBTermLoanBank3 { get; set; }
        public string lOffSCBTermLoanOriginalLimit3 { get; set; }
        public string lOffSCBTermLoanEMI3 { get; set; }
        public string lOffSCBTermLoanBank4 { get; set; }
        public string lOffSCBTermLoanOriginalLimit4 { get; set; }
        public string lOffSCBTermLoanEMI4 { get; set; }
        public string lOffSCBCreditCardBank1 { get; set; }
        public string lOffSCBCreditCardOriginalLimit1 { get; set; }
        public string lOffSCBCreditCardEMI1 { get; set; }
        public string lOffSCBCreditCardBank2 { get; set; }
        public string lOffSCBCreditCardOriginalLimit2 { get; set; }
        public string lOffSCBCreditCardEMI2 { get; set; }
        public string lOffSCBOverDraftBank1 { get; set; }
        public string lOffSCBOverDraftOriginalLimit1 { get; set; }
        public string lOffSCBOverDraftBank2 { get; set; }
        public string lOffSCBOverDraftOriginalLimit2 { get; set; }
        public string lOffSCBOverDraftBank3 { get; set; }
        public string lOffSCBOverDraftOriginalLimit3 { get; set; }
        public string lTotalSCBExposures { get; set; }
        public string lTotalLTV { get; set; }
        public string lTotalAggregateSecutityLoanRatio { get; set; }
        public string lSurplus { get; set; }
        public string lTotalAutoLoan { get; set; }
        public string lTotalMonthCommitments { get; set; }
        public string lTotalSCBSecuredLoans { get; set; }
        public string lTotalSCBUnsecuredLoans { get; set; }
        public string lCollerterization { get; set; }
        public string Label163 { get; set; }
        public string lWithCollerterization { get; set; }
        public string lAppRaisedBy1 { get; set; }
        public string lAppSupportedBy1 { get; set; }
        public string lStrength1 { get; set; }
        public string lStrength2 { get; set; }
        public string lStrength3 { get; set; }
        public string lStrength4 { get; set; }
        public string lStrength5 { get; set; }
        public string lStrength6 { get; set; }
        public string lStrength7 { get; set; }
        public string lWeakness1 { get; set; }
        public string lWeakness2 { get; set; }
        public string lWeakness3 { get; set; }
        public string lWeakness4 { get; set; }
        public string lWeakness5 { get; set; }
        public string lWeakness6 { get; set; }
        public string lWeakness7 { get; set; }
        public string lDeviationLevel1 { get; set; }
        public string lDeviationDescription1 { get; set; }
        public string lDeviationCode1 { get; set; }
        public string lDeviationLevel2 { get; set; }
        public string lDeviationDescription2 { get; set; }
        public string lDeviationCode2 { get; set; }
        public string lDeviationLevel3 { get; set; }
        public string lDeviationDescription3 { get; set; }
        public string lDeviationCode3 { get; set; }
        public string lDeviationLevel4 { get; set; }
        public string lDeviationDescription4 { get; set; }
        public string lDeviationCode4 { get; set; }
        public string lUnderWritingLevel { get; set; }
        public string lRemark1 { get; set; }
        public string lRemark2 { get; set; }
        public string lRemark3 { get; set; }
        public string lRemark4 { get; set; }
        public string lRemark5 { get; set; }
        public string lCondition1 { get; set; }
        public string Label265 { get; set; }
        public string Label266 { get; set; }
        public string lCondition2 { get; set; }
        public string Label268 { get; set; }
        public string Label267 { get; set; }
        public string lCondition3 { get; set; }
        public string Label269 { get; set; }
        public string Label270 { get; set; }
        public string lCondition4 { get; set; }
        public string Label272 { get; set; }
        public string Label271 { get; set; }
        public string lCondition5 { get; set; }
        public string Label273 { get; set; }
        public string Label274 { get; set; }
        public string lCondition6 { get; set; }
        public string Label2 { get; set; }
        public string Label3 { get; set; }
        public string lCondition7 { get; set; }
        public string Label4 { get; set; }
        public string Label5 { get; set; }
        public string lReworkDone { get; set; }
        public string lReworkCount { get; set; }
        public string lReworkReason { get; set; }
        public string lUnderWritingDate { get; set; }
        public string lValidityUpTo { get; set; }
        public string lMaxLoan { get; set; }
        public string lMaxLoanFlag { get; set; }
        public string lExperience { get; set; }
        public string lExperienceFlag { get; set; }
        public string lMinLoan { get; set; }
        public string lMinLoanFlag { get; set; }
        public string lAccountRelationshipe { get; set; }
        public string lAccountRelationshipeFlag { get; set; }
        public string lLoanPurpose { get; set; }
        public string lLoanPurposeFlag { get; set; }
        public string lMinimumIncome { get; set; }
        public string lMinimumIncomeFlag { get; set; }
        public string lSigment { get; set; }
        public string lSigmentFlag { get; set; }
        public string lPhisicalVarification { get; set; }
        public string lPhisicalVarificationFlag { get; set; }
        public string lMinAge { get; set; }
        public string lMinAgeFlag { get; set; }
        public string lPadTenor { get; set; }
        public string lPadTenorFlag { get; set; }
        public string lMaxAge { get; set; }
        public string lMaxAgeFlag { get; set; }
        public string lNationality { get; set; }
        public string lNationalityFlag { get; set; }
        public string lTelephone { get; set; }
        public string lTelephoneFlag { get; set; }
        public string lLocation { get; set; }
        public string lLocationFlag { get; set; }
        public string lAppRaisedBy { get; set; }
        public string lAppSupportedBy { get; set; }
        public string lAppApprovedBy { get; set; }
        public string lApproverComments { get; set; }
        public string lUnderWritingLabel { get; set; }
    }
}
