﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoLandLord
    {
        public int Id { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int EmployerCatagoryId { get; set; }
        public int SectorId { get; set; }
        public int IncomeAssId { get; set; }
        public double TotalRentalIncome { get; set; }
        public double ReflectionRequired { get; set; }
        public double Percentage { get; set; }
        public string Remarks { get; set; }
        public int AccountType { get; set; }

        //Extra Field
       public List<Auto_LandLord_VerifiedRent> Property { get; set; }

    }
}
