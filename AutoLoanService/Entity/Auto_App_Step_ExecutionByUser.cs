﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_App_Step_ExecutionByUser
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int Auto_LoanMasterId { get; set; }
        public int AppRaiseBy { get; set; }
        public int AppSupportBy { get; set; }
        public int AppAnalysisBy { get; set; }
        public int AppApprovedBy { get; set; }

        //Extra Property
        public string AppRaiseByName { get; set; }
        public string AppSupportByName { get; set; }
        public string AppAnalysisByName { get; set; }
        public string AppApprovedByName { get; set; }

    }
}
