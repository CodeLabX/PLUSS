﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
   public class AutoStatus
    {
       public AutoStatus()
       {
       }

       public int StatusId { get; set; }
       public string StatusName { get; set; }
       public int StatusType { get; set; }
       public string StatusCode { get; set; }

    }
}
