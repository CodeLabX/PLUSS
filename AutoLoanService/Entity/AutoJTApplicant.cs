﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoJTApplicant
    {

       public AutoJTApplicant()
       {
       }

        public int JtApplicantId { get; set; }
        public int Auto_LoanMasterId { get; set; }
        public string JtName { get; set; }
        public int Gender { get; set; }
        public string RelationShipNumber { get; set; }

        public string TINNumber { get; set; }
        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime DateofBirth { get; set; }

        public string MaritalStatus { get; set; }
        public string SpouseName { get; set; }
        public string SpouseProfession { get; set; }
        public string SpouseWorkAddress { get; set; }

        public string SpouseContactNumber { get; set; }
        public string RelationShipwithPrimary { get; set; }
        public string Nationality { get; set; }
        public string IdentificationNumberType { get; set; }
        public string IdentificationNumber { get; set; }
        public string IdentificationDocument { get; set; }

        public string NumberofDependents { get; set; }
        public string HighestEducation { get; set; }
        public string ResidenceAddress { get; set; }
        public string MailingAddress { get; set; }

        public string PermanentAddress { get; set; }
        public string ResidenceStatus { get; set; }
        public string OwnershipDocument { get; set; }
        public string ResidenceStatusYear { get; set; }
        public string PhoneResidence { get; set; }

        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool DirectorshipwithanyBank { get; set; }

        //Extra property
        public int Age { get; set; }


    }
}
