﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationRework
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int ReworkDone { get; set; }
        public string ReworkReason { get; set; }
        public string LastApprovalDate { get; set; }
        public string ReworkDate { get; set; }
        public int ReworkCount { get; set; }
        public double LastApprovalAmount { get; set; }

    }
}
