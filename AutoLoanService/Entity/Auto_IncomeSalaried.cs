﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
  public  class Auto_IncomeSalaried
    {
        public int Id { get; set; }
        public int AnalystMasterId { get; set; }
        public int SalaryMode { get; set; }
        public int Employer { get; set; }
        public int IncomeAssId { get; set; }
        public double GrosSalary { get; set; }
        public double NetSalary { get; set; }
        public Boolean VariableSalary { get; set; }
        public double VariableSalaryAmount { get; set; }
        public double VariableSalaryPercent { get; set; }
        public Boolean CarAllownce { get; set; }
        public double CarAllownceAmount { get; set; }
        public double CarAllowncePer { get; set; }
        public Boolean OwnHouseBenifit { get; set; }
        public double TotalIncome { get; set; }
        public string Remarks { get; set; }
        public int AccountType { get; set; }
        public List<Auto_VariableSalaryCalculation> ObjSalariedVariableSalaryMonthIncomelist { get; set; }  

    }
}
