﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_RepaymentCapability
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int DBRLevel { get; set; }
        public double AppropriateDBR { get; set; }
        public double ConsideredDBR { get; set; }
        public double MaxEMICapability { get; set; }
        public double ExistingEMI { get; set; }
        public double CurrentEMICapability { get; set; }
        public double BalanceSupported { get; set; }
        public double MaxEMI { get; set; }

    }
}
