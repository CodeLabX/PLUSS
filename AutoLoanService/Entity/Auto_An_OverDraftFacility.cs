﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_OverDraftFacility
    {
        public int ID { get; set; }
        public int BankStatementId { get; set; }
        public int BankId { get; set; }
        public double LimitAmount { get; set; }
        public double Interest { get; set; }
        public int Consider { get; set; }
        public int Type { get; set; }
        public int IncomeSumId { get; set; }
        public int AutoLoanMasterId { get; set; }

    }
}
