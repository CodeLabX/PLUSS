﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoLoanFacilityOffSCB
    {

        public int OffFacilityId { get; set; }
        public int AnalystMasterId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int BankID { get; set; }
        public double OriginalLimit { get; set; }
        public double EMI { get; set; }
        public double EMIPercent { get; set; }
        public double EMIShare { get; set; }
        public double Status { get; set; }
        public int Consider { get; set; }
        public int Type { get; set; }
        public double InterestRate { get; set; }

        //extra property
        public string BankName { get; set; }

    }
}
