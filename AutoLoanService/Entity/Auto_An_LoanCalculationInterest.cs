﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_LoanCalculationInterest
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public double VendorAllowed { get; set; }
        public double ARTAAllowed { get; set; }
        public double SegmentAllowed { get; set; }
        public double AskingRate { get; set; }
        public double AppropriateRate { get; set; }
        public double ConsideredRate { get; set; }

    }
}
