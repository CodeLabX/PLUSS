﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoVendorMOU
    {
        public AutoVendorMOU()
        {

        }

        public int VendorMouId { get; set; }
        public int AutoVendorId { get; set; }
        public double IrWithArta { get; set; }
        public double IrWithoutArta { get; set; }
        public double ProcessingFee { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int IsMou { get; set; }

        //Selected Property
        public string VendorName { get; set; }
        public string VendorCode { get; set; }
        public string DealerStatus { get; set; }
        public int DealerType { get; set; }
        public string Address { get; set; }
        public string phone { get; set; }
        public string Website { get; set; }
        public int TotalCount { get; set; }


    }
}
