﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class Auto_BankStatementAvgTotal
    {

        public Auto_BankStatementAvgTotal()
        {
           
        }


        public int BankStatementAvgTotalId { get; set; }
        public int LLId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int Type { get; set; }
        public int BankStatementId { get; set; }
        public int BankId { get; set; }
        public DateTime Month { get; set; }
        public double AVGTotal { get; set; }
        public double TrunOverTotal { get; set; }
        public double Adjustment { get; set; }
        public string AccountNumber { get; set; }

    }
}
