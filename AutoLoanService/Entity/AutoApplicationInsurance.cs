﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoApplicationInsurance
    {

        public AutoApplicationInsurance()
        {
        }

        public int ApplicationInsuranceId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int GeneralInsurance { get; set; }
        public int LifeInsurance { get; set; }
        public bool InsuranceFinancewithLoan { get; set; }
        public bool InsuranceFinancewithLoanLife { get; set; }


    }
}
