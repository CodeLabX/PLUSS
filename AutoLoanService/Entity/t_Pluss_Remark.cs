﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class t_Pluss_Remark
    {
        public int REMA_ID { get; set; }
        public int REMA_PROD_ID { get; set; }
        public int REMA_DTYPE { get; set; }
        public string REMA_NAME { get; set; }
        public int REMA_NO { get; set; }
        public int REMA_FOR { get; set; }
        public int REMA_STATUS { get; set; }

    }
}
