﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationRepaymentMethod
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public string Asking { get; set; }
        public int Instrument { get; set; }
        public int SecurityPDC { get; set; }
        public double PDCAmount { get; set; }
        public int BankID { get; set; }
        public string SCBAccount { get; set; }
        public int NoOfPDC { get; set; }
        public string AccountNumber { get; set; }

        // extra property
        public string BankName { get; set; }

    }
}
