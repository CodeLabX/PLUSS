﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_IncomeSegmentApplicantIncome
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int CalculationSource { get; set; }
        public double Income { get; set; }
        public int IncomeAssesmentMethod { get; set; }
        public string IncomeAssesmentCode { get; set; }
        public int EmployerCategory { get; set; }
        public string EmployerCategoryCode { get; set; }
        public int Consider { get; set; }
        public int ApplicantType { get; set; }

    }
}
