﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoLoanReference
    {

        public AutoLoanReference()
        {
        }

        public int ReferenceId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public string ReferenceName1 { get; set; }
        public string ReferenceName2 { get; set; }
        public string Relationship1 { get; set; }
        public string Relationship2 { get; set; }
        public string Occupation1 { get; set; }
        public string Occupation2 { get; set; }
        public string OrganizationName1 { get; set; }
        public string OrganizationName2 { get; set; }
        public string Designation1 { get; set; }
        public string Designation2 { get; set; }
        public string WorkAddress1 { get; set; }
        public string WorkAddress2 { get; set; }
        public string ResidenceAddress1 { get; set; }
        public string ResidenceAddress2 { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string MobileNumber1 { get; set; }
        public string MobileNumber2 { get; set; }


    }
}
