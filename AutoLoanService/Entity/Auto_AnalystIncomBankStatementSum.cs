﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_AnalystIncomBankStatementSum
    {
        public int Id { get; set; }
        public int AnalystMasterId { get; set; }
        public int BankStatementId { get; set; }
        public double TotalCTO12 { get; set; }
        public double AvgCTO12 { get; set; }
        public double AvgBalance12 { get; set; }
        public double TotalCTO6 { get; set; }
        public double AvgCTO6 { get; set; }
        public double AvgBalance6 { get; set; }
        public double ConsideredCTO { get; set; }
        public double ConsideredBalance { get; set; }
        public double ConsideredMargin { get; set; }
        public double GrossIncome { get; set; }
        public double NetAfterPayment { get; set; }
        public double Share { get; set; }
        public double NetIncome { get; set; }
        public int IncomeAssessmentMethod { get; set; }
        public int Industry { get; set; }
        public int SubSector { get; set; }
        public int PrivateIncome { get; set; }
        public double AvgBalance1st9m { get; set; }
        public double AvgBalance1st6m { get; set; }
        public double AvgBalanceLast6m { get; set; }
        public double AvgBalanceLast3m { get; set; }
        public string Remarks { get; set; }
        public int Type { get; set; }
        public int TabType { get; set; }

        public List<Auto_An_OverDraftFacility> AutoAnOverDraftFacility { get; set; }
        
        public List<Auto_BankStatementAvgTotal> AutoBankStatementAvgTotalList { get; set; }

        public Auto_PreviousRePaymentHistory AutoPreviousRePaymentHistory { get; set; }

        public Auto_PracticingDoctor AutoPracticingDoctor { get; set; }

        public Auto_TutioningTeacher AutoTeacherIncome { get; set; }

    }
}
