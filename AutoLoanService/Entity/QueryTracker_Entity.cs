﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
   public class QueryTracker_Entity
    {
       public int TrackerId { get; set; }
       public int LLId { get; set; }
       public int RefTrackerId { get; set; }
       public int PostingById { get; set; }
       public int QueryToId { get; set; }
       public string Comments { get; set; }
       public DateTime QueryDate { get; set; }
       public string UserAndDepartment { get; set; }
       public string QueriedBy { get; set; }
       public string QueriedOnComments { get; set; }
       //Extended Property
       public int Totalcount { get; set; }
       public int AutoUserTypeId { get; set; }

    }

}
