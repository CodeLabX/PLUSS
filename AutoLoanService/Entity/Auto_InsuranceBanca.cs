﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_InsuranceBanca
    {
        public int InsCompanyId { get; set; }
        public string CompanyName { get; set; }
        public int InsType { get; set; }
        public int InsAgeLimit { get; set; }
        public int InsVehicleCc { get; set; }
        public double InsurancePercent { get; set; }
        public int TotalCount { get; set; }
        public double Tenor { get; set; }
        public double ArtaRate { get; set; }
    }
}
