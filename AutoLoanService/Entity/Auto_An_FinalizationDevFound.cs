﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationDevFound
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public string Age { get; set; }
        public string DBR { get; set; }
        public string SeatingCapacilty { get; set; }
        public string SharePortion { get; set; }

    }
}
