﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_SalariedVariableSalaryMonthIncome
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public int SalaiedID { get; set; }
        public int Month { get; set; }
        public double Amount { get; set; }
    }
}
