﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoBank
    {
       public AutoBank()
       {
       }

       public int BankID { get; set; }
       public string BankCode { get; set; }
       public string BankName { get; set; }
       public int BankStatus { get; set; }

    }
}
