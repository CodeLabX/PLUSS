﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class SCBStatus
    {
        public SCBStatus()
       {
       }

       public int StatusId { get; set; }
       public string StatusName { get; set; }
       public int StatusType { get; set; }
    }
}
