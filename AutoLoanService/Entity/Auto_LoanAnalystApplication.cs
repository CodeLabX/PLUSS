﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_LoanAnalystApplication
    {
        public int Id { get; set; }
        public int AnalystMasterId { get; set; }
        public int Source { get; set; }
        public int BranchId { get; set; }
        public string BranchCode { get; set; }
        public int ValuePack { get; set; }
        public string BorrowingRelationShip { get; set; }
        public string BorrowingRelationShipCode { get; set; }
        public int CustomerStatus { get; set; }
        public int AskingRepaymentMethod { get; set; }
        public int ARTA { get; set; }
        public int ARTAStatus { get; set; }
        public int GeneralInsurance { get; set; }
        public int GeneralInsurancePreference { get; set; }
        public Boolean CashSecured100Per { get; set; }
        public Boolean CCBundle { get; set; }
        public double SecurityValue { get; set; }

        //Extra Property
        public string BranchName { get; set; }
        public string ARTAName { get; set; }


    }
}
