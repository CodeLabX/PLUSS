﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoOwnDamage
    {
        public AutoOwnDamage(){}
        public int OwnDamageId { get; set; }
        public string CCFrom { get; set; }
        public string CCTo { get; set; }
        public double OwnDamageCharge { get; set; }
        public double ActLiability { get; set; }
    }
}
