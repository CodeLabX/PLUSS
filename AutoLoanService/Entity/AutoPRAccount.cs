﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoPRAccount
    {

        public AutoPRAccount()
        {
        }

        public int PrAccountId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public string AccountNumberForSCB { get; set; }
        public string CreditCardNumberForSCB { get; set; }


    }
}
