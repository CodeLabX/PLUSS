﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoLoanFacility
    {
        public AutoLoanFacility()
        {
        }

        public int FacilityId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int FacilityType { get; set; }
        public double InterestRate { get; set; }
        public double PresentBalance { get; set; }
        public double PresentEMI { get; set; }
        public double PresentLimit { get; set; }
        public double ProposedLimit { get; set; }
        public double RepaymentAgrement { get; set; }
        public int Consider { get; set; }
        public double EMIPercent { get; set; }
        public double EMIShare { get; set; }
        public int Status { get; set; }

        //Extra property
        public string FacilityTypeName { get; set; }

        //Security property
        public int NatureOfSecurity { get; set; }
        public double FaceValue { get; set; }
        public double XTVRate { get; set; }
        public string FacilityFlag { get; set; }


    }
}
