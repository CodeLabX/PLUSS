﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoVendor
    {
        public AutoVendor()
        {

        }

        public int AutoVendorId { get; set; }
        public string VendorName { get; set; }
        public int DealerType { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string VendorCode { get; set; }
        public int IsMou { get; set; }

        //selected property
        public string DealerStatus { get; set; }

        // Extra Property
        public int isMOUorNeg { get; set; }
        public decimal IrWithArta { get; set; }
        public decimal IrWithoutArta { get; set; }

    }
}
