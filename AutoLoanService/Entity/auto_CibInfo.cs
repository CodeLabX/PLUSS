﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class auto_CibInfo
    {
        public int CibInfoId { get; set; }
        public string CibInfoName { get; set; }
        public string CibInfoDescription { get; set; }
        public string CibInfoCode { get; set; }

    }
}
