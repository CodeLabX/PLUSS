﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
  public  class Auto_PreviousRePaymentHistory
    {
        public int Id { get; set; }
        public int AutoLoanMasterId { get; set; }
        public int BankStatementId { get; set; }
        public int BankId { get; set; }
        public double OffScb_Total { get; set; }
        public double OnScb_Total { get; set; }
        public double TotalEMIClosingConsidered { get; set; }
        public string Remarks { get; set; }
        public int IncomeSumId { get; set; }
        public List<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB> AutoSubPreviousRepaymentHistoryOffScbOnScb { get; set; }

    }
}
