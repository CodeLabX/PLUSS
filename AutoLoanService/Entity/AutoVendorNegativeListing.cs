﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
   public class AutoVendorNegativeListing
    {
        public AutoVendorNegativeListing()
            {
            }

            public int NegativeVendorId { get; set; }
            public int AutoVendorId { get; set; }
            public DateTime ListingDate { get; set; }
            public string VendorName { get; set; }
            public int Totalcount { get; set; }
       
            
    }
}
