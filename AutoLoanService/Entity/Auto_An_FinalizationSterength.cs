﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class Auto_An_FinalizationSterength
    {
        public int ID { get; set; }
        public int AnalystMasterId { get; set; }
        public string Strength { get; set; }

    }
}
