﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoWorkFlowStatus
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
    }
}
