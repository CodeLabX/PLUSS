﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoPayrollRate
    {
        public AutoPayrollRate()
        {
            
        }

        public int AutoPayRateId { get; set; }
        public int AutoCompanyId { get; set; }
        public string CategoryName { get; set; }
        public string PayrollRate { get; set; }
        public string ProcessingFee { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string Remarks { get; set; }

        //Selected property
        public int TotalCount { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
    }
}
