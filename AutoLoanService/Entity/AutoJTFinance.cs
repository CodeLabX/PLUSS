﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    [Serializable]
    public class AutoJTFinance
    {
        public AutoJTFinance()
        {
        }

        public int JtFinanceId { get; set; }
        public int AutoLoanMasterId { get; set; }
        public string DPISource { get; set; }
        public double DPIAmount { get; set; }
        public string DOISource { get; set; }
        public double DOIAmount { get; set; }
        public double EDRentAndUtilities { get; set; }
        public double EDFoodAndClothing { get; set; }
        public double EDEducation { get; set; }
        public double EDLoanRePayment { get; set; }
        public double EDOther { get; set; }
        public string RepaymentTo { get; set; }
        public string PaymentFor { get; set; }

    }
}
