﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
   public  class AutoMenufacturer
    {
       public AutoMenufacturer()
       {
       }

       public int ManufacturerId { get; set; }
       public string ManufacturerName { get; set; }
       public string ManufacturCountry { get; set; }
       public string Remarks { get; set; }
       public string ManufacturerCode { get; set; }

    }
}
