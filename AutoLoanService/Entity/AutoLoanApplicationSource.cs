﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoLoanApplicationSource
    {
        public AutoLoanApplicationSource()
        {
        }

        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceRegionName { get; set; }
    }
}
