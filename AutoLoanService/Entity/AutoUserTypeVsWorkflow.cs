﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
    public class AutoUserTypeVsWorkflow
    {
        public int ID { get; set; }
        public int UserTypeID { get; set; }
        public int WorkFlowID { get; set; }
        public string StatusName { get; set; }
    }
}
