﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoLoanService.Entity
{
  public  class Auto_ChamberIncome
    {
        public int Id { get; set; }
        public int AutoPracticingDoctorId { get; set; }
        public int ChamberId { get; set; }
        public int OldPatents { get; set; }
        public double OldFee { get; set; }
        public double OldTotal { get; set; }
        public int NewPatents { get; set; }
        public double NewFee { get; set; }
        public double NewTotal { get; set; }
        public double Total { get; set; }
        public int DayMonth { get; set; }
        public double Income { get; set; }
        public int AutoLoanMasterId { get; set; }
        

    }
}
