﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Interface;
using AutoLoanService.Entity;

namespace AutoLoanService
{
    public class InsurenceService
    {
        private Auto_InsuranceBancaRepository repository { get; set; }

        public InsurenceService()
        {

        }

        public InsurenceService(Auto_InsuranceBancaRepository repository)
        {
            this.repository = repository;
        }

        public List<Auto_InsuranceBanca> GetInsuranceAndBancaSettingsSummary(int pageNo, int pageSize, string search)
        {
            return repository.GetInsuranceAndBancaSettingsSummary(pageNo, pageSize, search);
        }

        public string SaveAuto_InsuranceBanca(Auto_InsuranceBanca insurance)
        {
            return repository.SaveAuto_InsuranceBanca(insurance);
        }

        public string InsuranceDeleteById(int insurenceId)
        {
            return repository.InsuranceDeleteById(insurenceId);
        }

        public string SaveAutoInsurenceCalculationsettings(List<AutoOwnDamage> objOwnDamageList, AutoOthercharge objOtherCharge)
        {
            return repository.SaveAutoInsurenceCalculationsettings(objOwnDamageList, objOtherCharge);
        }

        public List<AutoOwnDamage> GetOwnDamage()
        {
            return repository.GetOwnDamage();
        }

        public AutoOthercharge GetOtherDamage()
        {
            return repository.GetOtherDamage();
        }
    }
}
