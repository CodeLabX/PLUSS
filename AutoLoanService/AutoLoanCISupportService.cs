﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace AutoLoanService
{
    public class AutoLoanCISupportService
    {

        private AutoLoanCISupportRepository repository { get; set; }

        public AutoLoanCISupportService()
        {

        }

        public AutoLoanCISupportService(AutoLoanCISupportRepository repository)
        {
            this.repository = repository;
        }

        public string SaveCISupportLoanApp(Auto_LoanAnalystMaster autoAnalistMaster,
            Auto_LoanAnalystApplication autoLoanAnalystApplication, Auto_LoanAnalystVehicleDetail autoLoanAnalystVehicleDetail,
            List<AutoApplicantSCBAccount> autoApplicantAccountSCBList, List<AutoPRAccountDetail> autoApplicantAccountOtherList,
            List<AutoLoanFacility> autoFacilityList, List<AutoLoanSecurity> autoSecurityList, List<AutoLoanFacilityOffSCB> oFFSCBFacilityList,
            int userID, AutoLoanVehicle objAutoLoanVehicle, AutoVendor objAutoVendor)
        {
            return repository.SaveCISupportLoanApp(autoAnalistMaster, autoLoanAnalystApplication, autoLoanAnalystVehicleDetail, autoApplicantAccountSCBList, autoApplicantAccountOtherList, autoFacilityList, autoSecurityList, oFFSCBFacilityList, userID, objAutoLoanVehicle, objAutoVendor);
        }







    }
}
