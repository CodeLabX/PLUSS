﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AzUtilities;
using Bat.Common;

namespace LoanLocatorDataService
{
    public class LoanReportDataService
    {
        public DataTable GetDuplicateSearch(string start, string to, string productId, string stateId)
        {
            var data = new DataTable();
            var productCondition = " and PROD_ID=" + productId;
            if (productId != "0")
                try
                {
                    var mSQL = string.Format(@"SELECT  ID,LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,CAST(REMARKS AS NVARCHAR(100)) REMARKS, count(*) as noofCount
                    FROM loan_app_details where LOAN_APPL_ID in(select LOAN_APPL_ID from loc_loan_app_info 
                    where APPL_DATE  between '{0}' and '{1}'{2})
                    group by LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID,ID,PERS_ID,DEPT_ID,CAST(REMARKS AS NVARCHAR(100)) 
                    HAVING count(*)>0  
                    ORDER BY noofCount desc", start, to, productCondition);
                    data = DbQueryManager.GetTable(mSQL);

                }
                catch (Exception)
                {
                    throw;
                }
            return data;
        }
        public void DeleteDuplicateData(string startDate, string endDate, string product)
        {
            var duplicateId = GetDuplicateRawData(startDate, endDate, product);
            string id = "";
            var dup = "";
            for (int i = 0; i < duplicateId.Rows.Count; i++)
            {

                id = Convert.ToInt32(duplicateId.Rows[i]["LOAN_APPL_ID"]).ToString();
                if (dup == "")
                {
                    dup = id;
                }
                dup += ("," + id);
            }

            if (duplicateId.Rows.Count > 0)
            {
                string dSQL = "DELETE FROM loan_app_details WHERE ID in (" + dup + ")";
                DbQueryManager.ExecuteNonQuery(dSQL);
            }
            string sql = String.Format(@"DELETE FROM loan_app_details_temp WHERE ID in (SELECT ID FROM (SELECT a.* FROM
	(SELECT top 1 ID,LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID from loan_app_details_temp ORDER BY ID desc) a
	INNER JOIN 
	(SELECT top 1 ID,LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID, count(*) as n
		  FROM loan_app_details_temp
		  group by LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID, ID
		  HAVING  count(*)>1
	ORDER BY n desc,LOAN_APPL_ID asc ,ID desc) AS b
	ON a.LOAN_APPL_ID=b.LOAN_APPL_ID AND a.DATE_TIME=b.DATE_TIME
	AND a.PROCESS_STATUS_ID=b.PROCESS_STATUS_ID
	AND a.ID!=b.ID
	)C)");
            DbQueryManager.ExecuteNonQuery(sql);
        }
        public int cleanDuplicateData(string start, string to, string productId, string stateId)
        {
            var productCondition = "";
            var statusCondition = "";
            int result1 = 0;
            var dSQL = "";
            if (productId != null)
            {
                productCondition = " and PROD_ID=" + productId;

            }
            else
            {
                productCondition = "";
            }

            if (stateId != "")
            {

                statusCondition = " ";
            }
            else
            {
                statusCondition = "";
            }
            var DuplicateId = GetDuplicateRawData(start, to, productId);
            string id = "";
            var dup = "";
            for (int i = 0; i < DuplicateId.Rows.Count; i++)
            {

                id = Convert.ToInt32(DuplicateId.Rows[i]["LOAN_APPL_ID"]).ToString();
                if (dup == "")
                {
                    dup = id;
                }
                dup += ("," + id);
            }

            if (DuplicateId.Rows.Count > 0)
            {
                dSQL = "DELETE FROM loan_app_details WHERE ID in (" + dup + ")";
                result1 = DbQueryManager.ExecuteNonQuery(dSQL);
            }
            dSQL = String.Format("DELETE FROM loan_app_details WHERE ID in (SELECT ID FROM (SELECT a.* FROm " +
                 "(SELECT top 1 ID,LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID from loan_app_details ORDER BY ID desc) a" +
                 " INNER JOIN (SELECT top 1 ID,LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS, count(*) as noofCount " +
                 "FROM loan_app_details where LOAN_APPL_ID in(select LOAN_APPL_ID from loc_loan_app_info " +
                 "where APPL_DATE between '" + start + "' and '" + to + "' {0}) group by" +
                 " LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID,ID,PERS_ID,DEPT_ID,REMARKS  HAVING count(*)>1 " +
                 "ORDER BY noofCount desc,LOAN_APPL_ID asc ,ID desc) AS b " +
                 "ON a.LOAN_APPL_ID=b.LOAN_APPL_ID AND a.DATE_TIME=b.DATE_TIME " +
                     "AND a.PROCESS_STATUS_ID=b.PROCESS_STATUS_ID AND a.ID!=b.ID)C)", productCondition);
            int result2 = DbQueryManager.ExecuteNonQuery(dSQL);

            if (result1 == 1 || result2 == 1)
            {
                return 1;
            }

            return 0;
        }

        public DataTable GetDuplicateRawData(string start, string to, string productId)
        {
            var data = new DataTable();
            var productCondition = " and PROD_ID=" + productId;
            if (productId != "0")
                try
                {
                    var mSQL = string.Format(@"SELECT  ID,LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS, 'OD' as noofCount
                    FROM loan_app_details where LOAN_APPL_ID in(select LOAN_APPL_ID from loc_loan_app_info 
                    where APPL_DATE between '{0}' and '{1}' {2})
                    ORDER BY ID ASC", start, to, productCondition);
                    data = DbQueryManager.GetTable(mSQL);

                }
                catch (Exception)
                {
                    throw;
                }
            return data;
        }

        public DataTable GetExpImp(int productId, int departmentId, int processId, string startDate, string endDate)
        {
            var data = new DataTable();
            var productCondition = "";
            var departmentCondition = "";
            var loanStatuscond = "";
            if (productId != 0)
            {
                productCondition = " and L.PROD_ID=" + productId;
            }
            else
            {
                productCondition = "";
            }
            if (departmentId != 0)
            {
                departmentCondition = " and LD.DEPT_ID=" + departmentId;
            }
            else
            {
                departmentCondition = "";
            }
            if (processId != 0)
            {
                loanStatuscond = " and LD.PROCESS_STATUS_ID=" + processId;
            }
            else
            {
                loanStatuscond = "";
            }

            var dateCondition = " and LD.DATE_TIME between '" + startDate + "' and '" + endDate + " 23:59" + "'";

            try
            {


                #region old

                //                var query = string.Format(@"select LD.LOAN_APPL_ID AS LOAN_APPL_ID,P.PROD_NAME,LD.DATE_TIME AS DATE_TIME,
                //                count(LD.LOAN_APPL_ID) noofCount,(select top 1 ID from loan_app_details as LI where LD.LOAN_APPL_ID=LI.LOAN_APPL_ID  group by 
                //                LI.LOAN_APPL_ID,ID ) as ID,(select PROCESS_STATUS_ID from loan_app_details as LS where LD.ID=LS.ID 
                //                group by PROCESS_STATUS_ID) as PROCESS_STATUS_ID,LOAN_STATUS_NAME, (select  REMARKS from loan_app_details as LR where 
                //                LD.ID=LR.ID) as REMARKS,(select DEPT_ID from loan_app_details as LDP where LD.ID=LDP.ID group by DEPT_ID) 
                //                as DEPT_ID,D.LOAN_DEPT_NAME,L.CUST_NAME, (select NAME_LEARNER from learner_details_table as LNR where 
                //                LD.PERS_ID=LNR.ID_LEARNER group by NAME_LEARNER) as PERSON_NAME from loan_app_details as LD left join 
                //                loan_status_info as LSN on LD.PROCESS_STATUS_ID=LSN.LOAN_STATUS_ID left join loan_dept_info as D on 
                //                LD.DEPT_ID=D.LOAN_DEPT_ID left join loc_loan_app_info as L on LD.LOAN_APPL_ID=L.LOAN_APPL_ID left join 
                //                t_pluss_product as P on L.PROD_ID=P.PROD_ID  where 1=1 {0}{1}{2}{3} group by LD.DATE_TIME,LD.LOAN_APPL_ID,
                //                PROD_NAME,LD.ID,LSN.LOAN_STATUS_NAME,D.LOAN_DEPT_NAME,L.CUST_NAME,LD.PERS_ID  order 
                //                by noofCount desc", productCondition, departmentCondition, loanStatuscond, dateCondition);

                #endregion

                var query = string.Format(@"select LD.LOAN_APPL_ID AS LLID,P.PROD_NAME as [Product Name],
                D.LOAN_DEPT_NAME as [Department Name], L.CUST_NAME as [Customer Name], LOAN_STATUS_NAME as [Process Status], 
                LD.DATE_TIME AS [Date Time],(select  REMARKS from loan_app_details as LR where LD.ID=LR.ID) as REMARKS,
                (select NAME_LEARNER from learner_details_table as LNR where 
                LD.PERS_ID=LNR.ID_LEARNER group by NAME_LEARNER) as [Person Name] 
                from loan_app_details as LD left join 
                loan_status_info as LSN on LD.PROCESS_STATUS_ID=LSN.LOAN_STATUS_ID left join loan_dept_info as D on 
                LD.DEPT_ID=D.LOAN_DEPT_ID left join loc_loan_app_info as L on LD.LOAN_APPL_ID=L.LOAN_APPL_ID left join 
                t_pluss_product as P on L.PROD_ID=P.PROD_ID  where 1=1 {0}{1}{2}{3} group by LD.DATE_TIME,LD.LOAN_APPL_ID,
                PROD_NAME,LD.ID,LSN.LOAN_STATUS_NAME,D.LOAN_DEPT_NAME,L.CUST_NAME,LD.PERS_ID ", productCondition, departmentCondition, loanStatuscond, dateCondition);

                LogFile.DebugWriteLine(query);

                data = DbQueryManager.GetDataTable(query, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        public int DeleteTable1()
        {
            var query = string.Format(@"DELETE FROM loan_app_details_temp");
            int status = DbQueryManager.ExecuteNonQuery(query);
            return status;
        }

        public int DeleteTable2()
        {
            var query = string.Format(@"DELETE FROM loan_app_details_temp where FORMAT( date_time,'YYYY-MM-DD HH:MI:SS')='0000-00-00 00:00:00'");
            int status = DbQueryManager.ExecuteNonQuery(query);
            return status;
        }

        public int InsertData(string startDate, string endDate, int productId)
        {
            string productCondition;
            if (productId > 0)
            {
                productCondition = " and PROD_ID=" + productId;

            }
            else
            {
                productCondition = "";
            }

        //    var query = string.Format(@"insert into loan_app_details_temp 
        //select LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,DEFE_DECL_REAS_ID,TAT,TAT_VARI
        //from loan_app_details where LOAN_APPL_ID in(select  LOAN_APPL_ID from loc_loan_app_info 
        //where APPL_DATE between '{0}' and '{1}' {2})", startDate, endDate, productCondition);

            var query = string.Format(@"
INSERT INTO [dbo].[loan_app_details_temp]
           ([LOAN_APPL_ID]
           ,[PERS_ID]
           ,[DEPT_ID]
           ,[DATE_TIME]
           ,[PROCESS_STATUS_ID]
           ,[REMARKS]
           ,[DEFE_DECL_REAS_ID]
           ,[TAT]
           ,[TAT_VARI])
        select LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,DEFE_DECL_REAS_ID,TAT,TAT_VARI
        from loan_app_details where LOAN_APPL_ID in(select  LOAN_APPL_ID from loc_loan_app_info 
        where APPL_DATE between '{0}' and '{1}' {2})", startDate, endDate, productCondition);
            int status = DbQueryManager.ExecuteNonQuery(query);
            return status;
        }

        public int InsertDataAfterRoleEnhancement(string startDate, string endDate, int productId)
        {
            string productCondition;
            if (productId > 0)
            {
                productCondition = " and PROD_ID=" + productId;

            }
            else
            {
                productCondition = "";
            }
            var query = string.Format(@"
INSERT INTO [dbo].[loan_app_details_temp]
           ([LOAN_APPL_ID]
           ,[MakerId]
           ,[RoleName]
           ,[DATE_TIME]
           ,[PROCESS_STATUS_ID]
           ,[REMARKS]
           ,[DEFE_DECL_REAS_ID]
           ,[TAT]
           ,[TAT_VARI])
SELECT a.LOAN_APPL_ID
	,a.MakerId
	,c.Name RoleName
	,a.DATE_TIME
	,a.PROCESS_STATUS_ID
	,a.REMARKS
	,a.DEFE_DECL_REAS_ID
	,a.TAT
	,a.TAT_VARI
FROM loan_app_details a
LEFT JOIN t_pluss_user b ON a.MakerId = b.USER_CODE
LEFT JOIN Roles c ON b.USER_LEVEL = c.Id
WHERE a.LOAN_APPL_ID IN (
		SELECT LOAN_APPL_ID
		FROM loc_loan_app_info
		WHERE APPL_DATE BETWEEN '{0}'
				AND '{1}' {2}
		)
", startDate, endDate, productCondition);

            LogFile.WriteLine(query);

            int status = DbQueryManager.ExecuteNonQuery(query);
            return status;
        }

        public DataTable SearchData()
        {
            var query = string.Format(@"SELECT a.loan_appl_id,

(select max(date_time) from loan_app_details_temp tcib where a.loan_appl_id=tcib.loan_appl_id 
and tcib.process_status_id=19  and tcib.remarks like '%Report%' group by tcib.process_status_id) 
	as cib_report_receive_time,

(select min(date_time) from loan_app_details_temp b where a.loan_appl_id=b.loan_appl_id 
and b.process_status_id=1 group by b.process_status_id) 
	as sales_start_time1,

(select min(date_time) from loan_app_details_temp c where c.process_status_id=10 and 
c.dept_id in(1,2,3) and a.loan_appl_id=c.loan_appl_id group by c.process_status_id) 
	as sales_end_time1,

(select min(date_time) from loan_app_details_temp d where a.loan_appl_id=d.loan_appl_id 
and d.process_status_id=4 and d.dept_id=5 group by d.process_status_id) 
	as ops1_start_time1,

(select min(date_time) from loan_app_details_temp f where a.loan_appl_id=f.loan_appl_id and f.dept_id=5 and f.process_status_id in(3,13,17) and 
	f.date_time< (select max(date_time) from loan_app_details_temp i where a.loan_appl_id=i.loan_appl_id and i.dept_id=5 and i.process_status_id in(8,12)) and 
	f.date_time< (select min(date_time) from loan_app_details_temp p where a.loan_appl_id=p.loan_appl_id and p.dept_id=4 and p.process_status_id in(2,5,15) ) ) 
	as ops1_hold_4sales_tosource_start_time1,

(select max(date_time) from loan_app_details_temp g where a.loan_appl_id=g.loan_appl_id and g.dept_id=5 and g.process_status_id=4 and 
	g.date_time> (select min(date_time) from loan_app_details_temp h where a.loan_appl_id=h.loan_appl_id and h.dept_id=5 and h.process_status_id in(3,13,17)) and 
	g.date_time< (select max(date_time) from loan_app_details_temp i where a.loan_appl_id=i.loan_appl_id and i.dept_id=5 and i.process_status_id in(8,12)) and 
	g.date_time<(select min(date_time) from loan_app_details_temp p where a.loan_appl_id=p.loan_appl_id and p.dept_id=4 and p.process_status_id in(2,5,15) ) group by g.process_status_id) 
	as ops1_hold_4sales_tosource_end_time1,

(select max(date_time) from loan_app_details_temp i where a.loan_appl_id=i.loan_appl_id and i.dept_id=5 and i.process_status_id in(8,12) and 
	i.date_time<(select min(date_time) from loan_app_details_temp p where a.loan_appl_id=p.loan_appl_id and p.dept_id=4 and p.process_status_id in(2,5,15) )) 
	as ops1_final_to_credit_end_time1,


(select min(date_time) from loan_app_details_temp k where a.loan_appl_id=k.loan_appl_id and k.dept_id=4 and k.process_status_id=4 and 
	k.date_time> (select max(date_time) from loan_app_details_temp i where a.loan_appl_id=i.loan_appl_id and i.dept_id=5 and i.process_status_id in(8,12) and 
	i.date_time< (select min(date_time) from loan_app_details_temp p where a.loan_appl_id=p.loan_appl_id and p.dept_id=4 and p.process_status_id in(2,5,15) )) group by k.process_status_id) 
	as credit_rciv_start_time1,

(select distinct loan_status_name from loan_status_info lsa where lsa.loan_status_id =(select distinct process_status_id from loan_app_details_temp n  where a.loan_appl_id=n.loan_appl_id and 
	n.date_time=(select min(date_time) from loan_app_details_temp o where a.loan_appl_id=o.loan_appl_id and o.dept_id=4 and o.process_status_id in(2,5,15)) ) ) 
	as credit_dcsi_name1,

(select min(date_time) from loan_app_details_temp p where a.loan_appl_id=p.loan_appl_id and p.dept_id=4 and p.process_status_id in(2,5,15) ) 
	as credit_dcsi_end_time1,

(select min(date_time) from loan_app_details_temp pa where a.loan_appl_id=pa.loan_appl_id and pa.dept_id=4 and pa.process_status_id in(3,17) and
	pa.date_time< (select min(date_time) from loan_app_details_temp pb where a.loan_appl_id=pb.loan_appl_id and pb.dept_id=4 and pb.process_status_id in(2,5,15)) )  
	as credit_hold_4sales_tosource_start_time1,

(select distinct loan_status_name from loan_status_info lsa where lsa.loan_status_id =(select distinct process_status_id from loan_app_details_temp naa  where a.loan_appl_id=naa.loan_appl_id and 
	naa.date_time= (select min(date_time) from loan_app_details_temp oa where a.loan_appl_id=oa.loan_appl_id and oa.dept_id=4 and oa.process_status_id in(3,17) and 
	oa.date_time< (select min(date_time) from loan_app_details_temp pb where a.loan_appl_id=pb.loan_appl_id and pb.dept_id=4 and pb.process_status_id in(2,5,15)))) ) 
	as credit_hold_4sales_tosource_name1,

(select distinct remarks from loan_app_details_temp nb  where a.loan_appl_id=nb.loan_appl_id and 
	nb.date_time= (select min(date_time) from loan_app_details_temp ob where a.loan_appl_id=ob.loan_appl_id and ob.dept_id=4 and ob.process_status_id in(3,17) and 
	ob.date_time< (select min(date_time) from loan_app_details_temp pb where a.loan_appl_id=pb.loan_appl_id and pb.dept_id=4 and pb.process_status_id in(2,5,15))) ) 
	as credit_hold_4sales_tosource_remarks1,

(select max(date_time) from loan_app_details_temp pa where a.loan_appl_id=pa.loan_appl_id and pa.dept_id=4 and pa.process_status_id=4 and 
	pa.date_time> (select min(date_time) from loan_app_details_temp pc where a.loan_appl_id=pc.loan_appl_id and pc.dept_id=4 and pc.process_status_id in(3,17)) and 
	pa.date_time< (select min(date_time) from loan_app_details_temp pb where a.loan_appl_id=pb.loan_appl_id and pb.dept_id=4 and pb.process_status_id in(2,5,15)) group by pa.process_status_id) 
	as credit_hold_4sales_tosource_end_time1,


(select min(date_time) from loan_app_details_temp q where a.loan_appl_id=q.loan_appl_id and q.dept_id=4 and q.process_status_id=4 and 
	q.date_time>(select min(date_time) from loan_app_details_temp r where a.loan_appl_id=r.loan_appl_id and r.dept_id=4 and r.process_status_id in(2,5,15))) 
	as credit_rciv_start_time2,

(select distinct loan_status_name from loan_status_info lsa where lsa.loan_status_id =(select distinct process_status_id from loan_app_details_temp s where a.loan_appl_id=s.loan_appl_id and 
	s.date_time=(select max(date_time) from loan_app_details_temp w where a.loan_appl_id=w.loan_appl_id and w.dept_id=4 and w.process_status_id in(5,15) and 
	w.date_time>(select min(date_time) from loan_app_details_temp q where a.loan_appl_id=q.loan_appl_id and q.dept_id=4 and q.process_status_id=4 and 
	q.date_time>(select min(date_time) from loan_app_details_temp r where a.loan_appl_id=r.loan_appl_id and r.dept_id=4 and r.process_status_id in(2,5,15) )   )    )) ) 
	as credit_dcsi_name2,

(select max(date_time) from loan_app_details_temp w where a.loan_appl_id=w.loan_appl_id and w.dept_id=4 and w.process_status_id in(5,15) and 
	w.date_time>(select min(date_time) from loan_app_details_temp q where a.loan_appl_id=q.loan_appl_id and q.dept_id=4 and q.process_status_id=4 and 
	q.date_time>(select min(date_time) from loan_app_details_temp r where a.loan_appl_id=r.loan_appl_id and r.dept_id=4 and r.process_status_id in(2,5,15) )   )    )      
	as credit_dcsi_end_time2,

(select min(date_time) from loan_app_details_temp w where a.loan_appl_id=w.loan_appl_id and w.dept_id=4 and w.process_status_id in(3,17) and 
	w.date_time>(select min(date_time) from loan_app_details_temp q where a.loan_appl_id=q.loan_appl_id and q.dept_id=4 and q.process_status_id=4 and 
	q.date_time>(select min(date_time) from loan_app_details_temp r where a.loan_appl_id=r.loan_appl_id and r.dept_id=4 and r.process_status_id in(2,5,15) )   )    )   
	as credit_hold_4sales_tosource_start_time2,

(select distinct loan_status_name from loan_status_info lsa where lsa.loan_status_id =(select distinct process_status_id from loan_app_details_temp sb where a.loan_appl_id=sb.loan_appl_id and 
	sb.date_time=(select min(date_time) from loan_app_details_temp w where a.loan_appl_id=w.loan_appl_id and w.dept_id=4 and w.process_status_id in(3,17) and 
	w.date_time>(select min(date_time) from loan_app_details_temp q where a.loan_appl_id=q.loan_appl_id and q.dept_id=4 and q.process_status_id=4 and 
	q.date_time>(select min(date_time) from loan_app_details_temp r where a.loan_appl_id=r.loan_appl_id and r.dept_id=4 and r.process_status_id in(2,5,15) )   )    )    ) ) 
	as credit_hold_4sales_tosource_name2,

(select distinct remarks from loan_app_details_temp sa where a.loan_appl_id=sa.loan_appl_id and 
	sa.date_time=(select min(date_time) from loan_app_details_temp wa where a.loan_appl_id=wa.loan_appl_id and wa.dept_id=4 and wa.process_status_id in(3,17) and 
	wa.date_time>(select min(date_time) from loan_app_details_temp qa where a.loan_appl_id=qa.loan_appl_id and qa.dept_id=4 and qa.process_status_id=4 and 
	qa.date_time>(select min(date_time) from loan_app_details_temp ra where a.loan_appl_id=ra.loan_appl_id and ra.dept_id=4 and ra.process_status_id in(2,5,15) )   )    )    ) 
	as credit_hold_4sales_tosource_remarks2,

(select max(date_time) from loan_app_details_temp w where a.loan_appl_id=w.loan_appl_id and w.dept_id=4 and w.process_status_id=4 and 
	w.date_time>(select min(date_time) from loan_app_details_temp w where a.loan_appl_id=w.loan_appl_id and w.dept_id=4 and w.process_status_id in(3,17) and 
	w.date_time>(select min(date_time) from loan_app_details_temp q where a.loan_appl_id=q.loan_appl_id and q.dept_id=4 and q.process_status_id=4 and 
	q.date_time>(select min(date_time) from loan_app_details_temp r where a.loan_appl_id=r.loan_appl_id and r.dept_id=4 and r.process_status_id in(2,5,15) )   )    )    )      
	as credit_hold_4sales_tosource_end_time2,

(select max(date_time) from loan_app_details_temp z where a.loan_appl_id=z.loan_appl_id and z.dept_id=4 and z.process_status_id in(10,11) ) 
	as credit_final_to_loanops_end_time,


(select min(date_time) from loan_app_details_temp aa where a.loan_appl_id=aa.loan_appl_id and aa.dept_id=5 and aa.process_status_id=4 and 
	aa.date_time>(select max(date_time) from loan_app_details_temp ab where a.loan_appl_id=ab.loan_appl_id and ab.dept_id=4 and ab.process_status_id in(10,11)) group by aa.process_status_id) 
	as ops2_rciv_time1,

(select min(date_time) from loan_app_details_temp ae where a.loan_appl_id=ae.loan_appl_id and ae.dept_id=5 and ae.process_status_id in (3,17) and 
	ae.date_time>(select min(date_time) from loan_app_details_temp af where a.loan_appl_id=af.loan_appl_id and af.dept_id=5 and af.process_status_id=4 and 
	af.date_time>(select max(date_time) from loan_app_details_temp ag where a.loan_appl_id=ag.loan_appl_id and ag.dept_id=4 and ag.process_status_id in(10,11))) ) 
	as ops2_hold4sales_start_time1,

(select max(date_time) from loan_app_details_temp ah where a.loan_appl_id=ah.loan_appl_id and ah.dept_id=5 and ah.process_status_id =4 and 
	ah.date_time>=(select min(date_time) from loan_app_details_temp ae where a.loan_appl_id=ae.loan_appl_id and ae.dept_id=5 and ae.process_status_id in (3,17) and 
	ae.date_time>(select min(date_time) from loan_app_details_temp af where a.loan_appl_id=af.loan_appl_id and af.dept_id=5 and af.process_status_id=4 and 
	af.date_time>(select max(date_time) from loan_app_details_temp ag where a.loan_appl_id=ag.loan_appl_id and ag.dept_id=4 and ag.process_status_id in(10,11)))) group by ah.process_status_id) 
	as ops2_hold4sales_end_time1,

(select max(date_time) from loan_app_details_temp ah where a.loan_appl_id=ah.loan_appl_id 
and ah.dept_id=5 and ah.process_status_id =7 group by ah.process_status_id) 
	as ops2_final_disb_end_time1

FROM loan_app_details_temp as a 

group by a.loan_appl_id");

            DataTable dt = DbQueryManager.GetTable(query);
            return dt;
        }


        public void DeleteTempData(int dupId)
        {
            try
            {
                var sql = "";
                if (dupId > 0)
                {
                    sql = string.Format(@"DELETE FROM loan_app_details_temp WHERE ID in ({0});", dupId);
                }
                else
                {
					//sql = string.Format(@"DELETE FROM loan_app_details_temp WHERE ID in (SELECT ID FROM (SELECT a.* FROM
					// (SELECT ID,LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID from loan_app_details_temp ORDER BY ID desc) a
					// INNER JOIN 
					// (SELECT  ID,LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID, count(*) as n
					//    FROM loan_app_details_temp
					//    group by LOAN_APPL_ID,DATE_TIME,PROCESS_STATUS_ID
					//    HAVING n>1
					// ORDER BY n desc,LOAN_APPL_ID asc ,ID desc) AS b
					// ON a.LOAN_APPL_ID=b.LOAN_APPL_ID AND a.DATE_TIME=b.DATE_TIME
					// AND a.PROCESS_STATUS_ID=b.PROCESS_STATUS_ID
					// AND a.ID!=b.ID
					// )C);");

					sql = string.Format(@"
WITH cte
AS (
	SELECT ID
		,LOAN_APPL_ID
		,DATE_TIME
		,PROCESS_STATUS_ID
		,ROW_NUMBER() OVER (
			PARTITION BY LOAN_APPL_ID
			,DATE_TIME
			,PROCESS_STATUS_ID ORDER BY LOAN_APPL_ID
				,DATE_TIME
				,PROCESS_STATUS_ID
			) rownum
	FROM loan_app_details_temp
	)
DELETE
FROM loan_app_details_temp
WHERE ID IN (
		SELECT ID
		FROM (
			SELECT a.*
			FROM (
				SELECT ID
					,LOAN_APPL_ID
					,DATE_TIME
					,PROCESS_STATUS_ID
				FROM loan_app_details_temp
				) a
			INNER JOIN cte AS b ON a.LOAN_APPL_ID = b.LOAN_APPL_ID
				AND a.DATE_TIME = b.DATE_TIME
				AND a.PROCESS_STATUS_ID = b.PROCESS_STATUS_ID
				AND a.ID != b.ID
			) C
		);

");
                }
                //sql += string.Format(@"DELETE FROM loan_app_details_temp where date_time='0000-00-00 00:00:00' ");
                DbQueryManager.ExecuteNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ExportLoanAppInfo(DateTime startDate, DateTime endDate)
        {
            try
            {
                var query = string.Format(@"	SELECT  
		a.LOAN_APPLI_ID, 				
		ISNULL(a.SECON_ID,'') AS SECON_ID,  			
		a.LOAN_STATU_DT, 
		a.LOAN_STATU_ID, 				
		a.BR_ID,				
		UPPER(REPLACE(REPLACE(REPLACE(a.LOAP_SOURCE_PERSON_NAME, CHAR(13), ''), CHAR(10), ''),';',' ')) AS LOAP_SOURCE_PERSON_NAME, 
		a.APPLI_DT,						
		a.RCV_DT, 
		CAST((case when a.PROD_ID = 1 then 'PL' when a.PROD_ID = 16 then 'CC' else Convert(varchar,a.PROD_ID) end) as varchar) as PROD_ID,
		UPPER(REPLACE(REPLACE(REPLACE(a.CUST_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS CUST_NM, 						
		ISNULL(a.ABBRI_NM,'') AS ABBRI_NM,
		ISNULL(a.ACC_NO,'') AS ACC_NO, 				
		ISNULL(a.LOAN_APP_AMT,'') AS LOAN_APP_AMT, 
		ISNULL(a.LOAP_DECLARED_INCOME,'') AS LOAP_DECLARED_INCOME, 		
		ISNULL(a.PURPO,'') AS PURPO, 				
		a.LOAN_AC_NO, 			
		a.TENOR, 
		ISNULL(a.TIN_NO,'') AS TIN_NO, 						
		ISNULL(a.PREFE_REPAY_DT,'') AS PREFE_REPAY_DT, 		
		ISNULL(a.PREVI_LOAN_SCB_TYPE,'') AS PREVI_LOAN_SCB_TYPE, 	
		ISNULL(a.PREVI_LOAN_SCB_AMT,'') AS PREVI_LOAN_SCB_AMT, 
		ISNULL(a.CR_CARD_NO_SCB,'') AS CR_CARD_NO_SCB, 				
		ISNULL(a.CR_CARD_LIMIT,'') AS CR_CARD_LIMIT, 		
		UPPER(REPLACE(REPLACE(REPLACE(a.FATHE_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS FATHE_NM, 			
		UPPER(REPLACE(REPLACE(REPLACE(a.MOTHE_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS MOTHE_NM,
		a.DOB, 							
		UPPER(a.MARIT_STATU) AS MARIT_STATU , 			
		a.GENDE, 				
		a.NO_DEPEN, 
		a.ID_TYPE, 						
		a.ID_NO, 				
		a.ID_EXPIR, 			
		UPPER(a.HIGH_EDU_LEVEL ) AS HIGH_EDU_LEVEL, 
		a.EMPLO_STATU_CODE, 			
		UPPER( REPLACE(REPLACE(REPLACE(a.RES_ADD, CHAR(13), ''), CHAR(10), ''),';',' ')) AS RES_ADD, 				
		UPPER( REPLACE(REPLACE(REPLACE(a.OFF_PH, CHAR(13), ''), CHAR(10), ''),';',' ')) AS OFF_PH, 				
		UPPER( REPLACE(REPLACE(REPLACE(a.RES_PH, CHAR(13), ''), CHAR(10), ''),';',' ')) AS RES_PH, 
		UPPER( REPLACE(REPLACE(REPLACE(a.MOBIL, CHAR(13), ''), CHAR(10), ''),';',' '))  AS MOBIL, 						
		UPPER( REPLACE(REPLACE(REPLACE(a.E_MAIL, CHAR(13), ''), CHAR(10), ''),';',' ')) AS E_MAIL, 				
		UPPER( REPLACE(REPLACE(REPLACE(a.YR_CURRE_ADD, CHAR(13), ''), CHAR(10), ''),';',' ')) AS YR_CURRE_ADD, 		
		a.HOME_OWNER, 
		ISNULL(a.RENT_MON,'') AS RENT_MON ,					
		ISNULL(a.PERMA_ADD,'') AS PERMA_ADD, 			
		ISNULL(a.DIREC_PRI_BANK,'') AS DIREC_PRI_BANK, 		
		ISNULL(a.DIREC_BANK_NM,'') AS DIREC_BANK_NM, 
		ISNULL(a.MAILI_ADD,'' ) AS MAILI_ADD, 					
		ISNULL(a.SPOUS_NM,'') AS SPOUS_NM, 			

		a.SPOUS_PROFE, 			
		ISNULL(a.SPOUS_WORK_ADD,'') AS SPOUS_WORK_ADD, 
		ISNULL(a.SPOUS_OFF_PH,'') AS SPOUS_OFF_PH, 				
		ISNULL(a.SPOUS_MOB,'') AS SPOUS_MOB, 			
		a.BUSIN_NATUR, 			
		UPPER( REPLACE(REPLACE(REPLACE(a.COMPA_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS COMPA_NM, 
		UPPER(REPLACE(REPLACE(REPLACE(a.OFF_ADD, CHAR(13), ''), CHAR(10), ''),';',' ')) AS OFF_ADD, 						
		ISNULL(a.FAX ,'') AS FAX, 					
		REPLACE(REPLACE(REPLACE(a.OFF_EMAIL, CHAR(13), ''), CHAR(10), ''),';',' ') AS OFF_EMAIL, 			
		REPLACE(REPLACE(REPLACE(a.DESIG, CHAR(13), ''), CHAR(10), ''),';',' ') AS DESIG, 

		a.MON_CURRE_JOB, 				
		ISNULL(a.EMPLO_STATU,'') AS EMPLO_STATU, 			
		ISNULL(a.PREVI_EMPLO_OCCUP,'') AS PREVI_EMPLO_OCCUP, 	
		ISNULL(a.PREVI_DESIG,'') AS PREVI_DESIG, 
		ISNULL(a.PREVI_EMPLO_YR,'') AS  PREVI_EMPLO_YR,				
		ISNULL(a.TOT_YR,'') AS TOT_YR, 				
		ISNULL(a.BUSSI_ESTAB_DT,'') AS BUSSI_ESTAB_DT, 		
		ISNULL(a.OFF_PREME,'') AS OFF_PREME, 
		ISNULL(a.FACTO_ADD,'') AS FACTO_ADD,			
		ISNULL(a.NO_PROFE,'') AS NO_PROFE, 			
		ISNULL(a.NO_OFFIC,'') AS NO_OFFIC, 			
		ISNULL(a.NO_STAFF,'') AS NO_STAFF, 
		ISNULL(a.TYPE_OWNER,'') AS TYPE_OWNER, 					
		ISNULL(a.EQUIT_SHARE,'') AS EQUIT_SHARE, 			
		ISNULL(a.MAIN_PRODU,'') AS MAIN_PRODU, 			
		ISNULL(a.MAIN_CLIEN,'') AS MAIN_CLIEN, 
		ISNULL(a.MAIN_SUPPL_PRINC,'') AS MAIN_SUPPL_PRINC, 			
		ISNULL(a.SELLI_TERMS,'') AS SELLI_TERMS,			
		ISNULL(a.MAIN_BANKE,'') AS MAIN_BANKE,			
		ISNULL(a. CREDI_FACIL,'') AS CREDI_FACIL, 
		ISNULL(a. OD,'') AS OD,							
		ISNULL(a.CC,'') AS CC,					
		ISNULL(a.TR,'') AS TR,					
		ISNULL(a.HYPO,'') AS HYPO , 
		ISNULL(a.PROJE_LOAN,'') AS PROJE_LOAN,					
		ISNULL(a.SECUR_AGAIN_FACIL,'') AS SECUR_AGAIN_FACIL,	   
		ISNULL(a.YR_TURN_PREVI1,'') AS YR_TURN_PREVI1, 		
		ISNULL(a.YR_TURN_PREVI2,'') AS  YR_TURN_PREVI2, 
		ISNULL(a.YR_TURN_PREVI3,'') AS YR_TURN_PREVI3,				
		ISNULL(a.NET_PROFI_PREVI1,'') AS NET_PROFI_PREVI1,		
		ISNULL(a.NET_PROFI_PREVI2 ,'') AS NET_PROFI_PREVI2,		
		ISNULL(a.NET_PROFI_PREVI3 ,'') AS NET_PROFI_PREVI3, 
		ISNULL(a.MON_SAL,'') AS MON_SAL ,						
		ISNULL(a.MON_RENT,'') AS MON_RENT,				
		ISNULL(a.MON_INTER,'')	AS MON_INTER,		
		ISNULL(a.MON_PROFI_DEVID,'') AS MON_PROFI_DEVID, 
		ISNULL(a.MON_OTHER,'') AS MON_OTHER,					    
		ISNULL(a.PRIMA_ANUAL_INCOM,'') AS PRIMA_ANUAL_INCOM,	    
		ISNULL(a.OTHER_ANUAL_INCOM,'') AS OTHER_ANUAL_INCOM,	
		ISNULL(a.OTHER_INCOM_SOURC,'') AS OTHER_INCOM_SOURC, 
		ISNULL(a.RENT_UTILI,'') AS RENT_UTILI,					
		ISNULL(a.FOOD_CLOTH,'') AS FOOD_CLOTH,			
		ISNULL(a.EDUCA,'') AS EDUCA,				
		ISNULL(a.LOAN_REPAY,'') AS LOAN_REPAY,
		ISNULL(a.OTHER_EXPEN,'') AS OTHER_EXPEN ,					
		a.OTHER_ACC_BANK_ID_1,	
		a.OTHER_ACC_TYPE_1,		
		a.OTHER_ACC_BR_ID_1, 
		a.OTHER_ACC_NO_1,				
		a.OTHER_ACC_BANK_ID_2,	
		a.OTHER_ACC_TYPE_2,		
		a.OTHER_ACC_BR_ID_2, 
		a.OTHER_ACC_NO_2,				
		a.OTHER_ACC_BANK_ID_3,	
		a.OTHER_ACC_TYPE_3,		
		a.OTHER_ACC_BR_ID_3, 
		a.OTHER_ACC_NO_3,				
		ISNULL(a.OTHER_BANK_DURAT,'') AS OTHER_BANK_DURAT,		
		ISNULL(a.OTHER_BANK_NO,'') AS OTHER_BANK_NO ,		
		ISNULL(a.CONSU_BANK_NM,'') AS CONSU_BANK_NM, 
		ISNULL(a.CONSU_LOAN_AMT,'') AS CONSU_LOAN_AMT,				
		ISNULL(a.CONSU_INSTA,'') AS CONSU_INSTA,			
		ISNULL(a.SECUR_BANK_NM,'') AS SECUR_BANK_NM,		
		ISNULL(a.SECUR_LOAN_AMT,'') AS SECUR_LOAN_AMT, 
		ISNULL(a.SECUR_INSTA,'') AS SECUR_INSTA,					
		ISNULL(a.C_CARD_BANK_NM,'') AS C_CARD_BANK_NM,		
		ISNULL(a.C_CARD_LOAN_AMT,'') AS C_CARD_LOAN_AMT,		
		ISNULL(a.C_CARD_INSTA,'') AS C_CARD_INSTA , 
		ISNULL(a.H_BUILD_BANK_NM,'') AS H_BUILD_BANK_NM,				
		ISNULL(a.H_BUILD_LOAN_AMT,'') AS H_BUILD_LOAN_AMT,		
		ISNULL(a.H_BUILD_INSTA,'') AS H_BUILD_INSTA,		
		ISNULL(a.OTHER_LOAN_BANK_NM,'') AS OTHER_LOAN_BANK_NM, 
		ISNULL(a.OTHER_LOAN_AMT,'') AS OTHER_LOAN_AMT,				
		ISNULL(a.OTHER_LOAN_INSTA,'') AS OTHER_LOAN_INSTA,		
		ISNULL(a.LAND_BUILD,'') AS LAND_BUILD ,			
		ISNULL(a.FD_PSP_BSP_SAVING,'') AS FD_PSP_BSP_SAVING, 
		ISNULL(a.WASE_EARNE_BOND,'') AS WASE_EARNE_BOND,				
		ISNULL(a.SHARE_CERTI,'') AS SHARE_CERTI,			
		ISNULL(a.LIFE_INSUR,'') AS LIFE_INSUR,			
		ISNULL(a.LOAN_APRV_AMT,'') AS LOAN_APRV_AMT , 
		ISNULL(a.APPRV_DATE,'') AS APPRV_DATE,					
		ISNULL(a.PERS_ID,'') AS PERS_ID,				
		ISNULL(a.CAR_PRICE,'') AS CAR_PRICE ,			
		ISNULL(a.CAR_BRAND_ID,'') AS CAR_BRAND_ID, 
		ISNULL(a.CAR_ID,'') AS CAR_ID, 			
		ISNULL(a.PURCH_DT,'') AS PURCH_DT,	
		ISNULL(a.CAR_STATUS,'') AS CAR_STATUS ,			
		ISNULL(a.CURRE_VALUE,'') AS CURRE_VALUE, 
		ISNULL(a.VENDO_ID,'') AS VENDO_ID ,						
		ISNULL(a.VENDO_ADD,'') AS VENDO_ADD,			
		ISNULL(a.VENDO_PH,'') AS VENDO_PH,				
		ISNULL(a.VENDO_MOBIL,'') AS VENDO_MOBIL, 
		ISNULL(a.VENDO_CONTA_PERSO,'') AS VENDO_CONTA_PERSO ,			
		ISNULL(a.MFY,'') AS MFY,					
		ISNULL(a.CAR_USAGE,'') AS CAR_USAGE,			
		ISNULL(a.CAR_CC,'') AS CAR_CC, 
		ISNULL(a.CAR_CHASS_NO,'') AS CAR_CHASS_NO,					
		ISNULL(a.PROFE,'') AS PROFE,				
		ISNULL(a.SEG_ID,'') AS SEG_ID ,				
		REPLACE(REPLACE(REPLACE(a.J_APP_NM, CHAR(13), ''), CHAR(10), ''),';',' ') AS J_APP_NM, 
		REPLACE(REPLACE(REPLACE(a.J_APP_FATHE_NM, CHAR(13), ''), CHAR(10), ''),';',' ') AS J_APP_FATHE_NM,				
		REPLACE(REPLACE(REPLACE(a.J_APP_MOTHE_NM, CHAR(13), ''), CHAR(10), ''),';',' ') AS J_APP_MOTHE_NM,		
		ISNULL(a.J_APP_DOB,'') AS J_APP_DOB,			
		ISNULL(a.J_APP_PROFE,'') AS J_APP_PROFE, 
		ISNULL(a.J_APP_EMPLO,'') AS J_APP_EMPLO,					
		ISNULL(a.J_APP_JOIN_DT,'') AS J_APP_JOIN_DT,		
		ISNULL(a.J_APP_FAX,'') AS J_APP_FAX,			
		ISNULL(a.J_APP_OFF_EMAIL,'') AS J_APP_OFF_EMAIL, 
		ISNULL(a.J_APP_EDUCA_LEVEL,'') AS J_APP_EDUCA_LEVEL,			
		ISNULL(a.J_APP_AC_NO,'') AS J_APP_AC_NO, 
		ISNULL(a.J_APP_AC_NO2 ,'') AS J_APP_AC_NO2,			
		ISNULL(a.J_APP_AC_NO3,'') AS J_APP_AC_NO3 , 
		ISNULL(a.J_APP_AC_NO4,'') AS J_APP_AC_NO4 ,					
		ISNULL(a.J_APP_AC_NO5,'') AS J_APP_AC_NO5,			
		ISNULL(a.J_APP_CC_NO,'') AS J_APP_CC_NO,			
		ISNULL(a.J_APP_CC_NO2,'') AS J_APP_CC_NO2, 
		ISNULL(a.J_APP_CC_NO3,'') AS J_APP_CC_NO3,					
		ISNULL(a.J_APP_CC_NO4,'') AS J_APP_CC_NO4,			
		ISNULL(a.J_APP_CC_NO5,'') AS J_APP_CC_NO5 ,			
		ISNULL(a.J_APP_CC_LIMIT,'') AS J_APP_CC_LIMIT , 
		ISNULL(a.J_TIN,'') AS J_TIN ,						
		ISNULL(a.J_APP_CONTACT_NO1,'') AS J_APP_CONTACT_NO1,	
		ISNULL(a.J_APP_CONTACT_NO2,'') AS J_APP_CONTACT_NO2,	
		ISNULL(a.J_APP_CONTACT_NO3,'') AS J_APP_CONTACT_NO3 , 
		ISNULL(a.J_APP_CONTACT_NO4,'') AS J_APP_CONTACT_NO4,			
		ISNULL(a.J_ID_TYPE1,'') AS J_ID_TYPE1,			
		ISNULL(a.J_ID_TYPE2,'') AS J_ID_TYPE2,			
		ISNULL(a.J_ID_TYPE3,'') AS J_ID_TYPE3, 
		ISNULL(a.J_ID_TYPE4,'') AS J_ID_TYPE4 ,					
		ISNULL(a.J_ID_TYPE5,'') AS J_ID_TYPE5 ,			
		ISNULL(a.J_ID_NO1,'') AS J_ID_NO1,				
		ISNULL(a.J_ID_NO2,'') AS J_ID_NO2, 
		ISNULL(a.J_ID_NO3,'') AS J_ID_NO3 ,						
		ISNULL(a.J_ID_NO4,'') AS J_ID_NO4 ,				
		ISNULL(a.J_ID_NO5,'') AS J_ID_EXPIR1,				
		ISNULL(a.J_ID_EXPIR1,'') AS J_ID_EXPIR1, 
		ISNULL(a.J_ID_EXPIR2,'') AS J_ID_EXPIR2,					
		ISNULL(a.J_ID_EXPIR3,'') AS J_ID_EXPIR3 ,			
		ISNULL(a.J_ID_EXPIR4,'') AS J_ID_EXPIR3,			
		ISNULL(a.J_ID_EXPIR5,'') AS J_ID_EXPIR5, 
		ISNULL(a.J2_APP_NM,'') AS J2_APP_NM,					
		ISNULL(a.J2_APP_AC_NO1,'') AS J2_APP_AC_NO1,		
		ISNULL(a.J2_APP_AC_NO2,'') AS J2_APP_AC_NO2,		
		ISNULL(a.J2_APP_AC_NO3,'') AS J2_APP_AC_NO3b , 
		ISNULL(a.J2_APP_AC_NO4,'') AS J2_APP_AC_NO4,				
		ISNULL(a.J2_APP_AC_NO5,'') AS J2_APP_AC_NO5,		
		ISNULL(a.J2_TIN,'') AS J2_TIN,				
		ISNULL(a.J2_APP_CC_NO,'') AS J2_APP_CC_NO, 
		ISNULL(a.J2_APP_CC_NO2,'') AS J2_APP_CC_NO2,				
		ISNULL(a.J2_APP_CC_NO3,'') AS J2_APP_CC_NO3,		
		ISNULL(a.J2_APP_CC_NO4,'') AS J2_APP_CC_NO4,		
		ISNULL(a.J2_APP_CC_NO5,'') AS J2_APP_CC_NO5, 
		ISNULL(a.J2_APP_FATHE_NM,'') AS J2_APP_FATHE_NM,				
		ISNULL(a.J2_APP_MOTHE_NM ,'') AS J2_APP_MOTHE_NM,		
		ISNULL(a.J2_APP_DOB ,'') AS J2_APP_DOB ,			
		ISNULL(a.J2_APP_EDUCA_LEVEL,'') AS J2_APP_EDUCA_LEVEL, 
		ISNULL(a.J2_APP_CONTACT_NO1,'') AS J2_APP_DOB ,			
		ISNULL(a.J2_APP_CONTACT_NO2,'') AS J2_APP_CONTACT_NO2,	
		ISNULL(a.J2_APP_CONTACT_NO3,'') AS J2_APP_CONTACT_NO3,	
		ISNULL(a.J2_APP_CONTACT_NO4,'') AS J2_APP_CONTACT_NO4, 
		ISNULL(a.J2_APP_EMPLO,'') AS J2_APP_EMPLO ,					
		ISNULL(a.J2_ID_TYPE1,'') AS J2_ID_TYPE1,			
		ISNULL(a.J2_ID_TYPE2,'') AS J2_ID_TYPE2,			
		ISNULL(a.J2_ID_TYPE3,'') AS J2_ID_TYPE3, 
		ISNULL(a.J2_ID_TYPE4,'') AS  J2_ID_TYPE4,					
		ISNULL(a.J2_ID_TYPE5,'') AS J2_ID_TYPE5	,	
		ISNULL(a.J2_ID_NO1,'') AS 	J2_ID_NO1,	
		ISNULL(a.J2_ID_NO2,'') AS  J2_ID_NO2,
		ISNULL(a.J2_ID_NO3,'') AS 	J2_ID_NO3,				
		ISNULL(a.J2_ID_NO4,'') AS 	J2_ID_NO4,	
		ISNULL(a.J2_ID_NO5,'') AS J2_ID_NO5,			
		ISNULL(a.J2_ID_EXPIR1,'') AS J2_ID_EXPIR1, 
		ISNULL(a.J2_ID_EXPIR2,'') AS J2_ID_EXPIR2,					
		ISNULL(a.J2_ID_EXPIR3,'') AS J2_ID_EXPIR3,			
		ISNULL(a.J2_ID_EXPIR4,'') AS J2_ID_EXPIR4,			
		ISNULL(a.J2_ID_EXPIR5,'') AS J2_ID_EXPIR5 , 
		ISNULL(a.J3_APP_NM,'') AS J3_APP_NM,					
		ISNULL(a.J3_APP_AC_NO1,'') AS J3_APP_AC_NO1,		
		ISNULL(a.J3_APP_AC_NO2,'') AS J3_APP_AC_NO2,		
		ISNULL(a.J3_APP_AC_NO3,'') AS J3_APP_AC_NO3, 
		ISNULL(a.J3_APP_AC_NO4,'') AS J3_APP_AC_NO4,				
		ISNULL(a.J3_APP_AC_NO5,'') AS J3_APP_AC_NO5,		
		ISNULL(a.J3_TIN,'') AS J3_TIN ,				
		ISNULL(a.J3_APP_CC_NO,'') AS J3_APP_CC_NO, 
		ISNULL(a.J3_APP_CC_NO2,'') AS J3_APP_CC_NO2,				
		ISNULL(a.J3_APP_CC_NO3,'') AS J3_APP_CC_NO3 ,		
		ISNULL(a.J3_APP_CC_NO4,'') AS J3_APP_CC_NO4,		
		ISNULL(a.J3_APP_CC_NO5,'') AS J3_APP_CC_NO5, 
		ISNULL(a.J3_APP_FATHE_NM,'') AS J3_APP_FATHE_NM,				
		ISNULL(a.J3_APP_MOTHE_NM,'') AS J3_APP_MOTHE_NM,		
		ISNULL(a.J3_APP_DOB,'') AS J3_APP_DOB,			
		ISNULL(a.J3_APP_EDUCA_LEVEL,'') AS J3_APP_EDUCA_LEVEL, 
		ISNULL(a.J3_APP_CONTACT_NO1,'') AS J3_APP_CONTACT_NO1,			
		ISNULL(a.J3_APP_CONTACT_NO2,'') AS J3_APP_CONTACT_NO2,	
		ISNULL(a.J3_APP_CONTACT_NO3,'') AS J3_APP_CONTACT_NO3,	
		ISNULL(a.J3_APP_CONTACT_NO4,'') AS J3_APP_CONTACT_NO4, 
		ISNULL(a.J3_APP_EMPLO,'') AS J3_APP_EMPLO,					
		ISNULL(a.J3_ID_TYPE1,'') AS J3_ID_TYPE1,			
		ISNULL(a.J3_ID_TYPE2,'') AS J3_ID_TYPE2 ,			
		ISNULL(a.J3_ID_TYPE3,'') AS J3_ID_TYPE3, 
		ISNULL(a.J3_ID_TYPE4,'') AS J3_ID_TYPE4,		
		ISNULL(a.J3_ID_TYPE5,'')  AS  J3_ID_TYPE5,			
		ISNULL(a.J3_ID_NO1,'') AS J3_ID_NO1,			
		ISNULL(a.J3_ID_NO2,'') AS J3_ID_NO2, 
		ISNULL(a.J3_ID_NO3,'') AS J3_ID_NO3,					
		ISNULL(a.J3_ID_NO4,'') AS J3_ID_NO4 ,			
		ISNULL(a.J3_ID_NO5,'') AS J3_ID_NO5 ,			
		ISNULL(a.J3_ID_EXPIR1,'') AS J3_ID_EXPIR1, 
		ISNULL(a.J3_ID_EXPIR2,'') AS J3_ID_EXPIR2,					
		ISNULL(a.J3_ID_EXPIR3,'') AS J3_ID_EXPIR3,			
		ISNULL(a.J3_ID_EXPIR4,'') AS J3_ID_EXPIR4,			
		ISNULL(a.J3_ID_EXPIR5,'') AS J3_ID_EXPIR5, 
		ISNULL(a.J4_APP_NM,'') AS J4_APP_NM,					
		ISNULL(a.J4_APP_AC_NO1,'') AS J4_APP_AC_NO1,		
		ISNULL(a.J4_APP_AC_NO2,'') AS J4_APP_AC_NO2,		
		ISNULL(a.J4_APP_AC_NO3,'') AS J4_APP_AC_NO3, 
		ISNULL(a.J4_APP_AC_NO4,'') AS J4_APP_AC_NO4,				
		ISNULL(a.J4_APP_AC_NO5,'') AS J4_APP_AC_NO5,		
		ISNULL(a.J4_TIN,'') AS J4_TIN,				
		ISNULL(a.J4_APP_CC_NO,'') AS J4_APP_CC_NO, 
		ISNULL(a.J4_APP_CC_NO2,'') AS J4_APP_CC_NO2 ,				
		ISNULL(a.J4_APP_CC_NO3,'') AS J4_APP_CC_NO3 ,		
		ISNULL(a.J4_APP_CC_NO4,'') AS J4_APP_CC_NO4,		
		ISNULL(a.J4_APP_CC_NO5,'') AS J4_APP_CC_NO5 , 
		ISNULL(a.J4_APP_FATHE_NM,'') AS J4_APP_FATHE_NM,				
		ISNULL(a.J4_APP_MOTHE_NM,'') AS J4_APP_MOTHE_NM,		
		ISNULL(a.J4_APP_DOB,'') AS J4_APP_DOB,			
		ISNULL(a.J4_APP_EDUCA_LEVEL,'') AS J4_APP_EDUCA_LEVEL, 
		ISNULL(a.J4_APP_CONTACT_NO1,'') AS J4_APP_CONTACT_NO1,			
		ISNULL(a.J4_APP_CONTACT_NO2,'') AS J4_APP_CONTACT_NO2,	
		ISNULL(a.J4_APP_CONTACT_NO3,'') AS J4_APP_CONTACT_NO3,	
		ISNULL(a.J4_APP_CONTACT_NO4,'') AS J4_APP_CONTACT_NO4, 
		ISNULL(a.J4_APP_EMPLO,'') AS J4_APP_EMPLO,					
		ISNULL(a.J4_ID_TYPE1,'') AS J4_ID_TYPE1,			
		ISNULL(a.J4_ID_TYPE2,'') AS J4_ID_TYPE2,			
		ISNULL(a.J4_ID_TYPE3,'') AS J4_ID_TYPE3, 
		ISNULL(a.J4_ID_TYPE4,'') AS J4_ID_TYPE4,					
		ISNULL(a.J4_ID_TYPE5,'') AS J4_ID_TYPE5,			
		ISNULL(a.J4_ID_NO1,'') AS J4_ID_NO1,			
		ISNULL(a.J4_ID_NO2,'') AS J4_ID_NO2, 
		ISNULL(a.J4_ID_NO3,'') AS J4_ID_NO3,					
		ISNULL(a.J4_ID_NO4,'') AS J4_ID_NO4,			
		ISNULL(a.J4_ID_NO5,'') AS J4_ID_NO5,			
		ISNULL(a.J4_ID_EXPIR1,'') AS J4_ID_EXPIR1, 
		ISNULL(a.J4_ID_EXPIR2,'') AS J4_ID_EXPIR2,					
		ISNULL(a.J4_ID_EXPIR3,'') AS J4_ID_EXPIR3,			
		ISNULL(a.J4_ID_EXPIR4,'') AS J4_ID_EXPIR4,			
		ISNULL(a.J4_ID_EXPIR5,'') AS J4_ID_EXPIR5, 
		ISNULL(a.ARM_DSE,'') AS ARM_DSE,						
		ISNULL(a.VEHIC_ID,'') AS VEHIC_ID,				
		ISNULL(a.CAR_ENG_NO,'') AS CAR_ENG_NO,			
		ISNULL(a.REF_NM_1,'') AS REF_NM_1 , 
		ISNULL(a.REF_REL_1,'') AS REF_REL_1 ,					
		ISNULL(a.REF_RES_ADD_1,'') AS REF_RES_ADD_1,		
		ISNULL(a.REF_WRK_ADD_1,'') AS REF_WRK_ADD_1,		
		ISNULL(a.REF_RES_PH_1,'') AS REF_RES_PH_1 , 
		ISNULL(a.REF_OFF_PH_1,'') AS REF_OFF_PH_1 ,					
		ISNULL(a.REF_MOB_1,'') AS REF_MOB_1 ,			
		ISNULL(a.REF_NM_2,'') AS REF_NM_2,				
		ISNULL(a.REF_REL_2,'') AS REF_REL_2 , 
		ISNULL(a.REF_RES_ADD_2,'') AS REF_RES_ADD_2,				
		ISNULL(a.REF_WRK_ADD_2,'') AS REF_WRK_ADD_2,		
		ISNULL(a.REF_RES_PH_2,'') AS REF_RES_PH_2,			
		ISNULL(a.REF_OFF_PH_2,'') AS REF_OFF_PH_2, 
		ISNULL(a.REF_MOB_2,'') AS REF_MOB_2,					
		ISNULL(a.PRO_SELEC,'') AS PRO_SELEC,			
		ISNULL(a.PRO_ADD,'') AS PRO_ADD,				
		ISNULL(a.NO_CAR_PAR,'') AS NO_CAR_PAR, 
		ISNULL(a.CAR_PARKI_COST,'') AS CAR_PARKI_COST,				
		ISNULL(a.REG_COST,'') AS REG_COST ,				
		ISNULL(a.UTILI_OTHER_COST,'') AS UTILI_OTHER_COST,		
		ISNULL(a.PRO_NM,'') AS PRO_NM, 
		ISNULL(a.NO_OF_CAR,'') AS NO_OF_CAR,					
		ISNULL(a.PASS_NO,'') AS PASS_NO,				
		ISNULL(a.PASS_EXP,'') AS PASS_EXP,				
		ISNULL(a.VEHIC_TYPE,'') AS VEHIC_TYPE, 
		ISNULL(a.SELLI_TERM_DESC,'') AS SELLI_TERM_DESC,				
		ISNULL(a.BUSI_OCCUP_YR,'') AS BUSI_OCCUP_YR,		
		ISNULL(a.BUSI_TIN,'') AS BUSI_TIN,				
		ISNULL(a.RESIDENCY,'') AS RESIDENCY, 
		ISNULL(a.SPOUS_INCOME,'') AS SPOUS_INCOME,					
		ISNULL(a.SUCCE_NM,'') AS SUCCE_NM,				
		ISNULL(a.SUCCE_REL,'') AS SUCCE_REL,			
		ISNULL(a.SUCCE_DOB,'') AS SUCCE_DOB, 
		ISNULL(a.SUCCE_BUSI_INVOL,'') AS SUCCE_BUSI_INVOL,				
		ISNULL(a.SUCCE_INVOL_YR,'') AS SUCCE_INVOL_YR,		
		ISNULL(a.PREM_STAS,'') AS PREM_STAS,			
		ISNULL(a.YR_IN_CURRE_OFF_ADD,'') AS YR_IN_CURRE_OFF_ADD, 
		a.LOAP_ACNOFORSCB2,				
		a.LOAP_ACNOFORSCB3,		
		a.LOAP_ACNOFORSCB4,		
		a.LOAP_ACNOFORSCB5, 
		a.LOAP_CREDITCARDNO2,			
		a.LOAP_CREDITCARDNO3,	
		a.LOAP_CREDITCARDNO4,	
		a.LOAP_CREDITCARDNO5, 
		a.LOAP_IDTYPE2,					
		a.LOAP_IDTYPE3,			
		a.LOAP_IDTYPE4,			
		a.LOAP_IDTYPE5, 
		REPLACE(REPLACE(REPLACE(a.LOAP_IDNO2, CHAR(13), ''), CHAR(10), ''),';',' ') AS LOAP_IDNO2,					
		REPLACE(REPLACE(REPLACE(a.LOAP_IDNO3, CHAR(13), ''), CHAR(10), ''),';',' ') AS LOAP_IDNO3,			
		REPLACE(REPLACE(REPLACE(a.LOAP_IDNO4, CHAR(13), ''), CHAR(10), ''),';',' ') AS  LOAP_IDNO4,			
		REPLACE(REPLACE(REPLACE(a.LOAP_IDNO5, CHAR(13), ''), CHAR(10), ''),';',' ') AS  LOAP_IDNO5, 
		 a.LOAP_EXPIRYDATE2,				
		 a.LOAP_EXPIRYDATE3,		
		 a.LOAP_EXPIRYDATE4,		
		 a.LOAP_EXPIRYDATE5, 
		REPLACE(REPLACE(REPLACE(a.LOAN_CONTACTNO4, CHAR(13), ''), CHAR(10), ''),';',' ') AS LOAN_CONTACTNO4,				
		REPLACE(REPLACE(REPLACE(a.LOAN_CONTACTNO5, CHAR(13), ''), CHAR(10), ''),';',' ') AS LOAN_CONTACTNO5,		
		a.LOAP_OTHERACWITHBANK_ID4, 
		a.LOAP_OTHERACWITHBANK_ID5, 
		a.LOAP_ORHREACTYPE_ID4,			
		a.LOAP_ORHREACTYPE_ID5, 
		a.LOAP_OTHERACWITHBRANCH_ID4,
		a.LOAP_OTHERACWITHBRANCH_ID5, 
		a.LOAP_OTHERACWITHACNO4,		
		a.LOAP_OTHERACWITHACNO5, 
		a.LOAP_EXTRA_FIELD1,	
		a.LOAP_EXTRA_FIELD2, 
		REPLACE(REPLACE(REPLACE(a.LOAP_EXTRA_FIELD3, CHAR(13), ''), CHAR(10), ''),';',' ') AS LOAP_EXTRA_FIELD3,			
		a.LOAP_EXTRA_FIELD4,	
		a.LOAP_EXTRA_FIELD5,	
		a.LOAP_EXTRA_FIELD6, 
		a.LOAP_EXTRA_FIELD7,			
		REPLACE(REPLACE(REPLACE(a.LOAP_EXTRA_FIELD8, CHAR(13), ''), CHAR(10), ''),';',' ') AS  LOAP_EXTRA_FIELD8,	
		a.LOAP_EXTRA_FIELD9,	
		a.LOAP_EXTRA_FIELD10, 
		a.LOAP_USERID,					
		ISNULL(a.DELI_DEPT_ID,'') AS DELI_DEPT_ID
	FROM  dbo.loan_app_info a
	WHERE	DATEDIFF(day, a.RCV_DT, '{0}' )<=0
		AND DATEDIFF(day, a.RCV_DT, '{1}' )>=0
	ORDER BY LOAN_APPLI_ID ASC", startDate, endDate);
                return DbQueryManager.GetTable(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ExportNegativeList(DateTime startDate, DateTime endDate)
        {
            try
            {
                var query = string.Format(@"
SELECT 
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_BANKCOMPANY, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_BANKCOMPANY,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_BRANCH, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_BRANCH,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_ADDRESS, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_ADDRESS,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_NEGATIVELIST, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_NEGATIVELIST,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_WATCHLIST, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_WATCHLIST,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_EXCLUSION, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_EXCLUSION,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_DELIST, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_DELIST,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_RESTRICTION, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_RESTRICTION,
	UPPER(REPLACE(REPLACE(REPLACE(a.NELE_REMARKS, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NELE_REMARKS,
	a.NELE_INCLUSIONDATE,
	a.NELE_CHANGEDATE,
	a.NELE_ENTRYDATE,
	a.NELE_USER_ID
FROM dbo.t_pluss_negativelistemployer a
WHERE	
	DATEDIFF(day, a.NELE_ENTRYDATE, '{0}' )<=0
	AND DATEDIFF(day, a.NELE_ENTRYDATE, '{1}' )>=0", startDate, endDate);
                var dt = DbQueryManager.GetTable(query);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
   
        public DataTable ExportDedupInfo(DateTime startDate, DateTime endDate)
        {
            try
            {
                var query = string.Format(@"
SELECT  
		DE_DUP_ID, 
		UPPER(REPLACE(REPLACE(REPLACE(a.NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS NM, 
		UPPER(REPLACE(REPLACE(REPLACE(a.FATHE_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS FATHE_NM, 
		UPPER(REPLACE(REPLACE(REPLACE(a.MOTHE_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS MOTHE_NM, 
		a.SCB_MST_NO, 
		a.DOB, 
		a.PROFE, 
		a.PROD_ID, 
		a.DECLI_REASO, 
		a.DECLI_DT, 
		UPPER(REPLACE(REPLACE(REPLACE(a.BUSIN_EMPLO_NM, CHAR(13), ''), CHAR(10), ''),';',' ')) AS BUSIN_EMPLO_NM, 
		UPPER(REPLACE(REPLACE(REPLACE(a.ADD1, CHAR(13), ''), CHAR(10), ''),';',' '))AS ADD1, 
		UPPER(REPLACE(REPLACE(REPLACE(a.ADD2, CHAR(13), ''), CHAR(10), ''),';',' '))AS ADD2, 
		a.PH1, 
		a.PH2, 
		a.MOB1, 
		a.MOB2, 
		a.BANK_BRANC, 
		a.SOURC, 
		a.STATU, 
		a.REMAR AS REMAR, 
		a.TIN,
		a.ID_TYPE, 
		a.DEDU_IDTYPE2, 
		a.DEDU_IDTYPE3, 
		a.DEDU_IDTYPE4, 
		a.DEDU_IDTYPE5, 
		a.ID_NO, 
		a.DEDU_IDNO2, 
		a.DEDU_IDNO3, 
		a.DEDU_IDNO4, 
		a.DEDU_IDNO5, 
		a.DEDU_RESIDENTIALSTATUS, 
		a.DEDU_EDUCATIONLEVEL, 
		a.DEDU_MARITALSTATUS, 
		a.CRITE_TYPE, 
		a.ENTRY_DT, 
		a.ACTIV, 
		a.DEDU_LOANACCOUNTNO
FROM dbo.de_dup_info a
WHERE	DATEDIFF(day, a.ENTRY_DT, '{0}' )<=0
		AND DATEDIFF(day, a.ENTRY_DT, '{1}' )>=0 ", startDate, endDate);
                return DbQueryManager.GetTable(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

     
    
    }
}



