﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Bat.Common;
using AzUtilities;
using BusinessEntities;
using Utilities;

namespace LoanLocatorDataService
{
    public class LocatorApplicationDataService
    {
        public DataTable GetCheckList(int prodId)
        {
            var sql = string.Format(@"select CHECK_LIST_ID as Id,CHECK_LIST_TITLE as Name from check_list_info where STATUS='1' and PROD_ID={0}", prodId);
            var data = DbQueryManager.GetTable(sql);
            return data;
        }

        public UserLoanDept GetUserLoanDepartment(int userId)
        {
            //var deptQuery = string.Format(@"select * from loan_dept_info where UserId={0}", userId);
            //var deptQuery = string.Format(@"select ID_LEARNER as UserId,LOAN_DEPT_ID as LoanDeptId from learner_details_table where ID_LEARNER={0}", userId);

            string deptQuery = string.Format(@"
SELECT a.USER_ID UserId, b.LOAN_DEPT_TYPE LoanDeptId FROM t_pluss_user a 
INNER JOIN Roles b ON a.USER_LEVEL = b.Id
WHERE a.USER_ID='{0}'", userId);
            return Data<UserLoanDept>.DataSource(deptQuery).FirstOrDefault();
        }

        public int SaveLoanAppInfo(Loan loan, UserLoanDept userLoanDept)
        {


            var loanId = 0;
            var accNo = loan.PreAccNo + "-" + loan.MidAccNo + "-" + loan.PostAccNo;
            var sql = string.Empty;
            if (loan.ForCrd == 1 && loan.ProductId == 2)
            {
                sql = string.Format(@"insert into loc_loan_app_info(RECE_DATE, PROD_ID,APPL_DATE ,ACC_NUMB ,CUST_NAME ,LOAN_APP_AMOU,PERS_ID,ACC_TYPE ,ORIG_DEPT_ID,DELI_DEPT_ID,EMPL_TYPE,ORGAN,BR_ID,PRE_DECLIEND,PROD_OTHER_INFO,LOAN_STATUS_ID,ARM_CODE,MakerID) values
                         ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8},{9},'{10}','{11}',{12},'{13}','{14}',{15},'{16}','{17}')",
                     loan.ApplicationDate, loan.ProductId, loan.SubmissionDate, accNo, loan.CustomerName, loan.AppliedAmount, loan.SubmitedBy, loan.AccType, userLoanDept.LoanDeptId, userLoanDept.LoanDeptId, loan.EmployeeType, loan.Organization, loan.EntryBranch, loan.PreDeclined, loan.AutoDelarName, loan.LoanState, loan.ARMCode, loan.MakerID);
            }
            else if (loan.ForCrd == 1)
            {
                sql = string.Format(@"insert into loc_loan_app_info(RECE_DATE, PROD_ID,APPL_DATE ,ACC_NUMB ,CUST_NAME ,LOAN_APP_AMOU,PERS_ID,ACC_TYPE ,ORIG_DEPT_ID,DELI_DEPT_ID,EMPL_TYPE,ORGAN,BR_ID,PRE_DECLIEND,LOAN_SOURCE_CODE,LOAN_STATUS_ID,ARM_CODE ,MakerID) values
                          ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8},{9},'{10}','{11}',{12},'{13}','{14}',{15},'{16}','{17}')",
                     loan.ApplicationDate, loan.ProductId, loan.SubmissionDate, accNo, loan.CustomerName, loan.AppliedAmount, loan.SubmitedBy, loan.AccType, userLoanDept.LoanDeptId, userLoanDept.LoanDeptId, loan.EmployeeType, loan.Organization, loan.EntryBranch, loan.PreDeclined, loan.LoanSourceCode, loan.LoanState, loan.ARMCode, loan.MakerID);
            }
            else if (loan.ForCrd == 4 && loan.ProductId == 2)
            {
                //DELI_DEPT_ID=5 pos(9)
                //LOAN_STATUS_ID=10 pos(12)

                sql = string.Format(@"insert into loc_loan_app_info(RECE_DATE, PROD_ID,APPL_DATE ,ACC_NUMB ,CUST_NAME ,LOAN_APP_AMOU,PERS_ID,ACC_TYPE ,ORIG_DEPT_ID,DELI_DEPT_ID,EMPL_TYPE,ORGAN,LOAN_STATUS_ID,BR_ID,PRE_DECLIEND,PROD_OTHER_INFO,ARM_CODE,MakerID) values
                                  ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8},{9},'{10}','{11}',{12},{13},'{14}','{15}','{16}','{17}')",
                              loan.ApplicationDate, loan.ProductId, loan.SubmissionDate, accNo, loan.CustomerName, loan.AppliedAmount, loan.SubmitedBy, loan.AccType, userLoanDept.LoanDeptId, 5, loan.EmployeeType, loan.Organization, 10, loan.EntryBranch, loan.PreDeclined, loan.AutoDelarName, loan.ARMCode, loan.MakerID);
            }
            else if (loan.ForCrd == 4)
            {
                //DELI_DEPT_ID=5 pos(9)
                //LOAN_STATUS_ID=10 pos(12)
                sql = string.Format(@"insert into loc_loan_app_info(RECE_DATE, PROD_ID,APPL_DATE ,ACC_NUMB ,CUST_NAME ,LOAN_APP_AMOU,PERS_ID,ACC_TYPE ,ORIG_DEPT_ID,DELI_DEPT_ID,EMPL_TYPE,ORGAN,LOAN_STATUS_ID,BR_ID,PRE_DECLIEND,LOAN_SOURCE_CODE,ARM_CODE,MakerID) values
                                  ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8},{9},'{10}','{11}',{12},{13},'{14}','{15}','{16}','{17}')",
                             loan.ApplicationDate, loan.ProductId, loan.SubmissionDate, accNo, loan.CustomerName, loan.AppliedAmount, loan.SubmitedBy, loan.AccType, userLoanDept.LoanDeptId, 5, loan.EmployeeType, loan.Organization, 10, loan.EntryBranch, loan.PreDeclined, loan.LoanSourceCode, loan.ARMCode, loan.MakerID);
            }
            if (!string.IsNullOrEmpty(sql))
            {
                loanId = DbQueryManager.ExecuteNonQueryAndReturnPrimaryKey(sql);
            }
            return loanId;
        }

        public void SaveLoanCheckList(int loanId, List<int> checkList)
        {
            var sql = checkList.Aggregate(string.Empty, (current, i) => current + string.Format(@"insert into loc_check_list_info(LOAN_APPL_ID,CHECK_LIST_ID) values ({0},{1});", loanId, i));
            DbQueryManager.ExecuteNonQuery(sql);
        }

        public void SaveLoanAppDetails(int loanId, Loan loan, UserLoanDept userDept)
        {
            string sql = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,MakerId)values({0},{1},{2},'{3}',{4},'{5}')",
                loanId, loan.SubmitedBy, userDept.LoanDeptId, DateTime.Now, loan.LoanState, loan.MakerID);
            var id = DbQueryManager.ExecuteNonQuery(sql);
            if (id > 0 && loan.ForCrd == 4)
            {
                sql = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID , MakerId)values({0},{1},{2},'{3}',{4},'{5}')",
                                  loanId, loan.SubmitedBy, userDept.LoanDeptId, DateTime.Now, 10, loan.MakerID); // Unknown state Id=10
                DbQueryManager.ExecuteNonQuery(sql);
            }
        }

        public DataTable GetLoanApplications(DateTime dateTime, int offset, int rowFetch, int loan = 0, string filter = "")
        {
            var condition = loan == 0 ? filter + string.Format(" order by l.LOAN_APPL_ID desc  OFFSET  {0} ROWS FETCH NEXT {1} ROWS ONLY ", offset, rowFetch) : string.Format(" where l.LOAN_APPL_ID={2} order by l.LOAN_APPL_ID desc  OFFSET  {0} ROWS FETCH NEXT {1} ROWS ONLY ", offset, rowFetch, loan);

            #region refernce code (previous)

            //                // var condition = loan == 0 ? filter+ string.Format(" order by LLId desc ") : string.Format(" where l.LOAN_APPL_ID=" + loan);
            //                var sql = string.Format(@"select l.PERS_ID,l.PRE_DECLIEND as PreDeclined,l.LOAN_APPL_ID as LLId,p.PROD_NAME as ProductName,p.PROD_ID as ProductId,s.SCSO_NAME as SourceName,s.SCSO_ID as SourceId,l.RECE_DATE as RecieveDate,
            //                l.APPL_DATE as AppliedDate,l.CUST_NAME as ApplicantName,l.ACC_NUMB as AccNo,l.ACC_TYPE as AccTypeId,(case when l.ACC_TYPE=1 then 'CPF' else 'Priority' end) AccType,l.LOAN_STATUS_ID as LoanStatusId,w.LOAN_STATUS_NAME as StateName,
            //                l.LOAN_APP_AMOU as AppliedAmount,pf.PROF_NAME as Profession,pf.PROF_ID as ProfessionId,l.ORGAN as Organization
            //                ,ld.NAME_LEARNER as SubmitedBy,d.LOAN_DEPT_NAME as DepartmentName,l.LOAN_APRV_AMOU as ApprovedAmount,LOAN_DECI_STATUS_ID as DecisionStatusId,ls.LOAN_STATUS_NAME as DecisionStatus,l.LOAN_AC_NO as LoanAccNo,l.PERS_ID as PersonId,l.DELI_DEPT_ID as DeliDeptId,d.LOAN_DEPT_TYPE as DeptType,l.MAKER as Maker
            //                ,l.OWNER as Owner,l.MOTHER_NAME as MotherName,l.Tenor,l.USER_DEFINE as UserDefine,USER_DEFINE2 as UserDefine2,USER_DEFINE3 as UserDefine3,USER_DEFINE4 as UserDefine4,PROD_OTHER_INFO as dealer_name
            //                from loc_loan_app_info l
            //                left join t_pluss_product p on l.PROD_ID=p.PROD_ID
            //                left join t_pluss_scbsource s on l.BR_ID=s.SCSO_ID
            //                left join loan_status_info w on l.LOAN_STATUS_ID=w.LOAN_STATUS_ID
            //                left join t_pluss_profession pf on l.EMPL_TYPE=pf.PROF_NAME
            //                left join t_pluss_user u on l.PERS_ID=u.USER_ID
            //				left join learner_details_table ld on l.PERS_ID=ld.ID_LEARNER
            //                left join loan_dept_info d on l.ORIG_DEPT_ID=d.LOAN_DEPT_ID
            //                left join loan_status_info ls on l.LOAN_DECI_STATUS_ID=ls.LOAN_STATUS_ID {0}", condition);

            //                var sql = string.Format(@"select l.LOAN_APPL_ID as LLId,l.RECE_DATE as RecieveDate,l.PROD_ID as ProductId,l.ORIG_DEPT_ID,l.DELI_DEPT_ID as DeliDeptId,l.APPL_DATE as AppliedDate,l.ACC_NUMB as AccNo,l.CUST_NAME as ApplicantName,l.LOAN_APP_AMOU as AppliedAmount,
            //                            l.PERS_ID,l.LOAN_STATUS_ID as LoanStatusId,l.APPRV_LEVEL,l.ACC_TYPE as AccTypeId,l.MAKER as Maker,l.EMPL_TYPE,l.ORGAN as Organization,l.LOAN_APRV_AMOU ,
            //                            l.LOAN_DISB_DATE,l.ARM_CODE,l.LOAN_AC_NO as LoanAccNo,l.OLD_LIMIT,l.LOAN_DECI_STATUS_ID as DecisionStatusId,l.MOTHER_NAME as MotherName,l.OWNER as Owner,l.TENOR as Tenor,l.USER_DEFINE as UserDefine,
            //                            l.BR_ID,USER_DEFINE2 as UserDefine2,USER_DEFINE3 as UserDefine3,USER_DEFINE4 as UserDefine4,l.DEFF_TAT,l.PRE_DECLIEND as PreDeclined,l.OP_TAT_CREDIT,l.OP_TAT_ASSET,l.PROD_OTHER_INFO as dealer_name,l.LOAN_SOURCE_CODE,
            //                            l.LOAN_DISB_AMOU,l.LOAN_COND_APRV_AMOU,NAME_LEARNER as SubmitedBy,LOAN_DEPT_NAME as DepartmentName,LOAN_STATUS_NAME as StateName,PROD_NAME as ProductName,ACC_TYPE_NAME as AccType,BR_NAME  --l.LOAN_APPV_DATE
            //                            from loc_loan_app_info as l,learner_details_table,loan_dept_info,loan_status_info,product_info,acc_type_info,loc_branch_info as branch_info
            //                            where l.LOAN_STATUS_ID=loan_status_info.LOAN_STATUS_ID 
            //                            and l.PROD_ID=product_info.ID and  l.DELI_DEPT_ID=loan_dept_info.LOAN_DEPT_ID and  l.ACC_TYPE=acc_type_info.ID 
            //                            and l.PERS_ID = learner_details_table.ID_LEARNER and l.BR_ID = branch_info.BR_ID {0}",condition);



            //                var sql = string.Format(@"select l.*,NAME_LEARNER,loan_dept_info.LOAN_DEPT_NAME, loan_status_info.LOAN_STATUS_NAME, PROD_NAME,ACC_TYPE_NAME ,BR_NAME,ds.LOAN_STATUS_NAME  LOAN_DECI_STATUS_Name
            //                            from loc_loan_app_info as l,learner_details_table,loan_dept_info,loan_status_info,product_info,acc_type_info,loc_branch_info as branch_info,loan_status_info ds
            //                            where l.LOAN_STATUS_ID=loan_status_info.LOAN_STATUS_ID 
            //                            and l.PROD_ID=product_info.ID and  l.DELI_DEPT_ID=loan_dept_info.LOAN_DEPT_ID and  l.ACC_TYPE=acc_type_info.ID 
            //                            and l.PERS_ID = learner_details_table.ID_LEARNER --and l.BR_ID = branch_info.BR_ID 
            //                            and l.LOAN_DECI_STATUS_ID=ds.LOAN_STATUS_ID {0}", condition);

            //var sql = string.Format(@"select l.*,u.USER_NAME as NAME_LEARNER,loan_dept_info.LOAN_DEPT_NAME, loan_status_info.LOAN_STATUS_NAME, PROD_NAME,ACC_TYPE_NAME ,BR_NAME,ds.LOAN_STATUS_NAME  LOAN_DECI_STATUS_Name
            //                            from loc_loan_app_info as l
            //                            left join t_pluss_user as u on l.PERS_ID = u.USER_ID
            //                            --left join learner_details_table on  l.PERS_ID = learner_details_table.ID_LEARNER
            //                            left join loan_dept_info on l.DELI_DEPT_ID=loan_dept_info.LOAN_DEPT_ID
            //                            left join loan_status_info on l.LOAN_STATUS_ID=loan_status_info.LOAN_STATUS_ID 
            //                            left join product_info on l.PROD_ID=product_info.ID
            //                            left join acc_type_info on l.ACC_TYPE=acc_type_info.ID 
            //                            left join loc_branch_info as branch_info on l.BR_ID = branch_info.BR_ID 
            //                            left join loan_status_info ds on l.LOAN_DECI_STATUS_ID=ds.LOAN_STATUS_ID
            //                            {0}", condition);

            #endregion


            string sql = string.Format(@"
            SELECT *
            FROM (
            	SELECT a.*
            		,u.USER_NAME AS NAME_LEARNER
            		,b.Name LOAN_DEPT_NAME
            		,loan_status_info.LOAN_STATUS_NAME
            		,PROD_NAME
            		,ACC_TYPE_NAME
            		,SCSO_NAME BR_NAME
            		,ds.LOAN_STATUS_NAME LOAN_DECI_STATUS_Name
                    ,product_info.PROD_LOAN_TYPE
            	FROM loc_loan_app_info AS a
            	LEFT JOIN t_pluss_user AS u ON a.PERS_ID = u.USER_ID
                LEFT JOIN loan_dept_info ON a.DELI_DEPT_ID = loan_dept_info.LOAN_DEPT_ID
            	LEFT JOIN Roles b ON a.DELI_ROLE_ID = b.Id
            	LEFT JOIN loan_status_info ON a.LOAN_STATUS_ID = loan_status_info.LOAN_STATUS_ID
            	LEFT JOIN product_info ON a.PROD_ID = product_info.ID
            	LEFT JOIN acc_type_info ON a.ACC_TYPE = acc_type_info.ID
            	--LEFT JOIN loc_branch_info AS branch_info ON l.BR_ID = branch_info.BR_ID
            	LEFT JOIN t_pluss_scbsource AS branch_info ON a.BR_ID = branch_info.SCSO_ID
            	LEFT JOIN loan_status_info ds ON a.LOAN_DECI_STATUS_ID = ds.LOAN_STATUS_ID
            	WHERE a.RECE_DATE > '{0}'

            	UNION ALL

            	SELECT l.*
            		,u.USER_NAME AS NAME_LEARNER
            		,loan_dept_info.LOAN_DEPT_NAME
            		,loan_status_info.LOAN_STATUS_NAME
            		,PROD_NAME
            		,ACC_TYPE_NAME
            		,SCSO_NAME BR_NAME
            		,ds.LOAN_STATUS_NAME LOAN_DECI_STATUS_Name
                    ,product_info.PROD_LOAN_TYPE
            	FROM loc_loan_app_info AS l
            	LEFT JOIN old_t_pluss_user AS u ON l.PERS_ID = u.USER_ID
            	--left join learner_details_table on  l.PERS_ID = learner_details_table.ID_LEARNER
            	LEFT JOIN loan_dept_info ON l.DELI_DEPT_ID = loan_dept_info.LOAN_DEPT_ID
            	LEFT JOIN loan_status_info ON l.LOAN_STATUS_ID = loan_status_info.LOAN_STATUS_ID
            	LEFT JOIN product_info ON l.PROD_ID = product_info.ID
            	LEFT JOIN acc_type_info ON l.ACC_TYPE = acc_type_info.ID
            	--LEFT JOIN loc_branch_info AS branch_info ON l.BR_ID = branch_info.BR_ID
            	LEFT JOIN t_pluss_scbsource AS branch_info ON l.BR_ID = branch_info.SCSO_ID
            	LEFT JOIN loan_status_info ds ON l.LOAN_DECI_STATUS_ID = ds.LOAN_STATUS_ID
            	WHERE l.RECE_DATE <= '{0}'
            	) l
            {1}
            ", dateTime.ToString("yyyy-MM-dd"), condition);

            

            //LogFile.WriteLine("LocLoanHome Searc Button - " + sql);
            return DbQueryManager.GetTable(sql);
        }

        public DataTable GetLoanRequestDetails(int loanId)
        {
            var sql = string.Format(@"select loan_app_details.*,learner_details_table.NAME_LEARNER as NAME_LEARNER,LOAN_STATUS_NAME,LOAN_DEPT_NAME 
                                from loan_app_details 
                                left join loan_status_info on loan_app_details.PROCESS_STATUS_ID=loan_status_info.LOAN_STATUS_ID  
                                left join learner_details_table on loan_app_details.PERS_ID =learner_details_table.ID_LEARNER
        						--left join t_pluss_user u on loan_app_details.PERS_ID=u.USER_ID
                                left join loan_dept_info on loan_app_details.DEPT_ID=loan_dept_info.LOAN_DEPT_ID 
                                where  loan_app_details.LOAN_APPL_ID={0} order by ID desc", loanId);
            return DbQueryManager.GetTable(sql);
        }


        public DataTable GetLoanRequestDetailsWithRole(int loanId, DateTime learnerTableSplitTime)
        {

            #region MyRegion
            //var sql = string.Format(@"SELECT DISTINCT * FROM (
            //        select a.*
            //         ,c.NAME_LEARNER as NAME_LEARNER
            //         ,LOAN_STATUS_NAME
            //         ,CONCAT(d.LOAN_DEPT_NAME, ' [Dept]') AS LOAN_DEPT_NAME
            //        from loan_app_details a
            //         left join loan_status_info b on a.PROCESS_STATUS_ID=b.LOAN_STATUS_ID  
            //         left join learner_details_table c on a.PERS_ID =c.ID_LEARNER
            //         left join loan_dept_info d on a.DEPT_ID=d.LOAN_DEPT_ID 
            //     where  a.LOAN_APPL_ID={0}  and ISNULL(a.MakerId,'') = ''
            //    UNION
            //        select a.*
            //         ,b.USER_NAME as NAME_LEARNER
            //         ,c.LOAN_STATUS_NAME
            //         ,d.Name as LOAN_DEPT_NAME
            //        from loan_app_details a
            //         INNER JOIN t_pluss_user b ON b.USER_CODE=a.MakerId
            //         left join loan_status_info c on a.PROCESS_STATUS_ID=c.LOAN_STATUS_ID 
            //         LEFT JOIN Roles d ON d.Id = b.USER_LEVEL
            //         where  a.LOAN_APPL_ID={0}
            //     ) t order by ID desc", loanId);
            //LogFile.WriteLine(sql); 
            #endregion
            string sql = string.Format(@"
SELECT *
FROM (
	SELECT loan_app_details.*
		,a.USER_NAME AS NAME_LEARNER
		,LOAN_STATUS_NAME
		,Roles.Name LOAN_DEPT_NAME
	FROM loan_app_details
	LEFT JOIN loan_status_info ON loan_app_details.PROCESS_STATUS_ID = loan_status_info.LOAN_STATUS_ID
	LEFT JOIN t_pluss_user a ON loan_app_details.MakerId = a.USER_CODE
	--left join t_pluss_user u on loan_app_details.PERS_ID=u.USER_ID
	LEFT JOIN Roles ON a.USER_LEVEL = Roles.Id
	--new insertion will be with MakerId
	WHERE loan_app_details.LOAN_APPL_ID = '{0}'
		AND loan_app_details.MakerId IS NOT NULL
        AND loan_app_details.DATE_TIME >= '{1}'
	
	UNION ALL
	
	SELECT loan_app_details.*
		,old_learner_details_table.NAME_LEARNER AS NAME_LEARNER
		,LOAN_STATUS_NAME
		,LOAN_DEPT_NAME
	FROM loan_app_details
	LEFT JOIN loan_status_info ON loan_app_details.PROCESS_STATUS_ID = loan_status_info.LOAN_STATUS_ID
	LEFT JOIN old_learner_details_table ON loan_app_details.PERS_ID = old_learner_details_table.ID_LEARNER
	--left join t_pluss_user u on loan_app_details.PERS_ID=u.USER_ID
	LEFT JOIN loan_dept_info ON loan_app_details.DEPT_ID = loan_dept_info.LOAN_DEPT_ID
	WHERE loan_app_details.LOAN_APPL_ID = '{0}'
		AND loan_app_details.MakerId IS NULL
        AND loan_app_details.DATE_TIME < '{1}'
	) t
ORDER BY ID DESC", loanId, learnerTableSplitTime.ToString());
            return DbQueryManager.GetTable(sql);
        }

        public DataTable GetCibStatus(int loanId)
        {
            var sql = string.Format(@"select PROCESS_STATUS_ID,REMARKS from loan_app_details
                                    where LOAN_APPL_ID={0} and PROCESS_STATUS_ID='19' order by ID desc", loanId);
            return DbQueryManager.GetTable(sql);
        }

        public DataTable GetLoanStatusByType(int type)
        {
            var sql = string.Format(@"select * from loan_status_info where LOAN_STATUS_ID<>9 and LOAN_STATUS_TYPE={0}", type);
            return DbQueryManager.GetTable(sql);
        }

        public string UpdateLoanInfo(Loan loan, LoanInformation obj)
        {
            //var con = new CommonConnection(IsolationLevel.ReadCommitted);
            var res = "";
            try
            {
                //con.BeginTransaction();
                var accNo = loan.PreAccNo + "-" + loan.MidAccNo + "-" + loan.PostAccNo;
                var sql = string.Empty;
                if (loan.ProductId == 2)
                {
                    //PROD_ID=$prd,APPL_DATE='$app_dt',ACC_NUMB='$acc',CUST_NAME='$cus_nm' ,LOAN_APP_AMOU=$loan_app,EMPL_TYPE='$app_type',ORGAN='$orgnz',BR_ID=$branch,ACC_TYPE=$acc_type, PRE_DECLIEND='$pre_decliend',PROD_OTHER_INFO='$dealers_name'
                    sql = string.Format(@"update loc_loan_app_info  set PROD_ID={0},APPL_DATE='{1}',ACC_NUMB='{2}',CUST_NAME='{3}' ,LOAN_APP_AMOU={4},EMPL_TYPE='{5}',ORGAN='{6}',BR_ID={7},ACC_TYPE={8}, PRE_DECLIEND='{9}',PROD_OTHER_INFO='{10}', ARM_Code='{12}' where  LOAN_APPL_ID={11}",
                        loan.ProductId, loan.ApplicationDate, accNo, loan.CustomerName, loan.AppliedAmount, loan.EmployeeType, loan.Organization, loan.EntryBranch, loan.AccType, loan.PreDeclined, loan.AutoDelarName, obj.LLId, loan.ARMCode);
                }
                else
                {
                    //PROD_ID=$prd,APPL_DATE='$app_dt',ACC_NUMB='$acc',CUST_NAME='$cus_nm' ,LOAN_APP_AMOU=$loan_app,EMPL_TYPE='$app_type',ORGAN='$orgnz',BR_ID=$branch,ACC_TYPE=$acc_type, PRE_DECLIEND='$pre_decliend',PROD_OTHER_INFO=''
                    sql = string.Format(@"update loc_loan_app_info  set PROD_ID={0},APPL_DATE='{1}',ACC_NUMB='{2}',CUST_NAME='{3}' ,LOAN_APP_AMOU={4},EMPL_TYPE='{5}',ORGAN='{6}',BR_ID={7},ACC_TYPE={8}, PRE_DECLIEND='{9}', ARM_Code='{11}', PROD_OTHER_INFO='' where  LOAN_APPL_ID={10}",
                        loan.ProductId, loan.ApplicationDate, accNo, loan.CustomerName, loan.AppliedAmount, loan.EmployeeType, loan.Organization, loan.EntryBranch, loan.AccType, loan.PreDeclined, obj.LLId, loan.ARMCode);
                }
                DbQueryManager.ExecuteNonQuery(sql);

                var strSQL1 = "";
                if (obj.DeliDeptId > 0)
                {
                    strSQL1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS)values
        					   ({0},{1},{2},'{3}',{4},'Updated by others')", obj.LLId, obj.PersonId, obj.DeliDeptId, DateTime.Now, obj.LoanStatusId);
                }
                else
                {
                    strSQL1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS)values
        					   ({0},{1},6,'{2}',{3},'Updated by Admin')", obj.LLId, obj.PersonId, DateTime.Now, obj.LoanStatusId);
                }
                DbQueryManager.ExecuteNonQuery(strSQL1);
                //con.CommitTransaction();
                res = "1";
            }
            catch (Exception e)
            {
                res = e.Message;
                //con.RollBack();
            }
            finally
            {
                //con.Close();
            }
            return res;
        }

        public string UpdateLoanOps(ActionParam param, string remarks, bool cibStatus, string action)
        {
            remarks = remarks.Replace("'", "''");
            try
            {
                var dt = DateTime.Now;
                var ldep = param.oriId;
                var pers = param.personId;
                var stDate = GetStartTime(param.appId);
                // con.BeginTransaction();
                if (param.status_update == 101) //Update Req from Source Unit
                {
                    if (action == "ForwLoanDocs" || action == "ForwCrea")
                    {
                        var pro_status = 0;
                        var deli_dept = 0;
                        if (action == "ForwLoanDocs")
                        {
                            pro_status = 10;
                            deli_dept = 5;
                        }
                        else
                        {
                            pro_status = 8;
                            deli_dept = 4;
                        }
                        var str_sql1 =
                            string.Format(
                                "insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId) " +
                                "values({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')", param.appId, param.personId, ldep, dt,
                                pro_status, remarks, (dt - stDate).TotalDays, param.makerID);

                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        //LogFile.WriteLine(str_sql1);

                        if (action == "ForwCrea")
                            str_sql1 =
                                string.Format(
                                    @"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1} where LOAN_APPL_ID={2}",
                                    pro_status, deli_dept, param.appId);
                        else
                            str_sql1 =
                                string.Format(
                                    @"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=0 where LOAN_APPL_ID={2}",
                                    pro_status, deli_dept, param.appId);

                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        //LogFile.WriteLine(str_sql1);
                    }
                }




                else if (param.status_update == 201) //Update Req from Creadit Unit
                {
                    if (action == "ForwLoanDocs" || action == "CreaOnProcess" || action == "On_Hold")
                    {
                        var pro_status = 0;
                        var deli_dept = 0;

                        if (action == "ForwLoanDocs")
                        {
                            pro_status = 10;
                            deli_dept = 5;

                        }
                        else if (action == "On_Hold")
                        {
                            pro_status = 17;
                            deli_dept = 4;
                        }

                        else
                        {
                            pro_status = 4;
                            deli_dept = 4;

                        }

                        var str_sql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId) values
					     ({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')", param.appId, param.personId, param.oriId, dt, pro_status, remarks,
                                (dt - stDate).TotalDays, param.makerID);

                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        //LogFile.WriteLine(str_sql1);

                        if (action == "CreaOnProcess")
                            str_sql1 =
                                string.Format(
                                    @"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1} where LOAN_APPL_ID={2}",
                                    pro_status, deli_dept, param.appId);
                        else
                        {
                            //call opertional TAT function.
                            var op_tat_credit = 0;
                            op_tat_credit = operational_tat(param.appId, 0);
                            str_sql1 =
                                string.Format(
                                    @"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=0, OP_TAT_CREDIT={2} where LOAN_APPL_ID={3}",
                                    pro_status, deli_dept, op_tat_credit, param.appId);

                        }
                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        //LogFile.WriteLine(str_sql1);
                    } //if ($HTTP_POST_VARS['ForwLoanDocs'] || $HTTP_POST_VARS['CreaOnProcess'])
                }
                else if (param.status_update == 301) //Update Req from Creadit Unit
                {

                    if (action == "AssetReadyDis" || action == "AssetOnProcess" || action == "AssetRetCheck" ||
                        action == "QuickDis" || action == "On_Hold")
                    {
                        var pro_status = 0;
                        if (action == "AssetReadyDis") //if($HTTP_POST_VARS['QuickDis'])
                        {
                            pro_status = 7;
                        }
                        else if (action == "On_Hold")
                        {
                            pro_status = 17;
                        }
                        else
                        {
                            pro_status = 4;
                        }

                        var deli_dept = 5;

                        var str_sql1 =
                            string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId) values
					        ({0},{1},{2},'{3}',{4},'{5}',{6})", param.appId, pers, ldep, dt, pro_status, remarks, (dt - stDate).TotalDays, param.makerID);

                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        //LogFile.WriteLine(str_sql1);
                        if (action == "AssetReadyDis")
                            str_sql1 =
                                string.Format(
                                    "update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=1 where LOAN_APPL_ID={2}",
                                    pro_status, deli_dept, param.appId);

                        else if (action == "QuickDis")
                        {
                            //call opertional TAT function.
                            var op_tat_asset = 0;
                            op_tat_asset = operational_tat(param.appId, 1);
                            str_sql1 =
                                string.Format(
                                    @"update loc_loan_app_info set LOAN_STATUS_ID=7,LOAN_DISB_DATE='{0}',OP_TAT_ASSET={1}, DELI_DEPT_ID= 5 where LOAN_APPL_ID={2}",
                                    dt, op_tat_asset, param.appId);

                        }
                        else
                            str_sql1 =
                                string.Format(
                                    @"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=0 where LOAN_APPL_ID={2}",
                                    pro_status, deli_dept, param.appId);

                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        //LogFile.WriteLine(str_sql1);
                    }
                }

                if (cibStatus && param.oriId == 5)
                {
                    var pro_status = 19;
                    var cibReport = true;
                    if (cibReport)
                    {
                        var str_sql1 =
                            string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId) values
						  ({0},{1},{2},'{3}',{4},'{5}',{6})", param.appId, param.personId, ldep, dt, pro_status, remarks,
                                (dt - stDate).TotalDays, param.makerID);
                        DbQueryManager.ExecuteNonQuery(str_sql1);

                    }
                    else
                    {
                        return "Select CIB Report status";
                    }

                }
                //con.CommitTransaction();
                return "1";
            }
            catch (Exception)
            {
                // con.RollBack();
            }
            finally
            {
                // con.Close();
            }
            return "0";
        }


        //calculate opertional TAT.
        public int operational_tat(int id, int type)
        {
            var queryInpro = "";
            if (type == 0)
                queryInpro = string.Format("select top 1 DATE_TIME from loan_app_details  where DEPT_ID=4 and PROCESS_STATUS_ID=4 and LOAN_APPL_ID={0} order by ID desc", id);
            else
                queryInpro = string.Format("select top 1 DATE_TIME from loan_app_details  where DEPT_ID=5 and PROCESS_STATUS_ID=4 and LOAN_APPL_ID={0} order by ID desc");

            var dt = DbQueryManager.ExecuteScalar(queryInpro);
            var days = (DateTime.Now - Convert.ToDateTime(dt)).TotalDays;
            return Convert.ToInt32(days);
        }

        //TAT Function 
        public DateTime GetStartTime(int appId)
        {
            var sql_d = string.Format("select top 1 DATE_TIME from loan_app_details where LOAN_APPL_ID='{0}' order by ID desc", appId);
            var dt = DbQueryManager.ExecuteScalar(sql_d);
            return Convert.ToDateTime(dt);
        }


        public string SaveOtherVc(ActionParam param, string user)
        {
            // var con = new CommonConnection(IsolationLevel.ReadCommitted); ;
            var res = "";
            try
            {
                //con.BeginTransaction();
                var loanDeptId = param.oriId; //current user's dept_no
                var pers = param.personId; //current user/id learner
                var loanId = param.appId; // the loan that will be transferred
                var curLoanStat = param.cond; //value from status.php- current loan status ID.
                var userName = user; //Usernum inputed by user from previous pg.
                var curDt = DateTime.Now; //gets Current Date.
                var dateTime = GetStartTime(param.appId);

                // Check User Name
                var query = string.Format("select * from learner_details_table where USERNAME='{0}'", userName);
                var userDt = DbQueryManager.GetTable(query);
                if (userDt == null || userDt.Rows.Count == 0)
                {
                    return "Enter Correct USER NUMBER";
                }

                var nameLearner = userDt.Rows[0][1].ToString(); //fetch the name for that user number.
                var idLearner = userDt.Rows[0][2].ToString(); //fetch the ID for that user .
                var cur_dept = userDt.Rows[0][8];

                var tat_deff = 0;
                var tatVeri = 0;
                tat_deff = curLoanStat == 6 ? 1 : 0;
                // Calculate "TAT_VARI" for "loan_app_details".		
                tatVeri = curLoanStat == 9 ? 1 : 0;


                var insQuery =
                string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,TAT_VARI,MakerId) 
                values({0},'{1}',{2},'{3}',14,'{5}',{6},{7},'{8}')", loanId, pers, loanDeptId, curDt, 14, "transfer", (curDt - dateTime).TotalDays, tatVeri, param.makerID);
                DbQueryManager.ExecuteNonQuery(insQuery);

                var updQuery = string.Format(@"UPDATE loc_loan_app_info set DELI_DEPT_ID={0},PERS_ID='{1}',LOAN_STATUS_ID=14 where LOAN_APPL_ID={2}", cur_dept, param.personId, param.appId);
                DbQueryManager.ExecuteNonQuery(updQuery);
                //con.CommitTransaction();
                res = string.Format("Successfully transferred to User Number:{0} Name:{1}", userName, nameLearner);
            }
            catch (Exception e)
            {
                res = " !!! Error occured.";
                // con.RollBack();
            }
            finally
            {
                //  con.Close();
            }
            return res;
        }

        public string UpdateProfile(ActionParam param, string own, string motherName, string teno, string userdef, string userDef2, string userDef3, string userDef4)
        {
            var res = "";
            try
            {
                var strSQL1 = string.Format(@"update loc_loan_app_info  set MOTHER_NAME='{0}', OWNER='{1}',TENOR='{2}',USER_DEFINE='{3}',USER_DEFINE2='{4}',USER_DEFINE3='{5}',USER_DEFINE4='{6}' where  LOAN_APPL_ID={7}",
                    motherName, own, teno, userdef, userDef2, userDef3, userDef4, param.appId);
                DbQueryManager.ExecuteNonQuery(strSQL1);
                res = "Optional  Status is Updated";
            }
            catch (Exception)
            {
                res = "Optional  Status not Updated";
            }
            return res;
        }

        public DataTable GetDeferredDeclineReason(int prodId, int type)
        {
            var str_sql1 = string.Format(@"select *,REAS_ID as num from deferred_decline_reason where PROD_ID={0} and DTYPE={1}", prodId, type);
            return DbQueryManager.GetTable(str_sql1);

        }

        public string SaveOnStatus(ActionParam param, decimal approvedAmount, decimal condApprovAmou, string objTenor, string objRemarks, string year, string month, string day, string disDate, string submit, string preAcc, string midAcc, string postAcc, string loanac, string loanAccExist, string defdec, string apprlvl, decimal disbuserdAmo)
        {
            objRemarks = objRemarks.Replace("'", "''");
            try
            {
                var sql1 = "select ORIG_DEPT_ID from loc_loan_app_info where LOAN_APPL_ID=" + param.appId;
                var tb = DbQueryManager.GetTable(sql1);
                var ddep = Convert.ToInt32(tb.Rows[0]["ORIG_DEPT_ID"]);

                if (param.LoanStateId == 2 || param.LoanStateId == 6)
                {

                    //TAT function call
                    var DATE_TIME = GetStartTime(param.appId);
                    var str_sql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,DEFE_DECL_REAS_ID,TAT,MakerId ) values
					  ({0},{1},{2},'{3}',{4},'{5}',{6},{7},'{8}')", param.appId, param.personId, param.oriId, DateTime.Now, param.LoanStateId, objRemarks, defdec, (DateTime.Now - DATE_TIME).TotalDays, param.makerID);
                    DbQueryManager.ExecuteNonQuery(str_sql1);

                    str_sql1 = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID=3, DELI_DEPT_ID={0}, LOAN_DECI_STATUS_ID={1} where LOAN_APPL_ID={2}", ddep, param.LoanStateId, param.appId);
                    DbQueryManager.ExecuteNonQuery(str_sql1);
                    return "'Loan Status has been Changed.";
                }
                else
                {
                    //$prev_status=$HTTP_POST_VARS['prev_status'];
                    var prev_status = param.LoanStateId;
                    var tat_vari = 0;
                    if (prev_status == 9)
                        tat_vari = 1;

                    if (param.JobState == 21)
                    {
                        //TAT function call
                        var DATE_TIME = GetStartTime(param.appId);

                        var str_sql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,TAT_VARI,MakerId) values
						  ({0},{1},{2},'{3}',{4},'{5}',{6},{7},'{8}')", param.appId, param.personId, param.oriId, DateTime.Now, 5, objRemarks, (DateTime.Now - DATE_TIME).TotalDays, tat_vari, param.makerID);
                        DbQueryManager.ExecuteNonQuery(str_sql1);

                        var ap_amou = approvedAmount;
                        var appr_lvl = string.IsNullOrEmpty(apprlvl) ? 0 : Convert.ToInt32(apprlvl);
                        var tenor = string.IsNullOrEmpty(objTenor) ? 0 : Convert.ToInt32(objTenor);

                        //,LOAN_DISB_DATE='$dis_dt'
                        var dis_dt = year + '-' + month + '-' + day;
                        var dt1 = DateTime.Now.ToString("yyyy-MM-dd");
                        var msql = string.Format("update loc_loan_app_info set LOAN_STATUS_ID=5,APPRV_LEVEL={0}, DELI_DEPT_ID=4,LOAN_APRV_AMOU={1},LOAN_DISB_DATE='{2}',LOAN_DECI_STATUS_ID={3},APPRV_DATE='{4}', TENOR={5} where LOAN_APPL_ID={6}",
                            appr_lvl, ap_amou, dis_dt, param.LoanStateId, dt1, tenor, param.appId);
                        DbQueryManager.ExecuteNonQuery(msql);
                        return "Loan Status has been Changed.\\" + "\\email\\" + param.appId;
                        //email.sender.php?ll_id=".$id
                    }
                    else if (param.JobState == 22)
                    {
                        //echo "amount::",$cond_approv_amou;
                        //TAT function call
                        var DATE_TIME = GetStartTime(param.appId);

                        var str_sql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId ) values
						  ({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')",
                                       param.appId, param.personId, param.oriId, DateTime.Now, param.LoanStateId, objRemarks, (DateTime.Now - DATE_TIME).TotalDays, param.makerID);

                        DbQueryManager.ExecuteNonQuery(str_sql1);

                        var cond_approv_amou = condApprovAmou;
                        //str_sql1 = string.Format(@"update loc_loan_app_info set LOAN_COND_APRV_AMOU={0},LOAN_STATUS_ID=3, DELI_DEPT_ID={1}, LOAN_DECI_STATUS_ID={2} where LOAN_APPL_ID={3}", cond_approv_amou, ddep, param.LoanStateId, param.appId);
                        str_sql1 = string.Format(@"update loc_loan_app_info set LOAN_COND_APRV_AMOU={0},LOAN_STATUS_ID={2}, DELI_DEPT_ID={1}, LOAN_DECI_STATUS_ID={2} where LOAN_APPL_ID={3}", cond_approv_amou, ddep, param.LoanStateId, param.appId);
                        DbQueryManager.ExecuteNonQuery(str_sql1);
                        return "Loan Status has been Changed.";
                    }
                    else
                    {
                        var lst = param.LoanStateId;
                        if (param.LoanStateId == 4)
                        {
                            lst = 7;
                        }
                        //TAT function call
                        var DATE_TIME = GetStartTime(param.appId);
                        var tat = (DateTime.Now - DATE_TIME).TotalDays;

                        var str_sql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId) values
						  ({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')",
                                      param.appId, param.personId, param.oriId, DateTime.Now, lst, objRemarks, tat, param.makerID);
                        DbQueryManager.ExecuteNonQuery(str_sql1);


                        if (lst != 2)
                        {
                            if (lst == 15)
                            {

                                str_sql1 = string.Format("update loc_loan_app_info set LOAN_STATUS_ID=12, DELI_DEPT_ID={0}, LOAN_DECI_STATUS_ID={1} where LOAN_APPL_ID={2}", ddep, lst, param.appId);
                            }
                            else if (lst == 7 && param.JobState == 10)
                            {
                                var disb_amou = disbuserdAmo;
                                var dis_dt = year + '-' + month + '-' + day;
                                str_sql1 = "update loc_loan_app_info set LOAN_STATUS_ID=7,LOAN_DISB_DATE='" + dis_dt + "', DELI_DEPT_ID= 5,LOAN_AC_NO='" + loanac + "',LOAN_DISB_AMOU=" + disb_amou + " where LOAN_APPL_ID=" + param.appId + "";

                            }
                            else
                            {
                                str_sql1 = "update loc_loan_app_info set LOAN_STATUS_ID=" + lst + ", DELI_DEPT_ID=4 where LOAN_APPL_ID=" + param.appId + "";
                            }

                            DbQueryManager.ExecuteNonQuery(str_sql1);

                            if (param.JobState == 1 && (lst == 16 || lst == 18))
                            {
                                return "Loan Status has been Changed.";
                            }
                            else
                            {
                                return "Loan Status has been Changed.\\" + "\\email\\" + param.appId;
                            }
                        }
                        else
                        {
                            str_sql1 = "update loc_loan_app_info set LOAN_STATUS_ID=" + lst + ", DELI_DEPT_ID=4, LOAN_DECI_STATUS_ID=" + lst + " where LOAN_APPL_ID=" + param.appId + "";
                        }


                        DbQueryManager.ExecuteNonQuery(str_sql1);

                    }

                }
            }
            catch (Exception ex)
            {
                return "Error ! Loan not Update (" + ex.Message + ")";
            }
            return "Error ! Loan not Update";
        }

        public string SubmitAssetStatus(ActionParam param, string remarks)
        {
            var res = "";
            remarks = remarks.Replace("'", "''");
            try
            {
                var id = param.appId;
                var lst = param.lst;
                var type = param.type;
                var pers = param.personId;
                var ldep = param.oriId;
                var status = lst;
                var deli_dep = 0;
                var statusMsg = "";

                var backfrom = param.backfrom;
                if (lst == 11)
                    statusMsg = "Forward to Loan admin";
                if (lst == 12)
                    statusMsg = "Send Back to Credit";
                if (lst == 3)
                    statusMsg = "Send Back to Source";


                if (lst == 3)
                {
                    var str_sql1 = string.Format("select ORIG_DEPT_ID from loc_loan_app_info where LOAN_APPL_ID='{0}'", id);
                    var result = DbQueryManager.ExecuteScalar(str_sql1);
                    deli_dep = Convert.ToInt32(result);
                    lst = param.Backfrom == 1 ? 13 : 13;
                }

                else if (lst == 11)
                { deli_dep = 7; }
                else if (lst == 12)
                { deli_dep = 4; }

                var dt = DateTime.Now;
                var DATE_TIME = GetStartTime(param.appId);
                var tat = (dt - DATE_TIME).TotalDays;

                var strsql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,makerID) values
                              ({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')", id, pers, ldep, dt, lst, remarks, tat, param.makerID);
                DbQueryManager.ExecuteNonQuery(strsql1);


                var sql1 = "";
                if (status == 3)
                {
                    var deff_tat = 1;
                    sql1 = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1},DEFF_TAT={2} where LOAN_APPL_ID={3}", lst, deli_dep, deff_tat, id);
                }
                else
                {
                    sql1 = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1} where LOAN_APPL_ID={2}", lst, deli_dep, id);

                }
                var result1 = DbQueryManager.ExecuteNonQuery(sql1);
                if (result1 == 1)
                {
                    res = "Optional  Status is Updated";
                }
                else
                {
                    res = "Optional  Status is not Updated";
                }

            }
            catch (Exception)
            {
                res = "Error: Optional  Status is not Updated";
            }
            return res;
        }

        public DataTable GetOnHolds()
        {
            var mSQL = "select ONHOLD_ID,ONHOLD_NAME from loc_onhold_info ";
            return DbQueryManager.GetTable(mSQL);
        }

        public DataTable GetLoanDept(int type)
        {
            var sql = string.Format("select * from loan_dept_info where LOAN_DEPT_ID={0}", type);
            return DbQueryManager.GetTable(sql);
        }


        public DataTable GetLoanUserRoleState(int type)
        {///
            var sql = string.Format("select * from loan_dept_info where LOAN_DEPT_ID={0}", type);
            return DbQueryManager.GetTable(sql);
        }

        public int GetCountOfLoans()
        {
            var sql = string.Format("select count(*) as Total from loc_loan_app_info");
            var total = Convert.ToInt32(DbQueryManager.ExecuteScalar(sql));
            return total;
        }

        public DataTable GetLoanCheckListInfo(int appId)
        {
            var sql = string.Format("select * from loc_check_list_info where LOAN_APPL_ID={0}", appId);
            return DbQueryManager.GetTable(sql);
        }

        public string OnClickUpdateCheckList(int appId, string selectedValues)
        {
            var sql = string.Format("delete from  loc_check_list_info  where LOAN_APPL_ID={0}", appId);
            DbQueryManager.ExecuteNonQuery(sql);
            var checkLists = selectedValues.Split(',');
            if (checkLists.Any())
            {
                sql = "";
                foreach (var checkList in checkLists)
                {
                    sql += string.Format(@"insert into loc_check_list_info(LOAN_APPL_ID,CHECK_LIST_ID) values ({0},{1});", appId, checkList);
                }
                DbQueryManager.ExecuteNonQuery(sql);
            }
            return "Check List update successfully of LLID: " + appId;
        }

        public DataTable GetLoanApplication(int loan)
        {
            var condition = string.Format(" where LOAN_APPL_ID={0} ", loan);
            var sql = string.Format(@"select l.PERS_ID,l.PRE_DECLIEND as PreDeclined,l.LOAN_APPL_ID as LLId,p.PROD_NAME as ProductName,p.PROD_ID as ProductId,branch.BR_NAME as SourceName,branch.BR_ID as SourceId,l.RECE_DATE as RecieveDate,
                                l.APPL_DATE as AppliedDate,l.CUST_NAME as ApplicantName,l.ACC_NUMB as AccNo,l.ACC_TYPE as AccTypeId,(case when l.ACC_TYPE=1 then 'CPF' else 'Priority' end) AccType,l.LOAN_STATUS_ID as LoanStatusId,w.LOAN_STATUS_NAME as StateName,
                                l.LOAN_APP_AMOU as AppliedAmount,pf.PROF_NAME as Profession,pf.PROF_ID as ProfessionId,l.ORGAN as Organization
                                ,ld.NAME_LEARNER as SubmitedBy,d.LOAN_DEPT_NAME as DepartmentName,l.LOAN_APRV_AMOU as ApprovedAmount,LOAN_DECI_STATUS_ID as DecisionStatusId,ls.LOAN_STATUS_NAME as DecisionStatus,l.LOAN_AC_NO as LoanAccNo,l.PERS_ID as PersonId,l.DELI_DEPT_ID as DeliDeptId,d.LOAN_DEPT_TYPE as DeptType,l.MAKER as Maker
                                ,l.OWNER as Owner,l.MOTHER_NAME as MotherName,l.Tenor,l.USER_DEFINE as UserDefine,USER_DEFINE2 as UserDefine2,USER_DEFINE3 as UserDefine3,USER_DEFINE4 as UserDefine4,PROD_OTHER_INFO as dealer_name , l.ARM_CODE as ARMCode
                                from loc_loan_app_info l
                                left join t_pluss_product p on l.PROD_ID=p.PROD_ID
                                left join loc_branch_info branch on l.BR_ID=branch.BR_ID 
                                left join loan_status_info w on l.LOAN_STATUS_ID=w.LOAN_STATUS_ID
                                left join t_pluss_profession pf on l.EMPL_TYPE=pf.PROF_NAME
                                left join t_pluss_user u on l.PERS_ID=u.USER_ID
                				left join old_learner_details_table ld on l.PERS_ID=ld.ID_LEARNER
                                left join loan_dept_info d on l.ORIG_DEPT_ID=d.LOAN_DEPT_ID
                                left join loan_status_info ls on l.LOAN_DECI_STATUS_ID=ls.LOAN_STATUS_ID {0}", condition);
            return DbQueryManager.GetTable(sql);
        }

        public string UpdateStatusOption(string inSql, string upSql)
        {
            var res = "";
            try
            {
                if (!string.IsNullOrEmpty(inSql))
                {
                    DbQueryManager.ExecuteNonQuery(inSql);
                    res = "Operation saved successfully";
                }
                if (!string.IsNullOrEmpty(upSql))
                {
                    DbQueryManager.ExecuteNonQuery(upSql);
                    res = "Updated successfully";
                }
            }
            catch (Exception)
            {
                res = "Error ! Try again";
            }
            return res;
        }

        public DataTable GetLoanDecDefReasonInfo(int appId)
        {
            var sql = string.Format("select * from loan_dec_def_reason_info where LOAN_APPL_ID={0}", appId);
            return DbQueryManager.GetTable(sql);
        }

        public string UpdateLoanToDeclinedOrAssessment(ActionParam param, string reasonCheckList, string objRemarks)
        {
            objRemarks = objRemarks.Replace("'", "''");
            var res = "";
            try
            {
                var remarks = objRemarks.Replace("'", "''");
                var id = param.appId;
                var lst = param.LoanStateId;
                var type = param.type;
                var pers = param.Learner_Pers;
                var job = param.JobState;
                var ldep = param.oriId;
                var dt = DateTime.Now;

                var arr = reasonCheckList.Split(',');
                var strsql1 = "";
                if (arr.Any())
                {
                    strsql1 = string.Format("select ORIG_DEPT_ID,LOAN_STATUS_ID from loc_loan_app_info where LOAN_APPL_ID={0}", id);
                    var dataTable = DbQueryManager.GetTable(strsql1);
                    var ddep = dataTable.Rows[0]["ORIG_DEPT_ID"];
                    var tat_vari = 0;
                    if (Convert.ToInt32(dataTable.Rows[0]["LOAN_STATUS_ID"]) == 9)
                        tat_vari = 1;

                    var DATE_TIME = GetStartTime(id);
                    var tat = (dt - DATE_TIME).Days;

                    if (!string.IsNullOrEmpty(arr[0]) && (Convert.ToInt32(arr[0]) > 0 || Convert.ToInt32(arr[0]) < 9))
                    {
                        strsql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,DEFE_DECL_REAS_ID,TAT,TAT_VARI,makerID ) values
                        ({0},{1},{2},'{3}',{4},'{5}',{6},{7},{8},'{9}')", id, pers, ldep, dt, lst, remarks, arr[0], tat, tat_vari, param.makerID);
                    }

                    else
                    {
                        strsql1 = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,TAT_VARI,makerID  ) values
                       ({0},{1},{2},'{3}',{4},'{5}',{6},{7},'{8}')", id, pers, ldep, dt, lst, remarks, tat, tat_vari, param.makerID);
                    }

                    DbQueryManager.ExecuteNonQuery(strsql1);
                    var def_dec = 0;
                    if (lst == 2)
                        def_dec = lst;
                    else
                        def_dec = lst;

                    strsql1 = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, LOAN_DECI_STATUS_ID={2}, DEFF_TAT=1 where LOAN_APPL_ID={3}",
                            def_dec, ddep, lst, id);

                    if (def_dec != 6)
                    {
                        DbQueryManager.ExecuteNonQuery(strsql1);
                    }
                    strsql1 = "delete from  loan_dec_def_reason_info  where LOAN_APPL_ID=" + id;
                    DbQueryManager.ExecuteNonQuery(strsql1);
                    strsql1 = "";
                    for (var i = 0; i < arr.Count() - 5; i++)
                    {
                        strsql1 += string.Format(@"insert into loan_dec_def_reason_info(LOAN_APPL_ID,REAS_ID) values({0},{1});", id, arr[i]);
                    }
                    DbQueryManager.ExecuteNonQuery(strsql1);
                    res = "Success";
                }
            }
            catch (Exception)
            {

                res = "Error";
            }
            return res;
        }
    }
}
