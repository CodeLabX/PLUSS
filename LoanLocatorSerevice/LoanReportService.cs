﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AzUtilities;
using DAL;
using LoanLocatorDataService;

namespace LoanLocatorSerevice
{
    public class LoanReportService
    {
        LoanReportDataService _dataService=new LoanReportDataService();
        ExpImpGateway gateway = new ExpImpGateway();
        public DataTable GetDuplicateSearch(string start, string to, string productId, string stateId)
        {
            try
            {
                return _dataService.GetDuplicateSearch(start,to,productId,stateId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int cleanDuplicateData(string start ,string  to ,string productId , string stateId )
        {
            try
            {
                return _dataService.cleanDuplicateData(start, to, productId, stateId);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public DataTable GetDuplicateRawData(string start, string to, string productId)
        {
            try
            {
                return _dataService.GetDuplicateRawData(start, to, productId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetExpImp(int productId, int departmentId, int processId, string startDate, string endDate)
        {
            try
            {
                return _dataService.GetExpImp(productId, departmentId, processId, startDate, endDate);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public int DeleteTable1()
        {
            return _dataService.DeleteTable1();
        }

        public int DeleteTable2()
        {
            return _dataService.DeleteTable2(); 
        }

        public int InsertData(string startDate, string endDate, int productId)
        {
            return _dataService.InsertData(startDate, endDate, productId); 
        }

        public int InsertDataAfterRoleEnhancement(string startDate, string endDate, int productId)
        {
            return _dataService.InsertDataAfterRoleEnhancement(startDate, endDate, productId); 
        }

        public DataTable SearchData()
        {
            return _dataService.SearchData();
        }

        public ExportImport update_data(DataTable csvarray, int personId, int depId)
        {
            try
            {
                return gateway.update_data(csvarray, personId, depId);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public void DeleteDuplicateData(string startDate, string endDate, string product)
        {
            _dataService.DeleteDuplicateData(startDate, endDate, product);
        }

        public void DeleteTempData(int dupId)
        {
            _dataService.DeleteTempData(dupId);
        }

        public DataTable ExportLoanAppInfo(DateTime startDate, DateTime endDate)
        {
            try
            {
                return _dataService.ExportLoanAppInfo(startDate, endDate);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ExportNegativeList(DateTime startDate, DateTime endDate)
        {
            try
            {
                return _dataService.ExportNegativeList(startDate, endDate);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable ExportDedupInfo(DateTime startDate, DateTime endDate)
        {
            try
            {
                return _dataService.ExportDedupInfo(startDate, endDate);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetExpImpCsv(int productId, int departmentId, int processId, string startDate, string endDate)
        {
            try
            {
                DataTable table = _dataService.GetExpImp(productId, departmentId, processId, startDate, endDate);
                var csv = string.Empty;
                if (table != null)
                {
                    csv = table.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
                    csv += "\r\n";

                    foreach (DataRow row in table.Rows)
                    {
                        csv = table.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", ";") + ','));
                        //csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", " ").Replace(";", " ").Replace("&amp", "&").Replace("amp", " ").Replace('\t', ' ') + ','));
                        csv += "\r\n";
                    }
                }

                return csv;

            }
            catch (Exception e)
            {
                LogFile.WriteLog(e);
            }

            return "";
        }
    }
}
