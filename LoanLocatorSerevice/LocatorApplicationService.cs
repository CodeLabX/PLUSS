﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Bat.Common;
using BusinessEntities;
using LoanLocatorDataService;

namespace LoanLocatorSerevice
{
    public class LocatorApplicationService
    {
        readonly LocatorApplicationDataService _locatorApplicationDataService = new LocatorApplicationDataService();
        public DataTable GetCheckList(int prodId)
        {
            return _locatorApplicationDataService.GetCheckList(prodId);
        }

        public int SaveNewLoan(Loan loan)
        {

            var res = 0;
            try
            {
                var userDept = _locatorApplicationDataService.GetUserLoanDepartment(loan.SubmitedBy);
                var loanId = _locatorApplicationDataService.SaveLoanAppInfo(loan, userDept);
                if (loanId > 0)
                {
                    if (loan.CheckList != null && loan.CheckList.Any())
                    {
                        _locatorApplicationDataService.SaveLoanCheckList(loanId, loan.CheckList);
                    }
                    _locatorApplicationDataService.SaveLoanAppDetails(loanId, loan, userDept);
                    res = loanId;
                }

            }
            catch (Exception e)
            {
                res = -1;
            }

            return res;
        }

        public DataTable GetLoanApplications(DateTime dateTime, int loan = 0, string filter = "")
        {
            try
            {
                int offset = 0;
                int rowFetch = 500;
                return _locatorApplicationDataService.GetLoanApplications(dateTime, offset, rowFetch, loan, filter);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetLoanApplication(int loan = 0)
        {
            try
            {
                return _locatorApplicationDataService.GetLoanApplication(loan);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetLoanRequestDetails(int loanId)
        {
            try
            {
                return _locatorApplicationDataService.GetLoanRequestDetails(loanId);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetLoanRequestDetailsWithRole(int loanId, DateTime learnerTableSplitTime)
        {
            try
            {
                return _locatorApplicationDataService.GetLoanRequestDetailsWithRole(loanId, learnerTableSplitTime);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetCibStatus(int loanId)
        {
            try
            {
                return _locatorApplicationDataService.GetCibStatus(loanId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetLoanStatusByType(int type)
        {
            return _locatorApplicationDataService.GetLoanStatusByType(type);
        }

        public string UpdateLoanInfo(Loan loan, LoanInformation selectedLoan)
        {
            try
            {
                return _locatorApplicationDataService.UpdateLoanInfo(loan, selectedLoan);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string UpdateLoanOps(ActionParam param, string remarks, bool cibStatus, string action)
        {
            try
            {
                return _locatorApplicationDataService.UpdateLoanOps(param, remarks, cibStatus, action);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string SaveOtherVc(ActionParam param, string user)
        {
            try
            {
                return _locatorApplicationDataService.SaveOtherVc(param, user);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string UpdateProfile(ActionParam param, string own, string motherName, string teno, string userdef, string userDef2, string userDef3, string userDef4)
        {
            try
            {
                return _locatorApplicationDataService.UpdateProfile(param, own, motherName, teno, userdef, userDef2, userDef3, userDef4);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDeferredDeclineReason(int prodId, int type)
        {
            return _locatorApplicationDataService.GetDeferredDeclineReason(prodId, type);
        }

        public string SaveOnStatus(ActionParam param, decimal approvedAmount, decimal condApprovAmou, string objTenor, string objRemarks, string year, string month, string day, string disDate, string submit, string preAcc, string midAcc, string postAcc, string loanac, string loanAccExist, string defdec, string apprlvl, decimal disbursedAmo)
        {
            try
            {
                return _locatorApplicationDataService.SaveOnStatus(param, approvedAmount, condApprovAmou, objTenor, objRemarks, year, month, day, disDate, submit, preAcc, midAcc, postAcc, loanac, loanAccExist, defdec, apprlvl, disbursedAmo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string SubmitAssetStatus(ActionParam param, string remarks)
        {
            try
            {
                return _locatorApplicationDataService.SubmitAssetStatus(param, remarks);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetOnHolds()
        {
            try
            {
                return _locatorApplicationDataService.GetOnHolds();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetLoanDept(int type)
        {
            try
            {
                return _locatorApplicationDataService.GetLoanDept(type);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetLoanUserRoleState(int type)
        {
            try
            {
                return _locatorApplicationDataService.GetLoanUserRoleState(type);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetCountOfLoans()
        {
            return _locatorApplicationDataService.GetCountOfLoans();
        }

        public DataTable GetLoanCheckListInfo(int appId)
        {
            return _locatorApplicationDataService.GetLoanCheckListInfo(appId);
        }

        public object OnClickUpdateCheckList(int appId, string selectedValues)
        {
            return _locatorApplicationDataService.OnClickUpdateCheckList(appId, selectedValues);
        }

        public DateTime GetStartTime(int appId)
        {
            return _locatorApplicationDataService.GetStartTime(appId);
        }

        public int GetOperationalTat(int appId, int type)
        {
            return _locatorApplicationDataService.operational_tat(appId, type);
        }

        public object UpdateStatusOption(string inSql, string upSql)
        {
            return _locatorApplicationDataService.UpdateStatusOption(inSql, upSql);
        }

        public DataTable GetLoanDecDefReasonInfo(int appId)
        {
            return _locatorApplicationDataService.GetLoanDecDefReasonInfo(appId);
        }

        public string UpdateLoanToDeclinedOrAssessment(ActionParam param, string reasonCheckList, string objRemarks)
        {
            return _locatorApplicationDataService.UpdateLoanToDeclinedOrAssessment(param,reasonCheckList,objRemarks);
        }
    }
}
