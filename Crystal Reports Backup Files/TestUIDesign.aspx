﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_TestUIDesign" Codebehind="TestUIDesign.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    
        <link href="../CSS/Default.css" rel="stylesheet" type="text/css" />

 <link href="../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />
    <%--<script src="../Scripts/AutoLoan/LoanApplication.js" type="text/javascript"></script>--%>
</head>
<body>
    <form id="form1" runat="server">

                                        
                                               
                                <div class="divRightBigNormalClass widthSize100_per" style="float: left; margin-left: 0px; margin-bottom: 0px; padding:2px; ">  


                                        <div class="divLeftBig widthSize48_per centerAlign" style="margin-bottom: 0px;">
                                            <span style="font-weight: bold; font-family: verdana; ">DEVIATION</span> 
          
                                 <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr class="theader">
                                                   <td class="widthSize20_per">Level</td>
                                                   <td class="widthSize60_per">Description</td>
                                                   <td class="widthSize20_per">Code</td>
                                                   
                                               </tr>
                                               <tr>
                                                   <td><select id="Select61" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td><select id="Select8" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text41" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select1" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td><select id="Select2" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text1" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select3" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td><select id="Select4" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text2" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select5" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td><select id="Select6" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text3" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>



                                        </table>

                                <div class="clear"></div>
                                
                                </div>
                                        
                                        <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px; float: right;" >
                                            <span style="font-weight: bold; font-family: verdana;">DEVIATION FOUND</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="leftAlign widthSize30_per" style="padding-left: 10px;">AGE</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text4" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize30_per" style="padding-left: 10px;">DBR</td>
                                                   <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text5" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize30_per" style="padding-left: 10px;">SEATING CAPACITY</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text6" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize30_per" style="padding-left: 10px;">SHARE PORTION</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text7" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>




                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                        <div class="clear"></div>
                                        <br/>
                                        <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;" >
                                            <span style="font-weight: bold; font-family: verdana;">STRENGTH [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select7" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select9" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select10" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select11" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select17" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>

                                         <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px; float: right;" >
                                            <span style="font-weight: bold; font-family: verdana;">WEAKNESS [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select12" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select13" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select14" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select15" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                                <tr>
                                                   <td><select id="Select16" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="clear"></div>
                                         <br/>
                                         
                                         <div class="divLeftNormalClass widthSize48_per" style="margin-bottom: 0px;" >
                                            <span style="font-weight: bold; font-family: verdana;">REPAYMENT METHOD</span> 
          
                                 <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Asking:</span>
                                        <div class="divLeft widthSize80_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                           
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Instrument:</span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                           <select class="comboSizeH_24">
                                               <option value="-1">Select</option>
                                               <option value="0">PDC</option>
                                               <option value="1">SI</option>
                                           </select>
                                            </div>
                                            <span class="spnLeftLabel1 widthSize18_per" style="margin-left:10px;">SCB Account:</span>
                                        <div class="divLeft widthSize35_per" style="padding:0px; border:none;">
                                            <select class="comboSizeH_24"><option value="-1">Account Number</option></select>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Security PDC:</span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <select class="comboSizeH_24">
                                               <option value="-1">Select</option>
                                               <option value="1">Yes</option>
                                               <option value="0">No</option>
                                           </select>
                                            </div>
                                            <span class="spnLeftLabel1 widthSize18_per" style="margin-left:10px;">No of PDC(s):</span>
                                        <div class="divLeft widthSize35_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per" >PDC Amount:</span>
                                        <div class="divLeft widthSize80_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                    </div>
                                     <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Bank Name:</span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <select class="comboSizeH_24">
                                               <option value="-1">Select a Bank</option>
                                           </select>
                                            </div>
                                            <span class="spnLeftLabel1 widthSize18_per" style="margin-left:10px;">Account No:</span>
                                        <div class="divLeft widthSize35_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>

                                    

                                
                                
       </div>
                                         <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px; float: right;" >
                                            <span style="font-weight: bold; font-family: verdana;">CONDITIONS [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select23" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select24" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select25" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select26" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                                <tr>
                                                   <td><select id="Select27" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="clear"></div>
                                         <br/>
                                         <div class="divLeftBig widthSize98_per centerAlign" style="margin-left: 5px; margin-bottom: 0px; float: right;" >
                                            <span style="font-weight: bold; font-family: verdana;">REMARKS [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select18" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select19" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select20" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        
                                        <div class="fieldDiv"><textarea id="Textarea2" class="txtFieldBig widthSize100_per" cols="1" rows="3" style="height: 84px;_height: 84px;*height: 84px;"></textarea> </div>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="clear"></div>
                                         <br/>
                                         
                                         
                                         
                                         
                                         
                                         
                                         <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;" >
                                            <span style="font-weight: bold; font-family: verdana;">REQUIRED DOCUMENT</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="rightAlign widthSize10_per">LETTER OF INTRODUCTION</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select21" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">PAYSLIPS/CASH VOUCHERS</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select22" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">TRADE LICENSE</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select28" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">MOA & AOA</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select29" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">PARTNERSHIP DEED</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select30" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">FORM X/FORM XII/FORM I</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select36" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">BOARD RESOULATION</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select37" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">DIPLOMA CERTIFICATE</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select38" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">PROFESSIONAL CERTIFICATE</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select39" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">MUNICIPAL TAX RECIPT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select40" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">TITLE DEED</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select41" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">RENTAL DEED</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select42" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">UTILITY BILLS</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select43" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">LOAN OFFER LETTER</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select44" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>

                                               
                                        </table>
                                        <div class="clear"></div>
                                         </div>

                                         <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px; float: right;" >
                                            <span style="font-weight: bold; font-family: verdana;">VERIFICATION REPORTS</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="rightAlign widthSize10_per">CPV REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select21" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CAR PRICE VERIFCATION REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select22" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">STATEMENT AUTHENTICATION</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select28" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CREDIT REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select29" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CAR VERIFICATION REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select30" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">USED CAR VALUATION REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select36" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">USED CAR DOCS</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select37" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CARD REPAYMENT HISTORY</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select38" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="divLeftNormalClass widthSize48_per" style="margin-bottom: 0px; float: right; margin-top: 5px;" >
                                            <span style="font-weight: bold; font-family: verdana;">APPRAISER & APPROVER</span> 
          
                                 
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Appraised By:</span>
                                        <div class="divLeft widthSize64_per" style="padding:0px; border:none;">
                                           <select class="comboSizeH_24">
                                               <option value="-1">Select a System User</option>
                                           </select>
                                            </div>
                                            
                                        <div class="divLeft widthSize15_per" style="padding:0px; border:none;margin-left:5px;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Appraised By:</span>
                                        <div class="divLeft widthSize64_per" style="padding:0px; border:none;">
                                           <select class="comboSizeH_24">
                                               <option value="-1">Select a System User</option>
                                           </select>
                                            </div>
                                            
                                        <div class="divLeft widthSize15_per" style="padding:0px; border:none;margin-left:5px;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Security PDC:</span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <select class="comboSizeH_24">
                                               <option value="-1">Select</option>
                                               <option value="1">Yes</option>
                                               <option value="0">No</option>
                                           </select>
                                            </div>
                                            <span class="spnLeftLabel1 widthSize18_per" style="margin-left:10px;">No of PDC(s):</span>
                                        <div class="divLeft widthSize35_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per" >PDC Amount:</span>
                                        <div class="divLeft widthSize80_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                    </div>
                                     <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize18_per">Bank Name:</span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <select class="comboSizeH_24">
                                               <option value="-1">Select a Bank</option>
                                           </select>
                                            </div>
                                            <span class="spnLeftLabel1 widthSize18_per" style="margin-left:10px;">Account No:</span>
                                        <div class="divLeft widthSize35_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>

                                    

                                
                                
       </div>
                                         
                                         <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px; float: right; margin-top: 5px;" >
                                            <span style="font-weight: bold; font-family: verdana;">VERIFICATION REPORTS</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="rightAlign widthSize10_per">CPV REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select31" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CAR PRICE VERIFCATION REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select32" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">STATEMENT AUTHENTICATION</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select33" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CREDIT REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select34" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CAR VERIFICATION REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select35" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">USED CAR VALUATION REPORT</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select45" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">USED CAR DOCS</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select46" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                                <tr>
                                                   <td class="rightAlign widthSize10_per">CARD REPAYMENT HISTORY</td>
                                                   <td class="widthSize10_per"><input class="txtBoxSize_H22" disabled="disabled"/></td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;" class=" widthSize10_per"><select id="Select47" class="widthSize100_per comboSizeH_24">
                                                            <option value="-1">Select</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                            </select></td>
                                                </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="clear"></div>
                                         <br/>
                                         
                                         
                                         
                                         
                                         
                                         
                                         


                                        <div style="margin: 0px;border: solid 1px green; border-radius: 15px;">
                                        <div class="divLeftBig widthSize73_per" style="margin-bottom: 0px; border: none;" >
                                            <span style="font-weight: bold; font-family: verdana;">LOAN SUMMARY</span> 
          
                                 <div class="div3in1">
                                     <span class="spnLeftLabel1"><label class="lbl" style="width: 140px;_width: 140px;*width: 140px; text-align: right; font-weight: bold;">Total Loan Amount:</label></span>
                                        <div class="divLeft widthSize50_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1"><label class="lbl" style="width: 140px;*width: 140px;_width: 140px;">LTV Excluding Insurance:</label></span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                            <span class="spnLeftLabel1"><label class="lbl" style="width: 140px;*width: 140px;_width: 140px;  margin-left: 25px;">LTV Excluding Insurance:</label></span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1"><label class="lbl" style="width: 140px;*width: 140px;_width: 140px;">LTV Excluding Insurance:</label></span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                            <span class="spnLeftLabel1"><label class="lbl" style="width: 140px;*width: 140px;_width: 140px;  margin-left: 25px;">LTV Excluding Insurance:</label></span>
                                        <div class="divLeft widthSize25_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                   

                                    

                                <div class="clear"></div>
                                
       </div>
                                        <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px; border: none;" >
                                            
                                            <br/>
          
                                 <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize30_per rightAlign" style="font-weight: bold;">EMR:</span>
                                        <div class="divLeft widthSize65_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize30_per leftAlign rightAlign">Extra LTV:</span>
                                        <div class="divLeft widthSize65_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>
                                    <div class="div3in1">
                                     <span class="spnLeftLabel1 widthSize30_per leftAlign rightAlign">Extra DBR:</span>
                                        <div class="divLeft widthSize65_per" style="padding:0px; border:none;">
                                            <input class="txtBoxSize_H22" disabled="disabled"/>
                                            </div>
                                            
                                    </div>

                                    

                                <div class="clear"></div>
                                
       </div>
       <div class="clear"></div>
  
        </div>                             
                                       <div class ="clear"></div>
                                        </div>
       
                                    

       
    </form>
</body>
</html>
