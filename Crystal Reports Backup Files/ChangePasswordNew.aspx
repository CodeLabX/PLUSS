﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="ChangePasswordNew.aspx.cs" Inherits="PlussAndLoan.UI.ChangePasswordNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <center>
        <fieldset class="WidthHeight" style="border: 1px; border-color: Black;">
            <%--<table class="WidthHeight">
                <tr>
                    <td align="left" colspan="3" style="padding-left: 160px;">
                        <h1><b>Change Password</b></h1>
                    </td>
                </tr>
                <tr>
                    <td align="center" width="350" colspan="3">
                        <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="350"></td>
                    <td>
                        <asp:TextBox ID="userIdTextBox" runat="server" Width="150px" MaxLength="7"></asp:TextBox>
                        
                    </td>
                    <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Old Password:&nbsp; </td>
                    <td>
                        <asp:TextBox ID="oldPasswordTextBox" runat="server" Width="150px"
                            TextMode="Password" MaxLength="14"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">New Password:&nbsp; </td>
                    <td>
                        <asp:TextBox ID="newPasswordTextBox" runat="server" Width="150px"
                            TextMode="Password" MaxLength="14"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" width="250">Confirm Password:&nbsp; </td>
                    <td>
                        <asp:TextBox ID="confrimPasswordTextBox" runat="server" Width="150px"
                            TextMode="Password" MaxLength="14"></asp:TextBox>
                    </td>
                    <td width="300">
                        <asp:CompareValidator ID="passwordCompareValidator" runat="server"
                            ErrorMessage="Password does not match" Width="180px"
                            ControlToCompare="newPasswordTextBox"
                            ControlToValidate="confrimPasswordTextBox"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">&nbsp;</td>
                    <td align="left" colspan="2">
                        <asp:Button ID="submitButton" runat="server" Text="Submit" Width="100px"
                            OnClick="submitButton_Click" />&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="LogInPageLinkButton" runat="server" Font-Bold="True"
                        OnClick="LogInPageLinkButton_Click" Font-Size="14pt">Log In</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3"></td>
                </tr>
            </table>--%>
            <div class="form-style-2">
                <div class="form-style-2-heading">Password Change</div>
                <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                <label for="field1">
                    <span>User Id <span class="required">*</span></span>
                    <asp:TextBox ID="userIdTextBox" runat="server" CssClass="input-field"/>
                </label>
                <label for="field1">
                    <span>Old Password <span class="required">*</span></span>
                    <asp:TextBox ID="oldPasswordTextBox" TextMode="Password" runat="server" CssClass="input-field" />
                </label>
                <label for="field1">
                    <span>New Password <span class="required">*</span></span>
                    <asp:TextBox ID="newPasswordTextBox" TextMode="Password" runat="server" CssClass="input-field" />
                </label>
                <label for="field1">
                    <span>Confirm Password <span class="required">*</span></span>
                    <asp:TextBox ID="confrimPasswordTextBox" TextMode="Password" runat="server" CssClass="input-field" />
                </label>
                <asp:CompareValidator ID="passwordCompareValidator" runat="server"
                    ErrorMessage="Password does not match" Width="180px"
                    ControlToCompare="newPasswordTextBox"
                    ControlToValidate="confrimPasswordTextBox"></asp:CompareValidator>
                <asp:Button ID="submitButton" runat="server" Text="Submit" Width="100px"
                    OnClick="submitButton_Click" CssClass="btn primarybtn" />&nbsp;&nbsp;&nbsp;
                    <%--<asp:LinkButton ID="LogInPageLinkButton" runat="server" Font-Bold="True"
                        OnClick="LogInPageLinkButton_Click" Font-Size="14pt">Log In</asp:LinkButton>--%>
            </div>
        </fieldset>
    </center>
</asp:Content>
