﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.Master" AutoEventWireup="true" CodeBehind="AuditReport.aspx.cs" Inherits="PlussAndLoan.UI.AuditReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function PrintDiv() {
            //debugger;
            var myContentToPrint = document.getElementById("ShowDiv");
            var myWindowToPrint = window.open('', '', 'width=800,height=600,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="reportDiv" runat="server">
        <table width="780" border="0" align="center" cellpadding="0" cellspacing="2" bgcolor="BDD3A5">
            <tr>
                <td height="20" colspan="7">
                    <div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">VIEW AUDIT REPORT</h1></font></div>
                </td>
            </tr>

            <tr>
                <td width="86" height="30" bgcolor="#D5E2C5">
                    <div align="right"></div>
                </td>
                <td width="57" height="30" align="left" bgcolor="#E7F7C6">
                    <div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Year :</font></div>
                </td>
                <td width="57" align="left" bgcolor="#E7F7C6">
                    <asp:DropDownList ID="YearDropDownList" runat="server"></asp:DropDownList>
                </td>
                <td width="56" height="30" align="left" bgcolor="#E7F7C6">
                    <div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Month: </font></div>
                </td>
                <td width="58" align="left" bgcolor="#E7F7C6">
                    <asp:DropDownList ID="MonthDropDownList" runat="server"></asp:DropDownList>
                </td>
                <td width="104" align="left" bgcolor="#E7F7C6"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Action Name :</font></td>
                <td width="346" align="left" bgcolor="#E7F7C6">
                    <div align="left">
                        <font face="Verdana, Arial, Helvetica, sans-serif">
        <asp:DropDownList ID="action_name" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="Add" Value="1" />
                                <asp:ListItem Text="Edit" Value="2" />
                                <asp:ListItem Text="All" Value="0" />
                                
                            </asp:DropDownList>
      </font>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;      </font></td>
                <td height="30" colspan="5"><font face="Verdana, Arial, Helvetica, sans-serif">
             <asp:Button runat="server" OnClick="btnView_OnClick" Text="VIEW" ID="btnView"  style="border:1px solid #999990; background-color: #F5F8FC; text-align: center;" Height="25" Width="45" />
            
	    
	  </font></td>
            </tr>

        </table>
    </div>

    <div id="ShowReportDiv" runat="server">
        <div id="ShowDiv">
            <div>
                <table width="780" height="79" cellpadding="2" cellspacing="1" bordercolor="#00CCCC" style='border: 1px solid #333333;'>
                    <tr bgcolor="BDD3A5">
                        <td height="20" colspan="5" align="left" valign="middle">
                            <strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif"><CENTER>AUDIT REPORT</CENTER></font></strong>
                        </td>
                        <td align="right" valign="middle" bgcolor="BDD3A5">
                            <font size="-1" face="Courier New, Courier, mono"><center>Click on the Icon to print this Report.</center> </font>
                        </td>
                        <td align="center" valign="middle" bgcolor="BDD3A5">
                            <%--<img src="images\printer.gif" OnClientClick="PrintDiv()" id="printButton" runat="server"/>--%>
                           <%-- <asp:Button ID="Button2" runat="server" Text="Print" Width="40px" Height="30px"
                                OnClientClick="PrintDiv()" />--%>
                            <asp:ImageButton  ID="imgexpand" runat="server" ImageAlign="Bottom" ImageUrl="images\printer.gif" OnClientClick="PrintDiv()" />

                        </td>
                        <td align="left" valign="middle" bgcolor="BDD3A5">
                            <asp:Button runat="server" OnClick="closebutton_OnClick" Text="CLOSE" ID="Button1" Style='border: 1px solid #83b67a; background-color: #D8EED5' />
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="10%" height="40" bgcolor="#D5E2C5"><b><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Action 
            Type </font></b>
                            <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></div>
                        </td>
                        <td width="23%" bgcolor="#E7F7C6">
                            <div align="left" id="actionDiv" runat="server">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		  </font>
                            </div>
                        </td>
                        <td width="15%" bgcolor="#D5E2C5">
                            <div align="right">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		  <b>Month:</b></font>
                            </div>
                        </td>
                        <td width="16%" bgcolor="#E7F7C6">
                            <div align="left" id="monthDiv" runat="server">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		 
              </font>
                            </div>
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
            <div align="center"></div>
            <div align="center"></div>
            </font></td>
                        <td width="11%" bgcolor="#D5E2C5">
                            <div align="right">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>Year:</b></font>
                            </div>
                        </td>
                        <td width="24%" colspan="3" bgcolor="#E7F7C6">
                            <div align="left" id="yearDiv" runat="server">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		 </font>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>
            <br />
            <br />
            <br />
            <div id="divAuditReport" style="overflow-y: scroll; height: 380px; width: 100%;" class="GridviewBorder">
                <asp:GridView ID="AuditGridView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    Font-Size="11px" PageSize="8"
                    Width="100%" EnableModelValidation="True" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                    <RowStyle BackColor="White" ForeColor="#333333" />
                    <Columns>
                        <asp:TemplateField HeaderText="User Name" HeaderStyle-Width="40%" HeaderStyle-Height="50px">
                            <ItemTemplate>
                                <asp:Label ID="UserName" runat="server" Text='<%# Bind("USER_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="15%"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Module Name" HeaderStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="ModuleName" runat="server" Text='<%# Bind("MODULE_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="15%"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action Name" HeaderStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="ActionName" runat="server" Text='<%# Bind("ACTION_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="15%"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Query Date" HeaderStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="QueryDate" runat="server" Text='<%# Bind("QRY_DATE","{0:dd/mm/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="15%"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Query Time" HeaderStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="QueryTime" runat="server" Text='<%# Format12Hour(Eval("QRY_TIME")) %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="15%"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Query String" HeaderStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="QueryString" runat="server" Text='<%# Bind("QRY_STRING") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="15%"></HeaderStyle>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle BackColor="#BDD3A5" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        &nbsp;&nbsp;No data found.
                    </EmptyDataTemplate>
                    <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                    <FooterStyle CssClass="GvFixedFooter" BackColor="White" ForeColor="#333333" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
