﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebConfigEncryption.aspx.cs"
    Inherits="LAMS.Web.UI.WebConfigEncryption" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Configure Web Config of LAMS</title>
</head>
<body>
    <form id="form1" runat="server">
    <h2 align="center" style="background-color: green; color: white">
        Setup web config of Loan locator and PLUSS
    </h2>
    <div>
        <table align="center">
            <tr>
                <td>
                    Connection Name:
                </td>
                <td>
                    <asp:DropDownList ID="ddlConnectionName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlConnectionName_SelectedIndexChanged">
                        <asp:ListItem>conString</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="height: 26px">
                    Server:
                </td>
                <td style="height: 26px">
                    <input type="text" id="txtServerName" runat="server" style="width: 176px" />
                </td>
            </tr>
            <tr>
                <td>
                    Database:
                </td>
                <td>
                    <input type="text" id="txtDatabaseName" runat="server" style="width: 176px" />
                </td>
            </tr>
            <tr>
                <td>
                    User Id:
                </td>
                <td>
                    <input type="text" id="txtDbUserId" runat="server" style="width: 176px" />
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <input type="password" id="txtDbPassword" runat="server" style="width: 176px" />
                </td>
            </tr>
            <tr>
                <td>
                    Confirm Password:
                </td>
                <td>
                    <input type="password" id="txtConPassword" runat="server" style="width: 176px" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="txtConfigure" runat="server" Text="Configure" OnClick="txtConfigure_Click" />
                    <br />
                    <asp:Label runat="server" ID="lblMessage" ForeColor="red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
