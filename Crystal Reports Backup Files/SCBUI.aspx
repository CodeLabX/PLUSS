﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SCBUI" MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="SCBUI.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    function getSelText() {
        debugger;
        var clipBoardText = "";
        if(window.clipboardData)
        {
           clipBoardText = window.clipboardData.getData('Text');
           document.getElementById('ctl00_ContentPlaceHolder_hiddenField').value="";
           document.getElementById('ctl00_ContentPlaceHolder_hiddenField').value = clipBoardText;
        }
    }       
    </script>
    

    <script type="text/javascript">
       function GetKeyPress(textBoxControl1,textBoxControl2, maxLength)
       {
          if(document.getElementById(textBoxControl1).value.length >= maxLength)
          {
            document.getElementById(textBoxControl2).focus();
          }
       }
    </script>

    <style type="text/css">
        .style1
        {
            width: 50px;
        }
        .style2
        {
            width: 98px;
        }
        .style3
        {
            width: 272px;
        }
        .style4
        {
            width: 382px;
        }
        
        .style5
        {
            width: 124px;
        }
        
    </style>

    <script type="text/javascript">
       function GetAdjustmentTextBoxKeyPress(textBoxControl1,textBoxControl2,textBoxControl3)
       {
            var result = document.getElementById(textBoxControl1).value + document.getElementById(textBoxControl2).value;
            //alert(result);
            document.getElementById(textBoxControl3).value = result;         
       }
    </script>

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div >
        <asp:HiddenField ID="hiddenField" runat="server" />
        <table style="width: 100%;" border="0" cellpadding = "0" cellspacing = "1" >
            <tr align="center">
                
                <td colspan="2" style="font-size:large">STATISTICAL HISTORY (12 months)</td>
                
            </tr>
            
            <tr >
                <td colspan ="2">
                </td>
            </tr>
            
            <tr align="right">
                
                <td align ="right">
                    <asp:Label ID="Label15" runat="server" Text="LL ID : " Width="40"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="llidTextBox" runat="server" Width="166px" MaxLength="9"></asp:TextBox>
                    <asp:Button ID="findButton" runat="server" Width="20px" Text="..." OnClick="findButton_Click" />
                    <asp:RequiredFieldValidator ID="llidValidator" runat="server" ControlToValidate="llidTextBox" EnableClientScript="false" ErrorMessage="Paste a valid Data"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="acNumberDropDownList" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="acNumberDropDownList_SelectedIndexChanged" 
                        Width="120px">
                    </asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td align ="right" >
                    <asp:Label ID="Label16" runat="server" Text="A/C Name : " ></asp:Label>
                </td>
                <td >
                    <asp:TextBox ID="acNameTextBox" runat="server" Width="166px"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td align ="right" >
                    <asp:Label ID="Label17" runat="server" Text="A/C Number : "></asp:Label>
                </td>
                <td style="width:170px;" align="left">
                    <asp:TextBox ID="acNoTextBox1" runat="server" Width="33px" MaxLength="2"></asp:TextBox>
                    <asp:TextBox ID="acNoTextBox2" runat="server" Width="78px" MaxLength="7"></asp:TextBox>
                    <asp:TextBox ID="acNoTextBox3" runat="server" Width="33px" MaxLength="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align ="right" class="style1" >
                    <asp:Button ID="browseButton" runat="server" Text="Paste" Width="100px" Height="27px" onclick="browseButton_Click" />
                </td>
                
                <td >
                    <br />
                </td>
                
            </tr>
            </table>
            
    <%--<div class="GridviewBorder2">  --%>   
        <asp:GridView ID="SCBGridView" runat="server" AutoGenerateColumns="false" 
            style="margin-top:1px; margin-bottom:1px;" CssClass="GridviewBorder2" >
            <Columns>
                <asp:TemplateField HeaderText="Period" HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="periodLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Month End Ledger Bal" >
                    <ItemTemplate>
                        <asp:Label ID="monthEndLedgerBalLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Max Ledger Balance" >
                    <ItemTemplate>
                        <asp:Label ID="maxLedgerBalanceLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Min Ledger Balance" >
                    <ItemTemplate>
                        <asp:Label ID="minLedgerBalanceLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Average Ledger Balance" >
                    <ItemTemplate>
                        <asp:Label ID="averageLedgerBalanceLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Adj (month) Avg Bal" >
                    <ItemTemplate>
                        <asp:DropDownList ID="adjmonthAvgBalDropdownList" runat="server"
                            Width="50px"  
                            AutoPostBack="True" 
                            onselectedindexchanged="adjmonthAvgBalDropdownList_SelectedIndexChanged">
                            <asp:ListItem Text="Yes" Value="1">                                
                            </asp:ListItem>
                            <asp:ListItem Selected="True" Text="No" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                    <HeaderStyle Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Credit Transaction Count">
                    <ItemTemplate>
                        <asp:Label ID="creditTransactionCountLabel" runat="server" Width="40px"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="40px" HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Total Transcation Count" >
                    <ItemTemplate>
                        <asp:Label ID="totalTranscationCountLabel" runat="server" Width="40px"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Debit Turnover" >
                    <ItemTemplate>
                        <asp:Label ID="debitTurnoverLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Credit Turnover" >
                    <ItemTemplate>
                        <asp:Label ID="creditTurnoverLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Adjustment" >
                    <ItemTemplate>
                        <asp:TextBox ID="adjustmentTextBox" runat="server" BorderStyle="None" 
                            BorderWidth="0px" Width="70px" Text="0" CssClass="RightAlignTextBox" 
                            ontextchanged="adjustmentTextBox_TextChanged" AutoPostBack="true" ></asp:TextBox>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Adjusted CTO" >
                    <ItemTemplate>
                        <asp:Label ID="adjustedCTOLabel" runat="server" ></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Adj (month) CTO" >
                    <ItemTemplate>
                        <asp:DropDownList ID="adjmonthCTODropDownList" runat="server" 
                            BorderStyle="None" BorderWidth="0px" Width="50px" 
                            onselectedindexchanged="adjmonthCTODropDownList_SelectedIndexChanged" 
                            AutoPostBack="True" >
                            <asp:ListItem Text="Yes" Value="1">       
                            </asp:ListItem>
                            <asp:ListItem Selected="True" Text="No" Value="0" >
                            </asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                    <HeaderStyle Width="50px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                
            </Columns>
            <HeaderStyle CssClass="SCBHeader" />
        </asp:GridView>
    <%--</div>--%>
    
    <table style="width: 100%;"  border="0" cellpadding = "0" cellspacing = "1">
        <tr>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div id="gridbox" style="background-color:white; width: 180%;">
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
        <td >
        <div style="width:181%">
            <table>
                <tr>
                    <td class="style2">
                    </td>
                    <td style="font-weight:bold; " class="style5">
                        <asp:Label ID="total12Label" runat="server" Text="12 Month Total"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="total12ValueLabel" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                    <td class="style4">
                        &nbsp;</td>
                    <td style="font-weight: bold; " align="left" class="style3">
                        <asp:Label ID="totalCto12Label" runat="server" Text="12 Month Total" Width="100px"></asp:Label>
                    </td>
                    
                    <td style="font-weight: bold; width: 100px" align="left">
                        <asp:Label ID="totalValueCto12Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                </tr>
            
                <tr>
                    <td class="style2">
                    </td>
                    <td style="font-weight:bold; " class="style5">
                        <asp:Label ID="average12Label" runat="server" Text="12 Month average"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="average12ValueLabel" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                    <td class="style4">
                    </td>
                    <td style="font-weight: bold" align="left">
                        <asp:Label ID="averageCto12Label2" runat="server" Text="12 Month Average"></asp:Label>
                    </td>
                    
                    
                    <td style="font-weight:bold" align="left">
                        <asp:Label ID="averageValueCto12Label" runat="server" Text="0"  ></asp:Label>
                    </td>
                </tr>
            
                <tr>
                    <td class="style2">
                    </td>
                    <td style="font-weight:bold; " class="style5">
                        <asp:Label ID="percent12Label" runat="server" Text="50%"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="parcentValue12Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                    <td class="style4">
                    </td>
                    <td class="style3">
                        &nbsp;</td>
                    <td style="width:500px">
                        &nbsp;</td>
                    <td style="width:500px">
                        &nbsp;</td>
                    <td style="font-weight:bold; width:100px">
                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="Label4" runat="server" Text="" Width="100px"></asp:Label>
                    </td>
                </tr>
            
                <tr>
                    <td colspan="8"></td>
                </tr>
                <tr>
                    <td colspan="8"></td>
                </tr>
                    
                <tr>
                    <td class="style2">
                    </td>
                    <td style="font-weight:bold; " class="style5">
                        <asp:Label ID="total6Label" runat="server" Text="6 Month Total"></asp:Label>
                    </td>
                    
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="totalValue6Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                    
                    <td style="font-weight: bold" align="right">
                    </td>
                    
                    <td style="font-weight:bold; width:100px" align="left">
                        <asp:Label ID="totalCto6Label" runat="server" Text="6 Month Total" Width="100px"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="left">
                        <asp:Label ID="totalvalueCto6Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                </tr>
                    
                <tr>
                    <td class="style2">
                    </td>
                    <td style="font-weight:bold; " class="style5">
                        <asp:Label ID="average6Label" runat="server" Text="6 Month Average"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="averageValue6Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                    
                    <td style="font-weight: bold; width: 120px" align="left">
                    </td>
                    
                    <td style="font-weight:bold; width:120px" align="left">
                        <asp:Label ID="averageCto6Label" runat="server" Text="6 Month Average"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="left">
                        <asp:Label ID="averageValueCto6Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                </tr>
                    
                <tr>
                    <td class="style2">
                    </td>
                    <td style="font-weight:bold; " class="style5">
                        <asp:Label ID="percent6Label" runat="server" Text="50%"></asp:Label>
                    </td>
                    <td style="font-weight:bold" align="right">
                        <asp:Label ID="percentValue6Label" runat="server" Text="0" Width="100px"></asp:Label>
                    </td>
                    <td class="style4">
                    </td>
                    <td class="style3">
                        &nbsp;</td>
                    <td style="width:500px">
                        &nbsp;</td>
                    <td style="width:500px">
                        &nbsp;</td>
                    <td style="font-weight:bold; width:100px">
                        <asp:Label ID="Label13" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="font-weight:bold; width:100px" align="right">
                        <asp:Label ID="Label14" runat="server" Text="" Width="100px"></asp:Label>
                    </td>
                </tr>
            
            </table>
        </div>
        </td>
        </tr>
    </table>
    </div>
    
    <table style="width:100%;" border = "0">
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            
            <td align="center">
                <asp:Button ID="saveButton" runat="server" Text="Save" Width="95px" Height="30px" 
                    onclick="saveButton_Click" />
                <asp:Button ID="clearAllButton" runat="server" Text="Clear all" Width="95px" 
                    Height="30px" onclick="clearAllButton_Click" />
                <asp:Button ID="printButton" runat="server" Text="Print" Width="95px" Height="30px" 
                    onclick="printButton_Click" />
            </td>
            
        </tr>
        <tr>
            <td >
            </td>
        </tr>
    </table>
 </asp:Content>
