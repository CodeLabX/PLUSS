﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_CIBResultUploadUI" Title="CIB Result Upload" Codebehind="CIBResultUploadUI.aspx.cs" %>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<style type="text/css">
        .tableWidth
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
        .tableWidth td
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
</style>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
<tr style="font-weight:bold; font-size:16px" align="center"  >
<td><asp:Label ID="nameLabel" runat="server" Text="CIB Result Upload"></asp:Label></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
    <asp:FileUpload ID="FileUpload" runat="server" Width="350px" Height="24px" />
    <asp:Button ID="UploadButton" runat="server" Text="Upload" Width="100px" 
        Height="24px" onclick="uploadButton_Click" />
    </td>
</tr>
<tr>
<td>
<%--<div id="gridbox"  style="background-color:white; width:805px; height:470px;"></div>--%>
<div id="tableHeadDiv" runat="server" style="border: solid 1px gray;"></div>
<div id="recordViewDiv" runat="server" style="width:805px; height:470px; z-index: 5000; border: solid 1px gray;" ></div>
<div id="pagingDiv" runat="server"></div>

</td>
</tr>
</table>
    <script type="text/javascript">
//	var mygrid;
//	mygrid = new dhtmlXGridObject('gridbox');
//	mygrid.setImagePath("../codebase/imgs/");
//	mygrid.setHeader("SL,LLID,Customer Name,Status,Remarks,Father's Name,Mother's Name,Ref Number,Type");
//	mygrid.setInitWidths("28,60,135,65,130,120,120,80,48");
//	mygrid.setColAlign("left,left,left,left,left,left,left,left,left");
//	mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
//	mygrid.setColSorting("int,int,str,str,str,str,str,str,str");
//	//mygrid.setSkin("modern");
//	mygrid.setSkin("light");	 
//	mygrid.init();
//	mygrid.enableSmartRendering(true,20);
	//mygrid.loadXML("../includes/CIBResultList.xml");
	
//	var maindiv=document.getElementById('recordViewDiv');
//	var maindiv2=document.getElementById('dataTable');
//	var maindiv3=document.getElementById('pagingTable');
//	maindiv.getAttribute(maindiv2);
//	maindiv.getAttribute(maindiv3);
//	maindiv.position = 'absolute';
	</script>
    <script type="text/javascript">
    function onLoadPage()
    {
        init('dataTable');
    }	
	</script>
</asp:Content>


