﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="DedupApproval.aspx.cs" Inherits="PlussAndLoan.UI.Dedups.DedupApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Decline UnApproved List</div>

    </div>
    <div style="width: 95%; height: 400px; padding-left: 20px; overflow: auto;">
        <asp:GridView ID="DedupDeclineInfo" CssClass="myGridStyle" AutoGenerateColumns="false" runat="server" OnRowCommand="DedupDeclineInfo_RowCommand">
            <RowStyle HorizontalAlign="Center" />
            <Columns>
                <asp:BoundField DataField="DEDU_LOANACCOUNTNO" HeaderText="LLID" />
                <asp:BoundField DataField="NM" HeaderText="Name" />
                <asp:BoundField DataField="FATHE_NM" HeaderText="Father Name" />
                <asp:BoundField DataField="MOTHE_NM" HeaderText="Mother Name" />
                <asp:BoundField DataField="SCB_MST_NO" HeaderText="SCB MST NO" />
                 <asp:BoundField DataField="DOB" HeaderText="DOB" dataformatstring="{0:MM-dd-yyyy}" />
                <asp:BoundField DataField="MAKER_PS_ID" HeaderText="Maker Id" />
                <%--<asp:BoundField DataField="NM" HeaderText="NM" />--%>
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button Text="Select" runat="server" CommandName="Select" CommandArgument='<%# Eval("DEDU_LOANACCOUNTNO") + "-" + Eval("DE_DUP_ID") %>' />
                        <%--<asp:Button Text="Delete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("DEDU_LOANACCOUNTNO") + "-" + Eval("DE_DUP_ID") %>' />--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
