﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_DisburseLoanUploadUI" Title="Disburseloan Upload" Codebehind="DisburseLoanUploadUI.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="CalendarInitialJs" Src="~/UI/UserControls/js/CalendarInitialJs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PopupWindowsJs" Src="~/UI/UserControls/js/PopupWindowsJs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PopupWindows" Src="~/UI/UserControls/css/PopupWindows.ascx" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="Server">
    <uc1:PopupWindows runat="server" ID="PopupWindows" />
    <uc1:PopupWindowsJs runat="server" ID="PopupWindowsJs" />
    <uc1:CalendarInitialJs runat="server" ID="CalendarInitialJs" />

    <script type="text/javascript">
    window.setInterval("updateTime()", 1000);    
    function updateTime()
    {
        var now = new Date();
        var tHrs = now.getHours();
        var tMin = now.getMinutes();
        var tSec = now.getSeconds();
        var tTime = ((tHrs < 10)?"0" + tHrs:tHrs) + ":" + ((tMin < 10)?"0" + tMin:tMin) + ":" + ((tSec < 10)?"0" + tSec:tSec);
        GetUploadCount();
    }   
    function GetUploadCount()
    {
        var uploadCount = '<%=Session["uploadCount"]%>';
        document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML ="Please wait data uploading...="+uploadCount;

    } 
    function LoadPopupWindows()
    {
        ShowPopUpWindows('pleaseWaidDiv',mainBody);
    }
    function PopUpWindowsClose()
    {
        closeDialog('pleaseWaidDiv');
    }
   function ExcelFileLoad()
   {
        var fileNamePath =  '<%=Session["fileName"]%>';
        if(fileNamePath.length<4)
        {
            alert("Please select file name");
            return;
        }
        try
        {
            var XMLData;
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");         
            if(XMLHttpRequestObject) 
            {
                
                XMLHttpRequestObject.open("POST", "DisburseLoanUploadUI.aspx?operation=upload");                        
                XMLHttpRequestObject.onreadystatechange = function ()
                {
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
                       {  
                        var responseData=new Array();
                        responseData=XMLHttpRequestObject.responseText.split('#');
                        if(parseInt(responseData[0])>0)
                        {
                            document.getElementById('okButton').style.display="none";
                            document.getElementById('loadingImg').style.display="block";
                            document.getElementById('ctl00_ContentPlaceHolder_TotalRecordLabel').innerHTML = "Total Record="+ responseData[0];    
                            document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML ="Please wait data uploading...";
                            LoadPopupWindows(); 
                            UploadDataInDB(responseData[0]); 
                        }
                        else
                        {
                            document.getElementById('okButton').style.display="block";
                            document.getElementById('loadingImg').style.display="none";
                            document.getElementById('ctl00_ContentPlaceHolder_TotalRecordLabel').innerHTML = responseData[0];    
                            document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML ="File not formated";
                            LoadPopupWindows(); 
                        }                    
                       }
                }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                XMLHttpRequestObject.setRequestHeader("Content-length", fileNamePath.length);
                XMLHttpRequestObject.send(fileNamePath);
            }    
          }
          catch(err)
          {}   
    }
    
    function UploadDataInDB(totalRecord)
    {
        var modResult=0;
        var completeData=0;
        var tempCompleteData=0;
        var modResult=Math.round(parseInt(totalRecord)%100);
        var count=0;
        if(parseInt(totalRecord)>0)
        {
           completeData=completeData+100;            
           StartUpload(totalRecord,completeData,completeData-100); 
        }
    }
   function StartUpload(totalRecord,recordCount,tempCompleteData)
   {
        try
        {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");         
            if(XMLHttpRequestObject) 
            {
                XMLHttpRequestObject.open("POST", "DisburseLoanUploadUI.aspx?operation=save&NoofRecord="+recordCount+"&LowIndex="+tempCompleteData);                        
                XMLHttpRequestObject.onreadystatechange = function ()
                {
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
                       {  
                        var responseData=new Array();
                        responseData=XMLHttpRequestObject.responseText.split('#');
                        document.getElementById('ctl00_ContentPlaceHolder_TotalRecordLabel').innerHTML = "Total Record="+ totalRecord + " in complete="+responseData[0];
                        if(parseInt(totalRecord)==parseInt(responseData[0]))
                        {
                            document.getElementById('okButton').style.display="block";
                            document.getElementById('loadingImg').style.display="none";
                            document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML ="Data upload complete";
                        }
                       }
                }
                XMLHttpRequestObject.send(null);
            }    
          }
          catch(err)
          {
          } 
            
    }    

    </script>

</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div id="bodyDiv">
        <table width="100%" border="0" cellpadding="1" cellspacing="1">
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="formNameLabel" runat="server" Text="Disburse Info Upload" Font-Bold="true"
                        Font-Size="16px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 40%;">
                    <asp:Panel ID="PanelUpload" runat="server" Visible="true">
                        <asp:FileUpload ID="FileUpload" runat="server" />
                        <asp:Button ID="ButtonUploadFile" runat="server" Text="Upload File" OnClick="ButtonUploadFile_Click"  /><br />
                        <asp:Label ID="LabelUpload" runat="server" Text=""></asp:Label>
                    </asp:Panel>
                    <asp:Label ID="criteriaLable" runat="server" Text="Criteria :"></asp:Label>
                    <asp:DropDownList ID="criteriaDropDownList" runat="server" Width="15%">
                        <asp:ListItem Value="D">D</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 15%;">
                    <input id="uploadButton" type="button" value=" Upload " style="height: 25px;" onclick="ExcelFileLoad();" />
                </td>
                <td style="width: 20%;">
                    <asp:TextBox ID="dateTextBox" runat="server" Text="" ReadOnly="true"></asp:TextBox><input
                        id="dateButton" type="button" value="..." onclick="return showCalendar('ctl00_ContentPlaceHolder_dateTextBox', '%d-%m-%Y');" />
                </td>
                <td style="width: 15%;">
                    <input id="showDataButton" type="button" value="Show data" style="height: 25px;"
                        onclick="LoadUploadData()" />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div id="gridbox" style="background-color: white; width: 100%; height: 490px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4">
                    <div id="pleaseWaidDiv" class="pleaseWaidDivwith" style="display: none; position: absolute;
                        border: solid 1px gray;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
                            align="center">
                            <tr bgcolor="#B70000">
                                <td colspan="2">
                                    <asp:Label runat="server" ID="TotalRecordLabel" Text="Data upload" Font-Bold="true"
                                        Font-Size="14px" ForeColor="White"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="center">
                                    <img id="loadingImg" src="../Images/ajax-loader.gif" alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="waitLabel" Text="Please wait data uploading..." Font-Names="Times New Roman"
                                        Font-Bold="false" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="center">
                                    <input id="okButton" type="button" value="OK" onclick="PopUpWindowsClose();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div onmouseup="return false" class="translucent" onmousemove="return false" onmousedown="return false"
        id="blockUI" ondblclick="return false" style="display: none; z-index: 50000;
        left: 0px; position: absolute; top: 0px; background-color: gray" onclick="return false">
    </div>

    <script type="text/javascript" language="javascript">
        var mygrid;
	    mygrid = new dhtmlXGridObject('gridbox');
	    function doOnLoad()
	    {
	    mygrid.setImagePath("../codebase/imgs/");
	    mygrid.setHeader("SL,NAME,MASTER,FathersName,MothersName,DateofBirth,DeclinedReason,DeclinedDate,CompanyName,ADD1,ADD2,TELC,TELR,Mob1,Mob2,PRODUCT,BankBranch,Source,Status,Remarks,TIN,IDTYPE1,IDNO1,IDTYPE2,IDNO2,IDTYPE3,IDNO3,IDTYPE4,IDNO4,IDTYPE5,IDNO5,RESIDENTIALSTATUS,EDUCATIONLEVEL,MARITALSTATUS,LOANACCOUNTNO");
	    mygrid.setInitWidths("50,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
	    mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	    mygrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	    mygrid.setSkin("light");
	    mygrid.init();
	    mygrid.enableSmartRendering(true,100);
	    mygrid.clearAll();
        }

	    function LoadUploadData()
        {
        try
        {
                var qType=document.getElementById('ctl00_ContentPlaceHolder_criteriaDropDownList').value;
                var qDate=document.getElementById('ctl00_ContentPlaceHolder_dateTextBox').value;
                XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
                if(XMLHttpRequestObject) 
                {                         
                    XMLHttpRequestObject.open("POST", "DisburseLoanUploadUI.aspx?operation=LoadUploadData&SEARCHKEY="+qType+"&QUERYDATE="+qDate);      
                    //XMLHttpRequestObject.open("POST", "TestUploadResult.aspx?operation=LoadUploadData&SEARCHKEY="+qType+"&QUERYDATE="+qDate);      
                    document.getElementById('ctl00_ContentPlaceHolder_TotalRecordLabel').innerHTML ="Data load";
                    document.getElementById('loadingImg').style.display="block";
                    document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML ="Please wait data loading...";
                    LoadPopupWindows();
                    XMLHttpRequestObject.onreadystatechange = function ()
                    {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
                        {
                            var TypeData=new Array();
                            TypeData=XMLHttpRequestObject.responseText.split('#');
                            //alert(TypeData[0]);
                            mygrid.clearAll();
                            mygrid.parse(TypeData[0]);
                            //mygrid.loadXML("../UI/TestUploadResult.aspx");
                            document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML ="Please wait data loading...";
                            PopUpWindowsClose();                    
                        }
                    }            
                    XMLHttpRequestObject.send(null);
                }                  
              }
              catch(err)
              {}                
        }
    </script>

</asp:Content>
