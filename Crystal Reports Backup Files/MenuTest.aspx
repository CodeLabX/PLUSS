﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MenuTest.aspx.cs" Inherits="PlussAndLoan.UI.Deletable.MenuTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .parent {
            display: block;
            position: relative;
            float: left;
            line-height: 30px;
            background-color: #4FA0D8;
            border-right: #CCC 1px solid;
        }

            .parent a {
                margin: 10px;
                color: #FFFFFF;
                text-decoration: none;
            }

            .parent:hover > ul {
                display: block;
                position: absolute;
            }

        .child {
            display: none;
            z-index:10;
        }

            .child li {
                background-color: #E4EFF7;
                line-height: 30px;
                border-bottom: #CCC 1px solid;
                border-right: #CCC 1px solid;
                width: 100%;
            }

                .child li a {
                    color: #000000;
                }

        ul {
            list-style: none;
            margin: 0;
            padding: 0px;
            min-width: 10em;
        }

            ul ul ul {
                left: 100%;
                top: 0;
                margin-left: 1px;
            }

        li:hover {
            background-color: #95B4CA;
        }

        .parent li:hover {
            background-color: #F0F0F0;
        }

        .expand {
            font-size: 12px;
            float: right;
            margin-right: 5px;
        }
    </style>
</head>
<body>
    <nav id="NavItem" runat="server">
    </nav>
       <%--<ul id="menu">
            <li class="parent"><a href="#">Popular Toys</a>
                <ul class="child">
                    <li class="parent"><a href="#">Video Games <span class="expand">»</span></a>
                        <ul class="child">
                            <li><a href="#">Car</a></li>
                            <li><a href="#">Bike Race</a></li>
                            <li><a href="#">Fishing</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Barbies</a></li>
                    <li><a href="#">Teddy Bear</a></li>
                    <li><a href="#">Golf Set</a></li>
                </ul>
            </li>
        </ul>--%>
    <form id="form1" runat="server">
    </form>
</body>
</html>
