﻿<%@ Page Title="Loan Locator Home" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.LoanApplication.UI_LocLoanHome" CodeBehind="LocLoanHome.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Loan Locator Query/View</div>
        <table width="100%" border="0">
            <tr align="center" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="#bce3ff">
                <td colspan="7"><b>Search By </b></td>
            </tr>

            <tr align="center" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="#bce3ff">
                <td style="width: 11%">
                    <b>Loan ID:</b><asp:TextBox CssClass="input-field" runat="server" ID="findById" Width="80px"></asp:TextBox>
                </td>
                <td>
                    <b>ARM Code:</b><asp:TextBox CssClass="input-field" MaxLength="3" runat="server" ID="txtARMCode" Width="80px"></asp:TextBox>
                </td>
                <td><b>Product:</b><asp:DropDownList CssClass="select-field" runat="server" ID="findByProduct" Width="150px" /></td>
                <td><b>A/C No.:</b><asp:TextBox CssClass="input-field" runat="server" ID="findAcBy" Width="120px"></asp:TextBox></td>
                <td><b>Customer:</b><asp:TextBox CssClass="input-field" runat="server" ID="findByCustomer"></asp:TextBox></td>
                <td><b>Status:</b>
                <asp:DropDownList CssClass="select-field" runat="server" ID="sel_loanStatus" Width="150px" />
                </td>
                <td>
                    <asp:Button CssClass="btn primarybtn" runat="server" ID="btnSearch" Text="GO" OnClick="btnSearch_Click" />
                    <asp:Button CssClass="btn clearbtn" runat="server" ID="refresh" Text="Refresh" OnClick="refresh_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="7"> <br />
                    <asp:Label runat="server" ID="totalLoan" Font-Bold="True">Total 0 Loan Applications</asp:Label> <br /></td>
            
            </tr>
        </table>
        <asp:GridView Width="100%"
            ID="gvData" runat="server" CssClass="myGridStyle" OnPageIndexChanging="gvData_PageIndexChanging"
            AllowPaging="True" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" EnableModelValidation="True" AutoGenerateColumns="False" CellSpacing="1" GridLines="None" PageSize="25">
            <Columns>
                <asp:TemplateField HeaderText="APPL. ID">
                    <ItemTemplate>
                        <a href='/PLUSS/UI/LoanApplication/loc_EditLoan.aspx?id=<%#Eval("LOAN_APPL_ID")%>' style="color: black; text-decoration: underline"><%#Eval("LOAN_APPL_ID")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PROD_NAME" HeaderText="Product Name" ItemStyle-HorizontalAlign="Center">
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="APPL_DATE" HeaderText="Application Date" DataFormatString="{0:yyyy-MM-dd}">
                    <ControlStyle Width="50px" />
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="RECE_DATE" HeaderText="Submission Date" DataFormatString="{0:yyyy-MM-dd}">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="ACC_NUMB" HeaderText="A/C NO.">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="ACC_TYPE_NAME" HeaderText="Type">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="LOAN_APP_AMOU" HeaderText="Amount (Applied)">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="ARM_CODE" HeaderText="ARM Code">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="LOAN_STATUS_NAME" HeaderText="State">
                    <HeaderStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="PROD_ID" HeaderText="ProductId" Visible="False" />
                <asp:BoundField DataField="LOAN_DECI_STATUS_ID" HeaderText="DecisionStatusId" Visible="False" />
                <%-- <asp:BoundField DataField="SCSO_ID" HeaderText="SourceId" Visible="False" />--%>
                <asp:BoundField DataField="LOAN_STATUS_ID" HeaderText="LoanStatusId" Visible="False" />
                <%--  <asp:BoundField DataField="DELI_DEPT_ID" HeaderText="DeliDeptId" Visible="False" />--%>
                <asp:BoundField DataField="DeptType" HeaderText="DeptType" Visible="False" />
                <asp:BoundField DataField="PERS_ID" HeaderText="PersonId" Visible="False" />
                <asp:BoundField DataField="MAKER" HeaderText="Maker" Visible="False" />

            </Columns>

            <EmptyDataTemplate>
                No loan application found
            </EmptyDataTemplate>
            <%--<AlternatingRowStyle CssClass="odd" />
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
            <PagerSettings PageButtonCount="4" />
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle ForeColor="Black" BackColor="#DEDFDE" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />--%>
        </asp:GridView>
    </div>
</asp:Content>

