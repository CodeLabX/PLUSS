<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_BankStvPersonUI" Title="Bank Statement Verification Person Entry" Codebehind="BankStvPersonUI.aspx.cs" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="content" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <div>
        <table width="100%" border="0">
            <tr>
                <td colspan="4" align="center">
                    <asp:Label ID="Label1" runat="server" Text="Bank Statement Verification" Font-Bold="true"
                        Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="2">
                    &nbsp;
                </td>
                <td>
                    &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" Text="Bank name: " Width="77px"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="bankNameDropDownList" runat="server" Width="170px"
                        DataTextField="Name" DataValueField="Id" OnSelectedIndexChanged="bankNameDropDownList_SelectedIndexChanged"
                        OnTextChanged="bankNameDropDownList_TextChanged" CausesValidation="True" 
                        TabIndex="2">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
<%--            <tr>
                <td align="right">
                    <asp:Label ID="Label3" runat="server" Text="Branch:"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="branchDropDownList" runat="server" Width="170px" DataTextField="Name"
                        DataValueField="Id" TabIndex="3" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>--%>
            <tr>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Text="Name:"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="nameTextBox" runat="server" Width="300px" TabIndex="8"></asp:TextBox>
                </td>
                <td>
                    e.g. abcd,bcde
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label5" runat="server" Text="Email Address:"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="emailTextBox" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td>
                    e.g. abcd@abc.com,bcde@bcd.com
                </td>
            </tr>
            <tr style="height: 30px">
                <td align="right">
                    <asp:Label ID="Label11" runat="server" Text="Status:"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="statusDropDownList" runat="server" Width="100px" TabIndex="6">
                        <asp:ListItem>Inactive</asp:ListItem>
                        <asp:ListItem Selected="True">Active</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="height: 35px" align="center">
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:Button ID="addButton" runat="server" Width="100px" OnClick="addButton_Click"
                        Text="Add" UseSubmitBehavior="False" Height="26px" />
                </td>
                <td align="left">
                    <asp:Button ID="clearButton" runat="server" Width="100px" Text="Clear" OnClick="clearButton_Click"
                        TabIndex="7" UseSubmitBehavior="False" Height="26px" />
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
            <tr align="center">
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding-left: 30px" colspan="3">
                    <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                    &nbsp
                    <asp:Button ID="searchButton" runat="server" Text="Search" Width="65px" OnClick="searchButton_Click"
                        TabIndex="1" Height="20px" />
                </td>
                <td>
                    &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div id="gridbox" style="background-color: white; width: 785px; height: 275px;"></div>
                </td>
            </tr>
        </table>
         <div style="display: none" runat="server" id="dvXml"></div>
    </div>
    <script type="text/javascript">
	    var mygrid;
	    mygrid = new dhtmlXGridObject('gridbox');
	    mygrid.setImagePath("../codebase/imgs/");	    
	    mygrid.setHeader("SL,BankId,Band name,Verification Person Name,Email address,Status");
	    mygrid.setInitWidths("40,0,165,200,300,60");
	    mygrid.setColAlign("right,left,left,left,left,left");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,ro");
	    mygrid.setColSorting("int,int,str,str,str,str");
	    mygrid.setSkin("light");
	    mygrid.init();
	    mygrid.enableSmartRendering(true,20);
	    mygrid.parse(document.getElementById("xml_data"));
	    mygrid.attachEvent("onRowSelect",DoOnRowSelected);
        function DoOnRowSelected()
        {
            if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
            {
              document.getElementById('ctl00_ContentPlaceHolder_addButton').value="Update";
              document.getElementById('ctl00_ContentPlaceHolder_bankNameDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(),1).getValue());
              document.getElementById('ctl00_ContentPlaceHolder_nameTextBox').value = mygrid.cells(mygrid.getSelectedId(),3).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_emailTextBox').value=mygrid.cells(mygrid.getSelectedId(),4).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_statusDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
             }
             else
             {
              document.getElementById('ctl00_ContentPlaceHolder_addButton').value="Add";
             }
        }
    </script>
</asp:Content>

