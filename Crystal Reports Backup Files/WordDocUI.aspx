﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_WordDocUI" Title="Word Doc" Codebehind="WordDocUI.aspx.cs" %>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <div style="padding-left:15px">
    <table class="style2">
        <tr>
            <td align="center" colspan="4" style="font-weight:bold">
                Letter Document</td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight:bold">
                <asp:Label ID="toLabel" runat="server" Text="To :"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="toTextBox" runat="server" Width="200px"></asp:TextBox>
                <asp:HiddenField ID="idHiddenField" runat="server" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" valign="top" style="font-weight:bold">
                Subject :&nbsp;<asp:DropDownList ID="subjectDropDownList" runat="server" Width="450px"></asp:DropDownList>
                <asp:Label ID="Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight:bold">
                Dear Sir,</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" width="100%">
                <asp:Label ID="starLable" runat="server" Text="*"></asp:Label>
                <asp:TextBox ID="bodyPartTextBox" runat="server" Width="20%"></asp:TextBox>
                <asp:Label ID="hashLabel" runat="server" Text="#"></asp:Label>
                <asp:TextBox ID="bodyTextBox" runat="server" Width="56%"></asp:TextBox>
                <asp:Label ID="comments" runat="server" Text="*=Customer Name," Font-Size="8px" ></asp:Label>
                <asp:Label ID="comments2" runat="server" Text=" #=Address" Font-Size="8px" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:TextBox ID="body1TextBox" runat="server" Rows="2" TextMode="MultiLine" 
                    Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="body2TextBox" runat="server" Width="82%"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="uderline" style="padding-left:50px;font-weight:bold;">
                Account Name</td>
            <td class="uderline" style="padding-left:200px;font-weight:bold;">
               Account Number</td>
            <td>
                </td>
            <td>
                </td>
        </tr>
        <tr>
            <td style="padding-left:50px">
                ........................</td>
            <td style="padding-left:200px">
                ...........................</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:TextBox ID="body3TextBox" runat="server" Columns="3" TextMode="MultiLine" 
                    Width="80%" Rows="2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:TextBox ID="body4TextBox" runat="server" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="body5TextBox" runat="server" Width="80%"></asp:TextBox>
                <br />
                <asp:TextBox ID="body6TextBox" runat="server" Rows="1" TextMode="MultiLine" 
                    Width="270px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                ...................................................</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                ..................................................., <b>Personal Loans</b></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Consumer Banking.</b></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center" valign="top">
                <asp:Button ID="updateButton" runat="server" Text="Update" Width="100px" 
                    Height="25px" onclick="updateButton_Click" />
                <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" 
                    Height="25px" onclick="clearButton_Click" />
                </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
                <td colspan="4" style="padding-left:0px">
                    <div id="gridbox"  style="background-color:white; width:785px; height:140px;"></div>
                     <div style="display: none" runat="server" id="docDiv"></div>
                </td>
        </tr>
    </table>
    </div>
    <script type="text/javascript">
	var mygrid;
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath("../codebase/imgs/");
	mygrid.setHeader("SL.No.,To,subjectid,Subject,Body,Body1,Body2,Body3,Body4,Body5,Body6");
	mygrid.setInitWidths("45,160,0,210,350,0,0,0,0,0,0");
//	mygrid.setColAlign("right,left,int,left,left,left,left,left,left,left,left");
//	mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
//	mygrid.setColSorting("int,str,int,str,str,str,str,str,str,str,str");
	//mygrid.setSkin("modern");
	mygrid.setSkin("light");	 
	mygrid.init();
	mygrid.enableSmartRendering(true,5);
        //mygrid.loadXML("../includes/WordDocList.xml");
	mygrid.parse(document.getElementById("xml_data"));
	mygrid.attachEvent("onRowSelect",doOnRowSelected);
    function doOnRowSelected(id)
    {
        var bodPart;
        if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
        {
          document.getElementById('ctl00_ContentPlaceHolder_updateButton').value="Update";
          document.getElementById('ctl00_ContentPlaceHolder_idHiddenField').value = mygrid.cells(mygrid.getSelectedId(),0).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_toTextBox').value = mygrid.cells(mygrid.getSelectedId(),1).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_subjectDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(),2).getValue());
          bodPart=mygrid.cells(mygrid.getSelectedId(),4).getValue().split("#");
          document.getElementById('ctl00_ContentPlaceHolder_bodyPartTextBox').value=bodPart[0].replace("*",'');
          document.getElementById('ctl00_ContentPlaceHolder_bodyTextBox').value=bodPart[1];//mygrid.cells(mygrid.getSelectedId(),4).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_body1TextBox').value=mygrid.cells(mygrid.getSelectedId(),5).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_body2TextBox').value=mygrid.cells(mygrid.getSelectedId(),6).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_body3TextBox').value=mygrid.cells(mygrid.getSelectedId(),7).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_body4TextBox').value=mygrid.cells(mygrid.getSelectedId(),8).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_body5TextBox').value=mygrid.cells(mygrid.getSelectedId(),9).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_body6TextBox').value=mygrid.cells(mygrid.getSelectedId(),10).getValue();
         }
         else
         {
          document.getElementById('ctl00_ContentPlaceHolder_updateButton').value="Save";
         }
    }
	</script>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">

    <style type="text/css">
        .style2
        {
            width: 100%;
        }
        </style>
</asp:Content>

