﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserControls.css.UI_UserControls_css_admincss" Codebehind="admincss.ascx.cs" %>
<style type="text/css">
    body.Admin #Content ul {
        xbackground: #eea url(../image/bgAdminMenu.png) no-repeat;
        width: 154px;
        margin-top: 20px;
        xheight: 178px;
    }

        body.Admin #Content ul li {
            margin: 0 0 12px;
            xheight: 24px;
            background: #BA404E;
            line-height: 24px;
        }

            body.Admin #Content ul li a {
                color: #fff;
                display: block;
                text-align: center;
            }


    table.summary th {
        color: #ffffff;
        font-weight: bold;
        background-color: #5D5454;
    }

    table.summary th, table.summary td {
        padding: 3px 2px;
    }

    table.summary tr.odd {
        background-color: #efedee;
    }

    table.summary tr {
        xheight: 25px;
    }

        table.summary tr td.empty {
            color: #999;
        }

    .calendar {
        z-index: 1500;
    }

    /**** <form> **/
    div.modal {
    }

        div.modal label {
            display: inline-block;
            width: 150px;
            margin: 2px 2px 2px 5px;
            padding: 2px;
            font-weight: bold;
            color: #0680CB;
        }

            div.modal label.lblquaryTracker {
                width: auto;
            }

        div.modal input.txt,
        div.modal textarea.txt,
        div.modal select.txt {
            display: inline-block;
            border: 1px solid #CCD76E;
            width: 230px;
            margin: 2px;
            padding: 2px;
            xfont-weight: bold;
        }

        div.modal select.txt {
            width: 235px;
        }

        div.modal textarea.txt {
            overflow: auto;
            height: 50px;
        }

        div.modal div.btns {
            margin-top: 20px;
            text-align: right;
            padding: 6px 28px;
            border-top: 1px solid #CCD76E;
        }

            div.modal div.btns a {
                xmargin: 0 8px;
                xcolor: #b8404a;
            }
    /**** </form> **/


    /********icon ****/
    .icon {
        display: inline-block;
        width: 16px;
        height: 16px;
        margin: 1px;
        background: url() no-repeat 50%;
    }

    .iconDoc {
        background-image: url(../image/clear-folder--pencil.png);
    }

    .iconDocDel {
        background-image: url(../image/clear-folders--minus.png);
    }

    .iconAdd {
        background-image: url(../image/plus-circle-frame.png);
    }

    .iconEdit {
        background-image: url(../image/pencil--arrow.png);
    }

    .iconReplay {
        background-image: url(../Images/Replay.png);
    }

    .iconDelete {
        background-image: url(../image/cross.png);
    }

    .iconCalendar {
        background-image: url(../image/calendar-select.png);
    }

    .iconContact {
        background-image: url(../image/user-business.png);
    }

    .iconContract {
        background-image: url(../image/users.png);
    }

    .iconlink {
        display: inline-block;
        line-height: 16px;
        padding-left: 18px;
        margin: 2px 5px 2px 0;
        background: url() no-repeat;
        color: #ffffff;
        font-weight: bold;
    }

    .iconlinkBlack {
        display: inline-block;
        line-height: 16px;
        padding-left: 18px;
        margin: 2px 5px 2px 0;
        background: url() no-repeat;
        color: black;
        font-weight: bold;
    }

    .iconlinkAdd {
        background-image: url(../image/plus-circle-frame.png);
        cposition: absolute;
        ctop: 30px;
        cright: 48px;
    }

    .iconlinkAddNew {
        background-image: url(../image/plus-circle-frame.png);
        cposition: absolute;
        ctop: 30px;
        cright: 48px;
        margin-right: 0;
    }

    .iconlinkSave {
        background-image: url(../image/disk-black.png);
        color: #333333;
    }

    .iconlinkClose {
        background-image: url(../image/cross-octagon-frame.png);
        color: #333333;
    }

    .iconlinkforward {
        background-image: url(../image/forward.png);
        color: #333333;
    }

    .iconlinkDefer {
        background-image: url(../image/Defer.png);
        color: #333333;
    }

    .iconSearchGrid {
        background-image: url(../Image/index.gif);
        color: #333333;
    }

    .iconlinkBackword {
        background-image: url(../image/Backward.png);
        color: #333333;
    }

    .iconlinkReject {
        background-image: url(../image/Reject.png);
        color: #333333;
    }

    .iconlinkDecline {
        background-image: url(../image/Decline.png);
        color: #333333;
    }

    .iconlinkDocumentCheckList {
        background-image: url(../image/DocumentCheckList.png);
        color: #333333;
    }

    .iconlinkCloseLoanCheckList {
        background-image: url(../image/LoanCheckList.png);
        color: #333333;
    }

    .iconlinkCloseBasel {
        background-image: url(../image/Basel.png);
        color: #333333;
    }

    .iconlinkCloseLLI {
        background-image: url(../image/LLI.png);
        color: #333333;
    }

    .iconlinkCloseOfferLeter {
        background-image: url(../image/OfferLeter.png);
        color: #333333;
    }

    .iconlinkCloseBookLet {
        background-image: url(../image/book.png);
        color: #333333;
    }

    /******** </icon> ****/

    /********* manage user *********/
    #userEntry {
        height: 396px;
    }

    #lstClients {
        height: 130px;
        overflow: auto;
        padding: 5px 10px;
        margin-right: 35px;
    }

        #lstClients input {
            xwidth: 80%;
            margin: 2px;
        }

        #lstClients label {
            width: 85%;
            color: #50A0CB;
        }
    /********* </manage user> *********/

    /********* manage client *********/
    #clientEntry {
        height: 146px;
    }
    /********* </manage client> *********/

    /********* manage site *********/
    #siteEntry {
        height: 310px;
    }
    /********* </manage site> *********/

    /********* manage contractor *********/
    #contractorEntry {
        height: 440px;
        *height: 450px;
        z-index: 2000;
    }

        #contractorEntry h3.disposal {
            margin-top: 8px;
        }

    #contractorUploader {
        height: 180px;
    }

    .alink {
        color: Blue;
    }
    /********* </manage contractor> *********/


    /*Search Button*/
    .search {
        width: 400px;
        height: 20px;
        padding-bottom: 10px;
        margin-top: -22px;
    }

    .searchwithUpload {
        width: 680px;
    }

    .txtSearch {
        border: 1px solid #999999;
        font-size: 12px;
        height: 19px;
        padding: 0;
        text-align: center;
        width: 300px;
    }

    .txtUpload {
        width: 185px;
        height: 22px;
        margin-left: 20px;
    }

    .uploaderForm {
        width: 600px;
        height: 20px;
    }
</style>
