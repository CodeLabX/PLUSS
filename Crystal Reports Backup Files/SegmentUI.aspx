﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SegmentUI" Title="Segment" CodeBehind="SegmentUI.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div style="padding-left: 100px;">
        <table>
            <tr>
                <td colspan="4" align="center">
                    <asp:Label ID="formNameLabel" runat="server" Text="Segment Info" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="segmentNameLabel" runat="server" Text="Segment :"></asp:Label></td>
                <td>
                    <asp:TextBox ID="segmentNameTextBox" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:HiddenField ID="segmentIdHiddenField" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="segmentCodeLabel" runat="server" Text="Segment Code:"></asp:Label></td>
                <td>
                    <asp:TextBox ID="segmentCodeTextBox" runat="server"></asp:TextBox></td>
                <td>Description: </td>
                <td>
                    <asp:TextBox ID="segmentDescriptionTextBox" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="professionLabel" runat="server" Text="Profession :"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="professionDropDownList" runat="server" Width="150px"></asp:DropDownList></td>
                <td>Minimum Income:</td>
                <td>
                    <asp:TextBox ID="miniMumIncomeTextBox" runat="server" MaxLength="10"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="maxLoanAmountLabel" runat="server" Text="Max Loan :"></asp:Label></td>
                <td>
                    <asp:TextBox ID="maxLoanTextBox" runat="server" MaxLength="10"></asp:TextBox></td>
                <td>
                    <asp:Label ID="minLoanAmountLabel" runat="server" Text="Min Loan :"></asp:Label></td>
                <td>
                    <asp:TextBox ID="minLoanTextBox" runat="server" MaxLength="10"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">
                    <div style="border: solid 1px gray; width: 100%">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label18" runat="server" Text="Interest Rate:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="IRTextBox" runat="server" Width="60px" MaxLength="5"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label19" runat="server" Text="Tenor:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="tenorTextBox" runat="server" Width="60px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label20" runat="server" Text="Frist Pay:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="firstPayTextBox" runat="server" Width="60px" MaxLength="2"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label21" runat="server" Text="Multiplier:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="multiplierTextBox" runat="server" Width="60px" MaxLength="2"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label16" runat="server" Text="Min Age:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="minAgeTextBox" runat="server" Width="60px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label17" runat="server" Text="Max Age:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="maxAgeTextBox" runat="server" Width="60px" MaxLength="3"></asp:TextBox>
                                    <asp:TextBox ID="maxAgeRemarksTextBox" runat="server" Width="100px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <div style="border: solid 1px gray; width: 100%">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="DBR1Label" runat="server" Text="DBR1"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="DBR1TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="DBR2Label" runat="server" Text="DBR2"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="DBR2TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="DBR3Label" runat="server" Text="DBR3"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="DBR3TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="DBR4Label" runat="server" Text="DBR4"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="DBR4TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text="L2DBR1"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2DBR1TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text="L2DBR2"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2DBR2TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="L2DBR3"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2DBR3TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="L2DBR4"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2DBR4TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="L1TopDBR1"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L1DBR1TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="L1TopDBR2"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L1DBR2TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="L1TopDBR3"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L1DBR3TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="L1TopDBR4"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L1DBR4TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label9" runat="server" Text="L2TopDBR1"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2TopDBR1TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label10" runat="server" Text="L2TopDBR2"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2TopDBR2TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label11" runat="server" Text="L2TopDBR3"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2TopDBR3TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label12" runat="server" Text="L2TopDBR4"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="L2TopDBR4TextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label13" runat="server" Text="MNCMUE"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="MNCTextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label14" runat="server" Text="LARMUE"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="LARDBRTextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label15" runat="server" Text="LOCMUE"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="LOCDBRTextBox" runat="server" Width="50px" MaxLength="3"></asp:TextBox></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div style="border: solid 1px gray; width: 100%">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label22" runat="server" Text="Experience:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="experienceTextBox" runat="server" Width="100px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label26" runat="server" Text="Criteria Code:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="criteriaCodeTextBox" runat="server" Width="100px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label27" runat="server" Text="Asset Code:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="assetCodeTextBox" runat="server" Width="100px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label25" runat="server" Text="A/C Rel:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="acRelTextBox" runat="server" Width="100px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label23" runat="server" Text="Physical Verification:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="physicalVarificationTextBox" runat="server" Width="100px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label24" runat="server" Text="Gurantee/Reference:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="guranteeReferanceTextBox" runat="server" Width="100px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label28" runat="server" Text="Telephone:"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="telephoneTextBox" runat="server" Width="100px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label29" runat="server" Text="MNCDBR" Visible="False"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="TextBox8" runat="server" Width="100px" Visible="False"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label30" runat="server" Text="MNCDBR" Visible="False"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="TextBox9" runat="server" Width="100px" Visible="False"></asp:TextBox></td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <asp:Button ID="saveButton" runat="server" Text="Save"
                        Width="100px" Height="27px" OnClick="saveButton_Click" />&nbsp;
                <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px"
                    Height="27px" OnClick="clearButton_Click" /></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        
    </div>
    <div id="gridbox" style="background-color: white; width: 805px; height: 150px;"></div>
     <div style="display: none" runat="server" id="dvXml"></div>
    <script type="text/javascript">
        var mygrid;
        mygrid = new dhtmlXGridObject('gridbox');
        mygrid.setImagePath("../codebase/imgs/");
        mygrid.setHeader("SL.,Segment,Code,Id,Profession,Description,MaxLoan,MinLoan,IR,Tenor,FirstPay,Multiplier,MaxAge,MinAge,DBR1,DBR2,DBR3,DBR4,L1DBR1,L1DBR2,L1DBR3,L1DBR4,L2DBR1,L2DBR2,L2DBR3,L2DBR4,L2TopDBR1,L2TopDBR2,L2TopDBR3,L2TopDBR4,MNCDBR,LARDBR,LOCDBR,MiniIcome,Exp,CriteriaCode,AssetCode,A/CRel,PhysicalVeri,Guran,Telephon,MaxAgedescription");
        mygrid.setInitWidths("25,60,50,0,70,100,50,50,40,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50");
        mygrid.setColAlign("right,right,right,right,left,left,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,left,left,left,left,left,left,left,left");
        mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
        mygrid.setSkin("light");
        mygrid.init();
        mygrid.enableSmartRendering(true, 20);
        mygrid.parse(document.getElementById("xml_data"));
        mygrid.attachEvent("onRowSelect", DoOnRowSelected);
        function DoOnRowSelected() {
            if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {
                document.getElementById('ctl00_ContentPlaceHolder_saveButton').value = "Update";
                document.getElementById('ctl00_ContentPlaceHolder_segmentIdHiddenField').value = mygrid.cells(mygrid.getSelectedId(), 0).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_segmentNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 1).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_segmentCodeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_professionDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 3).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_segmentDescriptionTextBox').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_maxLoanTextBox').value = mygrid.cells(mygrid.getSelectedId(), 6).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_minLoanTextBox').value = mygrid.cells(mygrid.getSelectedId(), 7).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_IRTextBox').value = mygrid.cells(mygrid.getSelectedId(), 8).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_tenorTextBox').value = mygrid.cells(mygrid.getSelectedId(), 9).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_firstPayTextBox').value = mygrid.cells(mygrid.getSelectedId(), 10).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_multiplierTextBox').value = mygrid.cells(mygrid.getSelectedId(), 11).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_minAgeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 12).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_maxAgeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 13).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_DBR1TextBox').value = mygrid.cells(mygrid.getSelectedId(), 14).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_DBR2TextBox').value = mygrid.cells(mygrid.getSelectedId(), 15).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_DBR3TextBox').value = mygrid.cells(mygrid.getSelectedId(), 16).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_DBR4TextBox').value = mygrid.cells(mygrid.getSelectedId(), 17).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L1DBR1TextBox').value = mygrid.cells(mygrid.getSelectedId(), 18).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L1DBR2TextBox').value = mygrid.cells(mygrid.getSelectedId(), 19).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L1DBR3TextBox').value = mygrid.cells(mygrid.getSelectedId(), 20).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L1DBR4TextBox').value = mygrid.cells(mygrid.getSelectedId(), 21).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2DBR1TextBox').value = mygrid.cells(mygrid.getSelectedId(), 22).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2DBR2TextBox').value = mygrid.cells(mygrid.getSelectedId(), 23).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2DBR3TextBox').value = mygrid.cells(mygrid.getSelectedId(), 24).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2DBR4TextBox').value = mygrid.cells(mygrid.getSelectedId(), 25).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2TopDBR1TextBox').value = mygrid.cells(mygrid.getSelectedId(), 26).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2TopDBR2TextBox').value = mygrid.cells(mygrid.getSelectedId(), 27).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2TopDBR3TextBox').value = mygrid.cells(mygrid.getSelectedId(), 28).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_L2TopDBR4TextBox').value = mygrid.cells(mygrid.getSelectedId(), 29).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_MNCTextBox').value = mygrid.cells(mygrid.getSelectedId(), 30).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_LARDBRTextBox').value = mygrid.cells(mygrid.getSelectedId(), 31).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_LOCDBRTextBox').value = mygrid.cells(mygrid.getSelectedId(), 32).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_miniMumIncomeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 33).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_experienceTextBox').value = mygrid.cells(mygrid.getSelectedId(), 34).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_criteriaCodeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 35).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_assetCodeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 36).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_acRelTextBox').value = mygrid.cells(mygrid.getSelectedId(), 37).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_physicalVarificationTextBox').value = mygrid.cells(mygrid.getSelectedId(), 38).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_guranteeReferanceTextBox').value = mygrid.cells(mygrid.getSelectedId(), 39).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_telephoneTextBox').value = mygrid.cells(mygrid.getSelectedId(), 40).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_maxAgeRemarksTextBox').value = mygrid.cells(mygrid.getSelectedId(), 41).getValue();
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_saveButton').value = "Save";
            }
        }
    </script>
</asp:Content>

