﻿<%--<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_DedupResultPrint" Codebehind="DedupResultPrint.aspx.cs" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.Dedups.UI_DedupResultPrint" ValidateRequest="false" Codebehind="DedupResultPrint.aspx.cs" %>
<%@ Register Src="~/UI/UserControls/css/spreadsheet.ascx" TagPrefix="uc1" TagName="spreadsheet" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <uc1:spreadsheet runat="server" ID="spreadsheet" />
   
    <script type="text/javascript" >
        var gIsThaiYear=false;
        function IsValidDate(obj)
        {
    	    var indate = obj.value;
	        var sdate = indate.split("/");
	        var syear = Math.abs(sdate[2]);	
	        if(syear-(gIsThaiYear?543:0) < 1900)	return false;
	        var indateEN = (Math.abs(sdate[1]))+"/"+(Math.abs(sdate[0]))+"/"+(syear-(gIsThaiYear?543:0));
	        var chkDate = new Date(Date.parse(indateEN));
	        var cmpDate = (chkDate.getMonth()+1)+"/"+(chkDate.getDate())+"/"+(chkDate.getFullYear());	
	        if(indateEN != cmpDate || cmpDate == "NaN/NaN/NaN")
		        return false;
	        else
		        return true;	
        }
        function PrintReport()
        {
            window.print();
        }
        function PrintDedup()
        {
            var myContentToPrint = document.getElementById("dedupPrintdiv");
            var myWindowToPrint = window.open('','','width=800,height=1200,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>
    </head>
<body onload="PrintReport();" style="margin-top:0; margin-bottom:0; margin-left:0; margin-right:0;">
    <form id="form1" runat="server">
    <div id="dedupPrintdiv">
    <center>
    <table width="95%" border="0" cellpadding="1" cellspacing="1"><tr><td>
    <div>
    <table width="95%" border="0" cellpadding="1" cellspacing="1"><tr><td align="right">
        <%--<input id="printButton" type="button" value="Print" onclick="PrintReport()" />--%></td></tr></table>
    <table width="95%" border="0" cellpadding="1" cellspacing="1">
        <tr>
        <td><div style="padding-left:0px;">
        <fieldset >
        <legend>Search Parameter</legend>
            <table width="95%" border="0" cellpadding="1" cellspacing="1" style="text-align:left;">
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" runat="server" Text="Loan Locator Id :"></asp:Label>
                    </td>
                    <td style="width:220px"><asp:Label ID="loanLocatorIdLabel" runat="server" Text=""></asp:Label></td>
                    <td align="right" style="width:100px">
                        &nbsp;</td>
                    <td style="width:200px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label5" runat="server" Text="Applicant Name :"></asp:Label>
                    </td>
                    <td><asp:Label ID="applicantNameLabel" runat="server" Text=""></asp:Label></td>
                    <td align="right">
                        <asp:Label ID="Label4" runat="server" Text="Date of Birth :"></asp:Label>
                    </td>
                    <td><asp:Label ID="dateOfBirthLabel" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label7" runat="server" Text="Father's Name :"></asp:Label>
                    </td>
                    <td><asp:Label ID="fathersNameLabel" runat="server" Text=""></asp:Label></td>
                    <td align="right">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="Label21" runat="server" Text="Mother's Name :"></asp:Label>
                    </td>
                    <td><asp:Label ID="mothresNameLabel" runat="server" Text=""></asp:Label></td>
                    <td align="right"  valign="top">
                        &nbsp;</td>
                    <td valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <span id="businessNameLabel0">Business Name :</span></td>
                    <td><asp:Label ID="businessNameLabel" runat="server" Text=""></asp:Label></td>
                    <td align="right" valign="top">
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="Label11" runat="server" Text="Contact No. :"></asp:Label>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                        <tr><td><asp:Label ID="contactNoLabel1" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="contactNoLabel2" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="contactNoLabel3" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="contactNoLabel4" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="contactNoLabel5" runat="server" Text=""></asp:Label></td></tr>
                        </table>
                    </td>
                    <td align="left" valign="top" colspan="2">
                        <table cellpadding="0" cellspacing="0">
                        <tr><td style="width:80px">ID Type</td><td style="width:120px">ID No.</td><td style="width:90px">Expiry Date</td></tr>
                        <tr><td><asp:Label ID="idTypeLabel1" runat="server" Text=""></asp:Label></td><td><asp:Label ID="idNoLabel1" runat="server" Text=""></asp:Label></td><td><asp:Label ID="expiryDateLabel1" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="idTypeLabel2" runat="server" Text=""></asp:Label></td><td><asp:Label ID="idNoLabel2" runat="server" Text=""></asp:Label></td><td><asp:Label ID="expiryDateLabel2" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="idTypeLabel3" runat="server" Text=""></asp:Label></td><td><asp:Label ID="idNoLabel3" runat="server" Text=""></asp:Label></td><td><asp:Label ID="expiryDateLabel3" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="idTypeLabel4" runat="server" Text=""></asp:Label></td><td><asp:Label ID="idNoLabel4" runat="server" Text=""></asp:Label></td><td><asp:Label ID="expiryDateLabel4" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="idTypeLabel5" runat="server" Text=""></asp:Label></td><td><asp:Label ID="idNoLabel5" runat="server" Text=""></asp:Label></td><td><asp:Label ID="expiryDateLabel5" runat="server" Text=""></asp:Label></td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="Label15" Text="A/C No. for SCB :" runat="server"></asp:Label>
                    </td>
                    <td>                    
                        <table>
                        <tr><td><asp:Label ID="scbAccNoLabel1" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="scbAccNoLabel2" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="scbAccNoLabel3" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="scbAccNoLabel4" runat="server" Text=""></asp:Label></td></tr>
                        <tr><td><asp:Label ID="scbAccNoLabel5" runat="server" Text=""></asp:Label></td></tr>
                        </table>
                     </td>
                    <td align="right" valign="top">
                        <asp:Label ID="Label14" runat="server" Text="TIN No. :"></asp:Label>
                    </td>
                    <td valign="top"><asp:Label ID="tinNoLabel" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="Label16" runat="server" Text="A/C with Other Bank :"></asp:Label>
                    </td>
                    <td colspan="3">
                        <table cellpadding="0" cellspacing="0">
                        <tr>
                        <td style="width:150px">Bank</td>
                        <td style="width:180px">Branch</td>
                        <td style="width:100px">A/C No.</td>
                        <td style="width:80px">A/C Type</td>
                        <td style="width:80px">Status</td>
                        </tr>
                        <tr>
                        <td><asp:Label ID="bankLabel1" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="branchLabel1" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acNoLabel1" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acTypeLabel1" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="statusLabel1" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                        <td><asp:Label ID="bankLabel2" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="branchLabel2" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acNoLabel2" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acTypeLabel2" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="statusLabel2" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                        <td><asp:Label ID="bankLabel3" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="branchLabel3" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acNoLabel3" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acTypeLabel3" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="statusLabel3" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                        <td><asp:Label ID="bankLabel4" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="branchLabel4" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acNoLabel4" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acTypeLabel4" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="statusLabel4" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                        <td><asp:Label ID="bankLabel5" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="branchLabel5" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acNoLabel5" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="acTypeLabel5" runat="server" Text=""></asp:Label></td>
                        <td><asp:Label ID="statusLabel5" runat="server" Text=""></asp:Label></td>
                        </tr>
                        </table>             
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td align="right">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </div></td>
            </tr>
            <tr>
                <td>
                <div style="padding-left:0px;">
                <fieldset>
                <legend>Search Result</legend>
                <div>
                <asp:GridView ID="negativeListPersonGridView" runat="server" AutoGenerateColumns="False"
                CellPadding="4" Font-Size="11px" ForeColor="#333333"  PageSize="20"
                Width="100%">
                <RowStyle BackColor="#FFFFFF" BorderStyle="Solid" ForeColor="#333333" />
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="2%" HeaderText="MF" SortExpression="MF">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewMfLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="2%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="8%" HeaderText="LLID" SortExpression="LLID">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewLlidLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="8%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Name" SortExpression="Name">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewNameLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%" HeaderText="A/C No." SortExpression="ACNo">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewAcNoLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="6%" HeaderText="Status" SortExpression="DReason">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewStatusLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="6%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%" HeaderText="TIN NO." SortExpression="DDate">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewTinNoLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%" HeaderText="ID" SortExpression="NationalID">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewNationalIDLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Phone1" SortExpression="Mobile">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewMobileNoLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Phone2" SortExpression="OfficePhone">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewOfficePhoneLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="12%" HeaderText="DOB" SortExpression="DOB">
                        <ItemTemplate>
                            <asp:Label ID="negativeListPersonGridViewDobLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    &nbsp;&nbsp;No data found.
                </EmptyDataTemplate>
                <EmptyDataRowStyle Font-Names="Tahoma" Font-Size="11px" ForeColor="DarkRed" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle CssClass="ssHeader" />
                <EditRowStyle BackColor="#999999" />
                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                </asp:GridView>
                </div>
                </fieldset>
                </div>
                </td>
            </tr>
            <tr>
            <td>
            <div style="padding-left:0px;">
            <fieldset >
            <legend>Applicant Information</legend>
                <table style="text-align:left;" width="95%" border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td align="right"><asp:Label ID="LLIDLabel2" runat="server" Text="Loan Locator ID:"></asp:Label></td>
                        <td style="width:220px"><asp:TextBox ID="LLIDTextBox1" runat="server" ReadOnly="True" Height="12px"></asp:TextBox></td>
                        <td align="right" valign="top" colspan="2">
                        <asp:Label ID="totalRecordCountLabel" runat="server" Text="Total Search Record :"></asp:Label>
                        </td>
                        <td valign="top">
                        <asp:TextBox ID="totalRecordCount" runat="server" ReadOnly="true" Width="90px" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="applicantNameLabel2" runat="server" Text="Applicant Name :" Height="12px"></asp:Label>
                        </td>
                        <td style="width:220px">
                            <asp:TextBox ID="nameTextBox" runat="server" Width="220px" ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                        <td align="right" style="width:150px" colspan="2">
                            <asp:Label ID="dateOfBirthLabel2" runat="server" Text="Date of Birth :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="dobTextBox" runat="server" ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="fatherNameLabel2" runat="server" Text="Father's Name :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="fatherNameTextBox" runat="server" Width="220px" 
                                ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Label ID="motherNameLabel" runat="server" Text="Mother's Name :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="motherNameTextBox" runat="server" Width="220px" 
                                ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="educationalLevelLabel" runat="server" Text="Educational Level :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="educationalTextBox" runat="server" Width="220px" 
                                ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Label ID="maritalStatusLabel" runat="server" Text="Marital Status :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="maritalStatusTextBox" runat="server" Width="200px" 
                                ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <asp:Label ID="residentialAddressLabel" runat="server" Text="Residential Address :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ResidentialAddressTextBox" runat="server" 
                                TextMode="MultiLine" Width="220px" ReadOnly="True" Height="30px"></asp:TextBox>
                        </td>
                        <td align="right" colspan="2"  valign="top">
                            <asp:Label ID="residentailStatusLabel" runat="server" Text="Residentail Status :"></asp:Label>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="residentialStatusTextBox" runat="server" ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <asp:Label ID="contactNoLabel" runat="server" Text="Contact No. :"></asp:Label>
                        </td>
                        <td>
                        <asp:TextBox ID="conractNoTextBox1" runat="server" Width="150px" Height="12px" ></asp:TextBox>
                        <br />
                        <asp:TextBox ID="conractNoTextBox2" runat="server" Width="150px" Height="12px" ></asp:TextBox>
                        <br />
                        <asp:TextBox ID="conractNoTextBox3" runat="server" Width="150px" Height="12px" ></asp:TextBox>
                        <br />
                        <asp:TextBox ID="conractNoTextBox4" runat="server" Width="150px" Height="12px" ></asp:TextBox>
                        <br />
                        <asp:TextBox ID="conractNoTextBox5" runat="server" Width="150px" Height="12px" ></asp:TextBox>
                        </td>
                        <td valign="top" colspan="3">
                        <asp:Label ID="idLabel" runat="server" Text="Id :"></asp:Label>
                        <br />
                        <asp:TextBox ID="idTypeTextBox1" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idNoTextBox1" runat="server" Width="120px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idExpiryDateTextBox1" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="idTypeTextBox2" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idNoTextBox2" runat="server" Width="120px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idExpiryDateTextBox2" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="idTypeTextBox3" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idNoTextBox3" runat="server" Width="120px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idExpiryDateTextBox3" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="idTypeTextBox4" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idNoTextBox4" runat="server" Width="120px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idExpiryDateTextBox4" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="idTypeTextBox5" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idNoTextBox5" runat="server" Width="120px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="idExpiryDateTextBox5" runat="server" Width="80px" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <asp:Label ID="tinNoLabel2" runat="server" Text="TIN No. :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="tinNoTextBox" runat="server" ReadOnly="True" Width="200px" Height="12px"></asp:TextBox>
                        </td>
                        <td align="right" valign="top" colspan="2">
                            <asp:Label ID="statusLabel" runat="server" Text="Status :"></asp:Label></td>
                        <td valign="top">
                            <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="True" Height="12px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <asp:Label ID="acNoSCBLabel" Text="A/C No. for SCB :" runat="server"></asp:Label>
                        </td>
                        <td colspan="2">
                        <asp:TextBox ID="preTextBox1" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="scbACNoTextBox1" runat="server" Width="100px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="postTextBox1" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="preTextBox2" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="scbACNoTextBox2" runat="server" Width="100px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="postTextBox2" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="preTextBox3" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="scbACNoTextBox3" runat="server" Width="100px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="postTextBox3" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="preTextBox4" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="scbACNoTextBox4" runat="server" Width="100px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="postTextBox4" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="preTextBox5" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="scbACNoTextBox5" runat="server" Width="100px" Height="12px"></asp:TextBox>
                        <asp:TextBox ID="postTextBox5" runat="server" Width="40px" Height="12px"></asp:TextBox>
                        <br />
                        </td>
                        <td valign="top">
                        <asp:Label ID="creditCardNoLabel" runat="server" Text="Credit Card No :"></asp:Label>
                        </td>
                        <td valign="top">
                        <asp:TextBox ID="creditCardNoTextBox" runat="server"></asp:TextBox>
                        <br />
                        <br />
                        <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="nameOfCompanyLabel" runat="server" Text="Name of Company :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="companyNameTextBox" runat="server" Width="220px" 
                                ReadOnly="True" Height="12px"></asp:TextBox>
                        </td>
                        <td align="right" colspan="2"><asp:Label ID="declineResonLabel" runat="server" Text="Decline Reson :"></asp:Label>
                        </td>
                        <td><asp:TextBox id="declineResonTextBox" runat="server" ReadOnly="true" 
                                Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="officeAddressLabel" runat="server" Text="Office Address :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="companyAddressTextBox" runat="server" TextMode="MultiLine" 
                                Width="220px" ReadOnly="True" Height="30px"></asp:TextBox>
                        </td>
                        <td align="right" colspan="2"><asp:Label ID="declineDateLabel" runat="server" Text="Decline Date :"></asp:Label>
                        </td>
                        <td><asp:TextBox ID="declineDateTextBox" runat="server" ReadOnly="true" Height="12px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
    </div>
                </td>
            </tr>
        </table>
    
    </div>
    </td></tr></table>
    </center>
    </div>
    </form>
</body>
</html>
