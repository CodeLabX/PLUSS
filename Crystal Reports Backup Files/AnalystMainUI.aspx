﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_AnalystMainUI" Codebehind="AnalystMainUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/dhtmlxtabbar.ascx" TagPrefix="uc1" TagName="dhtmlxtabbar" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxtabbar.ascx" TagPrefix="uc2" TagName="dhtmlxtabbar" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxtabbar_start_Scripts.ascx" TagPrefix="uc1" TagName="dhtmlxtabbar_start_Scripts" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_codebase.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_codebase" %>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Analyst Main</title>
    <uc1:dhtmlxtabbar runat="server" ID="dhtmlxtabbar" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc2:dhtmlxtabbar runat="server" ID="dhtmlxtabbar1" />
    <uc1:dhtmlxtabbar_start_Scripts runat="server" ID="dhtmlxtabbar_start_Scripts" />
    <uc1:dhtmlxgrid_codebase runat="server" ID="dhtmlxgrid_codebase" />
    <%--<link rel="STYLESHEET" type="text/css" href="../codebase/dhtmlxgrid_skins.css" />--%>

    <style type="text/css">
        .PaddingRight
        {
            padding-right: 50px;
            text-decoration:none;	
        }
    </style>
</head>
<body style="margin-top: 0; margin-bottom: 0; margin-left: 0; margin-right: 0;">
    <form id="analystMainForm" runat="server">
    <div>
        <img src="../images/pageFrameWork_Top.gif" alt="" style="width: 100%;" /></div>
    <div style="text-align: right; font-size: 12px; background-color: #009ACD; height:15px">
        <b style="color:#000000">Login as:</b>
        <asp:Label ID="userNameLabel" runat="server" ForeColor="#FFFFFF" Font-Bold="true"></asp:Label>
        &nbsp;<b style="color:#000000">|</b>&nbsp;
        <asp:LinkButton ID="logOutLinkButton" runat="server" Text="LogOut" ForeColor="#FFFFFF" 
            OnClick="logOutLinkButton_Click" CssClass="PaddingRight" Font-Bold="true"></asp:LinkButton>
    </div>
    <div>
        <table>
            <tr>
                <td>
                    <div id="a_tabbar" class="dhtmlxTabBar" imgpath="../codebase/imgs/" style="width: 990px;
                        height: 1600px;" skincolors="#FCFBFC,#F4F3EE" />
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript">
            var tabbar;
			tabbar=new dhtmlXTabBar("a_tabbar","top");
            tabbar.setImagePath("../codebase/imgs/");
            //tabbar.setStyle("silver");
            //tabbar.setStyle("modern");
            tabbar.preventIECashing(true);
            tabbar.loadXML("../includes/Aanlysttabs.xml");                        
            //tabbar.setOnSelectHandler(my_func);
            
             function my_func(idn,ido){
                if (confirm("Tab "+ido+" to set active\nPlease Confirm?"))
                    return true;
                return false;
            }
            function GotoAnyTab(idn,ido)
            {
                if(idn=="b2")
                {
                    tabbar.setTabActive("b2");
                }
            }
            function   goToNextTabA()
            {
				var z=tabbar.goToNextTab();
				if (!z) tabbar.setTabActive(tabbar.rows[0].childNodes[0].idd);
			}
    </script>
</body>
</html>
