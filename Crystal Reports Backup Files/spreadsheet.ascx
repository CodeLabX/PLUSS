﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserControls.css.UI_UserControls_css_spreadsheet" Codebehind="spreadsheet.ascx.cs" %>
<style type="text/css">
    body, input {
        font-family: Verdana, Arial;
        font-size: 8pt;
    }
    .az-required {
        color: red;
        font-weight: bold;   
    }
    .ssHeader {
        height: 20px;
        background: #d9eaff;
        background-image: url(../Images/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
    }

    .SCBHeader {
        height: 30px;
        background: #d9eaff;
        background-image: url(../Images/bar_edited.gif);
        border: solid 1px buttonshadow; /*color: #4d4d4d;*/
        color: #4d4d4d;
    }

    .ssHeaderSelected {
        background: #ffda82;
        color: #4d4d4d;
        border: solid 1px darkorange;
    }

    .ssRowLabel {
        background: #d9eaff;
        width: 18px;
        color: #4d4d4d;
        font-size: 9pt;
        border-top-width: 0px;
        border-right-width: 1px;
        border-right-style: Solid;
        border-right-color: buttonshadow;
        border-left-width: 1px;
        border-left-style: Solid;
        border-left-color: buttonshadow;
        border-bottom-width: 1px;
        border-bottom-color: buttonshadow;
        border-bottom-style: Solid;
    }

    .ssRowLabelSelected {
        background: #ffda82;
        color: #4d4d4d;
        border: solid 1px darkorange;
    }

    .ssCell {
        height: 9px;
        width: 60px;
        white-space: nowrap;
        overflow: hidden;
        padding: 0px 1px 0px 1px;
        border-top-width: 0px;
        border-right-width: 1px;
        border-right-style: Solid;
        border-right-color: buttonshadow;
        border-left-width: 0px;
        border-bottom-width: 1px;
        border-bottom-color: buttonshadow;
        border-bottom-style: Solid;
    }

    .ssCellSelected {
        height: 8px;
        border: solid 2px #000000;
        padding: 0px 0px 0px 0px;
    }

    .ssTextBox {
        font-size: 9pt;
        width: 58px;
        border: none 0px;
    }

    .rightTextBox {
        font-size: 9pt;
        width: 58px;
        border: none 0px;
        text-align: right;
    }

    .centerTextBox {
        font-size: 9pt;
        width: 58px;
        border: none 0px;
        text-align: center;
    }

    .FixedHeader {
        height: 20px;
        background: #d9eaff;
        background-image: url(../Images/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        position: relative;
        z-index: 10;
    }

    .FixedFooter {
        height: 20px;
        background: #d9eaff;
        background-image: url(../img/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        position: static;
        z-index: 10;
    }

    .RightAlignTextBox {
        text-align: right;
    }

    .Zindex {
        z-index: 0;
    }

    .FixedHeader1 {
        height: 20px;
        background: #d9eaff;
        background-image: url(../img/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        position: absolute;
        z-index: 10;
        margin-top: -20px;
    }

    .GridviewBorder {
        border: solid 1px black;
    }

    .GridviewBorder2 {
        border: solid 1px black;
        z-index: 10;
    }

    .gvTextBox {
        font-size: 9pt;
        width: 58px;
        border: none 0px;
    }

    .gvFooter {
        height: 20px;
        background: #d9eaff;
        background-image: url(../Images/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
    }

    .FixedHeader2 {
        height: 20px;
        background: #d9eaff;
        background-image: url(../Images/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        position: static;
        z-index: 10;
    }

    .GvTableHeader {
        height: 25px;
        background: #d9eaff url('../Images/bar.gif');
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        font-weight: bold;
        z-index: 10;
    }

    .GvHeader {
        height: 25px;
        background: #d9eaff url('../Images/bar_edited.gif');
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        font-weight: bold;
        z-index: 10;
    }

    .GvHeader2 {
        height: 25px;
        background: #d9eaff url('../Images/bar.gif');
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        font-weight: bold;
        z-index: 10;
    }

    .GvFixedFooter {
        height: 20px;
        background: #d9eaff;
        background-image: url(../Images/bar.gif);
        border: solid 1px buttonshadow;
        color: #4d4d4d;
        position: relative;
        z-index: 10;
    }

    .DivBorder {
        border: solid 1px black;
        width: 390px;
        z-index: 10;
    }

    .TdStyle {
        border-style: solid;
        border-width: 1px;
        font-family: verdana, arial, sans-serif;
        font-size: 10px;
        padding: 0px;
    }

    .inputStyle {
        border-style: solid;
        border-width: 1px;
        font-family: verdana, arial, sans-serif;
        font-size: 9px;
        padding: 0px;
        color: black;
    }
</style>
