﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_NegativeListPerson" Codebehind="NegativeListPersonUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_codebase.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_codebase" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon_codebase.ascx" TagPrefix="uc1" TagName="dhtmlxcommon_codebase" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgridcell.ascx" TagPrefix="uc1" TagName="dhtmlxgridcell" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_srnd.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_srnd" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgridcss.ascx" TagPrefix="uc1" TagName="dhtmlxgridcss" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_skinscss.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_skinscss" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Negative List Person Result</title>

    <uc1:dhtmlxgridcss runat="server" ID="dhtmlxgridcss" />
    <uc1:dhtmlxgrid_skinscss runat="server" ID="dhtmlxgrid_skinscss" />
    <uc1:dhtmlxgrid_codebase runat="server" ID="dhtmlxgrid_codebase" />
    <uc1:dhtmlxcommon_codebase runat="server" ID="dhtmlxcommon_codebase" />
    <uc1:dhtmlxgridcell runat="server" ID="dhtmlxgridcell" />
    <uc1:dhtmlxgrid_srnd runat="server" ID="dhtmlxgrid_srnd" />
</head>
<body style=" margin-top:0; margin-left:0; margin-right:0; margin-bottom:0">
   <form id="form1" runat="server">
       <div id="gridbox"  style="background-color:white; width:970px; height:253px;"></div>
    </form>
    <script type="text/javascript">
	var mygrid;
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath("../codebase/imgs/");
	mygrid.setHeader("MF,Dedup Id,Name,A/C No.,Decline Reason,Decline Date,ID,Phone1,Phone2, DOB");
	mygrid.setInitWidths("32,90,140,100,130,95,90,90,90,95");
	mygrid.setColAlign("left,right,left,right,left,right,right,right,right,right");
	mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	mygrid.setColSorting("int,int,str,int,str,int,int,int,int,int");
	mygrid.setSkin("light");	 
	mygrid.init();
	mygrid.enableSmartRendering(true,20);
	//mygrid.loadXML("../includes/DEDUPSLNO.xml");
	</script>
</body>
</html>
