﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.NegativeListApprove" Title="NegativeList Approve" CodeBehind="NegativeListApprove.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/azGridTableCss.ascx" TagPrefix="uc1" TagName="azGridTableCss" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <%--<uc1:azGridTableCss runat="server" ID="azGridTableCss" />--%>
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Negative List Approve</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr style="font-weight: bold; font-size: 16px" align="center">
                <td>
                    <%--<asp:Label ID="nameLabel" runat="server" Text="Watch List Upload"></asp:Label>--%></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <%--<tr>
                <td>
                    <asp:FileUpload ID="FileUpload" runat="server" Width="350px" Height="24px" />
                    <asp:Button ID="UploadButton" runat="server" Text="Upload" Width="100px"
                        Height="24px" OnClick="uploadButton_Click" />
                    <input id="clearButton" type="button" value="Clear" style="width: 100px; height: 24px; display: none;" onclick="JS_FunctionClear()" /></td>
            </tr>--%>
            <tr>
                <td>
                    <%--<div id="gridbox"  style="background-color:white; width:805px; height:470px;"></div>--%>
                    <%--<div id="tableHeadDiv" runat="server" style="border: solid 1px gray;"></div>
                    <div id="recordViewDiv" runat="server" style="width: 805px; height: 470px; z-index: 5000; border: solid 1px gray;"></div>--%>

                    <asp:GridView ID="gvRecords" runat="server" GridLines="None" CssClass="myGridStyle"
                        Font-Size="11px" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="SL." />
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="FatherName" HeaderText="Father's Name" />
                            <asp:BoundField DataField="MotherName" HeaderText="Mother's Name" />
                            <asp:TemplateField HeaderText="DOB">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("DateOfBirth")).ToString("dd-MMM-yyyy") %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:BoundField DataField="AccountNo.MasterAccNo1" HeaderText="Master No" />
                            <asp:BoundField DataField="DeclineReason" HeaderText="Decline Reason" />
                            <asp:BoundField DataField="BusinessEmployeeName" HeaderText="Business Name" />
                            <asp:BoundField DataField="Address1" HeaderText="Address 1" />
                            <asp:BoundField DataField="Address2" HeaderText="Address 2" />
                            <asp:BoundField DataField="ContactNumber.ContactNo1" HeaderText="Phone 1" />
                            <asp:BoundField DataField="ContactNumber.ContactNo2" HeaderText="Phone 2" />
                            <asp:BoundField DataField="ContactNumber.ContactNo3" HeaderText="Mobile 1" />
                            <asp:BoundField DataField="ContactNumber.ContactNo4" HeaderText="Mobile 2" />
                            <asp:BoundField DataField="Source" HeaderText="Source" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                        </Columns>
                        <EmptyDataTemplate>
                            No record found
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <br />
                    <br />

                    <%--<div id="pagingDiv" runat="server"></div>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnApprove" runat="server" CssClass="btn primarybtn" OnClick="btnApprove_Click" Text="Approve" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn clearbtn" Text="Delete From Temp" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnDownload" runat="server" CssClass="btn primarybtn" Text="Download" OnClick="btnDownload_Click" />

                </td>
                <td>
                    <%--<asp:Button ID="btnDelete" runat="server" CssClass="btn clearbtn" Text="Delete From Temp" OnClick="btnDelete_Click" />--%></td>
                <td>
                    </td>
            </tr>
        </table>
    </div>


    <script type="text/javascript">
        function JS_FunctionClear() {
            mygrid.clearAll();
        }
    </script>

    <br/><br/><br/>
    <br/><br/><br/>
</asp:Content>

