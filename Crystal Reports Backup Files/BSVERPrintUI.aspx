﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_BSVERPrintUI" Codebehind="BSVERPrintUI.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BSVER Print</title>
        <script language="javascript" type="text/javascript">
        function PrintReport()
        {
            window.print();
        }
    </script>
</head>
<body onload="PrintReport();" style="margin-top:0; margin-bottom:0; margin-left:0; margin-right:0;">
    <form id="form1" runat="server">
    <br />
    <br />
    <center>
    <table width="95%" border="0" cellpadding="1" cellspacing="1"><tr><td align="left">
        <%--<input id="printButton" type="button" value="Print" onclick="PrintReport()" />--%>
    <div style="padding-left:0px;">
        <fieldset>
            <div>
            <br />
            <asp:Label ID="refLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="dateLabel" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <asp:Label ID="theManagerLabel" runat="server" Text="The Manager" Font-Bold="true"></asp:Label>
            <br />
            <asp:Label ID="bankNameLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="branchNameLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="branchAddressLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="branchLocationLabel" runat="server" Text=""></asp:Label>
            <br />            
            <br />
            <br />
            <br />
            <asp:Label ID="Label" runat="server" Text="Subject :" Font-Bold="true" Font-Underline="true"></asp:Label>
            <asp:Label ID="subjectLabel" runat="server" Text="11" Font-Bold="true" Font-Underline="true"></asp:Label>
            <br />
            <br />
            <br /> 
            <asp:Label ID="dearSirLabel" runat="server" Text="Dear Sir/Madam." Font-Bold="true"></asp:Label>
            <br />
            <br /> 
            <asp:Label ID="bodyLabel" runat="server" Text=""></asp:Label>
            <br />
            <br />            
            <asp:Label ID="bodyOneLabel" runat="server" Text=""></asp:Label>
            <br />   
            <br /> 
            <br /> 
            <div style="padding-left:100px;">         
            <table border="0" cellspacing="1" cellpadding ="1" style="vertical-align:middle;" width="60%">
            <tr>
            <td style="font-weight:bold;"><asp:Label ID="accountNameLabel" runat="server" Text="Account Name" Font-Underline="true"></asp:Label></td>
            <td style="font-weight:bold;"><asp:Label ID="accountNoLabel" runat="server" Text="Account Number" Font-Underline="true"></asp:Label></td>
            </tr>
            <tr>
            <td style="font-weight:bold;"><asp:Label ID="accountNameValueLabel" runat="server" Text=""></asp:Label></td>
            <td style="font-weight:bold;"><asp:Label ID="accountNoValueLabel" runat="server" Text=""></asp:Label></td>
            </tr>            
            </table>
            </div>
            <br />
            <br />
            <br />
            <asp:Label ID="bodyTwoLabel" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <asp:Label ID="bodyThreeLabel" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <asp:Label ID="bodyFourLabel" runat="server" Text=""></asp:Label>
            <br /> 
            <br />
            <asp:Label ID="bodyFiveLabel" runat="server" Text=""></asp:Label>
            <br /> 
            <br /> 
            <asp:Label ID="bodySixLabel" runat="server" Text=""></asp:Label>
            <br /> 
            <br /> 
            <br /> 
            <br />             
            <asp:Label ID="approverNameLabel" runat="server" Text="" Font-Bold="true"></asp:Label>
            <br /> 
            <asp:Label ID="designationLabel" runat="server" Text="................." Font-Bold="true"></asp:Label>
            <asp:Label ID="personalLoansLabel" runat="server" Text="-Personal Loans" Font-Bold="true"></asp:Label>
            <br /> 
            <asp:Label ID="consumerBankingLabel" runat="server" Text="Consumer Banking" Font-Bold="true"></asp:Label>
            <br />             
            </div>
        </fieldset>
    </div>
    </td></tr></table>
    </center>
    </form>
</body>
</html>
