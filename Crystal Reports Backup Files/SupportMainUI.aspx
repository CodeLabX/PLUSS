﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SupportMainUI" Codebehind="SupportMainUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/dhtmlxtabbar.ascx" TagPrefix="uc1" TagName="dhtmlxtabbar" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxtabbar.ascx" TagPrefix="uc2" TagName="dhtmlxtabbar" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxtabbar_start.ascx" TagPrefix="uc1" TagName="dhtmlxtabbar_start" %>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>PLUSS</title> 
    <uc1:dhtmlxtabbar runat="server" ID="dhtmlxtabbar" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc2:dhtmlxtabbar runat="server" ID="dhtmlxtabbar1" />
    <uc1:dhtmlxtabbar_start runat="server" ID="dhtmlxtabbar_start" />
    <style type="text/css">
    .PaddingRight
        {
        	padding-right:50px;
        	text-decoration:none;	
    	}
    </style>
</head>
<body style=" margin-top:0; margin-left:0; margin-right:0; margin-bottom:0">
    <form id="form1" runat="server">
    <div><img src="../images/pageFrameWork_Top.gif" alt="" style="width:100%;" /></div>
    <div style="text-align:right; font-size:12px; background-color:#009ACD;height:15px;"><b>Login as:</b>
    <asp:Label ID="userNameLabel" runat="server" Font-Bold="true" ForeColor="#FFFFFF"></asp:Label> &nbsp;<b style="color:#000000">|</b>&nbsp;
    <asp:LinkButton ID="logOutLinkButton" runat="server" Text="LogOut" ForeColor="#FFFFFF" 
            onclick="logOutLinkButton_Click" CssClass="PaddingRight" Font-Bold="true"></asp:LinkButton>
    </div>
    <div>
    <table>
		<tr>
			<td>
				<div id="a_tabbar" class="dhtmlxTabBar" imgpath="../codebase/imgs/" style="width:990px; height:1500px;" skinColors="#FCFBFC,#F4F3EE"/>
			</td>
        </tr>
	</table>
    </div>
    </form>
    <script type="text/javascript">
			tabbar=new dhtmlXTabBar("a_tabbar","top");
            tabbar.setImagePath("../codebase/imgs/");
            tabbar.preventIECashing(true);
            tabbar.loadXML("../includes/Srpporttabs.xml");
	</script>   
</body>
</html>
