﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.Reports.Reports_ReportViewer" Codebehind="ReportViewer.aspx.cs" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
             Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report viewer page</title>
</head>
<body>
    <form id="form1" runat="server">  
    <div>
        <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />
    
  <%--<CR:CrystalReportViewer ID="crystalReportViewer" runat="server" AutoDataBind="true"
                                        DisplayGroupTree="False" ReuseParameterValuesOnRefresh="True" HasCrystalLogo="False" />--%>
    </div>
    </form>
</body>
</html>
