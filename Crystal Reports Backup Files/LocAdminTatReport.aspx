﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.Master" AutoEventWireup="true" CodeBehind="LocAdminTatReport.aspx.cs" Inherits="PlussAndLoan.UI.LocAdminTatReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">

    <tr>
        <td>
            
              <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                    <tr bgcolor="#CEDFBD">
                        <td height="30" colspan="11" valign="middle" style="text-align: center;">
                            <div style="text-align: center; font-weight: bold;"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">Application Status Wise Reprot</h1></font></div>
                        </td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                        </td>
                        <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:DropDownList ID="productDropDownList" runat="server" Width="200px"></asp:DropDownList>
              </font>&nbsp; 
                        </td>

                        <td width="315" height="30" align="left" valign="middle" bgcolor="#EFF3E7">
                            <asp:DropDownList ID="appr_disb" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="Approved" Value="5" />
                                <asp:ListItem Text="Disbursed" Value="7" />
                                <asp:ListItem Text="Deffered" Value="6" />
                                <asp:ListItem Text="Declined" Value="2" />
                                <asp:ListItem Text="Verification" Value="9" />
                                <asp:ListItem Text="In Process" Value="4" />
                            </asp:DropDownList>

                        </td>

                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                        </td>
                        <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                            <div align="left">
                                <font color="#FFFFFF" face="Courier New, Courier, mono"> </font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                   
                    <asp:DropDownList ID="DayDropDownList" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="MonthDropDownList" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="YearDropDownList" runat="server"> </asp:DropDownList>   

              </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font color="#FFFFFF" face="Courier New, Courier, mono"> </font>
                            </div>

                        </td>
                        <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                        <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                            <asp:DropDownList ID="ToDateDayList" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ToDateMonthList" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ToDateYearList" runat="server"></asp:DropDownList>


                        </td>
                    </tr>
                    <tr bgcolor="#CEDFBD">
                        <td height="30">
                            <div align="right"></div>
                        </td>
                        <td height="30">&nbsp;</td>
                        <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">
                    <asp:Button runat="server" Text="Show" ID="btnShow"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnShow_Click"/>
            
</font>

                        </td>
                    </tr>
                  
                    <tr bgcolor="#CEDFBD">
                        <td height="30" colspan="2" bgcolor="#CEDFBD">&nbsp;</td>
                        <td height="30" align="left" bgcolor="#CEDFBD">
                              <asp:Button runat="server" Text="Detail Report" ID="btnDetailReport"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="100" OnClick="btnDetailReport_Click"/>
                           
                        </td>
                    </tr>
                </table>
            
            <br/>
            
            
            
              <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                    <tr bgcolor="#CEDFBD">
                        <td height="30" colspan="11" valign="middle" style="text-align: center;">
                            <div style="text-align: center; font-weight: bold;"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">Individual TAT Report</h1></font></div>
                        </td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                        </td>
                        <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:DropDownList ID="TatProductDropDown" runat="server" Width="200px"></asp:DropDownList>
              </font>&nbsp; 
                        </td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                        </td>
                        <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                            <div align="left">
                                <font color="#FFFFFF" face="Courier New, Courier, mono"> </font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                   
                    <asp:DropDownList ID="TatDayDropDown" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="TatMonthDropDown" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="TatYearDropDown" runat="server"> </asp:DropDownList>   

              </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font color="#FFFFFF" face="Courier New, Courier, mono"> </font>
                            </div>

                        </td>
                        <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                        <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                            <asp:DropDownList ID="ByDayDropDown" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ByMonthDropDown" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ByYearDropDown" runat="server"></asp:DropDownList>


                        </td> 
                         <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;
                    <asp:Button runat="server" Text="Show" ID="btnIndividualTat"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnIndividualTat_Click" />
            </font>

                        </td>
                    </tr>
   
                </table>           
            <br/>
            
               <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                    <tr bgcolor="#CEDFBD">
                        <td height="30" colspan="11" valign="middle" style="text-align: center;">
                            <div style="text-align: center; font-weight: bold;"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">Individual TAT by Date</h1></font></div>
                        </td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                        </td>
                        <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:DropDownList ID="InProductDropDown" runat="server" Width="200px"></asp:DropDownList>
              </font>&nbsp; 
                        </td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                        </td>
                        <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                            <div align="left">
                                <font color="#FFFFFF" face="Courier New, Courier, mono"> </font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                   
                    <asp:DropDownList ID="InDayDropDown" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="InMonthDropDown" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="InYearDropDown" runat="server"> </asp:DropDownList>   

              </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font color="#FFFFFF" face="Courier New, Courier, mono"> </font>
                            </div>

                        </td>
                        <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                        <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                            <asp:DropDownList ID="InToDayDropDown" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="InToMonthDropDown" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="InToYearDropDown" runat="server"></asp:DropDownList>


                        </td> 
                         <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;
                    <asp:Button runat="server" Text="Show" ID="btnTatByDate"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnTatByDate_Click" />
            </font>

                        </td>
                    </tr>
   
                </table>
            <br/>
            
             <table width="780" border="0" cellpadding="0" cellspacing="1" bgcolor="BDD3A5">
                    <tr align="center" bgcolor="#CBDCB8">
                        <td height="30" colspan="3" valign="middle"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">View&nbsp;Quick&nbsp;&nbsp;Report</h1></font></td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#DCE8CE">PRODUCT:&nbsp;</td>
                        <td height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                       <asp:DropDownList ID="DropDownProductList" runat="server" Width="200px"></asp:DropDownList>
                        </td>
                        <td align="left" valign="middle" bgcolor="#EDF2E6">&nbsp;</td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="28%" height="30" align="right" valign="middle" bgcolor="#DCE8CE">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MONTH:&nbsp;</font></div>
                        </td>
                        <td width="19%" height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                            <asp:DropDownList ID="DropDownMonthList" runat="server"></asp:DropDownList>

                        </td>
                        <td width="53%" align="left" valign="middle" bgcolor="#EDF2E6"><font color="#FFFFFF" face="Courier New, Courier, mono">
                   
                  <asp:Button runat="server" Text="Quick Report -Monthly" ID="btnQuickReportMonthly"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="160" OnClick="btnQuickReportMonthly_Click" />
                 </font></td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#DCE8CE">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">YEAR:&nbsp;</font></div>
                        </td>
                        <td height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                            <asp:DropDownList ID="DropDownYearList" runat="server"></asp:DropDownList>
                        </td>
                        <td height="30" align="left" valign="middle" bgcolor="#EDF2E6"><font color="#FFFFFF" face="Courier New, Courier, mono">
                    <asp:Button runat="server" Text="Quick Report -Year To Date" ID="btnQuickReportYearly"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="190" OnClick="btnQuickReportYearly_Click" />
                          
                 </font></td>
                    </tr>
                    <tr bgcolor="#CBDCB8">
                        <td height="30">&nbsp;</td>
                        <td height="30" colspan="2" align="left" valign="middle" bgcolor="#CBDCB8">&nbsp;</td>
                    </tr>
           </table>
            <br/>
            
             <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC"  bgcolor="BDD3A5">
          <tr align="center" valign="middle" bgcolor="#CEDFBD"> 
            <td height="30" colspan="10"> <div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">List Of Application Deferred/Declined- According To Reasons</h1></font></div></td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td width="26%" height="30" align="right" valign="middle" bgcolor="#D5E2C5" ><font size="2"><font face="Verdana, Arial, Helvetica, sans-serif">PRODUCT:&nbsp;</font> 
              </font> <div align="right"></div></td>
            <td height="30" colspan="2" align="left" valign="middle"  bgcolor="#EFF3E7">
                 <asp:DropDownList ID="ProductListDropDown" runat="server" Width="200px"></asp:DropDownList>  
           </td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5" > 
              <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">STATUS:&nbsp;</font></div></td>
            <td height="30" colspan="2" align="left" valign="middle"  bgcolor="#EFF3E7">
                
                 <asp:DropDownList ID="statDropDownList" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="Deffered" Value="2" />
                                <asp:ListItem Text="Declined" Value="1" />
                            </asp:DropDownList>
                
            </td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5" > 
              <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MONTH:&nbsp;</font></div></td>
            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7" >
			 <asp:DropDownList ID="AppMonthDropDown" runat="server"></asp:DropDownList>
            </td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5" > 
              <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">YEAR:&nbsp;</font></div></td>
            <td height="30" colspan="2" align="left" valign="middle"  bgcolor="#EFF3E7">
                 <asp:DropDownList ID="AppYearDropDown" runat="server"></asp:DropDownList> 
           	</td>
          </tr>
          <tr bgcolor="#CEDFBD"> 
            <td height="30" > 
            </td>
            <td width="21%" height="30" align="left" valign="middle"> <font face="Verdana, Arial, Helvetica, sans-serif">&nbsp; 
              </font> 
            </td>
            <td width="53%" align="left" valign="middle">
                 <asp:Button runat="server" Text="Show" ID="btnAppShow"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnAppShow_Click"/>
             
            </td>
          </tr>
        </table>
            
            </td>
        </tr>
        </table>
</asp:Content>
