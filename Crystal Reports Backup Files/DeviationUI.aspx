﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_DeviationUI" MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="DeviationUI.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function PrintDeviation()
        {
            window.print();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table>
       <%-- <asp:ScriptManager ID="deviationScriptManager" runat="server" />--%>
        <tr>
            <td>
                <table cellpadding="1" cellspacing="1">
                    <tr>
                        <td>
                            LLID:
                        </td>
                        <td>
                            <asp:TextBox ID="llidTextBox" runat="server" Width="177px" MaxLength="9"></asp:TextBox>
                            <asp:Button ID="searchButton" runat="server" Text="..." 
                                OnClick="searchButton_Click" />
                            &nbsp;
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="digit only"
                                ControlToValidate="llidTextBox" ValidationExpression="^[0-9]+$">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td>
                            &nbsp;
                            <asp:HiddenField ID="hiddenField1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#339933" colspan="3" align="center" style="color: White">
                            PDD Deviation
                        </td>
                    </tr>
                    <tr bgcolor="#99FFCC">
                        <td align="center" style="color: Black">
                            LEVEL
                        </td>
                        <td align="center" style="color: Black">
                            Description
                        </td>
                        <td align="center" style="color: Black">
                            CODE
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="levelDropDownList1" runat="server" Width="76px" AutoPostBack="True"
                                        OnSelectedIndexChanged="levelDropDownList1_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="descriptionDropDownList1" runat="server" Width="426px" AutoPostBack="True"
                                        OnSelectedIndexChanged="descriptionDropDownList1_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="codeTextBox1" runat="server" Enabled="False"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="levelDropDownList2" runat="server" Width="76px" AutoPostBack="True"
                                        OnSelectedIndexChanged="levelDropDownList2_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="descriptionDropDownList2" runat="server" Width="426px" AutoPostBack="True"
                                        OnSelectedIndexChanged="descriptionDropDownList2_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="codeTextBox2" runat="server" Enabled="False"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="levelDropDownList3" runat="server" Width="76px" AutoPostBack="True"
                                        OnSelectedIndexChanged="levelDropDownList3_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="descriptionDropDownList3" runat="server" Width="426px" AutoPostBack="True"
                                        OnSelectedIndexChanged="descriptionDropDownList3_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="codeTextBox3" runat="server" Enabled="False"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="levelDropDownList4" runat="server" Width="76px" AutoPostBack="True"
                                        OnSelectedIndexChanged="levelDropDownList4_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td >
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="descriptionDropDownList4" runat="server" Width="426px" AutoPostBack="True"
                                        OnSelectedIndexChanged="descriptionDropDownList4_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="codeTextBox4" runat="server" Enabled="False"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="3">
                <table cellspacing="1">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td bgcolor="#339933" colspan="2" style="color: White">
                            Identified PDD Deviation
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td bgcolor="#00FFCC" style="color: Black">
                            Criteria
                        </td>
                        <td bgcolor="#00FFCC" style="color: Black">
                            Level
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Deviations to
                        </td>
                        <td>
                            DBR
                        </td>
                        <td>
                            <asp:TextBox ID="dbrTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            be arranged on
                        </td>
                        <td>
                            TENOR
                        </td>
                        <td>
                            <asp:TextBox ID="tenorTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            priority basis
                        </td>
                        <td>
                            MIN INCOME
                        </td>
                        <td>
                            <asp:TextBox ID="minIncomeTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            MUE
                        </td>
                        <td>
                            <asp:TextBox ID="mueTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            MIN AGE
                        </td>
                        <td>
                            <asp:TextBox ID="minAgeTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            MAX AGE
                        </td>
                        <td>
                            <asp:TextBox ID="maxAgeTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Banca DBR
                        </td>
                        <td>
                            <asp:TextBox ID="bancaDBRTextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Banca MUE
                        </td>
                        <td>
                            <asp:TextBox ID="bancaMUETextBox" runat="server" Width="69px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="100">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="1" cellspacing="1" class="style7">
                    <tr>
                        <td bgcolor="#339933" style="color: White">
                            Remarks / Condition
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="overflow-y: scroll; width: 655px; height: 160px; z-index: 2; border: solid 1px gray;"
                                visible="true">
                                <asp:GridView ID="remarksGridView" runat="server" Width="635px" AutoGenerateColumns="False"
                                    AllowPaging="False">
                                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="IdCheckBox" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Id" HeaderStyle-Width="20px" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="remarksTextBox" runat="server" Text='<%# Eval("Name") %>' Width="580px"
                                                    CssClass="ssTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="FixedHeader" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td >
                <table cellpadding="1" cellspacing="1">
                    <tr>
                        <td bgcolor="#339933" colspan="7" style="color: White">
                            Strength
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC"  align="center">
                            1
                        </td>
                        <td >
                            Salary a/c relationship with
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="relationshipDropDownList" runat="server" Width="186px">
                            </asp:DropDownList>
                        </td>
                        <td >
                            <asp:DropDownList ID="relationshipStatusDropDownList" runat="server" Width="76px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Excellent</asp:ListItem>
                                <asp:ListItem>Satisfactory</asp:ListItem>
                                <asp:ListItem>Good</asp:ListItem>
                                <asp:ListItem>Moderate</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            Since
                        </td>
                        <td>
                            <asp:TextBox ID="relationshipYearTextBox" runat="server" Width="105px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC"  align="center">
                            2
                        </td>
                        <td >
                            Residential Status
                        </td>
                        <td>
                            <asp:DropDownList ID="recedentialStatusDropDownList" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Own</asp:ListItem>
                                <asp:ListItem>Family Owned</asp:ListItem>
                                <asp:ListItem>Others</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td  colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC"  align="center">
                            3
                        </td>
                        <td >
                            Credit Card Repayment
                        </td>
                        <td >
                            <asp:DropDownList ID="ccRepaymentStatusDropDownList" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Excellent</asp:ListItem>
                                <asp:ListItem>Satisfactory</asp:ListItem>
                                <asp:ListItem>Good</asp:ListItem>
                                <asp:ListItem>Moderate</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td >
                            Limit
                        </td>
                        <td >
                            <asp:DropDownList ID="ccRepaymentLimitDropDownList" runat="server" Width="76px">
                                <%--<asp:ListItem>TK.50000</asp:ListItem>
                                <asp:ListItem>Closed</asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>
                        <td >
                            Availed since
                        </td>
                        <td>
                            <%--<asp:DropDownList ID="ccRepaymentYearDropDownList" runat="server" Width="110px">
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="ccRepaymentDateTextBox" runat="server" Width="105px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC"  align="center">
                            4
                        </td>
                        <td>
                            Previous Repayment
                        </td>
                        <td>
                            <asp:DropDownList ID="previousRepaymentStatusDropDownList1" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Excellent</asp:ListItem>
                                <asp:ListItem>Satisfactory</asp:ListItem>
                                <asp:ListItem>Good</asp:ListItem>
                                <asp:ListItem>Moderate</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            Loan Type
                        </td>
                        <td>
                            <asp:DropDownList ID="previousRepaymentLoanDropDownList1" runat="server" Width="76px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            Availed since
                        </td>
                        <td>
                            <%--<asp:DropDownList ID="previousRepaymentYearDropDownList1" runat="server" Width="110px">
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="previousRepaymentDateTextBox1" runat="server" Width="105px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC"  align="center">
                            5
                        </td>
                        <td>
                            Previous Repayment
                        </td>
                        <td>
                            <asp:DropDownList ID="previousRepaymentStatusDropDownList2" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Excellent</asp:ListItem>
                                <asp:ListItem>Satisfactory</asp:ListItem>
                                <asp:ListItem>Good</asp:ListItem>
                                <asp:ListItem>Moderate</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            Loan Type
                        </td>
                        <td>
                            <asp:DropDownList ID="previousRepaymentLoanDropDownList2" runat="server" Width="76px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            Availed since
                        </td>
                        <td>
                            <%--<asp:DropDownList ID="previousRepaymentYearDropDownList2" runat="server" Width="110px">
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="previousRepaymentDateTextBox2" runat="server" Width="105px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC"  align="center">
                            6
                        </td>
                        <td>
                            Others
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="strengthOthersDropDownList" runat="server" Width="484px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                Date format :dd-MM-yyyy
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td bgcolor="#339933" colspan="7" style="color: White">
                            Weakness
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC">
                            1
                        </td>
                        <td>
                            Repayment Not Satisfactory
                        </td>
                        <td>
                            <asp:DropDownList ID="RepaymentNotSatisfactoryDropDownList" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            Loan Type
                        </td>
                        <td>
                            <asp:DropDownList ID="repaymentNotLoanTypeDropDownList" runat="server" Width="76px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            DPD
                        </td>
                        <td>
                            <asp:DropDownList ID="weaknessDpdDropDownList" runat="server" Width="106px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>30+</asp:ListItem>
                                <asp:ListItem>60+</asp:ListItem>
                                <asp:ListItem>90+</asp:ListItem>
                                <asp:ListItem>120+</asp:ListItem>
                                <asp:ListItem>150+</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC">
                            2
                        </td>
                        <td>
                            Cash Salaried
                        </td>
                        <td>
                            <asp:DropDownList ID="cashSalariedStatusDropDownList" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC">
                            3
                        </td>
                        <td>
                            Frequent Cheque return
                        </td>
                        <td>
                            <asp:DropDownList ID="freqCheckReturnStatusDropDownList" runat="server" Width="120px">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#CCCCCC">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="weaknessOthersTextBox" runat="server" Width="417px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2" align="right">
                <table border="0" class="style24">
                    <tr>
                        <td align="right">
                            <asp:Button ID="saveButton" runat="server" Text="Save" Width="83px" Style="margin-left: 121px"
                                OnClick="saveButton_Click" Height="27px" />
                        </td>
                        <td align="left" class="style23">
                            <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" 
                                OnClick="clearButton_Click" Height="27px" />
                        </td>
                        <td align="left">
                            <asp:Button ID="printButton" runat="server" Text="Print" Width="100px" 
                                Height="27px" Visible="False" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
