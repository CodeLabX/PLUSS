﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_MISReportUI"
    MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="MISReportUI.aspx.cs" %>

<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
    <div>
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr>
                <td style="width: 20%; height: 450px;" valign="middle">
                    <fieldset>
                        <legend>Report Action</legend>
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="plchecklistButton" runat="server" Text="PL-Check List" Width="220px"
                                        OnClick="plchecklistButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="LLINewButton" runat="server" Text="LLINEW" Width="220px" OnClick="LLINewButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="baselButton" runat="server" Text="Basel" Width="220px" OnClick="baselButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="approvalButton" runat="server" Text="Approval Info." Width="220px"
                                        OnClick="approvalButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="docListButton" runat="server" Text="Doc List" Width="220px" OnClick="docListButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="declineBaselButton" runat="server" Text="Decline Basel" Width="220px"
                                        OnClick="declineBaselButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="showAllButton" runat="server" Text="Print All" Width="220px" OnClick="showAllButton_Click" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td>
                                    <asp:Button ID="exceptionRerptBtn" runat="server" Text="Exception Report" 
                                        Width="220px" onclick="exceptionRerptBtn_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="userRerptBtn" runat="server" Text="System Generated User List" 
                                        Width="220px" onclick="userRerptBtn_Click"  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="systemMatrixRerptBtn" runat="server" 
                                        Text="System Generated Security Matrix" Width="220px" 
                                        onclick="systemMatrixRerptBtn_Click"  />
                                </td>
                            </tr>--%>
                         
                            <tr>
                                <td>
                                    <%--<asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                    <%--                                            <asp:Button ID="activityReportButton" runat="server" Text="Activity Report" Width="220px"
                                                OnClick="activityReportButton_Click" />
--%>
                                    <%-- </ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td style="width: 80%; height: 450px;" align="center">
                    <fieldset>
                        <legend>Report Filter</legend>
                        <table>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lLIdLabel" runat="server" Text="Loan Locator Id:" Font-Bold="true"
                                        Font-Size="16px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="lLIdTextBox" runat="server" Width="180px" MaxLength="9"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%--<asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Always">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="activityReportButton" EventName="Click" />
                                        </Triggers>
                                        <ContentTemplate>--%>
                                    <div id="activityReportDiv" runat="server" visible="false">
                                        <%--<table width="100%">
                                                    <tr align="left">
                                                        <td>
                                                            From Date :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="dateTextBox1" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td>
                                                            To Date :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="dateTextBox2" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="showButton" runat="server" OnClick="showButton_Click" Text="Show" />
                                                        </td>
                                                    </tr>
                                                </table>--%>
                                    </div>
                                    <%-- </ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="width: 20%; height: 40px;">
                    &nbsp;
                </td>
                <td style="width: 80%; height: 40px;">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
