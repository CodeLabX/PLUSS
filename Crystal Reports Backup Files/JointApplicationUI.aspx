﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.LoanApplication.UI_JointApplicationUI" Title="Joint Application Information" CodeBehind="JointApplicationUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" TagPrefix="uc1" TagName="TextBoxDatePicker" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <div class="form-style-2 container-fluid">


        <div class="form-style-2-heading">Joint Application</div>
        <div class="container is-fluid">


            <table width="90%" border="0" cellspacing="6" cellpadding="2">
                <tr>
                    <td style="font-weight: bold; width: 200px">
                        <asp:Label ID="LLIDLabel" Text="LLID" runat="server"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox Width="260px" CssClass="input-field" ID="LLIDTextBox" runat="server" MaxLength="9"></asp:TextBox>
                        <asp:Button CssClass="btn primarybtn" ID="findButtonTextBox" Text="Search" runat="server" OnClick="findButtonTextBox_Click"
                            OnClientClick="return LLIdIsEmpty();" />
                        <asp:Label ID="errorMsgLabel" runat="server" ForeColor="#FF3300" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 200px">
                        <asp:Label ID="jointApplicantsNameLabel" Text="Joint Applicant's Name" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox Width="260px" CssClass="input-field" ID="jointApplicantsNameTextBox1" runat="server"></asp:TextBox>
                    </td>
                    <td style="font-weight: bold">
                        <asp:Label ID="dobLabel" Text="Date of Birth" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox Width="260px" CssClass="input-field" ID="JDOBTextBox1" runat="server" MaxLength="10"></asp:TextBox>
                        &nbsp;(dd-mm-yyyy)
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="jointApplicantFatherLabel" Text="Father's Name" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox Width="260px" CssClass="input-field" ID="jointApplicantFatherTextBox1" runat="server"></asp:TextBox>
                    </td>
                    <td style="font-weight: bold">
                        <asp:Label ID="jointApplicantMotherLabel" Text="Mother's Name" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox CssClass="input-field" ID="jointApplicantMotherTextBox1" runat="server" Width="260px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td style="font-weight: bold" valign="top">
                        <asp:Label ID="jointAcNoLabel" Text="A/C No. for SCB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <table>
                            <tr style="width: 280px">
                                <td>
                                    <asp:GridView ID="jointApplicantaccNoGridView1" runat="server" Width="268px" AutoGenerateColumns="False"
                                        class="style1" PageSize="5" CssClass="myGridStyle">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:TextBox CssClass="input-field" ID="jointAcNoSCBPreTextBox1" runat="server" Width="80px" MaxLength="2"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="A/C No.">
                                                <ItemTemplate>
                                                    <asp:TextBox CssClass="input-field" ID="jointAcNoSCBMasterTextBox1" runat="server" Width="140px" MaxLength="7"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:TextBox CssClass="input-field" ID="jointAcNoSCBPostTextBox1" runat="server" Width="60px" MaxLength="2"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>
                                </td>
                                <td valign="bottom">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-weight: bold" valign="top">
                        <asp:Label ID="jointCreditCardLabel" Text="Card Account No." runat="server"></asp:Label>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:GridView ID="jointCreditCardGridView1" runat="server" Width="400px" AutoGenerateColumns="False"
                                        class="style1" PageSize="5" CssClass="myGridStyle">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Card Account No.">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="jointCardNoTextBox1" runat="server" Width="260px" CssClass="input-field"
                                                        MaxLength="15"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>
                                </td>
                                <td valign="bottom">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="jointTinNumberLabel" Text="TIN No." runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox CssClass="input-field" ID="jTinPreTextBox1" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
                        <asp:TextBox CssClass="input-field" ID="jTinMasterTextBox1" runat="server" Width="30px" MaxLength="3" Visible="false"></asp:TextBox>
                        <asp:TextBox CssClass="input-field" ID="jTinPostTextBox1" runat="server" Width="40px" MaxLength="4" Visible="false"></asp:TextBox>
                    </td> 
                    <td style="font-weight: bold">
                        <asp:Label ID="jointEducationalLabel" Text="Highest Educational Level" runat="server"></asp:Label>
                    </td>
                    <td>
                        <div class="select is-small is-margin-5">
                            <asp:DropDownList CssClass="select-field" ID="jointEducationalDropDownList1" runat="server" Width="100px">
                            </asp:DropDownList>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold" valign="top">
                        <asp:Label ID="jointcontactLabel" Text="Contact No." runat="server"></asp:Label>
                    </td>
                    <td valign="top">
                        <table>
                            <tr>
                                <td>
                                    <asp:GridView ID="jointcontactGridView1" runat="server" Width="320px" AutoGenerateColumns="False"
                                        class="style1" PageSize="4" CssClass="myGridStyle">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                </ItemTemplate>
                                                <ItemStyle Width="80px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Number">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="jointContactNoTextBox1" runat="server" Width="200px" CssClass="input-field"
                                                        MaxLength="15"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                <td valign="top">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-weight: bold" valign="top" rowspan="2">
                        <asp:Label ID="jointIdLabel" Text="ID Info." runat="server"></asp:Label>
                    </td>
                    <td valign="top" rowspan="2">
                        <table>
                            <tr style="width: 400px">
                                <td>
                                    <asp:GridView ID="jointIdGridView1" runat="server" Width="400px" AutoGenerateColumns="False"
                                        class="style1" PageSize="5" CssClass="myGridStyle">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID Type">
                                                <ItemTemplate>
                                                    <div class="select is-small is-margin-5">
                                                        <asp:DropDownList CssClass="select-field" ID="jointIdTypeDropDownList1" runat="server" Width="100px">
                                                        </asp:DropDownList>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ID No.">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="jointIdNoTextBox1" runat="server" Width="150px" CssClass="input-field"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="jointIdexpiryDateTextBox1" runat="server" Width="100px" CssClass="input-field"
                                                        MaxLength="10"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                <td valign="bottom">&nbsp&nbsp&nbsp&nbsp
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="jointCompanyNameLabel" Text="Name of Company" runat="server"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox CssClass="input-field" ID="jointCompanyNameTextBox1" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
            
            </table>
            <br />


            <div class="collapsible-btn">Joint Applicant-2</div>
            <div class="collapsible-content">
                <br />
                <table  border="0">
                    <tr>
                        <td style="font-weight: bold; width: 200px">
                            <asp:Label ID="Label1" Text="Joint Applicant's Name" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppNameTextBox2"  Width="260px" runat="server"></asp:TextBox>
                        </td>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label7" Text="Date of Birth" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppDOBTextBox2" runat="server" Width="260px" MaxLength="10"></asp:TextBox>
                            &nbsp;(dd-mm-yyyy)
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label5" Text="Father's Name" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppFatherNameTextBox2" runat="server"  Width="260px"></asp:TextBox>
                        </td>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label6" Text="Mother's Name" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppMotherNameTextBox2" runat="server"  Width="260px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label2" Text="A/C No. for SCB" runat="server"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr style="width: 280px">
                                    <td>
                                        <asp:GridView ID="jAppSCBAccGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccPreTextBox2" runat="server" Width="60px"
                                                            MaxLength="2"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="A/C No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccMasterTextBox2" runat="server" Width="140px" MaxLength="7"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccPostTextBox2" runat="server" Width="60px" MaxLength="2"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--<HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                   
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label4" Text="Card Account No." runat="server"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="jCrdtCrdGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Card Account No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppCrdtCrdTextBox2" runat="server" Width="260px" MaxLength="15"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--<HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label3" Text="TIN No." runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppTinPreTextBox2" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                            <asp:TextBox CssClass="input-field" ID="jAppTinMasterTextBox2" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                            <asp:TextBox CssClass="input-field" ID="jAppTinPostTextBox2" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label8" Text="Highest Educational Level" runat="server"></asp:Label>
                        </td>
                        <td>
                            <div class="select is-small is-margin-5">

                                <asp:DropDownList CssClass="select-field" ID="jAppEducationDropDownList2" runat="server" Width="100px">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label9" Text="Contact No." runat="server"></asp:Label>
                        </td>
                        <td  valign="top">
                            <table>
                                <tr >
                                    <td valign="top">
                                        <asp:GridView ID="jAppContactGridView2" runat="server" Width="288px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Number">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppContactTextBox2" runat="server" Width="190px"
                                                            MaxLength="15"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                         </asp:GridView>
                                    </td>
                                    <td valign="top">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    
                        <td style="font-weight: bold" valign="top" rowspan="2">
                            <asp:Label ID="Label11" Text="ID Info." runat="server"></asp:Label>
                        </td>
                        <td rowspan="2">
                            <table>
                                <tr style="width: 280px">
                                    <td>
                                        <asp:GridView ID="jAppIdGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID Type">
                                                    <ItemTemplate>
                                                        <div class="select is-small is-margin-5">
                                                            <asp:DropDownList CssClass="select-field" ID="jAppIdTypeDropDownList2" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ID No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppIdNoTextBox2" runat="server" Width="150px"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppIdExpiryDateTextBox2" runat="server" Width="100px"
                                                            MaxLength="10"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--                                    <HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp&nbsp&nbsp&nbsp
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label10" Text="Name of Company" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppCompanyTextBox2" runat="server" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            <br />
            </div>
            

            <div class="collapsible-btn">Joint Applicant-3</div>
            <div class="collapsible-content">
                <br />
                <table border="0">
                    <tr>
                        <td style="font-weight: bold; width: 200px">
                            <asp:Label ID="Label12" Text="Joint Applicant's Name" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppNameTextBox3" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label18" Text="Date of Birth" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppDOBTextBox3" runat="server" Width="260px" MaxLength="10"></asp:TextBox>
                            &nbsp;(dd-mm-yyyy)
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label16" Text="Father's Name" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppFatherNameTextBox3" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label17" Text="Mother's Name" runat="server"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox CssClass="input-field" ID="jAppMotherNameTextBox3" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label13" Text="A/C No. for SCB" runat="server"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr style="width: 280px">
                                    <td>
                                        <asp:GridView ID="jAppSCBAccGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccPreTextBox3" runat="server" Width="60px"
                                                            MaxLength="2"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="A/C No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccMasterTextBox3" runat="server" Width="140px"
                                                            MaxLength="7"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccPostTextBox3" runat="server" Width="60px"
                                                            MaxLength="2"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                   
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label15" Text="Card Account No." runat="server"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="jAppCrdtCrdGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Card Account No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppCrdtCrdTextBox3" runat="server" Width="260px" MaxLength="15"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--                                    <HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label14" Text="TIN No." runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppTinPreTextBox3" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                            <asp:TextBox CssClass="input-field" ID="jAppTinMasterTextBox3" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                            <asp:TextBox CssClass="input-field" ID="jAppTinPostTextBox3" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label19" Text="Highest Educational Level" runat="server"></asp:Label>
                        </td>
                        <td>
                            <div class="select is-small is-margin-5">
                                <asp:DropDownList CssClass="select-field" ID="jAppEducationDropDownList3" runat="server" Width="100px">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label20" Text="Contact No." runat="server"></asp:Label>
                        </td>
                        <td valign="top">
                            <table style="width: 280px">
                                <tr>
                                    <td>
                                        <asp:GridView ID="jAppContactGridView3" runat="server" Width="288px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField >
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Number">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppContactTextBox3" runat="server" Width="190px"
                                                            MaxLength="15"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--<HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="top">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    

                        <td style="font-weight: bold" valign="top" rowspan="2">
                            <asp:Label ID="Label22" Text="ID Info." runat="server"></asp:Label>
                        </td>
                        <td rowspan="2">
                            <table>
                                <tr style="width: 280px">
                                    <td>
                                        <asp:GridView ID="jAppIdGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID Type">
                                                    <ItemTemplate>
                                                        <div class="select is-small is-margin-5">

                                                            <asp:DropDownList CssClass="select-field" ID="jAppIdTypeTextBox3" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ID No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppIdTextBox3" runat="server" Width="100px"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppIdExpiryDateTextBox3" runat="server" Width="100px"
                                                            MaxLength="10"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp&nbsp&nbsp&nbsp
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                
                    <tr>
                        <td style="font-weight: bold" >
                            <asp:Label ID="Label21" Text="Name of Company" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppCompanyTextBox3" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            
            
            <div class="collapsible-btn">Joint Applicant-4</div>
            <div class="collapsible-content">
                <br />
                <table border="0">
                    <tr>
                        <td style="font-weight: bold; width: 200px">
                            <asp:Label ID="Label23" Text="Joint Applicant's Name" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppNameTextBox4" Width="260px" runat="server"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label29" Text="Date of Birth" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppDOBTextBox4" runat="server" Width="260px" MaxLength="10"></asp:TextBox>
                            &nbsp;(dd-mm-yyyy)
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label27" Text="Father's Name" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppFatherNameTextBox4" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label28" Text="Mother's Name" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppMotherNameTextBox4" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label24" Text="A/C No. for SCB" runat="server"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr style="width: 280px">
                                    <td>
                                        <asp:GridView ID="jSCBAccGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccPreTextBox4" runat="server" Width="60px"
                                                            MaxLength="2"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="A/C No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccMasterTextBox4" runat="server" Width="140px"
                                                            MaxLength="7"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppSCBAccPostTextBox4" runat="server" Width="60px"
                                                            MaxLength="2"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--                                    <HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                   
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label26" Text="Card Account No." runat="server"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="jAppCrdtCrdGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Card Account No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppCrdtCrdTextBox4" runat="server" Width="260px"
                                                            MaxLength="15"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--                                    <HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label25" Text="TIN No." runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="jAppTinPreTextBox4" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                            <asp:TextBox CssClass="input-field" ID="jAppTinMasterTextBox4" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                            <asp:TextBox CssClass="input-field" ID="jAppTinPostTextBox4" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                        </td>
                    
                        <td style="font-weight: bold">
                            <asp:Label ID="Label30" Text="Highest Educational Level" runat="server"></asp:Label>
                        </td>
                        <td>
                            <div class="select is-small is-margin-5">
                                <asp:DropDownList CssClass="select-field" ID="jAppEducationDropDownList4" runat="server" Width="100px">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold" valign="top">
                            <asp:Label ID="Label31" Text="Contact No." runat="server"></asp:Label>
                        </td>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="jAppContactGridView4" runat="server" Width="288px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Number">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppContactTextBox4" runat="server" Width="200px"
                                                            MaxLength="15"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--                                    <HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="top">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    
                        <td style="font-weight: bold" valign="top" rowspan="2">
                            <asp:Label ID="Label33" Text="ID Info." runat="server"></asp:Label>
                        </td>
                        <td rowspan="2">
                            <table>
                                <tr style="width: 280px">
                                    <td>
                                        <asp:GridView ID="jAppIdGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                            class="style1" PageSize="5" CssClass="myGridStyle">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID Type">
                                                    <ItemTemplate>
                                                        <div class="select is-small is-margin-5">
                                                            <asp:DropDownList CssClass="select-field" ID="jAppIdTypeTextBox4" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ID No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppIdNoTextBox4" runat="server" Width="100px"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="input-field" ID="jAppIdExpiryDateTextBox4" runat="server" Width="100px"
                                                            MaxLength="10"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--                                    <HeaderStyle CssClass="ssHeader" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                        </asp:GridView>
                                    </td>
                                    <td valign="bottom">&nbsp&nbsp&nbsp&nbsp
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="Label32" Text="Name of Company" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:TextBox CssClass="input-field" ID="jAppCompanyTextBox4" runat="server" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            <br />
            <asp:Button CssClass="btn primarybtn" ID="saveButton" Text="Save" runat="server" Width="95px"
                Height="30px" OnClick="saveButton_Click" OnClientClick="return LLIdIsEmpty();" />
            &nbsp;<asp:Button CssClass="btn clearbtn" ID="clearButton" Text="Clear" runat="server" Width="95px"
                Height="30px" OnClick="clearButton_Click" />

            <script type="text/javascript">
                var coll = document.getElementsByClassName("collapsible-btn");
                var i;

                for (i = 0; i < coll.length; i++) {
                    coll[i].addEventListener("click", function () {
                        this.classList.toggle("collapsible-active");
                        var content = this.nextElementSibling;
                        if (content.style.maxHeight) {
                            content.style.maxHeight = null;
                        } else {
                            content.style.maxHeight = content.scrollHeight + "px";
                        }
                        return false;
                    });
                }

                function LLIdIsEmpty() {
                    var value1 = document.getElementById('ctl00_ContentPlaceHolder_LLIDTextBox').value;
                    if (value1.length == 0 || value1 == '') {
                        document.getElementById('ctl00_ContentPlaceHolder_errorMsgLabel').innerHTML = "Please Enter LLID!";
                        document.getElementById('ctl00_ContentPlaceHolder_LLIDTextBox').focus();
                        return false;
                    }
                    else {
                        document.getElementById('ctl00_ContentPlaceHolder_errorMsgLabel').innerHTML = '';
                        return true;
                    }
                }
            </script>
        </div>
    </div>
</asp:Content>
