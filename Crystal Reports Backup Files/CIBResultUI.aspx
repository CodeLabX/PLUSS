﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_CIBResult" Codebehind="CIBResultUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/spreadsheet.ascx" TagPrefix="uc1" TagName="spreadsheet" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CIB Result</title>
    <uc1:spreadsheet runat="server" ID="spreadsheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr valign="top">
            <td>
                <div>
                    <asp:GridView ID="cibResultGridView" runat="server" CellPadding="4" 
                        ForeColor="#333333" Width="100%" AutoGenerateColumns="False" Font-Size="11px">
                        <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="LLID" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="llidLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="5%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Name" HeaderStyle-Width="18%">
                                <ItemTemplate>
                                    <asp:Label ID="customerNameLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="18%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="statusLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="remarksLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Father's Name" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="fatherNameLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mother's Name" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="motherNameLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reference No." HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="referenceNoLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <asp:Label ID="typeLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="7%"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                         <EmptyDataTemplate>
                                &nbsp;&nbsp;No data found.            
                        </EmptyDataTemplate>  
                        <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed"/>
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle CssClass="ssHeader" />
                        <EditRowStyle BackColor="#999999" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
