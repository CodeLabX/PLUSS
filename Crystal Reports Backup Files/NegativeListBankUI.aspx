﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.UI_NegativeListBank" Codebehind="NegativeListBankUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/spreadsheet.ascx" TagPrefix="uc1" TagName="spreadsheet" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Negative List Bank/Company</title>
    <uc1:spreadsheet runat="server" ID="spreadsheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr valign="top">
            <td>
                <div>
                    <asp:GridView ID="negativeListBankGridView" runat="server" 
                        AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                        PageSize="15" GridLines="Both" Width="100%" Font-Size="11px">
                        <RowStyle BackColor="#FFFFFF" ForeColor="#333333" BorderStyle="Solid" />
                        <Columns>
                            <asp:TemplateField HeaderText="Bank/Company" HeaderStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="bankCompanyLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="150px"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch" HeaderStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="branchLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="150px"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Status" HeaderStyle-Width="120">
                                <ItemTemplate>
                                    <asp:Label ID="lastStatusLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="120px"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Update Date" HeaderStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Label ID="updateDateLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="100px"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Present Status" HeaderStyle-Width="120">
                                <ItemTemplate>
                                    <asp:Label ID="presentStatusLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="120px"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="remarksLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="150px"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                         <EmptyDataTemplate>
                                &nbsp;&nbsp;No data found.            
                        </EmptyDataTemplate>  
                        <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed"/>
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle CssClass="ssHeader" />
                        <EditRowStyle BackColor="#999999" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
