﻿<%@ Page Language="C#" MasterPageFile="~/UI/AnalystMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_DalyActivityReportUI" Title="Daily Underwriting Activity Report" Codebehind="DalyActivityReportUI.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .tableWidth
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
        .tableWidth td
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
        .line_bottom
         {
	        BORDER-BOTTOM: #202266 1px solid;
         }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<center>
        <table width="100%" border="0" >
            <tr>
                <td colspan="4" class="line_bottom">
                    <asp:Label runat="server" ID="formNameLabel" Text="Daily Underwriting Activity Report" Font-Bold="true" Font-Names="Arial" Font-Size="16px" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width:25%" align="left">                    
                <asp:Label runat="server" ID="Label1" Text="Recived & Re Received" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                </td>
                <td style="width:25%" align="left">
                <asp:Label runat="server" ID="Label2" Text="Pipe line & Pending" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                    
                </td>
                <td style="width:25%" align="left">
                <asp:Label runat="server" ID="Label3" Text="Process" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                    
                </td>
                <td style="width:25%" align="left" >
                <asp:Label runat="server" ID="Label4" Text="Analyst wise process" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                    
                </td>
            </tr>
            <tr>
                <td style="width:25%">
                 <div>                        
                    <asp:GridView ID="receivedGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" OnRowDataBound="receivedGridView_RowDataBound" 
                        ShowFooter="True">
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Date" HeaderText="Date" HeaderStyle-CssClass="ssHeader" SortExpression="Date" DataFormatString="{0:dd-MMM-yy}" />
                            <asp:BoundField DataField="RcFL" HeaderText="FL" HeaderStyle-CssClass="ssHeader"  SortExpression="FL" />
                            <asp:BoundField DataField="RcPLSAL" HeaderText="PL-SAL" HeaderStyle-CssClass="ssHeader"  SortExpression="PL-SAL" />
                            <asp:BoundField DataField="RcPLBIZ" HeaderText="PL-BIZ" HeaderStyle-CssClass="ssHeader"  SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="RcIPF" HeaderText="IPF" HeaderStyle-CssClass="ssHeader"  SortExpression="IPF" />
                            <asp:BoundField DataField="RcSTFL" HeaderText="STFL" HeaderStyle-CssClass="ssHeader"  SortExpression="STFL" />
                            <asp:BoundField DataField="ReTotal" HeaderText="TOTAL" HeaderStyle-CssClass="ssHeader"  SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
                <td style="width:25%">
                <div>                        
                    <asp:GridView ID="pendingGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" ShowFooter="True" OnRowDataBound="pendingGridView_RowDataBound">
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Date" HeaderText="Date" HeaderStyle-CssClass="ssHeader" SortExpression="Date" DataFormatString="{0:dd-MMM-yy}" />
                            <asp:BoundField DataField="PeFL" HeaderText="FL" HeaderStyle-CssClass="ssHeader" SortExpression="FL" />
                            <asp:BoundField DataField="PePLSAL" HeaderText="PL-SAL" HeaderStyle-CssClass="ssHeader" SortExpression="PL-SAL" />
                            <asp:BoundField DataField="PePLBIZ" HeaderText="PL-BIZ" HeaderStyle-CssClass="ssHeader" SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="PeIPF" HeaderText="IPF" HeaderStyle-CssClass="ssHeader" SortExpression="IPF" />
                            <asp:BoundField DataField="PeSTFL" HeaderText="STFL" HeaderStyle-CssClass="ssHeader" SortExpression="STFL" />
                            <asp:BoundField DataField="PeTotal" HeaderText="TOTAL" HeaderStyle-CssClass="ssHeader" SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
                <td style="width:25%">
                    <div>
                    <asp:GridView ID="processGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" ShowFooter="True" OnRowDataBound="processGridView_RowDataBound">
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Date" HeaderText="Date" HeaderStyle-CssClass="ssHeader" SortExpression="Date" DataFormatString="{0:dd-MMM-yy}" />
                            <asp:BoundField DataField="PoFL" HeaderText="FL" HeaderStyle-CssClass="ssHeader" SortExpression="FL" />
                            <asp:BoundField DataField="PoPLSAL" HeaderText="PL-SAL" HeaderStyle-CssClass="ssHeader" SortExpression="PL-SAL" />
                            <asp:BoundField DataField="PoPLBIZ" HeaderText="PL-BIZ" HeaderStyle-CssClass="ssHeader" SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="PoIPF" HeaderText="IPF" HeaderStyle-CssClass="ssHeader" SortExpression="IPF" />
                            <asp:BoundField DataField="PoSTFL" HeaderText="STFL" HeaderStyle-CssClass="ssHeader" SortExpression="STFL" />
                            <asp:BoundField DataField="PoTotal" HeaderText="TOTAL" HeaderStyle-CssClass="ssHeader" SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
                <td style="width:25%" valign="top" align="left" rowspan="5">
                    <div>
                    <asp:GridView ID="userProcessGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" ShowFooter="True" OnRowDataBound="userProcessGridView_RowDataBound"> 
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-CssClass="ssHeader" SortExpression="Name" />
                            <asp:BoundField DataField="PoFL" HeaderText="FL" HeaderStyle-CssClass="ssHeader"  SortExpression="FL" />
                            <asp:BoundField DataField="PoPLSAL" HeaderText="PL-SAL" HeaderStyle-CssClass="ssHeader"  SortExpression="PL-SAL" />
                            <asp:BoundField DataField="PoPLBIZ" HeaderText="PL-BIZ" HeaderStyle-CssClass="ssHeader"  SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="PoIPF" HeaderText="IPF" HeaderStyle-CssClass="ssHeader"  SortExpression="IPF" />
                            <asp:BoundField DataField="PoSTFL" HeaderText="STFL" HeaderStyle-CssClass="ssHeader"  SortExpression="STFL" />
                            <asp:BoundField DataField="PoTotal" HeaderText="TOTAL" HeaderStyle-CssClass="ssHeader" SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="part1label" runat="server" Text="Daily No. of approved & Volume ( Sent to assetops)**"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <tr align="left">
                    <td style="width:17%;" >
                    &nbsp;
                    </td>
                    <td style="width:26%;" >
                    <asp:Label runat="server" ID="Label5" Text="LWD" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                    </td>
                    <td style="width:25%;" >
                    <asp:Label runat="server" ID="Label6" Text="MTD" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                    </td>
                    <td style="width:32%;" >
                    <asp:Label runat="server" ID="Label7" Text="YTD" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4" align="left">
                    <div>                        
                    <asp:GridView ID="dailyNoofApprovedVolumeGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" OnRowDataBound="dailyNoofApprovedVolumeGridView_RowDataBound" 
                        ShowFooter="True">
                        <FooterStyle Font-Bold="True" HorizontalAlign="Right" />
                        <Columns>
                            <asp:BoundField DataField="ProductName" HeaderText="Product" 
                                HeaderStyle-CssClass="ssHeader" SortExpression="Product" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDQuantity" HeaderText="No of Approved" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="No of Approved" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDVolume" HeaderText="Approved volume" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="Approved volume" DataFormatString="{0:N}" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDQuantity" HeaderText="No of Approved" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="No of Approved" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDVolume" HeaderText="Approved volume" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="Approved volume" DataFormatString="{0:N}" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YTDQuantity" HeaderText="No of Approved" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="No of Approved"  >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YTDVolume" HeaderText="Approved volume" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="Approved volume" DataFormatString="{0:N}" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
                    </td>
                    </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <table width="100%" border="0" cellpadding="1" cellspacing="1"> 
                <tr>
                <td style="width:63%">
                <asp:Label runat="server" ID="Label8" Text="Direct Sales" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                
                </td>
                <td style="width:37%" align="left">
                <asp:Label runat="server" ID="Label9" Text="Shared Distribution" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                 
                </td>
                </tr>
                </table>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="left">
                <div>
                <asp:GridView ID="channelWiseGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" OnRowDataBound="channelWiseGridView_RowDataBound" 
                        ShowFooter="True">
                        <FooterStyle Font-Bold="True" HorizontalAlign="Right" />
                        <Columns>
                            <asp:BoundField DataField="ProductName" HeaderText="Product" 
                                HeaderStyle-CssClass="ssHeader" SortExpression="Product" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDQuantity" HeaderText="No of Approved" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="No of Approved" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDVolume" HeaderText="Approved volume" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="Approved volume" DataFormatString="{0:N}" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDQuantity" HeaderText="No of Approved" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="No of Approved" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDVolume" HeaderText="Approved volume" 
                                HeaderStyle-CssClass="ssHeader"  SortExpression="Approved volume" DataFormatString="{0:N}" >
<HeaderStyle CssClass="ssHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>                          
                        </Columns>
                    </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <input id="printButton" type="button" value="Print" onclick="OpenReport()" style="width: 150px; height: 28px;" />
                 </td>
            </tr>
        </table>
    </center>
    <script type="text/javascript">
    function OpenReport()
    {
        window.open('../Reports/ActivityReport.aspx','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=1024,height=768');
    }
    </script>
</asp:Content>

