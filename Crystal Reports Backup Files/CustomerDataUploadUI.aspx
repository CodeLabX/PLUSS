﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.CustomerDataUploadUI"
    Title="Customer DataUpload" CodeBehind="CustomerDataUploadUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/azGridTableCss.ascx" TagPrefix="uc1" TagName="azGridTableCss" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <uc1:azGridTableCss runat="server" ID="azGridTableCss" />
    <modal:modalPopup runat="server" ID="modalPopup" />

    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Customers Bulk Data Upload</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:FileUpload CssClass="input-field" ID="FileUpload" runat="server" Width="350px" Height="32px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="btn primarybtn" ID="UploadButton" runat="server" Text="Upload" Width="100px"
                        Height="35px" OnClick="uploadButton_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="btn clearbtn" ID="btnFlush" runat="server" Text="FLUSH" Width="100px"
                        Height="35px" OnClick="btnFlush_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <h3>Data Previously Uploaded</h3>
                    <asp:GridView ID="gvRecords" runat="server" GridLines="None" CssClass="myGridStyle"
                        Font-Size="11px" AutoGenerateColumns="False">
                        <Columns>
                            <%--<asp:BoundField DataField="ID" HeaderText="ID" />--%>
                            <asp:BoundField DataField="RefType" HeaderText="REF TYPE" />
                            <asp:BoundField DataField="RefrenceID" HeaderText="REFRENCE ID" />
                            <asp:BoundField DataField="MasterNo" HeaderText="MASTER NO" />
                            <asp:BoundField DataField="RelationshipNo" HeaderText="REL NO" />
                            <asp:BoundField DataField="FullName" HeaderText="FULL NAME" />
                            <asp:BoundField DataField="FatherName" HeaderText="FATHER NAME" />
                            <asp:BoundField DataField="MotherName" HeaderText="MOTHER NAME" />
                            <asp:BoundField DataField="DOB" HeaderText="DOB" />
                            <asp:BoundField DataField="HighestEduLevel" HeaderText="HIGHEST EDUCATION LEVEL" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        function JS_FunctionClear() {
            mygrid.clearAll();
        }
    </script>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
