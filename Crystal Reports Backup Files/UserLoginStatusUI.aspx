﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_UserLoginStatusUI" Title="User Login Status" Codebehind="UserLoginStatusUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/PopupWindows.ascx" TagPrefix="uc1" TagName="PopupWindows" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgridcss.ascx" TagPrefix="uc1" TagName="dhtmlxgridcss" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_skinscss.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_skinscss" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid.ascx" TagPrefix="uc1" TagName="dhtmlxgrid" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_srnd.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_srnd" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgridcell.ascx" TagPrefix="uc1" TagName="dhtmlxgridcell" %>
<%@ Register Src="~/UI/UserControls/js/PopupWindowsJs.ascx" TagPrefix="uc1" TagName="PopupWindowsJs" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_excell_link.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_excell_link" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
    <uc1:PopupWindows runat="server" ID="PopupWindows" />
    <uc1:dhtmlxgridcss runat="server" ID="dhtmlxgridcss" />
    <uc1:dhtmlxgrid_skinscss runat="server" ID="dhtmlxgrid_skinscss" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc1:dhtmlxgrid runat="server" ID="dhtmlxgrid" />
    <uc1:dhtmlxgrid_srnd runat="server" ID="dhtmlxgrid_srnd" />
    <uc1:dhtmlxgridcell runat="server" ID="dhtmlxgridcell" />
    <uc1:PopupWindowsJs runat="server" ID="PopupWindowsJs" />
    <uc1:dhtmlxgrid_excell_link runat="server" ID="dhtmlxgrid_excell_link" />   

</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
 <div id="dataEntryMainDiv">
        <table width="100%" border="0" cellpadding="1" cellspacing="1">
            <tr>
                <td align="center">
                    <asp:Label runat="server" ID="formNameLabe" Text="User Login Status" Font-Bold="true" Font-Size="16px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">                    
                    <asp:Label runat="server" ID="userLoginIdSearchLabel" Text="User Login Id :" Font-Bold="true" Font-Size="12px"></asp:Label>                    
                    <asp:TextBox runat="server" ID="searchTextBox" Width="200px"></asp:TextBox>
                    <input id="searchButton" type="button" value="Search" onclick="DataSearch();" />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="dataEntryList" style="background-color: white; width: 800px; height: 500px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="popupWinDiv" style="display:none; position: absolute; top:85px; left:300px; width:250px; border: solid 1px gray;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
                            align="center">
                            <tr bgcolor="#3DAE38">
                                <td>
                                <asp:Label runat="server" ID="formNameLabel" Text="User Login status" Font-Bold="true" Font-Size="14px" ForeColor="White"></asp:Label>
                                </td>
                                <td align="right">
                                    <img src="../Images/Close.png" alt="" width="15" height="15" style="cursor: pointer;
                                        cursor: hand;" onclick="closeDialog('popupWinDiv')" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="overflow-y: scroll; width: 500px; height: 150px; z-index: 5000; border: solid 1px gray;"
                                        visible="true">
                                        <table width="100%" border="0" cellpadding="1" cellspacing="1">
                                        <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                        <td><asp:Label runat="server" ID="branchIdLabel" Text=""></asp:Label></td>
                                        <td><asp:HiddenField ID="accessLogIdHiddenField" runat="server" /></td>
                                        </tr>
                                        <tr>
                                        <td align="right"><asp:Label runat="server" ID="userLogInIdLabel" Text="User Login Id:"></asp:Label></td>
                                        <td><asp:TextBox runat="server" ID="userLogInIdTextBox" Width="180px" ReadOnly="true"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                        <td align="right"><asp:Label runat="server" ID="userIPLabel" Text="IP:"></asp:Label></td>
                                        <td><asp:TextBox runat="server" ID="ipTextBox" Width="180px" ReadOnly="true"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                        <td align="right"><asp:Label runat="server" ID="Label1" Text="Login Datetime:"></asp:Label></td>
                                        <td><asp:TextBox runat="server" ID="loginDatetimeTextBox" Width="180px" ReadOnly="true"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                        <td align="right"><asp:Label runat="server" ID="statusLabel" Text="Status:" Font-Bold="true"></asp:Label></td>
                                        <td><asp:TextBox runat="server" ID="statusTextBox" Width="180px" ReadOnly="true" Font-Bold="true"></asp:TextBox></td>
                                        </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <div style="padding-left: 140px;">
                                        <asp:Button ID="saveButton" runat="server" Text="Logout" Width="85px" 
                                            Height="30px" onclick="saveButton_Click"  />
                                        <input id="cancleButton1" type="button" value="Cancel" style="Width:85px; height:30px" onclick="closeDialog('popupWinDiv');" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
      <div style="display: none" runat="server" id="dvXml"></div>
    </div>
<script type="text/javascript">
    var mygrid;
    mygrid = new dhtmlXGridObject('dataEntryList');
    debugger;
	    function doOnLoad() { 
	    mygrid.setImagePath("../codebase/imgs/");
	    mygrid.setHeader("SL,User Login Id,User IP,Login DateTime,Status,Action");
	    mygrid.setInitWidths("50,150,180,200,100,100");
	    mygrid.setColAlign("right,left,left,left,left,left");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,link");
	    mygrid.setColSorting("int,int,str,str,str,str");
	    mygrid.setSkin("light");	 
	    mygrid.init();
	    mygrid.enableSmartRendering(true, 20);
	    mygrid.parse(document.getElementById("xml_data"));
	   
	    mygrid.attachEvent("onRowDblClicked",LoadUserPopUp);
	    }
	    function DataSearch()
	    {
	      
            mygrid.clearAll();
          
	    }	    
	    function LoadUserPopUp(accessLogId)
	    {
	        var loginStatus;
	        if(accessLogId > 0)
	        {
	            document.getElementById('ctl00_ContentPlaceHolder_accessLogIdHiddenField').value=accessLogId;
                document.getElementById('ctl00_ContentPlaceHolder_userLogInIdTextBox').value=mygrid.cells(accessLogId,1).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_ipTextBox').value=mygrid.cells(accessLogId,2).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_loginDatetimeTextBox').value=mygrid.cells(accessLogId,3).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_statusTextBox').value=mygrid.cells(accessLogId,4).getValue();
	        }
	        else
	        {
	            document.getElementById('ctl00_ContentPlaceHolder_accessLogIdHiddenField').value="";
                document.getElementById('ctl00_ContentPlaceHolder_userLogInIdTextBox').value="";
                document.getElementById('ctl00_ContentPlaceHolder_ipTextBox').value="";
                document.getElementById('ctl00_ContentPlaceHolder_loginDatetimeTextBox').value="";
	        }
	        ShowPopUpWindows('popupWinDiv',mainBody);
	    }
    </script>
     <div onmouseup="return false" class="translucent" onmousemove="return false" onmousedown="return false"
        id="blockUI" ondblclick="return false" style="display: none; z-index: 50000;
        left: 0px; position: absolute; top: 0px; background-color: gray" onclick="return false">
    </div>
</asp:Content>

