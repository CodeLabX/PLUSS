﻿<%@ Page Language="C#" MasterPageFile="~/UI/AnalystMasterPage.master" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.UI_BankStatementCalculationUI" Title="Bank Statement Calculation" Codebehind="BankStatementCalculationUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/dhtmlxgridcss.ascx" TagPrefix="uc1" TagName="dhtmlxgridcss" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_skinscss.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_skinscss" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid.ascx" TagPrefix="uc1" TagName="dhtmlxgrid" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_srnd.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_srnd" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgridcell.ascx" TagPrefix="uc1" TagName="dhtmlxgridcell" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_excell_link.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_excell_link" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="Server">
    <uc1:dhtmlxgridcss runat="server" ID="dhtmlxgridcss" />
    <uc1:dhtmlxgrid_skinscss runat="server" ID="dhtmlxgrid_skinscss" />
    
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc1:dhtmlxgrid runat="server" ID="dhtmlxgrid" />
    <uc1:dhtmlxgrid_srnd runat="server" ID="dhtmlxgrid_srnd" />
    <uc1:dhtmlxgridcell runat="server" ID="dhtmlxgridcell" />
    <uc1:dhtmlxgrid_excell_link runat="server" ID="dhtmlxgrid_excell_link" />
   
   
    <style type="text/css">
         .TextBox
        {
            font-weight: bold;
            font-family: Arial;
            font-size: 12px;
        }
        .FontColor
        {
            color: White;
        }
        .style1
        {
            height: 377px;
        }
    </style>
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <center>
        <table width="100%" border="0" cellpadding="1" cellspacing="1">
            <tr>
                <td colspan="2">
                    <asp:Label runat="server" ID="Label2" Text="Bank Statement Calculation"></asp:Label>
                </td>
                
            </tr>
            <tr>
                <td align="left" style="width:20%;">
                    <asp:Label runat="server" ID="Label1" Text="LLId:"></asp:Label>
                    <asp:textbox id="LLIdTextBox" type="text" size="20%" MaxLength="9" runat="server"></asp:textbox> 
                    <input type="button" value="..." onclick="JS_FunctionLoadGrid();" />
                    <textarea id="str_xml" cols="0" rows="0" style="display:none;"></textarea>
                </td>
                <td align="left">
                    <div id="gridbox" style="background-color: white; width: 725px; height: 145px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                        <div id="calculationGrid" style="background-color: white; width: 920px; height: 293px;">
                        </div>                        
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" class="style1">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <tr>
                    <td style="width:35%;" valign="top">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <tr>
                    <td>
                    <div style="overflow-y: scroll; width: 100%; height: 137px; z-index: 2; border: solid 1px gray;" visible="true">
                        <asp:GridView ID="remarksGridView" runat="server" Width="345px" AutoGenerateColumns="False"
                            AllowPaging="False">
                            <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="IdCheckBox" runat="server"></asp:CheckBox>
                                        <asp:HiddenField ID="idTextBox" runat="server" Value='<%# Eval("Id") %>' ></asp:HiddenField>
                                    </ItemTemplate>
                                    <HeaderStyle Width="20px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="310px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="remarksTextBox" runat="server" Text='<%# Eval("Name") %>' Width="310px"
                                            CssClass="ssTextBox"></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle Width="310px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="FixedHeader" />
                        </asp:GridView>
                    </div>
                    </td>
                    </tr>
                    </table>
                    </td>
                    <td style="width:65%;">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <tr>
                    <td style="width:39%;"><asp:Label ID="Label3" runat="server" Text="Total-12 Month "></asp:Label></td>
                    <td style="width:15%;"><input id="total12MonthCreditTurnOverTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td style="width:7%;">&nbsp;</td>
                    <td style="width:15%;"><input id="totalAverageBalance12MonthTextBox" type="text" readonly="readonly" style="width:94px" /></td>
                    <td style="width:24%;">&nbsp;</td>
                    </tr>
                    <tr>
                    <td><asp:Label ID="Label4" runat="server" Text="Average-12 Months"></asp:Label></td>
                    <td><input id="average12MonthCreditTurnOverTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td><input id="checkBox12Month" type="checkbox" onclick="JS_FunctionCheckCreditTurnOver12()" /></td>
                    <td><input id="average12MonthAverageBalanceTextBox" type="text" readonly="readonly" style="width:94px" /></td>
                    <td><input id="maxBalDate12MonthTextBox" type="text" readonly="readonly" style="width:40px;" /></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><asp:Label ID="Label5" runat="server" Text="50% of Avg.Balance"></asp:Label></td>
                    <td><input id="averageBalance12Month50PerTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td><input id="avg12CheckBox" type="checkbox" onclick="JS_FunctionAvg12Month()" /></td>
                    </tr>
                    <tr>
                    <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                    <td><asp:Label ID="Label6" runat="server" Text="Total-6 Month"></asp:Label></td>
                    <td><input id="total6MonthCreditTurnOverTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td>&nbsp;</td>
                    <td><input id="totalAverageBalance6MonthTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td><asp:Label ID="Label7" runat="server" Text="Average-6 Months"></asp:Label></td>
                    <td><input id="average6MonthCreditTurnOverTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td><input id="CheckBox6Month" type="checkbox" onclick="JS_FunctionCheckCreditTurnOver6()" /></td>
                    <td><input id="average6MonthAverageBalanceTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td><input id="maxBalDate6MonthTextBox" type="text" readonly="readonly" style="width:40px;"/></td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><asp:Label ID="Label8" runat="server" Text="50% of Avg.Balance"></asp:Label></td>
                    <td><input id="averageBalance6Month50PerTextBox" type="text" readonly="readonly" style="width:95px" /></td>
                    <td><input id="avg6MonthCheckBox" type="checkbox" onclick="JS_FunctionAvg6Month()" /></td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input id="saveButton" type="button" value="Save Calculation" 
                        onclick="SaveCalculation()" style="width: 120px; " />                    
                    <input id="clearButton" type="button" value="Clear" onclick="JS_functionClearCalGrid()" style="width: 100px; height: 28px;" />
                    <input id="clearAllButton" type="button" value="Clear All" onclick="JS_functionClearAll()" style="width: 100px; height: 28px;" />
                    <input id="deleteButton" type="button" value="Delete Select row" onclick="JS_functionDelete()" style="width: 120px; height: 28px;" />
                </td>
            </tr>

            <tr>
                <td align="left" colspan="2">
                    <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="sp" width="100%"
                        style="height: 250px">
                        <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                    </object>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="button" value="Save Bank statement" onclick="SaveTemplate()" style="width: 150px; height: 28px;" />
                </td>
            </tr>
        </table>
    </center>
     
    <script type="text/javascript">    
    	var mygrid;
    	var calGrid;    	
    	mygrid = new dhtmlXGridObject('gridbox');
	    calGrid = new dhtmlXGridObject('calculationGrid');
	    function doOnLoad() {
	        debugger;
	        mygrid.setImagePath("../codebase/imgs/");
	        mygrid.setHeader("Sl,BankId,BankName,BranchId,BranchName,A/C No,ACTypeId,ACName,From,To,StartManth,LLID,ACType,Action,masterId");
	        mygrid.setInitWidths("30,0,150,0,150,120,0,0,100,100,0,0,0,55,0");
	        mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
	        mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,co,co,ro,ro,ro,link,ro");
	        mygrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	        mygrid.setSkin("modern");
	        mygrid.init();
	        mygrid.enableSmartRendering(true,5);
	        mygrid.parse("../UI/BankListShowXML.aspx");
	       
	        mygrid.attachEvent("onRowSelect",doOnRowSelected);	   
	    }

        function doOnRowSelected()
        {
            if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
            {           
              LoadTemplate();
              JS_FunctionLoadComboList();              
            }	    
	    }
	    function JS_FunctionLoadComboList()
	    {
	        var months = new Array(12);
                months[0] = "Jan";
                months[1] = "Feb";
                months[2] = "Mar";
                months[3] = "Apr";
                months[4] = "May";
                months[5] = "Jun";
                months[6] = "Jul";
                months[7] = "Aug";
                months[8] = "Sep";
                months[9] = "Oct";
                months[10] = "Nov";
                months[11] = "Dec";
                
            var startDate=mygrid.cells(mygrid.getSelectedId(),8).getValue();            
            var splitDate=startDate.split('-');
	        var month_value;
	        switch(splitDate[0])
            {
                case "Jan":
                month_value=0;
                break;
                case "Feb":
                month_value=1;
                break;
                case "Mar":
                month_value=2;
                break;
                case "Apr":
                month_value=3;
                break;
                case "May":
                month_value=4;
                break;
                case "Jun":
                month_value=5;
                break;
                case "Jul":
                month_value=6;
                break;
                case "Aug":
                month_value=7;
                break;
                case "Sep":
                month_value=8;
                break;
                case "Oct":
                month_value=9;
                break;
                case "Nov":
                month_value=10;
                break;
                case "Dec":
                month_value=11;
                break;
            }
            	mygrid.getCombo(8).clear();
    			mygrid.getCombo(9).clear();
            for(var i=0;i<12;i++)
            {
                mygrid.getCombo(8).put(months[month_value]+ "-" + splitDate[1],months[month_value]+ "-" + splitDate[1]);
    			mygrid.getCombo(9).put(months[month_value]+ "-" + splitDate[1],months[month_value]+ "-" + splitDate[1]);
                month_value +=1;
                if(month_value>11)
                {
                    month_value=0;
                    var year = splitDate[1];
                    //alert(parseFloat(year));
                    if(parseFloat(year) > 8)
                    {   
                        splitDate[1] = parseFloat(year)+1;
                    }
                    else
                    {
                        year = (parseFloat(year)+1);
                        splitDate[1] = '0' + year;                        
                    }
                }
            }
	    }
	    function JS_FunctionLoadCalculationData() {
	        debugger;
	        var startMonth=mygrid.cells(mygrid.getSelectedId(),8).getValue();
	        var endMonth=mygrid.cells(mygrid.getSelectedId(),9).getValue();
	        var numofItem=calGrid.getRowsNum();
	        var bankId = mygrid.cells(mygrid.getSelectedId(),1).getValue();
	        var bankName = mygrid.cells(mygrid.getSelectedId(),2).getValue();
            var branchId = mygrid.cells(mygrid.getSelectedId(),3).getValue();
            var branchName = mygrid.cells(mygrid.getSelectedId(),4).getValue();
            var acNo = mygrid.cells(mygrid.getSelectedId(),5).getValue();                
            var acTypeId = mygrid.cells(mygrid.getSelectedId(),6).getValue();
            var acName = mygrid.cells(mygrid.getSelectedId(),7).getValue();
            var masterId = mygrid.cells(mygrid.getSelectedId(),14).getValue();
	        calculate(startMonth,endMonth,numofItem,bankId,bankName,branchId,branchName,acNo,acName,acTypeId,masterId);     
	    }
	    function calculate(startMonth,endMonth,numofItem,bankId,bankName,branchId,branchName,acNo,acName,acTypeId,masterId)
        {
            try
            {
                XMLHttpRequestObject = new XMLHttpRequest();
              
                var spreadsheet = document.getElementById("sp");         
                if(XMLHttpRequestObject) 
                {
                    XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=Calculation&StartIndex="+startMonth+"&EndIndex="+endMonth+"&NumOfItem="+numofItem+"&BANKID="+bankId+"&BANKNAME="+bankName+"&BRANCHID="+branchId+"&BRANCHNAME="+branchName+"&ACNO="+acNo+"&ACNAME="+acName+"&ACTYPEID="+acTypeId+"&MASTERID="+masterId);
                    XMLHttpRequestObject.onreadystatechange = function ()
                    {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
                        {  
                            document.getElementById('sp').XMLData=spreadsheet.XMLData;
                            var TypeData=new Array();
                            TypeData=XMLHttpRequestObject.responseText.split('#');
                            document.getElementById('str_xml').value =document.getElementById('str_xml').value + TypeData[0]; //XMLHttpRequestObject.responseText;
                            LoadCalculationGridData();
                        }
                    }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                //XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
                XMLHttpRequestObject.send(spreadsheet.XMLData);
                }
                 
            }
            catch(err)
            {
                alert("Error");
            }          
	    }

	    function JS_FunctionLoadGrid() {
	        debugger;
	        var LLId = document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value;
            if(LLId.length!="")
            {
                mygrid.clearAll();                
                mygrid.loadXML("../UI/BankListShowXML.aspx?numLLId="+LLId);
               
                LoadExistingData(LLId);
            }
            else
            {
                alert("Please enter LLID");
            }
	    }
	    function JS_functionDelete()
	    {
	        document.getElementById('str_xml').value="";
	        calGrid.deleteRow(calGrid.getSelectedId());
	    }
	    function calculationGridOnLoad()
	    {
	    calGrid.setImagePath("../codebase/imgs/");
	    calGrid.setHeader("Sl,BankId,BankName,BranchId,BranchName,A/CNo,ACTypeId,ACName,Month,CreditTurn,Adj,AvgBalance,MaxDate,Adj");
	    calGrid.setInitWidths("25,0,120,0,120,105,0,120,80,100,40,100,70,40");
	    calGrid.setColAlign("left,left,left,left,left,left,left,left,left,right,center,right,center,center");
	    calGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,ch");
	    calGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str");
	    calGrid.setSkin("gray");
	    calGrid.init();
	    calGrid.enableSmartRendering(true,12);
	    calGrid.attachEvent("onCheckbox",doOnCheck);
	    }
	    function doOnCheck(rowId,cellId,state)
        {
            if(cellId==10)
            {
                JS_Function_CreditTurnOverAdjCheckBox();
            }
            if(cellId==13)
            {
                JS_Function_avgBalanceAdjCheckBox();
            }
        }
        function LoadCalculationGridData()
	    {
            var LLId = document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value;
            var sbCalData=document.getElementById('str_xml').value;
            if(LLId.length!="")
            {
                if(calGrid.getRowsNum()=='12')
                {
                    alert("Bank Statement calculation grid is full !");
                }
                else
                {
                    calGrid.clearAll();
                    document.getElementById('str_xml').value=document.getElementById('str_xml').value.replace("</rows><rows>","");
                    calGrid.parse(document.getElementById('str_xml').value);
                    JS_Function_CreditTurnOverAdjCheckBox();
                    JS_Function_avgBalanceAdjCheckBox();
                }
            }
            else
            {
                alert("Please enter LLID");
            }
	    }
	    function LoadCalculationTemplate()
        {
            try
            {
                XMLHttpRequestObject = new XMLHttpRequest();
            
                var spreadsheet = document.getElementById("spObj");                
                if(XMLHttpRequestObject) 
                {                         
                    XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=LoadCalculationTemplate&ID=7");      
                    XMLHttpRequestObject.onreadystatechange = function ()
                    {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
                        {
                               spreadsheet.XMLData = XMLHttpRequestObject.responseText;                                                    
                        }
                    }            
                    XMLHttpRequestObject.send(null);
                }                  
              }
              catch(err)
              {}                
        }
	    
        function LoadTemplate()
        {
        try
        {
            XMLHttpRequestObject = new XMLHttpRequest();
              
                var spreadsheet = document.getElementById("sp");
                
                if(XMLHttpRequestObject) 
                {                         
                    var lLId = mygrid.cells(mygrid.getSelectedId(),11).getValue();
                    var bankId = mygrid.cells(mygrid.getSelectedId(),1).getValue();
                    var branchId = mygrid.cells(mygrid.getSelectedId(),3).getValue();
                    var acNo = mygrid.cells(mygrid.getSelectedId(),5).getValue();
                    XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=loadTemplate&LLID="+lLId+"&BANKID="+bankId+"&BRANCHID="+branchId+"&ACNO="+acNo);      
                    XMLHttpRequestObject.onreadystatechange = function ()
                    {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
                        {
                               spreadsheet.XMLData = XMLHttpRequestObject.responseText;                      
                        }
                    }            
                    XMLHttpRequestObject.send(null);
                    //document.getElementById('ctl00_bodyContentPlaceHolder_lblMessage').innerText=XMLHttpRequestObject.responseText;
                }                  
              }
              catch(err)
              {}                
        }
        function LoadDefaultTemplate()
        {
        try
        {
            var LLId = document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value;
            XMLHttpRequestObject = new XMLHttpRequest();
          
            var spreadsheet = document.getElementById("sp");                
            if(XMLHttpRequestObject) 
            {                         
                XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=LoadDefaultTemplate&ID=6");      
                XMLHttpRequestObject.onreadystatechange = function ()
                {
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
                    {
                           spreadsheet.XMLData = XMLHttpRequestObject.responseText;                                                     
                    }
                }            
                XMLHttpRequestObject.send(null);
            }                  
          }
          catch(err)
          {}                
        }
        function LoadExistingData(LLId)
        {
            try
            {
                var XmlData=LLId;
                XMLHttpRequestObject = new XMLHttpRequest();
                if(XMLHttpRequestObject) 
                {                         
                    XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=LoadExistingData&LLId="+LLId);      
                    XMLHttpRequestObject.onreadystatechange = function ()
                    {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
                        {
                            var splitData=new Array();
                            var rowData=new Array();
                            splitData=XMLHttpRequestObject.responseText.split('*');
                            rowData=splitData[0].split('#');
                            var mainData=new Array();
                            mainData=rowData[0].split(',');                            
                            if(mainData[0] == LLId.toString())
                            {
                                document.getElementById('str_xml').value = rowData[1];                            
                                LoadCalculationGridData();
                                if(mainData[13] == 12)
                                {
                                    document.getElementById('checkBox12Month').checked = true;
                                }
                                else
                                {
                                    document.getElementById('checkBox6Month').checked = true;
                                }
                                if(mainData[14] == 12)
                                {
                                    document.getElementById('avg12CheckBox').checked = true;
                                }
                                else
                                {
                                    document.getElementById('avg6MonthCheckBox').checked = true
                                }
                                //alert(rowData[1] +"  "+ mainData[15])
                                if(mainData[15] !="" || mainData[15] !=null)
                                {
                                    JS_FunctionCheckRemarks(mainData[15]);
                                }
                            }
                            
                            LoadDefaultTemplate();                      
                        }
                    }            
                    XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                    //XMLHttpRequestObject.setRequestHeader("Content-length", XmlData.length);
                    XMLHttpRequestObject.send(XmlData);
                }                  
              }
              catch(err)
              {
                alert(err);
              }                
        }
       function SaveCalculation()
       {
            
            try
            {
                var XmlData = CreateXMLData();
                XMLHttpRequestObject = new XMLHttpRequest();
                      
                if(XMLHttpRequestObject) 
                {
                    var lLId = document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value;
                    var total12MonthCreditTurnOver = document.getElementById('total12MonthCreditTurnOverTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var average12MonthCreditTurnOver = document.getElementById('average12MonthCreditTurnOverTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var total6MonthCreditTurnOver = document.getElementById('total6MonthCreditTurnOverTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var average6MonthCreditTurnOver = document.getElementById('average6MonthCreditTurnOverTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');

                    var totalAverageBalance12Month = document.getElementById('totalAverageBalance12MonthTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var average12MonthAverageBalance = document.getElementById('average12MonthAverageBalanceTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var totalAverageBalance6Month = document.getElementById('totalAverageBalance6MonthTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var average6MonthAverageBalance = document.getElementById('average6MonthAverageBalanceTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                   
                    var averageBalance12Month50Per = document.getElementById('averageBalance12Month50PerTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var averageBalance6Month50Per = document.getElementById('averageBalance6Month50PerTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
                    var maxBalDate12Month = document.getElementById('maxBalDate12MonthTextBox').value;
                    var maxBalDate6Month = document.getElementById('maxBalDate6MonthTextBox').value;
                    var creditTurnOverPart=0;
                   if(document.getElementById('checkBox12Month').checked==true)
                   {
                        creditTurnOverPart=12;
                   }
                   else
                   {
                        creditTurnOverPart=6;
                   }
                   var averageBalancePart=0;
                   if(document.getElementById('avg12CheckBox').checked==true)
                    {
                        averageBalancePart=12;
                    }
                    else
                    {
                        averageBalancePart=6;
                    }
                    var headPart=lLId+","+total12MonthCreditTurnOver+","+average12MonthCreditTurnOver+","+total6MonthCreditTurnOver+","+average6MonthCreditTurnOver+","+totalAverageBalance12Month+","+average12MonthAverageBalance+","+totalAverageBalance6Month+","+average6MonthAverageBalance+","+averageBalance12Month50Per+","+averageBalance6Month50Per+","+maxBalDate12Month+","+maxBalDate6Month+","+creditTurnOverPart+","+averageBalancePart+","+JS_FunctionRemarks();
                    XmlData=headPart+"#"+XmlData;
                    XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=saveCalculation&LLID="+lLId);
                    XMLHttpRequestObject.onreadystatechange = function ()
                    {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
                        {  
                            var TypeData=new Array();
                            TypeData=XMLHttpRequestObject.responseText.split('#');
                            alert(TypeData[0]);                    
                        }
                    }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                //XMLHttpRequestObject.setRequestHeader("Content-length", XmlData.length);
                XMLHttpRequestObject.send(XmlData);
                }    
            }
            catch(err)
            {
                alert(err);
            }   
       } 
       function CreateXMLData()
       {
        var cellData="";
        var tempCellData="";
        var i=0;
        for(i=1;i<=calGrid.getRowsNum();i++)
        {
         tempCellData ="<row id='" + i + "'><cell>"+i+"</cell><cell>"+calGrid.cells(i,1).getValue()+"</cell><cell>"+calGrid.cells(i,2).getValue()+"</cell><cell>"+calGrid.cells(i,3).getValue()+
                       "</cell><cell>"+calGrid.cells(i,4).getValue()+"</cell><cell>"+calGrid.cells(i,5).getValue()+"</cell><cell>"+calGrid.cells(i,6).getValue()+"</cell><cell>"+calGrid.cells(i,7).getValue()+
                       "</cell><cell>"+calGrid.cells(i,8).getValue()+"</cell><cell>"+calGrid.cells(i,9).getValue()+"</cell><cell>"+calGrid.cells(i,10).getValue()+"</cell><cell>"+calGrid.cells(i,11).getValue()+
                       "</cell><cell>"+calGrid.cells(i,12).getValue()+"</cell><cell>"+calGrid.cells(i,13).getValue()+"</cell></row>";
            cellData +=tempCellData;
            if(i==12)
            {
                break;
            }
            
        }
        return XmlData = "<rows>" + cellData + "</rows>";
       }
       
       function JS_functionClearCalGrid()
       {
           calGrid.clearAll();
           document.getElementById('str_xml').value="";
           
           document.getElementById('total12MonthCreditTurnOverTextBox').value = "";
           document.getElementById('average12MonthCreditTurnOverTextBox').value = "";
           document.getElementById('total6MonthCreditTurnOverTextBox').value = "";
           document.getElementById('average6MonthCreditTurnOverTextBox').value = "";

           document.getElementById('totalAverageBalance12MonthTextBox').value = "";
           document.getElementById('average12MonthAverageBalanceTextBox').value = "";
           document.getElementById('totalAverageBalance6MonthTextBox').value = "";
           document.getElementById('average6MonthAverageBalanceTextBox').value = "";
           document.getElementById('averageBalance12Month50PerTextBox').value = "";
           document.getElementById('averageBalance6Month50PerTextBox').value = "";
           
           document.getElementById('average12MonthCreditTurnOverTextBox').style.backgroundColor = 'White';
           document.getElementById('average6MonthCreditTurnOverTextBox').style.backgroundColor = 'White';
           document.getElementById('averageBalance12Month50PerTextBox').style.backgroundColor = 'White';
           document.getElementById('averageBalance6Month50PerTextBox').style.backgroundColor = 'White';
           document.getElementById('checkBox12Month').checked = false;
           document.getElementById('checkBox6Month').checked = false;
           document.getElementById('avg6MonthCheckBox').checked = false;
           document.getElementById('avg12CheckBox').checked = false;
           
           document.getElementById('maxBalDate12MonthTextBox').value="";
           document.getElementById('maxBalDate6MonthTextBox').value="";
           JS_FunctionClearRemarks();

       }
       function JS_functionClearAll()
       {
            mygrid.clearAll();
            JS_functionClearCalGrid();
            LoadDefaultTemplate();
       }
       
       function SaveTemplate() {
           //debugger;
        try
        {
            var spreadsheet = document.getElementById("sp");
            var acName = mygrid.cells(mygrid.getSelectedId(), 7).getValue();
            XMLHttpRequestObject = new XMLHttpRequest();
                 
            if(XMLHttpRequestObject) 
            {
                var lLId = mygrid.cells(mygrid.getSelectedId(),11).getValue();
                var bankId = mygrid.cells(mygrid.getSelectedId(),1).getValue();
                var branchId = mygrid.cells(mygrid.getSelectedId(),3).getValue();
                var acNo = mygrid.cells(mygrid.getSelectedId(),5).getValue();                
                var acTypeId = mygrid.cells(mygrid.getSelectedId(),6).getValue();
                XMLHttpRequestObject.open("POST", "BankStatementCalculationUI.aspx?operation=save&LLID="+lLId+"&BANKID="+bankId+"&BRANCHID="+branchId+"&ACNO="+acNo+"&ACNAME="+acName+"&ACTYPEID="+acTypeId);
                XMLHttpRequestObject.onreadystatechange = function ()
                {
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
                    {  
                        document.getElementById('sp').XMLData=spreadsheet.XMLData;
                        var TypeData=new Array();
                        TypeData=XMLHttpRequestObject.responseText.split('#');
                        alert(TypeData[0]);                    
                    }
                }
            XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
            //XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
            XMLHttpRequestObject.send(spreadsheet.XMLData);
            }    
        }
        catch(err)
        {}   
        }
        
        function ParseValue(xmlString,xmlTag)
        {
        var TagValue = null;
        var StringXML = xmlString;
        var TagXML = xmlTag;
        xmlString = xmlString.toUpperCase();
        var StartTag = xmlTag.toUpperCase();
        var EndTag = "/" + xmlTag.toUpperCase();
        var j = 0;
        var i = xmlString.indexOf(StartTag, 0);
        if (i >= 0)
            j = xmlString.indexOf(EndTag, 0);
        if (i >= 0 && j >= i)
        {
            i = i + StartTag.length;
            TagValue = "<" + StartTag.toLowerCase() + " " + StringXML.substring(i, j - i) + EndTag.toLowerCase() + ">";
        }
        else
        {
            TagValue = "";
        }
        return TagValue;
      }

        function JS_Function_CreditTurnOverAdjCheckBox()
        {
            var total12MonthCreditTurnOver = 0;
            var total6MonthCreditTurnOver = 0;
            var average12MonthCreditTurnOver = 0;
            var average6MonthCreditTurnOver = 0;
            var credit12MonthCount = 0;
            var credit6MonthCount = 0;
            
            var LowCreditTurnOver = 0;
            var HighCreditTurnOver = 0;
            var HighCredit12MonthCount =0;
            var LowCredit12MonthCount =0;

            for(var i=1;i<=calGrid.getRowsNum();i++)
            {
                if(i<=6)
                {
                    if (calGrid.cells(i,10).getValue() == 0)
                    {
                        //var re = new RegExp(',','g');
                        LowCreditTurnOver = parseFloat(LowCreditTurnOver) + parseFloat('0'+calGrid.cells(i,9).getValue().replace(',','').replace(',','').replace(',',''));
                        LowCredit12MonthCount = LowCredit12MonthCount + 1;
                    }
                }
                else
                {
                    if (calGrid.cells(i,10).getValue() == 0)
                    {
                        HighCreditTurnOver = parseFloat(HighCreditTurnOver) + parseFloat('0'+calGrid.cells(i,9).getValue().replace(',','').replace(',','').replace(',',''));
                        HighCredit12MonthCount = HighCredit12MonthCount + 1;
                    }
                }
            }
            credit12MonthCount = LowCredit12MonthCount + HighCredit12MonthCount;
            total12MonthCreditTurnOver = parseFloat(LowCreditTurnOver) + parseFloat(HighCreditTurnOver);
            average12MonthCreditTurnOver = parseFloat(total12MonthCreditTurnOver) / credit12MonthCount;
            total6MonthCreditTurnOver = HighCreditTurnOver;
            average6MonthCreditTurnOver = parseFloat(HighCreditTurnOver) / HighCredit12MonthCount;
            document.getElementById('total12MonthCreditTurnOverTextBox').value = CommaFormatted(CurrencyFormatted(total12MonthCreditTurnOver));//round_decimals(total12MonthCreditTurnOver,4);
            document.getElementById('average12MonthCreditTurnOverTextBox').value = CommaFormatted(CurrencyFormatted(average12MonthCreditTurnOver));//round_decimals(average12MonthCreditTurnOver,4);
            document.getElementById('total6MonthCreditTurnOverTextBox').value = CommaFormatted(CurrencyFormatted(total6MonthCreditTurnOver));//round_decimals(total6MonthCreditTurnOver,4);
            document.getElementById('average6MonthCreditTurnOverTextBox').value = CommaFormatted(CurrencyFormatted(average6MonthCreditTurnOver));//round_decimals(average6MonthCreditTurnOver,4);
            JS_FunctionColorCreditTurnOverPart();
        }
        
        
        function JS_Function_avgBalanceAdjCheckBox()
        {
        var total12MonthAverageBalance = 0;
        var total6MonthAverageBalance = 0;
        var average12MonthAverageBalance = 0;
        var average6MonthAverageBalance = 0;
        var averageBalance12Month50Per = 0;
        var averageBalance6Month50Per = 0;
        var avgBalace12MonthCount = 0;
        var avgBalace6MonthCount = 0;
        var maxBalDate12Month="";
        var tempMaxBalDate12Month=0;
        var maxBalDate6Month="";
        var tempMaxBalDate6Month=0;
        
        var LowAverageBalance = 0;
        var HighAverageBalance = 0;
        var HighAverageBalanceMonthCount =0;
        var LowAverageBalanceMonthCount =0;
            
        for(var i=1;i<=calGrid.getRowsNum();i++)
        {

            if(i<=6)
            {
                if (calGrid.cells(i,13).getValue() == 0)
                {
                    LowAverageBalance = parseFloat(LowAverageBalance) + parseFloat('0'+calGrid.cells(i,11).getValue().replace(',','').replace(',','').replace(',',''));
                    LowAverageBalanceMonthCount = LowAverageBalanceMonthCount + 1;
                }
            }
            else
            {
                if (calGrid.cells(i,13).getValue() == 0)
                {
                    HighAverageBalance = parseFloat(HighAverageBalance) + parseFloat('0'+calGrid.cells(i,11).getValue().replace(',','').replace(',','').replace(',',''));
                    HighAverageBalanceMonthCount = HighAverageBalanceMonthCount + 1;
                    maxBalDate6Month += calGrid.cells(i,12).getValue() + ",";
                }
            }
            maxBalDate12Month += calGrid.cells(i,12).getValue() + ",";            
        }
        maxBalDate12Month=maxBalDate12Month.substring(0,maxBalDate12Month.length-1);
        maxBalDate6Month=maxBalDate6Month.substring(0,maxBalDate6Month.length-1);
        
        avgBalace12MonthCount = LowAverageBalanceMonthCount + HighAverageBalanceMonthCount;
        total12MonthAverageBalance = parseFloat(LowAverageBalance) + (HighAverageBalance);
        average12MonthAverageBalance = parseFloat(total12MonthAverageBalance) / avgBalace12MonthCount;
        average6MonthAverageBalance = parseFloat(HighAverageBalance) / HighAverageBalanceMonthCount;
        averageBalance12Month50Per = parseFloat(average12MonthAverageBalance) * 0.5;
        averageBalance6Month50Per = parseFloat(average6MonthAverageBalance) * 0.5;
        document.getElementById('totalAverageBalance12MonthTextBox').value = CommaFormatted(CurrencyFormatted(total12MonthAverageBalance));//round_decimals(total12MonthAverageBalance,4)
        document.getElementById('average12MonthAverageBalanceTextBox').value = CommaFormatted(CurrencyFormatted(average12MonthAverageBalance));//round_decimals(average12MonthAverageBalance,4);
        document.getElementById('totalAverageBalance6MonthTextBox').value = CommaFormatted(CurrencyFormatted(HighAverageBalance));//round_decimals(total6MonthAverageBalance,4);
        document.getElementById('average6MonthAverageBalanceTextBox').value = CommaFormatted(CurrencyFormatted(average6MonthAverageBalance));//round_decimals(average6MonthAverageBalance,4);
        document.getElementById('averageBalance12Month50PerTextBox').value = CommaFormatted(CurrencyFormatted(averageBalance12Month50Per));//round_decimals(averageBalance12Month50Per,4);
        document.getElementById('averageBalance6Month50PerTextBox').value = CommaFormatted(CurrencyFormatted(averageBalance6Month50Per));//round_decimals(averageBalance6Month50Per,4);
        document.getElementById('maxBalDate12MonthTextBox').value= Mode(maxBalDate12Month);
        document.getElementById('maxBalDate6MonthTextBox').value= Mode(maxBalDate6Month);

        JS_FunctionColorAverageBalancePart();
    }
    
    function Mode(arr)
    {
        var maxValue=0;
        var maxCount=0;
        var a =new Array();
        a = arr.split(',');
        for(var i = 0; i < a.length; i++)
        {
            var count = 0;
            for(var j = 0; j < a.length; j++)
            {
                if (a[j] == a[i])
                {
                 count++;
                }
            }
            if (count > maxCount)
            {
             maxCount = count;
             maxValue = a[i];
            }
        }

        return maxValue;
    }    
    
    function CurrencyFormatted(amount)
    {
	    var i = parseFloat(amount);
	    if(isNaN(i)) { i = 0.00; }
	    var minus = '';
	    if(i < 0) { minus = '-'; }
	    i = Math.abs(i);
	    i = parseInt((i + .005) * 100);
	    i = i / 100;
	    s = new String(i);
	    if(s.indexOf('.') < 0) { s += '.00'; }
	    if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	    s = minus + s;
	    return s;
    }

    function CommaFormatted(amount)
    {
	    var delimiter = ","; // replace comma if desired
	    var a = amount.split('.',2)
	    var d = a[1];
	    var i = parseInt(a[0]);
	    if(isNaN(i)) { return ''; }
	    var minus = '';
	    if(i < 0) { minus = '-'; }
	    i = Math.abs(i);
	    var n = new String(i);
	    var a = [];
	    while(n.length > 3)
	    {
		    var nn = n.substr(n.length-3);
		    a.unshift(nn);
		    n = n.substr(0,n.length-3);
	    }
	    if(n.length > 0) { a.unshift(n); }
	    n = a.join(delimiter);
	    if(d.length < 1) { amount = n; }
	    else { amount = n + '.' + d; }
	    amount = minus + amount;
	    return amount;
    }

    function round_decimals(original_number, decimals)
    {
      var result1 = original_number * Math.pow(10, decimals)
      var result2 = Math.round(result1)
      var result3 = result2 / Math.pow(10, decimals)
      return (result3)
     }     

     
     function JS_FunctionCheckCreditTurnOver12()
     {
        if(document.getElementById('checkBox12Month').checked==true)
        {
            document.getElementById('CheckBox6Month').checked = false;
        }
        else
        {
            document.getElementById('CheckBox6Month').checked = true;
        }        
     }
     function JS_FunctionCheckCreditTurnOver6()
     {
        if(document.getElementById('CheckBox6Month').checked==true)
        {
            document.getElementById('checkBox12Month').checked = false;
        }
        else
        {
            document.getElementById('checkBox12Month').checked = true;
        }        
     }
     function JS_FunctionAvg6Month()
     {
        if(document.getElementById('avg6MonthCheckBox').checked==true)
        {
            document.getElementById('avg12CheckBox').checked = false;
        }
        else
        {
            document.getElementById('avg12CheckBox').checked = true;
        }
     }
     function JS_FunctionAvg12Month()
     {
        if(document.getElementById('avg12CheckBox').checked==true)
        {
            document.getElementById('avg6MonthCheckBox').checked = false;
        }
        else
        {
            document.getElementById('avg6MonthCheckBox').checked = true;
        }
     }
     function JS_FunctionColorCreditTurnOverPart()
     {
      var average12MonthCreditTurnOver=document.getElementById('average12MonthCreditTurnOverTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
      var average6MonthCreditTurnOver=document.getElementById('average6MonthCreditTurnOverTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
          if(average12MonthCreditTurnOver>0 & average6MonthCreditTurnOver>0)
          {
              if(parseFloat(average12MonthCreditTurnOver)>parseFloat(average6MonthCreditTurnOver))
              {
                document.getElementById('average12MonthCreditTurnOverTextBox').style.backgroundColor = 'green';
                document.getElementById('average6MonthCreditTurnOverTextBox').style.backgroundColor = 'red';
              }
              else
              {
                document.getElementById('average12MonthCreditTurnOverTextBox').style.backgroundColor = 'red';
                document.getElementById('average6MonthCreditTurnOverTextBox').style.backgroundColor = 'green';
              }
          }
     }
     function JS_FunctionColorAverageBalancePart()
     {
      var averageBalance12Month50Per=document.getElementById('averageBalance12Month50PerTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
      var averageBalance6Month50Per=document.getElementById('averageBalance6Month50PerTextBox').value.replace(',','').replace(',','').replace(',','').replace(',','');
        if(averageBalance12Month50Per>0 & averageBalance6Month50Per>0)
        {
            if(parseFloat(averageBalance12Month50Per)>parseFloat(averageBalance6Month50Per))
            {
                 document.getElementById('averageBalance12Month50PerTextBox').style.backgroundColor = 'green';
                 document.getElementById('averageBalance6Month50PerTextBox').style.backgroundColor = 'red';
            }
            else
            {
                document.getElementById('averageBalance12Month50PerTextBox').style.backgroundColor = 'red';
                document.getElementById('averageBalance6Month50PerTextBox').style.backgroundColor = 'green';
            }
        }     
     }
     function JS_FunctionClearRemarks()
     {
        var remarksGrid=document.getElementById('ctl00_ContentPlaceHolder_remarksGridView');
        var theRows = remarksGrid.getElementsByTagName("tr");
        for(var i=2;i<=theRows.length;i++)
        {
            if(i>9)
            {
               document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+i+'_IdCheckBox').checked=false;
            }
            else
            {
               document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+i+'_IdCheckBox').checked=false;
            }
        }
     }
     function JS_FunctionRemarks()
     {
        var remarksGrid=document.getElementById('ctl00_ContentPlaceHolder_remarksGridView');
        var theRows = remarksGrid.getElementsByTagName("tr");
        var remarksValue="";
        for(var i=2;i<=theRows.length;i++)
        {
            if(i>9)
            {
                if(document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+i+'_IdCheckBox').checked==true)
                {
                    remarksValue = remarksValue + document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+i+'_idTextBox').value + "|" + document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+i+'_remarksTextBox').value +"$";
                }
            }
            else
            {
                if(document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+i+'_IdCheckBox').checked==true)
                {
                    remarksValue = remarksValue + document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+i+'_idTextBox').value + "|" + document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+i+'_remarksTextBox').value +"$";
                }
            }
        }
        return remarksValue.substring(0,remarksValue.length-1);
     }
     
     function JS_FunctionCheckRemarks(remarksObj)
     {
        var remarksGrid=document.getElementById('ctl00_ContentPlaceHolder_remarksGridView');
        var theRows = remarksGrid.getElementsByTagName("tr");
        var remarksValue = new Array();
        if(remarksObj.length>0)
        {
            remarksValue=remarksObj.split('$');
            for(var i=0;i<remarksValue.length;i++)
            {
                var remarksData = new Array();
                remarksData = remarksValue[i].split('|');
                if (remarksData[0]!="")
                {
                    for(var rowIndex=2;rowIndex<=theRows.length;rowIndex++)
                    {
                        if(rowIndex>9)
                        {
                            if(document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+rowIndex+'_idTextBox').value == remarksData[0])
                            {
                                document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+rowIndex+'_IdCheckBox').checked = true;
                                document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl'+rowIndex+'_remarksTextBox').value = remarksData[1];
                            }
                        }
                        else
                        {
                            if(document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+rowIndex+'_idTextBox').value == remarksData[0])
                            {
                                document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+rowIndex+'_IdCheckBox').checked = true;
                                document.getElementById('ctl00_ContentPlaceHolder_remarksGridView_ctl0'+rowIndex+'_remarksTextBox').value = remarksData[1];
                            }
                        }
                    }
                }
            }
        }
     }
function saveButton_onclick() {

}

    </script>

</asp:Content>
