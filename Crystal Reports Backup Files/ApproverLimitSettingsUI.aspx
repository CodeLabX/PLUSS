﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ApproverLimitSettingsUI"
    Title="Approver Limit Entry" Codebehind="ApproverLimitSettingsUI.aspx.cs" %>

<asp:Content ID="content" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div style="padding-left: 0px;">
        <table>
            <tr>
                <td colspan="4" align="center">
                    <asp:Label ID="Label1" runat="server" Text="Approver Limit Settings" Font-Bold="true" Font-Size="16px"></asp:Label>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" Text="Approver Name: "></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="approvalNameDropDownList" runat="server" Width="170px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label6" runat="server" Text="Product: "></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>

                <td>
                    
                    <asp:DropDownList ID="ddlProduct" runat="server" Width="170px" 
                        ontextchanged="LoadProductTypeByProductID" AutoPostBack="true">
                    </asp:DropDownList>

                </td>
                <td>
                    <asp:HiddenField ID="productIdHiddenField" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label3" runat="server" Text="Product Type: "></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="productDropDownList" runat="server" Enabled="false" Width="170px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Text="Level-1 Amount: "></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox ID="level1AmountTextBox" runat="server" Width="150px" MaxLength="8"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label5" runat="server" Text="Level-2 Amount:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox ID="level2AmountTextBox" runat="server" Width="150px" MaxLength="8"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label11" runat="server" Text="Level-3 Amount:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox ID="level3AmountTextBox" runat="server" Width="150px" MaxLength="8"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label12" runat="server" Text="Status:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="statusDropdownList" runat="server" Width="100px">
                        <asp:ListItem>Active</asp:ListItem>
                        <asp:ListItem>Inactive</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="addButton" runat="server" Text="Add" Width="100px" Height="26px"
                        OnClick="addButton_Click" />
                    <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" Height="26px"
                        OnClick="clearButton_Click" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                    &nbsp
                    <asp:Button ID="searchbutton" runat="server" Text="Search" Width="65px" OnClick="searchbutton_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <div id="gridbox" style="background-color: white; width: 785px; height: 275px;"></div>
                </td>
            </tr>
        </table>
         <div style="display: none" runat="server" id="approveDiv"></div>
    </div>

    <script type="text/javascript">
        debugger;
        var mygrid;
	    mygrid = new dhtmlXGridObject('gridbox');
	    mygrid.setImagePath("../codebase/imgs/");
	    mygrid.setHeader("SL,UserId,ProductId,Name,Product Name,ProductType,L1Amount,L2Amount,L3Amount,Status");
	    mygrid.setInitWidths("40,0,0,115,102,150,100,100,100,60");
	    mygrid.setColAlign("right,left,left,left,left,right,right,right,left");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
	    mygrid.setColSorting("int,int,str,str,str,int,int,int,str");
	    mygrid.setSkin("light");
	    mygrid.init();
	    mygrid.enableSmartRendering(true,20);
        //mygrid.loadXML("../includes/ApproverLimitList.xml");
	    mygrid.parse(document.getElementById("xml_data"));
	    mygrid.attachEvent("onRowSelect",DoOnRowSelected);
        function DoOnRowSelected()
        {
            if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
            {
                document.getElementById('ctl00_ContentPlaceHolder_addButton').value = "Update";
               
              document.getElementById('ctl00_ContentPlaceHolder_approvalNameDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 1).getValue());
              var productType = "";

              switch (mygrid.cells(mygrid.getSelectedId(),5).getValue()) {
                  case "Unsecure":
                      productType = "U";
                      break;
                  case "Secure":
                      productType = "S";
                      break;
                  case "Cash Secure":
                      productType = "C";
                      break;
              }
               
              document.getElementById('ctl00_ContentPlaceHolder_productDropDownList').value = productType;
              document.getElementById('ctl00_ContentPlaceHolder_ddlProduct').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
              //document.getElementById('ctl00_ContentPlaceHolder_ddlProduct').value = mygrid.cells(mygrid.getSelectedId(), 4).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_level1AmountTextBox').value=mygrid.cells(mygrid.getSelectedId(),6).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_level2AmountTextBox').value=mygrid.cells(mygrid.getSelectedId(),7).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_level3AmountTextBox').value = mygrid.cells(mygrid.getSelectedId(), 8).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_statusDropdownList').value = mygrid.cells(mygrid.getSelectedId(), 9).getValue();
             
             // document.getElementById('ctl00_ContentPlaceHolder_ddlProduct').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue().trim();
 
            }
             else
             {
              document.getElementById('ctl00_ContentPlaceHolder_addButton').value="Add";
             }
        }
    </script>

</asp:Content>
