﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_AnalystApproveUI" MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="AnalystApproveUI.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>    
        <table>
        <tr><td align="center" colspan="2" style="font-size:18px; font-weight:bold">Approval Queue</td></tr>
         <tr>
                <td colspan="2" align="left">                    
                    <asp:TextBox runat="server" ID="searchTextBox" Width="200px" MaxLength="9"></asp:TextBox>
                    <input id="searchButton" type="button" value="Search" onclick="DataSearch();" />
                </td>
            </tr>
            <tr>
                <td colspan="2" >
                <div id="gridbox"  style="background-color:white; width:970px; height:200px;"></div>                    
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td align="left"><asp:Label ID="lLIdLabel" runat="server" Text="Loan Locator Id :"></asp:Label>
                                </td>
                            <td align="left">
                                <asp:TextBox ID="lLIdTextBox" runat="server" Width="182px" MaxLength="9"></asp:TextBox>
                                <asp:Button ID="findButton" runat="server" Text="..." 
                                    onclick="findButton_Click" />
                            </td>
                        </tr>
                        <tr align="left">
                            <td><asp:Label ID="customerNameLabel" runat="server" Text="Customer Name:"></asp:Label></td>
                            <td><asp:TextBox ID="customerNameTextBox" runat="server" Width="280px" 
                                    ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr align="left">
                            <td><asp:Label ID="loanAmountLabel" runat="server" Text="Loan Amount:"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="loanAmountTextBox" runat="server" Width="182px" ReadOnly="True"></asp:TextBox>
                            &nbsp;
                            <asp:TextBox ID="lavelTextBox" runat="server" Width="40px" ReadOnly="True"></asp:TextBox>
                            &nbsp;
                            <asp:TextBox ID="loanTypeTextBox" runat="server" Width="40px" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr align="left">
                            <td><asp:Label ID="scoreLabel" runat="server" Text="Score :"></asp:Label>
                                </td>
                            <td>
                                <asp:TextBox ID="scoreTextBox" runat="server" Width="182px" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr align="left">
                            <td><asp:Label ID="resultLabel" runat="server" Text="Score Result :"></asp:Label></td>
                            <td><asp:TextBox ID="resultTextBox" runat="server" Width="182px" ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr align="left">
                            <td><asp:Label ID="decisionLabel" runat="server" Text="Decision :"></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="decisionDropDownList" runat="server" Width="185px">
                                <asp:ListItem Value="0">Select Decision</asp:ListItem>
                                <asp:ListItem Value="Approved">Approved</asp:ListItem>
                                <asp:ListItem Value="Rejected">Rejected</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr align="left">
                            <td><asp:Label ID="remarksLabel" runat="server" Text="Remarks :"></asp:Label>
                                </td>
                            <td>
                                <asp:TextBox ID="remarksTextBox" runat="server" Height="50px" TextMode="MultiLine" 
                                    Width="398px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="saveButton" runat="server" Text="Save" Width="100px" 
                                    Height="27px" onclick="saveButton_Click" />
                                <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" 
                                    Height="27px" onclick="clearButton_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                </td>
                <td>
                </td>
            </tr>
        </table>    
    </div>
     <div style="display: none" runat="server" id="dvXml"></div>
    <script type="text/javascript">
    	var mygrid;
	    mygrid = new dhtmlXGridObject('gridbox');
	    function doOnLoad()
	    {
	    mygrid.setImagePath("../codebase/imgs/");
	    mygrid.setHeader("SL,LLId,CustomerName,Product,LoanType,AppliedAmount,ProposedAmount,Date,Segment,Income,IR,Tenor,EMI,DBR,MUE,Level,Score,ScoreResult,IssueBy,Remarks,LoanTag");
	    mygrid.setInitWidths("25,80,150,80,80,80,100,100,100,80,70,70,70,70,70,70,70,80,150,150,0");
	    mygrid.setColAlign("right,right,left,left,left,right,right,right,center,right,right,right,right,right,right,right,right,left,right,right,right");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	    mygrid.setSkin("modern");
	    mygrid.init();
	    mygrid.enableSmartRendering(true, 20);
	    mygrid.parse(document.getElementById("xml_data"));
	  //  mygrid.loadXML("../UI/ApprovePLList.aspx");
	    mygrid.attachEvent("onRowSelect",DoOnRowSelected);
	    }
	    
	    function DataSearch()
	    {
	       // var searchKey=document.getElementById('ctl00_ContentPlaceHolder_searchTextBox').value;
            mygrid.clearAll();
          //  mygrid.loadXML("../UI/ApprovePLList.aspx?SearchKey="+searchKey);
	    }
	    	
        function DoOnRowSelected()
        {
            if(mygrid.cells(mygrid.getSelectedId(),1).getValue()>0)
            {
              document.getElementById('ctl00_ContentPlaceHolder_lLIdTextBox').value = mygrid.cells(mygrid.getSelectedId(),1).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_customerNameTextBox').value = mygrid.cells(mygrid.getSelectedId(),2).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_loanAmountTextBox').value = mygrid.cells(mygrid.getSelectedId(),6).getValue();         
              document.getElementById('ctl00_ContentPlaceHolder_lavelTextBox').value = mygrid.cells(mygrid.getSelectedId(),15).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_loanTypeTextBox').value = mygrid.cells(mygrid.getSelectedId(),20).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_scoreTextBox').value = mygrid.cells(mygrid.getSelectedId(),16).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_resultTextBox').value = mygrid.cells(mygrid.getSelectedId(),17).getValue();

            }
	    
	    }

	 </script>  
</asp:Content>
