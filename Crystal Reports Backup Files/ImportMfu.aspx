﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="ImportMfu.aspx.cs" Inherits="PlussAndLoan.UI.MFU.ImportMfu" %>

<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>
<%@ Register Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" TagPrefix="uc1" TagName="TextBoxDatePicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />
    <script type="text/javascript">
        function pageLoad() {
            $('#ctl00_ContentPlaceHolder_txtUploadDate').unbind();
            $('#ctl00_ContentPlaceHolder_txtUploadDate').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

        

            $('#ctl00_ContentPlaceHolder_txtUploadDateCC_DISBURSED').unbind();
            $('#ctl00_ContentPlaceHolder_txtUploadDateCC_DISBURSED').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_CHARGEOFF').unbind();
            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_CHARGEOFF').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_DISBURSED').unbind();
            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_DISBURSED').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_EBBS_CHARGEOFF').unbind();
            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_EBBS_CHARGEOFF').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_EBBS_DISBURSED').unbind();
            $('#ctl00_ContentPlaceHolder_txtUploadDateLOAN_EBBS_DISBURSED').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

        }

        function Hello_CC_CHARGEOFF() {
            document.getElementById("ctl00_ContentPlaceHolder_UploadButtonCC_CHARGEOFF").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder_messageLabeCC_CHARGEOFF").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        }

        function Hello_CC_DISBURSED() {
            document.getElementById("ctl00_ContentPlaceHolder_UploadButtonCC_DISBURSED").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder_messageLabeCC_DISBURSED").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        }

        function Hello_LOAN_CHARGEOFF() {
            document.getElementById("ctl00_ContentPlaceHolder_UploadButtonLOAN_CHARGEOFF").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder_messageLabeLOAN_CHARGEOFF").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        }

        function Hello_LOAN_DISBURSED() {
            document.getElementById("ctl00_ContentPlaceHolder_UploadButtonLOAN_DISBURSED").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder_messageLabeLOAN_DISBURSED").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        }

        function Hello_LOAN_EBBS_CHARGEOFF() {
            document.getElementById("ctl00_ContentPlaceHolder_UploadButtonLOAN_EBBS_CHARGEOFF").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder_messageLabeLOAN_EBBS_CHARGEOFF").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        }

        function Hello_LOAN_EBBS_DISBURSED() {
            document.getElementById("ctl00_ContentPlaceHolder_UploadButtonLOAN_EBBS_DISBURSED").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder_messageLabeLOAN_EBBS_DISBURSED").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">MFU Upload</div>
        


        <table style='border: 0px  double; width: 98%' align='center' border='0'>

            <tr>
                <td>
                    <label for="field1">
                        <span style="text-align: center">File Name</span>
                    </label>
                </td>
                <td>
                    <label for="field1">
                        <span style="width: 150px; text-align: center">File Upload Date</span>
                    </label>
                </td>
                <td>
                    <label for="field1">
                        <span style="text-align: center">Upload File</span>
                    </label>
                </td>
                <td>
                    <label for="field1">
                        <span style="text-align: center">Row Count</span>
                    </label>
                </td>
                <td>
                    <label for="field1">
                        <span style="text-align: center">Error Count</span>
                    </label>
                </td>
                <td colspan="4" style="text-align: center; font-weight: bold">
                    <span style="text-align: center">Action</span>
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <span>CC_CHARGEOFF</span>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" Style="width: 90px;" ID="txtUploadDate"></asp:TextBox>(yyyy-mm-dd)
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:FileUpload Style="width: 180px;" CssClass="input-field" ID="FileUpload_CC_CHARGEOFF" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCount_CC_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCount_CC_CHARGEOFF"  ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnCC_CHARGEOFF" runat="server" Text="Clear Temp Data" OnClick="ClearTempBtnCC_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="UploadButtonCC_CHARGEOFF" runat="server" Text="Upload Data"
                        OnClick="UploadButtonCC_CHARGEOFF_Click" OnClientClick="Hello_CC_CHARGEOFF()" UseSubmitBehavior="false" />
                    <asp:Label ID="messageLabeCC_CHARGEOFF" runat="server" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="HiddenFieldcount" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="CheckDataBtnCC_CHARGEOFF" runat="server" Text="Process Data" OnClick="CheckDataBtnCC_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnCC_CHARGEOFF" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnCC_CHARGEOFF_Click" />
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <span>CC_DISBURSED</span>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" Style="width: 90px;" ID="txtUploadDateCC_DISBURSED"></asp:TextBox>(yyyy-mm-dd)
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:FileUpload Style="width: 180px;" CssClass="input-field" ID="FileUpload_CC_DISBURSED" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCount_CC_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCount_CC_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnCC_DISBURSED" runat="server" Text="Clear Temp Data" OnClick="ClearTempBtnCC_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="UploadButtonCC_DISBURSED" runat="server" Text="Upload Data"
                        OnClick="UploadButtonCC_DISBURSED_Click" OnClientClick="Hello_CC_DISBURSED()" UseSubmitBehavior="false" />
                    <asp:Label ID="messageLabeCC_DISBURSED" runat="server" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="CheckDataBtnCC_DISBURSED" runat="server" Text="Process Data" OnClick="CheckDataBtnCC_DISBURSED_Click" />
                </td>
                <%--<td style="width:155px;">
                            <label for="field1" style="width:120px;">
                               <asp:Button style="width:160px;" CssClass="btn primarybtn" ID="Button4" runat="server" Text="Remove Errors From temp" OnClick="RemoveErrorsFromTempBtn_Click" />
                            </label>
                        </td>--%>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnCC_DISBURSED" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnCC_DISBURSED_Click" />
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <span>LOAN_CHARGEOFF</span>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" Style="width: 90px;" ID="txtUploadDateLOAN_CHARGEOFF"></asp:TextBox>(yyyy-mm-dd)
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:FileUpload Style="width: 180px;" CssClass="input-field" ID="FileUpload_LOAN_CHARGEOFF" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCount_LOAN_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCount_LOAN_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_CHARGEOFF" runat="server" Text="Clear Temp Data" OnClick="ClearTempBtnLOAN_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="UploadButtonLOAN_CHARGEOFF" runat="server" Text="Upload Data"
                        OnClick="UploadButtonLOAN_CHARGEOFF_Click" OnClientClick="Hello_LOAN_CHARGEOFF()" UseSubmitBehavior="false" />
                    <asp:Label ID="messageLabeLOAN_CHARGEOFF" runat="server" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="HiddenField2" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="CheckDataBtnLOAN_CHARGEOFF" runat="server" Text="Process Data" OnClick="CheckDataBtnLOAN_CHARGEOFF_Click" />
                </td>
                <%--<td style="width:155px;">
                            <label for="field1" style="width:120px;">
                               <asp:Button style="width:160px;" CssClass="btn primarybtn" ID="Button9" runat="server" Text="Remove Errors From temp" OnClick="RemoveErrorsFromTempBtn_Click" />
                            </label>
                        </td>--%>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_CHARGEOFF" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_CHARGEOFF_Click" />
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <span>LOAN_DISBURSED</span>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" Style="width: 90px;" ID="txtUploadDateLOAN_DISBURSED"></asp:TextBox>(yyyy-mm-dd)
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:FileUpload Style="width: 180px;" CssClass="input-field" ID="FileUpload_LOAN_DISBURSED" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCount_LOAN_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCount_LOAN_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_DISBURSED" runat="server" Text="Clear Temp Data" OnClick="ClearTempBtnLOAN_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="UploadButtonLOAN_DISBURSED" runat="server" Text="Upload Data"
                        OnClick="UploadButtonLOAN_DISBURSED_Click" OnClientClick="Hello_LOAN_DISBURSED()" UseSubmitBehavior="false" />
                    <asp:Label ID="messageLabeLOAN_DISBURSED" runat="server" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="HiddenField3" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="CheckDataBtnLOAN_DISBURSED" runat="server" Text="Process Data" OnClick="CheckDataBtnLOAN_DISBURSED_Click" />
                </td>
                <%--<td style="width:155px;">
                            <label for="field1" style="width:120px;">
                               <asp:Button style="width:160px;" CssClass="btn primarybtn" ID="Button14" runat="server" Text="Remove Errors From temp" OnClick="RemoveErrorsFromTempBtn_Click" />
                            </label>
                        </td>--%>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_DISBURSED" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_DISBURSED_Click" />
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <span>LOAN_EBBS_CHARGEOFF</span>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" Style="width: 90px;" ID="txtUploadDateLOAN_EBBS_CHARGEOFF"></asp:TextBox>(yyyy-mm-dd)
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:FileUpload Style="width: 180px;" CssClass="input-field" ID="FileUpload_LOAN_EBBS_CHARGEOFF" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCount_LOAN_EBBS_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCount_LOAN_EBBS_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Clear Temp Data" OnClick="ClearTempBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="UploadButtonLOAN_EBBS_CHARGEOFF" runat="server" Text="Upload Data"
                        OnClick="UploadButtonLOAN_EBBS_CHARGEOFF_Click" OnClientClick="Hello_LOAN_EBBS_CHARGEOFF()" UseSubmitBehavior="false" />
                    <asp:Label ID="messageLabeLOAN_EBBS_CHARGEOFF" runat="server" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="HiddenField4" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="CheckDataBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Process Data" OnClick="CheckDataBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
                <%-- <td style="width:155px;">
                            <label for="field1" style="width:120px;">
                               <asp:Button style="width:160px;" CssClass="btn primarybtn" ID="Button19" runat="server" Text="Remove Errors From temp" OnClick="RemoveErrorsFromTempBtn_Click" />
                            </label>
                        </td>--%>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <span>LOAN_EBBS_DISBURSED</span>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" Style="width: 90px;" ID="txtUploadDateLOAN_EBBS_DISBURSED"></asp:TextBox>(yyyy-mm-dd)
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:FileUpload Style="width: 180px;" CssClass="input-field" ID="FileUpload_LOAN_EBBS_DISBURSED" runat="server" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCount_LOAN_EBBS_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCount_LOAN_EBBS_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_EBBS_DISBURSED" runat="server" Text="Clear Temp Data" OnClick="ClearTempBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="UploadButtonLOAN_EBBS_DISBURSED" runat="server" Text="Upload Data"
                        OnClick="UploadButtonLOAN_EBBS_DISBURSED_Click" OnClientClick="Hello_LOAN_EBBS_DISBURSED()" UseSubmitBehavior="false" />
                    <asp:Label ID="messageLabeLOAN_EBBS_DISBURSED" runat="server" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="HiddenField5" runat="server" />
                </td>

                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="CheckDataBtnLOAN_EBBS_DISBURSED" runat="server" Text="Process Data" OnClick="CheckDataBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
                <%--<td style="width:155px;">
                            <label for="field1" style="width:120px;">
                               <asp:Button style="width:160px;" CssClass="btn primarybtn" ID="Button24" runat="server" Text="Remove Errors From temp" OnClick="RemoveErrorsFromTempBtn_Click" />
                            </label>
                        </td>--%>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_EBBS_DISBURSED" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
