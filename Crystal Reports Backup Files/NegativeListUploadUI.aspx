﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.UI_NegativeListUploadUI" Title="Negative List Upload" CodeBehind="NegativeListUploadUI.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="CalendarInitialJs" Src="~/UI/UserControls/js/CalendarInitialJs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PopupWindowsJs" Src="~/UI/UserControls/js/PopupWindowsJs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PopupWindows" Src="~/UI/UserControls/css/PopupWindows.ascx" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>



<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Negative List Upload</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:FileUpload CssClass="input-field" ID="FileUpload" runat="server" Width="350px" Height="32px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="criteriaLable" runat="server" Text="Criteria :"></asp:Label>
                    &nbsp;&nbsp;<asp:DropDownList CssClass="select-field" ID="criteriaDropDownList" runat="server" Width="80px">
                        <asp:ListItem>N</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="btn primarybtn" ID="UploadButton" runat="server" Text="Upload" Width="100px" Height="35px"
                        OnClick="uploadButton_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="btn clearbtn" ID="btnFlush" runat="server" Text="Flush" Width="100px" Height="35px"
                        OnClick="btnFlush_Click" />
                    <asp:HiddenField ID="HiddenFieldcount" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <br />
                    <br />
                    <asp:GridView ID="gvRecords" runat="server" GridLines="None" CssClass="myGridStyle"
                        Font-Size="11px" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="SL." />
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="FatherName" HeaderText="Father's Name" />
                            <asp:BoundField DataField="MotherName" HeaderText="Mother's Name" />
                            <asp:TemplateField HeaderText="DOB">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("DateOfBirth")).ToString("dd-MMM-yyyy") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="AccountNo.MasterAccNo1" HeaderText="Master No" />
                            <asp:BoundField DataField="DeclineReason" HeaderText="Decline Reason" />
                            <asp:BoundField DataField="BusinessEmployeeName" HeaderText="Business Name" />
                            <asp:BoundField DataField="Address1" HeaderText="Address 1" />
                            <asp:BoundField DataField="Address2" HeaderText="Address 2" />
                            <asp:BoundField DataField="ContactNumber.ContactNo1" HeaderText="Phone 1" />
                            <asp:BoundField DataField="ContactNumber.ContactNo2" HeaderText="Phone 2" />
                            <asp:BoundField DataField="ContactNumber.ContactNo3" HeaderText="Mobile 1" />
                            <asp:BoundField DataField="ContactNumber.ContactNo4" HeaderText="Mobile 2" />
                            <asp:BoundField DataField="Source" HeaderText="Source" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                        </Columns>
                    </asp:GridView>

                </td>
            </tr>

        </table>
        <div onmouseup="return false" class="translucent" onmousemove="return false" onmousedown="return false"
            id="blockUI" ondblclick="return false" style="display: none; z-index: 50000; left: 0px; position: absolute; top: 0px; background-color: gray"
            onclick="return false">
        </div>
        <script type="text/javascript">

            //function onLoadPage() {
            //    init('dataTable');
            //}
        </script>
    </div>

    <br />
    <br />
    <br />
    <br />
</asp:Content>

