﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.CustomerDataAppreove"
    Title="Customer DataUpload" CodeBehind="CustomerDataAppreove.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/azGridTableCss.ascx" TagPrefix="uc1" TagName="azGridTableCss" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>


<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <uc1:azGridTableCss runat="server" ID="azGridTableCss" />
    <modal:modalPopup runat="server" ID="modalPopup" />

    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Customer Bulk Data Approve</div>

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:GridView ID="gvRecords" runat="server" GridLines="None" CssClass="myGridStyle"
                        Font-Size="11px" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="RefType" HeaderText="Account Type" />
                            <asp:BoundField DataField="RefrenceID" HeaderText="Account" />
                            <asp:BoundField DataField="MasterNo" HeaderText="MasterNo" />
                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                            <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
                            <asp:BoundField DataField="MotherName" HeaderText="Mother Name" />

                            <asp:BoundField DataField="MakerId" HeaderText="Maker Id" />
                            <asp:TemplateField HeaderText="MakeDate">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("MakeDate")).ToString("dd-MMM-yyyy") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No record found
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <br />
                    <br />

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnApprove" runat="server" CssClass="btn primarybtn" OnClick="btnApprove_Click" Text="Approve" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn clearbtn" Text="Delete From Temp" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnDownload" runat="server" CssClass="btn primarybtn" Text="Download" OnClick="btnDownload_Click" />
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        function JS_FunctionClear() {
            mygrid.clearAll();
        }
    </script>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
