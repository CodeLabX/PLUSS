﻿<%@ Page Title="Asset Report" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="AssetReports.aspx.cs" Inherits="PlussAndLoan.UI.LocReports.AssetReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initPageLoaded() {
            $("#<%= txtStatusWiseReportFromDate.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $("#<%= txtStatusWiseReportToDate.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $("#<%= txtTATFrom.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $("#<%= txtTATTo.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $("#<%= txtTATFrom.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            $("#<%= txtTATIndividualByDateFrom.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            $("#<%= txtTATIndividualByDateTo.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });

            <%--$("#<%= txtViewQuickReportDT.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'MM yy'
            });
            
            $("#<%= txtAppMonthDropDownDT.ClientID %>").datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'MM yy'
            });--%>
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Asset Report</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">

            <tr>
                <td>
                    <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                        <tr bgcolor="#CEDFBD">
                            <td height="30" colspan="11" valign="middle" style="text-align: center;">
                                <div style="text-align: center; font-weight: bold;">
                                    <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                                        <h3 style="text-align: center;">Application Status Wise Report</h3>
                                    </font>
                                </div>
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                            <asp:DropDownList CssClass="select-field" ID="productDropDownList" runat="server" Width="200px"></asp:DropDownList>
                            </font>&nbsp; 
                            </td>

                            <td width="315" height="30" align="left" valign="middle" bgcolor="#EFF3E7">
                                <asp:DropDownList CssClass="select-field" ID="appr_disb" AppendDataBoundItems="true" runat="server">
                                    <asp:ListItem Text="Approved" Value="5" />
                                    <asp:ListItem Text="Disbursed" Value="7" />
                                    <asp:ListItem Text="Deffered" Value="6" />
                                    <asp:ListItem Text="Declined" Value="2" />
                                    <asp:ListItem Text="Verification" Value="9" />
                                    <asp:ListItem Text="In Process" Value="4" />
                                </asp:DropDownList>

                            </td>

                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                            </td>
                            <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                                <div align="left">

                                    <asp:TextBox CssClass="input-field has-width-190px" runat="server" ID="txtStatusWiseReportFromDate"></asp:TextBox>
                                    <font color="#FFFFFF" face="Courier New, Courier, mono"></font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                   
                                        <asp:DropDownList CssClass="select-field" ID="DayDropDownList" runat="server" Visible="false"></asp:DropDownList>
                                        <asp:DropDownList CssClass="select-field" ID="MonthDropDownList" runat="server" Visible="false"></asp:DropDownList>
                                        <asp:DropDownList CssClass="select-field" ID="YearDropDownList" runat="server" Visible="false"></asp:DropDownList>

                                    </font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font color="#FFFFFF" face="Courier New, Courier, mono"></font>
                                </div>

                            </td>
                            <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                            <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">
                                <asp:TextBox CssClass="input-field has-width-190px" runat="server" ID="txtStatusWiseReportToDate"></asp:TextBox>

                                <asp:DropDownList CssClass="select-field" ID="ToDateDayList" runat="server" Visible="false"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field" ID="ToDateMonthList" runat="server" Visible="false"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field" ID="ToDateYearList" runat="server" Visible="false"></asp:DropDownList>


                            </td>
                        </tr>
                        <tr bgcolor="#CEDFBD">
                            <td height="30">
                                <div align="right"></div>
                            </td>
                            <td height="30">&nbsp;</td>
                            <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">
                                <asp:Button runat="server" Text="Show" ID="btnShow" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="50" OnClick="btnShow_Click" />
                                <asp:Button runat="server" Text="Detail Report" ID="btnDetailReport" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="100" OnClick="btnDetailReport_Click" />
                            </font>

                            </td>
                        </tr>
                    </table>
                    <br />

                    <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                        <tr bgcolor="#CEDFBD">
                            <td height="30" colspan="11" valign="middle" style="text-align: center;">
                                <div style="text-align: center; font-weight: bold;">
                                    <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                                        <h3 style="text-align: center;">Individual TAT Report</h3>
                                    </font>
                                </div>
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:DropDownList CssClass="select-field" ID="TatProductDropDown" runat="server" Width="200px"></asp:DropDownList>
                            </font>&nbsp; 
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                            </td>
                            <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                                <div align="left">
                                    <font color="#FFFFFF" face="Courier New, Courier, mono"></font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                                    <asp:TextBox CssClass="input-field has-width-190px" runat="server" ID="txtTATFrom"></asp:TextBox>

                                        <asp:DropDownList CssClass="select-field" ID="TatDayDropDown" runat="server" Visible="false"></asp:DropDownList>
                                        <asp:DropDownList CssClass="select-field" ID="TatMonthDropDown" runat="server" Visible="false"></asp:DropDownList>
                                        <asp:DropDownList CssClass="select-field" ID="TatYearDropDown" runat="server" Visible="false"></asp:DropDownList>

                                    </font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font color="#FFFFFF" face="Courier New, Courier, mono"></font>
                                </div>

                            </td>
                            <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                            <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                                <asp:TextBox CssClass="input-field has-width-190px" runat="server" ID="txtTATTo"></asp:TextBox>
                                <asp:DropDownList CssClass="select-field" ID="ByDayDropDown" runat="server" Visible="false"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field" ID="ByMonthDropDown" runat="server" Visible="false"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field" ID="ByYearDropDown" runat="server" Visible="false"></asp:DropDownList>


                            </td>
                            <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;
                            </font>

                            </td>
                        </tr>
                        <tr bgcolor="#CEDFBD">
                            <td height="30">
                                <div align="right"></div>
                            </td>
                            <td height="30">&nbsp;</td>
                            <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">
                                <asp:Button runat="server" Text="Show" ID="btnIndividualTat" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="50" OnClick="btnIndividualTat_Click" />

                            </font>

                            </td>
                        </tr>
                    </table>
                    <br />

                    <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                        <tr bgcolor="#CEDFBD">
                            <td height="30" colspan="11" valign="middle" style="text-align: center;">
                                <div style="text-align: center; font-weight: bold;">
                                    <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                                        <h3 style="text-align: center;">Individual TAT by Date</h3>
                                    </font>
                                </div>
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:DropDownList CssClass="select-field" ID="InProductDropDown" runat="server" Width="200px"></asp:DropDownList>
                            </font>&nbsp; 
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                            </td>
                            <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                                <div align="left">
                                    <font color="#FFFFFF" face="Courier New, Courier, mono"></font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                                    
                                        
                                        <asp:TextBox CssClass="input-field has-width-190px" runat="server" ID="txtTATIndividualByDateFrom"></asp:TextBox>
                                        <asp:DropDownList CssClass="select-field" ID="InDayDropDown" runat="server" Visible="false"></asp:DropDownList>
                                        <asp:DropDownList CssClass="select-field" ID="InMonthDropDown" runat="server" Visible="false"></asp:DropDownList>
                                        <asp:DropDownList CssClass="select-field" ID="InYearDropDown" runat="server" Visible="false"></asp:DropDownList>

                                    </font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font face="Verdana, Arial, Helvetica, sans-serif"></font><font color="#FFFFFF" face="Courier New, Courier, mono"></font>
                                </div>

                            </td>
                            <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                            <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                                <asp:TextBox CssClass="input-field has-width-190px" runat="server" ID="txtTATIndividualByDateTo"></asp:TextBox>
                                <asp:DropDownList CssClass="select-field" ID="InToDayDropDown" runat="server" Visible="false"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field" ID="InToMonthDropDown" runat="server" Visible="false"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field" ID="InToYearDropDown" runat="server" Visible="false"></asp:DropDownList>
                        </tr>
                        <tr bgcolor="#CEDFBD">
                            <td height="30">
                                <div align="right"></div>
                            </td>
                            <td height="30">&nbsp;</td>
                            <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">
                                <asp:Button runat="server" Text="Show" ID="btnTatByDate" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="50" OnClick="btnTatByDate_Click" />

                            </font>

                            </td>
                        </tr>
                    </table>
                    <br />

                    <table width="780" border="0" cellpadding="0" cellspacing="1" bgcolor="BDD3A5">
                        <tr align="center" bgcolor="#CBDCB8">
                            <td height="30" colspan="3" valign="middle"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                                <h3 style="text-align: center;">View&nbsp;Quick&nbsp;&nbsp;Report</h3>
                            </font></td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#DCE8CE">PRODUCT:&nbsp;</td>
                            <td height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                                <asp:DropDownList CssClass="select-field" ID="DropDownProductList" runat="server" Width="200px"></asp:DropDownList>
                            </td>
                            <td align="left" valign="middle" bgcolor="#EDF2E6">&nbsp;</td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td width="28%" height="30" align="right" valign="middle" bgcolor="#DCE8CE">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MONTH:&nbsp;</font></div>
                            </td>
                            <td width="19%" height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                                <asp:DropDownList CssClass="select-field has-width-190px" ID="DropDownMonthList" runat="server"></asp:DropDownList>
                                <asp:DropDownList CssClass="select-field has-width-190px" ID="DropDownYearList" runat="server"></asp:DropDownList>
                                <asp:TextBox CssClass="input-field" ID="txtViewQuickReportDT" TextMode="Month" Visible="false" runat="server" />
                            </td>
                            <td width="53%" align="left" valign="middle" bgcolor="#EDF2E6"><font color="#FFFFFF" face="Courier New, Courier, mono">

                                <asp:Button runat="server" Text="Quick Report -Monthly" ID="btnQuickReportMonthly" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="160" OnClick="btnQuickReportMonthly_Click" />
                               <br />
                                <asp:Button runat="server" Text="Quick Report -Year To Date" ID="btnQuickReportYearly" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="190" OnClick="btnQuickReportYearly_Click" />

                            </font></td>
                        </tr>
                        <%--                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#DCE8CE">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">YEAR:&nbsp;</font></div>
                            </td>
                            <td height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                            </td>
                            <td height="30" align="left" valign="middle" bgcolor="#EDF2E6"><font color="#FFFFFF" face="Courier New, Courier, mono">

                            </font></td>
                        </tr>--%>
                        <tr bgcolor="#CBDCB8">
                            <td height="30">&nbsp;</td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#CBDCB8">&nbsp;</td>
                        </tr>
                    </table>
                    <br />

                    <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                        <tr align="center" valign="middle" bgcolor="#CEDFBD">
                            <td height="30" colspan="10">
                                <div align="center">
                                    <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                                        <h3 style="text-align: center;">List Of Application Deferred/Declined- According To Reasons</h3>
                                    </font>
                                </div>
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td width="26%" height="30" align="right" valign="middle" bgcolor="#D5E2C5"><font size="2"><font face="Verdana, Arial, Helvetica, sans-serif">PRODUCT:&nbsp;</font>
                            </font>
                                <div align="right"></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7">
                                <asp:DropDownList CssClass="select-field" ID="ProductListDropDown" runat="server" Width="200px"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">STATUS:&nbsp;</font></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7">

                                <asp:DropDownList CssClass="select-field" ID="statDropDownList" AppendDataBoundItems="true" runat="server">
                                    <asp:ListItem Text="Deffered" Value="2" />
                                    <asp:ListItem Text="Declined" Value="1" />
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MONTH:&nbsp;</font></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7">
                                <asp:DropDownList CssClass="select-field" ID="AppMonthDropDown" runat="server"></asp:DropDownList>
                                <%--<asp:TextBox CssClass="input-field" ID="txtAppMonthDropDownDT" TextMode="Month" runat="server"/>--%>
                                <asp:DropDownList CssClass="select-field" ID="AppYearDropDown" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <%--<tr bgcolor="BDD3A5">
                            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                                <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">YEAR:&nbsp;</font></div>
                            </td>
                            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7">
                            </td>
                        </tr>--%>
                        <tr bgcolor="#CEDFBD">
                            <td height="30"></td>
                            <td width="21%" height="30" align="left" valign="middle"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp; 
                            </font>
                            </td>
                            <td width="53%" align="left" valign="middle">
                                <asp:Button runat="server" Text="Show" ID="btnAppShow" Style="border: 1px solid #83b67a; background-color: #D8EED5; text-align: center; font-size: 13px;" Height="25" Width="50" OnClick="btnAppShow_Click" />

                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        initPageLoaded();
    </script>
</asp:Content>
