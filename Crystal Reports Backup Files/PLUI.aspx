﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_PLUI"
    MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="PLUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/js/PLCalculation.ascx" TagPrefix="uc1" TagName="PLCalculation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <uc1:PLCalculation runat="server" ID="PLCalculation" />

    <style type="text/css">
        .FontBold
        {
            font-weight: bold;
            font-family: Arial;
            font-size: 12px;
        }
        .FontColor
        {
            color: White;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div>
       <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
        </asp:ScriptManager>--%>
        <br />
        <table border="0" cellpadding="1" cellspacing="1" class="FontBold">
            <tr>
                <td>
                    <asp:Label ID="llIDLabel" runat="server" Text="LLID "></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="llidTextBox" runat="server" BackColor="#CCFFCC" 
                        Font-Bold="True" TabIndex="1" MaxLength="9"></asp:TextBox>
                    <asp:Button ID="findButton" runat="server" Text="..." 
                        OnClick="findButton_Click" TabIndex="2" />
                    &nbsp;
                    <asp:Label ID="messageLabel" runat="server" Font-Bold="true" ForeColor="DarkRed"
                        Font-Size="13px" Text=""></asp:Label>
                    &nbsp; &nbsp;
                </td>
                <td rowspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="professionLabel" runat="server" Font-Bold="true" Font-Size="13px"
                        Text=""></asp:Label>
                    <asp:Label ID="segmentCodeLabel" runat="server" Text=" Segment Code"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="segmentCodeDropDownList" runat="server" Width="120px" OnSelectedIndexChanged="segmentCodeDropDownList_SelectedIndexChanged"
                        BackColor="#CCFFCC" Font-Bold="True" TabIndex="3">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:HiddenField ID="educationLevelHiddenField" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="plLabel" runat="server" Text="PL/IPF/FL"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="plDropdownList" runat="server" Width="120px" OnSelectedIndexChanged="plDropdownList_SelectedIndexChanged"
                        BackColor="#CCFFCC" Font-Bold="True" TabIndex="4">
                        <asp:ListItem Value="0" Text="Select Loan">Select Loan</asp:ListItem>
                        <asp:ListItem Value="1" Text="PL">PL</asp:ListItem>
                        <asp:ListItem Value="4" Text="FL">FL</asp:ListItem>
                        <asp:ListItem Value="12" Text="IPF">IPF</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:HiddenField ID="jobDurationHiddenField" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="loanTypeLabel" runat="server" Text="Loan Type"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="loanTypeDropDownList" runat="server" Width="150px" OnSelectedIndexChanged="loanTypeDropDownList_SelectedIndexChanged"
                        AutoPostBack="True" BackColor="#CCFFCC" Font-Bold="True" TabIndex="5">
                        <asp:ListItem Value="1">NEW</asp:ListItem>
                        <asp:ListItem Value="2">ADDITIONAL</asp:ListItem>
                        <asp:ListItem Value="3">TOP UP</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:HiddenField ID="companyIdHiddenField" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="incAssmntMethodLabel" runat="server" Text="Inc Assmnt Method"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="incAssmntMethodDropDownList" runat="server" Width="200px" BackColor="#CCFFCC"
                                Font-Bold="True" TabIndex="6">
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:Label ID="sectorLabel" runat="server" Text="Sector"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="sectorDropDownList" runat="server" Width="250px" OnSelectedIndexChanged="sectorDropDownList_SelectedIndexChanged"
                        BackColor="#CCFFCC" Font-Bold="True" TabIndex="7">
                    </asp:DropDownList>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="companyNameLabel" runat="server" Text="Company Name "></asp:Label>
                    <asp:Label ID="companTypeLabel" runat="server" Text=" & Type"></asp:Label>
                </td>
                <td style="width:268px;">

                     <asp:DropDownList ID="companyNameDropDownList" runat="server" Width="190px" Enabled="true"
                        BackColor="#CCFFCC" Font-Bold="True" 
                        onselectedindexchanged="companyNameDropDownList_SelectedIndexChanged" AutoPostBack="True" 
                        >
                    </asp:DropDownList>

                    <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <asp:DropDownList ID="companyTypeDropDownList" runat="server" 
                        AutoPostBack="True" Width="50px" Enabled="true"
                        BackColor="#CCFFCC" Font-Bold="True" 
                        onselectedindexchanged="companyTypeDropDownList_SelectedIndexChanged">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">MNC</asp:ListItem>
                        <asp:ListItem Value="2">LLC</asp:ListItem>
                        <asp:ListItem Value="3">LC</asp:ListItem>
                    </asp:DropDownList>
                    </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="companyNameDropDownList" 
                                EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <input id="companyInfoButton" type="button" value="..." 
                        onclick="JS_FunctionShowCompanyInfo(companyInfoButton);" tabindex="9" />
                </td>
                <td>
                    <asp:DropDownList ID="categoryDropDownList" runat="server" Width="160px" 
                        AutoPostBack="True" BackColor="#CCFFCC" Font-Bold="True" 
                        onselectedindexchanged="categoryDropDownList_SelectedIndexChanged" 
                        TabIndex="10">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="A [MD/FCP/MRCP+3EXP+RH]">A [MD/FCP/MRCP+3EXP+RH]</asp:ListItem>
                        <asp:ListItem Value="B [MBBS+1EXP+RH]">B [MBBS+1EXP+RH]</asp:ListItem>
                        <asp:ListItem Value="C [MBBS+1EXP+H]">C [MBBS+1EXP+H]</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="subSectorLabel" runat="server" Text="Sub-Sector"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="subSectorDropDownList" runat="server" Width="200px" OnSelectedIndexChanged="subSectorDropDownList_SelectedIndexChanged"
                                BackColor="#CCFFCC" Font-Bold="True" TabIndex="8">
                            </asp:DropDownList>
                            <asp:TextBox ID="subsectorTextBox" runat="server" Width="40px" BackColor="#99CCFF"
                                Font-Bold="True"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="sectorDropDownList" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="subSectorDropDownList" EventName="SelectedIndexChanged">
                            </asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="subSectorDropDownList" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                        </Triggers>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="subSectorDropDownList" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="monthlySalaryIncomeLabel" runat="server" Text="Monthly Salary Income"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="monthlySalaryIncomeTextBox" runat="server" AutoPostBack="True" OnTextChanged="monthlySalaryIncomeTextBox_TextChanged"
                        BackColor="#CCFFCC" Font-Bold="True" TabIndex="11"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="percentageOfOwnershipLabel" runat="server" Text="Percentage of ownership"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="percentageOfOwnershipTextBox" runat="server" Width="99px" BackColor="#CCFFCC"
                        Font-Bold="True" AutoPostBack="true" ontextchanged="percentageOfOwnershipTextBox_TextChanged"></asp:TextBox>
                    <asp:Label ID="Label3" runat="server" Text="%" Font-Bold="true"></asp:Label>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="profitMarginLabel" runat="server" Text="Profit Margin"></asp:Label>
                </td>
                <td>
                   <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                           <asp:TextBox ID="profitMarginTextBox" runat="server" BackColor="#CCFFCC" 
                                Font-Bold="True" TabIndex="12" AutoPostBack="true"  ontextchanged="profitMarginTextBox_TextChanged"></asp:TextBox>
                            <asp:Label ID="profitMarginPersientLabel" runat="server" Text="%" Font-Bold="True"></asp:Label>
                        </ContentTemplate>
                        <Triggers>                        
                            <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:Label ID="overDreaftLabel" runat="server" Text="Overdraft/ Cash Credit/ Revolving Limit"></asp:Label>
                </td>
                <td>
                    &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="otherIncomeLabel" runat="server" Text="Other income (if any)"></asp:Label>
                    <asp:DropDownList ID="otherIncomeDropDownList" runat="server" Width="75px">
                        <asp:ListItem Value="0">Null</asp:ListItem>
                        <asp:ListItem Value="1">Rental</asp:ListItem>
                        <asp:ListItem Value="2">Pvt Practice</asp:ListItem>
                        <asp:ListItem Value="3">Business</asp:ListItem>
                        <asp:ListItem Value="4">Salary</asp:ListItem>
                        <asp:ListItem Value="5">Others</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="otherIncomeTextBox" runat="server" AutoPostBack="True" BackColor="#CCFFCC" 
                        Font-Bold="True" ontextchanged="otherIncomeTextBox_TextChanged"></asp:TextBox>
                </td>
                <td colspan="2" rowspan="2" valign="top">
                    <div style="border: solid 1px gray; width: 460px">
                        <asp:GridView ID="overDraftGridView" runat="server" PageSize="3" Width="100px" AutoGenerateColumns="False"
                            AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Limit" HeaderStyle-Width="80px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="limitTextBox" runat="server" Text='<%# Bind("Limit") %>' Width="80px"
                                            CssClass="rightTextBox" BackColor="#CCFFCC" 
                                            ontextchanged="limitTextBox_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                    </ItemTemplate>

<HeaderStyle Width="80px"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rate %" HeaderStyle-Width="80px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="rateTextBox" runat="server" Text='<%# Bind("Rate") %>' Width="80px"
                                            CssClass="rightTextBox" BackColor="#CCFFCC" 
                                            ontextchanged="rateTextBox_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </ItemTemplate>

<HeaderStyle Width="80px"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Q1" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="q1TextBox" runat="server" Text='<%# Bind("Q1") %>' Width="70px"
                                            CssClass="rightTextBox" BackColor="#CCFFCC" 
                                            ontextchanged="q1TextBox_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </ItemTemplate>

<HeaderStyle Width="70px"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Q2" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="q2TextBox" runat="server" Text='<%# Bind("Q2") %>' Width="70px"
                                            CssClass="rightTextBox" BackColor="#CCFFCC" 
                                            ontextchanged="q2TextBox_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </ItemTemplate>

<HeaderStyle Width="70px"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Q3" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="q3TextBox" runat="server" Text='<%# Bind("Q3") %>' Width="70px"
                                            CssClass="rightTextBox" BackColor="#CCFFCC" 
                                            ontextchanged="q3TextBox_TextChanged"  AutoPostBack="true"></asp:TextBox>
                                    </ItemTemplate>

<HeaderStyle Width="70px"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Q4" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="q4TextBox" runat="server" Text='<%# Bind("Q4") %>' Width="70px"
                                            CssClass="rightTextBox" BackColor="#CCFFCC" 
                                            ontextchanged="q4TextBox_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </ItemTemplate>

<HeaderStyle Width="70px"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="OldIdLabel" runat="server" Text='<%# Bind("Q4") %>' Width="20px" CssClass="rightTextBox"></asp:Label>
                                    </ItemTemplate>

<HeaderStyle Width="20px"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="netIncomeLabel" runat="server" Text="Net Income"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="netIncomeTextBox" runat="server" BorderColor="Black"
                        Font-Bold="True" AutoPostBack="true" ontextchanged="netIncomeTextBox_TextChanged"></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="declaredIncome" runat="server" Text="Declared Income"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="declaredIncomeTextBox" runat="server" AutoPostBack="True" OnTextChanged="declaredIncomeTextBox_TextChanged"
                        BackColor="#CCFFCC" Font-Bold="True"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="industryProfitLabel" runat="server" Text="Industry Profit Margin Rate"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="industryProfitTextBox" runat="server" Width="99px" BackColor="#99CCFF"
                                Font-Bold="True"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="subSectorDropDownList" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="scbCreditLimitLabel" runat="server" Text="SCB Credit Card Limit (aggregate)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="scbCreditLimitTextBox" runat="server" MaxLength="7" BackColor="#CCFFCC"
                        Font-Bold="True" TabIndex="13"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="propotionateMonthlyInterestLabel" runat="server" Text="Proportionate Monthly Interest"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="proportionateMonthlyInterestTextBox" runat="server" Width="99px"
                        BorderColor="Black" Font-Bold="True"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="plOutstandingLabel" runat="server" Text="PL + IPF + FL outstanding (aggregate)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="plOutstandingTextBox" runat="server" BackColor="#CCFFCC" 
                        Font-Bold="True" TabIndex="14"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="residentialStatusLabel" runat="server" Text="Residential Status"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="residentialStatusDropDownList" runat="server" Width="100px"
                        
                        OnSelectedIndexChanged="residentialStatusDropDownList_SelectedIndexChanged" BackColor="Green"
                        Font-Bold="True" TabIndex="18">
                        <asp:ListItem Value="0">Own</asp:ListItem>
                        <asp:ListItem Value="1">Family Owned</asp:ListItem>
                        <asp:ListItem Value="2">Others</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="flSecurityLabel" runat="server" Text="FL Security (If Less Than FL Outstanding)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="flSecurityTextBox" runat="server" BackColor="#CCFFCC" 
                        Font-Bold="True" TabIndex="15"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="averageBalanceLabel" runat="server" Text="50% of Average Balance"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="averageBalanceTextBox" runat="server" Width="99px" BackColor="#CCFFCC"
                        Font-Bold="True"></asp:TextBox>
                    &nbsp;
                    <asp:HiddenField ID="averageBalanceHiddenField" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="emiWithSCBLabel" runat="server" Text="EMI with SCB (if any)"></asp:Label>
                    <asp:DropDownList ID="emiWithSCBDropdownList" runat="server" Width="75px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="emiWithSCBTextBox" runat="server" BackColor="#CCFFCC" 
                        Font-Bold="True" TabIndex="16"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="instabuyBalanceLabel" runat="server" Text="Instabuy Balance (agg)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="instabuyBalanceTextBox" runat="server" Width="99px" BackColor="#CCFFCC"
                        Font-Bold="True"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="otherEMILabel" runat="server" Text="Other EMI (if any)"></asp:Label>
                    <asp:TextBox ID="otherEMITextBoxWithLabel" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="otherEMITextBox" runat="server" BackColor="#CCFFCC" 
                        Font-Bold="True" TabIndex="17"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="instabuyInstallmentLabel" runat="server" Text="Instabuy Installment (agg)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="instabuyInstallmentTextBox" runat="server" Width="99px" BackColor="#CCFFCC"
                        Font-Bold="True"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="1" cellspacing="1" class="FontBold">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div style="border: solid 1px gray;">
                        <table border="0" cellpadding="1" cellspacing="1">
                            <tr>
                                <td>
                                    <asp:Label ID="bBPDDAllowedMaxLoanAmountLabel" runat="server" Text="BB & PDD Allowed Max Loan Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="bBPDDAllowedMaxLoanAmountTextBox" runat="server" Width="99px" Text=""
                                                ReadOnly="True" BackColor="#FFFF99" Font-Bold="True"></asp:TextBox>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="dBRAllowableMaxLoanDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="loanTypeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="monthlySalaryIncomeTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="declaredIncomeTextBox" EventName="TextChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="dBRAllowableMaxLoanAmount" runat="server" Text="DBR allowable Max Loan Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="dBRAllowableMaxLoanTextBox1" runat="server" Width="99px" ReadOnly="True"
                                        BackColor="#FFFF99" Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="dBRAllowableMaxLoanTextBox2" runat="server" Width="50px" ReadOnly="True"
                                                BackColor="#99CCFF" Font-Bold="True"></asp:TextBox>
                                            <asp:Label ID="persentageLabel" runat="server" Text="%" Font-Bold="true"></asp:Label>
                                            <asp:HiddenField ID="dbrHF" runat="server" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="dBRAllowableMaxLoanDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="loanTypeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="residentialStatusDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="monthlySalaryIncomeTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="otherIncomeTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="profitMarginTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="declaredIncomeTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="netIncomeTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="percentageOfOwnershipTextBox" EventName="TextChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="overDraftGridView"  />
                                            </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:DropDownList ID="dBRAllowableMaxLoanDropDownList" runat="server" Width="65px"
                                        OnSelectedIndexChanged="dBRAllowableMaxLoanDropDownList_SelectedIndexChanged"
                                        BackColor="Green" Font-Bold="True">
                                        <asp:ListItem Value="L1" Text="L1">L1</asp:ListItem>
                                        <asp:ListItem Value="L2" Text="L2">L2</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="avgBalanceSupportedMaxLoanAmountLabel" runat="server" Text="Avg. Balance Supported Max Loan Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="avgBalanceSupportedMaxLoanAmountTextBox" runat="server" Width="99px"
                                        ReadOnly="True" BackColor="#FFFF99" Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="mEUAllowableMaxLoanAmountLabel" runat="server" Text="MUE Allowable Max Loan Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="mUEAllowableMaxLoanAmountTextBox" runat="server" Width="99px" ReadOnly="True"
                                        BackColor="#FFFF99" Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="maxLoanAmountAllowableMUETextBox" runat="server" Width="50px" ReadOnly="True"
                                                BackColor="#99CCFF" Font-Bold="True"></asp:TextBox>
                                            <asp:Label ID="multipliarLabel" runat="server" Text="X" Font-Bold="true"></asp:Label>
                                            <asp:HiddenField ID="gMueHiddenField" runat="server" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="oneAddDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="loanTypeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="categoryDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="companyNameDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="companyTypeDropDownList" EventName="SelectedIndexChanged" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="oneAddDropDownList" runat="server" Width="65px" OnSelectedIndexChanged="oneAddDropDownList_SelectedIndexChanged"
                                                BackColor="Green" Font-Bold="True">
                                                <asp:ListItem Value="0" Text="+1No">+1No</asp:ListItem>
                                                <asp:ListItem Value="1" Text="+1Yes">+1Yes</asp:ListItem>
                                                <asp:ListItem Value="2" Text="+2Yes">+2Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="loanTypeDropDownList" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="appliedLoanAmountLabel" runat="server" Text="Applied Loan Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="appliedLoanAmountTextBox" runat="server" Width="99px" BackColor="#CCFFCC"
                                        Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="appropriatedLoanAmountLabel" runat="server" Text="Appropriated Loan Amount"
                                        Font-Bold="True" ForeColor="#0000CC"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="appropriatedLoanAmountTextBox" runat="server" Width="99px" ReadOnly="True"
                                        BackColor="#0000CC" Font-Bold="True" ForeColor="White" 
                                        CssClass="FontColor" TabIndex="22"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="proposedLoanAmountLabel" runat="server" Text="Proposed Loan Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="proposedLoanAmountTextBox" runat="server" Width="99px" BackColor="#CCFFCC"
                                        Font-Bold="True" TabIndex="23"></asp:TextBox>
                                </td>
                                <td colspan="3">
                                    &nbsp; &nbsp;
                                    <asp:Label ID="labelDevisionLabel" runat="server" ForeColor="#FF3300"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="bancaLoanAmountLabel" runat="server" Text="Proposed Loan Amount (Including BANCA)"
                                        BackColor="#FFCC99"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="bancaLoanAmountTextBox" runat="server" Width="99px" ReadOnly="True"
                                        BackColor="#FFCC99" Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="bancaLabel" runat="server" Text="BANCA"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="bancaTextBox" runat="server" Width="65px" ReadOnly="True" BackColor="#FFCC99"
                                        Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="emilabel" runat="server" Text="EMI"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="emiTextBox" runat="server" Width="99px" ReadOnly="True" Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="emiFactorLabel" runat="server" Text="EMI Factor"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="emiFactorTextBox" runat="server" Width="65px" ReadOnly="True" Font-Bold="True"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="bancaWithoutDBRLabel" runat="server" Text="BANCA WITHOUT DBR"></asp:Label>
                                    &nbsp;
                                    <asp:TextBox ID="bancaWithoutDBRTextBox" runat="server" Width="65px" ReadOnly="True"
                                        BackColor="#FFCC99" Font-Bold="True"></asp:TextBox>
                                        <asp:Label ID="Label2" runat="server" Text="%" Font-Bold="true"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="dbrLabel" runat="server" Text="DBR"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="dbrTextBox" runat="server" Width="99px" ReadOnly="True" Font-Bold="True"></asp:TextBox>
                                    <asp:Label ID="Label4" runat="server" Text="%" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="bancaDBRLabel" runat="server" Text="BANCA DBR"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="bancaDBRTextBox" runat="server" Width="65px" ReadOnly="True" BackColor="#FFCC99"
                                        Font-Bold="True"></asp:TextBox>
                                    <asp:Label ID="bancaDBRPerLabel" runat="server" Text="%" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="mueLabel" runat="server" Text="MUE"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="mueTextBox" runat="server" Width="99px" ReadOnly="True" Font-Bold="True"></asp:TextBox>
                                    <asp:Label ID="Label5" runat="server" Text="X" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="bancaMueLabel" runat="server" Text="BANCA MUE"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="bancaMUETextBox" runat="server" Width="65px" ReadOnly="True" BackColor="#FFCC99"
                                        Font-Bold="True"></asp:TextBox>
                                    <asp:Label ID="bancaMUEXLabel" runat="server" Text="X" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td valign="top">
                    <table border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td>
                                <asp:Label ID="interestRateLabel" runat="server" Text="Interest Rate"></asp:Label>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="interestRateTextBox" runat="server" Width="56px" BackColor="#CCFFCC"
                                            Font-Bold="True" TabIndex="19"></asp:TextBox>
                                        <asp:Label ID="Label1" runat="server" Text="%" Font-Bold="true"></asp:Label>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="companyNameDropDownList" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="loanTypeDropDownList" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="tenureLabel" runat="server" Text="Tenure"></asp:Label>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="tenureTextBox" runat="server" Width="56px" BackColor="#CCFFCC" 
                                            Font-Bold="True" TabIndex="20"></asp:TextBox>
                                        <asp:HiddenField ID="segmentWiseMinLoanAmountHiddenField" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="segmentCodeDropDownList" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="loanTypeDropDownList" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="firstPaymentLabel" runat="server" Text="1st Payment"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="firstPaymentDropDownList" runat="server" Width="65px" BackColor="#CCFFCC"
                                    Font-Bold="True">
                                    <asp:ListItem Value="30">30</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="safetyPlusLabel" runat="server" Text="Banca"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="safetyPlusDropDownList" runat="server" Width="65px" BackColor="#CCFFCC"
                                    Font-Bold="True" TabIndex="21">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="2">SELF FIN</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="currentAgeLabel" runat="server" Text="Current Age " Font-Bold="True"
                                    ForeColor="#0000CC"></asp:Label>
                                <asp:Label ID="ageLabel" runat="server" BackColor="#99CCFF"></asp:Label>
                                <asp:HiddenField ID="ageHF" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="cashsecurityLabel" runat="server" Text="Cash Security"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="securityDropDownList" runat="server" Width="65px">
                                    <asp:ListItem Value="30%">30%</asp:ListItem>
                                    <asp:ListItem Value="50%">50%</asp:ListItem>
                                    <asp:ListItem Value="100%">100%</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td valign="top">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div style="width: 625px; height: 130px; z-index: 2; border: solid 0px gray;" visible="true">
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="remarksLabel" runat="server" Text="Remarks :"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 625px; height: 118px" valign="top">
                                    <asp:TextBox ID="remarksTextBox" runat="server" Width="622px" Height="118px" 
                                        TextMode="MultiLine" TabIndex="24"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td valign="top">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td valign="top">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="center">
                    <asp:Button ID="deferButton" runat="server" Text="Defer" Width="100px" Height="25px"
                        OnClick="deferButton_Click" Visible="False" />
                    <asp:Button ID="declineButton" runat="server" Text="Decline" Width="100px" Height="25px"
                        OnClick="declineButton_Click" Visible="False" />
                    <asp:Button ID="forwardButton" runat="server" Text="Forward" Width="100px" Height="25px"
                        OnClick="forwardButton_Click" TabIndex="25" />
                    <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" Height="25px"
                        OnClick="clearButton_Click" TabIndex="26" />
                </td>
                <td valign="top">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
