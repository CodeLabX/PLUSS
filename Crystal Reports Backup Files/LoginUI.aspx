﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.LoginUI" CodeBehind="LoginUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/loginCss.ascx" TagPrefix="uc1" TagName="loginCss" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
    <uc1:loginCss runat="server" ID="loginCss" />
    <style>
        body {
            background-color: #9ddc9a;
            font-family: 'Ubuntu', sans-serif;
        }

        .main {
            background-color: #FFFFFF;
            width: 400px;
            height: 400px;
            margin: 7em auto;
            border-radius: 1.5em;
            box-shadow: 0px 11px 35px 2px rgba(0, 0, 0, 0.14);
        }

        .sign {
            padding-top: 40px;
            color: #3dae38;
            font-family: 'Ubuntu', sans-serif;
            font-weight: bold;
            font-size: 23px;
        }

        .un {
            width: 76%;
            color: rgb(38, 50, 56);
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 1px;
            background: rgba(136, 126, 126, 0.04);
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            outline: none;
            box-sizing: border-box;
            border: 2px solid rgba(0, 0, 0, 0.30);
            margin-bottom: 50px;
            margin-left: 46px;
            text-align: left;
            margin-bottom: 27px;
            font-family: 'Ubuntu', sans-serif;
        }

        form.form1 {
            padding-top: 40px;
        }

        .pass {
            width: 76%;
            color: rgb(38, 50, 56);
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 1px;
            background: rgba(136, 126, 126, 0.04);
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            outline: none;
            box-sizing: border-box;
            border: 2px solid rgba(0, 0, 0, 0.30);
            margin-bottom: 50px;
            margin-left: 46px;
            text-align: left;
            margin-bottom: 27px;
            font-family: 'Ubuntu', sans-serif;
        }

            .un:focus, .pass:focus {
                border: 2px solid #8cda89 !important;
            }

        .submit {
            cursor: pointer;
            border-radius: 5px;
            color: #fff;
            background: linear-gradient(to right, #3dae38, #8cda89);
            border: 0;
            padding-left: 40px;
            padding-right: 40px;
            padding-bottom: 10px;
            padding-top: 10px;
            font-family: 'Ubuntu', sans-serif;
            margin-left: 35%;
            font-size: 13px;
            box-shadow: 0 0 20px 1px rgba(0, 0, 0, 0.04);
        }

        .forgot {
            text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12);
            color: #E1BEE7;
            padding-top: 15px;
        }

        a {
            text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12);
            color: #E1BEE7;
            text-decoration: none
        }

        @media (max-width: 600px) {
            .main {
                border-radius: 0px;
            }
    </style>
</head>
<body>
    <div class="main">
        <p class="sign" align="center">PLUSS</p>
        <form class="form1" runat="server" autocomplete="off" method="post">
            <div runat="server" id="loginTable">
                <%--<input class="un " type="text" align="center" placeholder="Username">--%>
                <asp:Label ID="conMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                <asp:TextBox ID="userIdTextBox" runat="server" CssClass="un" MaxLength="10" placeholder="Enter your PSID"></asp:TextBox>
                <input type="password" id="passwordTextBox" name="password" class="pass" runat="server" placeholder="Enter your password" />
                <asp:Button CssClass="submit" ID="logInButton" runat="server" Text="Log In" OnClick="logInButton_Click" />
            </div>
            <table runat="server" id="messageTable">
                <tr>
                    <td colspan="2">
                        <br />
                        <asp:Label runat="server" ID="lblMessage"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" ID="btnOK" Text="OK" Width="120px" OnClick="btnOK_Click" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnCancel" Text="CANCEL" Width="120px" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </form>
    </div>

</body>
</html>
