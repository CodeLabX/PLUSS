﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.UI_WatchListUploadUI"
    Title="WatchList Upload" CodeBehind="WatchListUploadUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/azGridTableCss.ascx" TagPrefix="uc1" TagName="azGridTableCss" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Watch List Upload</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:FileUpload CssClass="input-field" ID="FileUpload" runat="server" Width="350px" Height="32px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="btn primarybtn" ID="UploadButton" runat="server" Text="Upload" Width="100px"
                        Height="35px" OnClick="uploadButton_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="btn clearbtn" ID="btnFlush" runat="server" Text="FLUSH" Width="100px"
                        Height="35px" OnClick="btnFlush_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvRecords" runat="server" GridLines="None" CssClass="myGridStyle"
                        Font-Size="11px" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="BankCompany" HeaderText="BANK / COMPANY" />
                            <asp:BoundField DataField="Branch" HeaderText="BRANCH" />
                            <asp:BoundField DataField="Address" HeaderText="ADDRESS" />
                            <asp:BoundField DataField="NegetiveList" HeaderText="NEGATIVE LIST" />
                            <asp:BoundField DataField="WatchList" HeaderText="WATCH LIST" />
                            <asp:BoundField DataField="Exclusion" HeaderText="EXCLUSION" />
                            <asp:BoundField DataField="Delist" HeaderText="DELIST" />
                            <asp:BoundField DataField="Restriction" HeaderText="RESTRICTION" />
                            <asp:BoundField DataField="Remarks" HeaderText="REMARKS" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>


    <script type="text/javascript">
        function JS_FunctionClear() {
            mygrid.clearAll();
        }
    </script>
    <br />
    <br />
    <br />
    <br />
</asp:Content>

