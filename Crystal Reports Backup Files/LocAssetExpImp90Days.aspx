﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.Master" AutoEventWireup="true" CodeBehind="LocAssetExpImp90Days.aspx.cs" Inherits="PlussAndLoan.UI.LocReports.LocAssetExpImp90Days" %>

<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        A {
            COLOR: #333333;
            TEXT-DECORATION: none;
        }

            A:hover {
                TEXT-DECORATION: underline;
            }

        .LabelHead {
            font-size: 14px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .LabelSubHead {
            font-size: 12px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .TableHead {
            font-size: 16px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .label {
            font-size: 18px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .tableWidth {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }

            .tableWidth td {
                border-width: 1px;
                border-style: solid;
                border-collapse: collapse;
                border-color: Black;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Asset Exp/Imp Report</div>

       
            <%--<fieldset>
                <legend>Process Status Upload</legend>
                <label for="field1">
                    <asp:FileUpload CssClass="input-field" Width="400" runat="server" />
                    <asp:Button CssClass="btn primarybtn has-width-150px" runat="server" ID="btnUpload" Text="Upload" />
                </label>
            </fieldset>--%>
            <fieldset>
                <legend>Process Status Download</legend>
                <table style="border-spacing: 10px">
                    <tr>
                        <td>
                            <label for="field1">
                                <span>Start Date:</span>
                                <asp:TextBox ID="txtStartDateLoc" runat="server" CssClass="input-field"/>
                            </label>
                        </td>
                        <td>
                            <label for="field1">
                                <span>End Date:</span>
                                <asp:TextBox ID="txtToDateLoc" runat="server" CssClass="input-field is-uppercase"/>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="field1">
                                <span>Product Name:</span>
                                <asp:DropDownList ID="sel_product" AutoPostBack="false" runat="server" CssClass="select-field"></asp:DropDownList>
                            </label>
                        </td>
                        <td>
                            <label for="field1">
                                <span>Process Status:</span>
                                <asp:DropDownList ID="sel_loanStatus" AutoPostBack="false" runat="server" CssClass="select-field"></asp:DropDownList>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="field1">
                                <span>Department Name:</span>
                                <asp:DropDownList ID="cmbDepartment" AutoPostBack="false" runat="server" CssClass="select-field"></asp:DropDownList>
                            </label>
                        </td>
                        <td>
                            <label for="field1">
                                <asp:Button CssClass="btn primarybtn" ID="btn_search" OnClick="Search_onClick__" Text="SEARCH" runat="server" />
                                &nbsp;<a href="javascript:void(0);"><input class="btn primarybtn" type="submit" name="btn_export" onserverclick="ExportCsv" runat="server" id="Submit1" value="Export CSV" /></a>
                            </label>
                        </td>
                    </tr>
                </table>
                <input type="hidden" id="hid_cleanupstatus" name="hid_cleanupstatus" value="" />
                <input type="hidden" id="hid_personId" name="hid_personId" />
                <input type="hidden" name="hid_deptid" id="hid_deptid" />
            </fieldset>
        </section>



        <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="top">
                    <%--<fieldset style="width: 768px;">
                        <legend>Process Status Upload</legend>
                        <table width="100%" border="0">
                            <tr>
                                <td width="25%" align="right"></td>
                                <td width="25%" align="right">
                                    <asp:FileUpload runat="server" />
                                </td>
                                <td width="25%" align="right">
                                    <asp:Button runat="server" ID="btnUpload" Text="Upload" Style="border: 1px solid #84C3E7; background-color: #C2DBC2" />
                                </td>
                                <td width="25%" align="right"></td>
                            </tr>
                        </table>
                    </fieldset>--%>
                    <fieldset>
                        <legend id="leg_processResultDisplay">Download Process Result</legend>
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <label id="lab_pleasewait"></label>
                                    <div id="div_processResultDisplay" style="width: 100%; height: 260px; border: 1px solid #ddd; overflow: auto;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $(function () {
            debugger;
            var now = new Date();
            var minDate = now.setDate(now.getDate() + 90);


            $('#<%= txtStartDateLoc.ClientID %>, #<%= txtToDateLoc.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '/Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= ExpImpMaxMinDateAllowed.ToString("yyyy-MM-dd") %>"),
                value: '2022-09-01'
            });

        });
    </script>
</asp:Content>
