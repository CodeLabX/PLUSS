﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_BSVERUI" MasterPageFile="~/UI/SupportMasterPage.master" CodeBehind="BSVERUI.aspx.cs" %>

<%--<%@ Register Src="~/UI/UserControls/js/BSVERValidation.ascx" TagPrefix="uc1" TagName="BSVERValidation" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--  <uc1:BSVERValidation runat="server" ID="BSVERValidation" />--%>
    <title id="basVar" runat="server" title="BSVAR">BAS VAR</title>

    <script type="text/javascript">
        function jsFn_IsNumber(obj) {
            MyStr = new String();
            MyStr = document.getElementById(obj).value;
            for (var i = 0 ; i < MyStr.length; i++) {
                if (isNaN(parseInt(MyStr.charAt(i)))) {
                    document.getElementById(obj).value = "";
                    if (i != 0) {
                        for (var j = 0; j < i; j++) {
                            document.getElementById(obj).value += MyStr.charAt(j);
                        }
                    }//if
                    return false;
                }//if
            }//for
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table border="0" cellpadding="1" cellspacing="1">
        <tr>
            <td>
                <table border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td style="font-weight: bold;">
                            <asp:Label ID="lLIdLabel" runat="server" Text="LLId"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="lLIdTextBox" runat="server" MaxLength="9"></asp:TextBox>
                            <input id="findButton" type="button" value="..." style="width: 20px;" onclick="JS_FunctionLoadBankNameList()" />
                            <asp:Label ID="errorMsgLabel" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 150px">
                            <asp:Label ID="statementTypeLabel" runat="server" Text="Statement Type"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="statementTypeDropDownList" runat="server" Width="160px">
                                <asp:ListItem Value="0">Select statement type</asp:ListItem>
                                <asp:ListItem Value="1">Document</asp:ListItem>
                                <asp:ListItem Value="2">Email</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="needFild1Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="bankNameLabel" runat="server" Text="Bank Name"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="bankNameDropDownList" runat="server" Width="270px"
                                Enabled="False">
                            </asp:DropDownList>
                            <asp:Label ID="needFild2Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="bankIdHiddenField" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="branchLabel" runat="server" Text="Branch Name"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="branchNameDropDownList" runat="server" Width="270px"
                                Enabled="False">
                            </asp:DropDownList>
                            <asp:HiddenField ID="branchIdHiddenField" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="subjectLabel" runat="server" Text="Subject"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="subjectDropDownList" runat="server" Width="270px"></asp:DropDownList>
                            <asp:Label ID="needFild3Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="dearLabel" runat="server" Text="Dear"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="dearTextBox" runat="server" Width="265px"></asp:TextBox>
                            <asp:Label ID="needFild6Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="emailAddressLabel" runat="server" Text="Email Address"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="emailAddressTextBox" runat="server" Width="265px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="scbAddressLabel" runat="server" Text="SCB Email Address"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="scbAddressTextBox" runat="server" Width="265px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold"></td>
                        <td>
                            <asp:Label ID="emailTextLabel" runat="server" Text="Will Appreciate if you kindly confirm the transactions mentioned below."></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="CustomerNameLabel" runat="server" Text="Customer Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="customerNameTextBox" runat="server" Width="265px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>


                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="acNoLabel" runat="server" Text="A/C No. & Type"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="acNoTextBox" runat="server" Width="130px" ReadOnly="true"></asp:TextBox>
                            <asp:DropDownList ID="acTypeDropDownList" runat="server" Width="130px"
                                Enabled="false">
                            </asp:DropDownList>
                            <asp:HiddenField ID="acTypeIdHiddenField" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            <asp:Label ID="acNameLabel" runat="server" Text="A/C Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="acNameTextBox" runat="server" Width="265px" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>


                </table>



            </td>

            <td>
                <table border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td colspan="2" style="height: 110px" valign="top">
                            <div id="gridbox" style="background-color: white; width: 500px; height: 125px;"></div>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle"></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">
                            <asp:Label ID="customerAddressLabel" runat="server" Text="Customer Address"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="customerAddressTextBox" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold" valign="middle">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>

    <table style="width: 733px">
        <tr>
            <td>
                <div style="border: solid 1px gray;">
                    <asp:GridView ID="BSVARGridView" runat="server" AutoGenerateColumns="false"
                        Width="820px">
                        <Columns>
                            <asp:TemplateField HeaderText="SL. No" ControlStyle-Width="50px">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle CssClass="ssRowLabel" />
                                <ControlStyle Width="50px"></ControlStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Particular">
                                <ItemTemplate>
                                    <asp:Label ID="particularLabel" runat="server" Width="200px" CssClass="centerTextBox"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date (dd-mm-yyyy)">
                                <ItemTemplate>
                                    <asp:TextBox ID="dateTextBox" runat="server" Width="140px" CssClass="ssTextBox" MaxLength="10"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <asp:TextBox ID="descriptionTextBox" runat="server" Width="150px" CssClass="ssTextBox"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Withdraw">
                                <ItemTemplate>
                                    <asp:TextBox ID="withdrawlTextBox" runat="server" Width="100px" CssClass="rightTextBox"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Deposit">
                                <ItemTemplate>
                                    <asp:TextBox ID="dipositTextBox" runat="server" Width="100px" CssClass="rightTextBox"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Balance">
                                <ItemTemplate>
                                    <asp:TextBox ID="balanceTextBox" runat="server" Width="100px" CssClass="rightTextBox"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="oldIdTextBox" runat="server"></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="FixedHeader" Font-Size="9pt" Height="20px" />
                    </asp:GridView>
                </div>
            </td>
        </tr>

        <tr>
            <td>&nbsp
            </td>
        </tr>

        <tr>
            <td style="font-weight: bold">
                <asp:Label ID="approvedByLabel" runat="server" Text="Approved By"></asp:Label>
                &nbsp&nbsp&nbsp&nbsp
    <asp:DropDownList ID="approverDropDownList" runat="server" Width="150px"></asp:DropDownList>
            </td>
        </tr>

        <tr>
            <td>&nbsp
            </td>
        </tr>

        <tr>
            <td style="font-weight: bold">
                <asp:Label ID="designationLabel" runat="server" Text="Designation"></asp:Label>
                &nbsp&nbsp&nbsp&nbsp
    <asp:Label ID="designationLable2" runat="server" Text="............................."></asp:Label>
                &nbsp
    <asp:Label ID="Label1" runat="server" Text="Personal Loan"></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="font-weight: bold">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <asp:Label ID="Label2" runat="server" Text="Consumer Banking"></asp:Label>
            </td>
        </tr>


        <tr>
            <td>&nbsp
            </td>
        </tr>

        <tr>
            <td>
                <table>
                    <tr>
                        <td style="width: 600px" align="center">
                            <asp:Button ID="saveButton" runat="server" Text="Save" Width="100px"
                                Height="32px" OnClick="saveButton_Click" />
                            <%--        <asp:Button ID="clearAllButton" runat="server" Text="Clear all" Width="105px" 
            Height="32px" onclick="clearAllButton_Click" />--%>
                            <input id="clearAllButton" type="button" value="Clear all" style="width: 105px; height: 32px;" onclick="Js_functionClear(); JS_FunctionBSVERGridClear(); JS_GridClear()" />
                            <asp:Button ID="printButton" runat="server" Text="Print" Width="100px"
                                Height="32px" OnClick="printButton_Click" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <div style="display: none" runat="server" id="bsverDiv"></div>
    <script type="text/javascript">
        var mygrid;
        mygrid = new dhtmlXGridObject('gridbox');
        mygrid.setImagePath("../codebase/imgs/");
        mygrid.setHeader("Sl,BankId,BankName,BranchId,BranchName,A/C No,StatementTypeId,SubjectId,Subject,ACTypeId,LLID");
        mygrid.setInitWidths("25,0,103,0,120,100,0,0,135,0,0");
        mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left");
        mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
        mygrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str");
        mygrid.setSkin("light");
        mygrid.init();
        mygrid.enableSmartRendering(true, 5);
        //mygrid.loadXML("../includes/BSVERBankList.xml");
        var aa = document.getElementById("bsver_data");
        if (aa != null) {
            mygrid.parse(document.getElementById("bsver_data"));
        }
        
        mygrid.attachEvent("onRowSelect", DoOnRowSelected);
        function DoOnRowSelected() {
            if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {

                //document.getElementById('ctl00_ContentPlaceHolder_lLIdTextBox').value = mygrid.cells(mygrid.getSelectedId(),10).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_bankNameDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 1).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_bankIdHiddenField').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 1).getValue());
                document.getElementById('ctl00_ContentPlaceHolder_branchNameDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 3).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_branchIdHiddenField').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 3).getValue());
                document.getElementById('ctl00_ContentPlaceHolder_acNoTextBox').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_acTypeDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 9).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_acTypeIdHiddenField').value = mygrid.cells(mygrid.getSelectedId(), 9).getValue();
                JS_FunctionLoadCustomerInfo(
                    parseInt(mygrid.cells(mygrid.getSelectedId(), 10).getValue()),
                    parseInt(mygrid.cells(mygrid.getSelectedId(), 1).getValue()),
                    parseInt(mygrid.cells(mygrid.getSelectedId(), 3).getValue()),
                    parseInt(mygrid.cells(mygrid.getSelectedId(), 5).getValue()));
                JS_FunctionBSVERGridClear();
                if (mygrid.cells(mygrid.getSelectedId(), 7).getValue() > 0) {
                    document.getElementById('ctl00_ContentPlaceHolder_statementTypeDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 6).getValue();
                    JS_functionButtonTextChange();
                    document.getElementById('ctl00_ContentPlaceHolder_subjectDropDownList').value = mygrid.cells(mygrid.getSelectedId(), 7).getValue();
                    LoadBSVEREntryData(
                        parseInt(mygrid.cells(mygrid.getSelectedId(), 10).getValue()),
                        parseInt(mygrid.cells(mygrid.getSelectedId(), 1).getValue()),
                        parseInt(mygrid.cells(mygrid.getSelectedId(), 3).getValue()),
                        parseInt(mygrid.cells(mygrid.getSelectedId(), 5).getValue()),
                        parseInt(mygrid.cells(mygrid.getSelectedId(), 7).getValue()),
                        parseInt(mygrid.cells(mygrid.getSelectedId(), 6).getValue()));
                }
                else {
                    JS_functionButtonTextChange();
                    document.getElementById('ctl00_ContentPlaceHolder_statementTypeDropDownList').value = 0;
                    document.getElementById('ctl00_ContentPlaceHolder_subjectDropDownList').value = 0;
                    document.getElementById('ctl00_ContentPlaceHolder_approverDropDownList').value = 0;
                    document.getElementById('ctl00_ContentPlaceHolder_designationLable2').innerHTML = "..........................";

                }
            }
        }
        function JS_FunctionLoadGrid() {
            var LLId = document.getElementById('ctl00_ContentPlaceHolder_loanLocatorIdTextBox').value;
            var FileName = '../UI/BankListXML.aspx';
            mygrid.loadXML(FileName + '?' + '&numLLId=' + LLId);
        }
        function JS_GridClear() {
            mygrid.clearAll();
            document.getElementById('ctl00_ContentPlaceHolder_saveButton').disabled = false;
        }
        function Js_functionClear() {
            document.getElementById('ctl00_ContentPlaceHolder_bankNameDropDownList').value = 0;
            document.getElementById('ctl00_ContentPlaceHolder_bankIdHiddenField').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_branchNameDropDownList').value = 0;
            document.getElementById('ctl00_ContentPlaceHolder_branchIdHiddenField').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_acNoTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_acTypeDropDownList').value = 0;
            document.getElementById('ctl00_ContentPlaceHolder_acTypeIdHiddenField').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_scbAddressTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_dearTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_emailAddressTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_acNameTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_customerNameTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_customerAddressTextBox').value = "";
            document.getElementById('ctl00_ContentPlaceHolder_statementTypeDropDownList').value = 0;
            document.getElementById('ctl00_ContentPlaceHolder_subjectDropDownList').value = 0;
            document.getElementById('ctl00_ContentPlaceHolder_approverDropDownList').value = 0;
            document.getElementById('ctl00_ContentPlaceHolder_designationLable2').innerHTML = "..........................";
        }

        var ajax_list_objects = new Array();
        var ajax_objects = new Array();

        function JS_FunctionLoadCustomerInfo(lLID, bankId, branchId, acNo) {
            debugger;
            var FileName = '../UI/BSVerificationPersonLoad.aspx';
            var ajaxIndex = ajax_objects.length;
            ajax_objects[ajaxIndex] = new sack();
            lLID = document.getElementById('ctl00_ContentPlaceHolder_lLIdTextBox').value;
            var url = FileName + '?' + '&LLID=' + lLID + '&bankId=' + bankId + '&branchId=' + branchId + '&aCNo=' + acNo;
            //alert(url);
            ajax_objects[ajaxIndex].requestFile = url;	// Specifying which file to get
            ajax_objects[ajaxIndex].onCompletion = function () {
                ajax_showCustomerInfo(ajaxIndex);
            };
            ajax_objects[ajaxIndex].runAJAX();		// Execute AJAX function
        }
        function ajax_showCustomerInfo(ajaxIndex) {
            var arrayData = new Array();
            var responseData = ajax_objects[ajaxIndex].response;
            //alert(responseData);
            if (responseData.length > 0) {
                arrayData = responseData.split('#');
                if (arrayData[0].length > 0) {
                    document.getElementById('ctl00_ContentPlaceHolder_dearTextBox').value = arrayData[0];
                    document.getElementById('ctl00_ContentPlaceHolder_emailAddressTextBox').value = arrayData[1];
                }
                else {
                    document.getElementById('ctl00_ContentPlaceHolder_dearTextBox').value = "";
                    document.getElementById('ctl00_ContentPlaceHolder_emailAddressTextBox').value = "";
                }
                document.getElementById('ctl00_ContentPlaceHolder_acNameTextBox').value = arrayData[2];
                document.getElementById('ctl00_ContentPlaceHolder_customerNameTextBox').value = arrayData[3];
                document.getElementById('ctl00_ContentPlaceHolder_customerAddressTextBox').value = arrayData[4];
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_dearTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_emailAddressTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_acNameTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_customerNameTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_customerAddressTextBox').value = "";

            }

        }
        function JS_FunctionLoadBankNameList() {
            debugger;
            var LLId = document.getElementById('ctl00_ContentPlaceHolder_lLIdTextBox').value;
            if (LLId.length > 0) {
                var FileName = '../UI/BSVERBankList.aspx';
                var ajaxIndex = ajax_objects.length;
                ajax_objects[ajaxIndex] = new sack();
                var url = FileName + '?' + '&BsvrLLId=' + LLId;
                ajax_objects[ajaxIndex].requestFile = url;	// Specifying which file to get
                ajax_objects[ajaxIndex].onCompletion = function () {
                    ajax_LoadBankNameList(ajaxIndex);
                };
                ajax_objects[ajaxIndex].runAJAX();		// Execute AJAX function
            }
            else {
                alert("Please enter LLId");
                document.getElementById('ctl00_ContentPlaceHolder_lLIdTextBox').focus();
            }

        }
        function ajax_LoadBankNameList(ajaxIndex) {
            var TypeData = new Array();
            var responseData = ajax_objects[ajaxIndex].response;
            TypeData = responseData.split(',');
            if (TypeData[0] != 0) {
                mygrid.clearAll();
                // mygrid.loadXML("../includes/BSVERBankList.xml");
                document.getElementById("ctl00_ContentPlaceHolder_bsverDiv").innerHTML = responseData;
                mygrid.parse(document.getElementById("xml_data"));
                if (TypeData[1] == 5 || TypeData[1] == 15) {
                    //document.getElementById('ctl00_ContentPlaceHolder_saveButton').disabled=true;
                }
                else {
                    //document.getElementById('ctl00_ContentPlaceHolder_saveButton').disabled=false;
                }
                //document.getElementById('ctl00_ContentPlaceHolder_entryUserName').innerHTML =TypeData[2];	            
            }
            else {
                alert("No other bank entry yet!");
                mygrid.clearAll();
            }
        }

        function LoadBSVEREntryData(lLID, bankId, branchId, acNo, subjectId, statemenTypeId) {
            debugger;
            var FileName = '../UI/TempBSVERData.aspx';
            var ajaxIndex = ajax_objects.length;
            ajax_objects[ajaxIndex] = new sack();
            lLID = document.getElementById('ctl00_ContentPlaceHolder_lLIdTextBox').value;
            var url = FileName + '?' + '&LLID=' + lLID + '&bankId=' + bankId + '&branchId=' + branchId + '&aCNo=' + acNo + '&subjectId=' + subjectId + '&statemenTypeId=' + statemenTypeId;
            ajax_objects[ajaxIndex].requestFile = url;
            ajax_objects[ajaxIndex].onCompletion = function () {
                ajax_showResponse(ajaxIndex);
            };
            ajax_objects[ajaxIndex].runAJAX();
        }
        function ajax_showResponse(ajaxIndex) {
            var arrayData = new Array();
            var BSVERHeadDataArray = new Array();
            var BSVERDetailsArray = new Array();
            var BSVERDatatArray = new Array();
            var responseData = ajax_objects[ajaxIndex].response;
            //alert(responseData);
            if (responseData.length > 0) {
                JS_FunctionBSVERGridClear();
                arrayData = responseData.split('*');
                arrayData[0] = arrayData[0].substr(0, arrayData[0].length - 1);
                BSVERDetailsArray = arrayData[0].split('#');
                var i = 2;
                for (var m = 0; m < BSVERDetailsArray.length; m++) {
                    BSVERDatatArray = BSVERDetailsArray[m].split(',');
                    document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_dateTextBox').value = BSVERDatatArray[0];
                    document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_descriptionTextBox').value = BSVERDatatArray[1];
                    document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_withdrawlTextBox').value = BSVERDatatArray[2];
                    document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_dipositTextBox').value = BSVERDatatArray[3];
                    document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_balanceTextBox').value = BSVERDatatArray[4];
                    document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_oldIdTextBox').value = BSVERDatatArray[5];
                    i = i + 1;
                    if (i == 6) {
                        BSVERHeadDataArray = arrayData[1].split(',');
                        document.getElementById('ctl00_ContentPlaceHolder_scbAddressTextBox').value = BSVERHeadDataArray[0];
                        document.getElementById('ctl00_ContentPlaceHolder_approverDropDownList').value = BSVERHeadDataArray[1];
                        document.getElementById('ctl00_ContentPlaceHolder_designationLable2').innerHTML = "" + BSVERHeadDataArray[2];
                        document.getElementById('ctl00_ContentPlaceHolder_customerAddressTextBox').value = BSVERHeadDataArray[3];
                        document.getElementById('ctl00_ContentPlaceHolder_dearTextBox').value = BSVERHeadDataArray[4];
                        document.getElementById('ctl00_ContentPlaceHolder_emailAddressTextBox').value = BSVERHeadDataArray[5];
                        break;
                    }
                }
                //document.getElementById('ctl00_ContentPlaceHolder_mesageLabel').innerHTML ="";
            }
            else {
                JS_FunctionBSVERGridClear();
                //alert("Data entry not yet!");
                //document.getElementById('ctl00_ContentPlaceHolder_mesageLabel').innerHTML ="No data entry yet!";
            }
        }
        function JS_FunctionBSVERGridClear() {
            for (var i = 2; i <= 5; i++) {
                document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_dateTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_descriptionTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_withdrawlTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_dipositTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_balanceTextBox').value = "";
                document.getElementById('ctl00_ContentPlaceHolder_BSVARGridView_ctl0' + i + '_oldIdTextBox').value = "";
            }
        }
        function JS_FunctionLoadDesignation() {
            var userId = document.getElementById('ctl00_ContentPlaceHolder_approverDropDownList').value;
            if (userId > 0) {
                var FileName = '../UI/TempLoadDesignation.aspx';
                var ajaxIndex = ajax_objects.length;
                ajax_objects[ajaxIndex] = new sack();
                var url = FileName + '?' + '&userId=' + userId;
                ajax_objects[ajaxIndex].requestFile = url;
                ajax_objects[ajaxIndex].onCompletion = function () {
                    ajax_showDesignation(ajaxIndex);
                };
                ajax_objects[ajaxIndex].runAJAX();
            }
        }
        function ajax_showDesignation(ajaxIndex) {
            var arrayData = new Array();
            var responseData = ajax_objects[ajaxIndex].response;
            //alert(responseData);
            if (responseData.length > 0) {
                arrayData = responseData.split('#');
                document.getElementById('ctl00_ContentPlaceHolder_designationLable2').innerHTML = "" + arrayData[0];
                document.getElementById('ctl00_ContentPlaceHolder_scbAddressTextBox').value = arrayData[1];
            }
        }
        function JS_functionButtonTextChange() {
            switch (document.getElementById('ctl00_ContentPlaceHolder_statementTypeDropDownList').value) {
                case "0":
                case "1":
                    document.getElementById('ctl00_ContentPlaceHolder_saveButton').value = "Save";
                    break;
                case "2":
                    document.getElementById('ctl00_ContentPlaceHolder_saveButton').value = "Send";
                    break;
            }

        }
    </script>
</asp:Content>
