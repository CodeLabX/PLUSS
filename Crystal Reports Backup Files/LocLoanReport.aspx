﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_LocLoanReport" CodeBehind="LocLoanReport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
    <style type="text/css">
        .auto-style1 {
            width: 106%;
        }
    </style>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">


    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" valign="top" class="auto-style1">


                <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
                    <tr bgcolor="#CEDFBD">
                        <td height="30" colspan="11" valign="middle" style="text-align: center;">
                            <div style="text-align: center; font-weight: bold;"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">Application Status Wise Reprot</h1></font></div>
                        </td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product:&nbsp;</font></div>
                        </td>
                        <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:DropDownList ID="productDropDownList" runat="server" Width="200px"></asp:DropDownList>
              </font>&nbsp; 
                        </td>

                        <td width="315" height="30" align="left" valign="middle" bgcolor="#EFF3E7">
                            <asp:DropDownList ID="appr_disb" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="Approved" Value="5" />
                                <asp:ListItem Text="Disbursed" Value="7" />
                                <asp:ListItem Text="Deffered" Value="6" />
                                <asp:ListItem Text="Declined" Value="2" />
                                <asp:ListItem Text="Verification" Value="9" />
                                <asp:ListItem Text="In Process" Value="4" />
                            </asp:DropDownList>

                        </td>

                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date :&nbsp;</font></div>

                        </td>
                        <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                            <div align="left">
                                <font color="#FFFFFF" face="Courier New, Courier, mono"> </font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                   
                    <asp:DropDownList ID="DayDropDownList" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="MonthDropDownList" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="YearDropDownList" runat="server"> </asp:DropDownList>   

              </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font color="#FFFFFF" face="Courier New, Courier, mono"> </font>
                            </div>

                        </td>
                        <td width="41" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO</td>
                        <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                            <asp:DropDownList ID="ToDateDayList" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ToDateMonthList" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ToDateYearList" runat="server"></asp:DropDownList>


                        </td>
                    </tr>
                    <tr bgcolor="#CEDFBD">
                        <td height="30">
                            <div align="right"></div>
                        </td>
                        <td height="30">&nbsp;</td>
                        <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">
                    <asp:Button runat="server" Text="Show" ID="btnShow"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnShow_Click"/>
            
</font>

                        </td>
                    </tr>
                </table>

                <br />

        <table width="780" border="0" cellpadding="0" cellspacing="1" bgcolor="BDD3A5">
                    <tr align="center" bgcolor="#CBDCB8">
                        <td height="30" colspan="3" valign="middle"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">View&nbsp;Quick&nbsp;&nbsp;Report</h1></font></td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#DCE8CE">PRODUCT:&nbsp;</td>
                        <td height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                       <asp:DropDownList ID="DropDownProductList" runat="server" Width="200px"></asp:DropDownList>
                        </td>
                        <td align="left" valign="middle" bgcolor="#EDF2E6">&nbsp;</td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td width="28%" height="30" align="right" valign="middle" bgcolor="#DCE8CE">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MONTH:&nbsp;</font></div>
                        </td>
                        <td width="19%" height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                            <asp:DropDownList ID="DropDownMonthList" runat="server"></asp:DropDownList>

                        </td>
                        <td width="53%" align="left" valign="middle" bgcolor="#EDF2E6"><font color="#FFFFFF" face="Courier New, Courier, mono">
                   
                  <asp:Button runat="server" Text="Quick Report -Monthly" ID="btnQuickReportMonthly"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="160" OnClick="btnQuickReportMonthly_Click" />
                 </font></td>
                    </tr>
                    <tr bgcolor="BDD3A5">
                        <td height="30" align="right" valign="middle" bgcolor="#DCE8CE">
                            <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">YEAR:&nbsp;</font></div>
                        </td>
                        <td height="30" align="left" valign="middle" bgcolor="#EDF2E6">

                            <asp:DropDownList ID="DropDownYearList" runat="server"></asp:DropDownList>
                        </td>
                        <td height="30" align="left" valign="middle" bgcolor="#EDF2E6"><font color="#FFFFFF" face="Courier New, Courier, mono">
                    <asp:Button runat="server" Text="Quick Report -Year To Date" ID="btnQuickReportYearly"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="190" OnClick="btnQuickReportYearly_Click" />
                          
                 </font></td>
                    </tr>
                    <tr bgcolor="#CBDCB8">
                        <td height="30">&nbsp;</td>
                        <td height="30" colspan="2" align="left" valign="middle" bgcolor="#CBDCB8">&nbsp;</td>
                    </tr>
           </table>

<br/>

       <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC"  bgcolor="BDD3A5">
            <tr align="center" valign="middle" bgcolor="#CEDFBD">
              <td height="30" colspan="15"><div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">aPerfometer</h1></font></div></td>
            </tr>
            <tr bgcolor="BDD3A5">
              <td width="72" height="30" align="right" valign="middle" bgcolor="#DEEBCE" ><font face="Verdana, Arial, Helvetica, sans-serif">Month</font></td>
              <td width="129" align="left" valign="middle"  bgcolor="#EFF3E7">
                <asp:DropDownList ID="DropDownaPerfometerMonth" runat="server"></asp:DropDownList>
              </td>
              <td width="39" align="left" valign="middle"  bgcolor="#EFF3E7"><font face="Verdana, Arial, Helvetica, sans-serif">Year</font></td>
              <td width="63" align="left" valign="middle"  bgcolor="#EFF3E7">
                  <asp:DropDownList ID="DropDownaPerfometerYear" runat="server"></asp:DropDownList>
              </td>
              <td align="left" valign="middle"  bgcolor="#EFF3E7">PRODUCT:</td>
              <td width="200" align="left" valign="middle"  bgcolor="#EFF3E7">
                   <asp:DropDownList ID="DropDownaPerfometerProduct" runat="server" Width="200px"></asp:DropDownList>
              </td>
				  <td width="20" height="30" bgcolor="#EFF3E7" >
				       <asp:CheckBox ID="aPerfometerCheckBox" runat="server" value="cb1" />
				      
				  </td>
              <td style="width: 163px; height: 30px; background-color: #EFF3E7; "><font face="Verdana, Arial, Helvetica, sans-serif">Person wise Details</font></td>
            </tr>
            <tr bgcolor="#CEDFBD">
              <td height="30" colspan="4" bgcolor="#CEDFBD" >&nbsp;</td>
              <td width="85" align="left" bgcolor="#CEDFBD" >&nbsp;</td>
              <td height="30" colspan="3" align="left" bgcolor="#CEDFBD" > 
                   <asp:Button runat="server" Text="Show" ID="btnaPerfometer"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnaPerfometer_Click"/>
                
              </td>
            </tr>
          </table>

<br/>
                
  <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC"  bgcolor="BDD3A5">
          <tr align="center" valign="middle" bgcolor="#CEDFBD"> 
            <td height="30" colspan="12"> <div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">Capping 
                Report</h1></font></div></td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D6E3C6" > 
              <div align="right"><font color="#FFFFFF" face="Courier New, Courier, mono"> 
            </font></div></td>
            <td width="48" height="30" align="right" valign="middle" bgcolor="#D6E3C6" ><font face="Verdana, Arial, Helvetica, sans-serif">Date</font><font color="#FFFFFF" face="Courier New, Courier, mono">&nbsp;</font></td>
            <td width="227" align="left" valign="middle"  bgcolor="#EFF3E7">
                  <asp:DropDownList ID="CappingDayDropDown" runat="server"></asp:DropDownList>
                  <asp:DropDownList ID="CappingMonthDropDown" runat="server"></asp:DropDownList>
                  <asp:DropDownList ID="CappingYearDropDown" runat="server"></asp:DropDownList>
            </td>
            <td width="56" align="left" valign="middle"  bgcolor="#EFF3E7"><div align="center">TO</div></td>
            <td width="322" align="left" valign="middle"  bgcolor="#EFF3E7">
                  <asp:DropDownList ID="CappingToDayDropDown" runat="server"></asp:DropDownList>
                  <asp:DropDownList ID="CappingToMonthDropDown" runat="server"></asp:DropDownList>
                  <asp:DropDownList ID="CappingToYearDropDown" runat="server"></asp:DropDownList>
                <div align="right"> 
              </div>
            </td>
          </tr>
          <tr bgcolor="#CEDFBD"> 
            <td width="121" height="40" >&nbsp;</td>
            <td height="40" colspan="4" align="left" valign="middle" bgcolor="#CEDFBD">
                
             <asp:Button runat="server" Text="Product Wise" ID="btnprodcapp"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center; font-size: 13px;" Height="25px" Width="90px" OnClick="btnprodcapp_Click"/> &nbsp;
             <asp:Button runat="server" Text="Tenor Wise" ID="btntenocapp"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center;  font-size: 13px;" Height="25px" Width="85" OnClick="btntenocapp_Click"/> &nbsp;
             <asp:Button runat="server" Text="Level Wise for SME" ID="btnlevelcapp"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center; font-size: 13px;" Height="25px" Width="137" OnClick="btnlevelcapp_Click"/> &nbsp;
             <asp:Button runat="server" Text="Level Wise for PL" ID="btnlevelcapp2"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center; font-size: 13px;" Height="25px" Width="123" OnClick="btnlevelcapp2_Click"/> &nbsp;
             <asp:Button runat="server" Text="Level Wise for AL" ID="btnlevelcapp3"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center; font-size: 13px;" Height="25px" Width="123" OnClick="btnlevelcapp3_Click"/> &nbsp;
             <asp:Button runat="server" Text="Level Wise for Flexi" ID="btnlevelcapp45"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center;font-size: 13px;" Height="25px" Width="137" OnClick="btnlevelcapp45_Click"/> &nbsp;
             <asp:Button runat="server" Text="Segment Wise for PL" ID="btnSEGMENT"  style="border:1px solid #83b67a; background-color:#D8EED5; text-align: center; font-size: 13px;" Height="25px" Width="145" OnClick="btnSEGMENT_Click"/>
                
			 
</td>
          </tr>
          
        </table>

<br/>
 <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC"  bgcolor="BDD3A5">
          <tr align="center" valign="middle" bgcolor="#CEDFBD"> 
            <td height="30" colspan="10"> <div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">List Of Application Deferred/Declined- According To Reasons</h1></font></div></td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td width="26%" height="30" align="right" valign="middle" bgcolor="#D5E2C5" ><font size="2"><font face="Verdana, Arial, Helvetica, sans-serif">PRODUCT:&nbsp;</font> 
              </font> <div align="right"></div></td>
            <td height="30" colspan="2" align="left" valign="middle"  bgcolor="#EFF3E7">
                 <asp:DropDownList ID="ProductListDropDown" runat="server" Width="200px"></asp:DropDownList>  
           </td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5" > 
              <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">STATUS:&nbsp;</font></div></td>
            <td height="30" colspan="2" align="left" valign="middle"  bgcolor="#EFF3E7">
                
                 <asp:DropDownList ID="statDropDownList" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="Deffered" Value="2" />
                                <asp:ListItem Text="Declined" Value="1" />
                            </asp:DropDownList>
                
            </td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5" > 
              <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MONTH:&nbsp;</font></div></td>
            <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7" >
			 <asp:DropDownList ID="AppMonthDropDown" runat="server"></asp:DropDownList>
            </td>
          </tr>
          <tr bgcolor="BDD3A5"> 
            <td height="30" align="right" valign="middle" bgcolor="#D5E2C5" > 
              <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">YEAR:&nbsp;</font></div></td>
            <td height="30" colspan="2" align="left" valign="middle"  bgcolor="#EFF3E7">
                 <asp:DropDownList ID="AppYearDropDown" runat="server"></asp:DropDownList> 
           	</td>
          </tr>
          <tr bgcolor="#CEDFBD"> 
            <td height="30" > 
            </td>
            <td width="21%" height="30" align="left" valign="middle"> <font face="Verdana, Arial, Helvetica, sans-serif">&nbsp; 
              </font> 
            </td>
            <td width="53%" align="left" valign="middle">
                 <asp:Button runat="server" Text="Show" ID="btnAppShow"  style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnAppShow_Click"/>
             
            </td>
          </tr>
        </table>


            </td>
        </tr>
    </table>

</asp:Content>

