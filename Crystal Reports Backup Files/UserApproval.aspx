﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="UserApproval.aspx.cs" Inherits="PlussAndLoan.UI.UserCreation.UserApproval" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="tomato" TagName="modalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <tomato:modalPopup runat="server" ID="popupmodal" />    
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">User Approval</div>
        <form name="myform" method="post">

            <table width="100%" border="0" cellspacing="1" cellpadding="1">

                <tr>
                <td style="font-weight: bold; width: 200px">
                   <span>Staff ID</span>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="staffIdTextBox" runat="server" CssClass="input-field" Enabled="false" />
                </td>

                    <td style="font-weight: bold; width: 200px">
                   <span>Name</span>
                </td>

                     <td class="auto-style2">
                         <asp:TextBox ID="nameTextBox" runat="server" CssClass="input-field" Enabled="false" />
                    </td>
            </tr>


                <tr>
                     <td style="font-weight: bold; width: 200px">
                         <span>Designation </span>
                         </td>

                     <td class="auto-style2">
                          <asp:TextBox ID="designationTextBox" runat="server" CssClass="input-field" Enabled="false" />
                         </td>

                     <td style="font-weight: bold; width: 200px">
                         <span>Role</span>
                         </td>

                     <td class="auto-style2">
                          <asp:DropDownList ID="userRoleDDL" AutoPostBack="false" runat="server" CssClass="select-field" Enabled="false"></asp:DropDownList>
                         </td>

                </tr>

                <tr>
                    <td style="font-weight: bold; width: 200px">
                         <span>ARM Code</span>
                         </td>

                    <td class="auto-style2">
                        <asp:TextBox ID="codeTextBox" runat="server" CssClass="input-field" Enabled="false" MaxLength="3" />
                        </td>

                    <td style="font-weight: bold; width: 200px">
                        <span>Email</span>
                         </td>

                    <td class="auto-style2">
                        <asp:TextBox ID="emailTextBox" runat="server" CssClass="input-field" Enabled="false" />
                        </td>
                </tr>

                 <tr>
                    <td style="font-weight: bold; width: 200px">
                         <span>Status</span>
                         </td>

                    <td class="auto-style2">
                         <asp:DropDownList ID="statusDDL" AutoPostBack="false" runat="server" CssClass="select-field" Enabled="false"></asp:DropDownList>
                        </td>

                    <td style="font-weight: bold; width: 200px">
                        <span>Comment</span>
                         </td>

                    <td class="auto-style2">
                        <asp:TextBox ID="approverCommentTextBox" runat="server" CssClass="textarea-field" Rows="3" TextMode="MultiLine" />
                        </td>
                </tr>

                

            </table>


           <%-- <label for="field1">
                <span>Staff ID</span>
                <asp:TextBox ID="staffIdTextBox" runat="server" CssClass="input-field" Enabled="false" />
            </label>--%>
            <%--<label for="field1">
                <span>Name</span>
                <asp:TextBox ID="nameTextBox" runat="server" CssClass="input-field" Enabled="false" />
            </label>--%>
            <%--<label for="field2">
                <span>Designation </span>
                <asp:TextBox ID="designationTextBox" runat="server" CssClass="input-field" Enabled="false" />
            </label>
            <label for="field4">
                <span>Role</span>
                <asp:DropDownList ID="userRoleDDL" AutoPostBack="false" runat="server" CssClass="select-field" Enabled="false"></asp:DropDownList>
            </label>--%>
            <%--<label for="field2">
                <span>ARM Code</span>
                <asp:TextBox ID="codeTextBox" runat="server" CssClass="input-field" Enabled="false" MaxLength="3" />
            </label>
            <label for="field2">
                <span>Email</span>
                <asp:TextBox ID="emailTextBox" runat="server" CssClass="input-field" Enabled="false" />
            </label>--%>
            <%--<label for="field4">
                <span>Status</span>
                <asp:DropDownList ID="statusDDL" AutoPostBack="false" runat="server" CssClass="select-field" Enabled="false"></asp:DropDownList>
            </label>
            <label for="field2">
                <span>Comment</span>
                <asp:TextBox ID="approverCommentTextBox" runat="server" CssClass="textarea-field" Rows="3" TextMode="MultiLine" />
            </label>--%>

            <asp:HiddenField ID="UserIdHdn" runat="server" Value="" />
            <asp:HiddenField ID="UserTempIdHdn" runat="server" Value="" />

            <table>
            <tr>
                <td style="width: 220px" align="center"></td>
                <td style="width: 400px" align="center">
                    <%--<asp:Button ID="deferButton" runat="server" Text="Defer" Width="95px" Height="30px"
                        OnClick="deferButton_Click" />
                    <asp:Button ID="forwardButton" Text="Forward" runat="server" Width="95px" Height="30px"
                        OnClick="forwardButton_Click" OnClientClick="return ValidteLoanApplicationForm(); return LLIdIsEmpty();" />
                    <asp:Button CssClass="btn primarybtn" ID="saveButton" Text="Save" runat="server" OnClick="saveButton_Click" OnClientClick="return ValidteLoanApplicationForm(); return LLIdIsEmpty();" />
                    &nbsp;<asp:Button ID="Button1" CssClass="btn clearbtn" Text="Clear" runat="server" Width="95px" Height="30px" OnClick="clearButton_Click" />--%>

                    <asp:Button ID="approveButton" runat="server" Text="Approve" CssClass="btn primarybtn" OnClick="approveButton_Click" />
                        <asp:Button ID="clearButton" runat="server" Text="Clear" CssClass="btn clearbtn" OnClick="clearButton_Click" />
                        <asp:Button ID="DeleteButton" runat="server" Text="Reject" CssClass="btn clearbtn" OnClick="DeleteButton_Click" />
                </td>
                <td style="width: 280px; padding-left: 30px;" align="left">
                    <%--<asp:Label ID="errorMsgLabel" runat="server" ForeColor="#FF3300"></asp:Label>--%>
                </td>
            </tr>
        </table>
        </form>
    </div>
    <div style="height: 20%; width:95%; padding-left:20px; overflow:auto;">
        <asp:GridView ID="userGridView" runat="server" GridLines="None" CssClass="myGridStyle"
            Font-Size="11px" AutoGenerateColumns="False" OnRowCommand="userGridView_RowCommand"
            DataKeyNames="Id">
            <Columns>
                <%--<asp:BoundField DataField="Id" HeaderText="Id" Visible="false"/>--%>
                <%--<asp:BoundField DataField="USER_ID" HeaderText="User Id" Visible="false"/>--%>
                <asp:BoundField DataField="USER_CODE" HeaderText="PS ID" />
                <asp:BoundField DataField="USER_NAME" HeaderText="Name" />
                <asp:BoundField DataField="USER_DESIGNATION" HeaderText="Designation" />
                <asp:BoundField DataField="USER_EMAILADDRESS" HeaderText="Email" />
                <asp:BoundField DataField="USER_LEVELCODE" HeaderText="ARM Code" />
                <asp:BoundField DataField="USER_LEVEL_NAME" HeaderText="Role" />
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button Text="Select" runat="server" CommandName="Select" CommandArgument='<%# Eval("Id") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Record Found
            </EmptyDataTemplate>
        </asp:GridView>
        <br />
    </div>
</asp:Content>
