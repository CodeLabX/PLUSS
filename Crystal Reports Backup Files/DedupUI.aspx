﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_DedupUI" Codebehind="DedupUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/spreadsheet.ascx" TagPrefix="uc1" TagName="spreadsheet" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxtabbar.ascx" TagPrefix="uc1" TagName="dhtmlxtabbar" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxtabbar.ascx" TagPrefix="uc2" TagName="dhtmlxtabbar" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxtabbar_start.ascx" TagPrefix="uc1" TagName="dhtmlxtabbar_start" %>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <uc1:spreadsheet runat="server" ID="spreadsheet" />
    <uc1:dhtmlxtabbar runat="server" ID="dhtmlxtabbar" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc2:dhtmlxtabbar runat="server" ID="dhtmlxtabbar1" />
    <uc1:dhtmlxtabbar_start runat="server" ID="dhtmlxtabbar_start" />

	<script type="text/javascript">
        function selectAllCheckBox()
        {
        if(document.getElementById('selectAllCheckbox').checked==true)
          {
            for(var i=1;i<=6;i++)
            {
                document.getElementById('Checkbox'+i).checked=true;
            }
            document.getElementById('idEntryCheckBox').checked=true;
          }
          else
          {
            for(var i=1;i<=6;i++)
            {
                document.getElementById('Checkbox'+i).checked=false;
            }
            document.getElementById('idEntryCheckBox').checked=false;
          }        	
        }		

	</script>

</head>
<body>

    <form id="deDupform" runat="server">
    <div>
            <span style="padding-left: 400px">
            <asp:Label ID="dedupVerificationLabel" runat="server" Text="DE-DUP Verification"
                Font-Bold="true"></asp:Label></span>
        <fieldset>
            <legend>Search Parameter</legend>
            <table border="0" cellspacing="1" cellpadding="1">
                <tr>
                    <td align="right">
                        <asp:Label ID="loanLocatorIdLabel" runat="server" Text="Loan Locator ID :"></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="loanLocatorIdTextBox" runat="server" Width="160px"></asp:TextBox>
                        <asp:Button ID="findButton" runat="server" Text="..." OnClick="findButton_Click" />
                        &nbsp;<asp:Label ID="errorMsgLabel" runat="server" ForeColor="#FF3300"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="applicantNameLabel0" runat="server" Text="Applicant Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="applicantNameTextBox0" runat="server" Width="230px"></asp:TextBox>
                        <asp:CheckBox ID="applicantNameCheckbox" runat="server" OnCheckedChanged="applicantNameCheckbox_CheckedChanged" />
                    </td>
                    <td align="right">
                        <asp:Label ID="dateOfBirthLabel0" runat="server" Text="Date of Birth :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="dateOfBirthTextBox0" runat="server" Width="160px"></asp:TextBox>
                        <asp:CheckBox ID="dateOfBirthCheckBox0" runat="server" />
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="fatherNameLabel0" runat="server" Text="Father's Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="fatherNameTextBox10" runat="server" Width="230px"></asp:TextBox>
                        <asp:CheckBox ID="fatherNameCheckBox" runat="server" OnCheckedChanged="fatherNameCheckBox_CheckedChanged">
                        </asp:CheckBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="motherNameLabel0" runat="server" Text="Mother's Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="motherNameTextBox0" runat="server" Width="230px"></asp:TextBox>
                        <asp:CheckBox ID="motherNameCheckbox" runat="server" OnCheckedChanged="motherNameCheckbox_CheckedChanged">
                        </asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="businessNameLabel0" runat="server" Text="Business Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="businessNameTextBox0" runat="server" Width="230px"></asp:TextBox>
                        <asp:CheckBox ID="businessNameCheckbox" runat="server" OnCheckedChanged="businessNameCheckbox_CheckedChanged">
                        </asp:CheckBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="tinNoLabel0" runat="server" Text="TIN No. :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tinNoTextBox" runat="server" Width="160px"></asp:TextBox>
                        <asp:CheckBox ID="tinNoCheckBox" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="bottom">
                        <asp:CheckBox ID="selectAllCheckBox" runat="server" Text="select all" AutoPostBack="True"
                            OnCheckedChanged="selectAllCheckBox_CheckedChanged" />
                    </td>
                    <td align="left" valign="top">
                        <asp:GridView ID="contactNoEntryGridView" runat="server" AutoGenerateColumns="False"
                            Font-Size="11px" PageSize="3" Width="220px">
                            <Columns>
                                <asp:TemplateField HeaderText="Contact Number">
                                    <ItemTemplate>
                                        <asp:TextBox ID="contactNoEntryTextBox" runat="server" Width="205px" BorderWidth="0px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="contactNoEntryCheckBox" runat="server" Width="25px" BorderWidth="0px">
                                        </asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" />
                        </asp:GridView>
                    </td>
                    <td align="left" valign="top">
                        <asp:GridView ID="idEntryGridView" runat="server" AutoGenerateColumns="False" Font-Size="11px"
                            PageSize="3" Width="220px">
                            <Columns>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:TextBox ID="typeEntryTextBox" runat="server" Width="75px" BorderWidth="0px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID No.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="idNoEntryTextBox" runat="server" Width="75px" BorderWidth="0px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                        <asp:TextBox ID="expiryDateEntyTextBox" runat="server" Width="75px" BorderWidth="0px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="idEntryCheckBox" runat="server" Width="20px" BorderWidth="0px">
                                        </asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" />
                        </asp:GridView>
                    </td>
                    <td align="left" valign="top">
                        <asp:GridView ID="scbAccountEntryGridView" runat="server" AutoGenerateColumns="False"
                            Font-Size="11px" PageSize="3" Width="220px">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="prefixEntryTextBox" runat="server" Width="30px" MaxLength="2" BorderWidth="0px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SCB A/C No.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="accountNoEntryTextBox" runat="server" Width="140px" BorderWidth="0px"
                                            MaxLength="7"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="postfixEntryTextBox" runat="server" Width="30px" MaxLength="2" BorderWidth="0px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="scbAccEntryCheckBox" runat="server" Width="20px" BorderWidth="0px"
                                            AutoPostBack="True" OnCheckedChanged="scbAccEntryCheckBox_CheckedChanged"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </fieldset>
        </div>
    <div id="a_tabbar" class="dhtmlxTabBar" imgpath="../codebase/imgs/" style="width:972px; height:280px;" skinColors="#FCFBFC,#F4F3EE"></div>
    <table>
    <tr valign="top">
        <td>
        <div style="padding-left: 80px; width: 800px;">
        <fieldset>
            <legend>Applicant Information</legend>
            <table style="text-align: left; padding-left: 0px">
                <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td style="width: 220px">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" colspan="2">
                        &nbsp;
                    </td>
                    <td valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonDedupIdLabel" runat="server" Text="DEDUP ID :"></asp:Label>
                    </td>
                    <td style="width: 220px">
                        <asp:TextBox ID="negativeListPersonDedupIdTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" colspan="2" valign="top">
                        <asp:Label ID="negativeListPersonStatusLabel" runat="server" Text="Status :"></asp:Label>
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="negativeListPersonStatusTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonApplicantNameLabel" runat="server" Text="Applicant Name :"></asp:Label>
                    </td>
                    <td style="width: 220px">
                        <asp:TextBox ID="negativeListPersonApplicantNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" style="width: 150px" colspan="2">
                        <asp:Label ID="negativeListPersonDateOfBirthLabel" runat="server" Text="Date of Birth :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonDobTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonFatherNameLabel" runat="server" Text="Father's Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonFatherNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" colspan="2">
                        <asp:Label ID="negativeListPersonMotherNameLabel" runat="server" Text="Mother's Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonMotherNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonEducationalLevelLabel" runat="server" Text="Educational Level :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonEducationalTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" colspan="2">
                        <asp:Label ID="negativeListPersonMaritalStatusLabel" runat="server" Text="Marital Status :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonMaritalStatusTextBox" runat="server" Width="200px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonResidentialAddressLabel" runat="server" Text="Residential Address :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonResidentialAddressTextBox" runat="server" TextMode="MultiLine"
                            Width="220px" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" colspan="2" valign="top">
                        <asp:Label ID="negativeListPersonResidentailStatusLabel" runat="server" Text="Residentail Status :"></asp:Label>
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="negativeListPersonResidentialStatusTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonContactNoLabel" runat="server" Text="Contact No. :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="negativeListPersonContactNoGridView" runat="server" AutoGenerateColumns="False"
                            Font-Size="11px" PageSize="3" Width="220px">
                            <Columns>
                                <asp:TemplateField HeaderText="Number">
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonContactNoGridViewContactNoLabel" runat="server"
                                            Width="220px" Height="15px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" />
                        </asp:GridView>
                    </td>
                    <td align="right" valign="top" colspan="2">
                        &nbsp;
                        <asp:Label ID="negativeListPersonIdLabel" runat="server" Text="ID :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="negativeListPersonIdGridView" runat="server" AutoGenerateColumns="False"
                            Font-Size="11px" PageSize="3" Width="220px">
                            <Columns>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonIdGridViewTypeLabel" runat="server" Width="75px"
                                            Height="15px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID No.">
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonIdGridViewIdNoLabel" runat="server" Width="75px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonIdGridViewExpiryDateLabel" runat="server" Width="70px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonAcNoSCBLabel" runat="server" Text="A/C No. for SCB :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="negativeListPersonScbAccountGridView" runat="server" AutoGenerateColumns="False"
                            Font-Size="11px" PageSize="3" Width="220px">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonScbAccountGridViewPrefixLabel" runat="server" Height="15px"
                                            Width="30px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="A/C No.">
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonScbAccountGridViewAccountNoLabel" runat="server"
                                            Width="155px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="negativeListPersonScbAccountGridViewPostfixLabel" runat="server" Width="30px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" />
                        </asp:GridView>
                    </td>
                    <td align="right" valign="top" colspan="2">
                        <asp:Label ID="negativeListPersonTinNoLabel" runat="server" Text="TIN No. :"></asp:Label>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="negativeListPersonAccountNameLabel" runat="server" Text="Product Name :"></asp:Label>
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="negativeListPersonTinNoTextBox" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                        <br />
                        <br />
                        <br />
                        <asp:TextBox ID="negativeListPersonAccountNameTextBox" runat="server" ReadOnly="True"
                            Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        &nbsp;
                    </td>
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonNameOfCompanyLabel" runat="server" Text="Name of Company :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonCompanyNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" colspan="2">
                        <asp:Label ID="negativeListPersonDeclineDateLabel" runat="server" Text="Decline Date :"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="negativeListPersonDeclineDateTextBox" runat="server" ReadOnly="True"
                            Width="220px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonOfficeAddressLabel" runat="server" Text="Office Address :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonOfficeAddressTextBox" runat="server" TextMode="MultiLine"
                            Width="220px" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" colspan="2">
                        <asp:Label ID="negativeListPersonDeclineReasonLabel" runat="server" Text="Decline Reason :"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="negativeListPersonDeclineReasonTextBox" runat="server" ReadOnly="True"
                            TextMode="MultiLine" Width="220px"></asp:TextBox>
                    </td>
                </tr>
                </table>
            </fieldset>
        </div>
        </td>
        </tr>
    </table>
    </form>
    <script type="text/javascript">
	    tabbar=new dhtmlXTabBar("a_tabbar","top");
        tabbar.setImagePath("../codebase/imgs/");
        //tabbar.preventIECashing(true);
        tabbar.loadXML("../includes/Deduptabs.xml");
        function   goToNextTabA(){
		    var z=tabbar.goToNextTab();
		    if (!z) tabbar.setTabActive(tabbar.rows[0].childNodes[0].idd);
	    }
    </script>
</body>
</html>
