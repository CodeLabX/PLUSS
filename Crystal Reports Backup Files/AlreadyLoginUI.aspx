﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AlreadyLoginUI" Codebehind="AlreadyLoginUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/loginCss.ascx" TagPrefix="uc1" TagName="loginCss" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
    <uc1:loginCss runat="server" ID="loginCss" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div style="padding-left: 5px">
                        <table>
                            <tr>
                                <td align="left" class="paddingLeftTop" style="vertical-align: bottom; padding-left: 150px"
                                    colspan="5">
                                    <img src="../Images/template_1_01.jpg" alt="Standard Chartered" width="139px" height="45px" />
                                    <img src="../Images/template_1_02.jpg" alt="" width="550px" height="45px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2" align="center">
                                    <div style="width: 380px; padding-left:140px;">
                                        <fieldset>
                                            <legend><span style="font-family: Verdana; font-weight: bold; font-size: 11px; color: Green;">
                                                Login Check</span></legend>
                                            
                                            <table class="style1">
                                                <tr>
                                                    <td align="right" width="50">
                                                        &nbsp;</td>
                                                    <td align="right" style="padding-right:8px;">
                                                        User Id</td>
                                                    <td align="left">
                                                        <asp:Label ID="userIdTextBox" runat="server" Width="150px" MaxLength="7" ReadOnly="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        &nbsp;</td>
                                                    <td align="right" style="padding-right:8px;">
                                                        User Name</td>
                                                    <td align="left">
                                                        <asp:Label ID="passwordTextBox" runat="server" Width="150px" ReadOnly="true" ></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="3" style="height:13px;" align="center">
                                                        <asp:Label ID="conMsgLabel" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="yesButton" runat="server" Text="Yes" Width="70px" 
                                                            onclick="yesButton_Click" />
                                                    </td>
                                                    <td align="left">
                                                        <asp:Button ID="noButton" runat="server" Text="No" Width="70px" 
                                                            onclick="noButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            
                                        </fieldset>
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="padding-left: 150px">
                                    <img src="../images/footer.JPG" alt="" height="45px" style="padding-left: 15px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="style1">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2" align="center">
                                    <asp:Label ID="copyrightLabel" runat="server" Text="Copyright@2009 Standard Chartered Bank"
                                        CssClass="copyRight"></asp:Label>
                                    &nbsp;<br />
                                    <asp:Label ID="powerdByLabel" runat="server" Text="Powered by: " CssClass="copyRight" />
                                    &nbsp;
                                    <asp:Label ID="webLabel" runat="server" CssClass="copyRight"> <a href="http://www.batworld.com" target="_blank" class="copyRightLink">Business Automation Ltd.</a></asp:Label>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="style1">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
