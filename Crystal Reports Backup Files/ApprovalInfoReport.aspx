﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.Reports.Reports_ApprovalInfoReport" Codebehind="ApprovalInfoReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Approver Information</title>
    <style type="text/css">
        .tableWidth
        {
            width: 95%;
        }
        .fontSize
        {
        }
        .table2Width
        {
            width: 95%;
        }
        .table3Width
        {
            width: 95%;
            border-width: 1px;
            border-color: Black;
            border-style: solid;
            border-collapse: collapse;
        }
        .table3Width td
        {
            border-width: 1px;
            border-color: Black;
            border-style: solid;
            border-collapse: collapse;
        }
        .tdBorder
        {
            border-width: 1px;
            border-color: Black;
            border-style: solid;
            border-collapse: collapse;
        }
        .table3HeaderText
        {
            font-size: 11px;
            font-weight: bold;
            text-align: center;
        }
        .pddTableContentText
        {
            font-size: 11px;
            text-align: center;
        }
        .dvBorder
        {
            border: solid 1px Black;
            font-size: 11px;
        }
        .tdHeight
        {
            height: 18px;
        }
        .appraisedByText
        {
            font-size: 12px;
            font-weight: bold;
        }
        .designationText
        {
            font-size: 11px;
        }
        .headerFontsize
        {
            font-size: 14px;
            font-weight: bold;
        }
    </style>
        <script language="javascript" type="text/javascript">
        function PrintBasel()
        {
            window.print();
        }
        function ColorChange()
        {
             document.getElementById('printButton').style.backgroundColor = 'red';
        }
    </script>
</head>
<body onload="PrintBasel();">


    <form id="form1" runat="server">
    <center>
    <table border="0" cellpadding="1" cellspacing="1" width="95%">
    <tr>
    <td>
    <div>
        <table class="tableWidth">
            <tr>
                <td width="100" rowspan="2">
                    &nbsp; &nbsp;
                    <img alt="" src="../Images/scb_logo.jpg" style="width: 20px; height: 25px" />
                </td>
                <td width="400" style="font-size: 11px;" align="center">
                   <asp:Label ID="reportNameLabel" runat="server" Text="PERSONAL LOAN" Font-Size="11px"></asp:Label> 
                </td>
                <td width="150" align="left" style="font-size: 11px;">
                    <label style="font-size: 11px;">
                        Appraisal Date:
                    </label>
                    <asp:Label ID="appraisalDateLabel" runat="server" Style="font-size: 11px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <label style="font-size: 22px; font-weight: bold;">
                            APPROVAL INFORMATION
                        </label>
                    </center>
                </td>
                <td align="right">
                    <asp:Label ID="masterACNoLabel" runat="server" Text="MASTER:" Font-Size="11px"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style=" width:95%; border: solid 1px Black;">
        <table class="table2Width" style="width:95%">
            <tr align="left">
                <td bgcolor="Silver" class="tdBorder" width="160">
                    LOAN LOCATOR ID
                </td>
                <td>
                    &nbsp;&nbsp;
                    <asp:Label ID="llIdLabel" runat="server" CssClass="headerFontsize" />
                </td>
            </tr>
            <tr style="height: 3px; font-size: 6px;">
                <td height="3">
                </td>
            </tr>
            <tr align="left">
                <td bgcolor="Silver" class="tdBorder">
                    CUSTOMER
                </td>
                <td>
                    &nbsp;&nbsp;
                    <asp:Label ID="customerLabel" runat="server" CssClass="headerFontsize"  />
                </td>
            </tr>
            <tr style="height: 3px; font-size: 6px;">
                <td height="3">
                </td>
            </tr>
            <tr align="left">
                <td bgcolor="Silver" class="tdBorder">
                    APPROVED AMOUNT
                </td>
                <td>
                    &nbsp;&nbsp;
                    <asp:Label ID="aaprovedAmntLabel" runat="server"  CssClass="headerFontsize" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table border="0" cellpadding="1" cellspacing="1" width="95%"><tr><td align="left">
    <label style="font-size: 11px; font-weight: bold;">
        CUSTOMER</label>
    </td></tr></table>
    <div>
        <table class="table3Width">
            <tr class="table3HeaderText" style="background-color: Silver;">
                <td class="tdHeight" width="150">
                    INCOME
                </td>
                <td class="tdHeight" width="100">
                    DBR
                </td>
                <td class="tdHeight" width="100">
                    MUE
                </td>
                <td class="tdHeight" width="300">
                    SEGMENT
                </td>
            </tr>
            <tr class="table3HeaderText">
                <td class="tdHeight" width="150">
                    <asp:Label ID="incomeLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="dbrLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="mueLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="300">
                    <asp:Label ID="segmentLabel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table border="0" cellpadding="1" cellspacing="1" width="95%"><tr><td align="left">
    <label style="font-size: 11px; font-weight: bold;">
        LOAN DETAILS</label>
    </td></tr></table>
    <div>
        <table class="table3Width">
            <tr class="table3HeaderText" style="background-color: Silver;">
                <td class="tdHeight" width="150">
                    APPROVED AMT
                </td>
                <td class="tdHeight" width="100">
                    LEVEL
                </td>
                <td class="tdHeight" width="100">
                    IR
                </td>
                <td class="tdHeight" width="100">
                    EMI
                </td>
                <td class="tdHeight" width="150">
                    UDC AMOUNT
                </td>
                <td class="tdHeight" width="50">
                   TENURE
                </td>
            </tr>
            <tr class="table3HeaderText">
                <td class="tdHeight" width="150">
                    <asp:Label ID="approvedAmtLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="levelLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="irLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="emiLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="150">
                    <asp:Label ID="udcAmntLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="tenureLabel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
            <table border="0" cellpadding="1" cellspacing="1" width="95%"><tr><td align="left">

        <label id="exposureLabel" style="font-size: 11px;">
            Total SCB Exposure:
        </label>
        <asp:Label ID="exposureAmtLabel" runat="server" Style="font-size: 11px;" />
        ,
        <label id="creditCrdLabel" style="font-size: 11px;">
            Credit Card Limit:
        </label>
        <asp:Label ID="crdtCrdLabel" runat="server" Style="font-size: 11px;" />
        &nbsp;<asp:Label ID="topupLabel" runat="server" Style="font-size: 11px;">
            , Top Up Amount:
        </asp:Label>
        <asp:Label ID="topupAmntLabel" runat="server" Style="font-size: 11px;" />
        </td></tr></table>
    </div>
    <table border="0" cellpadding="1" cellspacing="1" width="95%"><tr><td align="left">
    <div>
    <asp:Label ID="bancaWithEMILabel" runat="server" Style="font-size: 11px;" />
    <br />
    <asp:Label ID="bancaWithLoanLabel" runat="server" Style="font-size: 11px;" />
    </div>
    </td></tr></table>
    <br />
    <table border="0" cellpadding="1" cellspacing="1" width="95%"><tr><td align="left">
    <label style="font-size: 11px; font-weight: bold;">
        REPAYMENT</label>
     </td></tr></table>
    <div>
        <table class="table3Width">
            <tr class="table3HeaderText" style="background-color: Silver;">
                <td class="tdHeight" width="100">
                    MODE
                </td>
                <td class="tdHeight" width="225">
                    BANK NAME
                </td>
                <td class="tdHeight" width="225">
                    A/C NUMBER
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="PDCDATEupLabel" runat="server" Text="PDC DATE"></asp:Label>
                </td>
            </tr>
            <tr class="table3HeaderText">
                <td class="tdHeight" width="100">
                    <asp:Label ID="modeLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="225">
                    <asp:Label ID="bankNameLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="225">
                    <asp:Label ID="accNoLabel" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="pdcDateLabel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table border="0" cellpadding="1" cellspacing="1" width="95%"><tr><td align="left">
    <label style="font-size: 11px; font-weight: bold;">
        PDD DEVIATION</label>
    </td></tr></table>
    <div>
        <table class="table3Width">
            <tr class="table3HeaderText" style="background-color: Silver;">
                <td class="tdHeight" width="50">
                    #
                </td>
                <td class="tdHeight" width="500">
                    REASON
                </td>
                <td class="tdHeight" width="100">
                    LEVEL
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="50">
                    <asp:Label ID="Label1" runat="server" Text="1"></asp:Label>
                </td>
                <td class="tdHeight" width="500">
                    <asp:Label ID="reasonLabel1" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="pddLevelLabel1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="50">
                    <asp:Label ID="Label2" runat="server" Text="2"></asp:Label>
                </td>
                <td class="tdHeight" width="500">
                    <asp:Label ID="reasonLabel2" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="pddLevelLabel2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="50">
                    <asp:Label ID="Label3" runat="server" Text="3"></asp:Label>
                </td>
                <td class="tdHeight" width="500">
                    <asp:Label ID="reasonLabel3" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="pddLevelLabel3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="50">
                    <asp:Label ID="Label4" runat="server" Text="4"></asp:Label>
                </td>
                <td class="tdHeight" width="500">
                    <asp:Label ID="reasonLabel4" runat="server"></asp:Label>
                </td>
                <td class="tdHeight" width="100">
                    <asp:Label ID="pddLevelLabel4" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="table3Width">
            <tr class="table3HeaderText" style="background-color: Silver;">
                <td class="tdHeight" width="650">
                    PRE-DISBURSEMENT CONDITION/REMARKS
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="650" align="left">
                    <asp:Label ID="preCndtionLabel1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="650" align="left">
                    <asp:Label ID="preCndtionLabel2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="650" align="left">
                    <asp:Label ID="preCndtionLabel3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="650" align="left">
                    <asp:Label ID="preCndtionLabel4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pddTableContentText">
                <td class="tdHeight" width="650" align="left">
                    <asp:Label ID="preCndtionLabel5" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdHeight" width="650" style="font-size: 11px;">
                    <label>
                        Res address & phone no[all] to be updated in eBBS as per CPV.
                    </label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label>
                        PG/Reference details to be updated in RLS</label>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <br />
        <br />
        <br />
    <table border="0" width="95%" cellpadding="0" cellspacing="0" >
    <tr>
    <td align="center" >______________________</td>
    <td style="width:25%;">&nbsp;</td>
    <td align="center" >______________________</td>
    </tr>
    <tr>
    <td align="center" ><label style="font-size: 11px; font-weight:bold;"> Appraised By</label></td>
    <td>&nbsp;</td>
    <td align="center" ><label style="font-size: 11px; font-weight:bold;"> Approved By</label></td>
    </tr>
    <tr>
    <td align="center" ><asp:Label ID="appraisedByLabel" runat="server" CssClass="appraisedByText"></asp:Label></td>
    <td>&nbsp;</td>
    <td align="center" ><asp:Label ID="approvedNameLabel" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td align="center" ><asp:Label ID="designationLabel" runat="server" CssClass="designationText"></asp:Label></td>
    <td>&nbsp;</td>
    <td align="center" ><asp:Label ID="approvedDesignationLabel" runat="server" Text=""></asp:Label></td>
    </tr>
    </table>      
    </div>
    <br />
    <br />
    <div>
    <label style="font-size: 11px;">
        This document contains general information about the approved loan and to be retained
        with original checlist before<br />
        disbursement after necessary rectification(s) validated by CCU (if any)
    </label>
    </div>
    </td>
    </tr>
    </table>
    </center>
    </form>

</body>
</html>
