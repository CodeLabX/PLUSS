﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="QuickReport.aspx.cs" Inherits="PlussAndLoan.UI.QuickReport" %>

<html runat="server">
    <head>
    <title></title>

</head>
    <body>
        <form runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center" valign="top">

                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr align="center" valign="middle" bgcolor="#FFFFFF">
                        <td height="30" style="text-align: center; font-weight: bold; font-size: 14px">QUICK LOAN REPORT<font face="Verdana, Arial, Helvetica, sans-serif">
          </font>
                            </font>       	  </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>



    <asp:table id="qTable" width="780" border="1" align="center" cellpadding="0" cellspacing="1" bgcolor="BDD3A5" runat="server">
        <asp:TableRow bgcolor="BDD3A5" Height="28">
            <asp:TableCell ColumnSpan="10">
                <font face="Verdana," size="3" color="#000033">
                 <div runat="server" id="proDiv" style="text-align: left; font-weight: bold;">&nbsp;&nbsp;</div>
	             </font>
            </asp:TableCell>
        </asp:TableRow>
        
         <asp:TableRow>
             <asp:TableCell ColumnSpan="10"  style="text-align: center; font-weight: bold;" height="25" valign="middle" BackColor="#DCE8CE"> 
                Sales <br/>
                 <div runat="server" id="monthDiv" style="text-align: center;"></div> 
	             
             </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow bgcolor="EDF2E6" Height="22">
             <asp:TableCell Width="130">&nbsp;</asp:TableCell>
             <asp:TableCell style="text-align: center; color: #000099; ">RECEIVED</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">RE-RECEIVED</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">VERIFICATION</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">ON-HOLD</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">COND. APPROVED</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">APPROVED</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">DEFERRED</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">DECLINED</asp:TableCell>  
             <asp:TableCell style="text-align: center; color: #000099; ">DISBURSED</asp:TableCell>  

         </asp:TableRow>
        
        
    </asp:table>

    <table width="780" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="BDD3A5">
        <tr height="22" bgcolor="EDF2E6">
            <td style="text-align: right;" colspan="2">Approval Rate: </td>
            <td  style="text-align: left;" colspan="2"><div runat="server" id="appDiv"></div> </td>
        </tr>
        <tr height="22" bgcolor="EDF2E6">
            <td style="text-align: right;" colspan="2">Disbursement Rate: </td>
            <td style="text-align: left;" colspan="2"><div runat="server" id="disbDiv"></div></td>
        </tr>
    </table>
    <br>

<table cellSpacing="1" cellPadding="0" width="780" align="center" bgColor="#AAC292" border="0">
  <tr>
      <td  bgColor="#d1e0be" height="25">
        <p style="text-align: center;"><font face="Verdana," size="2" ><b>Service
                Quality</b><br/>
		</font></p>
          <div runat="server" id="tittle2Div" style="text-align: center;"></div>
      </td>
  </tr>

</table>
    
    <table cellSpacing="2" cellPadding="0" width="780" align="center" bgColor="#FFFFFF" border="0">
       
        <tr height="22">
    <td >&nbsp;</td>
    <td background="">&nbsp;</td>
    <td colspan="3" align="center" >  
    <td>&nbsp;</td>
    <td align="center"  >&nbsp;</td>
  </tr>
  <tr height="22">
    <td >&nbsp;</td>
    <td background="">&nbsp;</td>
    <td colspan="3" style="text-align: center; font-weight: bold;" bgcolor="#ADCA8E" ><font size="2" face="Arial, Helvetica, sans-serif">Customer Experience TAT</font>  
    <td>&nbsp;</td>
    <td style="text-align: center; font-weight: bold;" bgcolor="#ADCA8E"  ><font size="2" face="Arial, Helvetica, sans-serif">Operational TAT</font></td>
  </tr>
  <tr height="22"> 
    <td width="232" >&nbsp; &nbsp;</td>
	<td width="27" background="">&nbsp;</td>
    <td width="138"  bgcolor="#d1e0be" style="text-align: center; font-weight: bold;" > 
      <font face="Arial" size="-1" >Standard Applications </font>
	<td width="55">&nbsp;</td>
    <td width="137" bgcolor="#d1e0be"  > 
      <div style="text-align: center; font-weight: bold;">
          <p><font face="Arial" size="-1">Deferred/
        Sent Back Applications </font></p>
    </div></td>
    <td width="54">&nbsp;</td>
    <td width="129" style="text-align: center; font-weight: bold;" bgcolor="#d1e0be"  > 
      <font face="Arial" size="-1">All Applications </font></td>
  </tr>
        <tr height="22">
           <td bgcolor="#eaf1e0" ><left><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2" font><b>Number Of Application</b></font></left> </td>
    <td background="">&nbsp;</td> 
             <td align="center" bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
                 <div runat="server" id="app1" style="text-align: center;"></div>
             </font>  </td>
    <td>&nbsp;</td>
 <td align="center" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
     <div runat="server" id="app2" style="text-align: center;"></div>
 </font></td>
    <td>&nbsp;</td>
             <td align="center" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
             <div runat="server" id="totApp" style="text-align: center;"></div>    
              </font></td>
         </tr> 
        
          <tr height="22">
    <td bgcolor="#d1e0be" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2" font><b>% Of Application </b></font> </td>
    <td background="">&nbsp;</td>
               <td align="center" bgcolor="#d1e0be" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
                   <div runat="server" id="standardApp" style="text-align: center;"></div>
                   </font></td>
               <td>&nbsp;</td>
    <td align="center" bgcolor="#d1e0be"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
         <div runat="server" id="deferApp" style="text-align: center;"></div>
      </font></td>
     <td>&nbsp;</td>
    <td align="center" bgcolor="#d1e0be" style="text-align: center;" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">100% </font></td>
              
        </tr>
        
          <tr height="22">
    <td >&nbsp;</td>
    <td background="">&nbsp;</td>
    <td align="center" >  
    <td>&nbsp;</td>
    <td align="center"  >&nbsp;</td>
    <td>&nbsp;</td>
    <td style="text-align: center; font-weight: bold;" ><font title="Arial">In day(s)</font></td>
  </tr>
        
     <tr height="22">
    <td bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2">TAT-Source (Customer To Sales) </font></td>
    <td background="">&nbsp;</td>
     <td align="center" bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
       <div runat="server" id="StanTat" style="text-align: center;"></div> 
    </font> </td> 
    <td>&nbsp;</td>  
    <td align="center" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
         <div runat="server" id="DeffTat" style="text-align: center;"></div> 
      

    </font></td>
    <td>&nbsp;</td>
    <td style="text-align: center;" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">NA</font></td>
          
    </tr>
        
      <tr height="22">
    <td bgcolor="#dce8ce" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2">TAT-Source To Credit</font></td>
    <td background="">&nbsp;</td>
     <td align="center" bgcolor="#dce8ce" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
           <div runat="server" id="CreditDiv1" style="text-align: center;"></div> 
     </font>  </td>
    <td>&nbsp;</td>
 <td align="center" bgcolor="#dce8ce"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
        <div runat="server" id="CreditDiv2" style="text-align: center;"></div> 
    
    </font></td>
    <td>&nbsp;</td>
    <td style="text-align: center;" bgcolor="#dce8ce"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">NA</font></td>

      </tr>
        
   <tr height="22">
    <td bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2">TAT-Credit Operations </font></td>
    <td background="">&nbsp;</td>
       
    <td align="center" bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
          <div runat="server" id="Operation11" style="text-align: center;"></div> 
    </font> </td>
    <td>&nbsp;</td>
       
 <td align="center" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
      <div runat="server" id="Operation22" style="text-align: center;"></div> 
 </font></td>
    <td>&nbsp;</td>
     <td align="center" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
          <div runat="server" id="divGetCredit" style="text-align: center;"></div> 
 </font></td>
   </tr>
        
 <tr height="22">
    <td bgcolor="#dce8ce" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2">TAT-Credit Verification</font></td>
    <td background="">&nbsp;</td>
         <td align="center" bgcolor="#dce8ce" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
             <div runat="server" id="divVerification1" style="text-align: center;"></div>  
     
    </font>  </td>
    <td>&nbsp;</td>
     <td align="center" bgcolor="#dce8ce"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
         <div runat="server" id="divVerification2" style="text-align: center;"></div> 
    </font></td>
    <td>&nbsp;</td>
    <td style="text-align: center;" bgcolor="#dce8ce"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">NA</font></td>
 </tr>
 <tr height="22">
    <td bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2">TAT-Credit To Asset</font></td>
    <td background="">&nbsp;</td>
       <td align="center" bgcolor="#eaf1e0" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
          
            <div runat="server" id="divAsset1" style="text-align: center;"></div> 
       </font> </td> 
    <td>&nbsp;</td>
       <td align="center" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
           <div runat="server" id="divAsset2" style="text-align: center;"></div> 
     
    </font></td>
    <td>&nbsp;</td>
    <td style="text-align: center;" bgcolor="#eaf1e0"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">NA</font></td>
 </tr>
  <tr height="22">
    <td bgcolor="#dce8ce" ><font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2">TAT-Asset operations</font></td>
    <td background="">&nbsp;</td>
    <td align="center" bgcolor="#dce8ce" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
        <div runat="server" id="divAssetOp1" style="text-align: center;"></div> 
    </font>  </td>
    <td>&nbsp;</td>
          <td align="center" bgcolor="#dce8ce"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
             
               <div runat="server" id="divAssetOp2" style="text-align: center;"></div> 
          </font></td>
    <td>&nbsp;</td>
        <td align="center" bgcolor="#dce8ce"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
             <div runat="server" id="divGetAsset" style="text-align: center;"></div> 
       </font></td>
 </tr>
  <tr height="22">
    <td bgcolor="#D2D8AF" ><left> <font face="Verdana, Arial, Helvetica, sans-serif" color="#000099" size="2" ><b>TAT- Total </b></font></left></td>
    <td background="">&nbsp;</td>
          <td align="center" bgcolor="#D2D8AF" ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
      <div runat="server" id="divTotal1" style="text-align: center; font-weight: bold;"></div> 
    </font>  </td>
    <td>&nbsp;</td>
       <td align="center" bgcolor="#D2D8AF"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
 <div runat="server" id="divTotal2" style="text-align: center; font-weight: bold;"></div> 
    </font></td>
    <td>&nbsp;</td>

 <td align="center" bgcolor="#D2D8AF"  ><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
      <div runat="server" id="divTotal" style="text-align: center; font-weight: bold;"></div> 
		
	
    </font></td>

 </tr>

     </table>
    <br/>
    <br/>
    </form>
    </body>

</html>
