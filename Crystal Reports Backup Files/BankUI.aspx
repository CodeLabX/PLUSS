﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_BankUI" Title="Bank Information" Codebehind="BankUI.aspx.cs" %>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div style="margin-left: 100px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4" align="center">
                    <asp:Label ID="Label1" runat="server" Text="Bank Information" Font-Bold="True" Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="idLabel" runat="server" Visible="false" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="bankInfoGridView" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ClearButton" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
          <%--  <tr>
                <td align="right" style="padding-left: 75px">
                    <asp:Label ID="Label4" runat="server" Text="Bank Code:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:TextBox ID="bankCodeTextBox" runat="server" Width="150px"></asp:TextBox>
                            <label id="bankCodeMF" style="color:Red;">*</label>
                            <asp:RequiredFieldValidator ID="bankCodeRequiredFieldValidator" runat="server" 
                                ControlToValidate="bankCodeTextBox" ErrorMessage="Enter bank code"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="bankInfoGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>--%>
            <tr>
                <td align="right">
                    <asp:Label ID="Label5" runat="server" Text="Bank Name:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:TextBox ID="bankNameTextBox" runat="server" Width="150px"></asp:TextBox>
                            <label id="Label2" style="color:Red;">*</label>
                            <asp:RequiredFieldValidator ID="bankNameRequiredFieldValidator" runat="server" 
                                ControlToValidate="bankNameTextBox" ErrorMessage="Enter bank name"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="bankInfoGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="statusLabel" runat="server" Text="Status: "></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:DropDownList ID="statusDropdownList" runat="server">
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">Inactive</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="bankInfoGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left" colspan="2">
                    &nbsp;
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Button ID="addButton" runat="server" Text="Save" Width="100px" Height="25px"
                                OnClick="addButton_Click" />
                            &nbsp;<asp:Button ID="ClearButton" runat="server" Height="25px" OnClick="ClearButton_Click"
                                Text="Clear" Width="100px" CausesValidation="False" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="bankInfoGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" height="20">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Always" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="bankInfoGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="4">                   
                    <table class="GvTableHeader" width="437px">
                        <tr>
                            <td align="center" width="65">
                                SL No.</td>
                           <%-- <td align="center" width="165">
                                Bank Code</td>--%>
                            <td align="center" width="205">
                                Bank Name</td>
                            <td align="center" width="120">
                                Status</td>
                        </tr>
                    </table>
                </td>                
            </tr>
            <tr>
                <td colspan="4" align="left">     
                    <div style="overflow-y: scroll; height:380px; width:417px;" class="GridviewBorder">
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Always">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="addButton" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:GridView ID="bankInfoGridView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    Font-Size="11px" ForeColor="#333333" PageSize="8" OnSelectedIndexChanged="bankInfoGridView_SelectedIndexChanged"
                                    DataKeyNames="Id" OnRowDataBound="bankInfoGridView_RowDataBound" 
                                    Width="417px" ShowHeader="False">
                                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                                    <Columns>
<%--                                        <asp:TemplateField HeaderText="SL No." HeaderStyle-Width="70px">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <HeaderStyle Width="70px"></HeaderStyle>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Bank Id" HeaderStyle-Width="150px" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="idLabel" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="150px"></HeaderStyle>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Bank Code" HeaderStyle-Width="140px">
                                            <ItemTemplate>
                                                <asp:Label ID="codeLabel" runat="server" Text='<%# Bind("Code") %>' Width="140px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="140px"></HeaderStyle>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Bank Name" HeaderStyle-Width="180px">
                                            <ItemTemplate>
                                                <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' Width="180px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="180px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StatusHidden" HeaderStyle-Width="100px" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="statusLabel" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="statusMsgLabel" runat="server" Text='<%# Bind("StatusMsg") %>' Width="90px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Left" />
                                    <EmptyDataTemplate>
                                        &nbsp;&nbsp;No data found.
                                    </EmptyDataTemplate>
                                    <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="FixedHeader2" />
                                    <EditRowStyle BackColor="#999999" />
                                    <FooterStyle CssClass="GvFixedFooter" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">

    </asp:Content>

