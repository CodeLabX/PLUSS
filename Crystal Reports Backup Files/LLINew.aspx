﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.Reports.Reports_LLINew" Codebehind="LLINew.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script language="javascript" type="text/javascript">
        function PrintLLINew()
        {
            window.print();
        }
        function ColorChange()
        {
             document.getElementById('printButton').style.backgroundColor = 'red';
        }
    </script>
    <title>Limit Loading Instruction</title>
</head>
<body style="margin-top:0.25px; left:0.25px; right:0px; margin-bottom:0.15px;" onload="PrintLLINew();">
    <form id="form1" runat="server" style="font-size:small;">
    <center>
    <table width="95%" border="0" cellpadding="0" cellspacing="0" >
    <tr><td><table width="95%">
            <tr>
                <td style="font-size:14px; font-weight:bold;" align="right">
                    <asp:Label ID="reportNameLabel" runat="server" Text="LIMIT LOADING INSTRUCTION: PERSONAL LOAN"></asp:Label>
                    
                </td>
                <td align="right">
                <table style="border-bottom-style:solid; border-bottom-width:thin; border-bottom-color:Black; border-top-color:Black; border-top-width:thin; border-top-style:solid;">
                    <tr>
                        <td>
                            <table style="font-size:small; border:0px;">
                                <tr style="border-collapse:collapse;">
                                    <td style="border:0px;">
                                        Standard
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:0px;">
                                        Chartered
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <img src="../Images/scb_logo.jpg" style="height: 35px; width: 23px;" alt="image" />
                        </td>
                    </tr>
                </table>                    
                </td>
            </tr>
        </table></td></tr>
    <tr><td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color:Black; border-style:solid; border-width:thin; border-bottom-width:0px; padding-top:5px">
            <tr>
                <td style="width:90px;">
                    Sales Person:
                </td>
                <td style="width:100px;">
                <br />
                    <asp:Label ID="salesPersonLabel" runat="server" Text=""></asp:Label>                    
                    <hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:90px;">
                    Referral Code:
                </td>
                <td style="width:100px;">
                <br />
                    <asp:Label ID="referralCodeLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:60px;">
                    Branch:
                </td>
                <td style="width:100px;">
                <br />
                    <asp:Label ID="branchLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td style="width:153px;">
                    Facility No:
                </td>
                <td style="width:218px;">
                    <asp:Label ID="facilityNoLabel" runat="server" Text=""></asp:Label>
                    <hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:151px;">
                    Sales Channel:
                </td>
                <td style="width:189px;">
                    <asp:Label ID="salesChannelLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:79px;">
                    Date:
                </td>
                <td style="width:190px;">
                    <asp:Label ID="dateLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
        </table></td></tr>
    <tr><td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color:Black; border-style:solid; border-width:thin; border-bottom-width:0px; padding-top:5px">
            <tr>
                <td style="width:100px; font-weight:bold;">
                    Name:
                </td>
                <td colspan="3" style="width:350px;">
                    <asp:Label ID="nameLabel" runat="server" Font-Bold="true" Text = ""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:120px;">
                    MIS4:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="llidLabel" runat="server" Text = ""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td style="width:100px; font-weight:bold;">
                    Criteria Code:
                </td>
                <td style="width:120px;">
                    <asp:Label ID="criteriaCodeLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:100px; font-weight:bold;">
                    Asset Code:
                </td>
                <td style="width:125px;">
                    <asp:Label ID="assetCodeLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:120px; font-weight:bold;">
                    Asset Category:
                </td>
                <td style="width:105px; font-weight:bold;">
                    DEF
                    <hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr style="width:100px;">
                <td>
                    Loanpurposecode:
                </td>
                <td style="width:209px;">
                    <asp:Label ID="purposeCodeLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:655px;" colspan="4"></td>
            </tr>           
            
        </table></td></tr>
    <tr><td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color:Black; border-style:solid; border-width:thin;border-bottom-width:0px; padding-top:5px">
            <tr>
                <td style="width:250px; font-weight:bold;">
                    Loan Amount:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="loanAmountLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:60px;">
                    Tenure:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="tenureLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:100px">
                    Start Date:
                </td>
                <td style="width:100px;">
                <br />
                    <asp:Label ID="startDateLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td style="width:20%;">
                                In Words (BDT):
                            </td>
                            <td>
                                <asp:Label ID="inWordsLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td rowspan="1" colspan="2">
                    <table>
                        <tr>
                            <td style="width:100px">
                                Link Account:
                            </td>
                        </tr>
                        <tr>
                            <td style="width:125px; border-color:Black; border-style:solid; border-width:thin;">
                                <asp:Label ID="linkAccountLabel" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td rowspan="1" colspan="4">
                    <table>
                        <tr>
                            <td style="width:60px; font-weight:bold;">
                                EMI:
                            </td>
                            <td style="width:140px;">
                                <asp:Label ID="emiLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                            <td style="width:105px; font-weight:bold;">
                                End Date:
                            </td>
                            <td style="width:140px;">
                                <asp:Label ID="endDateLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                            <td style="width:140px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:60px;">
                                Salary:
                            </td>
                            <td style="width:140px;">
                                <asp:Label ID="incomeLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                            <td style="width:105px">
                                DBR:
                            </td>
                            <td style="width:140px;">
                                <asp:Label ID="dbrLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                            <td style="width:140px;">
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width:105px; font-weight:bold;">
                    
                    <asp:Label ID="InterestRateupLabel" runat="server" Font-Bold="true" Text="Interest Rate:"></asp:Label>
                </td>
                <td style="width:100px;">
                    <asp:Label ID="InterestRateLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:90px;">
                    LTV:
                </td>
                <td style="width:140px;">
                    <asp:Label ID="ltvLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:95px;">
                    Debts:
                </td>
                <td style="width:140px;">
                    <asp:Label ID="debtsLabel" runat="server" Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
        </table></td></tr>
    <tr><td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color:Black; border-style:solid; border-width:thin; border-bottom-width:0px; padding-top:5px">
            <tr>
                <td  style="width:100px;">
                    FTP:
                </td>
                <td style="width:100px;">
                <br />
                    <asp:Label ID="ftpLabel" runat='server' Font-Bold="true" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:150px;">
                    Employercategory:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="empCategoryCodeLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:120px;">
                    Deviationcode:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="deviationCodeLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td  style="width:100px;">
                    Loantypecode:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="loanTypeLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:150px;">
                    Email2:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="loanCategoryLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:120px;">
                    Date of Joining:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="joiningDateLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td  style="width:100px;">
                    ISIC Code:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="isicCodeLabel" runat='server' Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:150px;">
                    Discretion Code:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="discretionCodeLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:120px;">
                    Collectiondata:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="collectionDataLabel3" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td  style="width:100px;">
                    FAX2:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="companyCodeLabel" runat='server' Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:150px;">
                    Emp Reference:
                </td>
                <td style="width:320px;" colspan="3">
                    <asp:Label ID="empReferenceLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td  style="width:100px;">
                </td>
                <td style="width:100px;">
                                    </td>
                <td style="width:150px;">
                    
                </td>
                <td style="width:100px;">
                    
                </td>
                <td style="width:120px;">
                    TIN:
                </td>
                <td style="width:100px;">
                    <asp:Label ID="tinLabel" runat="server" Font-Bold="true"  Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
        </table></td></tr>
    <tr><td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color:Black; border-style:solid; border-width:thin; padding-top:5px">
            <tr>
                <td  style="width:65px;">
                    PFC:
                </td>
                <td style="width:80px;">
                <br />
                    <asp:Label ID="pfcLabel" runat='server' Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:65px;">
                    COMM:
                </td>
                <td style="width:80px;">
                <br />
                    <asp:Label ID="commLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:145px;">
                    Stamp Charge:
                </td>
                <td style="width:80px;">
                <br />
                    <asp:Label ID="Label3" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
                <td style="width:65px;">
                    P.O.#:
                </td>
                <td style="width:80px;">
                <br />
                    <asp:Label ID="poLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <td style="width:100px;">
                                Notarization fee:
                            </td>
                            <td style="width:90px;">
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                            <td style="width:100px;">
                                Non-Judi fee:
                            </td>
                            <td style="width:60px;">
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="3">
                    <table>
                        <tr>
                            <td style="width:80px;">
                                Beneficiary
                            </td>
                            <td colspan="3" style="width:350px;">
                               <asp:Label ID="beneficiaryLabel" runat="server" Text=""></asp:Label><hr style="color:Black; border-width:thin;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table></td></tr>
    <tr><td>        <table style="width:100%; padding-top:5px">
            <tr>
                <td style="font-weight:bold;" align="left">
                    Remarks:
		<table style="width:100%;border-color:Black; border-style:solid; border-width:thin;">
            <tr>
                <td style="height:40px;">
		&nbsp;
                </td>
            </tr>
		</table>
                </td>
            </tr>
            <tr>
                <td style="border-color:Black; border-style:solid; border-width:thin; text-align:justify; font-size:11px;">
                    We certify that all the security documentation have been completed, physically checked, lodged and are in order. We further confirm compliance with all the terms pertaining to documentation as detailed in the Banking Arrangement Letter accepted by customer.
                </td>
            </tr>
        </table></td></tr>
    <tr><td><table width="100%">
            <tr>
                <td align="left">
                <br />
		<br />
                    ______________________
                </td>
                <td align="right">
                    
                </td>
                <td align="right">
                <br />
		<br />
                ______________________
                </td>
            </tr>
            <tr>
                <td align="left" >
                    &nbsp&nbsp&nbsp Authorized Signature
                </td>
                <td align="right">
                    
                </td>
                <td align="right">
                    Authorized Signature &nbsp&nbsp
                </td>
            </tr>
            <tr>
                <td align="left" style="font-size:x-small;">
                    <table style="border-color:Black; border-style:solid; border-width:1px; font-size:9px;">
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Step:
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                Maker
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                Checker
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Relationship
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Master
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Open Loan
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Open Coll.
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Int Rate
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Set Sl:
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Max Sweep
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                Set Limit
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-top:none; border-right:none">
                                PLS
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-left:none; border-bottom:none; border-right:none; border-top:none;">
                                Voucher
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-bottom:none; border-top:none; border-right:none;">
                                &nbsp
                            </td>
                            <td align="center" style="border-color:Black; border-style:dotted; border-width:1px; border-collapse:collapse; border-right:none; border-bottom:none; border-top:none;">
                                &nbsp
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/scbSeal.JPG" />
                </td>
                <td align="right">
                    <table>
                        <tr>
                            <td>
                                <h3><u> &nbsp&nbsp&nbsp P ............................... L  &nbsp&nbsp&nbsp</u> </h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3> &nbsp&nbsp&nbsp e-Lending Loan Account &nbsp&nbsp&nbsp </h3>
                            </td>
                        </tr>
                    </table>                   
                </td>
            </tr>
        </table></td></tr>
    <tr><td><table width="100%">
            <tr>
                <td align="left">
		<br />
                    ______________________
                </td>
                <td align="right">
		<br />
                    ______________________
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp&nbsp&nbsp Authorized Signature
                </td>
                <td align="right">
                    Authorized Signature &nbsp&nbsp
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="signatureLabel1" runat="server" Text="&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp(Maker Admin)"></asp:Label>
                </td>
                <td align="right">
                    <asp:Label ID="signatureLabel2" runat="server" Text="(Checker Admin)&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                <asp:Label ID="designationLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="right">
                 <asp:Label ID="designationLabel2" runat="server" Text=""></asp:Label>
                 </td>
            </tr>
        </table></td></tr>
    </table>    
    </center>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="95%"><tr><td align="left">
    <div>
        <table width="95%">
            <tr>
                <td style="font-size:large; font-weight:bold;" align="center">
                    SECURITY COMPLIANCE CERTIFICATE
                </td>
                <td align="right">
                <table style="border-bottom-style:solid; border-bottom-width:thin; border-bottom-color:Black; border-top-color:Black; border-top-width:thin; border-top-style:solid;">
                    <tr>
                        <td>
                            <table style="font-size:small; border:0px;">
                                <tr style="border-collapse:collapse;">
                                    <td style="border:0px;">
                                        Standard
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:0px;">
                                        Chartered
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <img src="../Images/scb_logo.jpg" style="height: 35px; width: 23px;" alt="image" />
                        </td>
                    </tr>
                </table>
                    
                </td>
            </tr>
        </table>
    </div>
    
    <div style="font-size:medium;">
        <table border="0" cellpadding="0" cellspacing="0" width="95%"><tr><td align="left">
        <b>Customer's Name : </b><asp:Label ID="customersNameLabel" runat="server" Text="" Font-Bold="true"></asp:Label>
        <br />
        <b>Description of Documents : </b><asp:Label ID="descriptionOfDocumentLabel" runat="server" Text="" Font-Bold="true"></asp:Label>
        </td></tr></table>
        <br />        
        <table width="95%" style="border-color:Black; border-style:solid; border-width:thin;">
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Approved Application Form
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    AOF
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Demand Promissory Note for BDT &nbsp&nbsp&nbsp <asp:Label ID="promissoryNoteLabel" runat="server" Text="" Font-Underline="true"></asp:Label>
                </td>
                <td style="width:35px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    DPN
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Letter of Continuation for BDT &nbsp <asp:Label ID="letterOfContinuationLabel" runat="server" Text="" Font-Underline="true"></asp:Label>
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    LOC
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Letter of Guarantee
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    PRG
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Irrevocable Letter of Authority
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    ILA
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Standing Order
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    DRA
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    Banking Arrangement Letter Dated
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    BAL
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    UDC for BDT : &nbsp&nbsp&nbsp <asp:Label ID="udcForBDTLabel" runat="server" Text="" Font-Underline="true"></asp:Label>
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    UDC
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-collapse:collapse">
                    No. of PDC(s) &nbsp&nbsp&nbsp 
                    <asp:Label ID="noOfPDCLabel" runat="server" text="" Font-Underline="true"></asp:Label> 
                    &nbsp&nbsp&nbsp amounting each PDC BDT &nbsp&nbsp&nbsp
                    <asp:Label ID="pdcBDTLabel" runat="server" Text="" Font-Underline="true"></asp:Label>
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-collapse:collapse" align="center">
                    PDC
                </td>
            </tr>
            <tr>
                <td style="width:15px; height:40px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-left:none; border-bottom:none; border-collapse:collapse" align="center">
                    ?
                </td>
                <td style="width:620px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-left:none; border-bottom:none; border-collapse:collapse">
                    Letter of EOSB assignment
                </td>
                <td style="width:35px; border-color:Black; border-style:solid; border-width:thin; border-top:none; border-right:none; border-bottom:none; border-collapse:collapse" align="center">
                    EOS
                </td>
            </tr>
        </table>
    </div>
    </td></tr></table>
    </center>
    </form>
</body>
</html>
