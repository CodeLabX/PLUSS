﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="MFUApprove.aspx.cs" Inherits="PlussAndLoan.UI.MFU.MFUApprove" %>

<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //function pageLoad() {
        //    $('#ctl00_ContentPlaceHolder_txtUploadDate').unbind();
        //    $('#ctl00_ContentPlaceHolder_txtUploadDate').datepicker({
        //        showOn: 'button',
        //        buttonImage: '../../Image/Calendar_scheduleHS.png',
        //        buttonImageOnly: true,
        //        changeMonth: true,
        //        changeYear: true,
        //        dateFormat: 'yy-mm-dd'
        //    });

        //}

        //function Hello() {
        //    document.getElementById("ctl00_ContentPlaceHolder_UploadButton").disabled = true;
        //    document.getElementById("ctl00_ContentPlaceHolder_messageLabel").innerHTML = "Data is uploading ........!!!! It will take few moments, Please don't close the window";
        //}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <modal:modalPopup runat="server" ID="modalPopup" />

    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">MFU Approval</div>
        <%--     <fieldset>
            <legend>Approve MFU</legend>
            <table cellspacing="5px" cellpadding="5px" width="600">
                <tr>
                    <td>
                        <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtn" runat="server" Text="Move To Main" OnClick="MoveToMainBtn_Click" />

                    </td>
                    <td>
                        <asp:Button CssClass="btn primarybtn" ID="DownloadBtn" runat="server" Text="Download Uploaded Data" OnClick="DownloadUploadedBtn_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>--%>

        <table style='border: 0px double; width: 98%' align='center' border='0'>

            <tr>
                <td>
                    <label for="field1">
                        <span style="text-align: center">File Name</span>
                    </label>
                </td>
                <td>
                    <label for="field1" style="width: 50px;">
                        <span style="text-align: center">Upload Date</span>
                    </label>
                </td>
                <td>
                    <label for="field1" style="width: 50px;">
                        <span style="text-align: center">Row Count</span>
                    </label>
                </td>
                <td>
                    <label for="field1" style="width: 50px;">
                        <span style="text-align: center">Error Count</span>
                    </label>
                </td>
                <td>
                    <label for="field1" style="width: 50px;">
                        <span style="text-align: center">Maker ID</span>
                    </label>
                </td>
                <td colspan="4" style="text-align: center; font-weight: bold">
                    <span>Action</span>
                </td>
            </tr>

            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtCC_CHARGEOFF" ReadOnly="true" Text="CC_CHARGEOFF" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" Style="width: 100px;" ReadOnly="true" runat="server" ID="txtDateCC_CHARGEOFF"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCountCC_CHARGEOFF" Style="width: 100px;" ReadOnly="true" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCountCC_CHARGEOFF" Style="width: 100px;" ReadOnly="true" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtMakerIDCC_CHARGEOFF" Style="width: 100px;" ReadOnly="true" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtnCC_CHARGEOFF" runat="server" Text="Approve" OnClick="MoveToMainBtnCC_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownloadUploadedBtnCC_CHARGEOFF" runat="server" Text="Uploaded Data Download" OnClick="DownloadUploadedBtnCC_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnCC_CHARGEOFF" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnCC_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnCC_CHARGEOFF" runat="server" Text="Delete" OnClick="ClearTempBtnCC_CHARGEOFF_Click" />
                </td>
            </tr>


            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="TextBox1" ReadOnly="true" Text="CC_DISBURSED" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" ReadOnly="true" Style="width: 100px;" ID="txtDateCC_DISBURSED"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCountCC_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCountCC_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtMakerIDCC_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtnCC_DISBURSED" runat="server" Text="Approve" OnClick="MoveToMainBtnCC_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownloadUploadedBtnCC_DISBURSED" runat="server" Text="Uploaded Data Download" OnClick="DownloadUploadedBtnCC_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnCC_DISBURSED" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnCC_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnCC_DISBURSED" runat="server" Text="Delete" OnClick="ClearTempBtnCC_DISBURSED_Click" />
                </td>
            </tr>

            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="TextBox4" ReadOnly="true" Text="LOAN_CHARGEOFF" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" ReadOnly="true" Style="width: 100px;" ID="txtDateLOAN_CHARGEOFF"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCountLOAN_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCountLOAN_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtMakerIDLOAN_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtnLOAN_CHARGEOFF" runat="server" Text="Approve" OnClick="MoveToMainBtnLOAN_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownloadUploadedBtnLOAN_CHARGEOFF" runat="server" Text="Uploaded Data Download" OnClick="DownloadUploadedBtnLOAN_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_CHARGEOFF" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_CHARGEOFF" runat="server" Text="Delete" OnClick="ClearTempBtnLOAN_CHARGEOFF_Click" />
                </td>
            </tr>

            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="TextBox7" ReadOnly="true" Text="LOAN_DISBURSED" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" ReadOnly="true" runat="server" Style="width: 100px;" ID="txtDateLOAN_DISBURSED"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCountLOAN_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCountLOAN_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtMakerIDLOAN_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtnLOAN_DISBURSED" runat="server" Text="Approve" OnClick="MoveToMainBtnLOAN_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownloadUploadedBtnLOAN_DISBURSED" runat="server" Text="Uploaded Data Download" OnClick="DownloadUploadedBtnLOAN_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_DISBURSED" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_DISBURSED" runat="server" Text="Delete" OnClick="ClearTempBtnLOAN_DISBURSED_Click" />
                </td>
            </tr>

            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="TextBox10" ReadOnly="true" Text="LOAN_EBBS_CHARGEOFF" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" ReadOnly="true" runat="server" Style="width: 100px;" ID="txtDateLOAN_EBBS_CHARGEOFF"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCountLOAN_EBBS_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCountLOAN_EBBS_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtMakerIDLOAN_EBBS_CHARGEOFF" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Approve" OnClick="MoveToMainBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownloadUploadedBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Uploaded Data Download" OnClick="DownloadUploadedBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_EBBS_CHARGEOFF" runat="server" Text="Delete" OnClick="ClearTempBtnLOAN_EBBS_CHARGEOFF_Click" />
                </td>
            </tr>

            <tr>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="TextBox13" ReadOnly="true" Text="LOAN_EBBS_DISBURSED" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox CssClass="input-field" runat="server" ReadOnly="true" Style="width: 100px;" ID="txtDateLOAN_EBBS_DISBURSED"></asp:TextBox>
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtRowCountLOAN_EBBS_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtErrorCountLOAN_EBBS_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:TextBox ID="txtMakerIDLOAN_EBBS_DISBURSED" ReadOnly="true" Style="width: 100px;" runat="server" CssClass="input-field" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="MoveToMainBtnLOAN_EBBS_DISBURSED" runat="server" Text="Approve" OnClick="MoveToMainBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownloadUploadedBtnLOAN_EBBS_DISBURSED" runat="server" Text="Uploaded Data Download" OnClick="DownloadUploadedBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn primarybtn" ID="DownLoadErrorBtnLOAN_EBBS_DISBURSED" runat="server" Text="Error Download" OnClick="DownLoadErrorBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
                <td style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px">
                    <asp:Button CssClass="btn clearbtn" ID="ClearTempBtnLOAN_EBBS_DISBURSED" runat="server" Text="Delete" OnClick="ClearTempBtnLOAN_EBBS_DISBURSED_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
