﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ApplicationDeferredOrDeclined.aspx.cs" Inherits="PlussAndLoan.UI.ApplicationDeferredOrDeclined" %>

<html runat="server">
<head>
    <title></title>

    <script language="javascript" type="text/javascript">
        function PrintDiv() {
            var myContentToPrint = document.getElementById("printDiv");
            var myWindowToPrint = window.open('', '', 'width=800,height=600,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>
</head>
<body>
    <form runat="server">
        <div id="printDiv">
            <table width="100%" height="45" cellpadding="2" cellspacing="1" bordercolor="#00CCCC" style='border: 1px solid #333333;'>
                <tr bgcolor="BDD3A5">
                    <td height="20" colspan="7" align="left" valign="middle">
                        <div align="center">
                            <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
           <div id="titleDiv" runat="server" style="text-align: center; font-weight: bold; font-size: 14px"> </div>
          </font>
                        </div>
                    </td>
                    <td align="right" valign="middle" bgcolor="BDD3A5" colspan="3">
                        <asp:ImageButton ID="imgexpand" runat="server" ImageAlign="Bottom" ImageUrl="images\printer.gif" Height="25px" Width="78px" OnClientClick="PrintDiv()" />

                    </td>

                </tr>
                <tr bgcolor="BDD3A5">
                    <td width="10%" height="20" bgcolor="#D5E2C5" colspan="1">
                        <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><B>PRODUCT:</B></font></div>
                    </td>
                    <td width="23%" bgcolor="#E7F7C6" colspan="2">
                        <div align="left" id="productDiv" runat="server">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
             
              </font>
                        </div>
                    </td>
                    <td width="10%" align="center" bgcolor="#E7F7C6" colspan="1">
                        <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><B> Month</B> 
            </font></td>
                    <td width="15%" align="center" bgcolor="#D5E2C5" colspan="2">
                        <div id="monthDiv" runat="server"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></div>
                    </td>
                    <td width="10%" align="center" bgcolor="#E7F7C6" colspan="1">
                        <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><B> Year</B> 
            </font></td>
                    <td width="11%" bgcolor="#D5E2C5">
                        <div id="yearDiv" runat="server" colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></div>
                    </td>

                </tr>
            </table>
            <br />

            <div style="padding-left: 1px; padding-right: 3px; width: 100%;">


                <div id="divReport" style="overflow-y: scroll; height: 100%; width: 100%;" class="GridviewBorder" runat="server">
                    <asp:GridView ID="reportgridView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Size="11px" PageSize="8" GridLines="Both" RowStyle-BorderWidth="1" RowStyle-BorderColor="Black"
                        Width="100%" EnableModelValidation="True" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                        <RowStyle ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="Reason" HeaderStyle-Width="50%" HeaderStyle-Height="25px">
                                <ItemTemplate>
                                    <asp:Label ID="USERNAME" runat="server" Text='<%# Bind("REAS_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Number" HeaderStyle-Width="50%">
                                <ItemTemplate>
                                    <asp:Label ID="Name" runat="server" Text='<%# Bind("ct") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>


                        </Columns>
                        <HeaderStyle BackColor="#BDD3A5" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#336666" ForeColor="Black" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            &nbsp;&nbsp;Welcome to Loan Locator.
                            <br />
                            No data found.
                        </EmptyDataTemplate>
                        <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <FooterStyle CssClass="GvFixedFooter" BackColor="White" ForeColor="#333333" />
                    </asp:GridView>
                </div>


            </div>


        </div>
    </form>
</body>
</html>
