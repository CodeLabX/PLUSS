﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.master" AutoEventWireup="true" CodeBehind="CheckList.aspx.cs" Inherits="PlussAndLoan.UI.CheckList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div>
    
        <table>
            <tr>
                <td colspan="3" align="left" style="padding-left:270px">
                    <asp:Label ID="Label1" runat="server" Text="Add or Update Check List  Information" Font-Bold="true" Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:HiddenField ID="idHiddenField" runat="server" /></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label11" runat="server" Text="Title"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="titleTextBox" runat="server" Width="250px"></asp:TextBox>
                    <asp:Label ID="Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" Text="Product"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="cmbProduct" runat="server" Width="254px">
                        </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                     <asp:Label ID="statusLabel" runat="server" Text="Status"></asp:Label>
                     </td>
                <td>
                    <asp:DropDownList ID="statusDropDowonList" runat="server" Width="180px">
                    <asp:ListItem  Value="1">Active</asp:ListItem>
                    <asp:ListItem  Value="0">Inactive</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                     <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                <asp:Button ID="saveButton" runat="server" Text="Add" Width="100px" Height="27px" OnClick="btnSave_Click" 
                        />&nbsp;
                
                <asp:Button ID="DeleteButton" runat="server" Text="Delete" Width="100px" Height="27px" OnClick="DeleteButton_Click"
                        />&nbsp;
                </td>
                <td>
                    &nbsp;</td>
            </tr>
           
        </table> 
            
    </div>
    
    <div style="overflow-y: scroll; height: 380px; width: 100%;" class="GridviewBorder">

                        <asp:GridView ID="CheckListGrid" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            Font-Size="11px" ForeColor="#333333" PageSize="8"
                            
                            Width="100%" EnableModelValidation="True" GridLines="None" OnSelectedIndexChanged="CheckListGrid_SelectedIndexChanged">
                            <RowStyle BackColor="#EFF3FB" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Title" HeaderStyle-Width="60%">
                                    <ItemTemplate>
                                        <asp:Label ID="titleLabel" runat="server" Text='<%# Bind("Title") %>' Width="70%"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="60%"></HeaderStyle>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Product Name" HeaderStyle-Width="20%">
                                    <ItemTemplate>

                                        <asp:Label Visible="false" ID="ProdIdLabel" runat="server" Text='<%# Bind("ProductId") %>'></asp:Label>
                                        <asp:Label ID="productNameLabel" runat="server" Text='<%# Eval("ProductName") %>' Width="40%"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="20%"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="status" runat="server" Text='<%# Bind("Status") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="StatusMsg" runat="server" Text='<%# Bind("StatusMsg") %>' Width="15%"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="15%"></HeaderStyle>
                                </asp:TemplateField>
                                
                                 <asp:TemplateField HeaderText="Id" HeaderStyle-Width="0%" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="Id" runat="server" Text='<%# Bind("Id") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="15%"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" Visible="true">
                                    <ItemTemplate>
                                        <asp:LinkButton Text="Select" ID="lnkSelect" runat="server" CommandName="Select"/>
                                    </ItemTemplate>
                                    <HeaderStyle Width="5%" HorizontalAlign="right"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                &nbsp;&nbsp;No data found.
                            </EmptyDataTemplate>
                            <EditRowStyle BackColor="#2461BF" />
                            <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <FooterStyle CssClass="GvFixedFooter" BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </div>
</asp:Content>
