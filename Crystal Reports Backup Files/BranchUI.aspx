﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_BranchUI" Title="Branch Information" CodeBehind="BranchUI.aspx.cs" %>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <style type="text/css">
        .hidden {
            display: none;
        }
    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div>
        <table class="style1" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4" align="center">
                    <asp:Label ID="Label1" runat="server" Text="Branch Information" Font-Bold="true"
                        Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="branchLabel" runat="server" Text="Branch Id:" Visible="false"></asp:Label>
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <asp:HiddenField ID="branchIdHiddenField" runat="server" />
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label3" runat="server" Text="Bank Name:"></asp:Label>
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="bankNameDropDownList" runat="server" Width="170px">
                    </asp:DropDownList>
                    <asp:Label ID="bankNameMF" runat="server" CssClass="MFeildForeColor" />
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" Text="Branch Name:"></asp:Label>
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="branchNameTextBox" runat="server" Width="170px"></asp:TextBox>
                    <label id="branchNmaeM" style="color: Red;">
                        *</label>
                    <asp:Label ID="branchNameMF" runat="server" CssClass="MFeildForeColor" />
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Text="Location:"></asp:Label>
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="locationDropDownList" runat="server" Width="170px">
                    </asp:DropDownList>
                    <asp:Label ID="locationMF" runat="server" CssClass="MFeildForeColor" />
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <asp:Label ID="Label5" runat="server" Text="Address:"></asp:Label>
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="addressTextBox" runat="server" Width="250px" Rows="2" TextMode="MultiLine"></asp:TextBox>
                    <label id="addressM" style="color: Red;">
                        *</label>
                    <asp:Label ID="addressMF" runat="server" CssClass="MFeildForeColor" />
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="clearingStatusLabel0" runat="server" Text="Clearing Status:"></asp:Label>
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="clearingStatusDropdownList" runat="server" Width="120px">
                        <asp:ListItem Value="0">Offline clear</asp:ListItem>
                        <asp:ListItem Value="1">Online clear</asp:ListItem>
                        <asp:ListItem Value="2">Offline not clear</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="activeLabel" runat="server" Text="Status:"></asp:Label>
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="statusDropdownList" runat="server" Width="120px">
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">Inactive</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <asp:Button ID="addButton" runat="server" Text="Add" Width="100px" OnClick="addButton_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px"
                        OnClick="clearButton_Click" />
                    <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px">
                    <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                    &nbsp;
                    <asp:Button ID="searchbutton" runat="server" Text="Search" Width="65px" OnClick="searchbutton_Click" />
                </td>
                <td>&nbsp;
                </td>
                <td>
                    <table>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td style="padding-left: 80px;">
                                <asp:FileUpload ID="FileUpload" runat="server" />
                            </td>
                            <td>&nbsp;<asp:Button ID="uploadButton" runat="server" Text="Upload" Height="20px" OnClick="uploadButton_Click" />
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding-left: 0px" colspan="4"></td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                     <div id="gridbox" style="background-color: white; width: 795px; height: 300px;"></div>
                </td>
            </tr>
        </table>
       
        <div style="display: none" runat="server" id="dvXml"></div>
    </div>

    <script type="text/javascript">
       var mygrid;
        mygrid = new dhtmlXGridObject('gridbox');
        mygrid.setImagePath("../codebase/imgs/");
        mygrid.setHeader("Id,'',Bank Name,Branch Name,'',Location,Address,'',Status,'',ClearingStatus");
        mygrid.setInitWidths("38,0,190,200,0,100,100,0,60,0,90");
        mygrid.setColAlign("right,left,left,left,left,left,left,left,left,left,left");
        mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
        mygrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str");
        mygrid.setSkin("light");
        mygrid.init();
        mygrid.enableSmartRendering(true, 20);
        //mygrid.loadXML("../includes/BranchList.xml");
        mygrid.parse(document.getElementById("xml_data"));
        mygrid.attachEvent("onRowSelect", doOnRowSelected);
        function doOnRowSelected(id) {
            debugger;
            if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {
                document.getElementById('ctl00_ContentPlaceHolder_addButton').value = "Update";
                document.getElementById('ctl00_ContentPlaceHolder_branchIdHiddenField').value = mygrid.cells(mygrid.getSelectedId(), 0).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_bankNameDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 1).getValue());
                document.getElementById('ctl00_ContentPlaceHolder_branchNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 3).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_locationDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 4).getValue());
                document.getElementById('ctl00_ContentPlaceHolder_addressTextBox').value = mygrid.cells(mygrid.getSelectedId(), 6).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_clearingStatusDropdownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 9).getValue());
                document.getElementById('ctl00_ContentPlaceHolder_statusDropdownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 7).getValue());
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_addButton').value = "Save";
            }
        }
        function LoadExcelData() {
            mygrid.clearAll();
            //mygrid.loadXML("../includes/BranchList.xml");
            mygrid.parse(document.getElementById("xml_data"));
        }


        function ShowGifImage() {
            document.getElementById('ctl00_ContentPlaceHolder_ImageButton').style.visibility = "visible";
        }
        function HidGifImage() {
            document.getElementById('ctl00_ContentPlaceHolder_ImageButton').style.visibility = "hidden";
        }
    </script>

</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1 {
            width: 100%;
        }

        .MFeildForeColor {
            color: Red;
        }
    </style>
</asp:Content>
