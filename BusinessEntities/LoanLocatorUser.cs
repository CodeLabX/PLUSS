﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class LoanLocatorUser
    {
        public int ID_LEARNER { get; set; }
        public string NAME_LEARNER { get; set; }
        public string USERNAME { get; set; }
        public int LOAN_DEPT_ID { get; set; }
        public int SERV_UNIT_ID { get; set; }
        public int ST_USER_LEVEL { get; set; }
        public int USER_ROLE_ID { get; set; }
        public DateTime logedintime { get; set; }
    }
}
