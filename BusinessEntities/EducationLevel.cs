﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Education level object class.
    /// </summary>
    public class EducationLevel
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public EducationLevel()
        {
            //Contructor logic here.
        }
        public Int32 Id { get; set; }
        public string Name { get; set; }
    }
}
