﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ActionPoints
    {
        public ActionPoints()
        {
        }
        public Int64 Id { get; set; }
        public string ColorName { get; set; }
        public string NewSourcing { get; set; }
        public string IndustryProfitMargin { get; set; }
        public string Deviation { get; set; }
        public string Topup { get; set; }
        public string BureauPolicy { get; set; }
        public int Status { get; set; }
    }
}
