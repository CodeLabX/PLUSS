﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class LoanApplicationDetails
    {
        public LoanApplicationDetails()
        {
            //Remarks = new Remarks { };
        }
        public Int32 LAId { get; set; }
        public string PersonId { get; set; }
        public Int32 DeptId { get; set; }
        public DateTime DateTime { get; set; }
        public Int32 ProcessStatusId { get; set; }
        //public List<Remarks> Remarks { get; set; }
    }
}
