﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class UserType
    {
        public UserType()
        {
            //Constructor logic here.
        }
        public Int32 Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
