﻿using System;

namespace BusinessEntities
{
    public class Salary
    {
        public Salary()
        {
        }
        public int SalaID { get; set; }
        public string SalaLlid { get; set; }
        public string SalaCustomername { get; set; }
        public int? SalaCompanyType { get; set; }
        public double? SalaAmount1 { get; set; }
        public double? SalaAmount2 { get; set; }
        public double? SalaNetincome { get; set; }
        public string SalaVariablesalarymonth1 { get; set; }
        public double? SalaAverage1 { get; set; }
        public double? Sala5Oot1 { get; set; }
        public double? Sala5Oconv1 { get; set; }
        public double? Sala2Oot1 { get; set; }
        public double? Sala2Otshift1 { get; set; }
        public double? Sala2Oothers1 { get; set; }
        public string SalaVariablesalarymonth2 { get; set; }
        public double? SalaAverage2 { get; set; }
        public double? Sala5Oot2 { get; set; }
        public double? Sala5Oconv2 { get; set; }
        public double? Sala2Oot2 { get; set; }
        public double? Sala2Otshift2 { get; set; }
        public double? Sala2Oothers2 { get; set; }
        public string SalaVariablesalarymonth3 { get; set; }
        public double? SalaAverage3 { get; set; }
        public double? Sala5Oot3 { get; set; }
        public double? Sala5Oconv3 { get; set; }
        public double? Sala2Oot3 { get; set; }
        public double? Sala2Otshift3 { get; set; }
        public double? Sala2Oothers3 { get; set; }
        public double? SalaSumofaverage { get; set; }
        public double? Sala5Ootaverage { get; set; }
        public double? Sala5Oconvaerage { get; set; }
        public double? Sala2Ootaverage { get; set; }
        public double? SalaTshiftaverage { get; set; }
        public double? SalaOthersaverage { get; set; }
        public double? SalaAvgotconv { get; set; }
        public double? SalaAvgothervariable { get; set; }
        public string SalaMealallowancemonth1 { get; set; }
        public double? SalaUsdall1 { get; set; }
        public double? SalaBdtapp1 { get; set; }
        public string SalaMealallowancemonth2 { get; set; }
        public double? SalaUsdall2 { get; set; }
        public double? SalaBdtapp2 { get; set; }
        public string SalaMealallowancemonth3 { get; set; }
        public double? SalaUsdall3 { get; set; }
        public double? SalaBdtapp3 { get; set; }
        public double? SalaUsdaverage { get; set; }
        public double? SalaBdtaverage { get; set; }
        public double? SalaTkrate { get; set; }
    }
}
