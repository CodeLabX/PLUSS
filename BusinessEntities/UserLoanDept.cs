﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class UserLoanDept
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int LoanDeptId { get; set; }
    }
}
