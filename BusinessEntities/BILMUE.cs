﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// BilMUE Class Object.
    /// </summary>
    public class BILMUE
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public BILMUE()
        {
            //Constructor logic here.
            OutStanding = new List<double>();
            Facility = new List<string>();
            CashSecurity = new List<double>();
        }
        public int Id { get; set; }
        public string LLId { get; set; }
        public int Mob12 { get; set; }
        public double BilGuardedMue { get; set; }
        public double CreditCard { get; set; }
        public double NetAllowed { get; set; }
        public double ExistingBilOs { get; set; }
        public double FdValue { get; set; }
        public double UnsecuredBil { get; set; }
        public double UnsecuredOther { get; set; }
        public double CreditCard2 { get; set; }
        public double NetUnsecured { get; set; }
        public double Available { get; set; }
        public string Facility1 { get; set; }
        public double Exposure1 { get; set; }
        public double Security1 { get; set; }
        public double Nar1 { get; set; }
        public string Facility2 { get; set; }
        public double Exposure2 { get; set; }
        public double Security2 { get; set; }
        public double Nar2 { get; set; }
        public string Facility3 { get; set; }
        public double Exposure3 { get; set; }
        public double Security3 { get; set; }
        public double Nar3 { get; set; }
        public string Facility4 { get; set; }
        public double Exposure4 { get; set; }
        public double Security4 { get; set; }
        public double Nar4 { get; set; }
        public string Facility5 { get; set; }
        public double Exposure5 { get; set; }
        public double Security5 { get; set; }
        public double Nar5 { get; set; }
        public string Facility6 { get; set; }
        public double Exposure6 { get; set; }
        public double Security6 { get; set; }
        public double Nar6 { get; set; }
        public double PLExposure { get; set; }
        public double PLSecurity { get; set; }
        public double PLNar { get; set; }
        public double CCExposure { get; set; }
        public double CCSecurity { get; set; }
        public double CCNar { get; set; }
        public double OtherBilExposure { get; set; }
        public double OtherBilSecurity { get; set; }
        public double OtherBilNar { get; set; }
        public double OtherPLIPEFLexiExposure { get; set; }
        public double OtherPLIPEFLexiSecurity { get; set; }
        public double OtherPLIPEFLexiNar { get; set; }
        public double OtherCCExposure { get; set; }
        public double OtherCCSecurity { get; set; }
        public double OtherCCNar { get; set; }
        public double NetExposure { get; set; }
        public double NetSecurity { get; set; }
        public double NetNar { get; set; }
        public List<string> Facility { get; set; }
        public List<double> OutStanding { get; set; }
        public List<double> CashSecurity { get; set; }
    }
}
