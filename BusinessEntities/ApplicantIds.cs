﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ApplicantIds
    {
        public ApplicantIds()
        {
            //Constructor logic here.
        }

        public string Type1 { get; set; }
        public string Id1 { get; set; }
        public DateTime ExpiryDate1 { get; set; }

        public string Type2 { get; set; }
        public string Id2 { get; set; }
        public DateTime ExpiryDate2 { get; set; }

        public string Type3 { get; set; }
        public string Id3 { get; set; }
        public DateTime ExpiryDate3 { get; set; }

        public string Type4 { get; set; }
        public string Id4 { get; set; }
        public DateTime ExpiryDate4 { get; set; }

        public string Type5 { get; set; }
        public string Id5 { get; set; }
        public DateTime ExpiryDate5 { get; set; }

    }
}
