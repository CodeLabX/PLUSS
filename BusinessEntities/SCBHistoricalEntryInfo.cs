﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class SCBHistoricalEntryInfo
    {
        public SCBHistoricalEntryInfo()
        {
        }
        public Int64 SCBDetailsId { get; set; }
        public string SCBAcNo { get; set; }
        public string SCBAcName { get; set; }
        public DateTime SCBPeriod { get; set; }
        public double SCBAverageledgerbalance { get; set; }
        public int SCBAdjmontaveragebalance { get; set; }
        public double SCBAdjustmentcto { get; set; }
        public int SCBAdjmonthcto { get; set; }
    }

}
