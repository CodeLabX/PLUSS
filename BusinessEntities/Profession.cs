﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Profession object class.
    /// </summary>
    public class Profession
    {
        public Profession() 
        {
            //Constructor logic here.
        }
        /// <summary>
        /// Property
        /// </summary>
        public Int32 Id { get; set; }
        public string Name { get; set; }
    }
}
