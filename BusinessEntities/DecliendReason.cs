﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class DecliendReason
    {
        public int REAS_ID { get; set; }
        public int PROD_ID { get; set; }
        public int DTYPE { get; set; }
        public string REAS_NAME { get; set; }
        public int REAS_NO { get; set; }
    }
}
