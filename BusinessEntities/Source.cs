﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Source object class
    /// </summary>
    public class Source
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Source()
        {
            //Constructor logic here.
        }
        /// <summary>
        /// Property
        /// </summary>
        #region
        public Int32 SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceRegionName { get; set; }
        public int UserId { get; set; }
        #endregion
    }
}
