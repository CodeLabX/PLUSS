﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Customer object class.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public Customer()
        {
            //Constructor logic here.
        }
        public int Id { get; set; }
        public string LLId { get; set; }
        public string Name { get; set; }
        public string MasterNo { get; set; }
        public string PDC { get; set; }
        public string PDCAccount { get; set; }
        public int CCPLBundleOffer { get; set; }
        public int FirstPayment { get; set; }
        public string OtherStatementBank1 { get; set; }
        public string OtherStatementAcc1 {get;set;}
        public string OtherStatementBank2 { get; set; }
        public string OtherStatementAcc2 {get;set;}
        public string OtherStatementBank3 { get; set; }
        public string OtherStatementAcc3 {get;set;}
        public string OtherStatementBank4 { get; set; }
        public string OtherStatementAcc4 {get;set;}
        public string OtherStatementBank5 { get; set; }
        public string OtherStatementAcc5 {get;set;}
        public string Facility { get; set; }
        public string Flag { get; set; }
        public Int32 TopUpCount { get; set; }
        public double TopUpAmount1 { get; set; }
        public double OutStanding { get; set; }
        public DateTime TopUpDate1 { get; set; }
        public double TopUpAmount2 { get; set; }
        public DateTime TopUpDate2 { get; set; }
        public double TopUpAmount3 { get; set; }
        public DateTime TopUpDate3 { get; set; }
        public int OtherDirLoan { get; set; }
        public int Mob12 { get; set; }
        public int LoanStatusId { get; set; }
    }
}
