﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Vechile object class.
    /// </summary>
    public class Vechile
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Vechile()        
        { 
            //Constructor logic here.
        }
        /// <summary>
        /// Property
        /// </summary>
        public Int32? VechileId { get; set; }
        public string VechileType { get; set; }
    }
}
