﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class CIBResult
    {
        public CIBResult()
        { 
        }
        public int ID { get; set; }
        public string LLID { get; set; }
        public string CustomerName { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string ReferenceNumber { get; set; }
        public string Type { get; set; }
        public DateTime EntryDate { get; set; }
        public Int32 UserId { get; set; }

    }
}
