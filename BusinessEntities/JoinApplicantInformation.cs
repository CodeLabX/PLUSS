﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Joint applicant object class.
    /// </summary>
    public class JoinApplicantInformation
    {
        /// <summary>
        /// Contstructor
        /// </summary>
        public JoinApplicantInformation()
        {
            //Constructor logic here.
        }
        public int LlId { get; set; }
        #region Join Applicant1 Property
        /// <summary>
        /// Class property Joint Applicant 1
        /// </summary>
        public Int64? JointApplicantLLId { get; set; }
        public string JointApp1Name { get; set; }
        public string J1FatherName { get; set; }
        public string J1MotherName { get; set; }
        public DateTime J1DOB { get; set; }
        //Join SCB account.
        public Int64? J1SCBAccNo1 { get; set; }
        public Int64? J1SCBAccNo2 { get; set; }
        public Int64? J1SCBAccNo3 { get; set; }
        public Int64? J1SCBAccNo4 { get; set; }
        public Int64? J1SCBAccNo5 { get; set; }

        public string J1TinNo { get; set; }
       //Joint credit card no.
        public string J1CrdtCrdNo1 { get; set; }
        public string J1CrdtCrdNo2 { get; set; }
        public string J1CrdtCrdNo3 { get; set; }
        public string J1CrdtCrdNo4 { get; set; }
        public string J1CrdtCrdNo5 { get; set; }

        public string JApp1EducationLevel { get; set; }
        //Joint applicant contact no.
        public string JApp1ContactNo1 { get; set; }
        public string JApp1ContactNo2 { get; set; }
        public string JApp1ContactNo3 { get; set; }
        public string JApp1ContactNo4 { get; set; }
        public string JApp1ContactNo5 { get; set; }
        public string JApp1Company { get; set; }
        //Joint applicant id.
        public string JApp1IdType1 { get; set; }
        public string JApp1IdType2 { get; set; }
        public string JApp1IdType3 { get; set; }
        public string JApp1IdType4 { get; set; }
        public string JApp1IdType5 { get; set; }
        public string JApp1Id1 { get; set; }
        public string JApp1Id2 { get; set; }
        public string JApp1Id3 { get; set; }
        public string JApp1Id4 { get; set; }
        public string JApp1Id5 { get; set; }
        public DateTime JApp1IdExpire1 { get; set; }
        public DateTime JApp1IdExpire2 { get; set; }
        public DateTime JApp1IdExpire3 { get; set; }
        public DateTime JApp1IdExpire4 { get; set; }
        public DateTime JApp1IdExpire5 { get; set; }
        #endregion

        #region Join Applicant2 Property
        /// <summary>
        /// Class property Joint Applicant 2
        /// </summary>
        public string JointApp2Name { get; set; }
        public string J2FatherName { get; set; }
        public string J2MotherName { get; set; }
        public DateTime J2DOB { get; set; }
        //Join SCB account.
        public Int64? J2SCBAccNo1 { get; set; }
        public Int64? J2SCBAccNo2 { get; set; }
        public Int64? J2SCBAccNo3 { get; set; }
        public Int64? J2SCBAccNo4 { get; set; }
        public Int64? J2SCBAccNo5 { get; set; }

        public string J2TinNo { get; set; }
        //Joint credit card no.
        public string J2CrdtCrdNo1 { get; set; }
        public string J2CrdtCrdNo2 { get; set; }
        public string J2CrdtCrdNo3 { get; set; }
        public string J2CrdtCrdNo4 { get; set; }
        public string J2CrdtCrdNo5 { get; set; }

        public string JApp2EducationLevel { get; set; }
        //Joint applicant contact no.
        public string JApp2ContactNo1 { get; set; }
        public string JApp2ContactNo2 { get; set; }
        public string JApp2ContactNo3 { get; set; }
        public string JApp2ContactNo4 { get; set; }
        public string JApp2ContactNo5 { get; set; }
        public string JApp2Company { get; set; }
        //Joint applicant id.
        public string JApp2IdType1 { get; set; }
        public string JApp2IdType2 { get; set; }
        public string JApp2IdType3 { get; set; }
        public string JApp2IdType4 { get; set; }
        public string JApp2IdType5 { get; set; }
        public string JApp2Id1 { get; set; }
        public string JApp2Id2 { get; set; }
        public string JApp2Id3 { get; set; }
        public string JApp2Id4 { get; set; }
        public string JApp2Id5 { get; set; }
        public DateTime JApp2IdExpire1 { get; set; }
        public DateTime JApp2IdExpire2 { get; set; }
        public DateTime JApp2IdExpire3 { get; set; }
        public DateTime JApp2IdExpire4 { get; set; }
        public DateTime JApp2IdExpire5 { get; set; }
        #endregion

        #region Join Applicant3 Property
        /// <summary>
        /// Class property Joint Applicant 3
        /// </summary>
        public string JointApp3Name { get; set; }
        public string J3FatherName { get; set; }
        public string J3MotherName { get; set; }
        public DateTime J3DOB { get; set; }
        //Join SCB account.
        public Int64? J3SCBAccNo1 { get; set; }
        public Int64? J3SCBAccNo2 { get; set; }
        public Int64? J3SCBAccNo3 { get; set; }
        public Int64? J3SCBAccNo4 { get; set; }
        public Int64? J3SCBAccNo5 { get; set; }

        public string J3TinNo { get; set; }
        //Joint credit card no.
        public string J3CrdtCrdNo1 { get; set; }
        public string J3CrdtCrdNo2 { get; set; }
        public string J3CrdtCrdNo3 { get; set; }
        public string J3CrdtCrdNo4 { get; set; }
        public string J3CrdtCrdNo5 { get; set; }

        public string JApp3EducationLevel { get; set; }
        //Joint applicant contact no.
        public string JApp3ContactNo1 { get; set; }
        public string JApp3ContactNo2 { get; set; }
        public string JApp3ContactNo3 { get; set; }
        public string JApp3ContactNo4 { get; set; }
        public string JApp3ContactNo5 { get; set; }
        public string JApp3Company { get; set; }
        //Joint applicant id.
        public string JApp3IdType1 { get; set; }
        public string JApp3IdType2 { get; set; }
        public string JApp3IdType3 { get; set; }
        public string JApp3IdType4 { get; set; }
        public string JApp3IdType5 { get; set; }
        public string JApp3Id1 { get; set; }
        public string JApp3Id2 { get; set; }
        public string JApp3Id3 { get; set; }
        public string JApp3Id4 { get; set; }
        public string JApp3Id5 { get; set; }
        public DateTime JApp3IdExpire1 { get; set; }
        public DateTime JApp3IdExpire2 { get; set; }
        public DateTime JApp3IdExpire3 { get; set; }
        public DateTime JApp3IdExpire4 { get; set; }
        public DateTime JApp3IdExpire5 { get; set; }
        #endregion

        #region Join Applicant4 Property
        /// <summary>
        /// Class property Joint Applicant 4
        /// </summary>
        public string JointApp4Name { get; set; }
        public string J4FatherName { get; set; }
        public string J4MotherName { get; set; }
        public DateTime J4DOB { get; set; }
        //Join SCB account.
        public Int64? J4SCBAccNo1 { get; set; }
        public Int64? J4SCBAccNo2 { get; set; }
        public Int64? J4SCBAccNo3 { get; set; }
        public Int64? J4SCBAccNo4 { get; set; }
        public Int64? J4SCBAccNo5 { get; set; }

        public string J4TinNo { get; set; }
        //Joint credit card no.
        public string J4CrdtCrdNo1 { get; set; }
        public string J4CrdtCrdNo2 { get; set; }
        public string J4CrdtCrdNo3 { get; set; }
        public string J4CrdtCrdNo4 { get; set; }
        public string J4CrdtCrdNo5 { get; set; }

        public string JApp4EducationLevel { get; set; }
        //Joint applicant contact no.
        public string JApp4ContactNo1 { get; set; }
        public string JApp4ContactNo2 { get; set; }
        public string JApp4ContactNo3 { get; set; }
        public string JApp4ContactNo4 { get; set; }
        public string JApp4ContactNo5 { get; set; }
        public string JApp4Company { get; set; }
        //Joint applicant id.
        public string JApp4IdType1 { get; set; }
        public string JApp4IdType2 { get; set; }
        public string JApp4IdType3 { get; set; }
        public string JApp4IdType4 { get; set; }
        public string JApp4IdType5 { get; set; }
        public string JApp4Id1 { get; set; }
        public string JApp4Id2 { get; set; }
        public string JApp4Id3 { get; set; }
        public string JApp4Id4 { get; set; }
        public string JApp4Id5 { get; set; }
        public DateTime JApp4IdExpire1 { get; set; }
        public DateTime JApp4IdExpire2 { get; set; }
        public DateTime JApp4IdExpire3 { get; set; }
        public DateTime JApp4IdExpire4 { get; set; }
        public DateTime JApp4IdExpire5 { get; set; }
        #endregion
    }
}
