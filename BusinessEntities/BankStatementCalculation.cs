﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BankStatementCalculation
    {
        public BankStatementCalculation()
        {
        }
        public Int64 Id { get; set; }
        public Int64 LlId { get; set; }
        public double Total12MonthCreditTurnover { get; set; }
        public double Total12MonthAverageBalance { get; set; }
        public double Average12MonthCreditTurnover { get; set; }
        public double Average12MonthAverageBalance { get; set; }
        public Int16 Month12MaxBalanceDate { get; set; }
        public double Month1250PerAverageBalance { get; set; }
        public double Total6MonthCreditTurnover { get; set; }
        public double Total6MonthAverageBalance { get; set; }
        public double Average6MonthCreditTurnover { get; set; }
        public double Average6MonthAverageBalance { get; set; }
        public Int16 Month6MaxBalanceDate { get; set; }
        public double Month650PerAverageBalance { get; set; }
        public string XmlData { get; set; }
        public string Remarks { get; set; }
        public int CreditPart { get; set; }
        public int AveragePart { get; set; }
        public DateTime EntryDate { get; set; }
        public Int64 UserId { get; set; }

    }
}
