﻿
using System;
namespace BusinessEntities
{
    public class Deviation
    {
        public int DeviID { get; set; }
        public int? DeviPlID { get; set; }
        public string DeviLlid { get; set; }
        public int? DeviLevel1 { get; set; }
        public int? DeviDescription1ID { get; set; }
        public string DeviCode1 { get; set; }
        public int? DeviLevel2 { get; set; }
        public int? DeviDescription2ID { get; set; }
        public string DeviCode2 { get; set; }
        public int? DeviLevel3 { get; set; }
        public int? DeviDescription3ID { get; set; }
        public string DeviCode3 { get; set; }
        public int? DeviLevel4 { get; set; }
        public int? DeviDescription4ID { get; set; }
        public string DeviCode4 { get; set; }
        public string DeviRemarkscondition1ID { get; set; }
        public int? DeviRemarkscondition2ID { get; set; }
        public int? DeviRemarkscondition3ID { get; set; }
        public int? DeviRemarkscondition4ID { get; set; }
        public int? DeviRelationshipwith1 { get; set; }
        public int? DeviRelationshipwith2 { get; set; }
        public DateTime DeviSince { get; set; }
        public int? DeviResidentialstatus { get; set; }
        public int? DeviCreditcardrepayment { get; set; }
        public string DeviLimit { get; set; }
        public DateTime DeviAvailedsince { get; set; }
        public int? DeviPreviousrepaymentId1 { get; set; }
        public int? DeviLoantypeId1 { get; set; }
        public DateTime DeviAvailedsince1 { get; set; }
        public int? DeviPreviousrepaymentId2 { get; set; }
        public int? DeviLoantypeId2 { get; set; }
        public DateTime DeviAvailedsince2 { get; set; }
        public string DeviOthersID { get; set; }
        public int? DeviRepaymentnotsatisfactory { get; set; }
        public int? DeviLoantypeId3 { get; set; }
        public int? DeviDpdID { get; set; }
        public int? DeviCashsalaried { get; set; }
        public int? DeviFrequentchangereturn { get; set; }
        public string DeviOthers { get; set; }
        public Int64 UserId { get; set; }
        public DateTime EntryDate { get; set; }

    }
}
