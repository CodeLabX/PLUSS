﻿using System;

namespace BusinessEntities
{
    public class UserInfoHistory
    {
        public UserInfoHistory()
        {

        }
        public Int32 UserHistoryId { get; set; }
        public string UserLogInId { get; set; }
        public string PassWord { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}
