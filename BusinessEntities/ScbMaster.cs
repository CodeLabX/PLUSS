﻿using System;

namespace BusinessEntities
{
    public class ScbMaster
    {
        public ScbMaster()
        {
        }

        public Int64 scbmID { get; set; }
        public string scbmLlid { get; set; }
        public string scbmAcno { get; set; }
        public string scbmAcname { get; set; }
        public double scbmLedgertotal { get; set; }
        public double scbmLedgeraverage { get; set; }
        public double scbmLedger5oper { get; set; }
        public double scbmLedger6monthtotal { get; set; }
        public double scbmLedger6monthaverage { get; set; }
        public double scbmLedger6month5oper { get; set; }
        public double scbmAdjustedctototal { get; set; }
        public double scbmAdjustedctoaverage { get; set; }
        public double scbmAdjustedcto6monthtotal { get; set; }
        public double scbmAdjustedcto6monthaverage { get; set; }
        public DateTime scbmEntrydate { get; set; }
        public int? scbmUserID { get; set; }
    }
}
