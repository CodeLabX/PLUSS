﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class Approver
    {
        public Approver()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string EmailAddress { get; set; }

        public int UserAppID { get; set; }
        public int UserID { get; set; }
        public string ProdTag { get; set; }
        public int Level1Amt { get; set; }
        public int Level2Amt { get; set; }
        public int Level3Amt { get; set; }
        public DateTime ChangDt { get; set; }
        public int ChangBy { get; set; }

        public string UserName { get; set; }
        public int Status { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        //public string ProdTagId { get; set; }
    }
}
