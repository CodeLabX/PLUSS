﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BankNameList
    {
        public BankNameList()
        {
        }
        public Int32? MasterId { get; set; }
        public Int32? OtherBankId { get; set; }
        public string OtherBank { get; set; }
        public Int32? OtherBranchId { get; set; }
        public string OtherBranch { get; set; }
        public string OtherAccType { get; set; }
        public string OtherAccNo { get; set; }
        public string OtherStatus { get; set; }
        public string OtherAcName { get; set; }
        public string StartMonth { get; set; }
        public string EndMonth { get; set; }
        public int ApplicationStatus { get; set; }
    }
}
