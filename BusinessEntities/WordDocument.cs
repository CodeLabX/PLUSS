﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class WordDocument
    {
        public WordDocument()
        {
        }
        public int Id { get; set; }
        public string To { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string Body { get; set; }
        public string Body1 { get; set; }
        public string Body2 { get; set; }
        public string Body3 { get; set; }
        public string Body4 { get; set; }
        public string Body5 { get; set; }
        public string Body6 { get; set; }
    }
}
