﻿using System;

namespace BusinessEntities
{
    public class ScbDetails
    {
        public ScbDetails()
        {
        }

        public Int64 scbdID { get; set; }
        public Int64 scbdScbmasterID { get; set; }
        public DateTime? scbdPeriod { get; set; }
        public double? scbdMonthendledgerbalance { get; set; }
        public double? scbdMaxledgerbalance { get; set; }
        public double? scbdMinledgerbalance { get; set; }
        public double? scbdAverageledgerbalance { get; set; }
        public int? scbdAdjmontaveragebalance { get; set; }
        public double? scbdCredittransactioncount { get; set; }
        public double? scbdTotaltransactioncount { get; set; }
        public double? scbdDebitturnover { get; set; }
        public double? scbdCreditturnover { get; set; }
        public double? scbdAdjustment { get; set; }
        public double? scbdAdjustmentcto { get; set; }
        public int? scbdAdjmonthcto { get; set; }
    }
}
