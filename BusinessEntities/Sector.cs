﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class Sector
    {
        public Sector()
        {
        }
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
