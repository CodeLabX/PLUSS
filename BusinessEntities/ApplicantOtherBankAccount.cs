﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ApplicantOtherBankAccount
    {
        public ApplicantOtherBankAccount()
        {
            //Constructor logic here.
        }
        public int? OtherBankId1 { get; set; }
        public string OtherBank1 { get; set; }
        public int? OtherBranchId1 { get; set; }
        public string OtherBranch1 { get; set; }
        public string OtherAccType1 { get; set; }
        public string OtherAccNo1 { get; set; }
        public string OtherStatus1 { get; set; }

        public int? OtherBankId2 { get; set; }
        public string OtherBank2 { get; set; }
        public int? OtherBranchId2 { get; set; }
        public string OtherBranch2 { get; set; }
        public string OtherAccType2 { get; set; }
        public string OtherAccNo2 { get; set; }
        public string OtherStatus2 { get; set; }

        public int? OtherBankId3 { get; set; }
        public string OtherBank3 { get; set; }
        public int? OtherBranchId3 { get; set; }
        public string OtherBranch3 { get; set; }
        public string OtherAccType3 { get; set; }
        public string OtherAccNo3 { get; set; }
        public string OtherStatus3 { get; set; }

        public int? OtherBankId4 { get; set; }
        public string OtherBank4 { get; set; }
        public int? OtherBranchId4 { get; set; }
        public string OtherBranch4 { get; set; }
        public string OtherAccType4 { get; set; }
        public string OtherAccNo4 { get; set; }
        public string OtherStatus4 { get; set; }

        public int? OtherBankId5 { get; set; }
        public string OtherBank5 { get; set; }
        public int? OtherBranchId5 { get; set; }
        public string OtherBranch5 { get; set; }
        public string OtherAccType5 { get; set; }
        public string OtherAccNo5 { get; set; }
        public string OtherStatus5 { get; set; }
        public int ApplicationStatus { get; set; }
    }
}
