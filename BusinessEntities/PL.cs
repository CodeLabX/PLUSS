﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class PL
    {
        public PL()
        {
        }
        public Int64 PlId {get; set; }
        public Int64 PlLlid {get; set; }
        public int PlProductId { get; set; }
        public int PlSegmentId {get; set; }
        public int PlLoantypeId {get; set; }
        public int PlIncomeacessmentmethodId {get; set; }
        public int PlSectorId {get; set; }
        public int PlSubsectorId {get; set; }
        public int PlCompanyId {get; set; }
        public int PlCompanycatagoryId {get; set; }
        public int PlCompanytypeId {get; set; }
        public int PlDocatagorizedId {get; set; }
        public int PlCompanysegmentId {get; set; }
        public string PlDoctorCategory { get; set; }
        public double PlIncome {get; set; }
        public double PlOwnership {get; set; }
        public double PlProfitmargin {get; set; }
        public int PlOtherincomesourceId {get; set; }
        public double PlOtherincome {get; set; }
        public double PlNetincome {get; set; }
        public double PlDeclaredincome {get; set; }
        public double PlScbcreditcardlimit {get; set; }
        public string PlIndustryprofitmarginrat {get; set; }
        public double PlPliplfloutstanding {get; set; }
        public double PlMonthlyinterest {get; set; }
        public int PlResiedentialstatusId {get; set; }
        public double PlFlsecuityos {get; set; }
        public double PlAveragebalance {get; set; }
        public int PlEmiwithscbsourceId {get; set; }
        public double PlEmiwithscb {get; set; }
        public string PlOtheremisource {get; set; }
        public double PlOtheremi {get; set; }
        public double PlInstabuybalance {get; set; }
        public double PlInstabuyinstallment {get; set; }
        public double PlBbpddmaxloanamound {get; set; }
        public int PlDbrmultiplier {get; set; }
        public int PlDbrlevelId {get; set; }
        public double PlDbrmaxloanamount {get; set; }
        public double PlAvgbalancmaxloanamount {get; set; }
        public double PlMuemultiplier {get; set; }
        public double PlMuemaxloanamount {get; set; }
        public int PlAddOneId { get; set; }
        public double PlInterestrat {get; set; }
        public int PlTenure {get; set; }
        public int PlFirstpayment {get; set; }
        public int PlSafetyplusId {get; set; }
        public double PlAppliedloanamount {get; set; }
        public double PlAppropriatedloanamount {get; set; }
        public double PlProposedloanamount {get; set; }
        public string PlLevel { get; set; }
        public double PlEmi {get; set; }
        public double PlEmifactor {get; set; }
        public double PlDbr {get; set; }
        public double PlMue {get; set; }

        public double PlBancaLoanAmount{get; set;}                              
        public double PlBanca {get;set;}                                        
        public double PlBancaDBR {get;set;}
        public double PlBancaMUE { get; set; }                           
        public double PlBancaWithoutDBR {get;set;}                              
        public string PlCurrentAge {get;set;}                            
        public string PlSecurity  {get;set;}  
                              
        public string PlRemarks { get; set; }
        public Int64 UserId { get; set; }
        public DateTime EntryDate { get; set; }
        public int Status { get; set; }
    }
}
