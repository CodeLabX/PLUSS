﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class Department
    {
        public Department()
        {
            
        }
        public int LOAN_DEPT_ID { get; set; }
        public string LOAN_DEPT_NAME { get; set; }
        public int LOAN_DEPT_TYPE { get; set; }
    }
}
