﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BASVERReportInfo
    {
        public BASVERReportInfo()
        {
        }
        public Int64 BasVerDetailsId { get; set; }
        public Int64 BasVerMasterId { get; set; }
        public Int64 LlId { get; set; }
        public Int16 StatementType_Id { get; set; }
        public Int16 Bank_Id { get; set; }
        public string BankName { get; set; }
        public Int16 Branch_Id { get; set; }
        public string BranchName { get; set; }
        public string BranchAddress { get; set; }
        public string LocationName { get; set; }
        public Int16 Subject_Id { get; set; }
        public string SubjectName { get; set; }
        public string Dear { get; set; }
        public string EmailAddress { get; set; }
        public string SCBEmailAddress { get; set; }
        public string EmailText { get; set; }
        public string CustomerName { get; set; }
        public string BusinessName { get; set; }
        public string CustomerAddress { get; set; }
        public string AcNo { get; set; }
        public Int16 AcType_Id { get; set; }
        public string AcName { get; set; }
        public Int32 Approved_Id { get; set; }
        public string ApproverName { get; set; }
        public string ApproverDesignation { get; set; }
        public Int32 User_Id { get; set; }
        public DateTime EntryDate { get; set; }
        public Int16 SlNo { get; set; }
        public string Particular { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public double Withdrawl { get; set; }
        public double Diposit { get; set; }
        public double Balance { get; set; }
    }
}
