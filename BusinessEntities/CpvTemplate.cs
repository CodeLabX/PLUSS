﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    public class CpvTemplate
    {
        public CpvTemplate()
        { }

        public int CpvtID { get; set; }
        public string CpvtLlid { get; set; }
        public DateTime CpvtDate { get; set; }
        public string CpvtGuarantorsname { get; set; }
        public string CpvtContactaddress { get; set; }
        public string CpvtTelephoneno { get; set; }
        public string CpvtCellphone { get; set; }
        public string CpvtGuarantoreeamount { get; set; }
        public string CpvtRelationshipwithapplicant { get; set; }
        public string CpvtEmailaddress { get; set; }
        public DateTime CpvtEntrydate { get; set; }
        public int? CpvtUserID { get; set; }
        public string CpvtDesignation { get; set; }
    }
}
