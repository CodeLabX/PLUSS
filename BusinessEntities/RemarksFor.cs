﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class RemarksFor
    {
        public RemarksFor()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
