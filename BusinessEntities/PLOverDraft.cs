﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class PLOverDraft
    {
        public PLOverDraft()
        {
        }
        public Int32 Id {get;set;}
        public Int64 PLId {get; set; }
        public Int64 LlId {get; set; }
        public double Limit {get; set; }
        public double Rate {get; set; }
        public double Q1 {get; set; }
        public double Q2 {get; set; }
        public double Q3 {get; set; }
        public double Q4 {get; set; }
        public Int32 OldId { get; set; }
    }
}
