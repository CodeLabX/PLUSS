﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BSVERMaster
    {
        public BSVERMaster()
        {
        }
         public Int64 Id {get ;set;}         
         public Int64 LlId {get ;set;}              
         public Int16 StatementType_Id {get ;set;}       
         public Int16 Bank_Id {get ;set;}                
         public Int16 Branch_Id {get ;set;}              
         public Int16 Subject_Id {get ;set;}
         public string Dear { get; set; }
         public string EmailAddress { get; set; }
         public string SCBEmailAddress { get; set; }
         public string EmailText { get; set; }
         public string CustomerName { get; set; }
         public string CustomerAddress { get; set; }                       
         public string AcNo {get ;set;}              
         public Int16 AcType_Id {get ;set;}              
         public string AcName {get ;set;}           
         public Int32 Approved_Id {get ;set;}        
         public Int32 User_Id {get ;set;}
         public DateTime EntryDate { get; set; }        
    }
}
