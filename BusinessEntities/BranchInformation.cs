﻿using System;

namespace BusinessEntities
{
    public class BranchInformation
    {
        public BranchInformation()
        {
            Bank = new BankInformation();
            Location = new LocationInformation();
        }

        public Int32 Id { get; set; }
        public Int32 BankId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string OnlineStatus { get; set; }
        public int Status { get; set; }
        public string StatusMsg { get; set; }
        public int ClearingStatus { get; set; }
        public string ClearingStatusMsg { get; set; }
        public BankInformation Bank { get; set; }        
        public LocationInformation Location { get; set; }
        public short InsertOrUploadFlag { get; set; }
    }
}
