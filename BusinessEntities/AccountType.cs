﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Account Type object class.
    /// </summary>
    public class AccountType
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public AccountType()
        {
            //Constructor logic here.
        }
        /// <summary>
        /// Properts
        /// </summary>
        public Int32 Id { get; set; }
        public string Name { get; set; }
    }
}
