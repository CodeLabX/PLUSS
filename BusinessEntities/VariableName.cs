﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Variable name object class.
    /// </summary>
    public class VariableName
    {
        public VariableName()
        {
            //Constructor logic here.
        }
        public Int32 Id { get; set; }
        public string Name { get; set; }
    }
}
