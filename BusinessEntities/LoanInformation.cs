﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Loan information object class.
    /// </summary>
    public class LoanInformation
    {    
        /// <summary>
        /// Constructor.
        /// </summary>
        public LoanInformation()
        {
            //Constructor logic here.            
            ContactNumber = new ApplicantContactNo();
            ApplicantId = new ApplicantIds();
            AccountNo = new ApplicantAccountNo();
            OtherBankAccount = new ApplicantOtherBankAccount();
            LoanLocatorLoanApplicant=new LoanLocatorLoanApplicant();
        }
        /// <summary>
        /// Class property.
        /// </summary>
        public Int32? LLId { get; set; }
        public string ApplicantName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Business { get; set; }
        public DateTime DOB { get; set; }
        public string TINNo { get; set; }
        public ApplicantContactNo ContactNumber { get; set; }
    
        public ApplicantIds ApplicantId { get; set; }
        public ApplicantAccountNo AccountNo { get; set; }
        public string AccNo { get; set; }
        public string AccType { get; set; }
        public int ProfessionId { get; set; }
        public string Organization  { get; set; }
        public ApplicantOtherBankAccount OtherBankAccount { get; set; }

        //
        public string StateName { get; set; }
        public Int32? ProductId { get; set; }
        public string ProductName { get; set; }
        public Int32? SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourcePersonName { get; set; }
        public string ARMCode { get; set; }
        public DateTime RecieveDate { get; set; }
        public DateTime AppliedDate { get; set; }
        public decimal AppliedAmount { get; set; }
        public decimal DeclaredAmount { get; set; }
        public string CreditCardNo1 { get; set; }
        public string CreditCardNo2 { get; set; }
        public string CreditCardNo3 { get; set; }
        public string CreditCardNo4 { get; set; }
        public string CreditCardNo5 { get; set; }
        public string EducationalLevel { get; set; }
        public string MaritalStatus { get; set; }
        public string ResidentialStatus { get; set; }
        public string ResidentAddress { get; set; }
        public string Profession { get; set; }
        public string OfficeAddress { get; set; }
        public int JobDuration { get; set; }
        public string SpousOcupation { get; set; }
        public Int32 VechileId { get; set; }
        public string VechileType{ get; set; }

        public string ExtraField1 { get; set; }
        public string ExtraField2 { get; set; }
        public string ExtraField3 { get; set; }
        public string ExtraField4 { get; set; }
        public string ExtraField5 { get; set; }
        public string ExtraField6 { get; set; }
        public string ExtraField7 { get; set; }
        public string ExtraField8 { get; set; }
        public string ExtraField9 { get; set; }
        public string ExtraField10 { get; set; }
        public int ForwardOrDesferStatus { get; set; }
        public Int32 EntryUserId{ get; set; }
        public LoanLocatorLoanApplicant LoanLocatorLoanApplicant { get; set; }

        public string SubmitedBy { get; set; }
        public string DepartmentName { get; set; }
        public decimal ApprovedAmount { get; set; }
        public string DecisionStatus { get; set; }
        public string LoanAccNo { get; set; }
        public int PersonId { get; set; }
        public int DecisionStatusId { get; set; }
        public int DeliDeptId { get; set; }
        public int DeptType { get; set; }
        public int Maker { get; set; }
        public int PreDeclined { get; set; }
        public List<CpvTemplate> CpvTemplates { get; set; }
        public int LoanStatusId { get; set; }

        public string Owner { get; set; }
        public int Tenor { get; set; }
        public string UserDefine { get; set; }
        public string UserDefine2 { get; set; }
        public string UserDefine3 { get; set; }
        public string UserDefine4 { get; set; }
        public int AccTypeId { get; set; }
        public string REMAR { get; set; }
        public string DECLI_REASO { get; set; }
        public string MultiLocatorRemarksID { get; set; }        

    }
}
