﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class User
    {
        public int LOAN_DEPT_ID { get; set; }
        public string LOAN_DEPT_NAME { get; set; }

        public User()
        {
            //Constructor logic here.
            UserType = new UserType();

        }
        public Int32 Id { get; set; }
        public string UserId { get; set; }
        public string LoginId { get; set; }
        //for Audit Trial//
        public int UsersId { get; set; }
        //for Audit Trial//
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Password { get; set; }
        public short Type { get; set; }
        public string UserLevelCode { get; set; }
        public bool IsExists { get; set; }
        public short ErrorMsgType { get; set; }
        public short Status { get; set; }
        public string StatusMsg { get; set; }
        public string Email { get; set; }
        public DateTime EntryDate { get; set; }
        public Int32 CreateUserId { get; set; }
        public UserType UserType { get; set; }
        public string UserTypeName { get; set; }
        public Int32 userLoginStatus { get; set; }
        public Int32 AutoUserLevel_Id { get; set; }
        public string AutoUserLevel_Name { get; set; }

        public DateTime LastLoginDateTime { get; set; }

        public Int32 []GroupID { get; set; }

        public Int32 Is_Deleted { get; set; }
        public string IsDeleteMsg { get; set; }

        public int ST_USER_LEVEL { get; set; }
        public Department Department { get; set; }
        public int IsAccessBoth { get; set; }

        public string  MakerId { get; set; }
        public DateTime MakeDate { get; set; }
        public string CheckerId { get; set; }
        public DateTime CheckDate { get; set; }
        public int Module { get; set; }
        public int IsApproved { get; set; }


        public string StaffId { get; set; }
        public int UserLevel { get; set; }
        public int RoleId { get; set; }
        public string RoleCode { get; set; }
    }
}
