﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessEntities
{
    public class Loan
    {
        public int LoanId { get; set; }
        public DateTime SubmissionDate { get; set; }
        public int ProductId { get; set; }
        public string PreAccNo { get; set; }
        public string MidAccNo { get; set; }
        public string PostAccNo { get; set; }
        public string CustomerName { get; set; }
        public int AppliedAmount { get; set; }
        /// <summary>
        /// Learner PERS ID
        /// </summary>
        public int SubmitedBy { get; set; }
        public string LoanSourceCode { get; set; }
        public string ARMCode { get; set; }
        public int LoanState { get; set; }
        public string StateName { get; set; }
        public int EntryBranch { get; set; }

        public string PreDeclined { get; set; }
        public int ForCrd { get; set; }
        public DateTime ApplicationDate { get; set; }
        public bool Cep { get; set; }
        public bool Prio { get; set; }
        public int ApplicantProfession { get; set; }
        public string EmployeeType { get; set; }
        public string Organization { get; set; }
        public string AutoDelarName { get; set; }
        public string AccountNo { get; set; }
        public List<int> CheckList { get; set; }
        public int AccType { get; set; }
        public string MakerID { get; set; }
    }
}
