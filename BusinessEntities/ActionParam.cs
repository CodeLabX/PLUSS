﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ActionParam
    {
        public int Learner_Pers;
        public int job;
        public int lst;
        public int backfrom;
        public int loan_status_type;
        public string loanBtn { get; set; }
        public string crdBtn { get; set; }
        public string toSrcBtn { get; set; }
        public int status_update { get; set; }
        public int prodId { get; set; }
        public int deci { get; set; }
        public int sourceId { get; set; }
        public int cond { get; set; }
        public int appId { get; set; }
        public int oriId { get; set; }
        public int type { get; set; }
        public int deptType { get; set; }
        public int personId { get; set; }
        public int maker { get; set; }
        public string makerID { get; set; }
        public decimal amount { get; set; }
        public int JobState { get; set; }
        public int LoanStateId { get; set; }
        public string LoanStateName { get; set; }
        public string AccountNumber { get; set; }
        public int Backfrom { get; set; }
        public int DeliDeptId { get; set; }
        public int PersId { get; set; }
        public int Op { get; set; }
        public string Button { get; set; }
        public string CustomerName { get; set; }
        public int DeliRoleId { get; set; }

    }
}
