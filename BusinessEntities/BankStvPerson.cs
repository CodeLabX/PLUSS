﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    public class BankStvPerson
    {
        public BankStvPerson()
        {
        }
        public Int64 Id {get; set;}                                              
        public Int16 Bank_Id {get; set;}                                                    
        public Int16 Branch_Id {get; set;}                                                   
        public string Name {get; set;}                                                   
        public string EmailAddress {get; set;}
        public int Status { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }                                                   
    }
}
