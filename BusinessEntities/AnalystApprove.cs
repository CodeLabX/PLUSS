﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class AnalystApprove
    {
        public AnalystApprove()
        {
            PL = new PL();
            LoanInformation = new LoanInformation();
            Product = new Product();
            Segment = new Segment();
            LoanScorePoint=new LoanScorePoint();
            LoanType = new LoanType();
            User = new User();
        }
        public Int64 Id { get; set; }
        public Int64 LlId { get; set; }
        public string CustomerName { get; set; }
        public double LoanAmount { get; set; }
        public string Level { get; set; }
        public string LoanTag { get; set; }
        public double Score { get; set; }
        public string Result { get; set; }
        public string Decision { get; set; }
        public string Remarks { get; set; }
        public Int32 UserId { get; set; }
        public Int32 DEVI_LEVEL1 { get; set; }
        public Int32 DEVI_LEVEL2 { get; set; }
        public Int32 DEVI_LEVEL3 { get; set; }
        public Int32 DEVI_LEVEL4 { get; set; }
        public DateTime EntryDate { get; set; }


        public PL PL { get; set; }
        public LoanInformation LoanInformation { get; set; }
        public Product Product { get; set; }
        public Segment Segment { get; set; }
        public LoanScorePoint LoanScorePoint {get;set;}
        public LoanType LoanType { get; set; }
        public User User { get; set; }

    }
}
