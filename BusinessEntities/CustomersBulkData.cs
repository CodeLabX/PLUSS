﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class CustomersBulkData
    {
        public CustomersBulkData()
        {
            //Constructor logic here.
        } 
        public int ID { get; set; }

        public string RefType { get; set; }
        public string RefrenceID { get; set; }
        public string MasterNo { get; set; }
        public string RelationshipNo { get; set; }
        public string FullName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string DOB { get; set; }
        public string HighestEduLevel { get; set; }
        // Address Res
        public string AddressRes { get; set; }
        public string AddressRes_PostalCode { get; set; }
        public string AddressRes_CityCode { get; set; }
        public string AddressRes_CityName { get; set; }
        public string AddressRes_CountryCode { get; set; }
        // Address Off
        public string AddressOff { get; set; }
        public string AddressOff_PostalCode { get; set; }
        public string AddressOff_CityCode { get; set; }
        public string AddressOff_CityName { get; set; }
        public string AddressOff_CountryCode { get; set; }

        public string ProfessionCode { get; set; }
        public string OrganisationName { get; set; }
        public string Department { get; set; }

        public string PassportNo { get; set; }
        public string PassportExp { get; set; }
        public string UniqueID2 { get; set; }
        public string UniqueID2Exp { get; set; }

        public string TIN { get; set; }

        public string NID_O { get; set; }
        public string NID_OExp { get; set; }
        public string NID_N { get; set; }
        public string NID_NExp { get; set; }


        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string SpouseName { get; set; }
        
        public string MakerId { get; set; }
        public DateTime MakeDate { get; set; }
        public string CheckerId { get; set; }
        public DateTime CheckDate { get; set; }
    }
}
