﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class LoanApplicationQueue
    {
        public LoanApplicationQueue()
        {
        }
        public Int64 LLId { get; set; }
        public string CustomerName { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public DateTime ReceiveDate { get; set; }
    }
}
