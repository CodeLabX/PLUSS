﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class CheckList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Status { get; set; }
        public string StatusMsg { get; set; }
    }

    public class UpdateStatus
    {
        public int LOAN_STATUS_ID { get; set; }
        public string LOAN_STATUS_NAME { get; set; }
        public int LOAN_STATUS_TYPE { get; set; }
        public int JOB_STAT { get; set; }
        public int UserId { get; set; }
    }
}
