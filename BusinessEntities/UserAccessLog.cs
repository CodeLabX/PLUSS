﻿using System;

namespace BusinessEntities
{
    public class UserAccessLog
    {
        public UserAccessLog()
        {
        }
        public Int32 AcessLogId { get; set; }
        public string UserLogInId { get; set; }
        public string IP { get; set; }
        public DateTime LogInDateTime { get; set; }
        public DateTime LogOutDateTime { get; set; }
        public Int32 LogInCount { get; set; }
        public Int32 userLoginSuccessStatus { get; set; }
        public Int32 userLoginUnSuccessStatus { get; set; }
        public Int32 userLoginStatus { get; set; }
        public string AccessFor { get; set; }
    }
}
