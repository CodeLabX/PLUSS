﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class GroupMember
    {
        public GroupMember()
        {
        }

        public int GroupMemberId { get; set; }
        public int UserID { get; set; }
        public int GroupID { get; set; }
    }
}
