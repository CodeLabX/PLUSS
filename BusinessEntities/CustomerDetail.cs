﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Customer detail object class.
    /// </summary>
    public class CustomerDetail
    {
        public CustomerDetail()
        {
            //Constructor logic here.
        }
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string CustomerLLId { get; set; }
        public string Facility { get; set; }
        public char Flag { get; set; }
        public double OutStanding { get; set; }
        public double OrginalLimit { get; set; }
        public double RePayment { get; set; }
        public DateTime DisbursDate { get; set; }
        public DateTime ExposureDate { get; set; }
        public double CashSecurity { get; set; }
        public int Mob12 { get; set; }
               
    }
}
