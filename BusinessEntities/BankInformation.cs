﻿using System;

namespace BusinessEntities
{
    public class BankInformation
    {
        public Int32 Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string StatusMsg { get; set; }
    }
}
