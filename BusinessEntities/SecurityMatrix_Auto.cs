﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class SecurityMatrix_Auto
    {
        public string Module { get; set; }
        public string AutoAdmin { get; set; }
        public string AutoAnalystAndApprover { get; set; }
        public string AutoApprover { get; set; }
        public string CIAnalyst { get; set; }
        public string CISupport { get; set; }
        public string OpsChecker { get; set; }
        public string OpsMaker { get; set; }
        public string Sales { get; set; }

    }
}
