﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BankStatement
    {
        public BankStatement()
        {
        }
        public Int64 BankStatementId { get; set; }
        public Int64 LlId { get; set; }
        public Int16 Bank_ID { get; set; }
        public Int16 Branch_ID { get; set; }
        public string ACNO { get; set; }
        public Int16 ACType_Id { get; set; }
        public string ACName { get; set; }
        public string StartMonth { get; set; }
        public string EndMonth { get; set; }
        public string Statement { get; set; }
        public DateTime EntryDate { get; set; }
        public Int64 UserId { get; set; }
    }
}
