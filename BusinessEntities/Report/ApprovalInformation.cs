﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class ApprovalInformation
    {
        public ApprovalInformation()
        {
            //Constructor logic here.
        }
        public DateTime AppraisalDate { get; set; }
        public string LLId { get; set; }
        public string Customer { get; set; }
        public string ApprovedAmount { get; set; }
        public Double Income { get; set; }
        public Double DBR { get; set; }
        public Double MUE { get; set; }
        public string Segment { get; set; }
        public Double ApprovedAmt { get; set; }
        public string Level { get; set; }
        public Double IR { get; set; }
        public Double EMI { get; set; }
        public Double UDCAmount { get; set; }
        public int Tenure { get; set; }
        public string Mode { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public int PdcDate { get; set; }
        public Double SCBExposure { get; set; }
        public Double CrdtCrdLimit { get; set; }
        public Double TopUpAmount { get; set; }

        //public string[] PreCondition { get; set; }
        public string PreCondition1 { get; set; }
        public string PreCondition2 { get; set; }
        public string PreCondition3 { get; set; }
        public string PreCondition4 { get; set; }
        public string PreCondition5 { get; set; }

        public int LoanTypeId { get; set; }
    }
}
