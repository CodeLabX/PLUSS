﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class NetIncomeInfo
    {
        public NetIncomeInfo()
        {
        }

        public double NetIncome { set; get; }
        public double DBR { set; get; }
        public double MUE { set; get; }
        public double LoanAmount { set; get; }
        public double EMI { set; get; }
        public double Tenor { set; get; }
        public double IR { set; get; }
    }
}
