﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class DailyNoofApprovedVolume
    {
        public DailyNoofApprovedVolume()
        {
        }
        public Int32 ProductId { get; set; }
        public string ProductName { get; set; }

        public double LWDQuantity { get; set; }
        public double LWDVolume { get; set; }

        public double MTDQuantity { get; set; }
        public double MTDVolume { get; set; }

        public double YTDQuantity { get; set; }
        public double YTDVolume { get; set; }

    }
}
