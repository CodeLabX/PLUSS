﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class StrengthWeaknessInfo
    {
        public StrengthWeaknessInfo()
        {
        }

        public int StrengthAcId { set; get; }
        public string StrengthAcRelationshipWith { set; get; }
        public int StrengthResidentialStatusId { set; get; }
        public string StrengthResidentialStatus { set; get; }
        public int StrengthCreditCardRepaymentId { set; get; }
        public string StrengthCreditCardRepayment { set; get; }
        public int StrengthPreviousRepaymentId1 { set; get; }
        public string StrengthPreviousRepayment1 { set; get; }
        public int StrengthPreviousRepaymentId2 { set; get; }
        public string StrengthPreviousRepayment2 { set; get; }
        public string StrengthOthers { set; get; }

        public int WeaknessRepaymentNotSatisfactoryId { set; get; }
        public string WeaknessRepaymentNotSatisfactory { set; get; }
        public int WeaknessCashSalariedId { set; get; }
        public string WeaknessCashSalaried { set; get; }
        public int WeaknessFrequentChequeReturnId { set; get; }
        public string WeaknessFrequentChequeReturn { set; get; }
        public string WeaknessOthers { set; get; }
        public int WeaknessLoanType { set; get; }
        public int WeaknessDPD { set; get; }

        public string StrengthCreditCardRepaymentDateTemp { set; get; }
        public DateTime StrengthCreditCardRepaymentDate { set; get; }
        public string StrengthPreviousRepaymentDate1Temp { set; get; }
        public DateTime StrengthPreviousRepaymentDate1 { set; get; }
        public string StrengthPreviousRepaymentDate2Temp { set; get; }
        public DateTime StrengthPreviousRepaymentDate2 { set; get; }
        public int CreditCardLimitId { set; get; }
        public string CreditCardLimit { set; get; }
        public int StrengthPreviousRepaymentLoneType1Id { set; get; }
        public string StrengthPreviousRepaymentLoneType1 { set; get; }
        public int StrengthPreviousRepaymentLoneType2Id { set; get; }
        public string StrengthPreviousRepaymentLoneType2 { set; get; }
    }
}
