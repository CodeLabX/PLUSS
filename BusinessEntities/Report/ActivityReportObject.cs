﻿using System;

namespace BusinessEntities.Report
{
    public class ActivityReportObject
    {
    public Int64 Id { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }

    public Int32 RcFL { get; set; }
    public Int32 RcPLSAL { get; set; }
    public Int32 RcPLBIZ { get; set; }
    public Int32 RcIPF { get; set; }
    public Int32 RcSTFL { get; set; }
    public Int32 ReTotal { get; set; }

    public Int32 PeFL { get; set; }   
    public Int32 PePLSAL { get; set; }
    public Int32 PePLBIZ { get; set; }
    public Int32 PeIPF { get; set; }
    public Int32 PeSTFL { get; set; }
    public Int32 PeTotal { get; set; }

    public Int32 PoFL { get; set; }
    public Int32 PoPLSAL { get; set; }
    public Int32 PoPLBIZ { get; set; }
    public Int32 PoIPF { get; set; }
    public Int32 PoSTFL { get; set; }
    public Int32 PoTotal { get; set; }

    }
}
