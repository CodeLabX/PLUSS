﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class StandingOrderInfo
    {
        public StandingOrderInfo()
        {
        }

        public string StandingOrder { set; get; }
        public string ACNo { set; get; }
        public string RpmnthDate { set; get; }
    }
}
