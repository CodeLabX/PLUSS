﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class SecurityMatrix
    {
        
        public string Module { get; set; }
        public string Approver { get; set; }
        public string Analyst { get; set; }
        public string BusinessAdmin { get; set; }
        public string ScoreAdmin { get; set; }
        public string SupportUser { get; set; }
        public string Adminmaker { get; set; }
        public string Adminchecker { get; set; }
        
    }
}
