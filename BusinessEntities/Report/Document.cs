﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class Document
    {
        public Document()
        {
            //Constructor logic here.
        }
        public string LLID { get; set; }
        public string CustomerName { get; set; }
        public string Account { get; set; }
        public string Bank1 { get; set; }
        public string Bank2 { get; set; }
        public string Bank3 { get; set; }
        public string Bank4 { get; set; }
        public string Bank5 { get; set; }
        public string Bank6 { get; set; }
        public string CreditSigName { get; set; }
    }
}
