﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class InitialInfo
    {
        public InitialInfo()
        {
        }

        public int LLID { set; get; }
        public string CustomerName { set; get; }
        public DateTime DOB { set; get; }
        public string MasterNo { set; get; }
        public string LoanType { set; get; }
    }
}
