﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class DeviationInfo
    {
        public DeviationInfo()
        {
        }

        public string Description1 { set; get; }
        public string Description2 { set; get; }
        public string Description3 { set; get; }
        public string Description4 { set; get; }

        public string Code1 { set; get; }
        public string Code2 { set; get; }
        public string Code3 { set; get; }
        public string Code4 { set; get; }

        public string Level1 { set; get; }
        public string Level2 { set; get; }
        public string Level3 { set; get; }
        public string Level4 { set; get; }
    }
}
