﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class ExposureInfo
    {
        public ExposureInfo()
        {
        }

        public double ExistingPLOutstanding { set; get; }
        public double ExistingPLOriginalLimit { set; get; }
        public double TopUpAmountOutstanding { set; get; }
        //public double TopUpAmountOriginalLimit { set; get; }
        public double TopUpAmountRepayment { set; get; }
        public double TopUpAmountEMIFactor { set; get; }
        //public string TopUpAmountSafetyPlusNo { set; get; }
        public double ThisPLOutstanding { set; get; }
        //public double ThisPLOriginalLimit { set; get; }
        public double ThisPLRepayment { set; get; }

        public string FacalityName1 { set; get; }
        public string FacalityName2 { set; get; }
        public string FacalityName3 { set; get; }
        public string FacalityName4 { set; get; }
        public string FacalityName5 { set; get; }

        public double FacalityOutstanding1 { set; get; }
        public double FacalityOutstanding2 { set; get; }
        public double FacalityOutstanding3 { set; get; }
        public double FacalityOutstanding4 { set; get; }
        public double FacalityOutstanding5 { set; get; }

        public double FacalityOriginalLimit1 { set; get; }
        public double FacalityOriginalLimit2 { set; get; }
        public double FacalityOriginalLimit3 { set; get; }
        public double FacalityOriginalLimit4 { set; get; }
        public double FacalityOriginalLimit5 { set; get; }

        public double FacalityRepayment1 { set; get; }
        public double FacalityRepayment2 { set; get; }
        public double FacalityRepayment3 { set; get; }
        public double FacalityRepayment4 { set; get; }
        public double FacalityRepayment5 { set; get; }

        public DateTime FacalityDisburseDate1 { set; get; }
        public DateTime FacalityDisburseDate2 { set; get; }
        public DateTime FacalityDisburseDate3 { set; get; }
        public DateTime FacalityDisburseDate4 { set; get; }
        public DateTime FacalityDisburseDate5 { set; get; }

        public DateTime FacalityExpiryDate1 { set; get; }
        public DateTime FacalityExpiryDate2 { set; get; }
        public DateTime FacalityExpiryDate3 { set; get; }
        public DateTime FacalityExpiryDate4 { set; get; }
        public DateTime FacalityExpiryDate5 { set; get; }

        public double CreditCardLimitOutstanding { set; get; }
        public double CreditCardLimitOriginalLimit { set; get; }
        public double CreditCardLimitRepayment { set; get; }
        public string CreditCardLimitDisburseDate { set; get; }
        public string CreditCardLimitExpiryDate { set; get; }
    }
}
