﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class UserInfo
    {
        public UserInfo()
        {
        }

        public string UserName { set; get; }
        public string Designation { set; get; }
    }
}
