﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.Report
{
    public class SegmentInfo
    {
        public SegmentInfo()
        {
        }

        public string SegmentCode { set; get; }
        public string SegmentDescription { set; get; }
        public double MaxLoan { set; get; }
        public double MinLoan { set; get; }
        public int MinAge { set; get; }
        public int MaxAge { set; get; }
        public string MaxAgeRemarks { set; get; }
        public string PhysicalVerification { set; get; }
        public string Telephone { set; get; }
        public string Experience { set; get; }
        public string ACRelationship { set; get; }
        public double MinIncome { set; get; }
        public double DBRatio { set; get; }
        public double DBR29 { set; get; }
        public double DBR50 { set; get; }
        public double DBR99 { set;get;}
        public double DBR1K { set; get; }
        public double DBR { set; get; }
        public double Tenur { set; get; }
        public double ProposedLoanAmount { set; get; }
        public double TNR { set; get; }
        public double MUE { set; get; }
        public double MUELOC { set; get; }
        public string GuaranteeRef { set; get; }
    }
}
