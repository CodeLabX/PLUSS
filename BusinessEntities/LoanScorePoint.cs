﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class LoanScorePoint
    {
        public LoanScorePoint()
        {
        }
        public Int64 Id { get; set; }
        public Int64 LlId { get; set; }
        public double CutOfScore { get; set; }
        public double IntreceptValue { get; set; }
        public double WorkExperiencePoint { get; set; }
        public double IncomeRatioPoint { get; set; }
        public double ResidentypePoint { get; set; }
        public double MaritalStatusPoint { get; set; }
        public double QualificationPoint { get; set; }
        public double GrossMonthlyIncomePoint { get; set; }
        public double SpouseOccupationPoint { get; set; }
        public double VehiclPoint { get; set; }
        public double TotalPoint { get; set; }
        public string Result { get; set; }
    }
}
