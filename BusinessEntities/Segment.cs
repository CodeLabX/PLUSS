﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class Segment
    {
        public Segment()
        {
            
        }
        public Int32 Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProfessionId {get;set;}

        public Int64 MaxLoanAmoutn { get; set; }
        public Int64 MinLoanAmoutn { get; set; }
        public Int64 MinIncome { get; set; }

        public double IR { get; set; }
        public Int32 Tenor { get; set; }

        public Int32 DBR29 { get; set; }
        public Int32 DBR50 { get; set; }
        public Int32 DBR99 { get; set; }
        public Int32 DBR1K { get; set; }

        public Int32 L1MultiPlire29 { get; set; }
        public Int32 L1MultiPlire50 { get; set; }
        public Int32 L1MultiPlire99 { get; set; }
        public Int32 L1MultiPlire1K { get; set; }

        public Int32 L2MultiPlire29 { get; set; }
        public Int32 L2MultiPlire50 { get; set; }
        public Int32 L2MultiPlire99 { get; set; }
        public Int32 L2MultiPlire1K { get; set; }

        public Int32 L2TopMultiPlire29 { get; set; }
        public Int32 L2TopMultiPlire50 { get; set; }
        public Int32 L2TopMultiPlire99 { get; set; }
        public Int32 L2TopMultiPlire1K { get; set; }

        public Int32 FirstPay { get; set; }
        public Int32 MultiPlire { get; set; }

        public Int32 MUEMul { get; set; }
        public Int32 MUELar { get; set; }
        public Int32 MUELoc { get; set; }

        public Int32 MinAge { get; set; }
        public Int32 MaxAge { get; set; }
        public string MaxAgeRemarks { get; set; }

        public string PhysicalVerification { get; set; }
        public string GuranteeReferenc { get; set; }
        public string Experience { get; set; }
        public string ACRel { get; set; }
        public string CriteriaCode { get; set; }
        public string AssetCode { get; set; }
        public string TelePhone { get; set; }
    }
}
