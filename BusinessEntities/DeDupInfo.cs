﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    public class DeDupInfo
    {
        public ApplicantContactNo ContactNumber { get; set; }
        public ApplicantIds ApplicantId { get; set; }
        public ApplicantAccountNo AccountNo { get; set; }
        public ApplicantOtherBankAccount OtherBankAccount { get; set; }

        public DeDupInfo()
        {
            ContactNumber = new ApplicantContactNo();
            ApplicantId = new ApplicantIds();
            AccountNo = new ApplicantAccountNo();
            OtherBankAccount = new ApplicantOtherBankAccount();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string ScbMaterNo { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Profession { get; set; }
        public int ProductId { get; set; }
        public string DeclineReason { get; set; }
        public DateTime DeclineDate { get; set; }
        public string BusinessEmployeeName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string BankBranch { get; set; }
        public string Source { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string Tin { get; set; }
        public string IdType { get; set; }
        public string IdNo { get; set; }
        public string IdType1 { get; set; }
        public string IdNo1 { get; set; }
        public string IdType2 { get; set; }
        public string IdNo2 { get; set; }
        public string IdType3 { get; set; }
        public string IdNo3 { get; set; }
        public string IdType4 { get; set; }
        public string IdNo4 { get; set; }
        public string EducationalLevel { get; set; }
        public string MaritalStatus { get; set; }
        public string ResidentaileStatus { get; set; }
        public string CriteriaType { get; set; }
        public DateTime EntryDate { get; set; }
        public string Active { get; set; }
        public string MfuTin { get; set; }

        public DateTime ReceivedDate { get; set; }
        public string CreditCardNo { get; set; }
        public string CreditCardNo2 { get; set; }
        public string CreditCardNo3 { get; set; }
        public string CreditCardNo4 { get; set; }
        public string CreditCardNo5 { get; set; }
        public string NameOfCompany { get; set; }
        public string OfficeAddress { get; set; }
        public List<string> MatchFound { get; set; }
        public int MatchFoundNumber { get; set; }
        public string NameFromLI { get; set; }

        public List<string> SplittedContactList { get; set; }
        public List<string> SplittedAccountList { get; set; }
        public List<string> SplittedIdList { get; set; }
        public string LoanAccountNo { get; set; }

        public string MakerPsId { get; set; }
        public DateTime MakeDate { get; set; }
        public string CheckerPsId { get; set; }
        public DateTime CheckDate { get; set; }
        public string MultiLocatorRemarksID { get; set; }
    }
}

