﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    public class LocationInformation
    {
        public LocationInformation()
        { 
            
        }

        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
