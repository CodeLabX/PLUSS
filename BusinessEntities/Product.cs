﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    /// <summary>
    /// Product object class.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Product()
        {
            //Constructor logic here.
        }

        public Int32? ProductId { get; set; }
        public string ProductName { get; set; }

        public int ProdID { get; set; }
        public string ProdName { get; set; }
        public string ProdCode { get; set; }
        public string ProdType { get; set; }
        public int? ProdStatus { get; set; }
        public string ProdStatusName { get; set; }
        public string ProdTypeName { get; set; }
        public int IdLearner { get; set; }
        public string NameLearner { get; set; }
        public int UserId { get; set; }
        public string ProdLoanType { get; set; }
    }
}
