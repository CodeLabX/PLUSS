﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    public class PayRoll
    {
        public PayRoll()
        {
        }
        public int CompID { get; set; }
        public string CompName { get; set; }
        public string CompSegmentID { get; set; }
        public string CompStructure { get; set; }
        public string CompDocategorized { get; set; }
        public string CompCatstatus { get; set; }
        public double? CompMue { get; set; }
        public string CompIrwitheosb { get; set; }
        public string CompIrwithouteosb { get; set; }
        public string CompCcucondition { get; set; }
        public string CompCcuremarks { get; set; }
        public string CompProposdby { get; set; }
        public DateTime CompApprovaldate { get; set; }
        public int? CompStatus { get; set; }
        public string Date { get; set; }
        public string ExtraBenefit { get; set; }
        public string ExtraField1 { get; set; }
        public string ExtraField2 { get; set; }
        public string ExtraField3 { get; set; }
        public string ExtraField4 { get; set; }

    }
}
