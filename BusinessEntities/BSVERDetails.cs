﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BSVERDetails
    {
        public BSVERDetails()
        {
        }
        public Int64 BasVerDetailsId { get; set; }
        public Int64 BasVerMaster_Id { get; set; }
        public Int16 SlNo { get; set; }
        public string Particular { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public double Withdrawl { get; set; }
        public double Diposit { get; set; }
        public double Balance { get; set; }
        public Int64 OldId { get; set; }
    }
}
