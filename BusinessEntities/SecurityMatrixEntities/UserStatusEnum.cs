﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.SecurityMatrixEntities
{
    public enum UserStatus
    {
        Inactive = 0,
        Active = 1,
        Deleted = 2,
        Dormant = 3,
        Rejected = 4
    }
}
