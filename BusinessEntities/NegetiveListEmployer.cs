﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class NegetiveListEmployer
    {
        public NegetiveListEmployer()
        {
            //Constructor logic here.
        } 
        public int ID { get; set; }
        public string BankCompany { get; set; }
        public string Branch { get; set; }
        public string Address { get; set; }
        public string NegetiveList { get; set; }
        public string WatchList { get; set; }
        public string Exclusion { get; set; }
        public string Delist { get; set; }
        public string Restriction { get; set; }
        public string Remarks { get; set; }
        public DateTime InclusionDate { get; set; }
        public DateTime ChangeDate { get; set; }
        public string ChangeDate2 { get; set; }
        public DateTime EntryDate { get; set; }
        public int UserId { get; set; }
        public string MakerPsId { get; set; }
    }
}
