﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Score card condition object class.
    /// </summary>
    public class ScoreCardCondition
    {
        public ScoreCardCondition()
        {
            //Constructor logic here.
            VariableName = new VariableName();
        }
        public Int32 Id { get; set; }
        public Int32 VariableId { get; set; }
        public string CompareOperator { get; set; }
        public double CompareValue { get; set; }
        public string  LogicalOperator { get; set; }
        public double ScoreValue { get; set; }
        public double NegativeValue { get; set; }
        public double CuttOffScore { get; set; }
        public double InterceptValue { get; set; }
        public VariableName VariableName { get; set; }
        public string Status { get; set; }
    }
}
