﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ApplicantAccountNo
    {
        public ApplicantAccountNo()
        {
            //Constructor logic here.
        }
        
        public string PreAccNo1 { get; set; }
        public string MasterAccNo1 { get; set; }
        public string PostAccNo1 { get; set; }

        public string PreAccNo2 { get; set; }
        public string MasterAccNo2 { get; set; }
        public string PostAccNo2 { get; set; }

        public string PreAccNo3 { get; set; }
        public string MasterAccNo3 { get; set; }
        public string PostAccNo3 { get; set; }

        public string PreAccNo4 { get; set; }
        public string MasterAccNo4 { get; set; }
        public string PostAccNo4 { get; set; }

        public string PreAccNo5 { get; set; }
        public string MasterAccNo5 { get; set; }
        public string PostAccNo5 { get; set; }
    }
}
