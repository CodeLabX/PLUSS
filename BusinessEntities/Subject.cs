﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class Subject
    {
        public Subject()
        {
        }
        public Int32 Id { get; set; }
        public string Name { get; set; }
    }
}
