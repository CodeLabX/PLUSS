﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Id type object class.
    /// </summary>
    public class IdType
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public IdType()
        {
            //Constructor logic here.
        }
        /// <summary>
        /// Propertys
        /// </summary>
        public Int32 Id { get; set; }
        public string Name { get; set; }
    }
}
