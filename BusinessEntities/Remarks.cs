﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Remarks object class.
    /// </summary>
    public class Remarks
    {
        public Remarks()
        {
            //Constructor logic here.
            RemarksForName = new RemarksFor();
        }
        public Int32 Id { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 Dtype { get; set; }
        public string Name { get; set; }
        public Int32 RemarksNo { get; set; }
        public Int32 RemarksFor { get; set; }
        public int Status { get; set; }
        public RemarksFor RemarksForName { get; set; }
    }
}
