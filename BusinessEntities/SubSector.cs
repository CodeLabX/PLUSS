﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class SubSector
    {
        public SubSector()
        {
        }
        public Int64 Id {get; set;}
        public string Name {get; set;}
        public Int32 SectorId {get; set;}
        public string SectorName { get; set; }
        public string Code {get; set;}
        public string InterestRate {get; set;}
        public string IRCode {get; set;}
        public string Color { get; set; }
        public int Status {get; set;}
    }
}
