﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class UserTModel
    {
        public int Id { get; set; }
        public int USER_ID { get; set; }
        public string USER_CODE { get; set; }
        public string USER_NAME { get; set; }
        public string USER_DESIGNATION { get; set; }
        public string USER_EMAILADDRESS { get; set; }
        public string USER_LEVELCODE { get; set; }
        public string USER_PASSWORD { get; set; }
        public DateTime USER_CHANGDATE { get; set; }
        public int USER_CHANGBY { get; set; }
        public int USER_LEVEL { get; set; }
        public string USER_LEVEL_NAME { get; set; }
        public int USER_STATUS { get; set; }
        public DateTime USER_ENTRYDATE { get; set; }
        public int USER_USER_ID { get; set; }
        public int USER_LOGINSTATUS { get; set; }
        public int AUTO_USER_LEVEL { get; set; }
        public int IS_APPROVED { get; set; }
        public DateTime APPROVED_DATE { get; set; }
        public string APPROVED_BY { get; set; }
        public int IS_DELETED { get; set; }
        public string DELETED_BY { get; set; }
        public int IsAccessBoth { get; set; }
        public int Module { get; set; }
        public int Type { get; set; }

        public int IS_REJECTED { get; set; }
        public string APPROVER_COMMENT { get; set; }
        public string USER_STATUS_NAME { get; set; }
        public string ROLE_NAME { get; set; }

        public override string ToString()
        {
            return "PSID : " + USER_CODE + ", NAME : " + USER_NAME + ", DESIGNATION : " + USER_DESIGNATION + ", EMAIL : " + USER_EMAILADDRESS + ", ROLE : " + ROLE_NAME + ", ARM CODE:" + USER_LEVELCODE + ", STATUS : " + USER_STATUS_NAME;
        }
    }
}
