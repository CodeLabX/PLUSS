﻿
namespace BusinessEntities
{
    public class PddDeviation
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int DescriptionId { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
    }
}
