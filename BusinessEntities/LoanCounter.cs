﻿using System;
namespace BusinessEntities
{
    public class LoanCounter
    {
        public int Id { get; set; }
        public DateTime DATE { get; set; }
        public int RCFL { get; set; }
        public int RCPL_SAL { get; set; }
        public int RCPL_BIZ { get; set; }
        public int RCIPF { get; set; }
        public int RCSTFL { get; set; }
        public int PEFL { get; set; }
        public int PEPL_SAL { get; set; }
        public int PEPL_BIZ { get; set; }
        public int PEIPF { get; set; }
        public int PESTFL { get; set; }
        public int POFL { get; set; }
        public int POPL_SAL { get; set; }
        public int POPL_BIZ { get; set; }
        public int POIPF { get; set; }
        public int POSTFL { get; set; }
    }
}
