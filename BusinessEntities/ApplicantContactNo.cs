﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ApplicantContactNo
    {
        public ApplicantContactNo()
        {
            //Constructor logic here.
        }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string ContactNo3 { get; set; }
        public string ContactNo4 { get; set; }
        public string ContactNo5 { get; set; }
    }
}
