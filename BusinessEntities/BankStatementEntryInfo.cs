﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BankStatementEntryInfo
    {
        public BankStatementEntryInfo()
        { 
        }
        public Int64 BankStatementId { get; set; }
        public Int64 LlId { get; set; }
        public Int16 Bank_ID { get; set; }
        public string BankName { get; set; }
        public Int16 Branch_ID { get; set; }
        public string BranchName { get; set; }
        public string ACNO { get; set; }
        public Int16 ACType_Id { get; set; }
        public string ACTypeName { get; set; }
        public string ACName { get; set; }
        public string StartMonth { get; set; }
        public double AVGBALANCE12Month { get; set; }
        public double AVGBALANCE6Month { get; set; }
        public double CTO12Month { get; set; }
        public double CTO6Month { get; set; }
    }
}
